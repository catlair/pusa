<?php

/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

namespace catlair;



require_once ROOT."/core/controller.php";


class TWebController extends TController
{
    public      $Session;
    private     $Web;



    /*
        Constructor
    */
    public function __construct( $AWeb )
    {
        parent::__construct( $AWeb );
        $this -> Web        = $AWeb;
        $this -> Session    = $AWeb -> GetSession();
    }



    public function GetSite()
    {
        return $this -> GetIncome( 'IDSite', $this -> Web -> GetIDSite() );
    }



    /*
        Return language
    */
    public function GetLang()
    {
        return $this -> Web -> GetIDLang();
    }


    /*
        Заполнение сообщения об ошибке
        Функция должна быть переписана в потомках
    */
     protected function MessageByCode()
     {
         return $this -> Web -> GetTemplate
         (
            $this -> Code,
            $this -> Code,
            $this -> Web -> GetIDSite(),
            $this -> Web -> GetIDLang()
         ) . ' [' . $this -> GetMessage() . ']';
     }
}
