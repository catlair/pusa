<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/



namespace catlair;

/*
    Source utils
    CatlairPHP
    still@itserv.ru
*/

require_once( ROOT . '/web/web_utils.php' );



/*
    Функции для работы с файлам исходного контента
*/
function clSourcesPath( $AIDSite )
{
    return clSitePath( $AIDSite ) . '/src';
}



/*
    возвращает структуру пути исходного кода
*/
function clSourceFile( $AID, $AIDSite, $AIDLang )
{
//    $Result = clPathControl( clSourcesPath( $AIDSite ) . clScatterName( $AID ) . '/' . $AIDLang . '/' . $AID );
    $Result = clPathControl( clSourcesPath( $AIDSite ) . '/' . $AIDLang . '/' . $AID );
    return $Result;
}



/*
    Проверка существования исходника для сайта
*/
function clSourceExists( $AID, $AIDSite, $AIDLang )
{
    return file_exists( clSourceFile( $AID, $AIDSite, $AIDLang ));
}



/*
    возвращает имя файла контента исходя из желательных языка и сайта
    подбирается максимально возможный контент на основании данных
    в случае отсутствия файла возвращается false.
*/
function clSourceFileAny
(
    string  $AID,                   /* идентификатор дескрипта */
    array   $AIDSiteTemplate,       /* Array of sites */
    string  $AIDLang = null,        /**/
    string  $AIDLangDefault = null  /**/
)
{
    $Result = null;
    $c = count( $AIDSiteTemplate );

    for( $i = 0; $i < $c && empty( $Result ); $i++ )
    {
        $IDSite = $AIDSiteTemplate[ $i ];

        $Result = clSourceFile( $AID, $IDSite, $AIDLang );
        if ( !file_exists( $Result ))
        {
            $Result = clSourceFile( $AID, $IDSite, $AIDLangDefault );
            if ( !file_exists( $Result )) $Result = null;
        }
    }

    return $Result;
}



/*
    Получение контента дескрипта по идентификатору
*/
function clGetSource
(
    $AID,                               /* IDDescript */
    $ADefault           = null,         /* Default value if content is not exists */
    $AIDSite            = SITE_DEFAULT, /* ID site for search content */
    $AIDLang            = null,         /* ID language */
    $AIDLangDefault     = null,         /* ID language default */
    $AIDSiteTemplate    = [],           /* List of ID Sites for template request */
    $AStrict            = false         /* true - return the content stricted for site and language or false for alseways */
)
{
    if ( $AStrict )
    {
        $File = clSourceFile( $AID, $AIDSite, empty( $AIDLang ) ? $AIDLangDefault : $AIDLang );
        if( !file_exists( $File )) $File = null;
    }
    else
    {
        $File = clSourceFileAny
        (
            $AID,
            $AIDSiteTemplate,
            $AIDLang,
            $AIDLangDefault
        );
    }

    if( !empty( $File ))
    {
        $Result = file_get_contents( $File );
    }
    else
    {
        $Result = $ADefault;
    }

    return $Result;
}



/*
    Сохранение контента дескрипта по идентификатору
*/
function clSetSource( $AID, $AIDSite, $AIDLang, $ASource )
{
    $File = clSourceFile( $AID, $AIDSite, $AIDLang );
    $Path = dirname( $File );

    if( !empty( $File ) && CheckPath( $Path ))
    {
        if( empty( $ASource ))
        {
            if( file_exists( $File )) unlink( $File );
        }
        else
        {
            if( !file_put_contents( $File, $ASource )) $File = false;
        }
    }
    else
    {
        $File = false;
    }
    return $File;
}
