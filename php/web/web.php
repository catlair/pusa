<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

namespace catlair;

/* Core library */
require_once( ROOT . '/core/loader.php' );
require_once( ROOT . '/core/utils.php' );
require_once( ROOT . '/core/debug.php' );
require_once( ROOT . '/core/moment_util.php' );
require_once( ROOT . '/core/builder.php' );
require_once( ROOT . '/core/config.php' );
require_once( ROOT . '/core/cli_util.php' );
require_once( ROOT . '/core/html.php' );

/* Web libraries */
require_once( ROOT . '/web/session.php' );
require_once( ROOT . '/web/web_utils.php' );
require_once( ROOT . '/web/web_pusa.php' );
require_once( ROOT . '/web/source_utils.php' );
require_once( ROOT . '/web/web_file.php' );



define( 'LANG_UNKNOWN'          , 'language_unknown');  /* Default language identiter */
define( 'SITE_DEFAULT'          , 'site_default' );     /* Default site identifier */
define( 'SITE_UNKNOWN'          , 'site_unknown' );     /* Unknown site identifier */
define( 'DOMAIN_LOCALHOST'      , 'localhost');         /* Localhost constatn like default domain*/

define( 'ACCOUNT_ROOT'          , 'root');              /* Root accoutn identifier */
define( 'ACCOUNT_ADMINISTRATOR' , 'administrator');     /* Administrator account identifier */
define( 'ACCOUNT_GUEST'         , 'guest');             /* Guest account identifier */
define( 'ACCOUNT_USER'          , 'user');              /* Any user accaount identifier */

define( 'FILE_RIGHT'            , 0770 );               /* Default file right for files and folders */



class TWeb extends TBuilder
{
    const IMPERSONATE_REQUEST   = 'r';              /* Only one authorized call form the impersonate operation */
    const IMPERSONATE_SESSION   = 's';              /* Set IDUser for the session. The user will be authorized after the impersonation operation. */

    protected $Engine           = null;             /* Engine identifier */
    protected $IDSite           = null;             /* Current site identifier */
    protected $IDSiteTemplate   = [];               /* Array of sites for gets templates */
    protected $IDLang           = null;             /* Current language identifier */
    protected $IDUser           = null;             /* Current user identifier */
    protected $IDDomain         = null;             /* Current domain */
    protected $IDGuest          = ACCOUNT_GUEST;    /* Guset user for this appliction example */

    protected $IDSiteLibrary    = null;             /* Controller and other libraries for this application */

    public $Domain              = null;
    public $Session             = null;

    /* Daemon */
    private $Name               = null;             /* Service name */
    private $SkipPause          = false;            /* True for main pause skeep */
    private $MainPause          = CL_SECOND;        /* Main timeout */
    private $DaemonPusa         = null;             /* Pusa controller for daemon */
    private $DaemonPusaMethod   = null;

    private $Builder            = null;



    /*
        It can not be called directly.
        Use Create().
    */
    function __construct
    (
        &$ADomain
    )
    {
        parent :: __construct();

        /* For access from the wild places. Only for debuging in nonsingltone modules. */
        global $Web;
        $Web = $this;

        /* Set time to GMT. All application must work with GMT date. */
        date_default_timezone_set( 'GMT' );

        /* Set domain parameter */
        $this -> Domain = $ADomain;

        /*
            Collect income parameters
            LABEL_SERIALIZE_POST
        */
        $Income = array_merge
        (
            GetCLIParameters(),                                     /* Read cli params */
            (array)$this -> Domain -> GetParam( 'DefaultURL', [] ), /* Read params from URL */
            $_COOKIE,                                               /* Read cookies */
            $_GET,                                                  /* Read GET */
            $_POST                                                  /* Read POST */
        );


        /* Decode all income parameters */
        foreach( $Income as $Key => $Value )
        {
            $j = json_decode( $Value, true );
            $this -> GetIncome() -> SetParam( $Key, gettype( $j ) == 'array' ? $j : $Value );
        }

        /* New session object */

        $this -> Session =
        TSession::Create
        (
            $ADomain -> GetString( 'SSLMethod'          , 'aes-256-cbc' ),
            $ADomain -> GetString( 'SSLKey'             , '' ),
            $ADomain -> GetString( 'SSLLengthVector'    , 16 )
        )
        -> Open( $this -> GetIncome() -> GetString( 'session') )
        -> SetExpireAfter( $ADomain -> GetInteger( 'SessionExpireMin', 60 ) * TMoment :: MINUTE );

        $this
        /* Get guest user */
        -> SetIDGuest( $ADomain -> GetString( 'IDGuest', ACCOUNT_GUEST ))

        /* Get site from CLI > Session > DomainConfig */
        -> SetIDSite
        (
            GetIncomeCLI
            (
                'IDSite',
                'Input the site identifier',
                $this -> Session -> GetSite( $ADomain -> GetString( 'IDSite' ))
            )
        )

        /* Get user from CLI > Session > DomainConfig.IDUser > IDGuest */
        -> SetIDUser
        (
            GetIncomeCLI
            (
                'IDUser',
                'Input the login of user',
                $this -> Session -> GetLogin( $this -> IDGuest )
            )
        )

        /* Get language */
        -> SetIDLang
        (
            GetIncomeCLI
            (
                'IDLang',
                'Input language identifier',
                $this -> GetPostValue( 'lang', $this -> Session -> GetLanguage( $ADomain -> GetString( 'IDLang' )))
            )
        )

        -> SetIDLangDefault( $ADomain -> GetString( 'IDLangDefault', 'language_default' ))
        -> SetIDSiteTemplate( $ADomain -> GetParam( 'IDSiteTemplate', [  $this -> GetIDSite(), SITE_DEFAULT ]))
        /* Set optimize content flag */
        -> SetOptimize(         $ADomain -> GetBoolean( 'Optimize', true ))
        /* Set debuging content flag */
        -> SetEnabled(          $ADomain -> GetBoolean( 'Debug', true ))
        /* Set library source site */
        -> SetIDSiteLibrary(    $ADomain -> GetString( 'IDSiteLibrary', $this -> GetIDSite() ));

        /*
            Start session log
        */
        $this -> CatchErrors();         /* All errors must return in to the log */

        $this
        -> GetIncome()
        -> AddFromArray
        (
            [
                'IDLang'        => $this -> IDLang,                     /* Set language */
                'IDLangCode'    => clIDLangToHTML( $this -> IDLang ),   /* Set HTML language */
                'IDSite'        => $this -> IDSite,                     /* Set IDSite */
                'IDDomain'      => $this -> IDDomain,                   /* Set IDDomain */
                'IDUser'        => $this -> IDUser                      /* Set IDUser */
            ]
        );

        /* Application of specific parameters */
        $this -> ApplyAdditionalIncomeParams();

        if( $this -> IsCLI() )
        {
            /* Log destined to console */
            $this -> SetDestination( TLog::CONSOLE );
        }
        else
        {
            /* From this point log will be destin to site log path */
            $this
            -> SetDestination( TLog::FILE )
            -> SetLogPath( clSiteLogPath( $this -> GetIDSite() ) . '/web/' . $this -> IDUser )
            -> SetLogFile( $_SERVER[ 'REQUEST_URI' ]);
        }
    }



    /*
        Web constructor
        1. Define the domain from CLI
        2. Create domain parameters
        3. Define the Engine
        4. Create and return the engine
    */
    static public function Create( $AEngineName = null )
    {
        $PreResult = new TResult();

        /*
            (1) Define domain
        */
        /* For CLI domain return from cli param IDDomain */
        $IDDomain = trim
        (
            self::GetServerCLI
            (
                'HTTP_HOST',
                'IDDomain',
                'Choose the domain for request',
                DOMAIN_LOCALHOST,
                true
            )
        );

        /*
            (2) Return TParams for domain
        */
        $Domain = new TParams();

        $FileName = clDomainFile( $IDDomain );
        if( file_exists( $FileName ))
        {
            $JSON = json_decode( file_get_contents( $FileName, true ));
            if( !empty( $JSON ))
            {
                $Domain -> SetParams( $JSON );
            }
            else
            {
                $PreResult -> SetResult
                (
                    'DomainConfigError',
                    'Domain configuration read error for [$IDDomain%]',
                    [ 'IDDomain' => $IDDomain ]
                );
            }
        }
        else
        {
            $Domain -> SetParam( 'IDSite', SITE_UNKNOWN );
            $PreResult -> SetResult
            (
                'DomainConfigFileNotFound',
                'Domain configuration file [%FileName%] not found for domain [%IDDomain%]',
                [
                    'FileName' => $FileName,
                    'IDDomain' => $IDDomain
                ]
            );
        }



        /*
            (3) Define engine class
        */
        $EngineName = empty($AEngineName) ? strtolower( $Domain -> GetString( 'Engine', 'web' )) : $AEngineName;
        /* Set default engine class */
        $EngineClass = 'catlair\\T' . $EngineName;

        if( $EngineName != 'web' )
        {
            $EngineFileName = clGetEngineFile( $EngineName );
            /* Loading the library */
            TLoader::Create()
            -> Load( $EngineFileName )
            -> ResultTo( $PreResult );

            if( $PreResult -> IsOk())
            {
                if( ! class_exists( $EngineClass ))
                {
                    $PreResult -> SetResult
                    (
                        'EngineClassNotFound',
                        'Engine class not found [%EngineClass%] in library [%EngineFileName%]',
                        [
                            'EngineClass'       => $EngineClass,
                            'EngineFileName'    => $EngineFileName
                        ]
                    );
                }
            }
        }

        /*
            (4) Create and return engine
        */

        $Engine =  $PreResult -> IsOk() ? new $EngineClass( $Domain ) : new TWeb( $Domain );
        $Engine -> ResultFrom( $PreResult );
        $Engine -> IDDomain     = $IDDomain;
        $Engine -> Engine       = $EngineClass;
        return  $Engine;
    }



    public function Run()
    {
        /* Befor run event */
        TWebPusa::Create( $this, 'Main' )
        -> SetParams( $this -> GetIncome() -> GetParams() )
        -> Run( 'OnBeforeApplicationRun', false )
        -> ResultTo( $this );

        /* Logging if the logs is enabled */
        if( $this -> GetEnabled() )
        {
            $this
            -> Trace() -> Param( 'SAPI'             , php_sapi_name())
            -> Trace() -> Param( 'ROOT'             , ROOT )
            -> Trace() -> Param( 'IDSiteLibrary'    , $this -> IDSiteLibrary )
            -> Trace() -> Param( 'Engine'           , $this -> Engine )
            -> Trace() -> Param( 'IDDomain'         , $this -> IDDomain )
            -> Trace() -> Param( 'IDSite'           , $this -> Session -> GetSite())
            -> Trace() -> Param( 'IDLang'           , $this -> Session -> GetLanguage())
            -> Trace() -> Param( 'IDUser'           , $this -> GetLogin())
            -> Dump( $_SERVER, '$_SERVER' )
            -> Dump( $this -> GetSession() -> GetJSON(), 'Session' )
            -> Dump( $this -> GetIncome() -> GetParams(), 'Income' )
            ;
        }

        $Daemon =  $this -> GetIncome() -> GetString( 'Daemon' );
        if( ! empty( $Daemon ) && $this -> IsCLI() )
        {
            $this -> DaemonControl( $Daemon );
        }
        else
        {
            $this -> ContentBuild();
        }

        return $this;
    }




    /*
        Build content
    */
    public function ContentBuild()
    {
        /* Set Pusa flag */
        $Pusa = false; //TWebPusa::Create( $this, 'Main' );

        if( $this -> IsOk() )
        {
            /* Set params from Impersonate parameter if exists */
            $ImpersonateData = rawurldecode( $this -> GetIncome() -> GetString( 'impersonate' ));
            if( !empty( $ImpersonateData )) $this -> Impersonate( $ImpersonateData );

            /* Begin processing */
            $this -> SetCode( rcUnknown );

            /* Call pusa */
            $PusaClass  = $this -> GetIncome() -> GetString( 'Pusa' );

            /* Read pusa class from propertyes */
            $Propertyes = $this -> GetIncome() -> GetParam( 'Propertyes' );
            if( !empty( $Propertyes ))
            {
                 $PusaClass = clValueFromObject( $Propertyes, 'Class' );
            }

            if( $this -> GetCode() == rcUnknown && ! empty( $PusaClass ))
            {
                $this -> Begin( 'Pusa run' );
                $PusaMethod = $this -> GetIncome() -> GetString( 'Method' );
                $this -> SetOk();  /* Set Ok after rcUnknown */
                $Pusa = TWebPusa::Create( $this, $PusaClass ) -> SetParams( $this -> GetIncome() -> GetParams() );

                /* Enable debug for client */
                if( $this -> GetEnabled())
                {
                    $Pusa -> Debug( TPusaCore::DEBUG_ON );
                }

                /* Pusa run */
                $Pusa
                -> Run( $PusaMethod )
                -> ResultTo( $this );

                $this -> Session -> Send();     /* Send cookies */
                $this -> End();
            }


            /*
                Call controller
                Old method for CMS
                TODO: remove it
            */
            if( $this -> GetCode() == rcUnknown )
            {
                $this -> Debug('Calls');
                $Call = $this -> GetIncome() -> GetString( 'call' );
                if ( !empty( $Call ))
                {
                    /* Call controller */
                    $this -> SetOk();
                    $Content = $this -> RunCalls( $Call );
                    if( $this -> IsOk() )
                    {
                        $this -> SetContent( $Content );
                        /* Send session AFTER calls */
                        $this -> Session -> Send();     /* Send cookies */
                    }
                }
            }


            /*
                File by path from storage folder
                Example:
                    The file robots.txt for site my_site will download
                    from [ROOT]/site/my_site/src/[ID_LANGUAGE]/robot.txt file
                    by the link https://my_site/robots.txt
            */
            if( $this -> GetCode() == rcUnknown )
            {
                /* Get file part from CLI or FPM*/
                $FilePart = urldecode
                (
                    self::GetServerCLI
                    (
                        'REQUEST_URI',
                        'URI',
                        'Request URI',
                        '',
                        false
                    )
                );

                /* Get file id*/
                $IDFile = explode( '&', explode( '?', $FilePart )[0])[0];
                if( strpos(  $IDFile, '/' ) === 0 )
                {
                    $IDFile = substr( $IDFile, 1 );
                }

                /* */
                if( !empty( $IDFile ))
                {
                    /* On File request event on Pusa */
                    $PusaRequest = TWebPusa::Create( $this, 'Main' )
                    -> SetParams( array_merge( $this -> GetIncome() -> GetParams(), [ 'AIDFile' => $IDFile ]) );

                    if( $PusaRequest -> IsMethod( 'OnFileRequest' ))
                    {
                        $PusaRequest
                        -> Run( 'OnFileRequest' )
                        -> ResultTo( $this );
                    }

                    $this -> Session -> Send();     /* Send cookies */

                    if( count( $PusaRequest -> GetCommands() ) > 0 )
                    {
                        /* Pusa return commands and does not use like a file */
                        $Pusa = $PusaRequest;
                    }
                    else
                    {
                        switch( strtolower( pathinfo( $IDFile, PATHINFO_EXTENSION )))
                        {
                            case 'png':
                            case 'jpg':
                            case 'jpeg':
                            case 'bmp':
                            case 'gif':
                                $this -> SendImage( $IDFile );
                            break;
                            default:
                                $this -> SendFile( $IDFile, true );
                            break;
                        }
                    }
                }
            }


            /* Send file */
            if( $this -> GetCode() == rcUnknown )
            {
                $IDFile = $this -> GetIncome() -> GetString( 'file' );
                if( !empty( $IDFile ))
                {
                    /* Send session BEFORE send image, becouse file content will change OUTPUT */
                    $this -> Session -> Send();     /* Send cookies */
                    $this -> SendFile( $IDFile );
                }
            }

            /* Send image */
            if( $this -> GetCode() == rcUnknown )
            {
                $IDImage = $this -> GetIncome() -> GetString( 'image' );
                if( !empty( $IDImage ))
                {
                    /* Send session BEFORE send image, becouse file content will change OUTPUT */
                    $this -> Session -> Send();     /* Send cookies */
                    $this -> SendImage( $IDImage );
                }
            }

            /*  Prepare content from &template */
            if( $this -> GetCode() == rcUnknown )
            {
                $IDTemplate = $this -> GetIncome() -> GetString( 'template' );
                if( !empty( $IDTemplate ))
                {
                    $this
                    -> SetOk()
                    -> SetContent( $this -> GetTemplate( $IDTemplate ))
                    -> Build();

                    if( $this -> IsOk() )
                    {
                        $this -> Session -> Send();
                    }
                }
            }

            /* Check default pusa controller */
            if
            (
                $this -> GetCode() == rcUnknown &&
                $this -> Domain -> GetBoolean( 'Pusa', false )
            )
            {
                $this -> Begin( 'Default Pusa run' );
                $PusaClass = 'Main';
                $PusaMethod = 'Init';
                $this -> SetOk();  /* Set Ok after rcUnknown */
                $Pusa = TWebPusa::Create( $this, $PusaClass )
                -> SetParams( $this -> GetIncome() -> GetParams() );

                /* Enable debug for client */
                if( $this -> GetEnabled())
                {
                    $Pusa -> Debug( TPusaCore::DEBUG_ON );
                }

                /* Pusa run */
                $Pusa -> Run( $PusaMethod ) -> ResultTo( $this );
                $this -> Session -> Send();     /* Send cookies */
                $this -> End();
            }


            /*  Prepare content */
            if( $this -> GetCode() == rcUnknown )
            {
                /* Read default template */
                $IDTemplate = $this -> Domain -> GetString( 'IDContentStart', 'index.html' );

                $this
                -> SetOk()
                -> SetContent( $this -> GetTemplate( $IDTemplate ))
                -> Build();

                if( $this -> IsOk())
                {
                    $this -> Session -> Send();
                }
            }
        }


        /*
            Error
        */
        if( !$this -> IsOk() )
        {
            $this -> ErrorMessageByCode();

            /* Error for HTML */
            if( empty( $Pusa ) )
            {
                switch( $this -> GetIncome() -> GetString( 'TypeContent', tcHTML ))
                {
                    default:
                    case tcHTML : $this -> HTMLError( '500' ); break;
                }
            }

            $this
            -> Warning( 'Error content build' )
            -> Param( 'Code', $this -> GetCode() )
            -> Param( 'Message', $this -> GetMessage())
            -> Dump( $this -> GetDetailes() );
        }

        /* Pusa result */
        if( !empty( $Pusa ))
        {
            /* On after run method call */
            $Pusa -> OnAfterWebBuild( $this );

            /* Write header JSON if is not content and not cli */
            if( !$Pusa -> IsContent() && !$this -> IsCLI() )
            {
                header( 'Content-Type: application/json; charset=utf-8' );
            }

            /* Return Pusa result as JSON content */
            $this -> SetContent( $Pusa -> GetAnswer( $this ) );
        }

        /* Send to client */
        $this -> Send();

        return $this;
    }



    /*
        Set message in result by code
    */
    public function ErrorMessageByCode()
    {
        /* Get message by code */
        if( empty( $this -> GetMessage() ))
        {
            $this -> SetMessage
            (
                $this -> BuildTemplate( 'errors/' . $this -> GetCode(), '', false )
            );
        }
        return $this;
    }



    /*
        Building html error for current state Result
    */
    protected function HTMLError( $AHTMLError )
    {
        /* Log error */
        $this
        -> Warning( 'Builder result' )
        -> Param( 'Code', $this -> GetCode() )
        -> Param( 'Message', $this -> GetMessage() );

        /* Information about error */
        $Protocol = self::GetServerCLI
        (
            'SERVER_PROTOCOL',
            'protocol',
            'Request protocol http https',
            'NO_PROTOCOL',
            false
        );

        $Content = THTML::Create()
        -> TagOpen( 'h1' )
        -> AddContent( $Protocol . ' ' . $AHTMLError )
        -> TagClose()
        -> TagOpen( 'h2' )
        -> AddContent( $this -> Code )
        -> TagClose()
        -> TagOpen( 'p' )
        -> AddContent
        (
            str_replace
            (
                PROJECT_ROOT,
                '[ROOT]',
                ObjectToContent( $this ->  GetDetailes(), $this -> Message )
            )
        )
        -> TagClose()
        -> TagOpen( 'code' )
        -> SetAttr( 'style', 'white-space:pre' )
        -> AddContent( json_encode( $this ->  GetDetailes(), JSON_PRETTY_PRINT ))
        -> TagClose()
        -> GetContent();

        if( !$this -> IsCLI() )
        {
            header
            (
                $Protocol . ' ' .
                $AHTMLError . ' ' .
                $this -> GetCode() . ' ' .
                $this -> GetMessage()
            );
        }

        $this -> SetContent( $Content );

        return $this;
    }



    public function GetIDDomain()
    {
        return $this -> IDDomain;
    }



    public function SetIDDomain( $AValue )
    {
        SetIncomeCLI( 'IDDomain', $AValue );
        $this -> Session -> SetDomain( $AValue );
        $this -> IDDomain = $AValue;
        return $this;
    }



    public function GetIDSite()
    {
        return $this -> IDSite;
    }



    public function SetIDSite( $AValue )
    {
        SetIncomeCLI( 'IDSite', $AValue );
        $this -> Session -> SetSite( $AValue );
        $this -> IDSite = $AValue;
        return $this;
    }




    public function GetIDLang()
    {
        return $this -> IDLang;
    }



    public function SetIDLang( $AValue )
    {
        SetIncomeCLI( 'IDLang', $AValue );
        $this -> Session -> SetLanguage( $AValue );
        $this -> IDLang = $AValue;
        return $this;
    }



    public function SetIDSiteTemplate( $AValue )
    {
        $this -> IDSiteTemplate = $AValue;
        return $this;
    }



    public function GetIDSiteTemplate()
    {
        return $this -> IDSiteTemplate;
    }



    public function GetIDUser()
    {
        return $this -> IDUser;
    }



    public function SetIDUser( $AValue )
    {
        SetIncomeCLI( 'IDUser', $AValue );
        $this -> IDUser = $AValue;
        $this -> Session -> SetLogin( $AValue );
        return $this;
    }



    public function GetIDSiteLibrary()
    {
        return $this -> IDSiteLibrary;
    }



    public function SetIDSiteLibrary( $AValue )
    {
        $this -> IDSiteLibrary = $AValue;
        return $this;
    }



    /*
        Return guest for current application
    */
    public function GetIDGuest()
    {
        return $this -> IDGuest;
    }



    /*
        Set id guest for current applicaiton
    */
    public function SetIDGuest( $AValue )
    {
        if ( $AValue != null ) $this -> IDGuest = ( string ) $AValue;
        else $this -> IDGuest = ACCOUNT_GUEST;
        return $this;
    }



    /*
        Impersonate interface
    */

    /*
        Create impersonate URL.
        This URL can be used for calls without authorization.

        IDUser          IDUser or null for current user
        Income          Array of parameters
        Rule            Authorization rules IMPERSONATE_REQUEST || IMPERSONATE_SESSION
        ExpiredTimeout  Expare Timeout TMoment :: DAY or other values
        Protocol        Protocol
        Domain          Domain
    */
    public function GetImpersonateURL( $AParams )
    {
        return
        clValueFromObject( $AParams, 'Protocol', 'https' ) .
        '://' .
        clValueFromObject( $AParams, 'Domain', $this -> GetIDDomain()) .
        '/?impersonate=' . $this -> GetImpersonate( $AParams );
    }



    /*
        Apply impersonate data
    */
    public function Impersonate( $AData )
    {
         $DecriptedData = $this -> DecryptedString( $AData );
         if( empty( $DecriptedData )) $this -> SetCode( 'UnknownData' );
         else
         {
             $ParamsArray = json_decode( $DecriptedData, true );
             if( empty( $ParamsArray )) $this -> SetCode( 'JSONDecodeError' );
             else
             {
                $Params = new TParams();
                $Params -> SetParams( $ParamsArray );

                /* Restore IDUser and Login if it matches the Rule */
                switch( $Params-> GetString( 'Rule' ))
                {
                    case self::IMPERSONATE_REQUEST:
                        /* After this point all actions in this call will work on behalfe of the user. */
                        $this -> SetIDUser( $Params -> GetString( 'IDUser', $this -> GetIDUser() ) );
                    break;
                    case self::IMPERSONATE_SESSION:
                        /* Session will be authorized from new IDUser */
                        $this -> SetIDUser( $Params -> GetString( 'IDUser', $this -> GetIDUser() ) );
                        $this -> GetSession() -> SetLogin( $this -> GetIDUser() );
                    break;
                }

                /* Overrite parameters from Income */
                if( !empty( $Params-> GetParam( 'Income' )))
                {
                    $this -> GetIncome() -> AddFromArray( $Params-> GetParam( 'Income' ));
                }
            }
        }
        return $this;
    }



    public function SendImage( string $AID )
    {
        TWebFile::Create( $this, $AID, $this -> IDSite, $this -> IDLang )
        -> ImageToClient( $_GET )
        -> ResultTo( $this );
        return $this;
    }



    public function SendFile( string $AID, bool $AParentCall = false )
    {
        TWebFile::Create( $this, $AID, $this -> IDSite, $this -> IDLang )
        -> FileToClient()
        -> ResultTo( $this );
        return $this;
    }



    /*
        Override function Include path
    */
    public function ExtIncludePath( $APath, $AType = TController :: CONTROLLER )
    {
        /* Build library name */
        return clLibraryFileAny( $AType . '/' . $APath, $this -> IDSiteLibrary, $this );
    }



    public function ApplyAdditionalIncomeParams()
    {
        return $this;
    }



    /*
        Get template by ID
    */
    public function GetTemplateContent
    (
        $AID,                   /* ID of template */
        $ADefault   = '',       /* Default value if template is not found */
        $AIDSite    = null,     /* ID Site or null for default */
        $AIDLang    = null,     /*  */
        $AStrict    = false
    )
    {
        if( $AIDSite == null ) $AIDSite = $this -> Session -> GetSite();
        if( $AIDLang == null ) $AIDLang = $this -> Session -> GetLanguage();

        return clGetSource
        (
            $AID,
            $ADefault,
            $AIDSite,
            $AIDLang,
            $this -> GetIDLangDefault(),
            $this -> GetIDSiteTemplate(),
            $AStrict
        );
    }



    /*
        Get template by ID and build content from its.
        Using templater.
    */
    public function BuildTemplate
    (
        $AID,                   /* ID of template */
        $ADefault   = '',       /* Default content if template not exists */
        $AOptimize  = true,     /* If true result will be optimized */
        $AIDSite    = null,
        $AIDLang    = null,
        $AStrict    = false
    )
    {
        return $this -> BuildContent
        (
            $this -> GetTemplate
            (
                $AID,
                $ADefault,
                $AIDSite,
                $AIDLang,
                $AStrict
            ),
            $AOptimize
        );
    }



    /**************************************************************************
        Authorization
    */


    /*
        Change loged user
    */
    public function Login
    (
        string $ALogin
    )
    {
         $this -> SetIDUser( $ALogin );
         return $this;
    }



    public function Logout()
    {
        $this -> SetIDUser( $this -> IDGuest );
        return $this;
    }



    /*
        Return login for current session
    */
    public function GetLogin()
    {
        return $this -> Session -> GetLogin( $this -> GetIDGuest() );
    }



    /*
        Set login for current ssession
        SECURE_SYSTEM
    */
    public function CreateLogin()
    {
        return $this -> SetIDUser( clGUID() );
    }



    /*
        Return true if current user is guest
    */
    public function IsGuest()
    {
        return $this -> GetLogin() == $this -> IDGuest;
    }



    /*
        Return role list for current user
    */
    public function GetRoles()
    {
        $Result = [];

        /* Добавляем самого пользователя */
        array_push( $Result, $this -> GetLogin() );

        return $Result;
    }



    public function GetSession()
    {
        return $this -> Session;
    }



    public function GetSitePath( $ALocal = null )
    {
        return clSitePath( $this -> GetIDSite() ) . ( empty( $ALocal ) ? '' : '/' . $ALocal );
    }



    public function GetSiteStoragePath( $ALocal = null )
    {
        return clSiteStoragePath( $this -> GetIDSite() ) . ( empty( $ALocal ) ? '' : '/' . $ALocal );
    }



    public function GetTmpFile( $AFileName )
    {
        return clSiteTmpFile( $this -> IDSite, $AFileName );
    }



    public function GetSiteLogPath( $ALocal = null )
    {
        return clSiteLogPath( $this -> IDSite ) . ( empty( $ALocal ) ? '' : '/' . $ALocal );
    }



    static public function GetServerCLI
    (
        $AKeyServer,            /* Name for key form $_SERVER */
        $AKeyCLI  = null,       /* Name for key from CLI */
        $ACaption = '',         /* Display request label for CLI input */
        $ADefault = '',         /* Default value */
        $ARequest = false       /* Strict request if value not found in $AKeyServer or $AKeyCLI */
    )
    {
        return GetIncomeCLI
        (
            $AKeyCLI ?? $AKeyServer,
            $ACaption,
            array_key_exists( $AKeyServer, $_SERVER ) ? $_SERVER[ $AKeyServer ] : $ADefault,
            $ARequest
        );
    }



    /**************************************************************************
        Crypt
    */

    /*
        Return encripting string
    */
    public function EncryptedString
    (
        string $AValue /* Value for encrypting */
    )
    {
        $InitVector = openssl_random_pseudo_bytes( $this -> Session -> GetSSLLengthVector() );

        $Data = openssl_encrypt
        (
            $AValue, /* Encripted Data */
            $this -> Session -> GetSSLMethod(),
            $this -> Session -> GetSSLKey(),
            OPENSSL_RAW_DATA,
            $InitVector /* Init Vector */
        );

        return base64_encode( $InitVector ) . '__' . base64_encode( $Data );
    }



    /*
        Return decrypted data or null on error
    */
    public function DecryptedString
    (
        string $AData   /* value for decrypting */
    )
    {
        $Result = null;
        $Parts = explode( '__', $AData );
        if ( count( $Parts ) == 2)
        {
            $InitVector = base64_decode( $Parts[ 0 ]);
            $Data       = base64_decode( $Parts[ 1 ]);

            $Result = openssl_decrypt
            (
                $Data,
                $this -> Session -> GetSSLMethod(),
                $this -> Session -> GetSSLKey(),
                OPENSSL_RAW_DATA,
                $InitVector
            );
        }

        return empty( $Result ) ? null : $Result;
    }



    /*
        Return the password hash for storage without password in external servises
    */
    public function GetPasswordHash
    (
        $ALogin,    /* Login */
        $APassword  /* Password for login */
    )
    {
        return md5( $ALogin . '-' . $APassword );
    }



    /*
        Return Cookie by key name
    */
    public function GetCookie
    (
        $AKey,              /* Key name */
        $ADefault = null    /* Default value if key cookie is not exist */
    )
    {
        return clValueFromObject( $_COOKIE, $AKey, $ADefault );
    }




    /*
        Return POST value
    */
    public function GetPostValue
    (
        $APath,             /* Key name or array with keys [ 'Key1', 'Key2', 'Key3', ... ] */
        $ADefault = null    /* Default value */
    )
    {
        $Result = clValueFromObject( $this -> GetIncome() -> GetParams(), $APath, $ADefault );
        switch( gettype( $ADefault ))
        {
            case 'string'   : $Result = (string) $Result; break;
            case 'boolean'  : $Result = $Result === 'true' || $Result === 'on' || $Result === 1; break;
            case 'integer'  : $Result = (int) $Result; break;
            case 'double'   : $Result = (float) $Result; break;
        }
        return $Result;
    }



    public function SetPostValue( $AKey, $AValue )
    {
        $this -> GetIncome() -> SetParam( $AKey, $AValue );
        return $this;
    }



    /*
        Async batch.
        It will call by quartz
    */
    public function Batch
    (
        string $APusaName,
        string $AMethod,
        $AParams = []
    )
    {
        /* Empty */
        $this -> Warning( 'This empty batch in web' );
        return $this;
    }







    /**************************************************************************
        Daemon
    */

    public function DaemonControl( $ACommand )
    {
        /* Daemon name must be define */
        $this -> Name = $this -> GetIncome() -> GetString( 'Name', '' );
        $this -> IfResult( empty( $this -> Name ), 'NameIsEmpty', 'The daemon needs a name --Name' );

        if( $this -> IsOk())
        {
            switch( $ACommand )
            {
                case 'exec':
                    $this -> Execute();
                break;
                case 'start':
                    $this -> Invoke();
                break;
                case 'stop':
                    $this -> Revoke();
                break;
                case 'restart':
                    $this -> Revoke();
                    sleep(1);
                    $this -> Invoke();
                break;
                case 'status':
                    $this -> Status();
                break;
                default:
                    $this -> SetCode( 'UnknownDaemonComamnd' );
                break;
            }
        }

        /* Error */
        if( !$this -> IsOk() )
        {
            $this -> ErrorMessageByCode();
            $this
            -> Warning( 'Daemon error' )
            -> Param( 'Code', $this -> GetCode() )
            -> Param( 'Message', $this -> GetMessage())
            -> Dump( $this -> GetDetailes() );
        }

        return $this;
    }



    /**************************************************************************
        Daemon on posx

    */


    /*
        Return PID file name
    */
    private function GetPIDFile()
    {
        return '/var/run/' . $this -> Name . '.pid';
    }



    /*
        Return true if daemon exists
    */
    private function IsDaemonActive()
    {
        $Result = false;
        $PIDFile = $this -> GetPIDFile();

        if( file_exists( $PIDFile ) && is_file( $PIDFile ))
        {
            $Result = posix_kill
            (
                file_get_contents( $PIDFile ),
                0
            );
        }

        return $Result;
    }



    private function WritePIDFile( $APID )
    {
        file_put_contents
        (
            $this -> GetPIDFile(),
            $APID
        );
        return $this;
    }



    /*
        Delete PID file
    */
    private function DeletePIDFile()
    {
        unlink( $this -> GetPIDFile() );
        return $this;
    }



    /*
        Invoke the daemon
    */
    public function Execute()
    {
        $this
        -> IfResult( $this -> IsDaemonActive(), 'DaemonAlreadyExists', 'Daemon already exists' );

        if( $this -> IsOk() )
        {
            /* Write PID */
            $this -> WritePIDFile( getmypid() );

            /* Run main loop */
            $this -> Job();

            /* Delete PID file after loop */
            $this -> DeletePIDFile();
            $this -> End();
        }
        return $this;
    }



    /*
        Invoke the daemon
    */
    public function Invoke()
    {
        $this
        -> IfResult( $this -> IsDaemonActive(), 'DaemonAlreadyExists', 'Daemon already exists' );

        if( $this -> IsOk() )
        {
            $ChildPID = pcntl_fork();
            if( $ChildPID )
            {
                /* Parent pocess */
            }
            else
            {
                /* Write PID */
                $this -> WritePIDFile( getmypid() );

                /* Switch log to file */
                $this
                -> SetDestination( TLog::FILE )
                -> SetLogPath( $this -> GetSiteLogPath() . '/daemons' )
                -> SetLogFile( $this -> Name )
                -> Begin( 'Daemon log being' );

                /* Child process transforms to main process */
                posix_setsid();

                /* Child process detach from console */
                fclose( STDIN );
                fclose( STDOUT );
                fclose( STDERR );

                /* Run main loop */
                $this -> Job();

                /* Delete PID file after loop */
                $this -> DeletePIDFile();
                $this -> End();

            }
        }
        return $this;
    }



    /*
        Status the daemon
    */
    public function Status()
    {
        if( $this -> IsDaemonActive() )
        {
            $this -> Info( 'Daemon is active' );
        }
        else
        {
            $this -> Info( 'Daemon is not active' );
        }
        return $this;
    }



    /*
        Stop the daemon
    */
    public function Revoke()
    {
        if( $this -> IsDaemonActive() )
        {
            $PIDFile = $this -> GetPIDFile();
            if( file_exists( $PIDFile ) && is_file( $PIDFile ))
            {
                posix_kill
                (
                    file_get_contents( $PIDFile ),
                    SIGTERM
                );
            }
        }
        return $this;
    }



    private function SignalInt()
    {
        $this -> Active = false;
    }



    private function SignalTerm()
    {
        $this -> Active = false;
    }



    /*
        Main job
    */

    /*
        Main daemon loop
    */
    private function Job()
    {
        /* Set signal SIGTERM */
        pcntl_signal( SIGTERM, [ $this, 'SignalTerm' ]);

        /* Set signal SIGHUP */
        pcntl_signal( SIGINT, [ $this, 'SignalInt' ]);

        /* Start main loop */
        $this -> Begin( 'Loop' ) -> LineEnd();

        /* Main job */
        if( method_exists( $this, 'OnStart' ))  $this -> OnStart();

        /* Main job */
        if( $this -> IsOk() && method_exists( $this, 'OnJob' ))
        {
            $this -> Active = true;
            while( $this -> Active )
            {
                $this -> OnJob();

                /* Loop pause with signal interrupt */
                $PauseAccum = 0;
                while
                (
                    $PauseAccum == 0 ||                     /* первый цикл сработает всегда */
                    ! $this -> SkipPause &&                 /* не надо пропускать паузу */
                    $PauseAccum < $this -> MainPause &&     /* пауза меньше основной */
                    $this -> Active                         /* сервис еще активен */
                )
                {
                    pcntl_signal_dispatch();
                    $Pause = 10 * TMoment::MILISECOND;
                    $PauseAccum += $Pause;
                    usleep( $Pause );
                }
            }
        }

        /* Call After action */
        if ( method_exists( $this, 'OnStop' )) $this -> OnStop();

        $this -> End();
        return $this;
    }



    protected function OnStart()
    {
        /* Get Pusa class name */
        $PusaClass  = $this -> GetIncome() -> GetString( 'Pusa' );
        $this -> DaemonPusaMethod = $this -> GetIncome() -> GetString( 'Method' );

        if( empty( $PusaClass ))
        {
            /* Pusa controller name not found */
            $this -> SetResult( 'UnknownPusaClass' );
        }
        else
        {
            /* Create pusa controller */
            $this -> DaemonPusa = TWebPusa::Create( $this, $PusaClass ) -> SetParams( $this -> GetIncome() -> GetParams() );

            /* Enable debug for client */
            if( $this -> GetEnabled())
            {
                $this -> DaemonPusa -> Debug( TPusaCore::DEBUG_ON );
            }

            $this
            -> IfResult( empty( $this -> DaemonPusaMethod ), 'DaemonPusaMethodIsEmpty' )
            -> IfResult( ! method_exists( $this -> DaemonPusa, $this -> DaemonPusaMethod), 'DaemonPusaMethodDoesNotExists' );

            if( $this -> IsOk() )
            {
                $this -> DaemonPusa -> OnStart();
            }
        }

        return $this;
    }



    /*
        On job callback
    */
    private function OnJob()
    {
        /* Call pusa */
        $this -> DaemonPusa
        -> Run( $this -> DaemonPusaMethod )
        -> ResultTo( $this );

        /* Skip pause set as false. Job can be set skip pause in true */
        $this -> SkipPause
        = property_exists( $this -> DaemonPusa, 'SkipPause' )
        ? $this -> DaemonPusa -> SkipPause
        : false;

        $this -> Active
        = property_exists( $this -> DaemonPusa, 'Active' )
        ? $this -> DaemonPusa -> Active
        : $this -> Active;

        $this -> MainPause
        = property_exists( $this -> DaemonPusa, 'MainPause' )
        ? $this -> DaemonPusa -> MainPause
        : $this -> MainPause;

        return $this;
    }



    /*
        Return Pusa's libraries path
    */
    public function GetPusaPath
    (
        string $ALocal = null
    )
    {
        return clLibraryPath( $this -> GetIDSite() ) . '/pusa' . ( empty( $ALocal ) ? '' : '/' . $ALocal );
    }



    static public function AnyToLowerCase( $Income )
    {
        $Result = [];
        foreach( $Income as $Name => $Value )
        {
            $Result[ clAnyToLower( $Name )] = $Value;
        }
        return $Result;
    }

}

