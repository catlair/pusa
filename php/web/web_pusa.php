<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/


namespace catlair;

require_once ROOT . '/core/pusa.php';
require_once 'web_pusa_directives.php';



class TWebPusa extends TPusa
{
    public  $Web = null;


    /*
        Constructor
    */
    public function __construct( $AWeb, $AClass )
    {
        parent::__construct( $AWeb, $AClass );
        $this -> Web = $AWeb;
    }



    /*
        Overriding Create funciton for Web
    */
    static public function Create( $AWeb, $AClass )
    {
        $Pusa = new TWebPusa( $AWeb, $AClass );
        return $Pusa -> Prepare() -> Mutate( $AClass );
    }



    /*
        Return TWeb
    */
    public function GetWeb()
    {
        return $this -> Web;
    }



    /*
        Overriding PrepareLibrary
    */
    protected function PrepareLibrary( $AClass )
    {
        $PusaLib = '/pusa/' . $AClass . '.php';

        /* Try to find the custom library */
        $PathName = clLibraryPath( $this -> Web -> GetIDSite() );
        $FileName = $PathName . $PusaLib;

        if( file_exists( $FileName ))
        {
            $this -> Library = $FileName;
        }
        else
        {
            /* Try to find default library */
            $PathNameDefault = clLibraryPath();
            $FileNameDefault = $PathNameDefault . $PusaLib;

            if( file_exists( $FileNameDefault ))
            {
                $this -> Library = $FileNameDefault;
            }
            else
            {
                $this -> Library = null;
                /* Library path not found */
                $this -> Log -> Warning( 'Library not found' );
                $this -> SetResult
                (
                    'LibraryNotFound', null,
                    [
                        'LibraryCustom' => $FileName,
                        'LibraryDefault' => $FileNameDefault,
                        'Class' => $AClass
                    ]
                );
            }
        }

        return $this;
    }



    /*
        Return Pusa answer structure in JSON format
    */
    public function GetAnswer
    (
        $AResult = null
    )
    {
        return $this -> IsContent()
        ?
        /* Buld the content */
        TWebPusaDirectives::Create( $this -> GetLog() )
        -> SetContent( $this -> GetContent() )
        -> Run( $this -> Commands )
        -> AppendDirectives( $this -> Commands )
        -> ResultTo( $this )
        -> GetContent()
        :
        /* Return pusa native JSON answer */
        parent::GetAnswer( $AResult );
    }



    public function GetTemplate
    (
        string $AID /* Template ID */
    )
    {
        return $this -> Web -> GetTemplate( $AID );
    }



    public function BuildTemplate
    (
        string  $AID,               /* Template ID */
        bool    $AOptimize = true   /* Default content */
    )
    {
        return $this -> Web -> BuildContent
        (
            $this -> GetTemplate( $AID ),
            $AOptimize
        );
    }



    /*
        Get content from template and put them to focus element
    */
    public function DOMTemplate
    (
        string $AID,                   /* Template ID */
        bool $AReplace      = true,
        bool $AOptimize     = true,    /* Template will be optimized */
        bool $ACheckRight   = true     /* Check right for template */
    )
    {
        if( $this -> IsOk() )
        {
            $Template = $this -> Web -> GetTemplate( $AID );
            $this -> ResultFrom( $this -> Web );
            $this -> DOMContent
            (
                $this -> Web -> BuildContent( $Template, $AOptimize, $AReplace )
            );
            $this -> Web -> ResultTo( $this );

        }
        return $this;
    }



    /*
        Do not use it
        Alias for DOMTemplate
    */
    public function PutContent( string $AID, bool $AOptimize = true, bool $ACheckRight= true )
    {
        return $this -> DOMTemplate( $AID, true, $AOptimize, $ACheckRight );
    }




    /*
        Get template by identifer and set it as CSS
    */
    public function DOMCSSAddFromTemplate
    (
        string $AID,
        bool $AOptimize     = true,    /* Template will be optimized */
        bool $ACheckRight   = true
    )
    {
        return $this -> DOMCSSAdd
        (
            $AID,
            $this -> Web -> BuildContent( $this -> Web -> GetTemplate( $AID ),  $AOptimize )
        );
    }



    /*
        Method on web after build event
    */
    public function OnAfterWebBuild( $AWeb )
    {
        if( !$AWeb -> IsOk() )
        {
            $Message = $AWeb -> GetMessage( [ PROJECT_ROOT => 'ROOT' ], '<span class="Value">', '</span>' );
            $this -> Warning
            (
                empty( $Message )
                ? $AWeb -> GetCode() . ' ' . str_replace( PROJECT_ROOT, '[ROOT]', json_encode( $AWeb -> GetDetailes(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES ))
                : $Message
            );
        }
        return $this
        -> DumpCommands();
    }



    /**************************************************************************
        Messages
    */

    public function Message
    (
        $AMessage,
        $AClass
    )
    {
        /*  Autohide messeges timeout */
        $WaitTimeoutMilliseconds = 10000;

        /* Prepare and show messages fo frontend */
        return $this
        -> FocusBody()
        -> FocusChildren( 'Log', TPusaCore::ID )
        -> IfFocusIsEmpty
        (
            $this -> Clone()
            -> FocusBody()
            /* Create log form */
            -> DOMCreate( 'div', TPusaCore::LAST )
            -> DOMID( 'Log' )
            -> DOMClass( 'Log' )
            -> EventFront( $this -> Clone() -> DOMHide() )
        )
        -> DOMShow()
        /* Create new message */
        -> DOMCreate()
        -> DOMClassAdd( 'LogLine' )
        -> DOMClassAdd( $AClass )
        -> DOMContent( $AMessage )
        -> DOMTimerFront( $this -> Clone() -> DOMDelete(), $WaitTimeoutMilliseconds )
        ;
    }



    /*
        Create new warning message at browser
    */
    public function Warning( $AMessage )
    {
        return $this
        -> Message( $AMessage, 'LogWar' );
    }



    /*
        Create new information message at browser
    */
    public function Info( $AMessage )
    {
        return $this
        -> Message( $AMessage, 'LogInf' );
    }



    /*
        Create new error message at browser
    */
    public function Error( $AMessage )
    {
        return $this
        -> Message( $AMessage, 'LogErr' );
    }




    public function InfoTemplate( $AIDTemplate )
    {
        return $this
        -> Info( $this -> GetTemplate( $AIDTemplate ));
    }



    /**************************************************************************
        Pathes
    */


    /*
        Muzzle on Web
    */
    public function GetSitePath
    (
        $ALocal = null
    )
    {
        return $this -> Web -> GetSitePath( $ALocal );
    }



    /*
        Muzzle on Web
    */
    public function GetSiteStoragePath
    (
        $ALocal = null
    )
    {
        return $this -> Web -> GetSiteStoragePath( $ALocal );
    }



    /**************************************************************************
        Works with Serialized values from POST
    */



    /*
        Return serealazed post parameters as named array
    */
    public function GetValues()
    {
        return $this -> Web -> GetIncome() -> GetParams();
    }



    /*
        Set value by Key
    */
    public function SetValue
    (
        $AKey,
        $AValue = null
    )
    {
        $this -> Web -> GetIncome() -> SetParam( $AKey, $AValue );
        return $this;
    }



    /*
        Return POST value
    */
    public function GetValue
    (
        $APath,             /* Key name or array with keys [ 'Key1', 'Key2', 'Key3', ... ] */
        $ADefault = null    /* Default value */
    )
    {
        $Result = clValueFromObject( $this -> GetValues(), $APath, $ADefault );
        switch( gettype( $ADefault ))
        {
            case 'string'   : $Result = (string) $Result; break;
            case 'boolean'  : $Result = $Result === 'true' || $Result === 'on' || $Result === 1; break;
            case 'integer'  : $Result = (int) $Result; break;
            case 'double'   : $Result = (float) $Result; break;
        }
        return $Result;
    }



    /*
        Return string value from POST by Key name or array Path of Keys
    */
    public function GetString
    (
        $APath,
        string  $ADefault = ''
    )
    {
        return $this -> GetValue( $APath, $ADefault );
    }



    /*
        Return integer value from POST by Key name or array Path of Keys
    */
    public function GetInteger
    (
        $APath,
        int     $ADefault = 0
    )
    {
        return $this -> GetValue( $APath, $ADefault );
    }



    public function GetFloat
    (
        $APath,
        float   $ADefault = 0.0
    )
    {
        return $this -> GetValue( $APath, $ADefault );
    }



    public function GetBoolean
    (
        $APath,
        bool    $ADefault = false
    )
    {
        return $this -> GetValue( $APath, $ADefault );
    }



    /**************************************************************************
        Data section
        Following methods work with attributes data-*
    */

    /*
        Return the untyped value from data- attribute for the event element.
    */
    public function GetData
    (
        string $AName,
        $ADefault = ''
    )
    {
        return $this -> GetValue([ 'Attributes', 'data-' . strtolower( $AName ) ], $ADefault );
    }



    /*
        Return the string value from data- attribute for the event element.
    */
    public function GetDataString
    (
        string  $AName,
        string  $ADefault = ''
    )
    {
        return $this -> GetString( [ 'Attributes', 'data-' . strtolower( $AName ) ], $ADefault );
    }



    /*
        Return the integer value from data- attribute for the event element.
    */
    public function GetDataInteger
    (
        string  $AName,
        int     $ADefault = 0
    )
    {
        return $this -> GetInteger( [ 'Attributes', 'data-' . strtolower( $AName ) ], $ADefault );
    }



    /*
        Return the boolean value from data- attribute for the event element.
    */
    public function GetDataBoolean
    (
        string  $AName,
        bool    $ADefault = false
    )
    {
        return $this -> GetBoolean( [ 'Attributes', 'data-' . strtolower( $AName ) ], $ADefault );
    }



    /*
        Set data-* value for focus elements
    */
    public function SetData
    (
        string  $AName,
        $AValue = null
    )
    {
        return $this
        -> DOMAttr([ 'data-' . strtolower( $AName ) => $AValue ]);
    }




    /*
         Set URL parameters by name
    */
    public function SetURLParam
    (
        string $AName,  /* Name of parameter */
        $AValue = null  /* Value for paremeter settings */
    )
    {
        switch( gettype( $AValue ))
        {
            case 'NULL':
            case 'string':   $this -> GetURL() -> SetString( $AName, $AValue ); break;
            case 'boolean':  $this -> GetURL() -> SetBoolean( $AName, $AValue ); break;
            case 'integer':  $this -> GetURL() -> SetInteger( $AName, $AValue );  break;
            case 'double':   $this -> GetURL() -> SetFloat( $AName, $AValue );  break;
        }
        return $this;
    }



    /*
         Set URL parameters by name
    */
    public function GetURLParam
    (
        string $AName,      /* Name of parameter */
        $ADefault = null    /* Value for paremeter settings */
    )
    {
        switch( gettype( $ADefault ))
        {
            default:
            case 'NULL':     $Result = $this -> GetURL() -> GetString( $AName, '' ); break;
            case 'string':   $Result = $this -> GetURL() -> GetString( $AName, $ADefault ); break;
            case 'boolean':  $Result = $this -> GetURL() -> GetBoolean( $AName, $ADefault ); break;
            case 'integer':  $Result = $this -> GetURL() -> GetInteger( $AName, $ADefault );  break;
            case 'double':   $Result = $this -> GetURL() -> GetFloat( $AName, $ADefault );  break;
        }
        return $Result;
    }



    /**************************************************************************
        Other
    */


    /*
        The muzle for COOKIE for returning value by key name
    */
    public function GetCookie
    (
        $AKey,              /* Key name */
        $ADefault = null    /* Default value */
    )
    {
        return $this -> Web -> GetCookie( $AKey, $ADefault );
    }



    public function GetClass()
    {
        $Items = explode( '\\', get_class( $this ) );
        return array_pop( $Items );
    }



    /*
        Build markdown document from Template
    */
    protected function DOMMarkdown
    (
        string $ATopic  = '', /* Template identify */
        string $AClass  = null,
        string $AMethod = null
    )
    {
        return $this
        /* FocusBodymples content */
        -> DOMContent
        (
            TMarkdown::Create()
            /* Set markdown calbacks*/
            -> SetOnLink
            (
                function( $AHTML, $ALink, $AContent )
                {
                    if( strpos( $ALink, '#' ) === 0 )
                    {
                        /* Anchor */
                        $AHTML
                        -> TagOpen( 'a' )
                        -> SetAttr( 'href', $ALink )
                        -> AddContent( empty( $AContent ) ? $ALink : $AContent )
                        -> TagClose();
                    }
                    elseif( strpos( $ALink, 'http' ) === 0 )
                    {
                        /* External link */
                        $AHTML
                        -> TagOpen( 'a' )
                        -> SetAttr( 'href', $ALink )
                        -> SetAttr( 'target', '_blank' )
                        -> AddContent( empty( $AContent ) ? $ALink : $AContent )
                        -> TagClose();
                    }
                    else
                    {
                        /* Local link */
                        $AHTML
                        -> TagOpen( 'a' )
                        -> SetAttr( 'class', 'Link' )
                        -> SetAttr( 'data-link', $ALink )
                        -> AddContent( empty( $AContent ) ? $ALink : $AContent )
                        -> TagClose();
                    }
                }
            )
            -> SetOnImage
            (
                function( $AHTML, $ALink, $AContent )
                {
                    $AHTML
                    -> TagOpen( 'img' )
                    -> SetAttr( 'class', 'Image' )
                    -> SetAttr( 'src', $ALink )
                    -> TagClose()
                    -> TagOpen( 'div' )
                    -> AddContent( $AContent )
                    -> TagClose();
                }
            )
            -> ToHTML( $this -> BuildTemplate( $ATopic, '', false ))
            -> GetContent()
        )
        -> FocusChildren( 'Link' )
        -> Event
        (
            $AClass,
            $AMethod,
            $this -> Clone() -> ReadAttribute( 'ATopic', 'data-link' )
        );
    }



    protected function FieldValidate
    (
        string  $AFieldClass,
        bool    $AValidate      = true,
        string  $AError         = 'Field',
        string  $AInvalidClass  = 'InvalidField'
    )
    {
        $this
        -> FocusPush()
        -> FocusChildren( $AFieldClass, TPusaCore::NAME );

        if( ! $AValidate )
        {
            $this -> DOMClassAdd( $AInvalidClass );
            $this -> SetCode( $AError );
        }
        else
        {
            $this -> DOMClassRemove( $AInvalidClass );
        }

        $this
        -> FocusPop();

        return $this;
    }



    /*
        Refill variable
    */
    protected function Refill
    (
        &$AValue,           /* Pointer to variable */
        $AIncomeName,       /* Income parameter name */
        $ADefault = null    /* Default value if income parameter not exists */
    )
    {
        if( $AValue === null )
        {
            $AValue = $this -> GetValue( $AIncomeName,   $ADefault );
        }

        return $this;
    }
}
