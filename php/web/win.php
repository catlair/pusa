<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/


namespace catlair;

/*
    Диалоговое окно
    WIN_BACK
    WIN_DIALOG
*/


require_once ROOT."/web/win_const.php";


trait TWin
{
    /*
        Функционал оконного интерфейса.
            Попап окна
            Окна подтверждений
            Диалоговые окна
            Окна форм

        Использовать с классами, наследуемыми от TPusa.

        Фокус должен стоять на окне и после исполнения оставаться на окне
    */



    /*
        Создание простого окна. Не имеет бордеров.
    */
    public function WinSimple( string $AID = null )
    {
        $AID = empty( $AID ) ? clGUID() : $AID;
        return $this
        -> DOMCreate( 'div', self::LAST )
        -> DOMClass( TWinConst::WIN_SIMPLE )
        -> DOMID( $AID );
    }



    /*
        Окно подложка для Popup интерфейсов.
        Занимает весь экран.
        При клике удаляется.
    */
    public function WinBack()
    {
        return $this
        -> FocusBody()                                /* Переходим на Body элемент */
        -> DOMCreate( 'div', self::LAST, clGUID())  /* Создаем новый контейнер */
        -> DOMClass( TWinConst::WIN_BACK )          /* Устанавливаем класс окна подложки */
        -> Event( 'Win', 'Delete' );             /* Добавляем действие на клик - удалить себя */
    }



    /*
        Основа выпадающего окна
    */
    public function WinPopup
    (
        int     $AX = 0,
        int     $AY = 0,
        string  $AID = null      /* Идентификатор окна */
    )
    {
        return $this
        -> WinBack()                            /* Создаем окно подложку */
        -> WinSimple( $AID )                    /* Создаем новое простое окно */
        -> DOMClassAdd( TWinConst::WIN_POPUP )  /* Добавляем клас выпадающего окна */
        -> WinCursor( $AX, $AY )                /* Позиционируем окошко на курсор */
        -> DOMEventTrap();                      /* Ловушка для onclick что бы клик не передавался для WIN_BACK */
    }



    /*
        Close popup window.
        Method can be called from any inner
    */
    public function WinPopupClose()
    {
        return $this
        -> FocusParents( TWinConst::WIN_BACK, TPusaCore::CLASS_NAME )
        -> DOMDelete();
    }



    /*
        Confirmation request
        Show only actions for select
    */
    public function WinConfirm
    (
        $AActions
        /*
            Class           - class for call on click
            Method          - method for call on click
            Label           - label for action button
            IDLabel         - id label for action button
            ActionClass     - CSS className for action button element
        */
    )
    {
        $this
        -> WinPopup()
        -> DOMClassAdd( TWinConst::WIN_CONFIRM )
        -> WinCursor();

        foreach( $AActions as $Action )
        {
            $this
            -> DOMCreate( 'button' )
            -> DOMClassAdd( TWinConst::WIN_ACTION_BTN )
            -> DOMClassAdd( clValueFromObject( $Action, 'ActionClass' ))
            -> DOMContent
            (
                array_key_exists( 'IDLabel', $Action )
                ? $this -> GetBuilder() -> GetTemplate( $Action[ 'IDLabel' ] )
                : $Action[ 'Label' ]
            )
            -> Event
            (
                clValueFromObject( $Action, 'Class' ),
                clValueFromObject( $Action, 'Method' ),
                null,
                TPusa::CLICK,
                0,
                true
            )
            -> FocusParent()
            ;
        }

        return $this;
    }



    public function WinForm( string $AID )
    {
        return $this
        -> FocusBody()
        -> DOMCreate( 'div', self::LAST, 'Dock' )
        -> DOMClassAdd( TWinConst::WIN_DOCK )
        -> DOMClassAdd( TWinConst::WIN_POPUP )
        -> DOMCreate( 'div', self::LAST, $AID . '_' . TWinConst.WIN_ANCHOR )
        -> DOMClassAdd( TWinConst::WIN_ANCHOR )
        -> DOMContent( $AID )

        -> FocusBody()
        -> WinSimple( $AID )
        /* Устанавливаем класс для диалогового окна*/
        -> DOMClassAdd( TWinConst::WIN_FORM )
        /* Описание титла */
        -> DOMCreate( 'div', self::LAST, $AID . '_' . TWinConst::WIN_TITLE ) -> DOMClass( TWinConst::WIN_TITLE )
        -> DOMCreate( 'div', self::LAST, $AID . '_' . TWinConst::WIN_LABEL ) -> DOMClass( TWinConst::WIN_LABEL )

        -> FocusParent()
        -> DOMCreate( 'button', self::LAST, $AID . '_' . TWinConst::WIN_BTN_MAXIMIZE )
        -> DOMClassAdd( TWinConst::WIN_BTN_MAXIMIZE )
        -> DOMClassAdd( TWinConst::WIN_BTN )
        -> Event( 'Win', 'DialogBtnMax' )

        -> FocusParent()
        -> DOMCreate( 'button', self::LAST, $AID . '_' . TWinConst::WIN_BTN_MINIMIZE )
        -> DOMClassAdd( TWinConst::WIN_BTN_MINIMIZE )
        -> DOMClassAdd( TWinConst::WIN_BTN )
        -> Event( 'Win', 'DialogBtnMin' )

        -> FocusParent()
        -> DOMCreate( 'button', self::LAST, $AID . '_' . TWinConst::WIN_BTN_CLOSE )
        -> DOMClassAdd( TWinConst::WIN_BTN_CLOSE )
        -> DOMClassAdd( TWinConst::WIN_BTN )
        -> Event( 'Win', 'FormBtnClose' )

        /* Вернулись на окно */
        -> FocusParent()
        -> FocusParent()

        /* Создали клиентскую часть окна */
        -> DOMCreate( 'div', self::LAST, $AID . '_' . TWinConst::WIN_CLIENT )
        -> DOMClass( TWinConst::WIN_CLIENT )
        -> FocusParent()
        -> WinMax();
    }



    public function WinDialog( string $AID = null )
    {
        return $this
        -> WinPopup( $AID )
        /* Устанавливаем класс для диалогового окна*/
        -> DOMClassAdd( TWinConst::WIN_DIALOG )
        /* Описание титла */
        -> DOMCreate() -> DOMClass( TWinConst::WIN_TITLE )
        -> DOMCreate() -> DOMClass( TWinConst::WIN_LABEL )

        -> FocusParent()
        -> DOMCreate( 'button' ) -> DOMClassAdd( TWinConst::WIN_BTN_MAXIMIZE ) -> DOMClassAdd( TWinConst::WIN_BTN )
        -> Event( 'Win', 'DialogBtnMax' )

        -> FocusParent()
        -> DOMCreate( 'button' ) -> DOMClassAdd( TWinConst::WIN_BTN_MINIMIZE ) -> DOMClassAdd( TWinConst::WIN_BTN )
        -> Event( 'Win', 'DialogBtnMin' )

        -> FocusParent()
        -> DOMCreate( 'button' ) -> DOMClassAdd( TWinConst::WIN_BTN_CLOSE ) -> DOMClassAdd( TWinConst::WIN_BTN )
        -> Event( 'Win', 'DialogBtnClose' )

        /* Вернулись на окно */
        -> FocusParent()
        -> FocusParent()

        /* Создали клиентскую часть окна */
        -> DOMCreate()
        -> DOMClass( TWinConst::WIN_CLIENT )
        -> FocusParent()

        /* Панель кнопок */
        -> DOMCreate() -> DOMClass( TWinConst::WIN_ACTIONS )
        /* Снова вренулись на окно что бы на нем остался фокус */
        -> FocusParent()

        -> WinMin();
    }



    /*
        Закрывает окно и удаляет его из контекста
        Фокус должен стоять на окне
    */
    public function WinClose()
    {
        $this
        -> FocusParents( TWinConst::WIN_BACK, TPusa::CLASS_NAME )
        -> DOMDelete()
        -> FocusBody();
        return $this;
    }



    /*
        Изменение клиентского наименования окна
    */
    public function WinLabel( $AValue )
    {
        $this
        -> FocusChildren( TWinConst::WIN_LABEL, TPusa::CLASS_NAME )
        -> DOMContent( $AValue )
        -> FocusParent()
        -> FocusParent();
        return $this;
    }



    /*
        Изменение клиентского контента окна
    */
    public function WinClient( $AValue )
    {
        $this
        -> FocusChildren( TWinConst::WIN_CLIENT, TPusa::CLASS_NAME )
        -> DOMContent( $AValue )
        -> FocusParent()
        -> FocusParent();
        return $this;
    }



    public function WinActionButtons( $AButtons )
    {
        $this
        -> FocusChildren( TWinConst::WIN_ACTIONS, TPusa::CLASS_NAME )
        -> DOMContent( '' );

        foreach( $AButtons as $Action )
        {
            $this
            -> DOMCreate( 'button' )
            -> DOMClass( TWinConst::WIN_ACTION_BTN )
            -> DOMContent( $Action[ 'Content' ])
            -> Event
            (
                $Action[ 'Class' ],
                $Action[ 'Method' ],
                null,
                TPusa::CLICK,
                0,
                true
            )
            -> FocusParent();
        }

        $this -> FocusParent();

        return $this;
    }



    public function WinMax()
    {
        $this
        -> WinFull()
        -> FocusChildren( TWinConst::WIN_TITLE, TPusa::CLASS_NAME )
        -> FocusChildren( TWinConst::WIN_BTN_MINIMIZE, TPusa::CLASS_NAME )
        -> DOMShow()
        -> FocusParent()
        -> FocusChildren( TWinConst::WIN_BTN_MAXIMIZE, TPusa::CLASS_NAME )
        -> DOMHide()
        -> FocusParent()
        -> FocusParent();
        return $this;
    }



    /*
        Минимизация диалоговго окна
    */
    public function WinMin()
    {
        $this
        -> WinNorm()
        -> FocusChildren( TWinConst::WIN_TITLE,         TPusa::CLASS_NAME )
        -> FocusChildren( TWinConst::WIN_BTN_MINIMIZE,  TPusa::CLASS_NAME )
        -> DOMHide()
        -> FocusParent()
        -> FocusChildren(  TWinConst::WIN_BTN_MAXIMIZE, TPusa::CLASS_NAME )
        -> DOMShow()
        -> FocusParent()
        -> FocusParent();
        return $this;
    }



    /*
        Максимально распахивает окно
    */
    public function WinFull()
    {
        $this
        -> DOMStyle
        ([
            'position'  => 'fixed',
            'left'      => '0',
            'top'       => '0',
            'bottom'    => '0',
            'right'     => '0',
        ]);
        return $this;
    }



    public function WinCursor
    (
        int $AX = 0,
        int $AY = 0
    )
    {
        return $this
        -> DOMStyle
        ([
            'position'  => 'fixed',
            'left'      => $AX . 'px',
            'top'       => $AY . 'px',
            'bottom'    => 'auto',
            'right'     => 'auto',
        ]);
        return $this;
    }



    public function WinNorm()
    {
        $this
        -> DOMStyle
        ([
            'position'  => 'static'
        ]);
        return $this;
    }



    public function ReadPos()
    {
        return $this
        -> ReadEvent( 'AX', 'clientX' )
        -> ReadEvent( 'AY', 'clientY' );
    }
}
