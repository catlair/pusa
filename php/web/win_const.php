<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/


namespace catlair;

/*
    Constants for win.php
    Диалоговое окно
    WIN_BACK
    WIN_DIALOG
*/

class TWinConst
{
    const POPUP_FULL            = 'full';
    const POPUP_EVENT           = 'event';
    const POPUP_CENTER          = 'center';

    const WIN_SIMPLE            = 'WindowSimple';       /* Empty window element */
    const WIN_BACK              = 'WindowBack';         /* Back object for popup */
    const WIN_POPUP             = 'MenuPopup';          /* Popup in back object */

    /*
        WIN_POPUP                       Background element for click and hiding
            WIN_DIALOG WIN_POPUP        Main dialog window
                WIN_TITLE               Window title frame
                    WIN_LABEL           Window label
                    WIN_BTN_MINIMIZE    Button for minimizing window
                    WIN_BTN_MAXIMIZE    Button for maximizing window
                    WIN_BTN_CLOSE       Button for closing window
                WIN_CLIENT              Client area for dialog window
                WIN_ACTIONS             Action dialog buttons ok, yes, no etc.
    */
    const WIN_DIALOG            = 'WindowDialog';       /* Диалоговое окно */

    const WIN_FORM              = 'WindowForm';         /* Интерфейсное окно для форм */

    const WIN_CLIENT_ITEM       = 'WindowDialogItem';
    const WIN_CONTROL           = 'WindowControl';
    const WIN_ACTIONS           = 'WindowActions';      /* Action buttons conteiner for dialog window */
    const WIN_ACTION_BTN        = 'WindowAction';
    const WIN_TITLE             = 'WindowTitle';
    const WIN_LABEL             = 'WindowLabel';
    const WIN_BTN               = 'WindowBtn';
    const WIN_BTN_CLOSE         = 'WindowBtnClose';
    const WIN_BTN_MINIMIZE      = 'WindowBtnMinimize';
    const WIN_BTN_MAXIMIZE      = 'WindowBtnMaximize';
    const WIN_CLIENT            = 'WindowClient';
    const WIN_CONFIRM           = 'WindowConfirm';
    const WIN_DOCK              = 'WindowDock';
    const WIN_ANCHOR            = 'WindowAnchor';

    const BTN_YES               = 'Yes';
    const BTN_NO                = 'No';
    const BTN_OK                = 'Ok';
    const BTN_CANCEL            = 'Cancel';
    const BTN_ABORT             = 'Abort';
    const BTN_CLOSE             = 'Close';
}
