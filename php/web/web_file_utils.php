<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

namespace catlair;

require_once( ROOT . '/core/echo_stream.php' );

/*
    Send file header to client
*/
function clSendFileHeader
(
    string  $ACaption,  /* Name of file */
    string  $AMIME,     /* MIME of file */
    int     $ASize      /* Size of file in bytes */
)
{
    /* Reset buffer PHP for memory overflow prevents */
    if (ob_get_level()) ob_end_clean();

    /* Show dialog of file save in browser */
    header( 'Content-Disposition: attachment; filename="' . $ACaption. '"' );
    header( 'Content-Type: ' . $AMIME );
    header( 'Content-Transfer-Encoding: binary' );
    header( 'Expires: 0' );
    header( 'Cache-Control: must-revalidate' );
    header( 'Pragma: public' );
    header( 'Content-Length: ' . $ASize );

    return true;
}



/*
    Send file to client without checking rights
*/
function clSendFile( $ACaption, $AFileName )
{
    if( !file_exists( $AFileName ))
    {
        /* Write message to buffer */
        print 'FileNotFound ' . $AFileName;
        $Result = false;
    }
    else
    {
        /* Get file size */
        $Size = filesize( $AFileName );

        /* Define the MIME of file */
        $MIME = mime_content_type( $AFileName );
        if( $MIME=='' ) $MIME = 'application/octet-stream';

        /* Crate the echo stream */
        $Destination = new TEchoStream();
        $Source = new TFileStream( $AFileName, 'r' );

        /* Send header to client */
        clSendFileHeader( $ACaption, $MIME, $Size );

        /* Send data to client */
        $Source->WriteTo( $Destination );
        $Result = true;
    }
    return $Result;
}



/*
    GDI write to file
*/
function clImageWrite
(
    &$AImage,           /* GDI content */
    string  $AFile,     /* Filename */
    string  $AExt,      /* Extention */
    string  $AMIME      /* MIME type */
)
{
    $Ext = strtolower( $AExt );
    $MIME = strtolower( $AMIME );

    switch ( true )
    {
    case ( $Ext == 'jpg' || $Ext == 'jpeg' || $MIME == 'image/jpg' ||  $MIME == 'image/jpeg' ):
        $r = imagejpeg( $AImage, $AFile, JPEG_QUALITY );
    break;
    case ( $Ext == 'gif' || $MIME == 'image/gif' ):
        $r = imagegif( $AImage, $AFile );
    break;
    case ( $Ext == 'png' || $MIME == 'image/png' ):
        $r = imagepng( $AImage, $AFile );
    break;
    case ( $Ext == 'bmp' || $MIME == 'image/bmp' ):
        $r = imagewbmp( $AImage, $AFile );
    break;
    default:
         $r = null;
    }

    return $r;
}



/*
    GDI read from file
*/
function &clImageRead
(
    string  $AFile,         /* File name for reading */
    string  $AExtention,    /* File extention */
    string  $AMIME          /* Content MIME */
)
{
    $Ext = strtolower( $AExtention );
    $MIME = strtolower( $AMIME );

    switch ( true )
    {
        case ( $Ext == 'jpg' || $Ext == 'jpeg' || $MIME == 'image/jpg' ||  $MIME == 'image/jpeg' ):
           $r = imagecreatefromjpeg( $AFile );
        break;
        case ( $Ext == 'gif' || $MIME == 'image/gif' ):
            $r = imagecreatefromgif( $AFile );
        break;
        case ( $Ext == 'png' || $MIME == 'image/png' ):
            $r = imagecreatefrompng( $AFile );
        break;
        case ( $Ext == 'bmp' || $MIME == 'image/bmp' ):
            $r = imagecreatefromwbmp( $AFile );
        break;
        default: $r = null;
    }

    return $r;
}



/*
    Построение файлового кэша для файла
*/
function clImageBuildCache
(
    string  $ASource,       /* $ASource - путь источник файла */
    string  $ADestination,  /* $ADestination - путь направления файла */
    string  $AExt,          /* Extention of file */
    string  $AMIME,         /* MIME file */
    &$AParams               /* $AParams - перечень параметров URL обычно $_GET */
)
{
    if ( file_exists( $ASource ))
    {
        list( $wo, $ho ) = getimagesize( $ASource );
        $Source = clImageRead( $ASource, $AExt, $AMIME );

        if( $Source )
        {
            /* Выясняем размеры к которым надо привести изображение */
            $Aspect = $wo / $ho;

            /* Set limits for size of images */
            /* TODO it can be move to config */
            if ( array_key_exists ( 'scalex', $AParams )) $wn = min( $AParams[ 'scalex' ], 1280 );
            else $wn = $wo;

            if ( array_key_exists ( 'scaley', $AParams )) $hn = min( $AParams[ 'scaley' ], 1280 );
            else $hn = $wn / $Aspect;

            $Result = imagecreatetruecolor( $wn, $hn );

            if ( $AExt=='jpeg' || $AExt =='jpg' || $AMIME == 'image/jpg' || $MIME == 'image/jpeg' )
            {
                imageinterlace( $Result, 1 );
            }

            if ( $AExt=='png' || $AMIME == 'image/png' )
            {
                imagealphablending( $Result, false );
                imagesavealpha( $Result, true );
            }

            /* Непосредственно копирование скалирование */
            imagecopyresampled
            (
                $Result,
                $Source,
                0, 0, 0, 0,
                $wn, $hn, $wo, $ho
            );

            if ( array_key_exists ( 'colorize', $AParams ) )
            {
                $val = $AParams[ 'colorize' ];
                if ( $val != '' )
                {
                    $r = hexdec( substr( $val, 0, 2 ));
                    $g = hexdec( substr( $val, 2, 2 ));
                    $b = hexdec( substr( $val, 4, 2 ));
                    $a = ( 255 - hexdec( substr( $val, 6, 2 ))) * 0.5;
                    imagefilter( $Result, IMG_FILTER_COLORIZE, $r, $g, $b, $a );
                }
            }

            /* write cache to file */
            $Path = pathinfo ( $ADestination, PATHINFO_DIRNAME );
            if ( !file_exists( $Path )) mkdir( $Path, FILE_RIGHT, true );

            $OldMask = umask( 0077 );
            clImageWrite( $Result, $ADestination, $AExt, $AMIME );
            umask( $OldMask );
        }
    }
}
