<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

/*
    Build DOM content by Pusa directives
    Result of this module it is returning the HTML content.
*/


namespace catlair;

require_once ROOT . '/core/debug.php';
require_once ROOT . '/core/result.php';



class TWebPusaDirectives extends TResult
{
    const BOM = "\xEF\xBB\xBF";


    private $DOM            = null; /* DOM object */
    private $Focus          = null; /* List of focused elements */
    private $FocusProperty  = null; /* Focused property */
    private $Created        = [];   /* List of created items */



    /*
        Constructor
    */
    public function __construct( TLog $ALog )
    {
        $this -> Log = $ALog;
        /* Create DOM */
        $this -> DOM = new \DOMDocument();
    }



    /*
        Overriding Create funciton for Web
    */
    static public function Create( TLog $ALog )
    {
        $Directives = new self( $ALog );
        return $Directives;
    }



    /************************************************************************************
        Payload
    */


    private function Push( &$Result, $AElements )
    {
        if( !empty( $AElements ))
        {
            if( !is_array( $AElements ))
            {
                $AElements = array( $AElements );
            }
            foreach ($AElements as $Element)
            {
                if( !in_array( $Element, $Result, true ))
                {
                    array_push( $Result, $Element );
                }
            }
        }
        return $this;
    }



    /*
        Select elements for focus
    */
    public function SelectElements
    (
        $APrm   /* Searching argumets */
    )
    {
        $Result = [];
        $Property = null;

        /* Property processing */
        if( array_key_exists( 'Property', $APrm ))
        {
            $Result = $this -> Focus;
            switch ( $APrm[ 'Property' ] )
            {
                case 'classList'    : $Property = 'class'; break;
                case 'style'        : $Property = 'style'; break;
            }
        }

        /**/
        if( array_key_exists( 'Target', $APrm ))
        {
            switch ( $APrm[ 'Target' ] )
            {
                /* Set the self element as focus. It will be the only focus element */
                case 'Self':
                    $this -> SetResult( 'SelfFocusNotSupported' );
                break;

                /* Set DOM document.body as focus. It will be the only focus element */
                case 'Body':
                    $this -> Push( $Result, $this -> GetBody());
                break;

                /* Set window as focus. It will be the only focus element */
                case 'Window':
                    /* WindowObjectFocusNotSupported */;
                break;

                /* Each focus element set its parent as focus */
                case 'Parent':
                    foreach( $this -> Focus as &$Element )
                    {
                        $this -> Push( $Result, $Element -> parentNode );
                    }
                break;

                case 'Parents':
                    foreach( $this -> Focus as &$Element )
                    {
                        $this -> Push
                        (
                            $Result,
                            $this -> Parents( $APrm[ 'Filter' ], $Element )
                        );
                    }
                break;

                case 'ParentFirst':
                    foreach( $this -> Focus as &$Element )
                    {
                        $this -> Push
                        (
                            $Result,
                            $this -> Parents( $APrm[ 'Filter' ], $Element, false )
                        );
                    }
                break;

                case 'Children':
                    foreach( $this -> Focus as &$Element )
                    {
                        $this -> Push
                        (
                            $Result,
                            $this -> Children( $APrm[ 'Filter' ], $Element )
                        );
                    }
                break;

                case 'ChildrenOfThis':
                    foreach( $this -> Focus as &$Element )
                    {
                        $this -> Push
                        (
                            $Result,
                            $this -> Children( $APrm[ 'Filter' ], $Element, 1 )
                        );
                    }
                break;
            }
        }

        $this -> Focus = $Result;
        $this -> FocusProperty = $Property;

        return $this;
    }



    /*
        Select elements for focus
    */
    private function Children
    (
        &$AFilter,
        &$AElement,
        $AMaxLevel      = null,
        &$AResult       = null,
        $ACurrentLevel  = 0
    )
    {
        if( $AResult == null ) $AResult = [];
        foreach( $AElement -> childNodes as $Node )
        {
            $ACurrentLevel++;
            if( $Node -> nodeType === 1 )
            {
                if(( $AMaxLevel == null || $AMaxLevel >= $ACurrentLevel ))
                {
                    $this -> Children( $AFilter, $Node, $AMaxLevel, $AResult, $ACurrentLevel );
                }

                if( $this -> MatchData( $AFilter, $Node ) )
                {
                    array_push( $AResult, $Node );
                }
            }
        }
        return $AResult;
    }



    /*
        Search and return parents element
    */
    private function Parents
    (
        &$AFilter,
        &$AElement,
        $AAllParent = true
    )
    {
        $Result = [];
        while
        (
            !empty( $AElement ) &&
            $AElement -> nodeType === 1 &&
            ( $AAllParent || empty( $Result ) )
        )
        {
            if( $this -> MatchData( $AFilter, $AElement ) )
            {
                array_push( $Result, $AElement );
            }
            $AElement = $AElement -> parentNode;
        }
        return $Result;
    }



    /*
        Return value from element by selector
    */
    private function GetValueElement
    (
        $AElement,          /* DOM element */
        string $ASelector   /* Selector: className, id, tag, name */
    )
    {
        $Result = null;

        if( is_object( $AElement ))
        {
            switch( $ASelector )
            {
                case 'className':
                    $Result = $AElement -> getAttribute( 'class' );
                break;
                case 'name':
                    $Result = $AElement -> getAttribute( 'name' );
                break;
                case 'id':
                    $Result = $AElement -> getAttribute( 'id' );
                break;
                case 'tag':
                    $Result = $AElement -> tagName;
                break;
                default:
                    $this -> SetResult( 'UnknownSelector', null, [ 'Selector' => $ASelector ] );
                break;
            }
        }

        return $Result;
    }



    private function MatchData
    (
        $AFilter,
        $AData
    )
    {
        return $this -> Match
        (
            $AFilter,
            function( $AArg ) use ( $AData )
            {
                $Result = $AArg;
                if( substr( $AArg, 0, 1 ) == '@' )
                {
                    $Key = substr($AArg, 1);
                    $Result = $this -> GetValueElement( $AData, $Key);
                }
                return $Result;
            }
        );
    }



    /*
        Return value from argument
    */
    private function Value( $AArg, $ACallback )
    {
        $Result = $AArg;
        if( is_string( $AArg ))
        {
            $Result = $ACallback ? $ACallback( $AArg ) : $AArg;
        }
        elseif( is_object( $AArg ))
        {
            $Result = Match( $AArg, $ACallback );
        }
        return $Result;
    }



    private function Match
    (
        $AFilter,    /* Conditions in format [ 'Operator', Arguments, ... ] */
        $ACallback   /* callback function function(AArg){return AArg;}*/
    )
    {
        $Result = false;
        if( !empty( $AFilter ))
        {
            $Operator = $AFilter[ 0 ];

            switch( $Operator )
            {
                case '='    :
                case '!='   :
                case '>'    :
                case '<'    :
                case '<='   :
                case '>='   :
                case '+'    :
                case '-'    :
                case '*'    :
                case '/'    :
                case 'in'   :
                    $a1 = $this -> Value( $AFilter[ 1 ], $ACallback );
                    $a2 = $this -> Value( $AFilter[ 2 ], $ACallback );
                break;
            }

            /* Do operation with parameters and operator */
            switch( $Operator )
            {
                case '='    : $Result = $a1 == $a2; break;
                case '!='   : $Result = $a1 != $a2; break;
                case '>'    : $Result = $a1 > $a2; break;
                case '<'    : $Result = $a1 < $a2; break;
                case '<='   : $Result = $a1 <= $a2; break;
                case '>='   : $Result = $a1 >= $a2; break;
                case '+'    : $Result = $a1 + $a2; break;
                case '-'    : $Result = $a1 - $a2; break;
                case '*'    : $Result = $a1 * $a2; break;
                case '/'    : $Result = $a1 / $a2; break;
                case 'in':
                    $Result = array_search( $a1, explode( ' ', $a2 )) !== false;
                break;
                case 'or':
                    $Result = false;
                    for( $i = 1; $i < count( $AFilter ); $i++ )
                    {
                        $Result = $Result || $this -> Value( $AFilter[ $i ] );
                    }
                break;
                case 'and':
                    $Result = true;
                    for( $i = 1; $i < count( $AFilter ); $i++ )
                    {
                        $Result = $Result && $this -> Value( $AFilter[ $i ] );
                    }
                break;
            }
        }
        else
        {
            $Result = true;
        }

        return $Result;
    }



    /*
        Display command warning at console
    */
    private function CommandWarning
    (
        $AMessage,          /* Text message */
        $AIndex     = null, /* Command line index */
        $ACommand   = null  /* List of commands */
    )
    {
        $this -> Log
        -> Warning( 'Directive warning ')
        -> Param( 'Message', $AMessage )
        -> Param( 'Index', $AIndex )
        -> Param( 'Source', json_encode( $ACommand ));
        return $this;
    }



    function FocusIsFull
    (
        $AIndex,    /* Index of command for message */
        $ALine      /* Command line for message */
    )
    {
        $c = count( $this -> Focus );
        if( $c == 0 )
        {
            $this -> CommandWarning( 'Focus is empty', $AIndex, $ALine );
        }
        return $c != 0;
    }



    /*
        Processing the pusa commands
    */
    public function Run
    (
        array $ACommands    /* Pusa commands list */
    )
    {
        $Index              = 0;
        $Focuses            = [];

        if( empty( $this -> Focus ))
        {
            $this -> Focus = $this -> GetBody();
        }

        /* Loop for the list of commands received from Pusa-Back */
        foreach( $ACommands as $Index => $Line )
        {
            $c = count( $Line );

            /* Get new command */
            $Command = $c > 0 ? $Line[ 0 ] : '';

            /* Get arguments for command */
            $Prm = $c > 1 ? $Line[ 1 ] : [];

            switch( $Command )
            {
                default:
                    $this -> CommandWarning( 'Unknown directive', $Index, $Line );
                break;

                /* Store copy of current focus list to buffer */
                case 'FocusPush':
                    array_push
                    (
                        $Focuses,
                        array_slice( $this -> Focus, 0 )
                    );
                break;

                /* Restore focus list from buffer */
                case 'FocusPop':
                    if( count( $Focuses ) > 0 )
                    {
                        $this -> Focus = array_pop( $Focuses );
                    }
                    else
                    {
                        $this -> Focus = [];
                        $this -> CommandWarning
                        (
                            'Focus stack is empty',
                            $Index,
                            $Line
                        );
                    }
                break;

                /* Set property for Focus */
                case 'Set':
                    if
                    (
                        array_key_exists( 'Values', $Prm ) &&
                        $this -> FocusIsFull( $Index, $Line )
                    )
                    {
                        foreach( $this -> Focus as $e )
                        {
                            switch( $this -> FocusProperty )
                            {
                                /* Works with property of elements */
                                case 'style':
                                    /* Works with elements */
                                    $e -> setAttribute
                                    (
                                        'style',
                                        THTML::ArrayToCSS
                                        (
                                            array_merge
                                            (
                                                THTML::CSSToArray( $e -> getAttribute( 'style' ) ),
                                                $Prm[ 'Values' ]
                                            )
                                        )
                                    );
                                break;

                                default:
                                    /* Works with elements */
                                    foreach( $Prm[ 'Values' ] as $Key => $Value )
                                    {
                                        /* Set property to argument list */
                                        switch( $Key )
                                        {
                                            case 'innerHTML':
                                                $this -> SetElementContent( $e, $Value );
                                            break;
                                            case 'value':
                                            case 'class':
                                            case 'name':
                                            case 'id':
                                                $e -> SetAttribute( $Key, $Value );
                                            break;
                                            default:
                                            break;
                                        }
                                    }
                                break;

                            }
                        }
                    }
                break;

                case 'Loop':
                    $Tasks = clValueFromObject( $Prm, 'Tasks', [] );
                    $Commands = clValueFromObject( $Prm, 'Commands', [] );
                    if( !empty( $Tasks ) && !empty( $Commands ) )
                    {
                        foreach( $Tasks as $Record )
                        {
                            /* Execute commands with current event object */
                            $this -> Run( clArrayReplace( $Commands, $Record ));
                        }
                    }
                break;

                /* Select and focus to DOM elements from current focus elements */
                case 'Focus':
                    if( array_key_exists( 'Target', $Prm ))
                    {
                        $this -> SelectElements( $Prm );
                    }

                    /* Move property from Focus objects to Focus objects */
                    if( array_key_exists( 'Property', $Prm ))
                    {
                        $this -> SelectElements( $Prm );
                    }

                    /* Move property from Focus objects to Focus objects */
                    if( array_key_exists( 'Method', $Prm ))
                    {
                        /* Property or method focus skiped */
                    }
                break;

                /*
                    Create DOM elements
                    Elements will be added if does not exists with the passed ID
                */
                case 'Create':
                    if( $this -> FocusIsFull( $Index, $Line) )
                    {
                        /*
                            Create new static identifier and save it in the list of Created identifiers.
                            This identifier need for find element on front-end.
                        */
                        $StaticID = clGUID();
                        array_push(  $this -> Created, $StaticID );

                        /* New elements array */
                        $NewElements = [];

                        /* Loop for focused elements */
                        foreach( $this -> Focus as $e )
                        {
                            /* Create new element. */
                            $New = $this -> CreateElement( clValueFromObject( $Prm, 'TagName', 'div' ));
                            $New -> setAttribute( 'data-static-id', $StaticID );
                            /* Store the new element in the future focus list */
                            array_push( $NewElements, $New );
                            switch( clValueFromObject( $Prm, 'Position' ))
                            {
                               default:
                               case 'Last'     : $this  -> Append( $e, $New );  break;
                               case 'First'    : $this  -> Prepend( $e, $New ); break;
                               case 'Before'   : $this  -> Before( $e, $New );  break;
                               case 'After'    : $this  -> After( $e, $New );   break;
                            }
                        }
                        /* Set focus to the selected elements */
                        $this -> Focus = $NewElements;
                    }
                break;

                /* Conditions */
                case 'If':
                    $Condition = null;

                    switch( clValueFromObject( $Prm, 'Condition' ))
                    {
                        case 'FocusIsEmpty':
                            $Condition = count( $this -> Focus ) == 0;
                        break;
                        default:
                            $this -> CommandWarning
                            (
                                'Unknown condition',
                                $Index,
                                $Line
                            );
                        break;
                    }

                    if( $Condition === true && array_key_exists( 'True', $Prm ))
                    {
                        $this -> Run( clValueFromObject( $Prm, 'True', [] ));
                    }

                    if( $Condition === false && array_key_exists( 'False', $Prm ))
                    {
                        $this -> Run( clValueFromObject( $Prm, 'False', [] ));
                    }
                break;

                case 'Call':
                    $Arg = clValueFromObject( $Prm, 'Arguments' );
                    foreach( $this -> Focus as $e )
                    {
                        switch ( clValueFromObject( $Prm, 'Method' ))
                        {
                            case 'add':
                                if( !empty( $this -> FocusProperty ))
                                {
                                    $Attribute = $e -> getAttribute( $this -> FocusProperty );
                                    $ClassList = explode( ' ', $Attribute );
                                    $Class = clValueFromObject( $Arg, '0');
                                    if( !in_array( $Class, $ClassList ))
                                    {
                                         array_push( $ClassList, $Class );
                                    }
                                    $e -> SetAttribute( $this -> FocusProperty, implode( ' ', $ClassList ) );
                                }
                            break;
                            case 'setAttribute':
                                $e -> setAttribute
                                (
                                    clValueFromObject( $Arg, '0' ),
                                    clValueFromObject( $Arg, '1' )
                                );
                            break;
                            default:
                                /* Skeep call method */
                            break;
                        }
                    }
                break;

                case 'Debug':
                    $this -> Log
                    -> Trace( 'Debug level' )
                    -> Param( 'Value', clValueFromObject( $Prm, 'Level' ));
                break;

                /* Skip */
                case 'Replace':
                case 'Arg':
                case 'JS':
                case 'Event':
                case 'Timer':
                case 'PileFrom':
                case 'PileTo':
                case 'PileEqual':
                case 'PileReplace':
                case 'CSSAttr':
                    /* Skip command */
                break;
            }

            $Index++;
        }

        return $this;
    }



    public function AppendDirectives
    (
        array $ACommands    /* Pusa commands list */
    )
    {
        $this -> Log -> Begin( 'Append directives to document' );
        /* Remove innerHTML from values */
        foreach( $ACommands as &$Command )
        {
            if( $Command[ 0 ] == 'Set' )
            {
                if
                (
                    array_key_exists( 1, $Command ) &&
                    array_key_exists( 'Values', $Command[ 1 ] ) &&
                    array_key_exists( 'innerHTML', $Command[ 1 ][ 'Values' ] )
                )
                {
                    unset( $Command[ 1 ][ 'Values' ][ 'innerHTML' ]);
                }
            }
        }

        /* Add commands */
        $Directives = $this -> CreateElement( 'div' );
        $Directives -> setAttribute( 'id', 'PusaDirectives' );
        $Directives -> setAttribute( 'style', 'display:none' );
        $Directives -> textContent = encodeURIComponent( json_encode( $ACommands ));
        $this -> Append( $this -> GetBody(), $Directives );

        /* Add static created list */
        $StaticCreated = $this -> CreateElement( 'div' );
        $StaticCreated -> setAttribute( 'id', 'PusaStaticCreated' );
        $StaticCreated -> setAttribute( 'style', 'display:none' );
        $StaticCreated -> nodeValue = implode( ' ', $this -> Created );
        $this -> Append( $this -> GetBody(), $StaticCreated );

        $this -> Log -> End();
        return $this;
    }


    /************************************************************************************
        DOM emplementation
        TODO: move into the external module
    */

    public function SetContent( $AContent )
    {
        $this -> DOM -> loadHTML( $AContent );
        return $this;
    }



    public function GetContent()
    {
        return $this -> DOM -> saveHTML();
    }



    public function GetRootElement()
    {
        return $this -> DOM -> documentElement;
    }



    public function SetElementContent
    (
        $AElement,
        $AValue
    )
    {
        /* Remove content */
        $Count = $AElement -> childNodes-> length;
        for( $i = 0; $i < $Count; $i++)
        {
           $AElement -> removeChild( $AElement -> childNodes -> item( 0 ));
        }

        /* Append content */
        if( !empty( $AValue ))
        {
            $tpl = new \DOMDocument();
            $tpl -> loadHTML( self::BOM . '<html><body>' . $AValue . '</body></html>' );
            $Doc = $tpl -> documentElement;
            $Node = $this -> DOM -> importNode( $Doc, true );
            $AElement -> appendChild( $Node );
        }

        return $this;
    }



    public function GetBody()
    {
        $XPath = new \DOMXpath( $this -> DOM );
        $Elements = $XPath -> query('//' . "*[contains(local-name(), 'body')]" );
        return count( $Elements ) == 0 ? null : $Elements[ 0 ];
    }



    public function CreateElement( $ATagName )
    {
        return $this -> DOM -> createElement( $ATagName );
    }



    public function Append( $AElement, $ANew )
    {
        $AElement -> appendChild( $ANew );
        return $this;
    }



    public function Prepend( $AElement, $ANew )
    {
        if( $AElement -> firstChild )
        {
            $AElement -> insertBefore( $ANew, $AElement -> fisrtChild );
        }
        else
        {
            $this -> Append( $AElement, $ANew );
        }
        return $this;
    }



    public function Before( $AElement, $ANew )
    {
        $AElement -> parentNode -> insertBefore( $ANew, $AElement );
        return $this;
    }



    public function After( $AElement, $ANew )
    {
        if( $AElement -> nextSibling )
        {
            $this -> Before( $AElement -> nextSibling, $ANew );
        }
        else
        {
            $this -> Append( $AElement -> parentNode, $ANew );
        }
        return $this;
    }

}
