<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.


    still@itserv.ru 16.03.2019

    Файлы
    Catlair PHP
    Работа с дескриптами как с файлами.
*/

namespace catlair;

require_once( ROOT . '/web/web_file_utils.php' );
require_once( ROOT . '/core/file_stream.php' );
require_once( ROOT . '/core/echo_stream.php' );
require_once( ROOT . '/core/result.php' );

class TWebFile extends TResult
{
    private $Log        = null;
    private $Web        = null;

    private $ID         = '';
    private $IDSite     = '';
    private $IDLang     = '';

    public $MIME            = '';
    public $Extention       = '';
    public $Size            = 0;

    private $FileNameWrite  = null;
    private $FileNameRead   = null;



    function __construct
    (
        $AWeb,
        string $AID,
        string $AIDSite = SITE_DEFAULT,
        string $AIDLang = null
    )
    {
        if( empty( $AIDLang )) $AIDLang = $AWeb -> GetIDLangDefault();

        $this -> SetOk();
        $this -> Log            = $AWeb;
        $this -> Web            = $AWeb;
        $this -> ID             = $AID;
        $this -> IDSite         = $AIDSite;
        $this -> IDLang         = $AIDLang;
        $this -> FileNameWrite  = clSourceFile( $this -> ID, $this -> IDSite, $this -> IDLang, $AWeb -> GetIDLangDefault() );
        $this -> FileNameRead   = clSourceFileAny
        (
            $this -> ID,
            $AWeb -> GetIDSiteTemplate(),
            $this -> IDLang,
            $AWeb -> GetIDLangDefault()
        );
    }



    static public function Create
    (
        $AWeb,
        string $AID,
        string $AIDSite = SITE_DEFAULT,
        string $AIDLang = null
    )
    {
        return new TWebFile( $AWeb, $AID, $AIDSite, $AIDLang );
    }



    /*
        If file information was load it return true.
    */
    public function IsPrepared()
    {
        return $this -> MIME != '';
    }



    /*
        Read information from $ASite
    */
    public function Info()
    {
        if ( $this -> IsOk())
        {
            if( empty( $this -> FileNameRead ))
            {
                $this -> SetResult( 'FileNotFound', $this -> ID );
            }
            else
            {
                /* Information is recived */
                $this -> Size       = filesize( $this -> FileNameRead );
                $this -> Extention  = pathinfo( $this -> FileNameRead, PATHINFO_EXTENSION );
                $this -> MIME       = mime_content_type( $this -> FileNameRead );
                if( empty( $this -> MIME)) $this -> MIME='application/octet-stream';
            }
        }
        return $this;
    }



    /*
        Move buffer to datasource for descript
    */
    public function BufferToFile( &$ABuffer )
    {
        if( $this -> IsOk() && $this -> IsPrepared())
        {
            /* Create streams */
            $Source = new TStream();
            $Source -> Buffer = $ABuffer;
            $Destination = TFileStream::Create( $this -> FileNameWrite, 'w' );
            /* Write file to datasource */
            $Source -> WriteTo( $Destination );
        }
        return $this;
    }



    /*
        Read data form datasource to file by path
    */
    public function FileToBuffer( &$ABuffer )
    {
        if( $this -> IsOk())
        {
            $FileName = $this -> FileNameWrite;
            if( !empty( $FileName ))
            {
                $Source = TFileStream::Create( $FileName, 'r' );
                $Destination = new TStream();
                $Source -> WriteTo( $Destination );
                $ABuffer = $Destination -> Buffer;
            }
        }
        return $this;
    }



    /*
        Read data form datasource and send to client
    */
    public function FileToClient()
    {
        if( $this -> IsOk())
        {
            $this -> Info();
            if( $this -> IsOk() )
            {
                self::SendFileHeaders( $this -> ID, $this -> MIME, $this -> Size );
                $Source = TFileStream::Create( $this -> FileNameRead, 'r' );
                $Destination = TEchoStream::Create();
                $Source -> WriteTo( $Destination );
            }
        }
        return $this;
    }




    /*
        Complex function return image form cache system
    */
    public function ImageToClient
    (
        &$AArray    /* Array of parameters for cache */
    )
    {
        if( $this -> ParamsForCache( $AArray ))
        {
            $CacheFile = $this -> CacheBuild( $AArray );
            /* Send file to client */
            if( $this -> IsOk()) self::SendFile( $this -> ID, $CacheFile );
        }
        else
        {
            /* Clear request wighout any get params */
            $this -> FileToClient();
        }
        return $this;
    }



    /*
        Complex function return image form cache system
    */
    public function GetCacheImage( &$AArray, &$ABuffer )
    {
        $ABuffer = null;

        if( $this -> IsOk() )
        {
            if( $this -> ParamsForCache( $AArray ))
            {
                $CacheFile = $this -> CacheBuild( $AArray );
                if( $this -> IsOk()) $ABuffer  = file_get_contents( $CacheFile );
            }
            else
            {
                $this -> FileToBuffer( $ABuffer );
            }
        }
        return $this;
    }



    public function ParamsForCache( &$AArray )
    {
        return
        array_key_exists( 'scalex', $AArray ) ||
        array_key_exists( 'scaley', $AArray ) ||
        array_key_exists( 'colorize', $AArray );
    }



    public function CacheBuild
    (
        $AArray /* Arrau of parameters scalex, scaley, colorize etc */
    )
    {
        $CacheFile = null;
        if( $this -> IsOk() )
        {
            if( ! file_exists( $this -> FileNameRead ))
            {
                $this -> SetResult( 'CacheSourceNotFound', null, [  'FileName' => $this -> FileNameRead ]);
            }
            else
            {
                /* Create cache name form GET params */
                $CacheFile = $this -> FileCachePath( $AArray );

                if( ! file_exists( $CacheFile ))
                {
                    /* Create path if it absend */
                    $CachePath = dirname( $CacheFile );
                    $this -> Info();
                    if( CheckPath( $CachePath ))
                    {
                        self::ImageBuildCache
                        (
                            $this -> FileNameRead,
                            $CacheFile,
                            $this -> Extention,
                            $this -> MIME,
                            $AArray,
                            $this -> Log
                        );
                    }
                }

                if( !file_exists( $CacheFile ))
                {
                    $this -> SetResult( 'CacheNotFound', $CacheFile );
                }
            }
        }

        return $CacheFile;
    }



    /*
        Delete the file by ID
    */
    public function &CacheDelete( $AIDLang )
    {
        $Mask = $this -> FileCachePath( '*' );

        /* удаление файла */
        foreach( glob($Mask) as $File )
        {
            if( !unlink( $File ))
            {
                $this -> SetCode( 'ErrorCacheFileDelete' );
                $this -> CMS -> Error( 'Cache delete error' ) -> Param( 'File', $File );
            }
        }
        return $this;
    }



    /*
        Attempt to convert file contents to array
    */
    public function &ToObject($ALineSep = PHP_EOL, $AColumnSep = ';')
    {
        $Result = [];
        $Buffer = null;
        $this -> DatasourceToBuffer($Buffer);
        if (!empty($Buffer))
        {
            if ($this -> Extention == 'json')
            {
                $Result  = json_decode( $Buffer, true );
            }
            if ($this -> Extention == 'txt')
            {
                $curLine = 0;
                $Lines = explode( $ALineSep, $Buffer );
                $CountLine = count($Lines);
                if ( $CountLine > 0 )
                {
                    $ColumnName = explode( $AColumnSep, $Lines[0] );
                    $CountColumnName = count($ColumnName);
                }
                for ($i = 1; $i < $CountLine; $i++)
                {
                    if (!empty($Lines[$i]))
                    {
                        $Columns = explode( $AColumnSep, $Lines[$i] );
                        for ($j = 0; $j < count($Columns); $j++)
                        {
                            if ($CountColumnName > $j) $Name = $ColumnName[$j];
                            else $Name = strval($j);
                            $Result[$curLine][$Name] = $Columns[$j];
                        }
                        $curLine++;
                    }
                }
            }
        }
        return $Result;
    }



    /*
        Build content form for controller
    */
    public function &ControllerOnAfterLoad( $ACMSController )
    {
        $ACMSController -> SetOutcome( 'MIME', $this -> MIME );
        $ACMSController -> SetOutcome( 'Extention', $this -> Extention );
        return $this;
    }



    public static function SendFileHeaders( $ACaption, $AMIME, $ASize )
    {
        /* сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти */
        if (ob_get_level())  ob_end_clean();

        /* заставляем браузер показать окно сохранения файла */
        header( 'Content-Disposition: attachment; filename="' . $ACaption. '"' );
        header( 'Content-Type: ' . $AMIME );
        header( 'Content-Transfer-Encoding: binary' );
        header( 'Expires: 0' );
        header( 'Cache-Control: must-revalidate' );
        header( 'Pragma: public' );
        header( 'Content-Length: '.$ASize );

        return true;
    }


    /*
        Построение файлового кэша для файла
    */
    public static function ImageBuildCache
    (
        $ASource,       /* $ASource - путь источник файла */
        $ADestination,  /* $ADestination - путь направления файла */
        $AExt,          /* Extention of file */
        $AMIME,         /* Extention of file */
        &$AParams,      /* $AParams - перечень параметров URL обычно $_GET */
        $ALog = null
    )
    {
        if( file_exists( $ASource ))
        {
            list( $wo, $ho ) = getimagesize( $ASource );
            $Source = self::ImageRead( $ASource, $AExt, $AMIME, $ALog );
            if( $Source )
            {
                if ( !empty( $ALog ))
                {
                    $ALog
                    -> Trace( 'Cache building' )
                    -> Param( 'Source', $ASource )
                    -> Param( 'Destination', $ADestination );
                }

                /* Выясняем размеры к которым надо привести изображение */
                $Aspect = $wo / $ho;

                /* Set limits for size of images */
                /* TODO it can be move to config */
                if ( array_key_exists ( 'scalex', $AParams )) $wn = min( $AParams[ 'scalex' ], 1280 );
                else $wn = $wo;

                if ( array_key_exists ( 'scaley', $AParams )) $hn = min( $AParams[ 'scaley' ], 1280 );
                else $hn = $wn / $Aspect;

                $Result = imagecreatetruecolor( $wn, $hn );

                if ( $AExt=='jpeg' || $AExt =='jpg' || $AMIME == 'image/jpg' || $AMIME == 'image/jpeg' )
                {
                    imageinterlace( $Result, 1 );
                }

                if ( $AExt=='png' || $AMIME == 'image/png' )
                {
                    imagealphablending( $Result, false );
                    imagesavealpha( $Result, true );
                }

                /* Непосредственно копирование скалирование */
                imagecopyresampled
                (
                    $Result,
                    $Source,
                    0, 0, 0, 0,
                    $wn, $hn, $wo, $ho
                );

                if ( array_key_exists ( 'colorize', $AParams ) )
                {
                    $val = $AParams[ 'colorize' ];
                    if ( $val != '' )
                    {
                        $r = hexdec( substr( $val, 0, 2 ));
                        $g = hexdec( substr( $val, 2, 2 ));
                        $b = hexdec( substr( $val, 4, 2 ));
                        $a = ( 255 - hexdec( substr( $val, 6, 2 ))) * 0.5;
                        imagefilter( $Result, IMG_FILTER_COLORIZE, $r, $g, $b, $a );
                    }
                }

                /* write cache to file */
                $Path = pathinfo ( $ADestination, PATHINFO_DIRNAME );
                if ( !file_exists( $Path )) mkdir( $Path, FILE_RIGHT, true );
                $OldMask = umask( 0077 );
                self::ImageWrite( $Result, $ADestination, $AExt, $AMIME );
                umask( $OldMask );
            }
        }
    }



    /*
        Низкоуровнеоая процедура отправки файлов.
        Отправляет файл на клиента без проверки прав.
    */
    public static function SendFile( $ACaption, $AFileName )
    {
        if ( !file_exists( $AFileName ))
        {
            print "FileNotFound ".$AFileName;
            $Result = false;
        }
        else
        {
            $Size = filesize($AFileName);

            /* определяем mime файла */
            $MIME = mime_content_type( $AFileName );

            if ( $MIME=='' ) $MIME = 'application/octet-stream';

            $Destination = new TEchoStream();
            $Source = new TFileStream( $AFileName, 'r' );

            /* Send data to client */
            self::SendFileHeaders( $ACaption, $MIME, $Size );
            $Source->WriteTo($Destination);
            $Result = true;
        }
        return $Result;
    }



    public function FileCachePath( &$AParams )
    {
        $Params = '';
        foreach( $AParams as $Key => $Value ) $Params = '@' . $Key . '=' . $Value ;

        /* Cutting part after / form ID */
        $ID = basename( $this -> ID );

        return clPathControl
        (
            clSiteCachePath( $this -> IDSite )
            . clScatterName( $ID )
            . '/'
            . $this -> IDLang
            . '/'
            . $Params . '_' . $ID
        );
    }



    /*
        File utels
    */
    public static function ImageRead( $AFile, $AExtention, $AMIME )
    {
        $Ext = strtolower( $AExtention );
        $MIME = strtolower( $AMIME );

        switch ( true )
        {
            case ( $Ext == 'jpg' || $Ext == 'jpeg' || $MIME == 'image/jpg' ||  $MIME == 'image/jpeg' ):
                $r = imagecreatefromjpeg( $AFile );
            break;
            case ( $Ext == 'gif' || $MIME == 'image/gif' ):
                $r = imagecreatefromgif( $AFile );
            break;
            case ( $Ext == 'png' || $MIME == 'image/png' ):
                $r = imagecreatefrompng( $AFile );
            break;
            case ( $Ext == 'bmp' || $MIME == 'image/bmp' ):
                $r = imagecreatefromwbmp( $AFile );
            break;
            default: $r = null;
        }
        return $r;
    }


    /*
    */
    public static function ImageWrite( &$AImage, $AFile, $AExt, $AMIME )
    {
        $Ext = strtolower( $AExt );
        $MIME = strtolower( $AMIME );

        switch ( true )
        {
            case ( $Ext == 'jpg' || $Ext == 'jpeg' || $MIME == 'image/jpg' ||  $MIME == 'image/jpeg' ):
                $r = imagejpeg( $AImage, $AFile, JPEG_QUALITY );
            break;
            case ( $Ext == 'gif' || $MIME == 'image/gif' ):
                $r = imagegif( $AImage, $AFile );
            break;
            case ( $Ext == 'png' || $MIME == 'image/png' ):
                $r = imagepng( $AImage, $AFile );
            break;
            case ( $Ext == 'bmp' || $MIME == 'image/bmp' ):
                $r = imagewbmp( $AImage, $AFile );
            break;
            default: $r = null;
        }

        return $r;
    }

}
