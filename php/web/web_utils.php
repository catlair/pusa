<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.


    Content menegment system (CMS) for Catlair PHP
    Enterance point ContentBuild().
    still@itserv.ru
*/

namespace catlair;

define( 'XML_HEADER',            '<?xml version="1.0" encoding="UTF-8"?>'); // заголовок xml для всех операций
define( 'JPEG_QUALITY', 50);



/* код ответа html */
$HTMLResponceCode = 0;


/*
    установка кода ответа html
*/
function clSetHTMLResult($ACode)
{
    global $HTMLResponceCode;
    $HTMLResponceCode = $ACode;
}



/*
    установка кода ответа html
*/
function clGetHTMLResult()
{
    global $HTMLResponceCode;
    return $HTMLResponceCode;
}


/*
    получение корневого пути
*/
function clRootPath( $ARoot = ROOT )
{
    if ( $ARoot == ROOT ) $ARoot = dirname( $ARoot );
    return $ARoot;
}



/*
    Return Site path
*/
function clSitePath( $AIDSite, $ARoot = ROOT )
{
    $Result = clRootPath( $ARoot ) . '/site/' . $AIDSite;
    return $Result;
}



/*
    Return path to any file in project
    [ROOT]/site/[FILE] - for custom site
    [ROOT]/[FILE] - for empty site
*/
function clSiteFile
(
    string $AFile,
    string $AIDSite    = '',
    string $ARoot      = ROOT
)
{
    return
    (
        empty( $AIDSite )
        ? clRootPath( $ARoot )
        : clSitePath( $AIDSite, $ARoot )
    ) .
    (
        strpos( $AFile, '/' ) === 0
        ? ''
        : '/'
    ) . $AFile;
}



/*
    Return content store path
*/
function clContentPath( $AIDSite, $AIDLang, $AID )
{
    return clSitePath( $AIDSite ) . '/content/' . $AIDLang . '/' . $AID;
}



/*
    Return temp path
*/
function clTmpPath( $AIDSite )
{
    return clSitePath($AIDSite) . '/tmp';
}



/*
    Return content store path
*/
function clDescriptPath( $AIDSite )
{
    return clSitePath($AIDSite) . '/descript';
}



/*
    Return export store path
*/
function clExportPath( $AIDSite )
{
    return clSitePath( $AIDSite ) . '/export';
}



/*
    Return export store path
*/
function clDomainPath( $ARoot = ROOT )
{
    return clRootPath( $ARoot ) . '/domain';
}



/*
    Return export store path
*/
function clDomainFile( $ADomain, $ARoot = ROOT )
{
    return clDomainPath( $ARoot ) . '/' . $ADomain;
}



/*
    Return Site config file
*/
function clSiteConfigPath( $AIDSite, $ARoot = ROOT )
{
    return clSitePath( $AIDSite, $ARoot ).'/config.json';
}



function clConfigPath( $AIDSite, $ARoot = ROOT )
{
    return clSitePath( $AIDSite, $ARoot ) . '/config';
}



/*
    Return $Afile in config path for site
*/
function clConfigFile( $AIDSite, $AFile, $ARoot = ROOT )
{
    return clConfigPath( $AIDSite, $ARoot ) . '/' . $AFile;
}



/*
    Return the global config path
    Example: /var/www/html/catlair/config
*/
function clConfigGlobal( $ARoot = ROOT )
{
    return clRootPath( $ARoot ) . '/config';
}



/*
    Return the global config path with file $AFile
    Example: /var/www/html/catlair/config/$AFile
*/
function clConfigGlobalFile( $AFile, $ARoot = ROOT)
{
    return clConfigGlobal( $ARoot ) . '/' . $AFile;
}



function clSiteCachePath( $AIDSite, $ARoot = ROOT)
{
    return clSitePath( $AIDSite, $ARoot ) . '/cache';
}



function clFileCachePath( $AIDSite, $AIDLang, $AFile, $AParams = null )
{
    return clPathControl
    (
        clSiteCachePath( $AIDSite ) .
        clScatterName( $AFile, 3 ) .
        '/' .
        $AIDLang .
        ( empty( $AParams ) ? '' : ( '/' . $AParams ))
    );
}




/*
    Возвращает путь до движка на базе Catlair
*/
function clGetEngineFile( $AEngine, $ARoot = ROOT )
{
    return clLibraryPath( null, $ARoot ) . '/' . $AEngine . '/' . $AEngine . '.php';
}



/*
    Функции для пуей PHP библиотек загружаемых при помощи cast
*/


/*
    Возвращает путь .
    $AIDSite - идентификтор сайта
*/
function clLibraryPath( $AIDSite = null, $ARoot = ROOT )
{
    return ( $AIDSite == null ? clRootPath( $ARoot ) : clSitePath( $AIDSite, $ARoot ) ) . '/php';
}



/*
    Возвращает полное имя файла библиотеки по имени
    $AID - идентификтор дескрипта
    $APath - путь ранее полученный функцией clDescriptPath
*/
function clLibraryFile( $AName, $APath )
{
 return $APath . '/' . $AName . '.php';
}



function clLibraryFileAny( $AName, $AIDSite )
{
    /* пытаемся найти файл библиотеки c указанного сайта */
    $PathName = clLibraryPath( $AIDSite );
    $FileName = clLibraryFile( $AName, $PathName );

    if( !file_exists( $FileName ))
    {
        /* пытаемся найти файл библиотеки с умолчального сайта */
        $PathName = clRootPath() . '/php';
        $FileName = clLibraryFile( $AName, $PathName );
        if( ! file_exists( $FileName ))
        {
            $FileName = null;
        }
    }
    return $FileName;
}



function clIDLangToHTML( $AIDLang )
{
    $Arr = explode( '_', $AIDLang );
    return ( count( $Arr ) > 1 ) ? $Arr[ 1 ] : 'en';
}



function clSiteTmpPath( $AIDSite, $ARoot = ROOT )
{
    return clSitePath( $AIDSite, $ARoot ) . '/tmp';
}


/*
    Return the tmp file by file name for site
*/
function clSiteTmpFile
(
    $AIDSite,           /* Site identifer */
    $AFile = null,      /* File name or null for guid */
    $ARoot = ROOT       /* ROOT folder */
)
{
    $Result = clPathControl
    (
        clSiteTmpPath( $AIDSite, $ARoot ) . '/' . clGUID() . ( empty( $AFile ) ? '' : ( '_' . $AFile ) )
    );
    CheckPath( dirname( $Result ) );
    return $Result;
}



function clSiteLogPath
(
    $AIDSite = null,
    $ARoot = ROOT       /* ROOT folder */
)
{
    if( empty( $AIDSite )) $AIDSite = SITE_DEFAULT;
    return clSitePath( $AIDSite, $ARoot ) . '/log';
}
