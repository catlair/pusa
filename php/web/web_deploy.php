<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/


namespace catlair;
/*
    Catlair PHP
    CLI интерфейс

    The Deploy.
    Build the default php code and specific php code.
    Made obfircate.

    still@itserv.ru
*/

require_once( ROOT . '/core/deploy.php' );



class TWebDeploy extends TDeploy
{
    const DEFAULT_PATHES =
    [
        /* PHP libraries,DOTO remove it from project */
        'cli',
        'root/index.php',
        'php/controller/Utils.php',
        'php/core/'     . '*.php',
        'php/pusa/'     . '*.php',
        'php/web/'      . '*.php',
        'php/README.md',
        'php/core/README.md',
        'domain/README.md'
    ];


    private $ProdPath = '';
    private $IDSite = '';



    public function GetIDSite()
    {
        return $this -> GetConfig() -> GetParam( 'IDSite', 'site_default' );
    }



    public function GetRootPath( $APath = '' )
    {
        return $this -> GetConfig() -> GetString( 'CatlairPath', '/var/www/html/catlair' ) . $APath;
    }



    public function GetLibraryPath( $AIDSite = null )
    {
        return clLibraryPath( $AIDSite, $this -> GetRootPath() );
    }



    public function GetPHPPath( $AIDSite )
    {
        return clLibraryPath( $AIDSite, $this -> GetRootPath() );
    }



    /*
        Build PHP code
        DODO удалить это из проекта
        Испольльзование построителя по сайту не практично
        Использовать BuildSourceByPath
    */
    public function BuildSource
    (
        $ARules     = self::DEFAULT_PATHES,    /* List pathes for building */
        $AIDSite    = null,                    /* ID Site */
        $AObfuscate = false
    )
    {
        /* Create export path */
        $this -> BuildRules
        (
            $ARules,
            /* Source path for root or path for site */
            empty( $AIDSite ) ? clRootPath() : clSitePath( $AIDSite ),
            /* Destination path for root or path for site */
            empty( $AIDSite ) ? $this -> GetRootPath() : $this -> GetSitePath( $AIDSite ),
            /* Callback */
            function( $AFile, $AContent ) use ( $AObfuscate )
            {
                $Ext = pathinfo( $AFile, PATHINFO_EXTENSION );
                switch( $Ext )
                {
                    /* PHP */
                    case 'php':
                        $Result = ! $AObfuscate ? $AContent : $this -> PHPObfuscate
                        (
                            $AContent,
                            [
                                [ 'Key' => '#!/usr/bin/php ',   'Value' => '#!/usr/bin/php' . chr(10) ],
                                [ 'Key' => '<?'.'php',          'Value' => '<?'.'php' . chr(10) . '/' . '* CATALIR PHP CODE *' . '/' . chr(10)]
                            ]
                        );
                    break;
                    /* Any files */
                    default:
                        $Result = $AContent;
                    break;
                }
                return $Result;
            }
        );
        return $this;
    }



    /*
        Build PHP code for site - do not use it
    */
    public function BuildSourceSites
    (
        $ASites     = []
    )
    {
        foreach( $ASites as $IDSite => $Config )
        {
            $this -> BuildSource
            (
                clValueFromObject( $Config, 'Files', [] ),          /* List pathes for building */
                $IDSite,                                            /* ID Site */
                clValueFromObject( $Config, 'Obfuscate', false )    /* Obfuscate source */
            )

            /* Domain config files prepare */
            -> DomainConfig
            (
                clValueFromObject( $Config, 'DomainFrom', '' ),
                clValueFromObject( $Config, 'DomainTo', [] )
            );
        }
        return $this;
    }








    public function NginxSiteConfig
    (
        $ASites     = []
    )
    {
//        clValueFromObject( $Config, 'NginxTemplate', 'http' ),
//        clValueFromObject( $Config, 'DomainTo', [] )
        return $this;
    }



    public function TemplateCopy
    (
        $AID,       /* Source from manual folder */
        $AIDSite    = SITE_DEFAULT,
        $AIDLang    = null,
        $ADest      = '',
        $ATarget    = self::TARGET_DEPLOY
    )
    {
        if( $this -> IsOk() )
        {
            if( empty( $AIDLang )) $AIDLang = $this -> Web -> GetIDLangDefault();
            if( empty( $ADest )) $ADest = $AID;
            $Content = $this -> GetLog() -> GetTemplate( $AID, '', $AIDSite, $AIDLang, false );
            $Dest = $this -> GetFullPath( $ADest, $ATarget );
            if( !$this -> IsTest( $ATarget ))
            {
                if( file_put_contents( $Dest, $Content ) === false )
                {
                    $this -> SetResult( 'ErrorWriteTemplate', $Dest );
                }
            }
        }
        return $this;
    }



    /*
        Create domain config from template
    */
    public function &DomainConfig( $ASource, $AName, $AReplace = [] )
    {
        foreach( $AName as $Key => $Value )
        {
            $this
            -> Copy
            (
                TDeploy::TARGET_LOCAL,
                TDeploy::TARGET_DEPLOY,
                clDomainFile( $ASource ),
                clDomainFile( $Value,  $this -> GetRootPath() )
            )
            -> ReplaceFile
            (
                TDeploy::TARGET_DEPLOY,
                clDomainFile( $Value,  $this -> GetRootPath() ),
                $AReplace
            );
        }
        return $this;
    }



    public function ConfigToProduction( $AIDSite, $AConfig, $AReplace = [] )
    {
        $this
        -> Copy
        (
            TDeploy::TARGET_LOCAL,
            TDeploy::TARGET_DEPLOY,
            clConfigFile( $AIDSite, $AConfig ),
            clConfigFile( $AIDSite, $AConfig, $this -> GetRootPath() )
        )

        -> ReplaceFile
        (
            TDeploy::TARGET_DEPLOY,
            clConfigFile( $AIDSite, $AConfig, $this -> GetRootPath() ),
            $AReplace
        );

        return $this;
    }






    /*
        Построитель кода по списку секций
    */
    public function BuildSourcesBySections
    (
        $ASections = []
        /*
            Source - constant TDeploy::TARGET_*
            Destination - constant TDeploy::TARGET_*
            Rules - list of rules for BuildSourcesByRules
            Obfuscate
        */
    )
    {
        foreach( $ASections as $Section )
        {
            $this -> BuildSourcesByRules
            (
                clValueFromObject( $Section, 'Source', '[LOCAL_PROJECT]' ),
                clValueFromObject( $Section, 'Destination', '[BUILD][DEPLOY_PROJECT' ),
                clValueFromObject( $Section, 'Rules', [] ),
                clValueFromObject( $Section, 'Obfuscate', false ),
                clValueFromObject( $Section, 'Exclude', [] )
            );
        }
        return $this;
    }



    /*
        Построитель кода по одной секции правил
    */
    public function BuildSourcesByRules
    (
        $ASource        = '[LOCAL_PROJECT]',   /**/
        $ADestination   = '[BUILD][DEPLOY_PROJECT]',  /**/
        $ARules         = [],                   /* List pathes for building */
        $AObfuscate     = false,
        $AExclude       = []
    )
    {
        $Source = $this -> PathPrep( $ASource );

        /* Create export path */
        $this -> BuildRules
        (
            $ARules,
            /* Source path for root or path for site */
            $this -> PathPrep( $ASource ),
            /* Destination path for root or path for site */
            $this -> PathPrep( $ADestination ),
            /* Callback */
            function( $AFile, $AContent ) use ( $Source, $AObfuscate )
            {
                $Ext = pathinfo( $AFile, PATHINFO_EXTENSION );
                switch( $Ext )
                {
                    /* PHP */
                    case 'php':
                        $Result = !$AObfuscate ? $AContent : $this
                        -> PHPObfuscate
                        (
                            $AContent,
                            [
                                '#!/usr/bin/php ' => '#!/usr/bin/php' . PHP_EOL,
                                '<?php '          => '<?php' . PHP_EOL
                            ]
                        );
                    break;
                    /* Any files */
                    default:
                        $Result = $AContent;
                    break;
                }
                return $Result;
            },
            $AExclude
        );
        return $this;
    }

}
