<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/


namespace catlair;
require_once ROOT . '/core/debug.php';
require_once ROOT . '/core/utils.php';
require_once ROOT . '/core/params.php';
require_once ROOT . '/web/web_utils.php';



class TWebTools
{
    /*
       Scan all domian files in ROOT/domain folder and check settings
    */
    static public function DomainsNormalize( $ALog )
    {
        $ALog -> Begin( 'Domains notmalize' );
        $Path = clDomainPath();
        $ALog -> Trace() -> Param( 'Local path', $Path );
        clFileScan
        (
            $Path,
            null,
            function( $AFile, $AIndex ) use ( $ALog )
            {
                $ALog -> Trace() -> Param( 'File', $AFile );

                $Content = file_get_contents( $AFile, true );
                $JSON = json_decode( $Content );
                if( $JSON )
                {
                    $Domain = new TParams();
                    $Domain -> SetParams( $JSON );

                    /* Check SSL key */
                    if( $Domain -> GetString( 'SSLKey', '' ) == 'AUTO_SSLKey' )
                    {
                        $ALog -> Info( 'Found AUTO_SSLKey' );
                        $Content = str_replace
                        (
                            'AUTO_SSLKey',
                            base64_encode(openssl_random_pseudo_bytes( 32 )),
                            $Content
                        );
                    }

                    if( file_put_contents( $AFile, $Content ) === false)
                    {
                        $ALog -> Warning( 'Error write to file' ) -> Param( 'File', $AFile );
                    }
                }
            }
        );

        $ALog -> End();
    }



    static public function FileContentUpdate( $ALog, $APath, $AMask, $AContectUpdate, $ASave = true )
    {
        $ALog -> Begin( 'File Contect Update' );
        if (!empty($APath))
        {

            $ALog -> Trace() -> Param( 'Local path', $APath );
            clFileScan
            (
                $APath,
                null,
                function( $AFile, $AIndex ) use ( $ALog, $AContectUpdate, $ASave, $AMask )
                {
                    $ALog -> Trace() -> Param( 'File', $AFile );

                    $Content = file_get_contents( $AFile, true );

                    if( fnmatch( $AMask, $AFile ) && !empty($Content) && !empty($AMask) )
                    {
                        $ALog -> Debug('File Update') -> Value($AFile);
                        $ContentModify = call_user_func( $AContectUpdate, $ALog, $Content );

                        if ( $ASave && strcmp($ContentModify, $Content) !== 0 )
                        {
                            $ALog -> Debug('SaveFile');

                            if( file_put_contents( $AFile, $ContentModify ) === false)
                            {
                                $ALog -> Warning( 'Error write to file' ) -> Param( 'File', $AFile );
                            }

                        }
                    }
                }
            );
        }
        $ALog -> End();
    }
}
