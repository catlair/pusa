<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/



/*
    Pusa daemon
*/

namespace catlair;

class Daemon extends TWebPusa
{
    private $Name       = null;         /* Service name */
    private $SkipPause  = false;        /* True for main pause skeep */
    private $MainPause  = CL_SECOND;    /* Main timeout */



    /*
        Call on before run event
    */
    public function OnBeforeRun
    (
        string $Name
    )
    {
        /* Daemon name must be define */
        $this -> Validate( !empty( $Name ), 'NameIsEmpty', 'The daemon needs a name --Name' );
        $this -> Name = $Name;
        return $this;
    }



    /**************************************************************************
        Daemon on systemctl

    */

    public function Install()
    {
        return $this;
    }



    public function Uninstall()
    {
        return $this;
    }



    /**************************************************************************
        Daemon on posx

    */


    /*
        Return PID file name
    */
    private function GetPIDFile()
    {
        return '/var/run/' . $this -> Name . '.pid';
    }



    /*
        Return true if daemon exists
    */
    private function IsDaemonActive()
    {
        $Result = false;
        $PIDFile = $this -> GetPIDFile();

        if( file_exists( $PIDFile ) && is_file( $PIDFile ))
        {
            $Result = posix_kill
            (
                file_get_contents( $PIDFile ),
                0
            );
        }

        return $Result;
    }



    private function WritePIDFile( $APID )
    {
        file_put_contents
        (
            $this -> GetPIDFile(),
            $APID
        );
        return $this;
    }



    private function DeletePIDFile()
    {
        unlink( $this -> GetPIDFile() );
        return $this;
    }



    /*
    */
    public function Start
    (
        string $Job = null
    )
    {
        $this
        -> Validate( ! empty( $Job ),             'JobNotFound', 'Need the Job parameter --Job' )
        -> Validate( ! $this -> IsDaemonActive(), 'DaemonAlreadyExists', 'Daemon already exists' );

        if( $this -> IsOk() )
        {
            $ChildPID = pcntl_fork();
            if( $ChildPID )
            {
                /* Parent pocess */
            }
            else
            {
                /* Write PID */
                $this -> WritePIDFile( getmypid() );

                /* Child process detach from console */
                fclose( STDIN );
                fclose( STDOUT );
                fclose( STDERR );

                /* Switch log to file */
                $this -> GetLog()
                -> SetDestination( TLog::FILE )
                -> SetLogPath( $this -> GetWeb() -> GetSiteLogPath() . '/daemons' )
                -> SetLogFile( $this -> Name )
                -> Info( 'Daemon log being' );

                /* Child process transforms to main process */
                posix_setsid();

                /* Run main loop */
                $this -> Job( $Job );

                /* Delete PID file after loop */
                $this -> DeletePIDFile();
            }
        }
        return $this;
    }



    /*
        Stop the daemon
    */
    public function Stop()
    {
        if( $this -> IsDaemonActive() )
        {
            $PIDFile = $this -> GetPIDFile();
            if( file_exists( $PIDFile ) && is_file( $PIDFile ))
            {
                posix_kill
                (
                    file_get_contents( $PIDFile ),
                    SIGTERM
                );
            }
        }
        return $this;
    }



    private function SignalInt()
    {
        $this->Active = false;
    }



    private function SignalTerm()
    {
        $this -> Active = false;
    }



    /*
        Main job
    */

    /*
        Main daemon loop
    */
    private function Job
    (
        string $AJob   /* Job name */
    )
    {
        /* Set signal SIGTERM */
        pcntl_signal( SIGTERM, [ $this, 'SignalTerm' ]);

        /* Set signal SIGHUP */
        pcntl_signal( SIGINT, [ $this, 'SignalInt' ]);

        /* Start main loop */
        $this -> Log -> Begin( 'Loop' ) -> LineEnd();

        $JobObject = $this -> Mutate( $AJob );

        /* Main job */
        if( method_exists( $JobObject, 'OnStart' ))  $JobObject -> OnStart();

        /* Main job */
        if( method_exists( $JobObject, 'OnJob' ))
        {
            $this -> Active = true;
            while( $this -> Active )
            {
                /* Skip pause set as false. Job can be set skip pause in true */
                $this -> SkipPause  = property_exists( $JobObject, 'SkipPause' ) ? $JobObject -> SkipPause : false;
                $this -> Active     = property_exists( $JobObject, 'Active' )    ? $JobObject -> Active : $this -> Active;

                $JobObject -> OnJob();

                /* Loop pause with signal interrupt */
                $PauseAccum = 0;
                while
                (
                    $PauseAccum == 0 ||                     /* первый цикл сработает всегда */
                    ! $this -> SkipPause &&                 /* не надо пропускать паузу */
                    $PauseAccum < $this -> MainPause &&     /* пауза меньше основной */
                    $this -> Active                         /* сервис еще активен */
                )
                {
                    pcntl_signal_dispatch();
                    $Pause = 10 * TMoment::MILISECOND;
                    $PauseAccum += $Pause;
                    usleep( $Pause );
                }

            }
        }

        /* Call After action */
        if ( method_exists( $JobObject, 'OnStop' )) $this -> OnStop();

        $this -> Log -> End();
        return $this;
    }
}
