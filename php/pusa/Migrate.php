<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/


namespace catlair;

class Migrate extends TWebPusa
{

    private $Data = null;



    /*
    */
    protected function Init
    (
        TData $AData
    )
    {
        $this -> Data = $AData;
        return $this;
    }

    public function GetDS()
    {
        return $this -> Data;
    }


    /*
        Return the file name by ScriptName
    */
    protected function GetFileName
    (
        string $AProcName
    )
    {
        $ClassName = ( new \ReflectionClass( $this )) -> getShortName();
        return $this
        -> GetWeb()
        -> GetSitePath
        (
            $this -> Data -> GetType() . '/pusa/migrate/' . $ClassName . '/' . $AProcName . '.sql'
        );
    }



    /*
        Stored procedure upload from file by name
    */
    protected function ProcUpload
    (
        string $AProcName
    )
    {
        $this -> Data -> ProcUpload( $AProcName, $this -> GetFileName( $AProcName ) ) -> ResultTo( $this );
        return $this;
    }



    protected function FuncUpload
    (
        string $AFuncName
    )
    {
        $this -> Data -> FuncUpload( $AFuncName, $this -> GetFileName( $AFuncName ) ) -> ResultTo( $this );
        return $this;
    }



    protected function DatabaseSelect()
    {
        $this -> Data -> DatabaseSelect() -> ResultTo( $this );
        return $this;
    }



    /*
        Create migration structure
    */
    protected function CreateMigrateStructure()
    {
        if ($this -> IsOk())
        {
            $this -> DS = $this -> GetWeb() -> GetDatasource(null, null, false );
            if( !$this -> DS -> DatabaseExists( $this -> DS -> GetDatabase() ))
            {
                $this -> DS
                -> DatabaseCreate
                (
                    $this -> DS -> GetDatabase(),
                    [
                        'Codepage'  =>  TData::CODEPAGE_UTF8
                    ]
                )
                -> ResultTo($this);

            }
            if ($this -> IsOk())
            {
                $this -> DS
                -> DatabaseSelect()
                -> ResultTo($this);
            }
            if (
                $this -> IsOk() && 
                !$this -> DS -> TableExists('SysMigrate')
                ) 
            {
                $this -> DS
                -> TableCreate
                ([
                    'Name'      => 'SysMigrate',
                    'Fields'    =>
                    [
                        [
                            'Name'      => 'ID',
                            'Type'      => TData::TYPE_BIN,
                            'Length'    => 16,
                            'NotNull'   => true,
                            'Key'       => TData::KEY_PRIMARY
                        ],

                        [
                            'Name'      => 'DTInsert',
                            'Type'      => TData::TYPE_MOMENT
                        ],

                        [
                            'Name'      => 'Name',
                            'Type'      => TData::TYPE_STRING_UNSIZE,
                            'Length'    => 1024
                        ]
                    ]
                ])
                -> ResultTo($this);
            }
            if ($this -> IsOk()) 
            {
                $ProcMigrateCheck =
                '
                CREATE PROCEDURE `SysMigrateCheck`( 
                    in aJson longtext
                    )
                BEGIN
                    declare lID binary(16);
                    declare lName varchar(1024);

                    IF (JSON_VALID(aJson)) THEN
                        set lName = JSON_UNQUOTE(JSON_EXTRACT(aJson, "$.Name" )); 
                    END IF;

                    SELECT ID INTO lID FROM SysMigrate WHERE Name=lName
                    ;

                    SELECT CASE WHEN lID is null THEN True ELSE False END Result
                    ;
                END
                ';
                $ProcMigrateInsert =
                '
                CREATE PROCEDURE `SysMigrateInsert`(
                    in aJson longtext
                    )
                BEGIN
                    declare lID binary(16);
                    declare lName varchar(1024);

                    IF (JSON_VALID(aJson)) THEN
                        set lName = JSON_UNQUOTE(JSON_EXTRACT(aJson, "$.Name" )); 
                    END IF;

                    IF (NOT EXISTS(SELECT ID FROM SysMigrate WHERE Name=lName)) THEN
                        SET lID=unhex(replace(UUID(),"-",""));
                        INSERT INTO SysMigrate (ID, DTInsert, Name)
                        VALUES (lID, utc_timestamp(), lName)
                        ;
                    END IF;

                    SELECT "Ok" Code
                    ;
                END
                ';

                $this -> DS
                -> ProcUploadByContent('SysMigrateInsert', $ProcMigrateInsert)
                -> ProcUploadByContent('SysMigrateCheck', $ProcMigrateCheck)
                -> ResultTo($this);
            }
        }
        return $this;
    }



    protected function CheckMigration($AMigration)
    {
        $Result = false;
        /* Read migration list*/
        if ($this -> IsOk())
        {
            $Dataset = $this -> GetWeb() -> GetDatasource()
            -> ProcJSON
            (
                'SysMigrateCheck',
                [
                    'Name'      => $AMigration
                ]
            )
            -> GetDataset()
            ;
            if ($Dataset && count($Dataset)>0 && $Dataset[0] -> Result)
            {
               $Result = true;
            }
        }
        return $Result;
    }



    protected function SaveMigration($AMigration)
    {
        if ($this -> IsOk())
        {
            $Dataset = $this -> GetWeb() -> GetDatasource()
            -> ProcJSON
            (
                'SysMigrateInsert',
                [
                    'Name'      => $AMigration
                ]
            )
            -> GetDataset()
            -> GetResult()
            -> ResultTo( $this );
        }
        return $this;
    }



    public function ApplyAll()
    {
        
        /* Check and create database */
        $this -> CreateMigrateStructure();

        /* Read migration list*/
        if ($this -> IsOk())
        {
            $Migrations = glob( $this -> GetWeb() -> GetPusaPath( 'migrate/' . '*.php' ));
            foreach( $Migrations as $Path )
            {
                $Migration = pathinfo( $Path, PATHINFO_FILENAME );
                $this -> GetLog() -> Begin( 'Migration' ) -> Text( ' ' . $Migration );
    
                /* Check migration in database */
                if( $this -> CheckMigration( $Migration ))
                {
                    /* Apply migration */
                    $this 
                    -> Mutate('migrate/' . $Migration) 
                    -> Up()
                    -> Switch($this)
                    ;

                    /* Save result to database */
                    $this -> SaveMigration($Migration);
                }
                $this -> GetLog() -> End( );
            }
        }
        return $this;
    }    

}
