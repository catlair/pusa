<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/



/*
    The tools Pusa controller.
*/
namespace catlair;
/*
require_once( ROOT . '/cms/descripts.php' );
require_once ROOT . '/cms/cms_pusa.php';
require_once ROOT . '/cms/cms_dataset.php';
require_once ROOT . '/cms/cms_utils.php';
require_once ROOT . '/webdb/webdb.php';
*/
require_once ROOT . '/web/source_utils.php';

class Tools extends TWebPusa
{

    /*
        Create default database
        CLI call:
            ./cli --IDDomain=dev.test.catlair.net --Pusa=Tools --Method=ExportDescripts
    */

    public function ExportDescripts()
    {
        if( !$this -> GetWeb() -> IsCLI() )
        {
            $this -> SetResult( 'OnlyForCLI' );
        }
        else
        {
            $IDSite         = GetIncomeCLI( 'IDSite',           'Site identify',        SITE_DEFAULT,   false );
            /*$IDTypeDescript = GetIncomeCLI( 'IDTypeDescript',   'Type Descript',        'Content',      false );*/
            $IDTypeDescript = GetIncomeCLI( 'IDTypeDescript',   'Type Descript',        'Descript',      false );
            $DatasourceName = GetIncomeCLI( 'Datasource',       'Datasource',           'default',      false); 
            $IDSiteSave     = GetIncomeCLI( 'IDSiteSave',           SITE_DEFAULT,        SITE_DEFAULT,   false );           
            $DBName = GetIncomeCLI( 'DBName',       'site_default_default',           'site_default_default',      false);            
            $GetSiteNullContent = GetIncomeCLI( 'GetSiteNullContent',       'GetSiteNullContent',           true,      false);

/*
            $IDSiteSave     = GetIncomeCLI( 'IDSiteSave',           'monhive',        'monhive',   false );           
            $DBName = GetIncomeCLI( 'DBName',       'monhive_default',           'monhive_default',      false);            
            $GetSiteNullContent = GetIncomeCLI( 'GetSiteNullContent',       true,           true,      false);
*/
/*            
            $IDSiteSave     = GetIncomeCLI( 'IDSiteSave',           'observing_site',        'observing_site',   false );           
            $DBName = GetIncomeCLI( 'DBName',       'DBName',           'monhive_default',      false);
            $GetSiteNullContent = GetIncomeCLI( 'GetSiteNullContent',       false,           false,      false);
*/
            $Datasource = $this -> GetWeb() -> GetDatasource( $DatasourceName, null, false );
            $Datasource  -> DatabaseSelect($DBName);

            $RecordCurrent = 0;
            $RecordCount = 100;
            $TotalRecord = 0;
            $DescriptList = [];

            $this -> Log -> Debug('ExportDescripts IDSiteSave') -> Value($IDSiteSave);
            $this -> Log -> Debug('ExportDescripts DBName') -> Value($DBName);

            do
            {
                $Filters = 
                [
                    'IDSiteQuery' => $IDSiteSave,
                    'IDType' => $IDTypeDescript,
                    'ReturnSiteNull' => $GetSiteNullContent,
                    'RecordCurrent' => $RecordCurrent,
                    'RecordCount' => $RecordCount
                ];
                $Datasource 
                -> Proc('DescriptSelectByType', array( json_encode($Filters) ))
                -> ResultTo( $this ); 

                if ($this -> IsOk())
                {
                    $Header = $Datasource -> GetDataset(0);
                    $Records = $Datasource -> GetDataset(1);
                    /*
                    $this -> Log -> Debug('ExportDescripts Header') -> Print($Header);
                    $this -> Log -> Debug('ExportDescripts Records') -> Print($Records);
    */
                    $TotalRecord = $Header[0] -> TotalRecord;
                    $RecordCurrent += $RecordCount;
    
    
                    foreach($Records as $Record)
                    {
                        
                        $IDDescript = 'descript/' . (strtolower(md5($Record -> Caption)) === strtolower($Record -> IDDescript) ? $Record -> Caption : $Record -> IDDescript);
                        $this -> Log -> Debug('ExportDescripts IDDescript') -> Value($IDDescript);
                        $this -> Log -> Debug('ExportDescripts IDDescript') -> Value($Record -> IDDescript);
                        $this -> Log -> Debug('ExportDescripts IDLang') -> Value($Record -> IDLang);
                        $this -> Log -> Debug('ExportDescripts Caption') -> Value($Record -> Caption);
                        //$this -> Log -> Debug('ExportDescripts Content') -> Print($Record -> Content);
                        clSetSource( $IDDescript, $IDSiteSave, $Record -> IDLang, $Record -> Content );
                    }
                }
            }
            while ($this -> IsOk() && $TotalRecord > $RecordCurrent);


        }
        return $this;
    }
}
