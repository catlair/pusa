<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/



/*
    The utils Pusa controller.
*/

namespace catlair;

class TestIntervals extends TWebPusa
{
    public function Test()
    {
        /* Интервалы */
        $Intervals = TIntervals::Create()
        -> Add( TInterval::Create() -> FromMomentString( '> 2022-01-02' ))
        -> Add( TInterval::Create() -> FromMomentString( '2021-01-04 > 2023-01-05' ))
        -> Add( TInterval::Create() -> FromMomentString( '2022-08-24 08:29:04 >' ))
        -> Optimize()
        -> Exclude( TInterval::Create() -> FromMomentString( '2022-07-24 12:30:00 > 2022-08-24 11:20:00' ))
        -> Round( TMoment::HOUR );

        /*
            Тестовое расписание
        */
        $WeekMonth = TInterval::Create() -> FromMomentString( '2022-09-01 > 2022-10-01');
        $IntervalsWeek = TIntervals::Create() -> CreateWeek
        (
            $WeekMonth,
            [
                [ '00:00 > 05:00', '21:00 > 24:00' ],
                [ '00:00 > 05:00', '21:00 > 24:00' ],
                [ '00:00 > 05:00', '21:00 > 24:00' ],
                [ '00:00 > 05:00', '21:00 > 24:00' ],
                [ '00:00 > 05:00', '21:00 > 24:00' ],
                [ '00:00 > 04:30', '18:00 > 24:00' ],
                [ '00:00 > 24:00' ],
            ]
        );

        $IntervalsTime = TIntervals::Create()
        -> Add( TInterval::Create() -> FromTimeString( '10:00 > 11:00' ));

        $Moment = TMoment::Create( '2022-07-24 13:00:00' );
        $Moment2 = TMoment::Create( '2020-07-24 13:00:00' );

        $this -> GetLog()
        -> Info( json_encode( $IntervalsTime -> ToStringODBCArray() ))
        -> Info( json_encode( $Intervals -> ToStringODBCArray() ))

        -> Info( json_encode( $IntervalsWeek -> ToStringODBCArray() ))
        -> Info( json_encode( $IntervalsWeek -> Sum( $WeekMonth ) / TMoment::HOUR ))
        -> Print( $IntervalsWeek -> DumpSchedule( $WeekMonth ))

//        -> Test
//        (
//            $Intervals -> TestMoment( $Moment ),
//            $Moment -> ToStringODBC(),
//            'Test Moment Pass',
//            'Test Moment Failed'
//        )
//
//        -> Test
//        (
//            $Intervals -> TestMoment( $Moment2 ),
//            $Moment2 -> ToStringODBC(),
//            'Test Moment Pass',
//            'Test Moment Failed'
//        )
        ;
    }
}
