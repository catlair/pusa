<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

namespace catlair;

require_once ROOT . '/web/win.php';
require_once ROOT . '/core/html.php';



class Moment extends TCMSPusa
{
    use TWin;

    /*
        Public propertyes
    */
    public $IDBegin = null;
    public $IDEnd   = null;
    public $Begin   = null;
    public $End     = null;
    public $Last    = null;
    public $Current = null;



    /*
        Activate moment element
        Backend method
    */
    public function Activate
    (
        string $AContainerBegin,            /* Container for begin element */
        string $AContainerEnd   = null,     /* Container for end element */
        string $ANameBegin      = null,     /* Name for element Begin of will be use the Container */
        string $ANameEnd        = null,     /* Name for element End of will be use the Container */
        array  $AOnChange       = []        /**/
    )
    {
        $this -> IDBegin    = clGUID();
        $this -> IDEnd      = clGUID();

        $this -> ActivateElement( $AContainerBegin, $ANameBegin, $this -> IDBegin, $this -> IDBegin, $this -> IDEnd );
        if( !empty( $AContainerEnd ))
        {
            $this -> ActivateElement( $AContainerEnd, $ANameEnd, $this -> IDEnd, $this -> IDBegin, $this -> IDEnd );
        }

        return $this;
    }



    /*
        Activate one moment element children Popup elements by class name
        backend
    */
    private function ActivateElement
    (
        string $AContainer,
        string $AName = null,
        string $AID,    /* Current ID */
        $IDBegin,       /* ID begin element */
        $IDEnd          /* ID end element */
    )
    {
        return $this
        -> FocusPush()
        -> FocusChildren( $AContainer )
        -> DOMClassAdd( 'Moment' )
        -> DOMID( $AID )
        -> DOMContent( $this -> GetTemplate( 'ui/moment/Moment.html' ) )

        -> FocusChildren( 'MomentValue' )
        -> DOMName( empty( $AName ) ? $AContainer : $AName )

        -> FocusSibling( 'Moment', 'SelectButton' )
        -> EventThis
        (
            'Popup',
            $this -> Clone()

            /* Read value from Begin */
            -> FocusParentFirst( 'Line' )
            -> FocusChildren( $IDBegin, TPusa::ID )
            -> FocusChildren( 'MomentValue' )
            -> ReadValue( 'AValueBegin' )

            /* Read value from End */
            -> FocusParentFirst( 'Line' )
            -> FocusChildren( $IDEnd, TPusa::ID )
            -> FocusChildren( 'MomentValue' )
            -> ReadValue( 'AValueEnd' )
        )
        -> FocusPop()
        ;
    }



    /*
        Popup moment dialog
        Frontend
    */
    public function Popup
    (
        $AValueBegin,
        $AValueEnd
    )
    {
        /*
            Read values from Inputs
        */

        $this -> Begin = ( empty( $AValueBegin ))
        ? TMoment::Create() -> Now() -> Get()
        : TMoment::Create() -> FromString( $AValueBegin ) -> Get();

        $this -> End = empty( $AValueEnd )
        ? TMoment::Create() -> Now() -> Get()
        : TMoment::Create() -> FromString( $AValueEnd ) -> Get();

        $this -> Current    = ( $this -> Begin + $this -> End ) / 2 ;    /* The Current moment */
        $this -> Last       = $this -> Begin;   /* The Last moment */

        $this
        -> FocusPush()
        -> WinPopup()
        -> WinFull()
        -> DOMContent( $this -> GetTemplate( 'MomentPopupPusa.html' ))

        /* Bottom buttons */
        -> FocusChildren( 'BtnCancel' )
        -> EventFront( $this -> Clone() -> WinPopupClose() )

        -> FocusSibling( 'MenuPopup', 'BtnOk' )             -> EventThis( 'Ok' )
        -> FocusSibling( 'MenuPopup', 'BtnReset' )          -> EventThis( 'Reset' )

        /* Top buttons */
        -> FocusSibling( 'MenuPopup', 'BtnYearPrev' )       -> EventThis( 'YearPrev' )
        -> FocusSibling( 'MenuPopup', 'BtnMonthPrev' )      -> EventThis( 'MonthPrev' )
        -> FocusSibling( 'MenuPopup', 'BtnMonthNext' )      -> EventThis( 'MonthNext' )

        -> FocusSibling( 'MenuPopup', 'BtnYearNext' )       -> EventThis( 'YearNext' )
        -> FocusSibling( 'MenuPopup', 'BtnPeriodPrev' )     -> EventThis( 'PeriodPrev' )
        -> FocusSibling( 'MenuPopup', 'BtnPeriodNext' )     -> EventThis( 'PeriodNext' )
        -> FocusSibling( 'MenuPopup', 'BtnMonthCurrent' )   -> EventThis( 'MonthCurrent' )
        -> FocusSibling( 'MenuPopup', 'BtnWeekCurrent' )    -> EventThis( 'WeekCurrent' )

        -> FocusSibling( 'MenuPopup', 'BtnToday' )          -> EventThis( 'Today' )
        -> FocusSibling( 'MenuPopup', 'BtnMidnight' )       -> EventThis( 'Midnight' )
        -> FocusSibling( 'MenuPopup', 'BtnNow' )            -> EventThis( 'Now' )
        -> FocusSibling( 'MenuPopup', 'BtnNoon' )           -> EventThis( 'Noon' )

        -> FocusSibling( 'MenuPopup', 'Hour' ) -> FillNum( 24 )
        -> FocusSibling( 'MenuPopup', 'Minute' ) -> FillNum( 60 )
        -> FocusSibling( 'MenuPopup', 'Second' ) -> FillNum( 60 )

        -> Fill()
        -> FocusPop();

        return $this;
    }



    private function FillNum
    (
        int $ACount
    )
    {
        $HTML = THTML::Create();
        for( $i = 0; $i < $ACount; $i++ )
        {
            $HTML
            -> TagOpen( 'option' )
            -> SetAttr( 'value', $i )
            -> AddContent( $i )
            -> TagClose();
        }
        $this -> DOMContent( $HTML -> GetContent() );
        return $this;
    }



    /*
        Fill the calendar month before, month current, month next
        Backend
    */
    private function Fill()
    {
        $this
        -> FillMonth( 'TableLeft',      TMoment::Create() -> Set( $this -> Current ) -> AddMonth( -1 ))
        -> FillMonth( 'TableCenter',    TMoment::Create() -> Set( $this -> Current ) )
        -> FillMonth( 'TableRight',     TMoment::Create() -> Set( $this -> Current ) -> AddMonth( +1 ))
        ;

        $Last = TMoment::Create() -> Set( $this -> Last );

        $this
        -> FocusSibling( 'MenuPopup', 'Second' )
        -> DOMValue( $Last -> GetSecond() )
        -> FocusSibling( 'MenuPopup', 'Minute' )
        -> DOMValue( $Last -> GetMinute() )
        -> FocusSibling( 'MenuPopup', 'Hour' )
        -> DOMValue( $Last -> GetHour() )
        ;

        return $this;
    }



    /*
        Fill one month
        Backend
    */
    private function FillMonth
    (
        string $ATable,
        TMoment $AMoment
    )
    {
        /*Первый день в календаре*/
        $Begin      = $AMoment -> GetMonthBegin();
        $End        = $AMoment -> GetMonthEnd();
        $Current    = $AMoment -> GetMonthBegin() -> Dec( $Begin -> GetDayWeekNumber( 1 ) * TMoment::DAY );

        $HTML = THTML::Create();

        /*
            mn tu we th fr sa sn
             -  -  1  2  3  4  5
             6  7  8  9 10 11 12
        */
        for ( $y=1; $y<7; $y++ )
        {
            $HTML -> TagOpen( 'tr' );

            for ( $x=1; $x<8; $x++ )
            {
                $HTML -> TagOpen( 'td' );

                if
                (
                    $Current -> Get() >= $Begin -> Get() &&
                    $Current -> Get() < $End -> Get()
                )
                {
                    $HTML
                    -> AddContent( $Current -> GetDayNumber() )
                    -> SetAttr( 'data-value', $Current -> Get() )
                    -> SetAttr( 'class', $Current -> Get() >= $this -> Begin && $Current -> Get() < $this -> End ? 'Marked DayActive' : 'Unselected  DayActive' )
                    ;

                    /* Set class for current cell */
                    if( $Current -> GetDayBegin() -> Get() == TMoment::Create() -> Set( $this -> Last ) -> GetDayBegin() -> Get() )
                    {
                        $HTML -> SetAttr( 'class', 'Selected DayActive' );
                    }
                }
                $Current -> Add( TMoment::DAY );

                $HTML -> TagClose();
            }
            $HTML -> TagClose();
        }

        $this
        -> FocusSibling( 'MenuPopup', $ATable )
        -> FocusChildren( 'MonthAndYear' )
        -> DOMContent( $AMoment -> GetMonthName() . ' ' . $AMoment -> GetYear() )
        -> FocusParentFirst( $ATable )
        -> FocusChildren( $ATable . 'Body' )
        -> DOMContent( $HTML -> GetContent() )
        -> FocusChildren( 'DayActive' )
        -> EventThis( 'Click', $this -> Clone() -> ReadAttribute( 'AValue', 'data-value' ) )
        ;

        return $this;
    }



    /*
        Select Now
        Frontend method
    */
    public function Now()
    {
        $Now = TMoment::Create() -> Now();

        $this -> Current    = $Now -> Get();
        $this -> Last       = $Now -> Get();
        $this -> Begin      = $Now -> Get();
        $this -> End        = $Now -> Get();

        return $this -> Fill();
    }



    /*
        Select Now
        Frontend method
    */
    public function Noon()
    {
        $Now = TMoment::Create() -> Now() -> SetDayBegin() -> Add( 0.5 * TMoment::DAY );

        $this -> Current    = $Now -> Get();
        $this -> Last       = $Now -> Get();
        $this -> Begin      = $Now -> Get();
        $this -> End        = $Now -> Get();

        return $this -> Fill();
    }



    /*
        Select Now
        Frontend method
    */
    public function Midnight()
    {
        $Now = TMoment::Create() -> Now() -> SetDayBegin();

        $this -> Current    = $Now -> Get();
        $this -> Last       = $Now -> Get();
        $this -> Begin      = $Now -> Get();
        $this -> End        = $Now -> Get();

        return $this -> Fill();
    }



    /*
        Set current month
        Frontend
    */
    public function MonthCurrent()
    {
        $Moment = TMoment::Create() -> Set( $this -> Last );

        $this -> Begin   = $Moment -> SetMonthBegin() -> Get();
        $this -> End     = $Moment -> SetMonthEnd() -> Get();

        return $this -> Fill();
    }



    /*
        Set current Week
        Frontend
    */
    public function WeekCurrent()
    {
        $Moment = TMoment::Create() -> Set( $this -> Last );

        $this -> Begin   = $Moment -> SetWeekBegin( 1 ) -> Get();
        $this -> End     = $Moment -> SetWeekEnd( 1 ) -> Get();

        return $this -> Fill();
    }



    /*
        Previous month
        Frontend method
    */
    public function MonthPrev()
    {
        $this -> Current = TMoment::Create() -> Set( $this -> Current ) -> AddMonth( -1 ) -> Get();
        return $this -> Fill();
    }



    /*
        Next month
        Frontend method
    */
    public function MonthNext()
    {
        $this -> Current = TMoment::Create() -> Set( $this -> Current ) -> AddMonth( +1 ) -> Get();
        return $this -> Fill();
    }



    /*
        Previous month
        Frontend method
    */
    public function YearPrev()
    {
        $this -> Current = TMoment::Create()
        -> Set( $this -> Current )
        -> AddYear( -1 )
        -> Get();

        return $this -> Fill();
    }



    /*
        Next month
        Frontend method
    */
    public function YearNext()
    {
        $this -> Current = TMoment::Create()
        -> Set( $this -> Current )
        -> AddYear( +1 )
        -> Get();

        return $this -> Fill();
    }



    /*
        Next month
        Frontend method
    */
    public function Ok()
    {
        return $this
        -> WinPopupClose()
        -> FocusBodyChildren( $this -> IDBegin, TPusa::ID )
        -> FocusChildren( 'MomentValue' )
        -> DOMValue( TMoment::Create() -> Set( $this -> Begin ) -> ToStringODBC() )
        -> FocusBodyChildren( $this -> IDEnd, TPusa::ID )
        -> FocusChildren( 'MomentValue' )
        -> DOMValue( TMoment::Create() -> Set( $this -> End ) -> ToStringODBC() )
        ;
    }



    /*
        Next month
        Frontend method
    */
    public function Reset()
    {
        return $this
        -> WinPopupClose()
        -> FocusBodyChildren( $this -> IDBegin, TPusa::ID )
        -> FocusChildren( 'MomentValue' )
        -> DOMValue()
        -> FocusBodyChildren( $this -> IDEnd, TPusa::ID )
        -> FocusChildren( 'MomentValue' )
        -> DOMValue()
        ;
    }



    /*
        Frontend method
    */
    public function PeriodPrev()
    {
        $Delta = $this -> End - $this -> Begin;

        $this -> Begin      = $this -> Begin - $Delta;
        $this -> End        = $this -> End - $Delta;
        $this -> Current    = $this -> Begin;

        return $this -> Fill();
    }



    /*
        Frontend method
    */
    public function PeriodNext()
    {
        $Delta = $this -> End - $this -> Begin;

        $this -> Begin      = $this -> Begin + $Delta;
        $this -> End        = $this -> End + $Delta;
        $this -> Current    = $this -> Begin;

        return $this -> Fill();
    }


    /*
        Frontend method
    */
    public function Today()
    {
        $Now = TMoment::Create() -> Now();

        $this -> Current    = $Now -> Get();
        $this -> Last       = $Now -> Get();
        $this -> Begin      = $Now -> GetDayBegin() -> Get();
        $this -> End        = $Now -> GetDayEnd() -> Get();

        return $this -> Fill();
    }



    /*
        Click on calendar day
        Frontend method
    */
    public function Click
    (
        int $AValue
    )
    {
        if( empty( $this -> Last ))
        {
            $this -> Last = $AValue;
        }

        if( $AValue >= $this -> Last )
        {
            $this -> End = $AValue + TMoment::DAY;
        }

        if( $AValue < $this -> Last )
        {
            $this -> Begin = $AValue;
        }

        $this -> Last = $AValue;

        return $this -> Fill();
    }
}
