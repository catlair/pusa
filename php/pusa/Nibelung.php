<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

namespace catlair;

/*
    Модуль полезной нагрузки для сайт нибелунга
    Реализует АПИ для добавления даных и чтения сохраненных данных
*/

require_once ROOT . '/core/nibelung.php';
require_once ROOT . '/core/url.php';



class Nibelung extends TWebPusa
{
    /*
        Умолчальный построитель страницы
    */
    public function page()
    {
        $this -> setContent( '<a href="">Nibelung</a>' );
        return $this;
    }



    /*
        Эндпоинт
        Добавляет новое значения в хранилище нибелунга
    */
    public function set
    (
        string $AValue  = '',      /* Значение для добавления */
        string $AKey    = null,    /* Необязательный идентификатор для добавления */
        string $ASecure = null     /* Необязательный пароль клиента */
    )
    {
        /* Создание идентификатор ссылки */
        $Key = empty( $AKey ) ? $AValue : $AKey;

        /* Получение имени файла по идентификатору */
        $File = $this -> getFile( $Key, $ASecure );

        $this
        -> setOutcome( 'key', $AKey )                                        /* Добавление идентификатора */
        -> setOutcome( 'hash', Nibelung::getHash( $Key, $ASecure ))           /* Получение hash */
        -> setOutcome( 'info_url', $this -> getInfoLink( $Key, $ASecure ))   /* Создание ссылки по которой будет доступна информация */
        -> setOutcome( 'get_url', $this -> getGetLink( $Key, $ASecure ))     /* Создание ссылки по которой будет доступна информация */
        -> setOutcome( 'size_byte', strlen( $AValue ))                      /* Добавление размера файла для информации */
        ;

        if( !@clCheckPath( dirname( $File )))
        {
            $this -> setResult( 'storage_path_not_found' );
        }
        else
        {
            /* Сохранение контента в файл */
            if( !file_put_contents( $File, $AValue ))
            {
                $this -> setResult( 'file_write_error' );
            }
        }

        return $this;
    }



    /*
        Эндпоинт
        Возвращает ранее сохраненный контент
    */
    public function info
    (
        string $AHash     = null, /* Необязательный Hash */
        string $AKey       = null, /* Необязательный идентификатор ключа */
        string $ASecure   = null  /* Необязательный пароль*/
    )
    {
        $Value = $this -> readFile( $this -> getFile( $AKey, $ASecure, $AHash ));
        $Json = json_decode( $Value );

        return $this
        -> setOutcome( 'get_url', $this -> getGetLink( $AKey, $ASecure, $AHash  ))
        -> setOutcome( 'value', empty( $Json ) ? $Value : $Json);
    }



    /*
        Эндпоинт
        Возвращает для получения ранее сохраненного контента
    */
    public function get
    (
        string $AHash     = null, /* Необязательный Hash */
        string $AKey      = null, /* Необязательный идентификатор ключа */
        string $ASecure   = null  /* Необязательный пароль*/
    )
    {
        return $this
        -> setContent
        (
            $this -> readFile
            (
                $this -> getFile( $AKey, $ASecure, $AHash )
            )
        );
    }



    /**************************************************************************
        Приватные свойства
    */

    /*
        Возвращает имя файла по идентификатор
    */
    private function getFile
    (
        string $AKey    = null, /* Необязательный идентификатор ключа */
        string $ASecure = null, /* Необязательный пароль*/
        string $AHash   = null  /* Необязательный Hash */
    )
    {
        return $this
        -> getApp()
        -> getSitePath
        (
            null,
            'store' . clScatterName /* Создает путь типа a/b/c/abcde */
            (
                clFileControl       /* Проверка на включение мусора в файловое имя */
                (
                    Nibelung::getHash( $AKey, $ASecure, $AHash )
                )
            )
        );
    }



    /*
        Возвращает контент файла по имени файла
    */
    private function readFile
    (
        string $AFileName
    )
    {
        return @file_get_contents( $AFileName );
    }



    private function getInfoLink
    (
        string $AKey,
        string $ASecure = null,
        string $AHash   = null
    )
    {
        return Nibelung::create
        (
            $this
            -> getApp()
            -> getCurrentURL()
            -> toString()
        )
        -> getInfoLink( $AKey, $ASecure, $AHash );
    }



    private function getGetLink
    (
        string $AKey      = null,
        string $ASecure   = null,
        string $AHash     = null
    )
    {
        return Nibelung::create
        (
            $this
            -> getApp()
            -> getCurrentURL()
            -> toString()
        )
        -> getGetLink( $AKey, $ASecure, $AHash );
    }
}
