<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/


namespace catlair;

/*
    Select popup interface for Pusa
*/

require_once 'Select.php';

class SelectDB extends Select
{
    /*
        Load list of records
    */
    protected function Load
    (
        string  $AFindString    = '',   /* Find string for list */
        string  $AIDType        = '',   /* Type of record identifier */
        int     $ACurrentRecord = 0     /* Current record number */
    )
    {
        return $this -> GetWeb() -> GetDatasource()
        -> ProcJSON
        (
            'DescriptSelect',
            [
                'Caption'       => $AFindString,
                'IDType'        => $AIDType,
                'CountRecord'   => 100,
                'CurrentRecord' => $ACurrentRecord
            ]
        )
        -> GetDataset();
    }



    /*
        Return caption by id
    */
    protected function Caption
    (
        string $AID = null
    )
    {
        return
        $this -> GetWeb() -> GetDatasource()
        -> ProcJSON
        (
            'CaptionByID',
            [
                'ID'        => $AID
            ]
        )
        -> GetDataset()
        -> GetParams()
        -> GetString( 'Caption', $AID );
    }
}
