<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

/*
    still@itserv.ru
*/

namespace catlair;


/*
    The Pusa Constructor
*/

class Constructor extends TWebPusa
{
    public function CreateDomain()
    {
        $ClassName = $this -> GetString( 'IDDomainNew' );
        return $this;
    }



    public function DeleteDomain()
    {
        $ClassName = $this -> GetString( 'IDDomain' );
        return $this;
    }



    public function CreateSite()
    {
        $ClassName = $this -> GetString( 'IDSiteNew' );
        return $this;
    }



    public function DeleteSite()
    {
        $ClassName = $this -> GetString( 'IDSite' );
        return $this;
    }



    public function CreateClass()
    {
/*        $ClassName = TWeb::GetServerCLI( null, 'ClassNew', 'Chouse new class name', 'MyClass', true ); */
        return $this;
    }



    public function CreateMethod()
    {
        $ClassName = $this -> GetString( 'MethodNew' );
        return $this;
    }
}

