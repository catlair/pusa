<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/


namespace catlair;

/*
    Select popup Pusa controller
*/


class Select extends TWebPusa
{
    private $OnChangeClass    = null;
    private $OnChangeMethod   = null;
    private $ID               = null;



    /***************************************************************************
        Private internal methods
    */

    protected function OnBeforeRun
    (
        string $AID             = null,
        string $AChangeClass    = null,
        string $AChangeMethod   = null
    )
    {
        $this -> ID               = $AID;
        $this -> OnChangeClass    = $AChangeClass;
        $this -> OnChangeMethod   = $AChangeMethod;

        return $this;
    }



    /*
        Activate children Popup elements by class name
    */
    public function Activate
    (
        string $AContainer,  /* Containr class */
        string $AIDType     = '',
        string $AName       = '',
        array  $AOnChange   = []
    )
    {
        $this -> ID = clGUID();
        if( count( $AOnChange ) == 2 )
        {
            $this -> OnChangeClass = $AOnChange[ 0 ];
            $this -> OnChangeMethod = $AOnChange[ 1 ];
        }

        $this
        -> FocusPush()
        -> FocusChildren( $AContainer )
        -> DOMID( $this -> ID )

        /* Set ID for container */
        -> DOMClassAdd( 'Select' )

        /* Hidden input value */
        -> DOMContent( $this -> GetTemplate( 'ui/select/Select.html' ))
        -> FocusChildren( 'SelectValue' )
        -> DOMName( $AName )

        /* Caption value */
        -> FocusSibling( 'Select', 'SelectCaption' )
        -> DOMName( 'Caption' . $AName )
        -> DOMValue( '' )

        /* Hide the list */
        -> ListHide()

        /* Press ArrowDown for popup */
        -> Event
        (
            $this -> GetClass(), 'KeyUp',
            $this -> Clone()
            -> ReadStatic( 'AIDType', $AIDType )
            -> ReadStatic( 'AID', $this -> ID )
            -> ReadStatic( 'AChangeClass', $this -> OnChangeClass )
            -> ReadStatic( 'AChangeMethod',  $this -> OnChangeMethod )
            -> ReadProperty( 'AFindString', 'value' )
            -> ReadEvent( 'AKey', 'key' )
            ,
            TPusaCore::KEY_UP,
            200
        )

        /* Press ArrowDown for popup */
        -> Event
        (
            $this -> GetClass(), 'Dropdown',
            $this -> Clone()
            -> ReadStatics
            ([
                'AIDType'       => $AIDType,
                'AID'           => $this -> ID,
                'AChangeClass'  => $this -> OnChangeClass,
                'AChangeMethod' => $this -> OnChangeMethod
            ]),
            TPusaCore::DBL_CLICK
        )

        /* Popup button */
        -> FocusParents( 'SelectInput' )
        -> FocusChildren( 'SelectButton' )
        -> Event
        (
            $this -> GetClass(), 'Dropdown',
            $this -> Clone()
            -> ReadStatic( 'AIDType', $AIDType )
            -> ReadStatic( 'AID', $this -> ID )
            -> ReadStatic( 'AChangeClass', $this -> OnChangeClass )
            -> ReadStatic( 'AChangeMethod',  $this -> OnChangeMethod ),
            TPusaCore::CLICK
        )

        -> FocusParents( 'Select' )
        -> FocusChildren( 'CancelButton' )
        -> Event
        (
            $this -> GetClass(), 'ListHide',
            $this -> Clone()
            -> ReadStatic( 'AID', $this -> ID ),
            TPusaCore::CLICK
        )
        -> FocusPop()
        ;

        return $this;
    }



    /*
        Reset value
    */
    public function ResetValue()
    {
        return $this
        -> OnChange()
        -> FocusPush()
        -> FocusBodyChildren(  $this -> ID, TPusaCore::ID )
        -> FocusChildren( 'SelectValue' )
        -> DOMValue( '' )
        -> FocusPop();
    }



    /*
        Reset caption
    */
    public function ResetCaption()
    {
        return $this
        -> FocusPush()
        -> FocusBodyChildren( $this -> ID, TPusaCore::ID )
        -> FocusChildren( 'SelectCaption' )
        -> DOMValue()
        -> FocusPop();
    }




    /*
        SetVal by ID
    */
    public function SetVal
    (
        string $AValue  = null, /* Value for setting */
        string $AClass  = null  /* Class name */
    )
    {
        $this -> FocusPush();

        if( empty( $AClass ))
        {
            $this -> FocusBodyChildren( $this -> ID, TPusaCore::ID );
        }
        else
        {
            $this -> FocusChildren( $AClass );
        }

        $this
        -> FocusChildren( 'SelectValue' )
        -> DOMValue( $AValue )

        -> FocusParents( 'Select' )
        -> FocusChildren( 'SelectCaption' )
        -> DOMValue( $this -> Caption( $AValue ))
        -> OnChange( $AValue )

        -> FocusPop()
        ;

        return $this;
    }



    /*
        Hide record list
    */
    public function ListHide()
    {
        return $this
        -> FocusPush()
        -> FocusBodyChildren( $this -> ID, TPusaCore::ID )
        -> FocusChildren( 'SelectPopup' )
        -> DOMHide()
        -> FocusPop()
        ;
    }



    /*
        Choise element
    */
    public function Choise
    (
        string $Value = ''
    )
    {
        return $this
        -> SetVal( $Value )
        -> ListHide()
        ;
    }



    /*
        Public method
    */
    protected function Refresh
    (
        string $AFindString  = '',
        string $AIDType        = ''
    )
    {
        /* Build the record */

        $Dataset = $this -> Load( $AFindString, $AIDType, 0 );

        switch( $Dataset -> GetCount() )
        {
            case 0:
                $this
                -> FocusParents( 'Select' )
                -> FocusChildren( 'SelectPopup' )
                -> DOMShow()
                -> FocusChildren( 'SelectList' )
                -> DOMContent( )
                -> FocusSibling( 'SelectPopup', 'CountRecord' )
                -> DOMContent(  'No results' )
                ;
            break;
            default:
                $this
                -> FocusSibling( 'Select', 'SelectPopup' )
                -> DOMShow()
                -> FocusChildren( 'SelectList' )
                /* Set uploaded records */
                -> DOMProp([ 'CurrentRecord' => $Dataset -> GetCount() ])

                -> DOMContent()
                -> Event
                (
                    'SelectDB', 'Scroll',
                    $this -> Clone()
                    -> ReadStatic( 'AIDSelect', $this -> ID )
                    -> ReadStatic( 'AIDType', $AIDType )
                    -> ReadStatic( 'AFindString', $AFindString )
                    -> ReadProperty( 'AScrollTop', 'scrollTop' )
                    -> ReadProperty( 'AScrollHeight', 'scrollHeight' )
                    -> ReadProperty( 'AClientHeight', 'clientHeight' )
                    -> ReadProperty( 'ACurrentRecord', 'CurrentRecord' ),
                    TPusaCore::SCROLL,
                    300
                )
                -> BuildRecords( $Dataset, $Dataset -> GetCount() );
            break;
        }

        return $this;
    }



    /*
    */
    public function Scroll
    (
        string  $AFindString    = '',
        string  $AIDType        = '',
        int     $ACurrentRecord = 0,
        int     $AScrollTop     = 0,
        int     $AScrollHeight  = 0,
        int     $AClientHeight  = 0
    )
    {
        if( $AScrollTop + $AClientHeight * 2 > $AScrollHeight )
        {
            $Dataset = $this -> Load( $AFindString, $AIDType, $ACurrentRecord );
            $this -> DOMProp([ 'CurrentRecord' => $ACurrentRecord + $Dataset -> GetCount() ]);
            $this -> BuildRecords( $Dataset, $ACurrentRecord + $Dataset -> GetCount() );
        }
        return $this;
    }



    /*
        Public method
    */
    private function BuildRecords
    (
        TDataset $ADataset,
        int $ACountRecords
    )
    {
        return $this
        -> FocusPush()
        -> FocusSibling( 'SelectPopup', 'CountRecord' )
        -> DOMContent( $ACountRecords )
        -> FocusPop()
        -> Loop
        (
            $this -> Clone()
            -> DOMCreate( 'button' )
            -> DOMAttr([ 'type' => 'button' ])
            -> DOMClass( 'Record Unselected' )
            -> DOMTemplate( 'ui/select/Record.html' )
            -> Event
            (
                $this -> GetClass(),
                'Choise',
                $this -> Clone()
               -> ReadStatic( 'Value', '%IDDescript%' )
               -> ReadStatic( 'AID', $this -> ID )
               -> FocusParentFirst( 'Select' )
               -> ReadStatic( 'AChangeClass', $this -> OnChangeClass )
               -> ReadStatic( 'AChangeMethod', $this -> OnChangeMethod )
            ),
            $ADataset
        );
    }








    /**************************************************************************
        Events
    */


    /*
        Key up event
    */
    public function KeyUp
    (
        string $AIDType,
        string $AFindString,
        string $AKey
    )
    {
        switch( $AKey )
        {
            case 'ArrowLeft':
            case 'ArrowRight':
            case 'ArrowUp':
            break;
            case 'Escape':
                $this
                -> ResetCaption()
                -> ResetValue( $AChangeClass, $AChangeMethod )
                -> ListHide();
            break;
            default:
                $this
                -> ResetValue( $AChangeClass, $AChangeMethod )
                -> Refresh( $AFindString, $AIDType )
                ;
            break;
        }
        return $this;
    }



    /*
        Blur event
    */
    public function Blur
    (
        string $AValue
    )
    {
        if( empty( $AValue ))
        {
            $this
            -> ResetCaption()
            -> ListHide();
        }
        return $this;
    }



    /*
        Key up event
    */
    public function Dropdown
    (
        string $AIDType,
        string $AKey
    )
    {
        switch( $AKey )
        {
            case 'ArrowLeft':
            case 'ArrowRight':
            case 'ArrowUp':
            case 'ArrowDown':
            break;
            default:
                $this
                -> Refresh( '', $AIDType )
                ;
            break;
        }
        return $this;
    }



    /*
        Onchange event from front and back
    */
    private function OnChange
    (
        string $AValue = null /* New value */
    )
    {
        if
        (
            !empty( $this -> OnChangeClass ) &&
            !empty( $this -> OnChangeMethod ) &&
            $this -> OnChangeMethod != 'null'
        )
        {
            $Method = ( $this -> OnChangeMethod );
            $this -> Mutate( $this -> OnChangeClass )
            -> $Method( $AValue )
            -> Switch( $this );
        }
        return $this;
    }



    /*
        Set Class and Method for OnChange event
    */
    public function SetOnChange
    (
        string  $AChangeClass    = null,
        string  $AChangeMethod   = null
    )
    {
        $this -> OnChangeClass  = $AChangeClass;
        $this -> OnChangeMethod = $AChangeMethod;

        return $this;
    }



    public function Enable()
    {
        return $this -> SetProperty( 'disabled', null );
    }



    public function Disable()
    {
        return $this -> SetProperty( 'disabled', 'disabled' );
    }

}
