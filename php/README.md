# Catlair PHP code


```mermaid
flowchart LR

TPusa --> TPusaCore
TPusaCore --> TResult
TDaemon --> TResult
TWebPusa --> TPusa
TWeb --> TBuilder
TBuilder --> TLog
TLog --> TResult
TCMS --> TWeb
TController --> TParams
TParams --> TResult
TPayload --> TResult
TGIT --> TShell --> TResult
THTMLBuilder --> THTML --> TText --> TParams

TSSL --> TResult
TConfig --> TParams
TDeploy --> TResult

TStream --> TResult
TEchoStream --> TStream
TFileStream --> TStream

THTMLCode --> THTML
TInterval --> TResult
TIntervals --> TResult
TLoader --> TResult
TMarkdown --> THTMLCode
TPDF --> TShell
TProcess --> TResult

TNibelung --> TResult

TTag --> TText
TVocab --> TResult
TQRCode --> TShell
TSpeech --> TResult

TCMSController --> TWebController

 TCMSDataset --> TParams
 TCMSDeploy --> TWebDeploy
 TCMSParams --> TParams
 TCMSPDF --> TPDF
```




