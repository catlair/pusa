<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

namespace catlair;


/*
    WebDB
    - Content builder
    - Datasources

    $this -> GetIncome() -> Return income parameters with $_GET $_POST $CLI parameters

    still@itserv.ru
*/


/* Core library */
require_once( ROOT . '/web/web.php' );
/* Datasource */
require_once( ROOT . '/datasource/data.php' );


class TWebDB extends TWeb
{
    const DATASOURCE_DEFAUT     = 'default';

    public $FDatasourceList     = [];               /* Array of datasource */



    function __construct( &$ADomain )
    {
        parent :: __construct( $ADomain );
        $this -> SetIDSiteDatasource( $ADomain -> GetString( 'IDSiteDatasource', $this -> IDSite ));
        $this -> Trace() -> Param( 'Datasource', $this -> IDSiteDatasource );
    }



    /**************************************************************************
        Datasourses
    */

    /*
        Return cache name
    */
    private function GetDatasourceCacheName( $AIDSite, $AName )
    {
        /* Костыль для подмены имени базы на имя базы из конфигурации домена */
        if( $AIDSite == $this -> IDSite ) $AIDSite = $this -> GetIDSiteDatasource();
        $AName = empty( $AName ) ? 'default' : $AName;
        /* Create and return key for datasource */
        return  $AIDSite . '_' . $AName . '_' . $AIDSite;
    }



    /*
        Datasource initialization for site $AIDSite:string and name $AName:string
    */
    public function GetDatasource
    (
        $AName      = null, /* Database name */
        $AIDSite    = null, /* Site */
        $AOpen      = true  /* Database will be open with true */
    )
    {
        if( empty( $AIDSite )) $AIDSite = $this -> IDSite;
        if( empty( $AName )) $AName = 'default';

        $CacheName = $this -> GetDatasourceCacheName( $AIDSite, $AName );

        if( array_key_exists( $CacheName, $this -> FDatasourceList ))
        {
            /* Return exists datasource from list */
            $Data = $this -> FDatasourceList[ $CacheName ];
        }
        else
        {
            /* Create new datasource from config file */
            $File = $this -> SiteDatasourceConfigPath( $AIDSite, $AName );

            $Data = TData::CreateFromConfig
            (
                $this,
                $File,
                /* Request active datasource from CLI or $_SERVER*/
                self::GetServerCLI
                (
                    'CATLAIR_DATA_SOURCE_ACTIVE',
                    'DatasourceActive',
                    'Input the active datasource form "' . $File . '"',
                    null,
                    false
                )
            );
            $this -> FDatasourceList[ $CacheName ] = $Data;
        }

        if( !empty( $Data ))
        {
            if( $Data -> IsOk() && !$Data -> GetOpened() )
            {
                $Data -> Open( $AOpen, $this -> IsCLI() ? 86400 : 60 );
            }
            $Data -> ResultTo( $this );
        }

        return $Data; // != null && $Data -> IsOk() ? $Data : null;
    }



    /*
        Datasource initialization for site $AIDSite:string and name $AName:string
    */
    public function DatasourceFree
    (
        $AIDSite,
        $AName = null
    )
    {
        $CacheName = $this -> GetDatasourceCacheName( $AIDSite, $AName );
        if ( array_key_exists( $CacheName, $this -> FDatasourceList ))
        {
            /* Return exists datasource from list */
            $Datasource = $this -> FDatasourceList[ $CacheName ];
            $Datasource -> Close();
            /* Remove datasourece from cache */
            unset( $this -> FDatasourceList[ $CacheName ] );
        }
        return $this;
    }



    /*
        Close all datasources
    */
    public function DatasourcesClose()
    {
        foreach( $this -> FDatasourceList as $Datasource )
        {
            $Datasource -> Close();
        }
        return $this;
    }



    /*
        Set datasource site
    */
    public function SetIDSiteDatasource( $AValue )
    {
        $this -> IDSiteDatasource = $AValue;
        return $this;
    }



    /*
        Set datasource site
    */
    public function GetIDSiteDatasource()
    {
        return empty( $this -> IDSiteDatasource ) ? $this -> IDSite : $this -> IDSiteDatasource;
    }



    /*
        Begin SQL Transaction for Datasource
    */
    public function &TranStart( $APrm = [])
    {
        return $this -> TranAction('TranStart', $APrm);
    }



    /*
        Commit SQL Transaction for Datasource
    */
    public function &TranCommit( $APrm = [])
    {
        return $this -> TranAction('TranCommit', $APrm);
    }



    /*
        Rollback SQL Transaction for Datasource
    */
    public function &TranRollback( $APrm = [])
    {
        return $this -> TranAction('TranRollback', $APrm);
    }



    /*
        Universal work with SQL Transaction for Datasource
    */
    public function &TranAction
    (
        $AAction,
        $APrm = []
    )
    {
        $IDSite = array_key_exists( 'IDSite', $APrm ) ? $APrm[ 'IDSite' ] : $this -> Session -> GetSite();

        $DatasourceName = ( array_key_exists( 'DatasourceName', $APrm))
        ? $APrm[ 'DatasourceName' ]
        : '';

        $Datasource = $this -> GetDatasource( $IDSite, $DatasourceName );

        $APrm = array( json_encode( $APrm ));

        if ( $Datasource == null )
        {
            $this -> SetCode( 'DatasourceNotFound' );
        }
        else
        {
            /* call method in Datacore */
            $Datasource -> $AAction();
        }

        return $this;
    }



    /*
        Return Datasource config file
    */
    public function SiteDatasourceConfigPath
    (
        string  $AIDSite    = '',
        string  $AName      = 'default',
        string  $ARoot      = ROOT
    )
    {
        if( empty( $AIDSite ))
        {
            $AIDSite = $this -> GetIDSite();
        }
        return clConfigFile( $AIDSite,  'datasource/' . $AName . '.json', $ARoot );
    }


}
