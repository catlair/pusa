<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

/*
    Catlair utils controller
*/

namespace catlair;

require_once( ROOT . "/core/utils.php" );
require_once( ROOT . "/cms/descript/file.php" );
require_once( ROOT . "/web/web_file.php" );
require_once( ROOT . "/web/web_tools.php" );


class Utils extends TCMSController
{
    /*
        Return guid
    */
    public function &GetGUID()
    {
        return
        $this -> SetOk() -> SetContent( clGUID() );
    }



    /*
        Pass
    */
    public function &GetPass()
    {
        return
        $this -> SetOk() -> SetContent( clRndID( '16', 'QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm1234567890!-_=+@#$*.' ) );
    }



    /*
        Store file to web
    */
    public function &FileToWeb()
    {
        $ID = $this -> GetIncome( 'ID' );
        $File = new TFile();
        $File
        -> Prepare( $ID, $this -> GetSite(), $this -> GetLang() )
        -> DatasourceToFile( TWebFile::Create( $this -> Log, $ID ) -> FileNameWrite );

        return $this;
    }
}
