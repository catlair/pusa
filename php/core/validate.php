<?php

namespace catlair;

class TValidate
{
    public function IsEmail( $AValue )
    {
        return filter_var( $AValue, FILTER_VALIDATE_EMAIL );
    }
}
