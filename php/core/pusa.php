<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

namespace catlair;

/*
    Addition methods for the TPusaPusa.

    https://gitlab.com/catlair/pusa/-/blob/main/site/pusa/src/language_default/man/pusa_php_methods.md

    Important!!!
    The purpose of this class is to minify HTML and JS entities in a project.
    When Pusa will becomes the standard protocol for the browser, the users application should not change.
*/

require_once( ROOT . '/core/pusa_core.php' );


class TPusa extends TPusaCore
{
    const SCROLL_BEGIN          = '0';
    const SCROLL_END            = '1';


    /**************************************************************************
        Browser operations
    */


    /*
        Open URL in browser new window
    */
    public function OpenURL
    (
        string $AURL,               /* URL for changing in brouser */
        $ATarget = self::CURRENT    /* Target for new window _blank, _self ets  */
    )
    {
        if( $AURL == $this -> URL && $ATarget == self::CURRENT )
        {
            $this
            -> FocusPush()
            -> Focus( TPusaCore::WINDOW )
            -> FocusProperty( 'location' )
            -> Call( 'reload' )
            -> FocusPop();
        }
        else
        {
            $this
            -> FocusPush()
            -> Focus( TPusaCore::WINDOW )
            -> Call( 'open', [ $AURL, $ATarget ])
            -> FocusPop();
        }
        return $this;
    }



    /*
        Setting the URL without loading
    */
    public function ChangeURL
    (
        string $AURL
    )
    {
        return $this
        -> Focus( TPusaCore::WINDOW )
        -> FocusProperty( 'history' )
        -> Call
        (
            'pushState',
            [
                null,
                null,
                $AURL
            ]
        );
    }



    /*
        Setting the URL without loading
    */
    public function ReloadPage()
    {
        return $this
        -> OpenURL( $this -> URL -> ToString() );
    }



    /**************************************************************************
        Focus
    */

    /*
        Return filter array by selector name and serach string
    */
    static private function SelectorToFilter( $ASelector, $ASearch )
    {
        switch( $ASelector )
        {
            case self::CLASS_NAME:
                $Result = [ TOperator::IN, $ASearch, '@className' ];
            break;
            case self::TAG:
                $Result = [ TOperator::EQUAL, strtoupper( $ASearch ), '@tagName' ];
            break;
            case self::ID:
                $Result = [ TOperator::EQUAL, $ASearch, '@id' ];
            break;
            case self::NAME:
                $Result = [ TOperator::EQUAL, $ASearch, '@name' ];
            break;
            default:
                $Result = [];
            break;
        }
        return $Result;
    }



    /*
        Selecting all childs of focused elements and set focus to them.
    */
    public function FocusChildren
    (
        string $ASearch   = null,               /* Name of selector */
        string $ASelector = self::CLASS_NAME    /* Type of selector CLASS ID NAME TAG */
    )
    {
        return $this -> Focus( self::CHILDS, self::SelectorToFilter( $ASelector, $ASearch ));
    }



    /*
        Selecting all parents of focused elements and set focus to them.
    */
    public function FocusParents
    (
        string $ASearch   = null,               /* Name of selector */
        string $ASelector = self::CLASS_NAME    /* Type of selector CLASS ID NAME TAG */
    )
    {
        return $this -> Focus( self::PARENTS, self::SelectorToFilter( $ASelector, $ASearch ));
    }



    /*
        Set focus at parents elements of focused elements
    */
    public function FocusParent()
    {
        return $this -> Focus( self::PARENT );
    }



    public function FocusChildrenNative
    (
        $ASearch   = null,               /* Name of selector */
        $ASelector = self::CLASS_NAME    /* Type of selector CLASS ID NAME TAG */
    )
    {
        return $this -> Focus( self::CHILDS_OF_THIS,  self::SelectorToFilter( $ASelector, $ASearch ));
    }



    public function FocusParentFirst
    (
        $ASearch   = null,               /* Name of selector */
        $ASelector = self::CLASS_NAME    /* Type of selector CLASS ID NAME TAG */
    )
    {
        return $this -> Focus( self::PARENT_FIRST,  self::SelectorToFilter( $ASelector, $ASearch ));
    }



    /*
        Set browser focus at event (SELF) element
    */
    public function FocusSelf()
    {
        return $this
        -> Focus( self::SELF );
    }



    /*
        Set focus at document.body
    */
    public function FocusBody()
    {
        return $this
        -> Focus( self::BODY );
    }



    /*
        Set focus to children of body
    */
    public function FocusBodyChildren
    (
        string $AChild = null,               /* Name of selector */
        string $ASelector = self::CLASS_NAME    /* Type of selector CLASS ID NAME TAG */
    )
    {
        return $this
        -> FocusBody()
        -> FocusChildren( $AChild, $ASelector );
    }



    /*
        Set focus to sibling
    */
    public function FocusSibling
    (
        string $AParent,
        string $AChild,              /* Name of selector */
        $ASelector = self::CLASS_NAME   /* Type of selector CLASS ID NAME TAG */
    )
    {
        return $this
        -> FocusParentFirst( $AParent, $ASelector )
        -> FocusChildren( $AChild, $ASelector );
    }



    /*
        Set focus at document.body
    */
    public function FocusWindow()
    {
        return $this
        -> Focus( self::WINDOW );
    }



    /**************************************************************************
        Extend Event methods
    */


    /*
        Event for for current object
    */
    public function EventThis
    (
        string      $AMethod    = null,     /*  2 Pusa method in class */
        TPusaCore   $APusa      = null,     /*  3 Pusa Commands*/
        string      $AEvent     = null,     /*  4 Event constant with JS event name */
        int         $ATimeout   = 0,        /*  5 Timeot in milliseconds */
        bool        $ACatch     = false,    /*  6 Catch the even on this object. JS will be call stopPropagation() */
        array       $AFilter    = null      /*  7 Event will be call only for values Key = Value */
    )
    {
        return $this -> Event
        (
            $this -> GetIDOrClass(),
            $AMethod,
            $APusa,
            $AEvent,
            $ATimeout,
            $ACatch,
            $AFilter
        );
    }



    /*
        Event for DOM element
        It will call JS addEventListener with Pusa commands for callback.
        Commands will execute on front side without call on back.
    */
    public function EventFront
    (
        $APusa,                                 /* TPusaCore-based object with commands for execution */
        string  $AEvent     = self::CLICK,      /* JS event name */
        bool    $ACatch     = false,            /* Catch the even on this object. JS will be call stopPropagation() */
        array   $AFilter    = null              /* Event will be call only for values Key = Value */
    )
    {
        return $this
        -> Event
        ([
            'Event'     => $AEvent,
            'Catch'     => $ACatch,
            'Commands'  => $APusa -> GetCommands(),
            'Filter'    => $AFilter
        ]);
    }



    /*
        Event for DOM element
        It will call JS addEventListener with JS commands.
        Commands will execute on front side without call on back.
        WARNING: Use only after exhausting all other possibilities.
    */
    public function EventJS
    (
        string  $AJS        = '',               /* JS code for callback */
        string  $AEvent     = self::CLICK,      /* JS event name */
        string  $ATarget    = self::TARGET_DOM, /* Target for event as TPusaCore::TARGET_* dom, window, document */
        bool    $ACatch     = false             /* Catch the even on this object. JS will be call stopPropagation() */
    )
    {
        return $this
        -> Event
        ([
            'Event'     => $AEvent,
            'Target'    => $ATarget,
            'Catch'     => $ACatch,
            'JS'        => $AJS
        ]);
    }



    /*
        Event for Focus elements
    */
    public function EventOnEnter
    (
        string      $AClass,            /*  1 Pusa class controller for call */
        string      $AMethod    = null, /*  2 Pusa method in class */
        TPusaCore   $APusa      = null  /*  3 Pusa Commands*/
    )
    {
        return $this
        -> Event
        (
            $AClass,
            $AMethod,
            $APusa,
            TPusaCore::KEY_UP,
            0,
            false,
            [ '=', '@key', 'Enter' ]
        );
    }


    /**************************************************************************
        DOM operations
    */

    /*
        Events trap for DOM element
        Events will be catch on this element and will be not propagation
        Look at JS topPropagation()
    */
    public function DOMEventTrap
    (
        string $AEvent  = TPusaCore::CLICK, /* JS event name */
        bool $ACatch    = true              /* Catch the even on this object. JS will be call stopPropagation() */
    )
    {
        return $this
        -> Event
        ([
            'Event' => $AEvent,
            'Catch' => $ACatch
        ]);
    }



    /*
        Change attributes for DOM elements
    */
    public function DOMAttr
    (
        array $AValues = [] /* Named value array of the HTML attributes [ "attribute" => "Value", ... ] */
    )
    {
        foreach( $AValues as $Key => $Value )
        {
            $this -> Call
            (
                'setAttribute',
                [
                    $Key,
                    $Value
                ]
            );
        }
        return $this;
    }



    /*
        Change styles for the focused DOM elements
    */
    public function DOMStyle
    (
        array $AValues = [] /* Named values array of the HTML styles [ "style-name" => "Value", ... ] */
    )
    {
        return $this
        -> FocusPush()
        -> FocusProperty( 'style' )
        -> SetProperty( $AValues )
        -> FocusPop();
    }



    /*
        Change property for DOM elements
    */
    public function DOMProp
    (
        array $AValues = []
    )
    {
        return $this -> SetProperty( $AValues );
    }



    /*
        Settings value field for the focused DOM elements
    */
    public function DOMValue
    (
        string $AValue = null  /* Value content for HTML element */
    )
    {
        return $this
        -> SetProperty([ 'value' => $AValue ]);
    }



    /*
        Setting the content (innerHTML) for the focuser DOM elements
    */
    public function DOMContent
    (
        $AContent = '' /* Текстовый контент */
    )
    {
        return $this
        -> SetProperty([ 'innerHTML' => $AContent ]);
    }



    /*
        Clear content in elements
    */
    public function DOMClear()
    {
        return $this
        -> SetProperty([ 'innerHTML' => '' ]);
    }



    public function DOMLink( $AValue )
    {
        return $this
        -> DOMAttr([ 'href' => $AValue ]);
    }



    public function DOMClass
    (
        string $AValue = null
    )
    {
        return $this
        -> DOMAttr([ 'class' => $AValue ]);
    }



    public function DOMID
    (
        string $AValue = null
    )
    {
        return $this
        -> DOMAttr([ 'id' => $AValue ]);
    }



    public function DOMName
    (
        string $AValue = null
    )
    {
        return $this
        -> DOMAttr([ 'Name' => $AValue ]);
    }



    /*
        Add new className for element
    */
    public function DOMClassAdd( $AValue )
    {
        if( !empty( $AValue ))
        {
            switch( gettype( $AValue ))
            {
                case 'string':
                    $this
                    -> FocusPush()
                    -> FocusProperty( 'classList' )
                    -> Call( 'add', [ $AValue ])
                    -> FocusPop();
                break;
                case 'array':
                    foreach( $AValue as $Record )
                    {
                        $this -> DOMClassAdd( $Record );
                    }
                break;
            }
        }
        return $this;
    }



    /*
        Remove className from focused DOM elements
    */
    public function DOMClassRemove( $AValue )
    {
        return $this
        -> FocusPush()
        -> FocusProperty( 'classList' )
        -> Call( 'remove', [ $AValue ])
        -> FocusPop();
    }



    /*
        Scroll parent element for view focus element
    */
    public function DOMView()
    {
        return $this
        -> Call
        (
            'scrollIntoView',
            [[
                'behavior'  => 'smooth',
                'block'     => 'nearest',
                'inline'    => 'start'
            ]]
        );
    }



    /*
        Horizontal scrolling of inner content in focus elements
    */
    public function DOMScrollHorizon
    (
        $APlace
    )
    {
        return $this
        -> SetProperty([ 'scrollLeft' => $APlace ]);
    }



    /*
        Vertical scrolling of inner content in focus elements
    */
    public function DOMScrollVertical
    (
        $APlace
    )
    {
        return $this
        -> SetProperty([ 'scrollTop' => $APlace ]);
    }



    /*
        Make all focus elements invisible
    */
    public function DOMHide()
    {
        return $this
        -> DOMStyle([ 'display' => 'none' ]);
    }



    /*
        Make all focus elements visible
    */
    public function DOMShow
    (
        string $ADisplay = null
    )
    {
        return $this
        -> DOMStyle([ 'display' => $ADisplay ]);
    }



    /*
        Hide for visible false and show for visible true the focus DOM elements
    */
    public function DOMVisible( bool $AValue = true )
    {
        if( $AValue )
        {
            $this -> DOMShow();
        }
        else
        {
            $this -> DOMHide();
        }
        return $this;
    }



    /*
        Make all focus elements visible
    */
    public function DOMEnable()
    {
        return $this
        -> SetProperty([ 'disabled' => null ]);
    }



    /*
        Make all focus elements visible
    */
    public function DOMDisable()
    {
        return $this
        -> SetProperty([ 'disabled' => true ]);
    }



    /*
        Set CSS section for browser
    */
    public function DOMCSSAdd
    (
        string $AID,        /* ID for CSS conteiner */
        string $AContent    /* CSS Content */
    )
    {
        return $this
        -> FocusBody()
        -> DOMCreate( 'style', self::LAST, $AID )
        -> DOMContent( $AContent );
    }



    /*
        Add records from array in to parent container
    */
    public function DOMRecords
    (
        $ARecords     = [],
        $AGetContent  = null,
        string $ATemplate   = null,
        string $ATagName    = 'div',
        string $APosition   = TPusaCore::LAST,
        string $AID         = null,
        string $AClassName  = 'Record'
    )
    {
        foreach( $ARecords as $Record )
        {
            if( !empty( $AGetContent ))
            {
                $ATemplate = $AGetContent( $Record );
            }
            $this
            -> DOMCreate( $ATagName, $APosition, $AID )
            -> DOMClass( $AClassName )
            -> DOMContent( ObjectToContent( $Record, $ATemplate ))
            -> FocusParent();
        }
        return $this;
    }



    /*
        Remove the focused DOM elements from parents
    */
    public function DOMDelete()
    {
        return $this
        -> Call( 'remove' );
    }



    /*
        Send message to console browser
    */
    public function Console
    (
        $AMessage   /* Message */
    )
    {
        return $this
        -> FocusPush()
        -> FocusWindow()
        -> FocusProperty( 'console' )
        -> Call
        (
            'log',
            [ $AMessage ]
        )
        -> FocusPop()
        ;
    }



    /*
        Mark current DOM element for debuging
    */
    public function Mark()
    {
        return $this
        -> DOMStyle([ 'border' => '1px solid red' ]);
    }



    /*
        Copy value property of focus element to Clipboard of browser
    */
    public function DOMValueToClipboard()
    {
        return $this
        -> PileFromProperty( 'value' )
        -> PileToClipboard();
    }


    /*
        Execute code Visible or Invisible for element visible state
    */
    public function DOMVisibleCode
    (
        TPusaCore $AVisible = null,
        TPusaCore $AInvisible = null
    )
    {
        return $this
        -> PileFromStyle( 'display' )
        -> PileEqual( 'none', $AInvisible, $AVisible );
    }




    /**************************************************************************
        Extended Read methods
    */


    /*
        Read attribute value from focus object
    */
    public function ReadValue( $AArgument )
    {
        return $this
        -> ReadProperty( $AArgument, 'value' );
    }


    /*
        Read id of focus object
    */
    public function ReadID( $AArgument )
    {
        return $this
        -> ReadProperty( $AArgument, 'id' );
    }



    /*
        Read attribute value from focus object
    */
    public function ReadContent( $AArgument )
    {
        return $this
        -> ReadProperty( $AArgument, 'innerHTML' );
    }



    /*
        Read one attribute and return it in to $_POST[ 'Arg' ]
    */
    public function ReadAttribute
    (
        string $AArgumentName,  /* Argument key name for return atribute */
        string $AAttributeName  /* Attribute name */
    )
    {
        return $this
        -> ReadMethod( $AArgumentName, 'getAttribute', [ $AAttributeName ] );
    }



    /*
        Read one attribute and return it in to $_POST[ 'Arg' ]
    */
    public function ReadStyle
    (
        string $AArgumentName,  /* Argument key name for return atribute */
        string $AStyleName  /* Attribute name */
    )
    {
        return $this
        -> FocusPush()
        -> FocusProperty( 'style' )
        -> ReadProperty( $AArgumentName, $AStyleName )
        -> FocusPop();
    }



    /*
        Read cursor position from event
    */
    public function ReadPos()
    {
        return $this
        -> ReadEvent( 'AX', 'clientX' )
        -> ReadEvent( 'AY', 'clientY' );
    }



    /*
        Read visible
    */
    public function ReadVisible()
    {
        return $this
        -> ReadStyle( 'AVisible', 'display' );
    }



    /**************************************************************************
    */

    static public function IsVisible( $AValue )
    {
        return $AValue != 'none';
    }



    /**************************************************************************
    */

    /*
        Set clipboard value
    */
    public function SetClipboard
    (
        $AValue /* Value for put to clipboard */
    )
    {
        return $this
        -> FocusPush()
        -> FocusWindow()
        -> FocusProperty( 'navigator' )
        -> FocusProperty( 'clipboard' )
        -> FocusMethod( 'writeText', [ $AValue ])
        -> FocusPop();
    }



    /*
        Move pile value to clipboard
    */
    public function PileToClipboard
    (
        $AName = 'default'     /* Key name of Pile */
    )
    {
        return $this
        -> FocusPush()
        -> FocusWindow()
        -> FocusProperty( 'navigator' )
        -> FocusProperty( 'clipboard' )
        -> PileToMethod( 'writeText', $AName )
        -> FocusPop();
    }



    public function PileFromStyle
    (
        $AStyle = 'display',
        $AName = 'default'
    )
    {
        return $this
        -> FocusPush()
        -> FocusProperty( 'style' )
        -> PileFromProperty( 'display', $AName )
        -> FocusPop();
    }
}

