<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

namespace catlair;

/*
    The class for HTML text output.
    Heracly: THTMLBuilder < THTML < TText < TResult

    2019-11-22 still@itserv.ru
*/


require_once 'html.php';
require_once ROOT . '/core/html.php';



class THTMLBuilder extends THTML
{
    static public function Create()
    {
        return new self();
    }



    /*
        Out parameter with $AName:string and $AValue:any to current line for result Name = type:Value
    */
    public function BuildParam
    (
        string $AName,
        $AValue
    )
    {
        $this
        -> tagOpen      ( 'div' )
        -> setClass     ( 'paramLine' )

        -> tagOpen      ( 'span' )
        -> setClass     ( 'paramName' )
        -> addContent   ( $AName )
        -> tagClose     ()

        -> tagOpen      ( 'span' )
        -> setClass     ( 'paramEqual' )
        -> addContent   ( '=' )
        -> tagClose     ()

        -> tagOpen      ( 'span' )
        -> setClass     ( 'paramValue' )
        -> addContent   ( $AValue )
        -> tagClose     ()

        -> tagClose     ();
        return $this;
    }



    public function Table
    (
        int $AWidth,
        int $AHeight,
        $AOnRow = null,
        $AOnCell = null
    )
    {
        $this -> TagOpen( 'table' );
        for( $Y = 0; $Y < $AHeight; $Y++ )
        {
            $this -> TagOpen( 'tr' );
            if( !empty( $AOnRow ))
            {
                call_user_func_array( $AOnRow, [ $this, $Y ] );
            }
            for( $X = 0; $X < $AWidth; $X++ )
            {
                $this -> TagOpen( 'td' );
                if( !empty( $AOnCell ))
                {
                    call_user_func_array( $AOnCell, [ $this, $X, $Y ] );
                }
                $this -> TagClose();
            }
            $this -> TagClose();
        }
        $this -> TagClose();
        return $this;
    }
}
