# Catlair PHP core

Module list of Catlair core

## Main components

- result.php - An object for working with the result of errors. It is the parent class for most of the main Catlair objects. An alternative to exceptions for chaining support.
- loader - A libraryes loader using the require_once method. Catches errors and returns the execution in to result.
- debug.php - Loger, debugging, tracking system. The output can be directed to stdout or fs.
- params.php - A typed parameter object. We read, write the parameters by name, indicating the types. Explicit type conversion is implemented. The basis of many Catlair objects.

## Payload

- builder.php - Построитель контента Catlair. Контент строится на основе тектовых файлов с возможностью использования шаблонов и постобработки.
- controller.php - Базовый объект для пользовательсвнх контроллеров в парадигме MVC. На его основе написаны методы вызова &call=Class.Method.
- pusa_core.php - Pusa engine, contains protocol and baisic commands.
- pusa.php - Extends the Pusa Core.

## Utils

- url.php - Object for URL works. It can read and write parameters.
