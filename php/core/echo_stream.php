<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

namespace catlair;

/*
    Echo strem
    still@itserv.ru 16.10.2019
*/

require_once 'stream.php';
require_once 'debug.php';



class TEchoStream extends TStream
{
    private $FPosition=0;



    function __construct()
    {
        parent::__construct();
    }



    public static function Create()
    {
        return new TEchoStream();
    }



    public function &SetPosition($APosition)
    {
        /* do nothing */
        return $this;
    }



    public function IsEnd()
    {
        /* alvways return true becouse clients output it is console */
        return true;
    }



    public function GetSize()
    {
        return $this->Position;
    }



    public function &ReadBuffer($ASize)
    {
        /* do nothing and return empty */
        return '';
    }



    public function &WriteBuffer( $ABuffer )
    {
        print( $ABuffer );
        $this -> FPosition += strlen($ABuffer);
        return $this;
    }
}
