<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/


namespace catlair;

require_once( 'result.php' );
require_once( 'moment.php' );



class TProcess extends TResult
{
    private $SkipPause      = false;
    private $SleepTime      = 1000;
    private $DaemonPID      = null;
    private $Terminated     = false;

    /**************************************************************************
        Extnternal control from main application
    */

    /*
        Start new process
    */
    public function Start()
    {
        if( ! $this -> IsDaemonized() ) $this -> Daemonize();
        return $this;
    }



    /*
        Stop process
    */
    public function Stop()
    {
        if( $this -> IsDaemonized())
        {
            /* Daemon must close itself. It is his issue. */
            posix_kill( $this -> DaemonPID, SIGTERM );
            pcntl_waitpid( $this -> DaemonPID, $Status /*, WNOHANG  */ );
            /* Process forgot about daemon */
            $this -> DaemonPID = null;
        }
        return $this;
    }



    /*
        Send pause to process
    */
    public function Pause()
    {
        if( $this -> IsDaemonized() ) posix_kill( $this -> DaemonPID, SIGSTOP );
        return $this;
    }



    /*
        Contunue work process after pause
    */
    public function Continue()
    {
        if( $this -> IsDaemonized() ) posix_kill( $this -> DaemonPID, SIGCONT );
        return $this;
    }



    /*
        Return true if process has been demonized
    */
    public function IsDaemonized()
    {
        return $this -> DaemonPID != null;
    }



    /*
        Fork process and call main Loop
    */
    private function Daemonize()
    {
        $PID = pcntl_fork();

        if( $PID == -1 )
        {
            /* Daemonize error */
            $this -> SetCode( 'DaemonizeError' );
        }
        elseif( $PID > 0 )
        {
            /* Parent process */
            $this -> SetOk();
            $this -> DaemonPID = $PID;
        }
        else
        {
            /* Daemon*/
            $this -> Loop();
            exit();
        }
        return $this;
    }



    /**************************************************************************
        Internal control from demonized application
    */



    public function Terminate()
    {
        $this -> Terminated = true;
        return $this;
    }



    /**************************************************************************
        Private methods
    */



    /*
        Main loop
    */
    private function Loop()
    {
        $that = $this;

        /* Close console exchange */
        fclose( STDIN );
        fclose( STDOUT );
        fclose( STDERR );

        $STDIN  = fopen( '/dev/null', 'r' );
        $STDOUT = fopen( '/dev/null', 'wb' );
        $STDERR = fopen( '/dev/null', 'wb' );

        /* Set signal SIGTERM for terminate process */
        pcntl_signal
        (
            SIGTERM,
            function() use( $that )
            {
                $Stop = method_exists( $that, 'OnBeforeStop' ) ? $that -> OnBeforeStop() : true;
                if( $Stop ) $that -> Terminated = true;
            }
        );

        /* Set signal SIGTERM for terminate process */
        pcntl_signal
        (
            SIGINT,
            function() use( $that )
            {
                if( method_exists( $that, 'OnBeforeStop' )) $that -> OnBeforeStop();
                $that -> Terminated = true;
            }
        );

        /* Call the event on start job */
        if ( method_exists( $this, 'OnStart' )) $this -> OnStart();

        /* Loop */
        while( ! $this -> Terminated )
        {
            /* Make a job if exists */
            if ( method_exists( $this, 'Job' )) $this -> Job();

            /* Loop pause with signal interrupt */
            $PauseAccum = 0;
            while
            (
                ! $this -> GetSkipPause() &&                    /* не надо пропускать паузу */
                ! $this -> Terminated &&                        /* сервис еще активен */
                $PauseAccum < $this -> SleepTime                /* пауза меньше основной */
            )
            {
                pcntl_signal_dispatch();
                $Pause = 100;
                $PauseAccum += $Pause;
                usleep( $Pause * CL_MILI_SECOND );
            }
        }

        /* Call On Stop */
        if ( method_exists( $this, 'OnStop' )) $this -> OnStop();

        return $this;
    }


    /**************************************************************************
        Setters and getterd
    */


    private function SetStatus( $AValue )
    {
        $this -> Status = $AValue;
        return $this;
    }



    public function SetSleepTime( $AValue )
    {
        $this -> SleepTime = empty( $AValue ) ? 1000 : $AValue;
        return $this;
    }



    public function GetSleepTime()
    {
        return $this -> SleepTime;
    }



    public function SetSkipPause( $AValue )
    {
        $this -> SkipPause = $AValue;
        return $this;
    }



    public function GetSkipPause()
    {
        return $this -> SkipPause;
    }



    public function GetDaemonPID()
    {
        return $this -> DaemonPID;
    }
}
