<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

namespace catlair;

/*
    Converter code to HTML
*/
class TCodeToHTML
{

    //xxx/*...  -> XXX<span class="Comment">/*...</span>
    //...*/XXX  -> <span class="Comment">...*/</span>XXX
    //X/*...*/X -> X<span class="Comment">/*...*/</span>X
    //XX//....  -> XX<span class="Comment">//...</span>

    static public function Convert( $ASource )
    {
        $ASource = htmlspecialchars( $ASource, ENT_NOQUOTES );
        $Lines = explode( PHP_EOL, $ASource );

        /* запоминаем начало комментария */
        $IsComment                  = false;

        /* спан который окружает комментарий */
        $ReplacementSpanComment     = '<span class="Comment">$1</span>';

        foreach( $Lines as $Index => $Line )
        {
            /* Change first spaces to nbsp */
            $Line = preg_replace_callback
            (
                '/^( )+/',
                function( $matches )
                {
                    return str_repeat( '&nbsp;', strlen( $matches[0] ));
                },
                $Line
            );

            /* Расставляем комментарии */
            if (!$IsComment)
            {
                /* определяем, является ли строка началом комментария */
                if (preg_match('/(?:\/\*[^(?:\*\/)]*?$)/', $Line)) $IsComment = true;
                /* ищем в строке комментарии */
                $Line = preg_replace('/((?:\/\*.*?\*\/)|(?:\/\*.*?$)|(?:^[^(?:\/\*)]*?\*\/)|(?:\/\/.*$))/', $ReplacementSpanComment, $Line);
            }
            else
            {
                /* определяем, является ли строка концом комментария */
                if (preg_match('/(?:^[^(?:\/\*)]*?\*\/)/', $Line)) $IsComment = false;
                /* если в строке комментарий не закончился то вся строка комментарий */
                if ($IsComment) $Line = str_replace('$1', $Line, $ReplacementSpanComment);
                /* иначе пытаемся заменить комментарии комментарии */
                else $Line = preg_replace('/((?:\/\*.*?\*\/)|(?:\/\*.*?$)|(?:^.*?\*\/)|(?:\/\/.*$))/', $ReplacementSpanComment, $Line);
            }

            $Line = str_replace( '/*', '&#47;*', $Line );
            $Line = str_replace( '*/', '*&#47;', $Line );

            $Lines[ $Index ] = '<code>' . $Line . '</code>';
        }

        return implode( '', $Lines );
    }
}
