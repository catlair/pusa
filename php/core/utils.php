<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

/*
    Miscellaneous utilities
*/

namespace catlair;

/*
    Information volume conversion from count of bytes to string
*/
function clSizeToStr
(
    int      $ADelta,       /* Count of bytes */
    string  $AZero = '0'    /* A Zero value */
)
{
     if     ($ADelta >= 1024*1024*1024*1024)    $r=round($ADelta/(1024*1024*1024*1024),2) . 'TiB';
     elseif ($ADelta >= 1024*1024*1024)         $r=round($ADelta/(1024*1024*1024),2) . 'GiB';
     elseif ($ADelta >= 1024*1024)              $r=round($ADelta/(1024*1024),2) . 'MiB';
     elseif ($ADelta >= 1024)                   $r=round($ADelta/1024,2).'KiB';
     elseif ($ADelta > 0.1 && !$AZero)          $r=$ADelta . 'B';
     else                                       $r=$AZero;

     return $r;
}



/*
    Analog for JS encodeURIComponent
*/
function encodeURIComponent( $str )
{
    $revert = array( '%21' => '!', '%2A' => '*', '%28' => '(', '%29' => ')' );
    $r = strtr( rawurlencode( $str ), $revert );
    $r = strtr( $r, chr( 39 ), '%27' );
    return $r;
}



/*
    Создание GUIDоподобной строки.
*/
function clGUID()
{
    /* TODO !!! Надо будет переписать на нормальный алгоритм. */
    $A=str_pad(dechex(rand(0,hexdec('EFFFFFFF'))),8,'0');
    $B=str_pad(dechex(rand(0,hexdec('FFFF'))),4,'0');
    $C=str_pad(dechex(rand(0,hexdec('FFFF'))),4,'0');
    $D=str_pad(dechex(rand(0,hexdec('FFFF'))),4,'0');
    $E=str_pad(dechex(rand(0,hexdec('FFFF'))),4,'0');
    $F=str_pad(dechex(rand(0,hexdec('EFFFFFFF'))),8,'0');
    return $A . '-' . $B . '-' . $C . '-' . $D . '-' . $E . $F;
}



function clIsGUID( string $AValue )
{
    return preg_match
    (
        '/^[a-f0-9]{8}\-[a-f0-9]{4}\-[a-f0-9]{4}\-[a-f0-9]{4}\-[a-f0-9]{12}$/',
        $AValue
    );
}



/*
    Создание UUIDподобной строки.
*/
function clUUID()
{
    /* TODO !!! Надо будет переписать на нормальный алгоритм. */
    $A = str_pad( dechex( rand( 0, hexdec( 'EFFFFFFF' ))), 8, '0' );
    $B = str_pad( dechex( rand( 0, hexdec( 'FFFF' ))), 4, '0' );
    $C = str_pad( dechex( rand( 0, hexdec( 'FFFF' ))), 4, '0' );
    $D = str_pad( dechex( rand( 0, hexdec( 'FFFF' ))), 4, '0' );
    $E = str_pad( dechex( rand( 0, hexdec( 'FFFF' ))), 4, '0' );
    $F = str_pad( dechex( rand( 0, hexdec( 'EFFFFFFF' ))), 8, '0' );
    return $A . $B . $C . $D . $E . $F;
}



/*
    Создание случайного индентификатора
*/
function clRndID
(
    $ALength = 16,
    $AString = 'ABDEFHKLMNPRSTUXYZ'
)
{
    $Length = strlen( $AString ) - 1;
    $Result = '';
    for( $i=0; $i<$ALength; $i++ )
    {
        $Result .= $AString[ rand(0, $Length) ];
    }
    return $Result;
}



/*
    Создание случайного инденификатора
*/
function clRndNum( $ALength )
{
    return clRndID( $ALength, '0123456789' );
}



function clSeries( $AMasq )
{
    $Result = '';
    $Chains = explode( ';', $AMasq );

    foreach ( $Chains as $Chain )
    {
        $Param = explode( '=', $Chain );
        if ( count( $Param ) > 1 )
        {
            switch( $Param[ 0 ])
            {
                case 'GUID':
                    $Part = clUUID();
                break;
                case 'Now':
                    $Now = new TMoment();
                    $Part = $Now -> Now() -> ToString( $Param[ 1 ]);
                break;
                case 'String':
                    $Part = clRndID( (int) $Param[ 1 ]);
                break;
                case 'Number':
                    $Part = clRndNum( (int) $Param[ 1 ]);
                break;
                default:
                    $Part = $Param[ 0 ];
                break;
            }
        }
        else
        {
            $Part = $Param[ 0 ];
        }
        $Result = $Result . $Part;
    }

    if ( $Result == '' ) $Result = clUUID();
    return $Result;
}



/*
    Path control. Use for remove dangerous sequences from path.
*/
function clPathControl( $APath )
{
    return str_replace
    (
        [ '\\', '/../', '\\..\\', '/./', '?', '*', '$', '[', ']', ' ', '>', '<', '|' ],
        [ '_',  '_',   '_',       '_',   '_', '_', '_', '_', '_', '_', '_', '_', '_' ],
        $APath
    );
}



/*
    File control. Use for remove dangerous sequences from file name.
*/
function clFileControl( $AFile )
{
    return str_replace
    (
        [ '/', '&', '..', '?', '"', '*', ' ', '$', '[', ']', '\\', '<', '>', '|', '=' ],
        [ '_', '_', '_',  '_', '_', '_', '_', '_', '_', '_', '_',  '_', '_', '_', '_' ],
        $AFile
    );
}



/*
    Scatter string $AName = ABCD to path /A/B/C/ABCD with $ADepth for $ACharSet
*/
function clScatterName
(
    string  $AName,
    int     $ADepth     = 3,
    string  $ACharSet   = 'UTF-8'
)
{
    $Result = '';
    $l = mb_strlen( $AName, $ACharSet );
    for( $i=0; $i<$ADepth && $i<$l; $i++)
    {
        $Result .= '/' . mb_substr( $AName, $i, 1, $ACharSet );
    }
    return $Result . '/' . $AName;
}



/*
    Удаление папки рекурсивное
*/
function clDeleteFolder( $APath )
{
    if( is_dir( $APath ) === true )
    {
        $files = array_diff(scandir($APath), array('.', '..'));
        foreach( $files as $file) clDeleteFolder(realpath($APath) . '/' . $file );
        return rmdir($APath);
    }
    else
    {
        if( is_file( $APath ) === true ) return unlink( $APath );
    }
    return !file_exists( $APath );
}



function StringToHex($AString)
{
    $hex="";
    for ($i=0; $i < strlen($AString); $i++) $hex .= dechex(ord($AString[$i]));
    return $hex;
}



function HexToString($hex)
{
    $string="";
    for ($i=0; $i < strlen($hex)-1; $i+=2) $string .= chr(hexdec($hex[$i].$hex[$i+1]));
    return $string;
}



/*
    Copy file with subfolders
*/
function FileCopy($src,$dst)
{
    $dir = opendir($src);
    @mkdir($dst);
    while (false !== ( $file = readdir($dir)) )
    {
        if (( $file != '.' ) && ( $file != '..' ))
        {
            if ( is_dir($src . '/' . $file) ) FileCopy($src . '/' . $file, $dst . '/' . $file);
            else copy($src . '/' . $file, $dst . '/' . $file);
        }
    }
    closedir($dir);
}



/*
    Convert dirt string #tAg1 qwe #TAG2 to #tag1 #tag2
*/
function StringToTag($AString)
{
    return ($AString==null)?"":preg_replace('/ ?#END \Z/','',preg_replace('/(?:.|\n)*?(#\w+)/','$1 ',$AString.'#END'));
}



function IDToBin( $AValue )
{
    if (strlen((string)$AValue)>16) $Result = md5((string)$AValue);
    else $Result = (string)$AValue;
    return $Result;
}



/*
    Check and create path if that not exists
*/
function CheckPath( $APath )
{
    if ( ! file_exists( $APath )) mkdir( $APath, 0770, true );
    return file_exists( $APath );
}



/*
    Content from object
*/
function ObjectToContent( $AObject, $AContent )
{
    foreach( $AObject as $Key => $Value)
    {
        $Type = gettype( $Value );
        switch ( $Type )
        {
            case 'boolean':
            case 'integer':
            case 'double':
            case 'string':
                $AContent = str_replace( '%' . $Key . '%', $Value, $AContent );
            break;
            case 'array':
            case 'object':
                $AContent = ObjectToContent($Value, $AContent);
            break;
            case 'NULL':
               $AContent = str_replace( '%' . $Key . '%', 'null', $AContent );
            break;
        }
    }
    return $AContent;
}



/*
    Content from object
*/
function clArrayReplace
(
    array $AArray,      /* Array with strings */
    array $APattern     /* Named array key => value */
)
{
    $Result = [];
    foreach( $AArray as $Param => $Value)
    {
        $Type = gettype( $Value );
        switch ( $Type )
        {
            case 'string':
                foreach( $APattern as $Key => $Replace )
                {
                    $Value = str_replace( '%' . $Key . '%', $Replace, $Value );
                }
                $Result[ $Param ] = $Value;
            break;
            case 'array':
                $Result[ $Param ] = clArrayReplace( $Value, $APattern );
            break;
            default:
                $Result[ $Param ] = $Value;
            break;
        }
    }
    return $Result;
}



/*
    Remove tags from HTML text in $ABody and cut the string to $ALength
    Return preview text
*/
function Preview
(
    $ABody,     /* Text with HTML tags */
    $ALength    /* Max length of result */
)
{
    return mb_substr(preg_replace('/(\<(\/?[^>]+)>)/', '', $ABody), 0, $ALength).'...';
}



function Check( $AValue, $ATrue = null, $AFalse = null )
{
     if ( empty ( $AValue) )
     {
        if ( $AFalse == null ) $Result = null;
        else $Result = $AFalse;
     }
     else
     {
        if ( $ATrue == null ) $Result = $AValue;
        else $Result = $ATrue;
     }
     return $Result;
}



/*
    Return key from object
*/
function clValueFromObject
(
    $AObject,           /* Array or object */
    $AKey,              /* Key as string or as array of sting */
    $ADefault = null    /* Default value */
)
{
    $Result = $ADefault;
    switch( gettype( $AKey ))
    {
        case 'array':
            if( count( $AKey ) > 1 )
            {
                $Key = array_shift( $AKey );
                $Object = clValueFromObject( $AObject, $Key, $ADefault );
                $Result = clValueFromObject( $Object, $AKey, $ADefault);
            }
            else
            {
                $Result = clValueFromObject( $AObject, $AKey[ 0 ], $ADefault );
            }
        break;
        default:
            switch ( gettype( $AObject ))
            {
                case "array":
                    $Result = array_key_exists( $AKey, $AObject ) ? $AObject[ $AKey ] : $ADefault;
                break;
                case "object":
                    $Result = property_exists( $AObject, $AKey ) ? $AObject -> $AKey : $ADefault;
                break;
            }
        break;
    }
    return clConvert( $Result, gettype( $ADefault ));
}




/*
    Check exists key in array or object
*/
function clValueExists
(
    $AObject,           /* Array or object */
    $AKey               /* Key as string or as array of sting */
)
{
    $Result = false;
    switch( gettype( $AKey ))
    {
        case 'array':
            if( count( $AKey ) > 1 )
            {
                $Key = array_shift( $AKey );
                $Object = clValueFromObject( $AObject, $Key );
                $Result = clValueFromObject( $Object, $AKey );
            }
            else
            {
                $Result = clValueExists( $AObject, $AKey[ 0 ] );
            }
        break;
        default:
            switch ( gettype( $AObject ))
            {
                case 'array':
                    $Result = array_key_exists( $AKey, $AObject );
                break;
                case 'object':
                    $Result = property_exists( $AObject, $AKey );
                break;
            }
        break;
    }
    return $Result;
}




function clStringFromObject
(
    $AObject,           /* Array or object */
    $AKey,              /* Key as string or as array of sting */
    $ADefault = null    /* Default value */
)
{
    return ( string ) clValueFromObject( $AObject, $AKey, $ADefault );
}



function clIntegerFromObject
(
    $AObject,           /* Array or object */
    $AKey,              /* Key as string or as array of sting */
    $ADefault = null    /* Default value */
)
{
    return (int) clValueFromObject( $AObject, $AKey, $ADefault );
}



function clFloatFromObject
(
    $AObject,           /* Array or object */
    $AKey,              /* Key as string or as array of sting */
    $ADefault = null    /* Default value */
)
{
    return (float) str_replace( ' ', '', clValueFromObject( $AObject, $AKey, $ADefault ));
}



function clBooleanFromObject
(
    $AObject,           /* Array or object */
    $AKey,              /* Key as string or as array of sting */
    $ADefault = false   /* Default value */
)
{
    $Value = clValueFromObject( $AObject, $AKey, $ADefault );
    return $Value === true || $Value == 'true' || $Value == 'on' || $Value === 1;
}



/*
    Convert Value of any type in to Float
*/
function ValueToFloat( $AValue, $AThousandDelimeter = ' ' )
{
    $Type = gettype( $AValue );
    switch( $Type )
    {
        case 'integer':
        case 'boolean':
            $Value = (float) $AValue;
        break;

        case 'double':
            $Value = $AValue;
        break;

        case 'string':
            $Value = (float) str_replace( $AThousandDelimeter, '', $AValue );
        break;

        default:
            $Value = 0.0;
        break;
    }
    return $Value;
}



function PurgeComments( $ASource )
{
    $ASource = preg_replace('/\/\/.+/', '', $ASource );
    return $ASource;
}



/*
    Recursion runing the files tree
    TODO - move it at file_utils.php
*/
function clFileScan
(
    $APath,
    $AOnFolder  = null,
    $AOnFile    = null,
    $AIndex     = 0
)
{
    if( is_dir( $APath ))
    {
        /* Path is directory */
        $Dir = opendir( $APath );

        while(( $File = readdir( $Dir )) !== false )
        {
            if(( $File != '.' ) && ( $File != '..' ))
            {
                $Full = $APath . '/' . $File;
                if( is_dir( $Full ))
                {
                    if( $AOnFolder == null ? true : call_user_func( $AOnFolder, $Full, $AIndex ))
                    {
                        /* Recursion */
                        clFileScan( $Full, $AOnFolder, $AOnFile, $AIndex + 1 );
                    }
                }
                else
                {
                    call_user_func( $AOnFile, $Full, $AIndex );
                }
            }
        }
        closedir( $Dir );
    }
    else
    {
        /* Path is file */
        if( file_exists( $APath ))
        {
            call_user_func( $AOnFile, $APath, $AIndex );
        }
    }

    return true;
}



/*
    Wrap the Body between the prefix and suffix
    or return the empty string if result does not exists.
*/
function clWrap
(
    string  $ABody   = null,
    string  $APrefix = null,
    string  $ASuffix = null
)
{
    return empty( $ABody ) ? '' :
    (
        ( empty( $APrefix ) ? '' : ( $APrefix . ' ' ) ) .
        $ABody .
        ( empty( $ASuffix ) ? '' : ( ' ' . $ASuffix ) )
    );
}




/*
    Convert any value to type
*/
function clConvert
(
    $AValue,                    /* Value */
    string $AType = 'string'    /* Type for convert from gettype() */
)
{
     $Result = $AValue;
     switch( $AType )
     {
         case 'integer':
             $Result = (int) $AValue;
         break;
         case 'boolean':
             $Result = $AValue === true || $AValue === 'true' || $AValue === 'on' || $AValue == 1.0;
         break;
         case 'double':
             switch( gettype( $AValue ))
             {
                case 'boolean':
                case 'integer':
                    $Result = (float) $Result;
                break;
                case 'double':
                    $Result = $Result;
                break;
                case 'string':
                    $Result = floatval( str_replace( ' ', '', $Result ));
                break;
                default:
                    $Result = null;
                break;
            }
         break;
         case 'string':
             switch( gettype( $AValue ))
             {
                 case 'boolean':
                 case 'integer':
                 case 'double':
                 case 'array':
                 case 'object':
                     $Result = json_encode( $AValue );
                 break;
             }
        break;
        case 'object':
        break;
        case 'array':
        break;
        case 'NULL':
        break;
    }
    return $Result;
}




/*
    Notation
*/

function clAnyToLower( string $AValue )
{
    return strtolower( preg_replace( '/[\W|_]/', '', $AValue ));
}


/*
    Convert PascalCase to SnakeCase
*/
function clPascalToSnake( $AValue )
{
    return str_replace( ' ', '', strtolower( preg_replace('/(?<!^)[A-Z]/', '_$0', $AValue )));
}



/*
    Convert PascalCase to CONSTANT_CASE
*/
function clPascalToConstant( $AValue )
{
    return str_replace( ' ', '', strtoupper( preg_replace('/(?<!^)[A-Z]/', '_$0', $AValue )));
}



/*
    Convert snake_case to PascalCase
*/
function clSnakeToPascal( $AValue )
{
    return str_replace( ' ', '', ucwords( str_replace( '_', ' ', $AValue )));
}



/*
    Convert any to PascalCase
*/
function clAnyToPascal( $AValue )
{
    $Lower = strtolower( str_replace( ['-', ' ', '.' ], [ '_', '_', '_' ],  $AValue ));

    if( strpos( $AValue, '_' ) >=0 ) $Result = clSnakeToPascal( $Lower );
    else $Result = $AValue;

    return $Result;
}



/*
    Replace all values int Source from array of Replace pairs
*/
function clReplace
(
    string $ASource,            /* Source for raplace */
    array  $AReplace,           /* Array of pairs key => value */
    string $ABegin      = '%',  /* Begin of macro */
    string $AEnd        = '%',  /* EndOfMacro */
    array  $AExclude    = [],   /* List of exclude Keynames */
    $ACallback          = null  /* Callback function(string $AKeyName) return KeyValue */
)
{
    /* Move Source to Result */
    $Result = $ASource;

    do
    {
        /* Split the source to lexemes */
        $Source = preg_split( '/(' .$ABegin. '\w*' . $AEnd . ')/', $Result, 0, PREG_SPLIT_DELIM_CAPTURE );

        /* Loop for split result */
        $Continue = false;
        foreach( $Source as $Lexeme )
        {
            /* Check the lexeme on exclude list */
            $ExcludeFlag = false;

            foreach( $AExclude as $Item )
            {
                $ExcludeFlag = $ExcludeFlag || ( $ABegin . $Item . $AEnd == $Lexeme );
            }

            /* Replace for unexcluded */
            if( ! $ExcludeFlag )
            {
                if( preg_match( '/' . $ABegin. '\w*' . $AEnd .  '/', $Lexeme ))
                {
                    $ResultBefore = $Result;

                    /* Define name */
                    $Name = str_replace( [ $ABegin, $AEnd ], [ '','' ], $Lexeme );

                    /* Define value */
                    $Value = null;
                    if( array_key_exists( $Name, $AReplace ) )
                    {
                        /* Get value from parameters */
                        $Value = $AReplace[ $Name ];
                    }
                    elseif( ! empty( $ACallback ))
                    {
                        /* Get value from callback function by Name */
                        $Value = call_user_func( $ACallback, $Name );
                    }

                    /* Replace */
                    if( $Value !== null )
                    {
                        $Result = str_replace( $Lexeme, $Value, $Result );
                    }

                    /* Continue while result not equal previous */
                    $Continue = $Continue || $ResultBefore != $Result;
                }
            }
        }
    }
    while( $Continue );

    return $Result;
}
