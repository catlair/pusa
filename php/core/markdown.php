<?php


namespace Catlair;

require_once 'plantuml.php';
require_once 'html_code.php';

class TMarkdown extends THTMLCode
{
    private $OnLink     = null;
    private $OnImage    = null;
    private $OnCode     = null;

    static public function Create()
    {
        return new TMarkdown();
    }



    public function ToHTML( $ASource )
    {
        preg_match_all( '/^```[\s\S]*?^```$|^.*?$/m', $ASource, $Lines );

        if( $Lines > 0 )
        {
            foreach( $Lines[ 0 ] as $Line )
            {
                if( $this -> Code( $Line )) {}      /*```*/
                elseif( $this -> Line( $Line )) {}  /* --- */
                elseif( $this -> Empty( $Line )) {} /* Empty line */
                elseif( $this -> Head( $Line )) {}  /* # Header */
                elseif( $this -> Dot( $Line )) {}   /* - Bullet item  */
                elseif( $this -> Num( $Line )) {}   /* #. Number item */
                else
                {
                    $this -> AddContent( $Line );
                }
            }
            $this -> PrepareContent() -> TagCloseAll();
        }

        return $this;
    }



    private function Inline( string $AContent )
    {
        $Lines = preg_split
        (
            '/(?:(\*\*.*?\*\*)|(_.*?_)|(\[[^\]]*?\]\(.*?\))|(\!\[[^\]]*?\]\(.*?\)))/',
            $AContent,
            -1,
            PREG_SPLIT_DELIM_CAPTURE
        );

        foreach( $Lines as $Line )
        {
            if( $this -> InlineBold( $Line ) ) {}
            elseif( $this -> InlineImage( $Line ) ) {}
            elseif( $this -> InlineLink( $Line ) ) {}
            else $this -> InlineString( $Line );
        }

        return $this;
    }



    /*
        Inline bold tag
    */
    private function InlineBold( $ALine )
    {
        $Result = preg_match( '/\*\*(.*)?\*\*/', $ALine, $m );
        if( $Result )
        {
            $this
            -> TagOpen( 'b' )
            -> AddContent( $m[ 1 ] )
            -> TagClose()
            -> GetContent();
        }
        return $Result;
    }



    private function InlineLink( $ALine )
    {
        $Result = preg_match( '/\[([^\]]*?)\]\((.*?)\)/', $ALine, $m );
        if( $Result )
        {
            $Link = $m[ 2 ];
            $Content = $m[ 1 ];
            if( empty( $Link ))
            {
                $Link = '#' . $Content;
            }
            if( empty( $this -> OnLink ))
            {
                $this
                -> TagOpen( 'a' )
                -> AddContent( empty( $Content ) ? $Link : $Content )
                -> TagClose();
            }
            else
            {
                call_user_func( $this -> OnLink, $this, $Link, $Content );
            }
        }
        return $Result;
    }



    private function InlineImage( $ALine )
    {
        $Result = preg_match( '/\!\[([^\]]*?)\]\((.*?)\)/', $ALine, $m );
        if( $Result )
        {
            $Link = $m[ 2 ];
            $Content = $m[ 1 ];
            if( empty( $this -> OnImage ))
            {
                $this
                -> TagOpen( 'img' )
                -> SetAttr( 'src', $Link )
                -> TagClose()
                -> TagOpen( 'div' )
                -> AddContent( empty( $Content ) ? $Link : $Content )
                -> TagClose();
            }
            else
            {
                call_user_func( $this -> OnImage, $this, $Link, $Content );
            }
        }
        return $Result;
    }



    private function InlineString( $ALine )
    {
        if( !empty( $ALine ))
        {
            $this -> AddContent( $ALine );
        }
        return $this;
    }



    private function PrepareContent()
    {
        $Tag = $this -> TagCurrent();
        if( !empty( $Tag ))
        {
            switch( $Tag -> tagName )
            {
                case 'p':
                case 'li':
                case 'h1':
                case 'h2':
                case 'h3':
                case 'h4':
                case 'h5':
                case 'h6':
                    $Content = $Tag -> GetContent( ' ' );
                    $Tag -> SetContent( '' );
                    $this -> Inline( $Content );
                break;
            }
        }
        return $this;
    }



    public function AddContent( $AContent )
    {
        if( empty( $this -> TagCurrent() ))
        {
            $this -> TagOpen( 'p' );
        }
        parent::AddContent( $AContent );
        return $this;
    }



    private function Code( $ALine )
    {
        $Result = preg_match_all( '/^```(.*?)$([\s\S]*?)^```$/m', $ALine, $m );

        if( $Result )
        {
            $CodeType = $m[ 1 ][ 0 ];
            $CodeBody = $m[ 2 ][ 0 ];

            if( empty( $this -> OnCode ))
            {
                switch( $CodeType )
                {
                    default:
                        $this
                        -> PrepareContent()
                        -> TagCloseAll()
                        -> BuildCode( $CodeBody );
                    break;

                    case 'mermaid':
                        $this
                        -> PrepareContent()
                        -> TagClose()
                        -> TagOpen( 'img' )
                        -> SetAttr( 'class', 'Mermaid' )
                        -> SetAttr( 'src', $this -> MermaidLink( $CodeBody ))
                        -> PrepareContent()
                        -> TagClose();
                    break;

                    case 'plantuml':
                        $this
                        -> PrepareContent()
                        -> TagClose()
                        -> TagOpen( 'img' )
                        -> SetAttr( 'class', 'PlantUML' )
                        -> SetAttr( 'src', $this -> PlantUMLLink( $CodeBody ))
                        -> PrepareContent()
                        -> TagClose();
                    break;
                }
            }
        }
        return $Result;
    }



    private function Empty( $ALine )
    {
        $Result = !$this -> IsTag( 'code' ) && empty( $ALine );
        if( $Result )
        {
            $this
            -> PrepareContent()
            -> TagCloseAll();
        }
        return $Result;
    }



    private function Line( $ALine )
    {
        $Result = !$this -> IsTag( 'code' ) && $ALine === '---';
        if( $Result )
        {
            $this
            -> PrepareContent()
            -> TagCloseAll()
            -> TagOpen( 'hr' )
            -> TagClose();
        }

        return $Result;
    }



    private function Head( $ALine )
    {
        $Result = !$this -> IsTag( 'code' ) &&
        preg_match( '/^(#{1,6}) (.*)$/', $ALine, $m );

        if( $Result )
        {
            $this -> PrepareContent();

            $this
            -> TagCloseAll()
            -> TagOpen( 'a' )
            -> SetAttr( 'name', $m[ 2 ])
            -> SetClass( 'Anchor' )
            -> TagClose()
            -> TagOpen( 'h' . strlen( $m[ 1 ]) )
            -> AddContent( $m[ 2 ] )
            -> TagClose();
        }

        return $Result;
    }



    private function Dot( $ALine )
    {
        $Result =
        !$this -> IsTag( 'code' ) &&
        preg_match( '/^ *- (.*)$/', $ALine, $m );

        if( $Result )
        {
            $this -> PrepareContent();

            if( $this -> IsTag( 'li' ))
            {
                $this -> TagClose();
            }

            if( !$this -> IsTag( 'ul') )
            {
                $this -> TagCloseAll() -> TagOpen( 'ul' );
            }

            $this
            -> TagOpen( 'li' )
            -> AddContent( $m[ 1 ] );
        }

        return $Result;
    }



    private function Num( $ALine )
    {
        $Result =
        !$this -> IsTag( 'code' ) &&
        preg_match( '/^ *\d*. (.*)$/', $ALine, $m );

        if( $Result )
        {
            $this -> PrepareContent();

            if( $this -> IsTag( 'li' ))
            {
                $this -> TagClose();
            }

            if( !$this -> IsTag( 'ol') )
            {
                $this -> TagCloseAll() -> TagOpen( 'ol' );
            }

            $this
            -> TagOpen( 'li' )
            -> AddContent( $m[ 1 ] );
        }

        return $Result;
    }



    public function SetOnLink( $AEvent )
    {
        $this -> OnLink = $AEvent;
        return $this;
    }



    public function SetOnImage( $AEvent )
    {
        $this -> OnImage = $AEvent;
        return $this;
    }



    public function PlantUMLLink( $AText )
    {
        return 'https://www.plantuml.com/plantuml/svg/' . encodep( $AText );
    }



    public function MermaidLink( $AText )
    {
        return 'https://mermaid.ink/img/' . base64_encode( $AText );
    }
}
