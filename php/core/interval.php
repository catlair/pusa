<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/


namespace catlair;

require_once( 'result.php' );
require_once( 'moment.php' );
require_once( 'moment_util.php' );



class TInterval extends TResult
{
    const MAX_VALUE = PHP_FLOAT_MAX;


    public $Begin       = null;
    public $End         = null;
    public $Operator    = 1;


    function __construct()
    {
        $this -> SetOk();
    }



    static public function Create()
    {
        return new TInterval();
    }



    static public function CreateByPeriod
    (
        float $ABegin,
        float $AEnd
    )
    {
        return self :: Create() -> Set( $ABegin, $AEnd );
    }



    /*
        Set operator for interval
    */
    public function SetOperator
    (
        int $AValue
    )
    {
        $this -> Operator = $AValue;
        return $this;
    }



    /*
        Get and return the operator of interval
    */
    public function GetOperator()
    {
        return $this -> Operator;
    }



    /*
        Set begin and end for interval
    */
    public function Set
    (
        float $ABegin = -self::MAX_VALUE,
        float $AEnd   = self::MAX_VALUE
    )
    {
        $this -> Begin = $ABegin;
        $this -> End = $AEnd;
        if( ! $this -> IsValid() ) $this -> SetCode( 'IntervalNotValid' );
        return $this;
    }



    /*
        Create and return copy of this interval
    */
    public function Copy()
    {
        return self :: CreateByPeriod
        (
            $this -> GetBegin(),
            $this -> GetEnd()
        )
        -> SetOperator( $this -> GetOperator() );
    }



    /*
        Test on interval.

        Comparator
                           Test    interval
        CL_LESS  -------------*---{--------}----------------->
        CL_EQUAL -----------------*--------}----------------->
        CL_EQUAL -----------------{----*---}----------------->
        CL_MORE  -----------------{--------*----------------->
        CL_MORE  -----------------{--------}---*------------->
        CL_WRONG -----------------}--------{----------------->
    */
    public function Test
    (
        float $AValue
    )
    {
        /* Comparator */
        if( $AValue >= $this -> Begin && $AValue <= $this -> End ) $Result = CL_EQUAL;
        elseif ( $AValue < $this -> Begin ) $Result = CL_LESS;
        elseif ( $AValue > $this -> End ) $Result = CL_MORE;
        return $Result;
    }



    /*
        Set begin
    */
    public function SetBegin
    (
        float $AValue
    )
    {
        $this -> Begin = $AValue;
        return $this;
    }



    /*
        Set begin
    */
    public function SetEnd
    (
        float $AValue
    )
    {
        $this -> End = $AValue;
        return $this;
    }



    /*
        Return the begin
    */
    public function GetBegin()
    {
        return $this -> Begin;
    }



    /*
        Return the End
    */
    public function GetEnd()
    {
        return $this -> End;
    }



    /*
        Check validability
    */
    public function IsValid()
    {
        return $this -> Begin <= $this -> End;
    }



    /*
        Округление начала и конца интервала до зерна
    */
    public function Round
    (
        $ASeed = 0
    )
    {
        if( !empty( $ASeed ))
        {
            /* Округление производится если значения не -бесконечность */
            if( $this -> Begin != -self::MAX_VALUE )
            {
                $this -> Begin  = floor( $this -> Begin / $ASeed ) * $ASeed;
            }
            /* Округление производится если значения не +бесконечность */
            if( $this -> Begin != self::MAX_VALUE )
            {
                $this -> End    = ceil( $this -> End / $ASeed ) * $ASeed;
            }
        }
        return $this;
    }



    /**************************************************************************
        Функционал TMoment
    */


    /*
        Создание интервала по двум моментам времени
    */
    static public function CreateByMoment
    (
        TMoment $ABegin,
        TMoment $AEnd
    )
    {
        return self :: CreateByPeriod
        (
            $ABegin == null || $ABegin -> Get(true) <= 0 ? -self::MAX_VALUE : $ABegin -> Get(),
            $AEnd == null || $AEnd -> Get(true) <= 0 ? self::MAX_VALUE : $AEnd -> Get()
        );
    }




    /*
        Test AMoment on interval.

        Time comparator
                      24> <00      interval     24> <00
        CL_LESS  --------|----*---{--------}-------|--------->
        CL_EQUAL --------|--------*--------}-------|--------->
        CL_EQUAL --------|--------{----*---}-------|--------->
        CL_MORE  --------|--------{--------*-------|--------->
        CL_MORE  --------|--------{--------}---*---|--------->
        CL_WRONG --------|--------}--------{-------|--------->

        Moment comparator
                           Test    interval
        CL_LESS  -------------*---{--------}----------------->
        CL_EQUAL -----------------*--------}----------------->
        CL_EQUAL -----------------{----*---}----------------->
        CL_MORE  -----------------{--------*----------------->
        CL_MORE  -----------------{--------}---*------------->
        CL_WRONG -----------------}--------{----------------->
    */
    public function TestMoment( $AMoment )
    {
        $Moment = $AMoment -> Get();
        $Begin  = $this -> Begin;
        $End    = $this -> End;

        if( $Begin <= TMoment::DAY && $End <= TMoment::DAY )
        {
            /* Add begin of day for each eage becouse it is a time */
            $DayBegin = $AMoment -> GetDayBegin() -> Get();
            $Begin += $DayBegin;
            $End += $DayBegin;
        }

        /* Comparator */
        if( $Moment >= $Begin && $Moment < $End ) $Result = CL_EQUAL;
        elseif ( $Moment < $Begin ) $Result = CL_LESS;
        elseif ( $Moment >= $End ) $Result = CL_MORE;

        return $Result;
    }



    /*
        $AString in format hh[:mm[:ss]] > hh[:mm[:ss]]
        0 >= hh <= 24
        0 >= mm <= 60
    */
    public function FromTimeString
    (
        $AString,
        TMoment $ADay = null
    )
    {
        $Lexems = explode( '>', $AString );
        $this -> Set
        (
            clTimeFromString( count( $Lexems ) > 0 ? trim( $Lexems[ 0 ]) : 0 ) + ( empty( $ADay ) ? 0 : $ADay -> Get()),
            clTimeFromString( count( $Lexems ) > 1 ? trim( $Lexems[ 1 ]) : 0 ) + ( empty( $ADay ) ? 0 : $ADay -> Get())
        );

        return $this;
    }



    /*
        $AString in format DD.MM.YYYY [hh:mm:ss] > DD.MM.YYYY [hh:mm:ss]
    */
    public function FromMomentString( $AString )
    {
        $Lexems = explode( '>', $AString );

        $Begin  = count( $Lexems ) > 0 ? TMoment::Create( clMomentStringFromText( $Lexems[ 0 ] )) -> Get() : 0;
        $End    = count( $Lexems ) > 1 ? TMoment::Create( clMomentStringFromText( $Lexems[ 1 ] )) -> Get() : 0;

        $this -> Set
        (
            $Begin  != 0 ? $Begin   : -self::MAX_VALUE,
            $End    != 0 ? $End     : +self::MAX_VALUE
        );

        return $this;
    }

}
