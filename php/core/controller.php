<?php

/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.


    Объект обработки результатов исполнения публичных процедур
    в Content принимается контент
    в процессе работы производится сборка параметров при помощи SetOutcome()
    при завершении сборки вызывается End().
    в зависимости от наличия контента производится либо подмена ключей
    %key% в нем, либо параметры собираются в JSON структуру.

    still@itserv.ru
*/

namespace catlair;

require_once "debug.php";
require_once "params.php";
require_once "cli_util.php";



/* Content type */
define( "tcAuto", 'Auto' ); /* If content is absent then return XML else HTML */
define( "tcHTML", 'HTML' ); /* Content type is HTML */
define( "tcXML",  'XML' );  /* Content type is XML */
define( "tcJSON", 'JSON' ); /* Content type is JSON. Now is not use. */
define( "tcNone", 'None' ); /* Content not return. */



class TController extends TParams
{
    const CONTROLLER            = 'controller';
    const TEST                  = 'test';

    private $FContent           = null;     /* Content with macro in %Example% */
    private $Outcome            = null;     /* Array of params */
    private $Data               = [];       /* Array of records */
    private $Detale             = [];       /* Detale for arrays of records */
    private $FTypeContent       = tcAuto;
    private $FReturnResult      = true;
    private $Builder            = null;     /* Link to TBuilder */
    private $ResultDataEnabled  = true;

    private $XMLRoot            = 'root';
    private $XMLLines           = [];

    public $Log;
    public $RecordContent       = null;     /* Template for records */

    private $Type               = null;
    private $Method             = null;
    private $Call               = null;
    private $Stat               = null;     /* Statistic of controller */



    /*
        Constructor
    */
    public function __construct( $ABuilder )
    {
        $this->Builder = $ABuilder;

        /* Create internal arrays */
        $this -> Outcome    = [];
        $this -> Stat       = [];

        /* Copy parameter from Builder */
        $this -> CopyFrom( $ABuilder -> GetIncome() );

        $this -> SetTypeContentIncome();
        $this -> Log = $ABuilder;

        /* Call init method for controller */
        if ( method_exists( $this, 'Init' )) $this -> Init();
        $this -> SetOk();
    }



    /*
        Static create
    */
    static public function Construct( $ABuilder, $AName, $AType = TController :: CONTROLLER )
    {
        $Call = $ABuilder -> LoadController( $AName, $AType );

        if( $ABuilder -> IsOk() )
        {
            $Result = new $Call[ 'Class' ]( $ABuilder );
        }
        else
        {
            $Result = new TController( $ABuilder );
        }

        $Result
        -> SetMethod( $Call[ 'Method' ])
        -> SetCall(  $AName )
        -> SetType( $AType );

        return $Result;
    }



    static public function ConstructTest( $ABuilder, $AName )
    {
        return TController :: Construct( $ABuilder, $AName, $AType = TController :: TEST );
    }



    public function GetBuilder()
    {
        return $this -> Builder;
    }



    /*
        Set content type
    */
    public function SetTypeContent( $AValue )
    {
        $this -> FTypeContent = $AValue;
        return $this;
    }



    /*
        Set content type from income
    */
    public function &SetTypeContentIncome()
    {
        $this -> FTypeContent = $this -> GetParam( 'TypeContent', tcAuto );
        return $this;
    }



    /*
        Set return result flag
    */
    public function &SetReturnResult($AValue)
    {
        $this->FReturnResult = $AValue;
        return $this;
    }



    /*
        Set content
    */
    public function &SetContent( $AContent )
    {
        $this -> FContent = $AContent;
        return $this;
    }



    /*
        Get content
    */
    public function GetContent()
    {
        return $this -> FContent;
    }



    /*
        Устанавливает значение параметра
        Do not use. Use SetOutcome
    */

    public function &Set( $AName,  $AValue = null )
    {
        $this -> Log -> Warning( 'ControllerSetUsing' );
        $this -> SetOutcome( $AName, $AValue );
        return $this;
    }



    /*
        Устанавливает значение параметра
        Do not use. Use GetOutcome
    */
    public function Get( $AName, $ADefault = null )
    {
        $this -> Log -> Warning( 'ControllerGetUsing' );
        return ( array_key_exists( $AName, $this->Outcome )) ? $this -> Outcome[ $AName ] : $ADefault;
    }



    /*
        Устанавливает значение параметра
    */
    public function SetOutcome ( $AName, $AValue = null )
    {
        if ( $AValue !== null )
        {
            $this -> Outcome[ $AName ] = $AValue;
        }
        else
        {
            switch ( gettype( $AName ))
            {
                case 'object': $this -> Outcome = array_merge ( $this -> Outcome, (array)$AName ); break;
                case 'array': $this -> Outcome = array_merge ( $this -> Outcome, $AName ); break;
            }
        }
        return $this;
    }



    /*
        Устанавливает значение параметра
    */
    public function GetOutcome($AName, $ADefault = null )
    {
        return ( array_key_exists( $AName, $this -> Outcome )) ? $this -> Outcome[ $AName ] : $ADefault;
    }



    /*
        Return income parameter $AName:string value or $ADefault:string
    */
    function GetIncome( $AName, $ADefault = null )
    {
        $Result = $this -> GetParam( $AName );
        if( empty( $Result ) && $this -> Builder -> IsCLI() )
        {
            $Result = GetIncomeCLI( $AName, 'Input parameter', $ADefault, true );
        }

        if( empty( $Result )) $Result = $ADefault;

        return $Result == 'null' ? null : $Result;
    }



    /*
        Добавление ассоциативного массива как как очередная запись в сегдмент Data
        $AGroup - имя группы записей. Если ранее отсутствовала - создается.
        $AArray - массив именованых ключей в виде записи
    */
    public function SetGroup($AGroup, $AName, $AValue)
    {
        if (!array_key_exists($AGroup, $this->Detale)) $this->Detale[$AGroup] = [];
        $this->Detale[$AGroup][$AName]=$AValue;
        return $this;
    }



    /*
        Добавление ассоциативного массива как как очередная запись в сегдмент Data
        $AGroup - имя группы записей. Если ранее отсутствовала - создается.
        $AArray - массив именованых ключей в виде записи
    */
    public function AddRecord($AGroup, &$AArray)
    {
        // создание массива
        if (!array_key_exists($AGroup, $this->Data)) $this->Data[$AGroup] = [];
        array_push($this->Data[$AGroup], $AArray);
        return $this;
    }



    /*
    */
    public function &AddDataSection( $AGroup, &$AValue )
    {
        $this -> Data[ $AGroup ] = $AValue;
        return $this;
    }



    /*
        Устанавливает Header в случае если текущее API - сайт
    */
    public function Header( $AValue )
    {
        switch( php_sapi_name() )
        {
            case 'cgi':
            case 'fpm-fcgi':
                header( $AValue );
            break;
        }
        return $this;
    }



    /*
        Заполнение сообщения об ошибке
        Функция должна быть переписана в потомках
    */
    protected function MessageByCode()
    {
        return $this -> Builder -> GetTemplate( $this -> Code, $this -> Code ) . ' [' . $this -> GetMessage() . ']';
    }



    /*
         Завершает сбор параметров и возвращает результат в виде тектового контента.
         в случае если подменный контент отсутствовал возвращается структурированный json, html, xml.
         возвращаемый тип зависит от FTypeContent.
     */
    public function End()
    {
        $this -> Log -> Begin( 'Build controller result ' . get_class( $this ) );

        /* Получение собщения по коду ошибки */
        if( $this -> IsOk() )
        {
            $this -> Message = 'Ok';
        }
        else
        {
            if( empty( $this -> Message )) $this -> Message = $this -> MessageByCode();
            if( empty( $this -> Message )) $this -> Message = $this -> Code;

            $this
            -> Log
            -> Warning  ( 'Controller result' )
            -> Param    ( 'Code', $this -> Code )
            -> Param    ( 'Message', $this -> Message )
            -> Dump     ( $this -> Detaile );
        }

        /* Processing */
        $r = '';

        if( $this -> FReturnResult )
        {
            /* Build XML content */
            if( $this -> FTypeContent == tcXML )
            {
                $this -> Builder -> Begin( 'XML result' );
                $r = [];
                array_push( $r, XML_HEADER );
                array_push( $r, '<' . $this -> XMLRoot . '>' );

                /* Сборка исходящих параметров в список */
                foreach ( $this -> XMLLines as $Line )
                {
                    array_push( $r, $Line );
                }

                /* Сборка исходящих параметров в список */
                if( count( $this -> Outcome ) > 0)
                {
                    array_push( $r, '<Outcome>' );
                    foreach ( $this -> Outcome as $Key => $Value )
                    {
                        array_push( $r, '<' .$Key . '>' . encodeURIComponent( $Value ) . '</'.$Key.'>' );
                    }
                    array_push( $r, '</Outcome>' );
                }


                /* Сборка списков в ключ Data */
                if( count( $this -> Data ) > 0 )
                {
                    array_push( $r, '<Data>' );
                    foreach ( $this->Data as $Name => $Array)
                    {
                        /* Сборка очередного списка */
                        array_push( $r, '<'.$Name.' Type="Array">' );

                        foreach ( $Array as $Index => $Value )
                        {
                            /* Сборка записи списка */
                            array_push( $r, '<Record_'.$Index );
                            foreach ($Value as $NameP => $ValueP)
                            {
                                if (gettype($ValueP)!=='object' && gettype($ValueP)!=='array')
                                {
                                    array_push( $r, ' ' . $NameP . '="' );
                                    array_push( $r, encodeURIComponent( (string) $ValueP).'"' );
                                }
                            }
                            array_push( $r, '/>' );
                        }
                        array_push( $r, '</'.$Name.'>' );
                    }
                    array_push( $r, '</Data>' );
                }

                if( count( $this->Detale ) > 0 )
                {
                    /* Сборка списков в ключ Detale */
                    array_push( $r, '<Detale>' );
                    foreach ($this->Detale as $Name => $Array)
                    {
                        /* Сборка очередного списка*/
                        array_push( $r, '<'.$Name );
                        foreach ($Array as $Key => $Value)
                        {
                            if (gettype($Value)!=='object') array_push( $r, ' '.$Key.'="'.encodeURIComponent($Value).'"' );
                        }
                        array_push( $r, '/>' );
                    }
                    array_push( $r, '</Detale>' );
                }

                if( $this -> ResultDataEnabled )
                {
                    array_push( $r, '<Header Code="' . $this->Code . '" ' . 'Message="' . $this->Message .'"/>' );
                };

                array_push( $r, '</' . $this -> XMLRoot . '>' );
                $r = implode( '', $r);
                $this->Header( 'Content-Type: application/xml; charset=utf-8' );
                $this -> Builder->End();
            }



            /* Build JSON content */
            if( $this -> FTypeContent == tcJSON || $this -> FContent == null && $this -> FTypeContent==tcAuto )
            {
                $this -> Builder->Begin('JSON result');
                $this -> Header('Content-Type: application/json; charset=utf-8');

                $JSON = (object)[];

                /* Header */
                $JSON -> Header = ( object )
                [
                    'Code'      => encodeURIComponent( $this -> Code ),
                    'Message'   => encodeURIComponent( $this -> Message )
                ];

                /* Outcome params */
                $JSON -> Outcome = $this -> Outcome;

                /* Data record params */
                $JSON -> Data = $this -> Data;

                /* Prepare Content like a JSON param */
                if ( $this -> FContent != '')
                {
                    foreach( $this -> Outcome as $Key => $Value )
                    {
                        switch( gettype( $Value ))
                        {
                            case 'string':
                            case 'integer':
                            case 'double':
                            case 'boolean':
                                $this -> FContent = str_replace( '%' . $Key . '%', $Value, $this -> FContent );
                            break;
                        }
                    }
                    $JSON -> Content = encodeURIComponent( $this -> FContent );
                }

                $r = json_encode( $JSON );
                /*Set header*/
                $this -> Builder -> End();
            }

            /* Build HTML content */
            if ( $this -> FTypeContent==tcHTML || $this -> FContent != '' && $this -> FTypeContent==tcAuto)
            {
                $this -> Log -> Begin('HTML result');
                $this -> header('Content-Type: text/html; charset=utf-8');

                /* добавление Code и Message что бы они подменились */
                $this->SetOutcome( 'Code', $this -> Code );
                $this->SetOutcome( 'Message', $this -> Message );

                /* Build like content */
                if ( $this -> RecordContent != null && array_key_exists( 'Records', $this->Data ))
                {
                    $RecordsArray = [];
                    foreach ( $this -> Data[ 'Records' ] -> Dataset as $Record )
                    {
                        array_push( $RecordsArray, ObjectToContent( $Record, $this -> RecordContent ));
                    }
                    $this -> FContent .= implode( $RecordsArray );
                }

                /* prepare params from outcome */
                foreach ($this->Outcome as $Key => $Value) $this -> FContent = str_replace( '%'.$Key.'%', $Value, $this -> FContent );

                $r = $this -> FContent;
                $this -> Log -> End();
            }
        }

        $this -> Log -> End();

        return $r;
    }



    public function SetRecordContent( $AValue )
    {
        $this -> RecordContent = $AValue;
        return $this;
    }



    public function GetRecordContent()
    {
        return $this -> RecordContent;
    }



    public function SetXMLRoot( $AValue )
    {
        $this -> XMLRoot = $AValue;
        return $this;
    }



    public function AddXMLLine( $AValue )
    {
        array_push( $this -> XMLLines, $AValue );
        return $this;
    }



    public function SetResultDataEnabled( $AValue )
    {
         $this -> ResultDataEnabled = $AValue;
         return $this;
    }



    public function SetMethod( $AValue )
    {
        $this -> Method = $AValue;
        return $this;
    }



    public function SetType( $AValue )
    {
        $this -> Type = $AValue;
        return $this;
    }



    public function SetCall( $AValue )
    {
        $this -> Call = $AValue;
        return $this;
    }



    public function Call()
    {
        if( $this -> IsOk() && ! empty( $this -> Method ))
        {
            $this -> Log -> Begin( 'Call ' . $this -> Type . ' ' .  $this -> Call );
            call_user_func( array( $this, $this -> Method ));
            $this -> Log -> End();
        }
        return $this;
    }



    public function StatTo( $AController )
    {
        array_push( $AController -> Stat, $this );
        if( $AController -> IsOk() ) $this -> ResultTo( $AController );
        return $this;
    }



    public function StatDump()
    {
        $this -> Log -> Begin( 'Controller result' );
        foreach( $this -> Stat as $Record )
        {
            $this -> Log
            -> Info()
            -> Text( $Record -> Call . ' ' )
            -> Text( count( $Record -> LastOk ) > 0 ? implode( ',', $Record -> LastOk ) . ' ' : '', TLog :: ESC_INK_CYAN )
            -> Text( $Record -> GetCode(), $Record -> IsOk() ? TLog :: ESC_INK_GREEN : TLog :: ESC_INK_RED );
        }
        $this -> Log -> End();
        return $this;
    }



    public function &SetAnswer( $AString, $AOkCode = [ rcOk ] )
    {
        if( $this -> IsOk() )
        {
            $Answer = json_decode( $AString, true );
            if( empty( $Answer )) $this -> SetCode( 'AnswerJSONEncodeError', true );
            else
            {
                if( ! array_key_exists( 'Header', $Answer )) $this -> SetCode( 'AnswerFormatIsUnknown' );
                else
                {
                    $Code = $Answer[ 'Header' ]['Code'];
                    if( ! in_array( $Code, $AOkCode ))
                    {
                        $this -> SetResult
                        (
                            ( $Code == rcOk ? 'Code_' : '' ) . $Code,
                            $Answer[ 'Header' ]['Message']
                        );
                    }
                }
            };
        }
        return $this;
    }



    public function CallController
    (
        $ACallName,
        $AType          = TController :: CONTROLLER,
        $AParams        = null,
        $ASilentErrors  = null
    )
    {
        $Result = $this;
        if( $this -> IsOk() )
        {
            $Params = empty( $AParams ) ? $this -> GetParams() : $AParams;

            $Result = TController :: Construct( $this -> Log, $ACallName, $AType )
            -> SetParams( $Params )
            -> Call();

            if( !empty( $ASilentErrors )) $Result -> SetOk( $ASilentErrors );

            $Result -> StatTo( $this );
        }

        return $Result;
    }
}
