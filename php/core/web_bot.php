<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/


namespace catlair;
/*
  2019-02-21 still@itserv.ru
*/


require_once 'params.php';

class TWebBot extends TParams
{
    const HTTP      = 'http';
    const HTTPS     = 'https';

    private $Path       = [];
    private $Get        = null;
    private $Post       = null;
    private $Content    = null;
    private $Answer     = null; /* Object JSON, DOM, or other*/

    private $Handle     = null; /* Current request handle */

    /*
        Constructor
    */
    function __construct( $ALog )
    {
        $this -> Log    = $ALog;
        $this -> Get    = new TParams();
        $this -> Post   = new TParams();
        $this -> SetOk();
    }



    static public function Create( $ALog )
    {
        return new TWebBot( $ALog );
    }



    /*
        Start URL
    */
    public function GetURL()
    {
        $Result =
        $this -> GetString( 'Protocol', self :: HTTP )
        . '://'
        . $this -> GetString( 'Domain', 'catlair.ru' );

        $PathString     = implode( $this -> Path, '/' );

        /* Build GET parameters */
        $Keys = [];
        foreach( $this -> Get -> GetParams() as $Key => $Value )
        {
            array_push( $Keys, $Key . '=' . $Value );
        }
        $ParamsString   = implode( $Keys, '&' );

        if( !empty( $PathString )) $Result .= $PathString;
        if( !empty( $ParamsString )) $Result .= '/?' . $ParamsString;

        return $Result;
    }



    /*
        Start request
    */
    public function Execute()
    {
        $URL = $this -> GetURL();
        $this -> Log -> Begin() -> Param( 'URL', $URL );

        $Handle = curl_init();
        curl_setopt( $Handle, CURLOPT_URL, $URL );

        /* Build POST parameters  for CURL */
        $Keys = [];
        foreach( $this -> Post -> GetParams() as $Key => $Value )
        {
            array_push( $Keys, $Key . '=' . $Value );
        }
        $ParamsString   = implode( $Keys, '&' );
        if( !empty( $ParamsString ))
        {
            curl_setopt( $Handle, CURLOPT_POST, true);
            curl_setopt( $Handle, CURLOPT_POSTFIELDS, $ParamsString );
        }

        curl_setopt( $Handle, CURLOPT_RETURNTRANSFER, 1 );
        /* Send request */
        $this -> Content = curl_exec( $Handle );

        /* Closw curl */
        curl_close( $Handle );

        $this -> Log -> End();
        return $this;
    }



    public function GetContent()
    {
        return $this -> Content;
    }



    public function GetGet()
    {
        return $this -> Get;
    }



    public function GetPost()
    {
        return $this -> Post;
    }



    public function DecodeJSON()
    {
        if( $this -> IsOk() )
        {
            $this -> Answer = json_decode( $this -> Content );
            if( empty( $this -> Answer )) $this -> SetCode( 'JSONError' );
        }
        return $this;
    }



    public function GetAnswer()
    {
        return empty( $this -> Answer ) ? $this -> Content : $this -> Answer;
    }



    public function DecodeDOM()
    {
        if( $this -> IsOk() )
        {
            $Lines = explode( PHP_EOL, $this -> Content );
            $this -> Answer = new DOMDocument();

            $Last = libxml_use_internal_errors(true);
            $this -> Answer -> loadHTML( $this -> Content );

            foreach ( libxml_get_errors() as $error)
            {
                $Line   = $Lines[ $error -> line - 1];
                $Before = substr( $Line, $error -> column - 30, 30 );
                $Place  = substr( $Line, $error -> column, 1 );
                $After  = substr( $Line, $error -> column + 1, 30 );

                $this -> Log
                -> Warning( 'DOM Error' )
                -> Param( 'Line',       $error -> line )
                -> Param( 'Position',   $error -> column )
                -> Param( 'Message',    $error -> message )
                -> Info()
                -> Text( $Before )
                -> Text( $Place, TLog :: ESC_INK_RED )
                -> Text( $After );
            }
            libxml_use_internal_errors( $Last );
        }
        return $this;
    }



    public function CheckDOMTagsValue( $ATag, $AValue )
    {
        $Result = false;
        $Nodes = $this -> Answer -> getElementsByTagName( $ATag );
        foreach( $Nodes as $Node) $Result = $Result || $Node -> textContent;
        return $Result;
    }



    public function CheckDOMTagsExists( $ATag )
    {
        $Nodes = $this -> Answer -> getElementsByTagName( $ATag );
        return ! empty( $Nodes ) && count( $Nodes ) > 0;
    }



    public function SetGetParams( $AParams )
    {
        $this -> Get -> SetParams( $AParams );
        return $this;
    }



    public function SetPostParams( $AParams )
    {
        $this -> Post -> SetParams( $AParams );
        return $this;
    }
}
