<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.


    Controller library for speech.
    Based on festival for speech & lame for conver to mp3
    execute commant text2vawe

    still@itserv.ru
*/

namespace catlair;

require_once( ROOT . '/core/shell.php' );
require_once( ROOT . '/core/result.php' );


class TSpeech extends TResult
{
    private $Voice = 1;
    private $Path = '';
    private $Hash = null;
    private $Text = 'test';



    function __construct( $ALog )
    {
        $this -> Log = $ALog;
        $this -> SetOk();
    }



    /*
        Create speech object
    */
    static function Create( $ALog )
    {
        return new TSpeech( $ALog );
    }



    /*
        Build string
    */
    public function Build()
    {
        if( $this -> IsOk())
        {
            $FullWAV    = $this -> GetFilePath() . '.wav';
            $FullMP3    = $this -> GetResultFileName();

            /* Generate mp3 */

            /* Set */
            if ( ! CheckPath( $this -> Path )) $this -> SetResult( 'PathForSpeachNotFound', $this -> Path );
            else
            {
                if ( file_exists( $FullMP3 ))
                {
                    $this -> Log -> Info( 'File getting form cache' ) -> Param( 'File', $FullMP3 );
                }
                else
                {
                    $this -> Log
                    -> Info( 'Creatring wav' ) -> Param( 'File', $FullWAV )
                    -> Info( 'Creatring mp3' ) -> Param( 'File', $FullMP3 )
                    -> Info( 'Voice' ) -> Param( 'Voice', $this -> Voice );

                    switch ( $this -> Voice )
                    {
                        case '1': $VoiceName    = 'voice_cmu_us_slt_cg'; break;
                        case '2': $VoiceName    = 'voice_cmu_us_aup_cg'; break;
                        case '3': $VoiceName    = 'voice_cmu_us_fem_cg'; break;
                        case '4': $VoiceName    = 'voice_cmu_us_awb_cg'; break;
                        case '5': $VoiceName    = 'voice_cmu_us_ksp_cg'; break;
                        case '6': $VoiceName    = 'voice_kal_diphone'; break;
                        default: $VoiceName     = 'voice_cmu_us_fem_cg'; break;
                    }

                    TShell::Create( $this -> Log )
                    /* Interface for festival */
                    -> Cmd( 'echo "'.$this -> Text.'" | text2wave -eval "('.$VoiceName.')" -o ' . $FullWAV )
                    /* convert to mp3 */
                    -> Cmd( 'lame -f ' . $FullWAV . ' ' . $FullMP3 )
                    -> ResultTo( $this );

                    /* Remove file WAV */
                    if ( file_exists( $FullWAV )) unlink( $FullWAV );
                }
            }
        }

        return $this;
    }



    public function BuildRemote( $AURL )
    {
        $this -> SetHash( null );

        $Answer =  TShell::Create( $this -> Log )
        -> CmdBegin()
        -> CmdAdd( 'curl ')
        -> CmdAdd
        (
            $AURL . '?' . http_build_query
            ([
                'call'  => 'Speech.Build',
                'voice' => $this -> GetVoice(),
                'text'  => $this -> Text
            ]),
            '"'
        )
        -> CmdEnd()
        -> ResultTo( $this )
        -> GetResult();

        if( $this -> IsOk() )
        {
            $Json = $Answer != null && count( $Answer ) > 0 ? json_decode( $Answer[ 0 ], true ) : null;
            if( $Json == null ) $this -> SetCode( 'ErrorReadAnswer' );
            else
            {
                if
                (
                    ! empty( $Json ) &&
                    array_key_exists( 'Outcome', $Json ) &&
                    array_key_exists( 'Hash', $Json[ 'Outcome' ])
                )
                {
                    $this -> SetHash( $Json[ 'Outcome' ][ 'Hash' ] );
                }
                else $this -> SetCode( 'ErrorGetHashForVoiceBuild' );
            }
        }

        return $this;
    }



    /*
        Send result file
    */
    public function Send()
    {
        if ( $this -> IsOk())
        {
            $File = $this -> GetResultFileName();
            if ( file_exists( $File )) clSendFile( basename( $File ), $File );
            else $this -> SetResult( 'FileNotFound', $File );
        }
        return $this;
    }



    /**************************************************************************
        Setters and getters
    */


    public function SetPath( string $AValue )
    {
        $this -> Path = $AValue;
        return $this;
    }



    public function GetPath()
    {
        return $this -> Path;
    }



    private function GetFilePath()
    {
        return $this -> Path . '/' . $this -> GetHash();
    }



    public function GetResultFileName()
    {
        return $this -> GetFilePath() . '.mp3';
    }



    public function SetVoice( $AValue )
    {
        $this -> Voice = $AValue;
        return $this;
    }



    public function GetVoice()
    {
        return $this -> Voice;
    }



    public function SetHash( $AValue = null )
    {
        $this -> Hash = $AValue;
        return $this;
    }



    public function GetHash()
    {
        return $this -> Hash;
    }



    public function SetText( $AValue = null )
    {
        $this -> Text = $AValue;
        if ( $this -> Text != null ) $this -> SetHash( md5( $this -> Text . $this -> Voice ));
        return $this;
    }
}
