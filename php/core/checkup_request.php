<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

/*
    Валидатор документа

    Разработан за период с 2022.10.15 по 2022.10.16 в рамках проекта Catlair для Pusa.
    Выполняет проверку Документа относительно Предоставленной схемы.

    Автор: still@itserv.ru
*/
namespace catlair;

require_once 'checkup.php';



class TCheckupRequest extends TCheckup
{
    /*
        Создание валидатора
    */
    public static function create()
    {
        return new TCheckupRequest();
    }



    /*
        Валидация запроса
    */
    public function validateRequest
    (
        string $AURL    = '',
        array $AGet     = null,
        array $APost    = null,
        array $ACookie = null,
        array $AServer  = null
    )
    {
        $URL = parse_url( $AURL );
        if( $this -> ifResult( !empty( $URL ) ) -> isOk() )
        {
            $Document =
            [
                'get'       => empty( $AGet ) ? $_GET : $AGet,
                'post'      => empty( $APost ) ? $_POST : $APost,
                'cookies'   => empty( $ACookie ) ? $_COOKIE : $ACookie,
                'server'    => empty( $AServer ) ? $_SERVER : $AServer,
                'protocol'  => $URL[ 'scheme' ],
                'host'      => $URL[ 'host' ]
            ];
            /* Заполнение параметров URL */
            if( array_key_exists( 'port', $URL )) $Document[ 'port' ] = $URL[ 'port' ];
            if( array_key_exists( 'user', $URL )) $Document[ 'user' ] = $URL[ 'user' ];
            if( array_key_exists( 'pass', $URL )) $Document[ 'password' ] = $URL[ 'pass' ];
            if( array_key_exists( 'path', $URL )) $Document[ 'path' ] = explode( '/', $URL[ 'path' ] );
            if( array_key_exists( 'fragment', $URL )) $Document[ 'anchor' ] = $URL[ 'fragment' ];

            $this -> validate( $Document );
        }
        return $this;
    }



    /*
        Установка схемы валидации
    */
    public function setSchemeJSON
    (
        $AValue
    )
    {
        if( $this -> isOk() )
        {
            $Scheme = json_decode( $AValue, true );
            if( $this -> ifResult( empty( $Scheme ), 'scheme_is_invalid' ) -> isOK() )
            {
                $this -> setScheme( $Scheme );
            }
        }
        return $this;
    }



    /*
        Валидация JSON документа
    */
    public function validateJSON
    (
        string $ADocument = ''
    )
    {
        if( $this -> isOk() )
        {
            $Document = json_decode( $ADocument, true );
            if( $this -> ifResult( $Document === null, 'document_is_invalid' ) -> isOk() )
            {
                $this -> validate( $Document );
            }
        }
        return $this;
    }
}
