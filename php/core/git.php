<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/


namespace catlair;

/*
    Shell for git
*/


include_once 'shell.php';
include_once 'debug.php';



/*
    Class GIT from TShell
*/
class TGIT extends TShell
{
    /* Merge politics */
    const MERGE_MANUAL      = '';           /* Manual conflict */
    const MERGE_KEEP_DEST   = '-Xours';     /* Keep destination if confllict*/
    const MERGE_KEEP_SOURCE = '-Xtheirs';   /* Keep source it conflict */

    public $Server = 'github';


    /*
        Go to project dir
    */
    public function &SetProjectDir( $AProjectDir )
    {
        if ( chdir( $AProjectDir )) $this -> SetOk();
        else $this -> SetCode( 'UNKNOWN_PATH' );
        return $this;
    }



    /*
        return list of branches as array ['0'=>'branch1', ... ]
    */
    public function &Init()
    {
        $Result = [];
        $this
        -> Log
        -> Info     ()
        -> Text     ( 'Init git' );

        $this
        -> CmdBegin ()
        -> CmdAdd   ( 'git init' )
        -> CmdEnd   ();
        foreach ( $this -> CommandResultLines as $Line )
        {
             $Items = explode( ' ', $Line );
             array_push( $Result, array_pop( $Items ));
        }
        return $this;
    }



    /*
        return list of branches as array ['0'=>'branch1', ... ]
    */
    public function &RemoteAdd( $AURL )
    {
        $Result = [];

        $this
        -> Log
        -> Info     ()
        -> Text     ( 'Remote add' );

        $this
        -> CmdBegin ()
        -> CmdAdd   ( 'git remote add ' . $this->Server . ' ' . $AURL )
        -> CmdEnd   ();
        foreach ( $this -> CommandResultLines as $Line )
        {
             $Items = explode( ' ', $Line );
             array_push( $Result, array_pop( $Items ));
        }
        return $this;
    }



    /*
        return list of branches as array ['0'=>'branch1', ... ]
    */
    public function GetBranchList()
    {
        $Result = [];
        $this
        -> Log
        -> Info     ()
        -> Text     ( 'Build branch list' );

        $this
        -> CmdBegin ()
        -> CmdAdd   ( 'git branch' )
        -> CmdEnd   ();

        foreach ( $this -> CommandResultLines as $Line )
        {
             $Items = explode( ' ', $Line );
             array_push( $Result, array_pop( $Items ));
        }
        return $Result;
    }



    /*
        Return current branch name
    */
    public function GetBranchCurrent()
    {
        $Result = '';

        $this
        -> Log
        -> Info     ()
        -> Text     ( 'Get branch current' );

        $this
        -> CmdBegin ()
        -> CmdAdd   ( 'git branch' )
        -> CmdEnd   ();

        foreach ( $this -> CommandResultLines as $Line )
        {
            $Line = explode( ' ', $Line );
            if ( count( $Line ) > 1 && $Line[ 0 ] == '*' ) $Result = $Line[ 1 ];
        }
        return $Result;
    }



    /*
        Return true if $aBranch:strin exists
    */
    public function BranchExists($ABranch)
    {
        $List = $this->GetBranchList();
        return in_array( $ABranch, $List );
    }



    /*
        Delete branch
    */
    public function &BranchStatus()
    {
        $this
        -> Log
        -> Info     ();

        $this
        -> CmdBegin ()
        -> CmdAdd   ( 'git status' )
        -> CmdEnd   ();
        return $this;
    }



    /*
        Show branchies
    */
    public function &BranchList()
    {
        if ($this->IsOk())
        {
            $this
            -> Log
            -> Info     ()
            -> Text     ( 'Show branches' );

            $this
            -> CmdBegin ()
            -> CmdAdd   ( 'git branch' )
            -> CmdEnd   ();
        }
        return $this;
    }



    /*
        Switch branch
    */
    public function &BranchSwitch( $AIDBranch )
    {
        if ($this->IsOk())
        {
            $this
            -> Log
            -> Info     ()
            -> Param    ( 'Switch to', $AIDBranch );

            $this
            -> CmdBegin ()
            -> CmdAdd   ( 'git checkout ' )
            -> CmdAdd   ( $AIDBranch )
            -> CmdEnd   ();
        }
        return $this;
    }



    /*
        Delete branch
    */
    public function &CreateAndDownloadBranch( $AIDBranch )
    {
        $this
        -> Log
        -> Info     ()
        -> Param    ( 'Create', $AIDBranch )
        -> Param    ( 'and download form', 'origin/'.$AIDBranch );

        $this
        -> CmdBegin ()
        -> CmdAdd   ( 'git checkout -b ' )
        -> CmdAdd   ( $AIDBranch )
        -> CmdAdd   ( ' origin/' )
        -> CmdAdd   ( $AIDBranch )
        -> CmdEnd   ();
        return $this;
    }



    /*
        Create branch
    */
    public function &BranchCreate($AIDBranch)
    {
        if ($this->IsOk())
        {
            $this
            -> Log
            -> Info     ()
            -> Param    ( 'Create', $AIDBranch );

            $this
            -> CmdBegin ()
            -> CmdAdd   ('git checkout -b ')
            -> CmdAdd   ( $AIDBranch )
            -> CmdEnd   ();
        }
        return $this;
    }



    /*
        Delete branch
    */
    public function &BranchDelete($AIDBranch)
    {
        if ($this->IsOk())
        {
            $this
            -> Log
            -> Info     ()
            -> Param    ( 'Delete branch', $AIDBranch );

            $this
            -> CmdBegin ()
            -> CmdAdd   ( 'git branch -D ' )
            -> CmdAdd   ( $AIDBranch )
            -> CmdEnd   ();
        }
        return $this;
    }



    /*
        Download branch $aIDBranch:string
    */
    public function &BranchDownload($AIDBranch)
    {
        if ($this->IsOk())
        {
            $this
            -> Log
            -> Info     ()
            -> Param    ( 'Download branch', $AIDBranch );

            $this
            -> CmdBegin ()
            -> CmdAdd   ( 'git pull' )
            -> CmdAdd   ( ' ' . $this->Server )
            -> CmdAdd   ( ' ' . $AIDBranch)
            -> CmdEnd   ();
        }
        return $this;
    }



    /*
        Upload branch $aIDBranch:string
    */
    public function &BranchUpload( $AIDBranch )
    {
        if ($this->IsOk())
        {
            $this
            -> Log
            -> Info     ()
            -> Param    ( 'Upload branch', $AIDBranch )
            -> Param    ( 'server', $this -> Server );

            $this
            -> CmdBegin ()
            -> CmdAdd   ( 'git push' )
            -> CmdAdd   ( ' ' . $this -> Server )
            -> CmdAdd   ( ' ' . $AIDBranch )
            -> CmdEnd   ();
        }
        return $this;
    }



    /*
        Commit to branch $aIDBranch
    */
    public function &BranchCommit( $aMessage )
    {
        if ($this->IsOk())
        {
            $this
            -> Log
            -> Info     ()
            -> Param    ('Commit to branch with message', $aMessage);

            $this
            -> cmdBegin ()
            -> cmdAdd   ('git commit -m ')
            -> cmdAdd   ('"' . $aMessage . '"')
            -> cmdEnd   ();
        }
        return $this;
    }



    public function CommitLast()
    {
        $Result = null;
        if ($this->IsOk())
        {
            $this
            -> Log
            -> Info     ()
            -> Text     ('Get last coomit ID from HEAD');

            $this
            -> cmdBegin ()
            -> cmdAdd   ('git rev-parse HEAD')
            -> cmdEnd   ();

            if ($this->isOk() && count( $this -> CommandResultLines ) > 0 )
            {
                $Result = $this->CommandResultLines[0];
            }
        }
        return $Result;
    }



    public function &CommitDetale($ACommit)
    {
        if ($this->IsOk())
        {
            $this
            -> Log
            -> Info     ()
            -> Text     ( 'Show commit detail' );

            $this
            -> cmdBegin ()
            -> cmdAdd   ( 'git --no-pager show ' )
            -> cmdAdd   ( $ACommit )
            -> cmdEnd   ();
        }
        return $this;
    }



    /*
        Merge branch $aIDBranch
    */
    public function &MergeFromBranch($AIDBranch, $ARuleMerge)
    {
        if ($this->IsOk())
        {
            $this
            -> Log
            -> Info     ()
            -> Param    ('Merge from ', $AIDBranch);

            $this
            -> CmdBegin ()
            -> cmdAdd   ('git merge ')
            -> cmdAdd   ($aRuleMerge)
            -> cmdAdd   (' ')
            -> cmdAdd   ($aIDBranch)
            -> cmdEnd   ();
        }
        return $this;
    }



    /*
        Add all files for index
    */
    public function &AddAllFiles()
    {
        if ($this->IsOk())
        {
            $this
            -> Log
            -> Info     ()
            -> Text     ('Add all change files in to branch');

            $this
            -> CmdBegin ()
            -> CmdAdd   ('git add -A')
            -> CmdEnd   ();
        }
        return $this;
    }



    /*
        show difference between $aBranch and current branch
        git cherry -v $aBranch
    */
    public function &ShowDifferenceCommit($aBranch)
    {
        if ($this->IsOk())
        {
            $this
            -> Log
            -> Info     ()
            -> Param    ('Show difference commits for ', $aBranch);

            $this
            -> CmdBegin ()
            -> CmdAdd   ('git cherry -v ')
            -> CmdAdd   ($aBranch)
            -> CmdEnd   ();
        }
        return $this;
    }



    /*
        move commit $idCommit to current branch
        cherry-pick --no-commit $idCommit
    */
    public function &CommitToBranch($idCommit, $aMessage)
    {
        if ($this->IsOk())
        {
            $this
            -> Log
            -> Info         ()
            -> Param        ('Apply commit to current branch', $idCommit);

            $this
            -> CmdBegin     ()
            -> CmdAdd       ('git cherry-pick --no-commit ')
            -> CmdAdd       ($idCommit)
            -> CmdEnd       ()
            -> CommitBranch ($aMessage);
        }
        return $this;
    }



    /*
        return difernese commits between current branch and etalon branch
    */
    public function &GetCommitsByEtalon($aBranchEtalon)
    {
        if ($this->IsOk())
        {
            $result = [];
            $this -> showDifferenceCommit($aBranchEtalon);
            if ($this->isOk())
            {
                foreach($this -> commandResultLines as $line)
                {
                    $line = explode(' ',$line);
                    if (count($line)>2) array_push($result, $line[1]);
                }
            }
        }
        return $result;
    }



    /*
        Splice commits for curent branch by $aBranchEtalon
    */
    public function &SpliceCommitsByEtalon($aBranchEtalon, $aMessage)
    {
        if ($this->IsOk())
        {
            $commits = $this -> getCommitsByEtalon($aBranchEtalon);
            $count = count($commits);
            $this->param('Found commits', $count)->end();

            switch ($count)
            {
                case 0:
                    $this -> Code = 'NO_COMMITS_FOR_SPLICE';
                    $this -> Message = 'In branch'
                        . $this->getBranchCurrent()
                        . ' no different commits for '
                        . $aBranchEtalon;
                break;

                case 1:
                break;

                default:
                    $this
                    -> Log
                    -> Info         ()
                    -> Text         ('Splice commits in current branch by')
                    -> Value        ($aBranchEtalon);

                    $this
                    -> CmdBegin     ()
                    -> CmdAdd       ('git reset --soft HEAD~')
                    -> CmdAdd       ((string)$count)
                    -> CmdEnd       ()
                    -> CommitBranch ($aMessage);
                break;
            }
        }
        return $this;
    }
}
