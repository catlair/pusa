<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/


namespace catlair;

/*
    Сбор уникального словаря для поиска и его сохранение
*/

require_once( ROOT . '/core/result.php' );

class TVocab extends TResult
{
    /* Префиксы для сборки словаря */
    const TYPE_ID_PARENT        = "IDP";        /* ID PARENT */
    const TYPE_ID_PARENT_BIND   = "IDPB";       /* IDPARENT+IDBIND */
    const TYPE_ID               = "ID";         /* IDDescript */
    const TYPE_ENABLED          = "E";
    const TYPE_OPENED           = "OP";
    const TYPE_ID_TYPE          = "IDT";        /* Descript Type */
    const TYPE_ID_CLIENT        = "IDCLNT";     /* Client  */
    const TYPE_ID_USER_INSERT   = "IDUI";       /* ID User Insert */
    const TYPE_RIGHT_SELECT     = "RS";         /* Descript Type */
    const TYPE_DT_ACTUAL        = "DTA";        /* DTActual */
    const TYPE_DT_INSERT        = "DTI";        /* DTActual */

    const PT_FIND               = "PTFind";
    const PT_LIKE               = "PTLike";
    const PT_STRICT             = "PTStrict";

    const AND                   = "+";
    const LIKE                  = "*";

    const VOCAB_MYSQL           = "MySql";


    private $WordNew            = []; /* Добавляемые */



    function __construct( $ALog, $ATypeVocab = null )
    {
        $this -> Log        = $ALog;
        $this -> TypeVocab  = $ATypeVocab;
        $this -> SetOk();
    }



    public static function Create( $ALog, $ATypeVocab = null )
    {
        $Result = new TVocab( $ALog, $ATypeVocab );
        return $Result -> Begin();
    }



    /*
       Запуск индексации для указанного ALink
    */
    public function &Begin()
    {
        $this -> WordNew        = [];
        $this -> SetOk();
        return $this;
    }




    /*
        Завершение парсинга и сохранение результатов.
        TODO это надо удалить из проекта
    */
    public function &End($ATypeVocab = null)
    {
        return $this;
    }



    /*
        Возвращает буффер
    */
    public function GetBuffer()
    {
        if( count( $this->WordNew ) == 0 ) $this -> SetCode( 'EmptyVocab' );

        if( $this -> TypeVocab == self::VOCAB_MYSQL )
        {
            foreach( $this->WordNew as &$Word )
            {
                if ( strlen( $Word ) > 0 && strlen( $Word ) < 3 && preg_match( '/\w/', $Word )) $Word = $Word . 'HHHHHH';
            }
        }

        return implode( ' ', $this -> WordNew );
    }




    /*
        Добавляет группу строк к словарю
    */
    public function &AddGroup( $AGroup, $ATypeText, $AType, $APre = '+', $APost = null )
    {
        array_push( $this -> WordNew, '+(' );
        foreach( $AGroup as $Record)
        {
            $this -> FilterPars( $Record,  $ATypeText, $AType, $APre, $APost );
        }
        array_push( $this -> WordNew, ')' );
        return $this;
    }



    /*
        Парсинг текста
        добавляет в индексацию новый перечень слов из текста
        $AText - текст
        $AType - тип разбиеня на слова
    */
    public function &Pars( $AText, $AType )
    {
        return $this -> ParsExt( $AText, $AType, null, null);
    }



    public function &ParsExt( $AText, $AType, $APre, $APost)
    {
        $Income = $this -> TextToWord($AText, $AType);
        if ($APre != null || $APost != null)
        {
            if( $APre == null ) $APre = "";
            if( $APost == null ) $APost = "";
            foreach($Income as $key => $value)
            {
                $Income[$key] = $APre.$Income[$key].$APost;
            }
        }
        $this->WordNew = array_merge($this->WordNew, $Income);
        $this->WordNew = array_unique($this->WordNew);
        return $this;
    }



    /*
        оборачивание строго фильтруемых эелементов и занесение их в виде md5
     */
    public function &FilterPars( $AText, $ATypeText, $AType = TVocab :: PT_LIKE, $APre = '+', $APost = null )
    {
        if( $AText != null )
        {
            $this -> ParsExt( 'B' . $ATypeText . '_' . md5( $AText ) . '_E' . $ATypeText, $AType, $APre, $APost );
        }
        return $this;
    }



    /*
        генерация ключа даты
     */
    public function &FilterDTPars( $ADT, $ATypeText, $AFormat = 'YmdHis', $AType = TVocab :: PT_LIKE, $APre = '+', $APost = '*' )
    {
        $DT = new TMoment();
        if (empty($ADT)) $ADT = "";
        else
        {
            $ADT = $DT -> FromString ($ADT) -> ToString($AFormat);
            $ADT = (empty($ADT))?"":"B".$ATypeText."_".$ADT.(!empty($APost)?"":"_E".$ATypeText);
        }
        return $this -> ParsExt ($ADT, $AType, $APre, $APost);
    }



    /*
        Преобразование текста в массив слов
        Принцип разбиения зависит от $AType ptLike, ptStrict, ptFind
        Удаляются все знаки препинания и иные символы за исключением алфавита цифр и _
    */
    private function &TextToWord( $AText, $AType )
    {
        $AText = trim( $AText );

        /* В случае если обрааботка производится для поисковой строки */
        switch( $AType )
        {
            case self::PT_FIND:
                $AText = preg_replace('/:/','',$AText);
                /* Очистка текста от знаков припинания и прочего */
                $AText = preg_replace('/[^a-zA-Zа-яА-Я0-9_#]/ui', ' ', mb_strtolower($AText/*, 'UTF-8'*/));
                $AText = preg_replace('/  +/',' ',$AText);
                $Result = explode(' ', $AText);
                foreach($Result as $Key=>$Value)
                {
                    if (strpos($Value,'#')===0) $Result[$Key]=clIndexTagString($Value);
                }
            break;
            case self::PT_LIKE:
                /* Очистка текста от знаков припинания за исключением _ */
                $AText = preg_replace('/[^a-zA-Zа-яА-Я0-9_]/ui', ' ', mb_strtolower($AText/*, 'UTF-8'*/));
                $AText = preg_replace('/  +/',' ',$AText);
                $Result = explode(' ', $AText);
            break;
            case self::PT_STRICT:
                /* Очистка текста от знаков припинания за ислючением .,_ замена их на '' */
                $AText = preg_replace('/[^a-zA-Zа-яА-Я0-9.,_]/ui', '', mb_strtolower($AText, 'UTF-8'));
                $AText = '#'.$AText;
                $Result = explode(' ', $AText);
            break;
            default:
//                $Result = [];
            break;
        }

        /* Создание массива уникальных слов */
        $Result = array_unique( $Result );

        /* Удаляем пустые эелементы */
        if (($key = array_search('', $Result)) !== false) {
            unset($Result[$key]);
        }

        /* Возвращаем массив результат */
        return $Result;
    }
}
