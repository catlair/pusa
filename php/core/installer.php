<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

/*
    Installer
    09.10.2019 still@itserv.ru
*/

namespace catlair;

include_once ROOT . '/core/config.php';
include_once ROOT . '/core/cli_util.php';
include_once ROOT . '/core/update.php';
include_once ROOT . '/core/moment_util.php';

class TInstaller
{
    /* Type update */
    const UPDATE_UNIVERSE = 'Universe'; /* Universal update from PathUniversalUpdate*/
    const UPDATE_SPECIFIC = 'Specific';

    /* Internal paremeters of class */
    private $Log = null; /* Logger */
    private $Config = null; /* Config will creaded it Constructor */
    private $PathSpecific = './update';
    private $PathUniverse = './update';



    /* Constructor */
    function __construct($ALog, $APathUniverse, $APathSpecific)
    {
        $this->Log = $ALog;
        if ($APathUniverse!==null) $this->PathUniverse = $APathUniverse.'/update';
        if ($APathSpecific!==null) $this->PathSpecific = $APathSpecific.'/update';
        $this->Config = new TConfig();
        $ConfigFile = $this->PathSpecific . '/version.json';
        $this->Config->SetFile($ConfigFile);
        $this->Log->Debug()->Param('Version config', $ConfigFile);
    }



    /*
        Return file name by Update:[ID, Type]
    */
    private function GetFile($AUpdate)
    {
        switch ($AUpdate->Type)
        {
            case self::UPDATE_SPECIFIC: $Result = $this->PathSpecific; break;
            default:
            case self::UPDATE_UNIVERSE: $Result = $this->PathUniverse; break;
        }
        return $Result . '/'. $AUpdate->ID . '.update/' . 'index.php';;
    }



    /*
        Create new update from template
    */
    public function &Create($AType)
    {
        /* Create ID */
        $ID = clNowToString('Ymd_His');
        $File = $this->GetFile((object)['ID'=>$ID, 'Type'=>$AType]);

        /* Create path */
        $FilePath = dirname($File);
        if (!file_exists($FilePath)) mkdir($FilePath, FILE_RIGHT, true);

        /* Prepare content */
        $TemplateFile=clRootPath().'/php/update_template.php';
        if (!file_exists($TemplateFile)) $this->Log->Error()->Param('Template file not exists');
        {
            $Content = file_get_contents($TemplateFile);
            $Content = str_replace('%ID%', $ID, $Content);
        }

        /* Write content */
        if (file_put_contents($File, $Content)) $this->Log->Info()->Param('Create file',$File);
        else $this->Log->Error()->Param('Error create file',$File);

        /* Return this */
        return $this;
    }



    /*
        Return list of updates from universe and specific
        Each record contain object
            ID:string - identify
            Type:[self::UPDATE_SPECIFIC,self::UPDATE_UNIVERSE] - type of update
    */
    private function BuildUpdateList()
    {
        /* Collect updates from universe and specific pathes */
        $Dump = [];

        /* Collect specific update */
        foreach (glob($this->PathSpecific .'/'.'*.update') as $File)
        {
            $ID = pathinfo($File)['filename'];
            $Dump[$ID] = (object)['ID'=>$ID, 'Type' => self::UPDATE_SPECIFIC];
        }

        /* Collect universe update */
        foreach (glob($this->PathUniverse.'/'.'*.update') as $File)
        {
            $ID = pathinfo($File)['filename'];
            $Dump[$ID] = (object)['ID'=>$ID, 'Type' => self::UPDATE_UNIVERSE];
        }

        /* Build list */
        $List = [];
        foreach ($Dump as $Key=>$Result) array_push($List, $Result);

        /* Sorting */
        usort
        (
            $List,
            function($a,$b)
            {
                if ($a->ID > $b->ID) $r=1;
                else if ($a->ID < $b->ID) $r=-1;
                    else $r=0;
               return $r;
            }
        );

        /* Return result */
        return $List;
    }



    /*
        Find update by ID:string.
    */
    private function Find($AArray, $AID)
    {
        $Result = null;
        foreach($AArray as $Index => $Object) if ($Object->ID==$AID) $Result = $Index;
        return $Result;
    }



    public function GetCurrentID()
    {
        $Config = $this->Config->Read()->GetParams();
        if (!property_exists($Config, 'VersionList')) $Config->VersionList = [];
        if (count($Config->VersionList)>0) $ID=$Config->VersionList[count($Config->VersionList)-1];
        else $ID = null;

        return $ID;
    }



    /*

    */
    public function GetNextUpdate()
    {
        $Result=null;

        $List = $this->BuildUpdateList();
        $Length = count($List);
        if ($Length > 0)
        {
            $Current = $this->GetCurrentID();

            /* if Curent not found then it will be first update */
            if ($Current===null) $Result = $List[0];
            else
            {
                /* else find next update */
                $Index = $this->Find($List, $Current);
                if ($Index!==null && $Index<$Length-1) $Result = $List[$Index+1];
            }
        }

        return $Result;
    }



    /*
        Return last update
    */
    public function GetLastUpdate()
    {
        $Result=null;

        $List = $this->BuildUpdateList();
        $Length = count($List);
        if ($Length > 0)
        {
            $Current = $this->GetCurrentID();
            $Index = $this->Find($List, $Current);
            if ($Index!==null) $Result = $List[$Index];
        }

        return $Result;
    }



    /*
        Install next update
    */
    public function InstallNext()
    {
        $Result = false;
        $Next = $this->GetNextUpdate();

        if ($Next==null)
        {
            $this->Log->Info()->Text('No aviable update');
            $Result = 'NoUpdatesForInstall';
        }
        else
        {
            $this->Log->Info()->Param('Found next update', $Next->ID);

            $File = $this->GetFile($Next);
            if (!file_exists($File)) $this->Log->Error()->Param('Update not found', $File);
            {
                $this->Log->Begin();
                $UpdateName = 'TUpdate_'.$Next->ID;

                /* Set new error handler for catch errors in user module */
                set_error_handler
                (
                    function($errno, $errstr, $errfile, $errline, $errcontext)
                    {
                        $this
                        ->Log
                        ->Error()->Text('Install exception')->Param($errno,$errstr)->Param('File',$errfile)->Param('line',$errline);
                        $Result = $errno;
                    }
                );

                $this->Log->Info()->Param('ID', $Next->ID);
                $this->Log->Info()->Param('File', $File);
                /* Include and initislization */
                include_once $File;
                $Update = new $UpdateName($Next->ID, dirname($File), $this->Log);
                $this->Log->Info()->Param('Caption', $Update->Caption);
                $Update->Code = rcOk;
                $Update->Init();

                /* If initialization is successfull then install */
                if ($Update->Code != rcOk)
                {
                    $this
                    -> Log
                    -> Warning( 'Initialization error' )
                    -> Param( 'Code', $this->Result );
                }
                else
                {
                    $Update->Install();
                    $Result= $Update->Code;
                }

                /* Restore error handler */
                restore_error_handler();

                if ($Result != rcOk) $this->Log->Warning()->Text('Update not install')->Param('Result', $Update->Code);
                else
                {
                    $this->Log->Info()->Text('Update install successfull');
                    /* Write version */
                    $Config = $this->Config->Read()->GetParams();
                    if (!property_exists($Config, 'VersionList')) $Config->VersionList = [];
                    array_push($Config->VersionList, $Next->ID);
                    $this->Config->Flush();
                }
                $this->Log->End();
            }
        }
        return $Result;
    }



    /*
        Uninstull last Update
    */
    public function UninstallLast()
    {
        $Result = rcUnknown;

        $Last = $this->GetLastUpdate();
        if ($Last === null)
        {
            $this->Log->Info()->Text('No updates');
            $Result = 'NoUpdatesForUninstall';
        }
        else
        {
            $this->Log->Info()->Param('Found Update', $Last->ID);
            $File = $this->GetFile($Last);

            if (!file_exists($File)) $this->Log->Error()->Param('Update not found', $File);
            {
                $this->Log->Begin();
                $UpdateName = 'TUpdate_'.$Last->ID;

                set_error_handler
                (
                    function($errno, $errstr, $errfile, $errline, $errcontext)
                    {
                        $this->Log->Error()->Text('Uninstall excaption')->Param($errno,$errstr)->Param('File',$errfile)->Param('line',$errline);
                        $Result=$errno;
                    }
                );

                $this->Log->Info()->Param('ID', $Last->ID);
                $this->Log->Info()->Param('File', $File);
                include_once $File;
                $Update = new $UpdateName($Last->ID, dirname($File), $this->Log);
                $this->Log->Info()->Param('Caption', $Update->Caption);
                $Update->Code = rcOk;
                $Update->Init();

                if ($Update->Code != rcOk) $this->Log->Warning()->Text('Initialization error')->Param('Code', $Update->Code);
                else
                {
                    $Update->Uninstall();
                    $Result = $Update->Code;
                }

                /* Restore error handler */
                restore_error_handler();

                if ($Result != rcOk) $this->Log->Warning()->Text('Update not uninstall')->Param('Result', $Update->Code);
                else
                {
                    $this->Log->Info()->Text('Update uninstall successfull');
                    /* Write version */
                    $Config = $this->Config->Read()->GetParams();
                    if (!property_exists($Config, 'VersionList')) $Config->VersionList = [];
                    array_pop($Config->VersionList);
                    $this->Config->Flush();
                }
                $this->Log->End();

            }
        }
        return $Result;
    }



    public function &InstallAll()
    {
        $Countinue = rcOk;
        while ($Countinue == rcOk) $Countinue = $this->InstallNext();
        return $this;
    }



    /*
        Uninstall all updates while not error
    */
    public function &UninstallAll()
    {
        $Countinue = rcOk;
        while ($Countinue == rcOk) $Countinue = $this->UninstallLast();
        return $this;
    }
}
