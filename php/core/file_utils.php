<?php

/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

/*
    The file utils
    still@itserv.ru 21.11.2021
*/

namespace catlair;

require_once 'debug.php';
require_once 'utils.php';



/*
    Replace text content in the file
*/
function clFileReplace
(
    $AFile,     /* File name */
    $ASearch,   /* Nidle */
    $AReplace   /* Replacement */
)
{
    $Result = false;

    if( file_exists( $AFile ))
    {
        $Content = file_get_contents( $AFile );
        $ReplacedContent = str_replace( $ASearch, $AReplace, $Content );
        if( $Content != $ReplacedContent )
        {
            $Result = file_put_contents( $AFile, $ReplacedContent );
        }
    }

    return $Result;
}



/*
    Recursive file replacement
*/
function clFilesReplace
(
    $ALog,
    $APath,
    $ASearch,
    $AReplace
)
{
    $ALog -> Begin( 'Replace' );
    clFileScan
    (
        $APath,
        null,
        function( $FileName ) use ( $ALog, $ASearch, $AReplace )
        {
            $ALog -> Trace();
            if( clFileReplace( $FileName, $ASearch, $AReplace ))
            {
                $ALog -> Text( 'Replaced ', TLog::ESC_INK_YELLOW );
            }
            else
            {
                $ALog -> Text( 'Keeped   ', TLog::ESC_INK_SKY );
            }
            $ALog -> Text( $FileName );
        }
    );
    $ALog -> End();
    return true;
}
