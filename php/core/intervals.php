<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

namespace catlair;

/*
    Модуль работы с интервалами
    Содержит перечень список объектов TInterval

    07.09.2022 still@itserv.ru igorptx@gmail.com
*/


require_once( 'result.php' );
require_once( 'interval.php' );



class TIntervals extends TResult
{
    private $List = [];



    /*
        Create and return new intervals
    */
    static public function Create()
    {
        return new TIntervals();
    }



    /*
        Return true if interals is empty else return false
    */
    public function IsEmpty()
    {
        return count( $this -> List ) == 0;
    }



    /*
        Add new interval in to list of intervals
    */
    public function Add
    (
        TInterval $AInterval
    )
    {
        array_push( $this -> List, $AInterval );
        return $this;
    }



    /*
        Clear the intervals list
    */
    public function Clear()
    {
        $this -> List = [];
        return $this;
    }



    /*
        Return the list object with TIntervals
    */
    public function GetList()
    {
        return $this -> List;
    }



    /*
        Return the count of intervals from list
    */
    public function GetLength()
    {
        return count( $this -> List );
    }



    /*
        Return Interval by index
        Can return null value for index is outrange
    */
    public function GetInterval
    (
        int $AIndex /* Index of interval */
    )
    {
        return ( $AIndex < 0 || $this -> GetLength() >= $AIndex )
        ? null
        : $this -> List[ $AIndex ];
    }




    public function Clone()
    {
        $Result = TIntervals::Create();
        foreach( $this -> List as $Interval )
        {
            $Result -> Add( $Interval -> Copy() );
        }
        return $Result;
    }





    public function CopyFrom
    (
        TIntervals $AIntervals
    )
    {
        $this -> Clear();
        foreach( $AIntervals -> List as $Index => $Interval )
        {
            $this -> Add( $Interval -> Copy() );
        }
        return $this;
    }



    public function Join
    (
        TIntervals $AIntervals
    )
    {
        foreach( $AIntervals -> List as $Interval )
        {
            $this -> Add( $Interval );
        }
        return $this;
    }



    /*
        Устанавливает для всех интервалов оператор
        Оператор - число на которое изменяется баланс Интервалов при обнаружени
        начала и конца отрезка.
        Оператор используется для выполнения операций Merge, Optimize и тд
    */
    private function SetOperator
    (
        int $AOperator  /* Оператор */
    )
    {
        foreach( $this -> List as $Index => $Interval )
        {
            $Interval -> SetOperator( $AOperator ) ;
        }

        return $this;
    }



    /*
        Return Interval with begin or end nearest to $AValue from right side

        1.
        -------b+++++e-------b+++*===>e------b+++++e----------> t
                                 ^    ^
                           $AValue    RESULT

        2.
        -------b+++++e-------b++++++++e--*==>b+++++e----------> t
                                         ^   ^
                                   $AValue   RESULT
    */
    public function GetMore
    (
        float $AValue /* The value for searching nearest */
    )
    {
        $Result =
        [
            'Balans'    => 0,          /* + Begins; -Ends; 0 */
            'Value'     => INF,
            'Cut'       => false
        ];

        foreach( $this -> List as $Index => $Interval )
        {
            $b = $Interval -> GetBegin();
            $e = $Interval -> GetEnd();

            if( $Interval -> IsValid() && $b < $e )
            {
                if( $e > $AValue && $Result[ 'Cut' ])
                {
                    /*
                        Рссчет баланса для начала интервала
                        -inf ....*...b====e............. +inf
                                 ^AValue  ^R[ Value ]
                    */
                    if( $e == $Result[ 'Value' ])
                    {
                        $Result[ 'Balans' ] -= $Interval -> GetOperator();
                    }

                    /*
                        Рассчет баланса для конца интеркала
                        -inf ....*...b====e............. +inf
                                 ^AValue
                                     ^R[ Value ]
                    */
                    if( $b == $Result[ 'Value' ] )
                    {
                        $Result[ 'Balans' ] += $Interval -> GetOperator();
                    }
                }

                /**/
                if( $e > $AValue && ( $e < $Result[ 'Value' ] || !$Result[ 'Cut' ] && $e == +INF ))
                {
                    $Result[ 'Balans' ] = - $Interval -> GetOperator();
                    $Result[ 'Value' ]  = $e;
                    $Result[ 'Cut' ]    = true;
                }

                /* Found begin more left limit and less last more */
                if(( $b > $AValue || !$Result[ 'Cut' ] && $b == -INF ) && $b < $Result[ 'Value' ])
                {
                    $Result[ 'Balans' ] = + $Interval -> GetOperator();
                    $Result[ 'Value' ]  = $b;
                    $Result[ 'Cut' ]    = true;
                }
            }
        }
        return $Result;
    }



    /*
        Переработка интервалов на основании рассчитанного баланса
        Функция проходит имеющийся перечень интервалов.
        При обнаружении перехода баланса через 0 для очередного интервала выполняется
        создание новго интервала в результирующем списке.
    */
    private function Operation
    (
        $ALimit = 0
    )
    {
        $Result     = TIntervals::Create();
        $Current    = - INF;
        $Balans     = 0;
        $NewBegin   = null;
        $NewEnd     = null;

        do
        {
            $More = $this -> GetMore( $Current );

            if( $More[ 'Cut' ] )
            {
                /* Начало отрезка переход от -0 к + */
                if( $Balans <= $ALimit && $Balans + $More[ 'Balans' ] > $ALimit ) $NewBegin = $More[ 'Value' ];

                /* Конец отрезка переход от + к -0 */
                if( $Balans > $ALimit && $Balans + $More[ 'Balans' ] <= $ALimit ) $NewEnd = $More[ 'Value' ];

                /* Create interval */
                if( $NewBegin !== null && $NewEnd !== null )
                {
                    $Result -> Add( TInterval::CreateByPeriod( $NewBegin, $NewEnd ));
                    $NewBegin = null;
                    $NewEnd = null;
                }

                $Balans = $Balans + $More[ 'Balans' ];
                $Current = $More[ 'Value' ];
            }
        }
        while( $More[ 'Cut' ] );

        $this -> List = $Result -> GetList();

        return $this;
    }



    /*
        Оптимизация интервалов
        Все пересекающиеся интервалы объединяются в один
        В результате возвращается список непересекающихся интервалов

        |this      +=====-
        |          .              +======-
        |          .                     .     +===-
        |          .   +===-             .     .   .
        |          . +==============-    .     .   .
                   .                     .     .   .
        Result     *=====================*     *===*
                0  1 2 3 2 1      2 1    0     1   0  0
    */
    public function Optimize()
    {
        return $this
        -> SetOperator( +1 )
        -> Operation();
    }



    /*
        Return merge
        this       +=====-        +======-     +===-
                   .   +===-             .     .   .
                   .                     .     .   .
        AInterval  . +==============-    .     .   .
                   .                     .     .   .
        Result     *=====================*     *===*
                0  1 2 3 2 1      2 1    0     1   0  0
    */
    public function Merge( $AInterval )
    {
        return $this
        -> Add( $AInterval )
        -> Optimize();
    }



    /*
        Return minus
        this       +=====-        +======-     +===-
                   .     .               .     .   .
                   .   +====-            .     .   .
                   .   . .               .     .   .
        AInterval  . -==============+    .     .   .
                   . . . .          .    .     .   .
        Result     *=* *=*          *====*     *===*
                0  1 0 1 0 -1     0 1    0     1   0
    */
    public function Exclude
    (
        TInterval $AInterval
    )
    {
        return $this
        -> SetOperator( +1 )
        -> Add( $AInterval -> SetOperator( -1 ) )
        -> Operation();
    }



    /*
        Выполняет исключение множества интервалов
    */
    public function Excludes
    (
        TIntervals $AIntervals
    )
    {

        $this -> SetOperator( +1 );
        foreach( $AIntervals -> List as $Index => $Interval )
        {
            $this -> Add( $Interval -> SetOperator( -1 ) );
        }
        return $this -> Operation();
    }



    /*
        Return difference
        this           -======+      -===+  -=====+   -===+
                       .   -=====+   .   .  .
                       .         .   .   .  .
        AInterval   +==========================-
                    .  .         .   .   .  .
        Result      *==*         *===*   *==*
                0   1  0  -1  0  1   0   1  0  1  0  -1   0  0
    */
    public function Difference
    (
        TInterval $AInterval
    )
    {
        return TIntervals::Create()
        -> CopyFrom( $this )
        -> SetOperator( -1 )
        -> Add( $AInterval -> SetOperator( +1 ))
        -> Operation();
    }



    /*
        Return Intersection

        Return Intersection Border
        this           +=====-       +===-  +=====-   -===+
        AInterval   +==========================-
        Result         *=====*       *===*  *==*
    */
    public function Intersection
    (
        TInterval $AInterval    /* Интервал маска для расчета пересечения */
    )
    {
        return $this
        -> Optimize()
        -> Add( $AInterval -> SetOperator( +1 ))
        -> Operation( 1 );
    }



    /*
        Выполняет исключение множества интервалов

        this           +=====-       +===-  +=====-   -===+
        AIntervals  -==========+   +==========-
        Result         *=====*       *===*  *=*
    */
    public function Intersections
    (
        TIntervals $AIntervals
    )
    {
        return $this
        -> Optimize()
        -> Join( $AIntervals -> Clone() -> Optimize() )
        -> Operation( 1 );
    }




    /*
        Округление начала и конца интервалов до временного зерна
        Если зерно 0 то округление не производится

        this        |    ===A==          |                  |
                    |              ===B==|==                |
                    *Зерно               *Зерно             *Зерно
        Result      |=========A==========|                  |
                    |====================B==================|
    */
    public function Round
    (
        $ASeed = 0  /* Временное зерно */
    )
    {
        foreach( $this -> List as $Interval )
        {
            $Interval -> Round( $ASeed );
        }
        return $this;
    }



    /*
        Возвращает сумму интервалов в пределах $AInterval
        $this         *===========* *=========*
        $AInterval           *===========*
        Result          sum( *====* *====* )
    */
    public function Sum
    (
        TInterval   $AInterval = null
    )
    {
        $Source = empty( $AInterval )
        ? $this
        : TIntervals::Create() -> CopyFrom( $this ) -> Intersection( $AInterval );

        $Result = 0;
        foreach( $Source -> List as $Interval )
        {
            $b = $Interval -> GetBegin();
            $e = $Interval -> GetEnd();

            if( $b != -TInterval::MAX_VALUE && $e != TInterval::MAX_VALUE )
            {
                $Result += $e - $b;
            }
            else
            {
                $Result = INF;
            }
        }
        return $Result;
    }



    /*
        Возвращает true если переданный момент попадает хотя бы в один из интервалов
    */
    public function Test
    (
        float $AValue
    )
    {
        $Result = false;
        foreach( $this -> List as $Index => $Interval )
        {
            $Result = $Result || ( $Interval -> Test( $AValue ) == CL_EQUAL );
        }
        return $Result;
    }



    /**************************************************************************
        Работа с моментами
    */


    /*
        Формирование момента из строки
    */
    public function FromMomentString
    (
        ?string $AString,
        int $AOperator = 0
    )
    {
        $AString = trim( $AString );
        if (!empty($AString))
        {
            $ALines = explode( ';', $AString );
            foreach( $ALines as $Index => $Value )
            {
                if( ! empty( $Value ))
                {
                    $Interval = TInterval::Create() -> FromMomentString( trim( $Value ));
                    if( $AOperator != 0 )
                    {
                        $Interval -> SetOperator( $AOperator );
                    }
                    $this -> Add( $Interval );
                }
            }
        }
        return $this;
    }



    /*
        Convert intervals to ODBC string list
    */
    public function ToStringODBCArray()
    {
        $Result = [];
        foreach( $this -> List as $Index => $Interval )
        {
            array_push
            (
                $Result,
                [
                    TMoment::Create() -> Set( (int) $Interval -> GetBegin() ) -> ToStringODBC(),
                    TMoment::Create() -> Set( (int) $Interval -> GetEnd() ) -> ToStringODBC()
                ]
            );
        }
        return $Result;
    }



    /*
        Возвращает true если переданный момент попадает хотя бы в один из интервалов
    */
    public function TestMoment
    (
        TMoment $AMoment
    )
    {
        $Result = false;
        foreach( $this -> List as $Index => $Interval )
        {
            $Result = $Result || ( $Interval -> TestMoment( $AMoment ) == CL_EQUAL );
        }
        return $Result;
    }



    /*
        Week schedule
        [
            [ '00:00 > 00:00', '00:00 > 00:00', ... ],   Monday line
            [ '00:00 > 00:00', ... ],                    Thusday line
            ...
        ]
    */
    public function CreateWeek
    (
        TInterval $AInterval,
        array $AWeek            /* A week schedule */
    )
    {
        $this -> Clear();
        $d = TMoment::Create() -> Set( $AInterval -> GetBegin() ) -> GetDayBegin();

        while( $d -> Get() < $AInterval -> GetEnd() )
        {
            $DayNumber = $d -> GetDayWeekNumber();
            /* Correct the saturday to 6 day */
            $DayNumber = $DayNumber == 0 ? $DayNumber = 6 : $DayNumber - 1;

            if( count( $AWeek ) > $DayNumber )
            {
                $DaySchedule = $AWeek[ $DayNumber ];
                foreach( $DaySchedule as $Schedule )
                {
                    $this -> Add( TInterval::Create() -> FromTimeString( $Schedule, $d ) );
                }
            }
            $d -> Add( TMoment::DAY );
        }
        return $this
        -> Optimize()
        -> Intersection( $AInterval )
        ;
    }



    /*
        Вывод интервала
        Возвращает массив расписания с количеством времени в каждой ячейке
        Линии длинной Line
        Ячейка длинной Step
    */
    public function BuildSchedule
    (
        TInterval   $AInterval,             /* Интервал для сборки расписания */
        float       $ALine = TMoment::DAY,  /* Длительность линии */
        float       $AStep = TMoment::HOUR  /* Длительность ячейки */
    )
    {
        $Result = [];
        $d = $AInterval -> GetBegin();

        while( $d < $AInterval -> GetEnd() )
        {
            $Line = [];             /* Массив для очередной линии */
            $LineEnd = $d + $ALine;
            while( $d < $LineEnd )
            {
                array_push( $Line, $this -> Sum( TInterval::Create() -> Set( $d, $d + $AStep )) );
                $d += $AStep;
            }
            array_push( $Result, $Line );
        }
        return $Result;
    }



    public function DumpSchedule
    (
        TInterval   $AInterval,
        float       $ALine = TMoment::DAY,
        float       $AStep = TMoment::HOUR
    )
    {
        $Result = '';

        /* Build schedule */
        $Schedule = $this -> BuildSchedule( $AInterval, $ALine, $AStep );

        /* Fill characters */
        $Chars = [ ' ', '░', '▒', '▓', '█' ];

        /* Dump schedule */
        foreach( $Schedule as $Line )
        {
            foreach( $Line as $Cell )
            {
                $Result .= $Chars[ (int) ( $Cell / $AStep * 4 ) ];
            }
            $Result .= PHP_EOL;
        }

        return $Result;
    }



    public function LogMomentToConsole($Log)
    {
        foreach( $this -> List as $Index => $Interval )
        {
            $Log -> Debug() -> Value( TMoment::Create() -> Set( $Interval -> GetBegin() <= -TInterval::MAX_VALUE ? 0 : $Interval -> GetBegin() ) -> ToStringODBC() . ' -> ' .  TMoment::Create() -> Set( $Interval -> GetEnd() >= TInterval::MAX_VALUE ? 0 : $Interval -> GetEnd() ) -> ToStringODBC() . '(' . $Interval -> GetOperator() . ')');
        }
    }
}
