<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

namespace catlair;


/*
    Params
*/



require_once( 'moment.php' );



class TParams extends TResult
{
    const VALUE_DEFAULT = null;

    /* TODO make this Params like private */
    private $Params = [];



    /*
        Create new Params
    */
    static public function New
    (
        array $AValues = []
    )
    {
        $Result = new TParams();
        $Result -> Params = empty( $AValue ) ? [] : $AValue;
        return $Result;
    }



    /*
        Set param value by name
    */
    public function SetParam
    (
        string $AKey,
        $AValue = null
    )
    {
        $this -> Params[ $AKey ] = $AValue;
        return $this;
    }



    /*
        Return value by name or default value
    */
    public function GetParam
    (
        string $AKey,
        $ADefault = null
    )
    {
        return
        array_key_exists( $AKey, $this -> Params )
        ? $this -> Params[ $AKey ]
        : $ADefault;
    }



    /*
        Return params list $Name => $Value
    */
    public function &GetParams()
    {
        return $this -> Params;
    }




    /*
        Set params from
    */
    public function SetParams
    (
        $AParams = []
    )
    {
        switch( gettype( $AParams ))
        {
            case 'array':
                $this -> Params = $AParams;
            break;
            case 'object':
                $this -> Params = (array) $AParams;
            break;
            default:
                $this -> Params = [];
            break;
        }
        return $this;
    }



    /*
        Add params from
    */
    public function AddParams
    (
        array $AParams = []
    )
    {
        switch( gettype( $AParams ))
        {
            case 'array':
                $this -> Params = array_merge( $this -> Params, $AParams );
            break;
            case 'object':
                $this -> Params = array_merge( $this -> Params, (array) $AParams );
            break;
        }
        return $this;
    }



    /*
        Clear params
    */
    public function Clear()
    {
        $this -> Params = [];
        return $this;
    }



    /*
        Remove parameter by name
    */
    public function DeleteParam( $AName )
    {
        if( array_key_exists( $AName, $this -> Params )) unset( $this -> Params[ $AName ]);
        return $this;
    }



    public function ParamFrom( $AName, $ASource, $ADefault = null )
    {
        return $this -> SetParam( $AName, $ASource -> GetParam( $AName, $ADefault ));
    }



    /*
    */
    public function GetParamsAsURL()
    {
        $Result = [];
        foreach( $this -> Params as $Key => $Value )
        {
            switch( gettype( $Value ))
            {
                case 'boolean':         $Value = $Value ? 'true' : 'false'; break;
                case 'string':
                case 'integer':
                case 'double':          $Value = ( string ) $Value; break;
                case 'array':           $Value = json_encode( $Value ); break;
                case 'object':
                    switch( get_class( $Value ))
                    {
                        case 'TMoment': $Value = $Value -> ToString(); break;
                        default:        $Value = json_encode( $Value ); break;
                    }
                break;
                case 'NULL':            $Value = 'null';
                default:                $Value = null;
            }
            if( $Value != null ) array_push(  $Result, $Key . '=' . $Value );
        }
        return implode( '&', $Result );
    }




    /**************************************************************************
        Typed params
    */

    /*
        Setting the string value for parameter
    */
    public function SetString
    (
        string $AName,
        ?string $AValue = ''
    )
    {
        return $this -> SetParam( $AName, ( string ) $AValue );
    }



    /*
        Get value for parameter by name if it exists elseways default
    */
    public function GetString
    (
        string $AName,
        ?string $ADefault = ''
    )
    {
        return (string) $this -> GetParam( $AName, $ADefault );
    }



    /*
        Set value for parameter by name
    */
    public function SetInteger
    (
        string $AName,
        int $AValue = 0
    )
    {
        return $this -> SetParam( $AName, (int)$AValue );
    }



    /*
        Get value for parameter by name if it exists elseways default
    */
    public function GetInteger
    (
        string $AName,
        int $ADefault = 0
    )
    {
        return (int) $this -> GetParam( $AName, $ADefault );
    }



    /*
        Set value for parameter by name
    */
    public function SetBoolean
    (
        string $AName,
        bool $AValue = false
    )
    {
        return $this -> SetParam
        (
            $AName,
            $AValue == 'true' || $AValue === true || $AValue === 1 || $Bool === '1' || $Bool === 'on'
        );
    }



    /*
        Get value for parameter by name if it exists elseways default
    */
    public function GetBoolean
    (
        string $AName,
        bool $ADefault = false
    )
    {
        $Bool =  $this -> GetParam( $AName, $ADefault );
        return $Bool === 'true' || $Bool === true || $Bool === 1 || $Bool === '1' || $Bool === 'on';
    }



    /*
        Set value for parameter by name
    */
    public function SetFloat
    (
        string $AName,
        float $AValue = 0.0
    )
    {
        return $this -> SetParam
        (
            $AName,
            $AValue
        );
    }



    /*
        Get value for parameter by name if it exists elseways default
    */
    public function GetFloat
    (
        string $AName,
        bool $ADefault = false
    )
    {
        return ( float ) str_replace( ',', '.', $this -> GetParam( $AName, $ADefault ));
    }



    /*
        Get value for parameter by name if it exists elseways default
    */
    public function GetMoney( $AName, $ADefault = null )
    {
        return round(( float ) $this -> GetParam( $AName, $ADefault ), 4 );
    }



    /*
        Set value as money (round 4 signs) for parameter by name
    */
    public function SetMoney( $AName, $AValue = 0 )
    {
        return $this -> SetParam( $AName, round( $AValue, 4 ));
    }



    public function SetMoment
    (
        $AName, $AValue = null,
        $ATimezone = 0,
        $AUTC = false
    )
    {
        $this -> SetParam( $AName, TMoment :: Create( $AValue, $ATimezone, $AUTC ));
        return $this;
    }



    public function GetMoment
    (
        $AName,
        $AFormat    = null,     /* Use moment format */
        $ATimezone  = false,    /* Return time zone in format */
        $AUTC       = false,    /* Return UTC */
        $ADefault   = ''
    )
    {
        return $this
        -> GetParam( $AName, TMoment :: Create())
        -> ToString( $AFormat, $ATimezone, $AUTC, $ADefault );
    }



    public function GetMomentODBC
    (
        $AName,
        $AUTC = true,
        $ADefault = ''
    )
    {
        return $this
        -> GetParam( $AName, TMoment :: Create())
        -> ToStringODBC( $AUTC, $ADefault );
    }



    /*
        Converts all parameters to strings
        Objects exclude TMomoen are converted to json
        TMoment are converted to string
        Arrays are converted to json
        Booleans are converted to 'true' or 'false'
    */
    public function &GetParamsAsString()
    {
        $Result = [];
        foreach( $this as $Key => $Value )
        {
            switch( gettype( $Value ))
            {
                case 'boolean':         $Value = $Value ? 'true' : 'false'; break;
                case 'integer':
                case 'double':          $Value = ( string ) $Value; break;
                case 'array':           $Value = json_encode( $Value ); break;
                case 'object':
                    switch( get_class( $Value ))
                    {
                        case 'TMoment': $Value = $Value -> ToString(); break;
                        default:        $Value = json_encode( $Value ); break;
                    }
                break;
                case 'NULL':            $Value = 'null';
                default:                $Value = null;
            }
            if( $Value != null ) $Result[ $Key ] = $Value;
        }
        return $Result;
    }



    public function SetParamsFromJSON( $AString )
    {
        if( !empty( $AString ))
        {
            $Params = json_decode( $AString );
            if( empty( $Params ))
            {
                $this -> SetCode( 'JSONDecodeError' );
            }
            else
            {
                $this -> SetParams( $Params );
            }
        }
        return $this;
    }



    /*
        Copy all records from source to this
    */
    public function &CopyFrom
    (
        TParams $ASource,
        array $ANames = []
    )
    {
        $NamesLength = count( $ANames );
        foreach( $ASource -> Params as $Key => $Value )
        {
            if( in_array( $Key, $ANames ) || $NamesLength == 0 )
            {
                $this -> SetParam( $Key, $Value );
            }
        }
        return $this;
    }



    /*
        Copy all records from source to this
    */
    public function &AddFromArray( $ASource )
    {
        foreach( $ASource as $Key => $Value) $this -> SetParam( $Key, $Value );
        return $this;
    }



    /*
        Compare parameters by list of names
    */
    public function ParamsIsEqual
    (
        $ANames,            /* Array of names */
        $ADefault = null    /* Value for compare */
    )
    {
        $Result = true;
        if( count( $ANames ) > 0 )
        {
            $Value = empty( $ADefault ) ? $ANames[ 0 ] : $ADefault;
            foreach( $ANames as $Name )
            {
                $Result = $Result && $Value === $this -> GetParam( $Name );
            }
        }
        return $Result;
    }



    /*
        Return true for parameter is null
    */
    public function ParamIsNull( $AName )
    {
        return $this -> GetParam( $AName ) === null;
    }



    public function ParamIsEmpty( $AName )
    {
        $ParamValue = $this -> GetParam( $AName );
        if ( empty( $ParamValue )) $Result = true;
        else
        {
            $Value = gettype( $ParamValue );
            switch( $Value )
            {
                case 'object':
                    switch( get_class( $ParamValue ))
                    {
                        case 'TMoment': $Result = $ParamValue -> IsEmpty(); break;
                        default:        $Result = empty( $ParamValue ); break;
                    }
                break;
                default:
                    $Result = empty( $ParamValue );
                break;
            }
        }
        return $Result;
    }




    /*
        Rename keys
    */
    public function RenameKeys
    (
      array $AKeys = [] /* Array with key name translation: Old key name => New key name */
    )
    {
        $Result = [];
        foreach( $this -> Params as $Key => $Value )
        {
            if( array_key_exists( $Key, $AKeys ))
            {
                $Key = $AKeys[ $Key ];
            }
            $Result[ $Key ] = $Value;
        }
        $this -> Params = $Result;
        return $this;
    }


}
