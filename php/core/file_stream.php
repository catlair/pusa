<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

/*
    The file stream
    still@itserv.ru 16.10.2019
*/


namespace catlair;


include_once 'debug.php';
include_once 'stream.php';

class TFileStream extends TStream
{
    private $FHandle = null;


    function __construct
    (
        $AFileName,
        $AMode
    )
    {
        parent::__construct();
        $this->FHandle = fopen($AFileName, $AMode);
        if ($this->FHandle===false) $this->Code = 'FileOpenError';
    }



    public static function Create($AFileName, $AMode)
    {
        $Result = new TFileStream($AFileName, $AMode);
        return $Result;
    }



    public function GetPosition()
    {
        if ($this->IsOk()) $Result = ftell($this->FHandele);
        else $Result = false;
        return $Result;
    }



    public function SetPosition($APosition)
    {
        if ($this->IsOk()) fseek($FHandele, $APosition);
        return $this;
    }



    public function IsEnd()
    {
        if ($this->IsOk()) $Result = feof($this->FHandle);
        else $Result = true;
        return $Result;
    }



    public function GetSize()
    {
        if ($this->IsOk())
        {
            $Stat=fstat($this->FHandle);
            $Result = $Stat['size'];
        }
        else $Result = false;
        return $Result;
    }



    public function ReadBuffer($ASize)
    {
        if ($this->IsOk()) $Result=fread($this->FHandle, $ASize);
        else $Result='';
        return $Result;
    }



    /*
        Write buffer
    */
    public function &WriteBuffer($ABuffer)
    {
        if ($this->IsOk()) fwrite($this->FHandle, $ABuffer);
        return $this;
    }
}
