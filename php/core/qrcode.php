<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/


namespace catlair;

/*
    QRCode generator

    It request the cli tool qrencode.
    For install:
    sudo apt install qrencode
*/

include_once ('result.php');
include_once ('utils.php');
include_once ('shell.php');



class TQRCode extends TShell
{
    public $ResultPath = '/tmp/qrcode';


    /*

    */
    public function &EncodeToFile
    (
        $AValue,    /* Value for coding */
        $AFileName  /* Result file name */
    )
    {
        /* Get result path */
        $ResultPath = dirname( $AFileName );

        if ( CheckPath( $ResultPath ))
        {
            $this -> SetOk();
            $this
            -> CmdBegin()
            -> CmdAdd( 'qrencode' )
            -> CmdAdd( '-o', ' ' )
            -> CmdAdd( $AFileName, '"' )
            -> CmdAdd( ' ' )
            -> CmdAdd( $AValue, '"' )
            -> CmdEnd()
            -> ResultTo( $this );
        }
        else
        {
            $this -> SetResult( 'ErrorCreatePath', $Path );
        }

        /* Return result */
        return $this;
    }



    /*
        Return path for processed
    */
    private function GetNewFile()
    {
        return $this -> ResultPath . '/' . clUUID() . 'png';
    }



    /*
        Return QRCode as data in to variable as result
    */
    public function Encode
    (
        $AValue /* Value for coding */
    )
    {
        $FileName = GetNewFile();

        $this -> EncodeToFile( $AValue, $FileName );

        if ( file_exists( $FileName ))
        {
            /* Read file */
            $Result = file_get_contents( $FileName );
            /* Delete temporary file */
            unlink( $FileName );
        }
        else
        {
            $Result = null;
        }

        /* Return result */
        return $Result;
    }
}
