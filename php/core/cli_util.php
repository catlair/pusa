<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

/*
    18.09.2019 - still@itserv.ru
*/

namespace catlair;


include_once "console.php";


/*
    Return parameter for CLI mode by name $AKey:string from command line in full format [--name=vale]
    or STDIN over $ALog:TLog with caption $ACaption:string.
    If parameter not found, it will return $ADefault:string.
*/



global $IncomeParams;
$IncomeParams = BuildCLIParameters(); /* Array with default keys */



function SetIncomeCLI( $AKey, $AValue )
{
    global $IncomeParams;
    $IncomeParams[ $AKey ] = $AValue;
}



function GetCLIParameters()
{
    global $IncomeParams;
    return $IncomeParams;
}


function BuildCLIParameters()
{
    $Result = [];
    if( array_key_exists( 'argv', $_SERVER ))
    {
        $c = count( $_SERVER[ 'argv'] );
        for( $i = 1; $i < $c; $i ++ )
        {
            /* Read next index */
            $Param = $_SERVER[ 'argv'][ $i ];

            /* Split the param */
            $Couple = explode( '=', $Param, 2 );
            $Count = count( $Couple );

            /* Ket key and value */
            $Key    = $Count > 0 ? $Couple[ 0 ] : null;
            $Value  = $Count > 1 ? $Couple[ 1 ] : true;
            /* Remove -- and - before keys */
            $Key = preg_replace( '/(^--|^-)/', '', $Key);

            $Result[ $Key ] = $Value;
        }
    }
    return $Result;
}



function GetIncomeCLI
(
    $AKey,                  /* Key name */
    $ACaption   = '',       /* Caption for request in cli */
    $ADefault   = '',       /* Default value */
    $ARequest   = false     /* For true value will be request from readline */
)
{
    global $IncomeParams;
    $Result = $ADefault;

    /* Get value from params */
    if ( array_key_exists( $AKey, $IncomeParams ))
    {
        $Result = $IncomeParams[ $AKey ];
    }
    else
    {
        if( php_sapi_name() == 'cli' && $ARequest )
        {
            print
            (
                TConsole::ESC_INK_SILVER    . $ACaption .
                TConsole::ESC_INK_GREY      . ' [' .
                TConsole::ESC_INK_LIME      . $AKey  .
                TConsole::ESC_INK_SILVER    . ' = ' .
                TConsole::ESC_INK_YELLOW    . $ADefault .
                TConsole::ESC_INK_GREY      . ']:' .
                TConsole::ESC_INK_DEFAULT
            );
            $Result = readline();
            /* Store input */
            $IncomeParams[ $AKey ] = $Result;
        }
    }

    if( empty( $Result )) $Result = $ADefault;

    return $Result;
}
