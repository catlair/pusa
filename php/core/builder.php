<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

namespace catlair;

/*
    Class for compiling content with embeded XML constructions:
    <cl param="command" param="value" ... />
    or
    <cl param="command" param="value" ... >
    <command param="value"/>
    </cl>

    still@itserv.ru
*/

include_once "debug.php";           /* Include debug system */
include_once "utils.php";           /* Include base utils for project */
include_once "controller.php";      /* Include controller for API calls */
include_once "html_code.php";       /* convert code to html */



class TBuilder extends TLog
{
    /* Settings */
    private $FContent           = null;     /* Current content */
    private $FRecursDepth       = 100;      /* Maximum recursion depth */
    private $FRootPath          = '.';      /* Root path for content and libraries */

    /* Search and replace arrays */
    public $Income              = null;

    /* Internal objects */
    private $Optimize           = true;

    private $IDLangDefault      = 'language_default';

    private $Log                = null;     /* Log object */



    /*
        Catlairs Builder constructor
    */
    function __construct
    (
        TLog $ALog = null
    )
    {
        $this -> Log = empty( $ALog ) ? $this : $ALog;
        $this -> Income = new TParams();
        $this -> SetOk();
    }



    /*
        Remove all syntaxis from Content
        TODO it should be static
    */
    public function Optimize( $AContent )
    {
        return preg_replace
        (
            [
                '/(?:("(?:.|\n)*?")|(?:( |^|\n)\/\/.*))/',
                '/(?:\/\*(?:.|\n)*?\*\/)/',
                '/(?:("(?:.|\n)*?")|([\r\n ]+))/',
                '/(?:("(?:.|\n)*?")|(?: ?((?:>=)|(?:<=)|(?:===)|(?:==)|(?:!==)|(?:!=)|(?:[=\[\]{};:])) ?))/i',
                '/" /'
            ]
            ,
            [
                '$1$2',
                '',
                '$1 ',
                '$1$2',
                '"'
            ],
            $AContent
        );
    }



    /*
        Building content
    */
    public function &Build()
    {
        $this -> FContent = $this -> Parsing( $this -> FContent );
        $this -> FContent = $this -> Replace( $this -> FContent );
        if ( $this -> Optimize ) $this -> FContent = $this -> Optimize( $this -> FContent );
        return $this;
    }



    /*
        Building external content
        TODO it should be static
    */
    public function BuildContent
    (
        string  $AContent,
        bool    $AOptimize  = null,
        bool    $AReplace   = true
    )
    {
        $AContent = $this -> Parsing( $AContent );
        if( $AReplace)
        {
            $AContent = $this -> Replace( $AContent );
        }
        if( $AOptimize === null ? $this -> Optimize : $AOptimize )
        {
            $AContent = $this -> Optimize( $AContent );
        }
        return $AContent;
    }



    /*
        Send content to browser
    */
    public function &Send()
    {
        if( $this -> IsCLI() )
        {
            $this
            -> GetLog()
            -> Info( 'Content' )
            -> Print( $this -> FContent );
        }
        else
        {
            print( $this -> FContent );
        }
        return $this;
    }



    /*
        Parsing any content from $AContent:string and return result
    */
    public function Parsing( $AContent )
    {
        return $this -> Pars( $AContent, 0 );
    }



    /*
        Return Income params interface
    */
    public function &GetIncome()
    {
        return $this -> Income;
    }



    /*
        Rename keys in array
    */
    public function IncomeKeysConvert
    (
        array $AKeys = []
    )
    {
        $this -> Income -> RenameKeys( $AKeys );
        return $this;
    }



    /*
        `Replace all keys in the content $AContent:string and return it
    */
    private function Replace( $AContent )
    {
        $Names = [];
        $Values = [];
        foreach( $this -> Income -> GetParams() as $Name => $Value )
        {
            array_push( $Names, '%' . $Name . '%' );
            array_push( $Values, $Value );
        }
        return str_replace( $Names, $Values, $AContent );
    }



    /*
        Recurcive parsing for content from $ACountent:string with depth $ADepth:integer
        After parsing funciton will return new content
    */
    final private function Pars( $AContent, $ADepth )
    {
        $ADepth = $ADepth + 1;
        if( $ADepth < $this -> FRecursDepth )
        {
            do
            {
                    /* Getting list of tags over regvar with cl tag */
                    preg_match( '/\<cl(?:(\<)|".+?"|.|\n)*?(?(1)\/cl|\/)\>/', $AContent, $m, PREG_OFFSET_CAPTURE);
                    if( count( $m ) > 0 )
                    {
                        $b = $m[0][1];
                        $l = strlen ($m[0][0]);
                        $Source = $m[0][0];
                        if ( $l > 0 )
                        {
                            $Source = $this -> Replace( $Source );
                            $XMLSource = '<?xml version="1.0"?>' . $Source;
                            libxml_use_internal_errors(true);
                            $XML = simplexml_load_string( $XMLSource );
                            if( empty( $XML ))
                            {
                                $this -> SetResult
                                (
                                    'XMLError',
                                    'XML Error [%Source%]',
                                    [ 'Source' => $Source ]
                                );
                                $Result = '';
                            }
                            else
                            {
                                $Content = '';
                                $this -> BuildElement( $Content, $XML, $ADepth );
                                $Result = $Content;
                            }

                            /* Check recurstion error */
                            if ( $ADepth + 1 ==  $this -> FRecursDepth )
                            {
                                $this -> SetResult( 'Recurs', 'Recursion depth limit [' . $ADepth .'] [' . $XMLSource . ']');
                            }
                            $AContent = trim( substr_replace( $AContent, $Result, $b, $l ));
                        }
                    }
            } while ( count( $m ) > 0 && $this -> IsOk() );
        }
        else
        {
            $AContent='';
        }

        return $AContent;
    }



    /*
        Recursive processing of XML command in cl tag
    */
    final private function BuildElement
    (
        &$AContent,         /* contain text content */
        &$AElement,         /* simplexml it is text <cl param="value".../> or multiline XML <cl><command params="value"...></cl>. */
        $ARecursDepth       /* integer it is a current recursion depth. Zero by default. */
    )
    {
        if( $this -> IsOk() )
        {
            /* Processing of string as a key with parametes */
            $this -> Exec( $AContent, $AElement -> getName(), null, $AElement );

            /* Processing of directives in pair key=>value */
            if( $AElement -> getName() == 'cl' )
            {
                foreach( $AElement -> attributes() as $Key => $Value )
                {
                    $this -> Exec( $AContent, $Key, $Value, $AElement );
                }
            }
            /* Processing of internal cl tags */
            $AContent = $this -> Pars( $AContent, $ARecursDepth );
            /* Prpcessing of child stings with keys */
            foreach ($AElement -> children() as $Line => $Param)
            {
                $this -> BuildElement( $AContent, $Param, $ARecursDepth );
            }
        }
        return $this;
    }



    /*
        Main work with cl tag $AContent:string content
    */
    final private function Exec
    (
        &$AContent,
        $Command,
        $AValue,
        $Params
    )
    {
        switch (strtolower($Command))
        {

            default:
                if( $this -> IsOk())
                {
                    if ($AValue) $AContent = str_replace( '%'.$Command.'%', $AValue, $AContent );
                    else
                    {
                        /* Unknow key is found and it is not a macrochange string (%example%) in cl tag */
                        $this -> SetResult
                        (
                            'UnknownKey',
                            'Unknown key [' . $Command . '] (cl; set; add; file; replace; convert; exec; header; include ets...)'
                        );
                    }
                }
            break;


            /* Empty tag. Nothing to do */
            case 'cl':
            break;


            /* Involuntary parsing for last content */
            /* <cl .... pars="true"/> */
            case 'pars':
                if( $this -> IsOk())
                {
                    if( $AValue ) $Value = $AValue;
                    else $Value = (string) $Params['value'];
                    if( $Value == 'true' ) $AContent = $this -> Pars( $AContent, 0 );
                }
            break;


            /* Set new content from $AValue:string */
            /* <cl content="IDContent"/> */
            case 'set':
                if( $this -> IsOk())
                {
                    if ($AValue) $AContent = $AValue;
                    else
                    {
                        if( $Params['value']) $AContent = $Params['value'];
                        else
                        {
                            $this -> SetResult
                            (
                                'ParamNotFound',
                                'Parameter <b>value</b> not found'
                            );
                        }
                    }
                }
            break;


            /* Adding of content from $AValue:string */
            /* <cl ... add="IDContent"/> */
            case 'add':
                if( $this -> IsOk())
                {
                    if ($AValue) $Value = $AValue;
                    else $Value = (string) $Params[ 'value' ];
                    $AContent =  $AContent .= $Value;
                }
            break;

            /* This is an uncompleted URL builder. I must do it. */
            case 'url':
                if( $this -> IsOk())
                {
                    $Search = [];
                    $Replace = [];
                    /* параметры из */
                    foreach ($clURL as $Key => $Value)
                    {
                        array_push( $Search, '%' . $Key . '%' );
                        array_push( $Replace, $Value );
                    }
                    $AContent = str_replace( $Search, $Replace, $AContent );
                }
            break;



            /* Replace one parameter */
            case 'replace':
                if( $this -> IsOk())
                {
                    $AContent = str_replace( $Params[ 'from' ], $Params[ 'to' ], $AContent );
                }
            break;



            /* Mass replace parameters */
            case 'masreplace':
                if( $this -> IsOk())
                {
                    $Search = [];
                    $Replace = [];
                    foreach( $Params -> attributes() as $Key => $Value )
                    {
                        array_push( $Search, '%' . $Key . '%' );
                        array_push( $Replace, $Value );
                    }
                    $AContent = str_replace( $Search, $Replace, $AContent );
                }
            break;

            /*
                Replace named parameters from Template in the Content
            */
            case 'params':
                if( $this -> IsOk())
                {
                    $ID = $AValue ? $AValue : $Params[ 'id' ];
                    $Source = $this -> GetTemplate( (string) $ID );
                    $JSON = json_decode( $Source, true );
                    if( empty( $JSON ))
                    {
                        $this -> SetResult( 'JSONParsingParamsError', null, [ 'Source' => $Source ] );
                    }
                    else
                    {
                        $Search = [];
                        $Replace = [];
                        foreach( $JSON as $Key => $Value )
                        {
                            array_push( $Search, '%' . $Key . '%' );
                            array_push( $Replace, $Value );
                        }
                        $AContent = str_replace( $Search, $Replace, $AContent );
                    }
                }
            break;


            /* Collection */
            case 'keys':
                if( $this -> IsOk())
                {
                    $JSON = json_decode( $AContent, true );
                    if ( ! empty( $JSON ))
                    {
                        foreach( $JSON as $Key => $Value )
                        {
                            switch( $Key )
                            {
                                case 'IDUser':
                                case 'IDSite':
                                    /* This keys can not be changed with security rules. */
                                break;
                                default:
                                    $this -> Income -> SetParam( $Key, $Value );
                                break;
                            }
                        }
                        $AContent = '';
                    }
                    else
                    {
                        $this -> SetResult
                        (
                            'JSONParsingKeyError',
                            null,
                            [
                                'Content' => $AContent,
                                'Command' => $Command,
                                'Value' => $AValue,
                                'Params' => $Params
                            ]
                        );
                    }
                }
            break;



            /* Optimize content */
            case 'optimize':
            case 'pure':
                if( $this -> IsOk())
                {
                    $AContent = $this -> Optimize( $AContent );
                }
            break;

            /* Convert content to clear, html, pure, uri, md5, code */
            case 'convert':
                if( $this -> IsOk())
                {
                    $To = strtolower( $AValue ? $AValue : $Params[ 'to' ]);
                    $AContent = $this -> Replace( $AContent );
                    switch ($To)
                    {
                        case 'clear':   $AContent = ''; break;
                        case 'html':
                            $AContent = htmlspecialchars( $AContent, ENT_NOQUOTES );
                            $AContent = str_replace( PHP_EOL    , '<br>'    , $AContent );
                            $AContent = str_replace( '/*'       , '&#47;*'  , $AContent );
                            $AContent = str_replace( '*/'       , '*&#47;'  , $AContent );
                            $AContent = str_replace( ' '        , '&nbsp;'  , $AContent );
                        break;
                        case 'code':
                            $Type = $Params[ 'type' ];
                            $AContent = THTMLCode::Create() -> BuildCode( $AContent, $Type ) -> GetContent();
                        break;
                        case 'pure':
                            $AContent = preg_replace('/  +/','', preg_replace('/[\r\n]/',' ',$AContent));
                        break;
                        case 'uri':     $AContent = encodeURIComponent ($AContent); break;
                        case 'md5':     $AContent = md5( $AContent ); break;
                        case 'base64':  $AContent = base64_encode( $AContent ); break;
                        case 'default':; break;
                        default:
                            $this -> SetResult
                            (
                                'UnknownConvert',
                                'Unknown convert mode [' . $To . '] (clear; html; pure; uri; md5; base64; default)'
                            );
                        break;
                    }
                }
            break;

            case 'image':
                if( $this -> IsOk())
                {
                    if ($AValue) $ID = (string) $AValue;
                    else $ID = (string) $Params['id'];

                    if ($ID!='none')
                    {
                        $p = [];
                        foreach( $Params -> attributes() as $Key => $Value)
                        {
                            $p[ (string) $Key ] = (string) $Value;
                        }

                        $r = $this -> ExtData( $ID, $p );

                        if ($this -> IsOk())
                        {
                            if (empty($r['Error']))
                                $AContent = $r['Content'];
                            else
                                $AContent = empty($r['Message']) ? $r['Error'] : $r['Message'];
                        }
                        else
                        {
                            $AContent = $this -> GetMessage();
                        }
                    }
                }
            break;

            /* Read the file and return it as source */
            case 'source':
                if( $this -> IsOk())
                {
                    $ID = (string)( $AValue ? $AValue : $Params[ 'id' ]);
                    if ( $ID != 'none' )
                    {
                        $this -> GetSource( $ID, $Params[ 'branch' ]);
                        if( !$this -> IsOK()) $AContent = $this -> GetMessage();
                    }
                }
            break;

            case 'pusa':
                if( $this -> IsOk())
                {
                    require_once( 'pusa_core.php' );

                    $PusaClass = (string) ( $AValue ? $AValue : $Params[ 'class' ]);
                    $PusaMethod = (string) $Params[ 'method' ];

                    $AContent = TWebPusa::Create( $this, $PusaClass )
                    -> SetParams( $_POST )
                    -> Run( $PusaMethod )
                    -> ResultTo( $this )
                    -> GetContent();
                }
            break;

            /* Get content from file or descript */
            case 'content':
                if( $this -> IsOk())
                {
                    $ID = (string) ( $AValue ? $AValue : $Params[ 'id' ]);
                    if( $ID != 'none' )
                    {
                        $AContent = $this -> GetTemplate( $ID );
                    }
                }
            break;

            /* Include library */
            case 'library':
            case 'include':
                if( $this -> IsOk())
                {
                    $this->Begin( 'Include' );
                    $PrefixLib = empty( $AValue ) ? (string)$Params[ 'name' ] : (string)$AValue;

                    $this
                    -> GetLog()
                    -> Info()
                    -> Param('Library',$PrefixLib);

                    /* Call upper level */
                    $FileName = $this -> ExtIncludePath( $PrefixLib, $Command );
                    TLoader::Create() -> Load( $FileName ) -> ResultTo( $this );

                    $this -> End();
                }
            break;

            /* Execute PHP code over controller */
            /* It is recommended instead of exec */
            case 'call':
                if( $this -> IsOk())
                {
                    if ($AValue) $Call = (string)$AValue;
                    else $Call = (string)$Params['name'];

                    $Income = [];
                    foreach( $Params -> attributes() as $Key => $Value )
                    {
                        $Income[ (string)$Key ] = (string) $Value;
                    }

                    /* Run calls */
                    $Content = $this -> RunCalls( $Call, $AContent, $Income );

                    /* Return content */
                    $AContent = $this -> IsOK() ? $Content : $this -> GetMessage();
                }
            break;

            /* Write HTTP header */
            case 'header':
                if( $this -> IsOk())
                {
                    /* Collect params */
                    if( $AValue ) $St = ( string )$AValue;
                    else $St = ( string )$Params['value'];
                    /* Work */
                    if ( PHP_SAPI !== 'cli' )
                    {
                        header( $St );
                    }
                    else
                    {
                        $this -> GetLog()
                        -> Warning()
                        -> Param( 'Header is not applyed on cli mode', $St );
                    }
                }
            break;

            /* Set redirect for FPM page */
            case 'redirect':
                if( $this -> IsOk())
                {
                    /* Collect params */
                    if( $AValue ) $URL = $AValue;
                    else $URL = $Params[ 'url' ];
                    /* Work */
                    if( PHP_SAPI !== 'cli' )
                    {
                        if ( $URL ) $this -> Redirect( $URL );
                        else
                        {
                            $this -> SetResult
                            (
                                'ParamererNoFound',
                                'Parameter <b>url</b> not found'
                            );
                        }
                    }
                }
            break;

            /* Suppression of errors */
            case 'error':
                /* Collect params */
                if( $AValue ) $Value = $AValue;
                else $Value = $Params['value'];
                /* Work */
                if( strtolower( $Value ) == 'false')
                {
                    $this -> SetOk();
                }
            break;
        }

        return $this;
    }



    /*
        Return path and file from root
        TODO Remove it after 2022-05-30
    */
    /*
    private function GetFileNameFromRoot( $AFileName )
    {
        return clPathControl( $this -> FRootPath . '/' . $AFileName );
    }
    */


    /*
        Выполняет серию вызовов в формате Class.Method;....
    */
    public function &RunCalls
    (
        string $ACalls,                                /* Class.Method; Class.Method ...*/
        string $AContent    = null,                    /* Start content */
        $AIncome            = [],
        $AType              = TController :: CONTROLLER
    )
    {
        $Result = $AContent;
        $Calls = explode( ';', $ACalls );
        foreach( $Calls as $CallName )
        {
            $Call = $this -> LoadController( $CallName, $AType );
            if( $this -> IsOk() )
            {
                /* Start controller execute */

                $Class = 'catlair\\'.$Call[ 'Class' ];
                $Controller = new $Class( $this );
                /*
                $Controller = new $Call[ 'Class' ]( $this  );
                */
                $Controller
                -> SetContent( $Result )
                -> AddParams( empty($AIncome) ? $this -> GetIncome() -> GetParams() : $AIncome )
                /*-> AddParams( $this -> GetIncome() -> GetParams() )*/
                -> SetTypeContentIncome();
                $this -> Begin( $CallName );
                call_user_func( array( $Controller, $Call[ 'Method' ]));
                $this -> End();
                /* Return Controller content */
                $Result = $Controller -> End();
            }
        }
        return $Result;
    }



    /*
        Load controller from $ACall:string
        in format "ControllerName.MethodName"
        Return array
        [
            Class:ControllerName
            Method:MethodName
        ]
        or null
    */
    public function &LoadController
    (
        string $ACall,                             /* Call in format Class.Method */
        string $AType = TController :: CONTROLLER
    )
    {
        $Result = null;

        /*Get class name and method name*/
        $Split = explode( '.', $ACall );
        if( count( $Split ) !=2 )
        {
            $this -> SetResult
            (
                'CallFormatError',
                'Call can consist "class.method" but have ['. $ACall .']'
            );
        }
        else
        {
            $Class = $Split[ 0 ];
            $Method = $Split[ 1 ];

            $Result = [ 'Class' => $Class, 'Method' => $Method ];

            /* Call upper level */
            $FileName = $this -> ExtIncludePath( $Class, $AType );
            TLoader::Create() -> Load( $FileName ) -> ResultTo( $this );

            /* Controll class */
            $Class = 'catlair\\'.$Class;
            if( $this -> IsOK() && ! class_exists( $Class ))
            {
                $this -> SetResult
                (
                    'UnknownClass',
                    'Unknown class [' . $Class . '] for library [' . $FileName . ']'
                );
            }
            /* Controll class */
            if( $this -> IsOK() && class_exists( $Class ) && ! method_exists( $Class, $Method ))
            {
                $this -> SetResult
                (
                    'UnknownMethod',
                    'Unknown method [' . $Method . '] for class [' . $Class . ']'
                );
            }
        }
        return $Result;
    }



    public function Redirect( $URL )
    {
        header( 'Location: '.$URL );
    }



    /*
    */
    public function GetSource( $AID, $ABranch )
    {
        return '';
    }



    /*
        Read end return file content by $AID:string
        Warning! This function is not safe, because it returns any file from RootPath without controling path.
        This function must be overridden on upper level.
        Use this function as a template only.
    */
    public function GetTemplateContent
    (
        $AID,
        $ADefault   = null,
        $AIDSite    = SITE_DEFAULT,     /* for overriding */
        $AIDLang    = null,             /* for */
        $AStrict    = false
    )
    {
        /* Set IDLang as default if empty */
        if( empty( $AIDLang )) $AIDLang = $this -> GetIDLangDefault();
        $FileName = $AIDSite . '/' . $AIDLang . '/' . $AID;

        if( file_exists( $FileName ))
        {
            $Result = @file_get_contents( $FileName );
        }

        return $Result;
    }



    /*
        Return template
    */
    public function GetTemplate
    (
        $AID,
        $ADefault   = '',
        $AIDSite    = null, /* for overriding */
        $AIDLang    = null, /* for */
        $AStrict    = false
    )
    {
        $Result = $this -> GetTemplateContent( $AID, $ADefault, $AIDSite, $AIDLang, $AStrict );
        if( $Result === null && $this -> IsOk())
        {
            $this -> SetResult
            (
                'TemplateNotFound',
                null,
                [
                    'ID'        => $AID,
                    'Default'   => $ADefault,
                    'IDSite'    => $AIDSite,
                    'IDLang'    => $AIDLang,
                    'Strict'    => $AStrict
                ]
            );
        }
        return $Result;
    }



    /*
        Return message by code
        Сообщения хранятся по умолчанию в папке error.
        В случае отсутствия сообщения вернется сам код ошибки.
        Функция может быть переопределена.
    */
    public function GetErrorByCode( $ACode )
    {
        return $this -> GetTemplateContent( 'errors/' . $ACode, $ACode );
    }



    /*
    */
    public function ExtData( $AID, &$AParams )
    {
        $this -> SetResult
        (
            'EmptyFunction',
            'Ext data is empty function'
        );
        return null;
    }



    /*
        Extended function for including libraries
    */
    public function ExtIncludePath( $ALibrary, $ACommand )
    {
        return $ALibrary;
    }



    /*
        Extended function for extended call when need function name for exec
    */
    public function ExtFunctionName($AFunction)
    {
        return $AFunction;
    }



    /**************************************************************************
        Setters and getters
    */



    /*
        Set optimizer
    */
    public function &SetOptimize( $AValue )
    {
        $this -> Optimize = $AValue;
        return $this;
    }



    /*
        Set content
    */
    public function &SetContent( $AContent )
    {
        $this -> FContent = $AContent;
        return $this;
    }



    /*
        Get content
    */
    public function GetContent()
    {
        return $this -> FContent;
    }



    /*
        Set depth of recursion from $ARecursDepth:integer
    */
    public function &SetRecursDepth($ARecursDepth)
    {
        $this->FRecursDepth = $ARecursDepth;
        return $this;
    }



    /*
        Get depth of recursion
    */
    public function GetRecursDepth()
    {
        return $this->FRecursDepth;
    }



    /*
        Set Root path from $APath:string for file operations.
    */
    public function &SetRootPath($APath)
    {
        $this -> FRootPath = $APath;
        return $this;
    }



    /*
        Return root path for all file operations.
    */
    public function &GetRootPath()
    {
        return $this->FRootPath;
    }



    /*
        Set default language for builder
    */
    public function &SetIDLangDefault( $AValue )
    {
        $this -> IDLangDefault = $AValue;
        return $this;
    }



    /*
        Get default language for builder
    */
    public function &GetIDLangDefault()
    {
        return $this -> IDLangDefault;
    }



    /*
        Return Log object
    */
    public function GetLog()
    {
        return $this -> Log;
    }
}

