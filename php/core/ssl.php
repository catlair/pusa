<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

/*
    Web TLS SSL certificates request
    Based on dehydrate:
        https://github.com/dehydrated-io/dehydrated

    Preparing:
        install
            apt install dehydrate

    Example:
        TSSL::Create( TLog::Create() )
        -> Configure( 'example@email.com', ['domain1','domain2'] )
        -> Registrate()
        -> Request();
*/

namespace catlair;

require_once 'result.php';
require_once 'shell.php';
require_once 'debug.php';



class TSSL extends TResult
{
    private $Log            = null;
    private $WebRootPath    = null;
    private $Test           = false;
    private $WebserverUser  = null;



    /*
        Constructor
    */
    function __construct
    (
        $ALog   /* TLog object */
    )
    {
        $this -> Log = $ALog;
    }



    /*
        Create the new TSSL object and return it
    */
    static public function Create
    (
        $ALog   /* TLog object */
    )
    {
        $Result = new TSSL( $ALog );
        return $Result;
    }



    /*
        Configure the request
    */
    public function Configure
    (
        $AEmail,                                        /* Requester email for notifications */
        $ADomains       = [],                           /* arrays string with domain names */
        $AWebserverUser = 'www-data',                   /* Webserver user and group */
        $AWebRootPath   = '/var/www/html/dehydrated'    /* Путь до папки обмена. Nginx должен быть настроен */
    )
    {
        $this -> WebRootPath = $AWebRootPath;
        $this -> WebserverUser = $AWebserverUser;

        $this -> Log
        -> Begin( 'SSL configure' )
        -> Trace() -> Param( 'Dehydrate config file',       $this -> GetDehydratedConfigPath())
        -> Trace() -> Param( 'Dehydrate domains file',      $this -> GetDehydratedDomainsPath())
        -> Trace() -> Param( 'Nginx config for public',     $this -> GetNginxDehydrateConfig())
        -> Trace() -> Param( 'Dehydrate web share path',    $this -> GetWebSharePath());

        /* Create path for receive dehydrated fils */
        if( $this -> IsOk())
        {
            if( CheckPath( $this -> GetWebSharePath() ))
            {
                TShell::Create( $this -> Log )
                -> Cmd
                (
                    'chown -R ' .
                    $this -> WebserverUser . ':' .
                    $this -> WebserverUser . ' ' .
                    $this -> WebRootPath
                );
            }
            else
            {
                $this -> SetResult( 'ErrorCreateWebSharePath', $this -> GetWebSharePath() );
            }
        }

        /* Create dehydrete config path */
        if( $this -> IsOk() && !CheckPath( dirname( $this -> GetDehydratedPath() )))
        {
            $this -> SetResult( 'ErrorCreateDehydratePath', $this -> GetDehydratedPath() );
        }

        /* Dehydrated config */
        if
        (
            $this -> IsOk() &&
            !file_put_contents
            (
                $this -> GetDehydratedConfigPath(),
                implode
                (
                    PHP_EOL,
                    [
                        'CA="https://acme-v02.api.letsencrypt.org/directory"',
                        'CHALLENGETYPE="http-01"',
                        'WELLKNOWN="' . $this -> GetWebSharePath(). '"',
                        'CONTACT_EMAIL="'.$AEmail.'"'
                    ]
                )
            )
        )
        {
            $this -> SetResult( 'ErrorCreateDehydratedConfigFile', $this -> GetDehydratedConfigPath() );
        }

        /* Write dehydrated domains file */
        if
        (
            $this -> IsOk() &&
            !file_put_contents
            (
                $this -> GetDehydratedDomainsPath(),
                implode( PHP_EOL, $ADomains )
            )
        )
        {
            $this -> SetResult( 'ErrorCreateDehydratedDomainsFile', $this -> GetDehydratedDomainsPath() );
        }

        $this -> Log -> End();

        return $this;
    }



    /*
        Generate config for nginx
    */
    public function FilesByTemplate
    (
        $ADomains       = [],                           /* Array of domains [ IDTask1, Task2, Task3 ] */
        $ATemplate      = '',                           /* Nginx template config file */
        $ADestination   = '/etc/nginx/sites-enabled'    /* Path for the site configs */
    )
    {
        if( $this -> IsOk() )
        {
            if( CheckPath( dirname( $ADestination )))
            {
                foreach( $ADomains as $Domain )
                {
                    if
                    (
                        file_put_contents
                        (
                            $ADestination . '/' . $Domain,
                            str_replace( '%Domain%', $Domain, $ATemplate )
                        )
                    )
                    {
                        $this -> SetResult( 'ErrorCreateGinxConfig', $Domain );
                    }
                }
            }
            else
            {
                $this -> SetResult( 'ErrorCreateGinxPath', $ADestination );
            }
        }
        return $this;
    }



    public function NginxConfig()
    {
        /* Write nginx site */
        if( $this -> IsOk() )
        {
            if( !CheckPath( dirname( $this -> GetNginxDehydrateConfig() )))
            {
                $this -> SetResult( 'ErrorCreatePath', dirname( $this -> GetNginxDehydrateConfig()) );
            }
            else
            {
                if
                (
                    !file_put_contents
                    (
                        $this -> GetNginxDehydrateConfig(),
                        implode
                        (
                            PHP_EOL,
                            [
                                'server {',
                                '    listen 80;',
                                '    server_name _;',
                                '    location /.well-known/acme-challenge/ {',
                                '        root ' . $this -> GetWebSharePath() . ';',
                                '        return 301 https://$host$request_uri;',
                                '   }',
                                '}'
                            ]
                        )
                    )
                )
                {
                    $this -> SetResult( 'ErrorWriteNginxDehydrateConfig', $this -> GetNginxDehydrateConfig() );
                }
            }
        }
        return $this;
    }



    /*
        Registrate new email
    */
    public function Registrate()
    {
        if( $this -> IsOk())
        {
            $this
            -> ResultFrom
            (
                TShell::Create( $this -> Log )
                -> Cmd( '/usr/bin/dehydrated --register --accept-terms', $this -> Test )
            );
        }
        return $this;
    }



    /*
        Final request sertificates
    */
    public function Request()
    {
        if( $this -> IsOk() )
        {
            $this
            -> ResultFrom
            (
                TShell::Create( $this -> Log )
                -> Cmd( '/usr/bin/dehydrated -c', $this -> Test )
            );
        }

        return $this;
    }



    /**************************************************************************
        Getters
    */


    /*
        Return path for ssh
    */
    public function GetWebSharePath()
    {
        return $this -> WebRootPath . '/.well-known/acme-challenge';
    }



    /*
        Return pure configuration path
    */
    private function GetDehydratedPath()
    {
        return '/etc/dehydrated';
    }



    /*
    */
    private function GetDehydratedConfigPath()
    {
        return $this -> GetDehydratedPath() . '/config';
    }



    /*
    */
    private function GetDehydratedDomainsPath()
    {
        return $this -> GetDehydratedPath() . '/domains.txt';
    }



    /*
        Return path for NGIX config with Dehydrate public link
    */
    private function GetNginxDehydrateConfig()
    {
        return '/etc/nginx/sites-enabled/dehydrate';
    }
}
