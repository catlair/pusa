<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/


namespace catlair;

/*
    RGBA module for Catlair
*/


include_once "math.php";

class TRGBA
{
    public $R = 0;
    public $G = 0;
    public $B = 0;
    public $A = 0;



    function __construct ( $R = 0, $G = 0 , $B = 0, $A = 1 )
    {
        $this -> Set( $R, $G, $B, $A );
    }



    public function Set( $R = 0, $G = 0 , $B = 0, $A = 1 )
    {
        $this -> R = (float) $R;
        $this -> G = (float) $G;
        $this -> B = (float) $B;
        $this -> A = (float) $A;
        return $this;
    }



    /*
        Trim component between 0..1
    */
    public function Trim($AMin = 0, $AMax = 1)
    {
        if ($this -> R < $AMin ) $this -> R = $AMin;
        if ($this -> G < $AMin ) $this -> G = $AMin;
        if ($this -> B < $AMin ) $this -> B = $AMin;
        if ($this -> A < $AMin ) $this -> A = $AMin;

        if ($this -> R > $AMax ) $this -> R = $AMax;
        if ($this -> G > $AMax ) $this -> G = $AMax;
        if ($this -> B > $AMax ) $this -> B = $AMax;
        if ($this -> A > $AMax ) $this -> A = $AMax;

        return $this;
    }



    /*
        Normalize component between 0..1
    */
    public function Norm()
    {
        /* Trim under zero */
        if ($this -> R < 0 ) $this -> R = 0;
        if ($this -> G < 0 ) $this -> G = 0;
        if ($this -> B < 0 ) $this -> B = 0;
        if ($this -> A < 0 ) $this -> A = 0;

        /* Normalize */
        $Max = max([$this->R, $this->G, $this->B]);
        if ( $Max > EPSILON )
        {
            $this -> R /= $Max;
            $this -> G /= $Max;
            $this -> B /= $Max;
        }

        /* Trim alpha chanel */
        if ($this -> A > 1) $this -> A = 1;
        return $this;
    }



    public function Add( &$ARGBA )
    {
        $this -> R += $ARGBA -> R;
        $this -> G += $ARGBA -> G;
        $this -> B += $ARGBA -> B;
        $this -> A += $ARGBA -> A;
        return $this;
    }



    public function AddComponent( $R = 0, $G = 0 , $B = 0, $A = 1 )
    {
        $this -> R += $R;
        $this -> G += $G;
        $this -> B += $B;
        $this -> A += $A;
        return $this;
    }



    public function Sub( &$ARGBA )
    {
        $this -> R -= $ARGBA -> R;
        $this -> G -= $ARGBA -> G;
        $this -> B -= $ARGBA -> B;
        $this -> A -= $ARGBA -> A;
        return $this;
    }



    public function SubComponent( $R = 0, $G = 0 , $B = 0, $A = 1 )
    {
        $this -> R -= $R;
        $this -> G -= $G;
        $this -> B -= $B;
        $this -> A -= $A;
        return $this;
    }



    public function Mul( &$ARGBA )
    {
        $this -> R *= $ARGBA -> R;
        $this -> G *= $ARGBA -> G;
        $this -> B *= $ARGBA -> B;
        $this -> A *= $ARGBA -> A;
        return $this;
    }



    public function MulComponent( $R = 0, $G = 0 , $B = 0, $A = 1 )
    {
        $this -> R *= $R;
        $this -> G *= $G;
        $this -> B *= $B;
        $this -> A *= $A;
        return $this;
    }



    public function ItpLin( &$ARGBA, $ATime )
    {
        $this -> R = ItpLin( $this -> R, $ARGBA -> R, $ATime );
        $this -> G = ItpLin( $this -> G, $ARGBA -> G, $ATime );
        $this -> B = ItpLin( $this -> B, $ARGBA -> B, $ATime );
        $this -> A = ItpLin( $this -> A, $ARGBA -> A, $ATime );
        return $this;
    }



    /*
        Return Hex format 00000000 .. FFFFFFFF
    */
    public function ToHex()
    {
        return sprintf
        (
            '%02X%02X%02X%02X',
            $this -> R * 255,
            $this -> G * 255,
            $this -> B * 255,
            $this -> A * 255
        );
    }



    /*
        Return CSS format rgba(0..255, 0..255,0..255, 0..1)
    */
    public function ToCSSRGBA()
    {
/*        return 'rgba(' . $this->R*255 . ',' . $this->G*255 . ',' . $this->B*255 . ',' . $this->A . ')';*/
    }
}
