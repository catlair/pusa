<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

namespace catlair;

/*
    Catlair PHP
    CLI интерфейс

    The Deploy.
    Build the default php code and specific php code.
    Made obfircate.

    still@itserv.ru

    RFS - Relatitions File System
    AFS - Absolute File System
*/

require_once( 'result.php' );
require_once( 'moment.php' );
require_once( 'ssl.php' );
require_once( 'file_utils.php' );


class TDeploy extends TResult
{
    const MODE_FULL             = 'full';
    const MODE_BUILD            = 'build';
    const MODE_TEST             = 'test';

    const TARGET_LOCAL          = 'local';          /* local path from file system root */
    const TARGET_WORKPLACE      = 'workplace';      /* local path to deployer */
    const TARGET_TEMPLATE       = 'template';       /* template in this task */
    const TARGET_DEFAULT        = 'default';        /* default templates */
    const TARGET_DEPLOY         = 'deploy';         /* deploy folder on local host */
    const TARGET_IMAGE          = 'image';          /* docker images */
    const TARGET_LOCAL_PROJECT  = 'local_project';  /* deploy project path $LocalProjectPath */
    const TARGET_DEPLOY_PROJECT = 'deploy_project'; /* deploy project path [deploy].$DeployProjectPath */
    const TARGET_REMOTE         = 'remote';         /* remote target */

    private $Log                = null;
    private $ProdPath           = '';
    private $Path               = '';
    private $DefaultTemplate    = '';
    private $DeployBegin        = null;
    private $DeployPath         = null; /* local path for deploy. If is null, using $Pass/deploy */
    private $LocalProjectPath   = null;
    private $DeployProjectPath  = null;

    private $Mode               = self::MODE_FULL;
    private $DeploySSHKey       = null;
    private $DeployUser         = 'root';
    private $DeployHost         = 'unknown';

    /* Docker */
    private $ImageID            = null;
    private $ImageName          = null;
    private $PathMacros         = [];

    private $AddCapabilities    = [];
    private $ImagePorts         = [];


    function __construct( $ALog, $APath )
    {
        $this -> Log            = $ALog;
        $this -> Path           = $APath;
        $this -> DeployBegin    = TMoment::Create() -> Now();
        $this -> DeployMoment   = TMoment::Create() -> Now() -> ToStringODBC();
        $this
        -> AddPathMacro( 'DEPLOYER', $this -> GetPath() )
        -> AddPathMacro( 'BUILD',  $this -> GetDeployPath() );
    }



    static public function Create( $ALog, $APath )
    {
        $Result = null;
        $File = $APath . '/' . 'index.php';

        if ( file_exists( $File ))
        {
            $ALog -> Info( 'Deploy source' ) -> Param( 'File', $File );

            require_once( $File );

            if ( ! class_exists ( 'catlair\TDeployScript' )) $ALog -> Error( 'Class DeployScript not found' ) -> Param( 'File', $File );
            else
            {
                $Result = new TDeployScript( $ALog, $APath );
                $Result -> SetPath( $APath );
            }
        }
       return $Result;
    }



    public function Run()
    {
        return $this;
    }



    public function ExecuteOnLocal
    (
        $ALines,
        $AComment = ''
    )
    {
        if( $this -> IsOk() )
        {
            /* Execute after install */
            $Shell = TShell::Create( $this -> Log )
            -> CmdBegin()
            -> SetComment( $AComment );

            $Lines = [];
            foreach( $ALines as $Line) array_push( $Lines, $Line );

            $Shell
            -> CmdAdd( implode( ' && ', $Lines))
            -> CmdEnd( ' ', $this -> IsTest(self::TARGET_LOCAL))
            -> ResultTo( $this );
        }

        return $this;
    }



    /*
        Плстроитель исходного кода
        По списку правил с вызовом для каждого правила процедуры обнатного вызова
    */
    public function BuildRules
    (
        $ARules,        /* List pathes for building */
        $ASource,       /* RFS Source path for scan begining */
        $ADestination,  /* RFS Each file from the source must be moved to the destination according to the rules */
        $AOnProcessing, /* Transit callback */
        $AExclude       /* Exclude list */
    )
    {
        if( $this -> IsOk() && $this -> Mode != self::MODE_TEST )
        {
            foreach( $ARules as $Rule )
            {
                if( $this -> IsOk() )
                {
                    $this -> Convert
                    (
                        $Rule,
                        $ASource,
                        $ADestination,
                        $AOnProcessing,
                        $AExclude
                    );
                }
            }
        }
        return $this;
    }



    /*
        Конвертация файлов одного правила для кода
        и перекладывание из источника в направление
        с вызовом функции обратного вызова для обработки
        контента
    */
    public function Convert
    (
        $ARule,         /* Rule */
        $ASource,       /* AFS Source path */
        $ADestination,  /* RFS Destination for files */
        $AOnProcessing, /* Callback for source processing */
        $AExclude
    )
    {
        $CountProcessed = 0;

        $RuleFolders    = explode( '/', $ARule );

        $this -> Log
        -> Begin( 'Converting rule' )
        -> Trace() -> Key( 'Rule', $ARule )
        -> Trace() -> Key( 'Source', $ASource )
        -> Trace() -> Key( 'Destination', $ADestination );

        clFileScan
        (
            $ASource,

            /* OnFolder */
            function( $AFolder, $AIndex) use ( $RuleFolders )
            {
                $Rule = $RuleFolders[ $AIndex < count( $RuleFolders ) ? $AIndex : count( $RuleFolders ) -1];
                $Current = basename( $AFolder );
                return fnmatch( $Rule, $Current );
            },

            /* OnFile */
            function( $AFile, $AIndex )
            use
            (
                $ADestination,
                $ASource,
                $RuleFolders,
                $AOnProcessing,
                &$CountProcessed,
                $AExclude
            )
            {
                $ExcludeMatch = false;
                foreach( $AExclude as $Exclude )
                {
                    $ExcludeMatch = $ExcludeMatch || fnmatch( $ASource . '/' . $Exclude, $AFile );
                }

                if( $ExcludeMatch )
                {
                    $this -> GetLog() -> Trace() -> Key( 'Exclude', $AFile );
                }
                else
                {
                    $Rule = $RuleFolders[ $AIndex < count( $RuleFolders ) ? $AIndex : count( $RuleFolders ) -1];
                    $Current = basename( $AFile );
                    if( fnmatch( $Rule, $Current ))
                    {
                        $CountProcessed++;

                        $this -> Log -> Begin( 'Converting file' );
                        /* Content read */
                        $Content = file_get_contents( $AFile );

                        /* Obfuscation */
                        if( ! empty( $AOnProcessing ))
                        {
                            $PreparedContent = call_user_func( $AOnProcessing, $AFile, $Content );
                        }
                        else
                        {
                            $PreparedContent = $Content;
                        }

                        /* Сontent sizechecking. It should not be empty. */
                        if( empty( $PreparedContent ))
                        {
                             $this -> SetCode( 'ResultFileIsEmpty' );
                             $this -> Log -> Error( 'Result file is empty' );
                        }

                        /* Write result to local file */
                        $Dest = str_replace( $ASource, $ADestination, $AFile );
                        $this -> WriteToLocal( $Dest, $PreparedContent );

                        $this -> Log
                        -> Trace() -> Key( 'Source', $AFile )
                        -> Trace() -> Key( 'Dest', $Dest )
                        -> Trace() -> Key( 'Size before ', strlen( $Content ))
                        -> Trace() -> Key( 'Size after', strlen( $PreparedContent ))
                        -> End();
                    }
                }
            }
        );

        if( $CountProcessed == 0 )
        {
            $this -> SetResult
            ( 'NoJobForRule',
                null,
                [
                    'Rule' => $ARule,
                    'Source' => $ASource
                ]
            );
        }

        $this -> Log -> End();
        return $this;
    }



    /*
        Обфускатор
    */
    public function PHPObfuscate( $ASource, $AVac )
    {
        $ASource = preg_replace
        (
            [
                '/(\'(?:.|\n)*?\')|(?:\/\/.*?(\n|$))/', /* выкусывание однострочных коментариев */
                /*
                Попытка игоря убрать однострочные с учетом двойных кавычек
                '/(\'(?:.|\n)*?\')|("(?:.|\n)*?")|(?:\/\/.*?(\n|$))/',
                */
                '/(\'.*?\')|(?:\/\*(?:.|\n)*?\*\/)/',   /* выкусывание многострочных коментариев */
                '/\s+?(\n)/',                           /* все пробелы до конца строки */
                '/(\'(?:.|\n)*?\')|(?: ?(=) ?)/',       /* убрать все пробелы вокруг = */
                '/(\'(?:.|\n)*?\')|(?: ?(>) ?)/',       /* убрать все пробелы вокруг > */
                '/(\'(?:.|\n)*?\')|(?: ?(<) ?)/',       /* убрать все пробелы вокруг < */
                '/(\'(?:.|\n)*?\')|(?: ?(\.) ?)/',      /* убрать все пробелы вокруг . */
                '/(\'(?:.|\n)*?\')|(?: ?(,) ?)/',       /* убрать все пробелы вокруг . */
                '/(\'(?:.|\n)*?\')|(?: ?(\() ?)/',      /* убрать все пробелы вокруг ( */
                '/(\'(?:.|\n)*?\')|(?: ?(\)) ?)/',      /* убрать все пробелы вокруг ) */
                '/(\'(?:.|\n)*?\')|(?: ?(\-) ?)/',      /* убрать все пробелы вокруг - */
                '/(\'(?:.|\n)*?\')|(?: ?(\+) ?)/',      /* убрать все пробелы вокруг + */
                '/\n/',                                 /* Косим все энтеры */
                '/(\'(?:.|\n)*?\')|(?:( ) +)/',         /* Косим двойные пробелы */
                '/(\'(?:.|\n)*?\')|(?: ?(\{) ?)/',      /* убрать все пробелы вокруг { */
                '/(\'(?:.|\n)*?\')|(?: ?(\}) ?)/',      /* убрать все пробелы вокруг } */
                '/(\'(?:.|\n)*?\')|(?: ?(\[) ?)/',      /* убрать все пробелы вокруг [ */
                '/(\'(?:.|\n)*?\')|(?: ?(\]) ?)/',      /* убрать все пробелы вокруг ] */
                '/(\'(?:.|\n)*?\')|(?: ?(\;) ?)/',      /* убрать все пробелы вокруг ; */
            ],
            [
                '$1',
                '$1',
                '$1',
                '$1$2',
                '$1$2',
                '$1$2',
                '$1$2',
                '$1$2',
                '$1$2',
                '$1$2',
                '$1$2',
                '$1$2',
                ' ',
                '$1$2',
                '$1$2',
                '$1$2',
                '$1$2',
                '$1$2',
                '$1$2',
            ],
            $ASource
        );

//        $ASource = preg_replace_callback ('/\'(?:.|\n)*?\'/', 'ConvertString', $ASource); /* конвертим строки в кекс */
//        $ASource = preg_replace_callback ('/\$\w+/', 'ConvertVar', $ASource); /* превращаем переменнные в мусор */

        /* Подмены словаря */
        if ($AVac!=null)
        {
            foreach ( $AVac as $Key => $Value )
            {
                $ASource = str_replace( $Key, $Value, $ASource );
            }
        }
        return $ASource;
    }



    /* Обфурскация имен переменных */
    private function ConvertVar( $AValue )
    {
        if
        (
            $AValue[0] == '$this' ||
            $AValue[0] == '$GLOBALS' ||
            $AValue[0] == '$_SERVER' ||
            $AValue[0] == '$_GET' ||
            $AValue[0] == '$_POST' ||
            $AValue[0] == '$_FILES' ||
            $AValue[0] == '$_COOKIE' ||
            $AValue[0] == '$_SESSION' ||
            $AValue[0] == '$_REQUEST' ||
            $AValue[0] == '$_ENV'
        )
        {
            $Result=$AValue[0];
        }
        else
        {
            $Result='$f'.MD5((string)$AValue[0]);
        }

        return $Result;
    }



    /*
        Обфурскация имен переменных
    */
    private function ConvertString($AValue)
    {
        if ($AValue!='\'\'')
        {
            $s = StringToHex( substr($AValue[0], 1, -1));
            $Result = 'HexToString(\'' . $s . '\')';
        }
        else
        {
            $Result = $AValue[0];
        }
        return $Result;
    }



    public function MoveToProduction( $ASource, $ADestination )
    {
        if ( $this -> IsOk() )
        {
            $Destination = $this -> GetFullPath( $ADestination );
            if( $this -> Mode != self::MODE_TEST )
            {
                if ( ! CheckPath( dirname( $Destination ))) $this -> SetCode( 'ErrorCreateFolder' );
                elseif ( ! rename( $ASource, $Destination) )  $this -> SetCode( 'ErrorMoveFile' );
            }
            else
            {
                $this -> Log
                -> Info( 'Move folder in test mode' )
                -> Param( 'Destination',  $ADestination );
            }
        }
        return $this;
    }



    public function MoveInProduction( $ASource, $ADestination )
    {
        if ( $this -> IsOk() )
        {
            /* Prepare the real sourse and destination pathes */
            $Source = $this -> GetFullPath( $ASource );
            $Destination = $this -> GetFullPath( $ADestination );
            if( $this -> Mode != self::MODE_TEST )
            {
                /* Check destination path */
                if( ! CheckPath( dirname( $Destination ))) $this -> SetResult( 'ErrorCreateFolder', $Destination );
                /* Move the file from source to destination */
                elseif( rename( $Source, $Destination )) $this -> Log -> Info( 'Move' ) -> Param( 'From', $Source ) -> Param( 'To', $Destination );
                /* elseways set error */
                else $this -> SetResult( 'ErrorMoveFile', $Source );
            }
            else
            {
                $this -> Log
                -> Info( 'Move folder in test mode' )
                -> Param( 'Source',  $ASource )
                -> Param( 'Destination',  $ADestination );
            }
        }
        return $this;
    }



    /*
        Write content in to production
    */
    public function WriteToLocal( $AFilePath, $AContent )
    {
        if( $this -> IsOk() )
        {
            if( CheckPath( dirname( $AFilePath )))
            {
                if( !file_put_contents( $AFilePath, $AContent ))
                {
                    $this -> SetResult( 'ErrorWriteFile', $AFilePath );
                }
            }
            else
            {
                $this -> SetResult( 'ErrorCreateFolder', $AFilePath );
            }
        }
        return $this;
    }



    /*
        Write content in to production
    */
    public function WriteToProduction( $AFilePath, $AContent )
    {
        if( $this -> IsOk() )
        {
            $FilePath = $this -> GetFullPath( $AFilePath );
            if( CheckPath( dirname( $FilePath )))
            {
                file_put_contents( $FilePath, $AContent );
            }
            else
            {
                $this -> SetResult( 'ErrorCreateFolder', $FilePath );
            }
        }
        return $this;
    }



    /*
        Recursive replaces for values in files
    */
    public function &Replace
    (
        $ATasks      = []
    )
    {
        if( $this -> IsOk())
        {
            foreach( $ATasks as $Task)
            {
                $Path = $this -> PathPrep( clValueFromObject( $Task, 'Path', 'NOPATH' ));
                $Values = clValueFromObject( $Task, 'Values', [] );

                $this
                -> Log
                -> Begin( 'Replace' )
                -> Info( 'Target path ' )
                -> Value( $Path );

                $Search     = [ '%' . 'DeployMoment' . '%' ];
                $Replace    = [ $this -> DeployMoment ];

                /* Build list of repleces */
                foreach( $Values as $Record )
                {
                    $String = clValueFromObject( $Record, 'String' );
                    $Pattern = clValueFromObject( $Record, 'Pattern' );
                    if( !empty( $Pattern )) $String = '%' . $Pattern . '%';

                    $Value = clValueFromObject( $Record, 'Value', '' );
                    $File = clValueFromObject( $Record, 'File', '' );
                    if( !empty( $File ))
                    {
                        $File = $this -> PathPrep( $File );
                        if( file_exists( $File )) $Value = file_get_contents( $File );
                    }

                    array_push( $Search, $String );
                    array_push( $Replace, $Value );
                }

                clFilesReplace( $this -> Log, $Path, $Search, $Replace );

                $this -> Log -> End();
            }
        }
        return $this;
    }




    public function &ReplaceFile
    (
        $ATarget,
        $APath = '',
        $AVocab = []
    )
    {
        if ( $this -> IsOk() )
        {

            $AVocab = array_merge
            (
                [
                    '%' . 'DeployMoment' . '%' => $this -> DeployBegin -> ToString()
                ],
                $AVocab
            );

            if ( $ATarget == self::TARGET_REMOTE ) $this -> SetCode( 'ReplaceCanNotWorkWithRemoteTarget');
            else
            {
                $Path = $this -> GetFullPath( $APath, $ATarget );

                $Search = [];
                $Replace =[];

                foreach( $AVocab as $Key => $Value )
                {
                    array_push( $Search, $Key );
                    array_push( $Replace, $Value);
                }

                clFileReplace( $Path, $Search, $Replace );
            }
        }
        return $this;
    }



    /**************************************************************************
        Universal FS operation
    */


    /*
    */
    public function ChangeOwnerPath
    (
        $AOwner     = 'root:root',
        $APath      = '',
        $ARecursion = true
    )
    {
        if ( $this -> IsOk() )
        {
            $Folder = $this -> PathPrep( $APath );
            if( !CheckPath( $Folder ))
            {
                $this -> SetCode( 'ErrorCreateFolderForChangeOwner' );
            }
            else
            {
                TShell::Create( $this -> Log )
                -> SetComment( 'Change owner for path' )
                -> CmdBegin ()
                -> CmdAdd   ( 'chown' )
                -> CmdAdd   ( $AOwner )
                -> CmdAdd   ( $ARecursion ? '-R' : '' )
                -> CmdAdd   ( $Folder )
                -> CmdEnd   ( ' ', $this -> IsTest( $ATarget ))
                -> ResultTo ( $this );
            }
        }
        return $this;
    }



    /*
        Synchronize local folder with production
    */
    public function ChangeOwner
    (
        $AOwner     = 'root:root',
        $AFolder    = '',
        $ATarget    = self::TARGET_DEPLOY,
        $ARecursion = true
    )
    {
        if ( $this -> IsOk() )
        {
            $Folder = $this -> GetFullPath( $AFolder, $ATarget );
            if ( !CheckPath( $Folder ))
            {
                $this -> SetCode( 'ErrorCreateFolderForChangeOwner' );
            }
            else
            {
                TShell::Create( $this -> Log )
                -> SetComment( 'Change owner for path' )
                -> CmdBegin ()
                -> CmdAdd   ( 'chown' )
                -> CmdAdd   ( $AOwner )
                -> CmdAdd   ( $ARecursion ? '-R' : '' )
                -> CmdAdd   ( $Folder )
                -> CmdEnd   ( ' ', $this -> IsTest( $ATarget ))
                -> ResultTo ( $this );
            }
        }
        return $this;
    }




    /*
        Change rights pathes
    */
    public function ChangeRightsPathes
    (
        $APathes    = []
    )
    {
        foreach( $APathes as $Path )
        {
            $this -> ChangeRightsPath
            (
                clValueFromObject( $Path, 'Path',       'NOPATH' ),
                clValueFromObject( $Path, 'Rights',     '' ),
                clValueFromObject( $Path, 'Recursion',  true )
            );
        }
        return $this;
    }



    /*
        Create pathes
    */
    public function CreatePathes
    (
        $APathes    = []
    )
    {
        foreach( $APathes as $Path )
        {
            CheckPath( $this -> PathPrep( $Path ));
        }
        return $this;
    }



    /*
    */
    public function ChangeRightsPath
    (
        $AFolder    = '',
        $ARights    = '',
        $ARecursion = true
    )
    {
        if( $this -> IsOk() )
        {
            $Folder = $this -> PathPrep( $AFolder );
            if( !file_exists( $Folder ))
            {
                $this -> SetResult( 'ErrorPathForChangeRights', $Folder );
            }
            else
            {
                TShell::Create( $this -> Log )
                -> SetComment( 'Change rights for path' )
                -> CmdBegin ()
                -> CmdAdd   ( 'chmod' )
                -> CmdAdd   ( $ARights )
                -> CmdAdd   ( $ARecursion ? '-R' : '' )
                -> CmdAdd   ( $Folder )
                -> CmdEnd   ( ' ', false )
                -> ResultTo ( $this );
            }
        }
        return $this;
    }



    /*
        Synchronize local folder with production
    */
    public function ChangeRights
    (
        $ARights,
        $AFolder    = '',
        $ATarget    = self::TARGET_DEPLOY,
        $ARecursion = true
    )
    {
        if ( $this -> IsOk() )
        {
            $Folder = $this -> GetFullPath( $AFolder, $ATarget );
            if ( !CheckPath( $Folder ))
            {
                $this -> SetCode( 'ErrorCreateFolderForChangeRights' );
            }
            else
            {
                TShell::Create( $this -> Log )
                -> CmdBegin ()
                -> CmdAdd   ( 'chmod' )
                -> CmdAdd   ( $ARights )
                -> CmdAdd   ( $ARecursion ? '-R' : '' )
                -> CmdAdd   ( $Folder )
                -> CmdEnd   ( ' ', $this -> IsTest( $ATarget ))
                -> ResultTo ( $this );
            }
        }
        return $this;
    }



    /*
    */
    public function ChangeRightsEach
    (
        $ARights,
        $AFolder    = '',
        $ATarget    = self::TARGET_DEPLOY,
        $ARecursion = true    )
    {
        if ( $this -> IsOk() )
        {
            $Folders = explode( '/', $AFolder );
            $Path = '/';
            foreach( $Folders as $Item )
            {
                if( !empty( $Item ))
                {
                    $Path .= $Item;

                    TShell::Create( $this -> Log )
                    -> CmdBegin ()
                    -> CmdAdd   ( 'chmod' )
                    -> CmdAdd   ( $ARights )
                    -> CmdAdd   ( $this -> GetFullPath( $Path, $ATarget ))
                    -> CmdEnd   ( ' ', $this -> IsTest( $ATarget ))
                    -> ResultTo ( $this );

                    $Path .= '/';
                }
            }
        }
        return $this;
    }



    public function ChangeFolder
    (
        $AFolder = '',                      /* RFS */
        $ATarget = self::TARGET_DEPLOY
    )
    {
        if ( $this -> IsOk() )
        {
            $Folder = $this -> GetFullPath( $AFolder, $ATarget );
            if( file_exists( $Folder ) && is_dir( $Folder ))
            {
                chdir( $Folder );
            }
            else
            {
                $this -> SetResult( 'FolderNotExists', $Folder );
            }
        }
        return $this;
    }



    public function CreateFolder
    (
        $AFolder = '',                      /* RFS */
        $ATarget = self::TARGET_DEPLOY
    )
    {
        if ( $this -> IsOk() )
        {
            $Folder = $this -> GetFullPath( $AFolder, $ATarget );
            if( !$this ->  IsTest( $ATarget ))
            {
                if ( CheckPath( $Folder ))
                {
                    $this -> Log -> Info() -> Param( 'Create folder', $Folder );
                }
                else
                {
                    $this -> SetResult( 'ErrorCreateFolder', $Folder );
                }
            }
            else
            {
                $this -> Log -> Info( 'Create folder in test mode' ) -> Param( 'Path',  $Folder );
            }
        }
        return $this;
    }



    public function DeleteFolder
    (
        $AFolder    = '',                   /* RFS */
        $ATarget    = self::TARGET_DEPLOY
    )
    {
        if ( $this -> IsOk() && $ATarget != self::TARGET_REMOTE )
        {
            $Folder = $this -> GetFullPath( $AFolder, $ATarget );
            TShell::Create( $this -> Log )
            -> CmdBegin()
            -> SetComment( 'Delete folder' )
            -> CmdAdd( 'rm' )
            -> CmdAdd( '-rf' )
            -> CmdAdd( $Folder, '"' )
            -> CmdEnd( ' ', $this -> IsTest( $ATarget ))
            -> ResultTo( $this );
        }
        return $this;
    }



    /*
        TODO remove it
    */
    public function DeleteBuild()
    {
        if ( $this -> IsOk())
        {
            TShell::Create( $this -> Log )
            -> CmdBegin()
            -> CmdAdd( 'rm' )
            -> CmdAdd( '-rf' )
            -> FileAdd( $this -> PathPrep( '[BUILD]' ) )
            -> CmdEnd( ' ', $this -> Mode == self::MODE_TEST  )
            -> ResultTo( $this );
        }
        return $this;
    }



    /*
        Delete files by task
    */
    public function DeleteTasks
    (
        $ATasks /* Tasks array with list of pathes for delete */
    )
    {
        if( $this -> IsOk() )
        {
            foreach( $ATasks as $Task )
            {
                clDeleteFolder( $this -> PathPrep( $Task ));
            }
        }
        return $this;
    }



    /*
        Synchronize by task array
    */
    public function SyncTask
    (
        $ATasks                 /* Reading sync section */
    )
    {
        foreach( $ATasks as $Task )
        {
            $this -> SyncPathes
            (
                $this -> PathPrep( clValueFromObject( $Task, 'From',    'NOPATH' )),
                $this -> PathPrep( clValueFromObject( $Task, 'To',      'NOPATH' )),
                $this -> PathPrep( clValueFromObject( $Task, 'Delete',  false )),
                $this -> PathPrep( clValueFromObject( $Task, 'Rename',  false ))
            );
        }
        return $this;
    }



    public function SyncPathes
    (
        $ASourcePath            = '', /* RFS */
        $ADestinationPath       = '', /* RFS */
        $ADelete                = false,
        $ARename                = false,
        $AExcludes              = []
    )
    {
        if( $this -> IsOk() )
        {
            if( !file_exists( $ASourcePath ))
            {
                $this -> SetResult( 'NoSourcePathForSync', $ASourcePath );
            }
            else
            {
                if( $ARename )
                {
                    CheckPath( dirname( $ADestinationPath ));
                }
                else
                {
                    CheckPath( $ADestinationPath );
                }

                $Shell = TShell::Create( $this -> Log )
                -> SetComment( 'Synchronize path' )
                -> CmdBegin ()
                -> CmdAdd   ( 'rsync' );

                if( $ARename )
                {
                    $Shell
                    -> LongAdd( "no-R" )
                    -> LongAdd( "no-implied-dirs" );
                }

                $Shell -> CmdAdd   ( '-rzaog' );

                if( !empty( $ADelete ))
                {
                    $Shell -> CmdAdd( '--delete' );
                    if ( gettype( $ADelete ) == 'array' )
                    {
                        foreach( $ADelete as $Value) $Shell -> CmdAdd( '--filter "protect ' . $Value . '"' );
                    }
                }

                foreach( $AExcludes as $Exclude )
                {
                    $Shell -> CmdAdd( '--exclude "' . $Exclude . '" ' );
                }

                $Shell
                -> FileAdd   ( $ASourcePath )
                -> FileAdd   ( $ADestinationPath )
                -> CmdEnd   ( ' ', $this -> Mode == self::MODE_TEST )
                -> ResultTo ( $this );
            }
        }
        return $this;
    }



    public function Sync
    (
        $ASourceTarget,
        $ADestinationTarget,
        $ASourcePath            = '', /* RFS */
        $ADestinationPath       = '', /* RFS */
        $ADelete                = false,
        $AExcludes              = []
    )
    {
        if( $this -> IsOk() )
        {
            $SourcePath         = $this -> GetFullPath( $ASourcePath, $ASourceTarget );
            $DestinationPath    = $this -> GetFullPath( $ADestinationPath, $ADestinationTarget );

            if( $ASourceTarget != self::TARGET_REMOTE && is_dir( $SourcePath )) $SourcePath .= '/';
            if( $ADestinationTarget != self::TARGET_REMOTE ) CheckPath( $DestinationPath );

            $Shell = TShell::Create( $this -> Log );

            $Shell
            -> CmdBegin ()
            -> CmdAdd   ( 'rsync' )
            -> CmdAdd   ( '-rzaog' );

            if( !empty( $ADelete ))
            {
                $Shell -> CmdAdd( '--delete' );
                if ( gettype( $ADelete ) == 'array' )
                {
                    foreach( $ADelete as $Value) $Shell -> CmdAdd( '--filter "protect ' . $Value . '"' );
                }
            }

            foreach( $AExcludes as $Exclude )
            {
                $Shell -> CmdAdd( '--exclude "' . $Exclude . '" ' );
            }

            $Shell
            -> CmdAdd   ( $SourcePath, '"' )
            -> CmdAdd   ( $DestinationPath, '"' )
            -> CmdEnd   ( ' ', $this -> IsTest( $ADestinationTarget ))
            -> ResultTo ( $this );
        }
        return $this;
    }



    public function Copy
    (
        $ASourceTarget          = TARGET_DEPLOY,    /* Source target */
        $ADestinationTarget     = TARGET_DEPLOY,    /* Destination target */
        $ASourcePath            = '',               /* RFS */
        $ADestinationPath       = '',               /* RFS */
        $AReplace               = []
    )
    {
        if ( $this -> IsOk() )
        {
            $SourcePath         = $this -> GetFullPath( $ASourcePath, $ASourceTarget );
            $DestinationPath    = $this -> GetFullPath( $ADestinationPath, $ADestinationTarget );

            if( $ADestinationTarget != self::TARGET_REMOTE ) CheckPath( dirname( $DestinationPath ));

            TShell::Create( $this -> Log )
            -> CmdBegin ()
            -> CmdAdd   ( 'scp' )
            -> CmdAdd   ( '-r' )
            -> CmdAdd   ( $SourcePath, '"' )
            -> CmdAdd   ( $DestinationPath, '"' )
            -> CmdEnd   ( ' ', $this -> IsTest( $ADestinationTarget ))
            -> ResultTo ( $this );
        }
        return $this;
    }



    /**************************************************************************
        Remote operations
    */
    public function ExecuteOnRemote
    (
        $ALines,
        $AComment    = null,
        $AResultCode = null
    )
    {
        if( $this -> IsOk() )
        {
            /* Execute after install */
            $Shell = TShell::Create( $this -> Log );

            $Shell
            -> CmdBegin()
            -> SetComment( $AComment )
            -> CmdAdd( 'ssh' )
            -> CmdAdd( '-tt' );
            if( ! empty( $this -> DeploySSHKey ))
            {
                $Shell
                -> CmdAdd( '-i' )
                -> CmdAdd( $this -> DeploySSHKey );
            }

            $Shell -> CmdAdd( $this -> GetConnection() );

            $Lines = [];
            foreach( $ALines as $Line) array_push( $Lines, $this -> PathPrep( $Line ));
            $Shell -> CmdAdd(  implode( ' && ', $Lines), '"' );

            $Shell
            -> CmdEnd( ' ', $this -> IsTest( self::TARGET_REMOTE ));

            if( !empty( $AResultCode ))
            {
                $this -> SetCode( $AResultCode );
            }
            else
            {
                $Shell -> ResultTo( $this );
            }
        }
        return $this;
    }



    /*
        Reboot remote host
    */
    public function &RebootRemote()
    {
        $this -> ExecuteOnRemote([ 'shutdown -r +1' ], 'Reboot remote system' );
        return $this;
    }



    /*************************************************************************
        Getters and setters
    */



    /*
        Return full path for local path
    */
    public function GetFullPath( $ALocalPath, $ATarget = self::TARGET_DEPLOY )
    {
        $Local= empty( $ALocalPath ) ? '' : '/' . $ALocalPath;
        switch( $ATarget)
        {
            case self::TARGET_LOCAL             : $Result = $ALocalPath; break;
            case self::TARGET_DEPLOY            : $Result = $this -> GetDeployPath() . $ALocalPath; break;
            case self::TARGET_TEMPLATE          : $Result = $this -> GetTemplatePath( $ALocalPath ); break;
            case self::TARGET_DEFAULT           : $Result = $this -> GetDefaultPath( $ALocalPath ); break;
            case self::TARGET_REMOTE            : $Result = $this -> GetConnection() . ':' . ( empty( $ALocalPath ) ? '/' : $ALocalPath ); break;
            case self::TARGET_IMAGE             : $Result = $this -> GetPath() . '/images' . $Local; break;
            case self::TARGET_WORKPLACE         : $Result = $this -> GetPath() . $Local; break;
            case self::TARGET_LOCAL_PROJECT     : $Result = $this -> LocalProjectPath . $Local; break;
            case self::TARGET_DEPLOY_PROJECT    : $Result = $this -> GetDeployPath()
                                                            . ( empty( $this -> DeployProjectPath ) ? '' : $this -> DeployProjectPath )
                                                            . $Local; break;
        }
        return $Result;
    }




    public function SetDefaultTemplate( $AValue )
    {
        $this -> DefaultTemplate = $AValue;
        return $this;
    }



    public function SetDeployPath( $AValue )
    {
        $this -> DeployPath = $AValue;
        return $this;
    }



    public function GetPath()
    {
        return $this -> Path;
    }



    public function SetPath( $AValue )
    {
        $this -> Path = $AValue;
        return $this;
    }



    /*
        Return deploy AFS path
    */
    public function GetDeployPath( $AAddPath = null )
    {
        return
        (
            empty( $this -> DeployPath )
            /* from devault place */
            ?( $this -> GetPath() . '/' . 'build' )
            /* from specific place */
            : $this -> DeployPath
        )
        .
        (
            empty( $AAddPath )
            ? ''
            : '/' . $AAddPath
        );
    }



    /*
        return the template path
    */
    public function GetTemplatePath( $ALocalPath = '' )
    {
        return $this -> GetPath() . '/' . 'template' . $ALocalPath;
    }



    /*
        return the template path
    */
    public function GetDefaultPath( $ALocalPath = '' )
    {
        return realpath( $this -> GetPath() . '/../' . '_templates' . '/' . $this -> DefaultTemplate . $ALocalPath );
    }



    public function GetConnection()
    {
        return $this -> DeployUser . '@' . $this -> DeployHost;
    }



    public function IsTest( $ADestinationTarget )
    {
        return
        $ADestinationTarget == self::TARGET_REMOTE
        && $this -> Mode != self::MODE_FULL
        || $this -> Mode == self::MODE_TEST;
    }




    /**************************************************************************
        Docker
    */

    /*
        Warning! Change directory
    */
    public function ImageBuild()
    {
        if( $this -> IsOk() )
        {
            $this -> ChangeFolder();

            /* Building container */
            $Shell = TShell::Create( $this -> Log )
            -> SetComment( 'Build the docker image' )
            -> CmdBegin()
            -> CmdAdd( 'docker build -t ' . $this -> GetImageBuild( +1 ) . ' -t ' . $this -> GetImageLatest() . ' .' )
            -> CmdEnd( ' ', $this -> IsTest( self::TARGET_DEPLOY ))
            -> ResultTo( $this );

            /* Set container id if building success */
            if( $this -> IsOk())
            {
                $this -> ImageID = $Shell -> GetResultByKey( 'Successfully built' );
                if( empty( $this -> ImageID ))
                {
                    $this -> SetResult( 'IDImageNotFound', $this -> GetImageBuild( +1 ));
                }
                else
                {
                   $this -> VersionInc();
                }
            }
        }

        return $this;
    }



    public function ImageExport
    (
        $AFolder    = '',                   /* RFS */
        $ATarget    = self::TARGET_IMAGE
    )
    {
        if( $this -> IsOk() )
        {
            $AFolder = $this -> GetFullPath( $AFolder, $ATarget );
            $AFile = $AFolder . '/' . $this -> GetImageFile();
            if( CheckPath( dirname( $AFile )))
            {
                /* Export */
                $Shell = TShell::Create( $this -> Log )
                -> SetComment( 'Export docker image' )
                -> CmdBegin()
                -> CmdAdd( 'docker save -o ' . $AFile . ' ' . $this -> GetImageBuild() )
                -> CmdEnd( ' ', $this -> IsTest( self::TARGET_DEPLOY ))
                -> ResultTo( $this );
            }
        }
        return $this;
    }



    public function ImagePublic( $ALogin, $APasswordFile )
    {
        if( $this -> IsOk() )
        {
            $PasswordFile = $this -> GetPath() . '/' . $APasswordFile;

            /* Export */
            $Shell = TShell::Create( $this -> Log )
            -> SetComment( 'Publicate image' )
            -> Cmd( 'docker login --username ' . $ALogin . ' --password-stdin < ' . $PasswordFile, $this -> IsTest( self::TARGET_DEPLOY ))
            -> Cmd( 'docker push ' . $this -> GetImageBuild(), $this -> IsTest( self::TARGET_DEPLOY ))
            -> Cmd( 'docker push ' . $this -> GetImageLatest(), $this -> IsTest( self::TARGET_DEPLOY ))
            -> ResultTo( $this );
        }
        return $this;
    }



    public function ImageExportDelete
    (
        $AFolder    = '',                   /* RFS */
        $ATarget    = self::TARGET_IMAGE
    )
    {
        if( $this -> IsOk() )
        {
            $AFolder = $this -> GetFullPath( $AFolder, $ATarget );
            $AFile = $AFolder . '/' . $this -> GetImageFile();
            if( file_exists( $AFile ))
            {
                if( !unlink( $AFile ))
                {
                    $this -> SetResult( 'ErrorImageExportDelete', $AFile );
                }
            }
        }
        return $this;
    }



    public function ImageDeploy
    (
        $AImageID   = null,
        $APorts     = []    /* array of ports */
    )
    {
        if( $this -> IsOk() )
        {
            if( empty( $AIDImage ))
            {
                $AImageID = $this -> ImageID;
            }

            $this
            -> Sync( self::TARGET_IMAGE, TDeploy::TARGET_REMOTE, $this -> GetImageFile() , '/tmp/image/' )

            -> ExecuteOnRemote
            (
                [ 'docker load --input "' . '/tmp/image/' . $this -> GetImageFile() . '"' ],
                'Upload container on remote system'
            )

            -> ExecuteOnRemote
            (
                [ 'rm /tmp/image/' . $this -> GetImageFile() ],
                'Remove docker file with image'
            )

            -> ExecuteOnRemote
            (
                [ 'docker stop \$(docker ps | grep ' . $this -> GetImageName() . ' | awk \'{print \$1}\')' ],
                'Stop docker container on remote',
                rcOk
            )

            -> ExecuteOnRemote
            (
                [
                    'docker run -itd --rm' .
                    ( empty( $APorts ) ? '' : ' -p ' ) . implode( ' -p ', $APorts ) . ' ' .
                    ( empty( $this -> ImagePorts ) ? '' : ' -p ' ) . implode( ' -p ', $this -> ImagePorts ) . ' ' .
                    ( empty( $this -> AddCapabilities ) ? '' : '--cap-add=' ) . implode(  ' --cap-add=', $this -> AddCapabilities ) . ' ' .
                    $this -> GetImageBuild()
                ],
                'Run new container on remote'
            )
            ;
        }
        return $this;
    }



    public function ImageDelete
    (
        $AImageID = null
    )
    {
        if( $this -> IsOk() )
        {
            if( empty( $AIDImage ))
            {
                $AImageID = $this -> ImageID;
            }
            $Shell = TShell::Create( $this -> Log )
            -> SetComment( 'Delete docker image' )
            -> CmdBegin()
            -> CmdAdd( 'docker rmi -f ' . $AImageID )
            -> CmdEnd( ' ', $this -> IsTest( self::TARGET_DEPLOY ))
            -> ResultTo( $this );
        }
        return $this;
    }



    /*
        Return current image ID
    */
    public function GetImageID()
    {
        return $this -> ImageID;
    }



    public function GetImageFile( $AShift = 0 )
    {
        return str_replace( '/', '_', $this -> GetImageBuild( $AShift )) . '.tar';
    }



    /*
    */
    public function SetImageName( $AValue )
    {
        $this -> ImageName = $AValue;
        return $this;
    }



    /*
        Return image name owner/name
    */
    public function GetImageName()
    {
        return $this -> ImageName;
    }


    /**************************************************************************
        Version
    */


    /*
        Return full image name with version
        owner/name:version
    */
    public function GetImageBuild
    (
        $AShift = 0
    )
    {
        return $this -> ImageName . ':' . $this -> VersionAsString( $AShift );
    }



    /*
        Return full image name with version
        owner/name:version
    */
    public function GetImageLatest()
    {
        return $this -> ImageName . ':latest';
    }



    public function GetVersionFile()
    {
        return $this -> GetPath() . '/' . 'version.json';
    }



    public function VersionRead()
    {
        $File = $this -> GetVersionFile();
        $Result = null;

        if( file_exists( $File ))
        {
            $Result = json_decode( @file_get_contents( $this -> GetVersionFile() ), true );
        }

        if( empty( $Result ))
        {
            $Result =
            [
                'Version'   => "alpha",
                'Build'     => 0
            ];
        }
        return $Result;
    }



    public function VersionWrite( $AVersion )
    {
        $Result = file_put_contents( $this -> GetVersionFile(), json_encode( $AVersion ));
        return $this;
    }



    public function VersionAsString
    (
        $AShift = 0 /* Version shift -1 previous; 0 current; +1 next */
    )
    {
        $Version = $this -> VersionRead();
        return $Version[ 'Version' ] . '.' . (string)( $Version[ 'Build' ] + $AShift );
    }



    /*
        Reading the version
        Increase by 1
        Writeing version
    */
    public function VersionInc()
    {
        $Version = $this -> VersionRead();
        $Version[ 'Build' ] = $Version[ 'Build' ] + 1;
        $this -> VersionWrite( $Version );
        return $this;
    }



    public function RequestCertificates( $AConfig )
    {
        TSSL::Create( $this -> Log )
        -> Configure
        (
            clValueFromObject( $AConfig, 'RequesterEmail', '' ),
            clValueFromObject( $AConfig, 'Domains', [] )
        )
        -> Registrate()
        -> Request()
        -> ResultTo( $this );

        return $this;
    }


    /**************************************************************************
        Getters and setters
    */


    public function GetLog()
    {
        return $this -> Log;
    }



    public function SetLocalProjectPath( $AValue )
    {
        $this -> LocalProjectPath = $AValue;
        return $this;
    }



    public function SetDeployProjectPath( $AValue )
    {
        $this -> DeployProjectPath = $AValue;
        return $this;
    }



    public function PathPrep( $AValue )
    {
        $Result = $AValue;
        foreach( $this -> PathMacro as $From => $To )
        {
            $Result = str_replace( '[' . $From . ']', $To, $Result );
        }
        return $Result;
    }



    public function AddPathMacro( $AKey, $AValue )
    {
        $this -> PathMacro[ $AKey ] = $AValue;
        return $this;
    }



    public function SetMode( $AValue )
    {
        $this -> Mode = $AValue;
        return $this;
    }



    public function SetDeployUser( $AValue )
    {
        $this -> DeployUser = $AValue;
        return $this;
    }



    public function SetDeployHost( $AValue )
    {
        $this -> DeployHost = $AValue;
        return $this;
    }



    public function SetDeploySSHKey( $AValue )
    {
        $this -> DeploySSHKey = $AValue;
        return $this;
    }



    public function SetImageAddCapabilities( $AValue )
    {
        $this -> AddCapabilities = $AValue;
        return $this;
    }



    public function SetImagePorts( $AValue )
    {
        $this -> ImagePorts = $AValue;
        return $this;
    }
}
