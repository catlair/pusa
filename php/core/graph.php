<?php

/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.


    18.09.2019 - still@itserv.ru
*/

namespace catlair;


class TGraph
{
    const   SCROLL_LEFT         = 1;
    const   LOOP_LEFT_TO_RIGHT  = 2;

    private $Values             = [];

    private $MinY               = 0.0;
    private $MaxY               = 1.0;

    private $Width              = 30;           /* Weight of graph in characters */
    private $Height             = 10;           /* Height of graph in characters */

    private $LineCache          = null;         /* Line cache */
    private $Type               = self :: LOOP_LEFT_TO_RIGHT;
    private $Position           = 0;



    static public function Create()
    {
        $Result = new TGraph();
        $Result -> SetLength( $Result -> Width );
        return $Result;
    }



    /*
        Add new value
    */
    public function Add( $AValue )
    {
        switch( $this -> Type )
        {
            case self :: SCROLL_LEFT:
                /* Appent element and shift array */
                array_shift( $this -> Values );
                array_push( $this -> Values, $AValue );
            break;
            case self :: LOOP_LEFT_TO_RIGHT:
                $this -> Values[ $this -> Position ] = $AValue;
                $this -> Position ++;
                if( $this -> Position >= count( $this -> Values )) $this -> Position = 0;
            break;
        }

        /* Recalculate min and max */
        if( $AValue < $this -> MinY ) $this -> MinY = $AValue;
        if( $AValue > $this -> MaxY ) $this -> MaxY = $AValue;

        /* Reset line cache */
        $this -> LineCache = null;

        return $this;
    }



    /*
        Return the index of values array by character position
    */
    public function GetIndexByPos( $APosX )
    {
        $Scale = $APosX / $this -> Width;
        $Result = intval( count( $this -> Values ) * $Scale );
        return $Result;
    }



    /*
        Return Max value by position
    */
    public function GetMaxValueByPos( $APosX )
    {
        $Count = count( $this -> Values );
        $Scale = $APosX / $this -> Width;
        $Begin = intval( $Count * $Scale );
        $Len = ceil( $Count / $this -> Width );

        $Result = - INF;
        for( $i = 0; $i < $Len; $i++ )
        {
            $Value = $this -> Values[ $Begin + $i ];
            if( $Result < $Value ) $Result =  $Value;
        }

        return $Result;
    }



    /*
        NormPos            0                  1
                    0======*=========O--------*------> Y
                          min      Value     max
    */
    public function TestByPos( $APosX, $APosY )
    {
        /* Get value by position */
        $Value = $this -> Values[ $this -> GetIndexByPos( $APosX ) ];
        /* Normalize position */
        $NormPos = $APosY / $this -> Height;
        /* Scaling */
        $ScaleValue = $this -> MinY + ( $this -> MaxY - $this -> MinY ) * $NormPos;
        /* Return result */
        return $ScaleValue >= $Value;
    }



    public function CalcMinMax()
    {
        $this -> MaxY = - INF;
        $this -> MinY = + INF;

        $c = count( $this -> Values );
        for( $i = 0; $i < $c; $i++ )
        {
            $Value = $this -> Values[ $i ];
            if( $this -> MinY > $Value ) $this -> MinY = $Value;
            if( $this -> MaxY < $Value ) $this -> MaxY = $Value;
        }

        return $this;
    }



    public function Line()
    {
        if( $this -> LineCache == null )
        {
            $this -> LineCache = '';

            $Cur = ceil( $this -> Position / count( $this -> Values ) * $this -> Width );

            for( $PosX = 0; $PosX < $this -> Width; $PosX++ )
            {
                /* Get value by position */
                $Value = $this -> GetMaxValueByPos( $PosX );

                $Delta = $this -> MaxY - $this -> MinY;
                $NormValue = $Delta > 0 ? ( $Value - $this -> MinY ) / $Delta : 0;

                if( $Cur == $PosX )         $Char = '▓';
                elseif( $NormValue <= 0.1 ) $Char = '_';
                elseif( $NormValue <= 0.2 ) $Char = '▁';
                elseif( $NormValue <= 0.3 ) $Char = '▂';
                elseif( $NormValue <= 0.4 ) $Char = '▃';
                elseif( $NormValue <= 0.5 ) $Char = '▄';
                elseif( $NormValue <= 0.6 ) $Char = '▅';
                elseif( $NormValue <= 0.7 ) $Char = '▆';
                elseif( $NormValue <= 0.8 ) $Char = '▇';
                else                        $Char = '█';

                $this -> LineCache .= $Char;
            }
        }

        return $this -> LineCache;
    }



    public function Print( $APosY )
    {
        $Result = str_pad( '', $this -> Width, ' ' );
        for( $PosX = 0; $PosX < $this -> Width; $PosX++ )
        {
             $Result[ $PosX ] = $this -> TestByPos( $PosX, $APosY ) ? ' ' : '*';
        }

        /* Normalize position */
        $NormPos = $APosY / $this -> Height;
        /* Scaling */
        $ScaleValue = $this -> MinY + ( $this -> MaxY - $this -> MinY ) * $NormPos;

        return sprintf( "%' 9f|", $ScaleValue ) . $Result;
    }



    /*
    */
    public function Dump( $ACallback, &$AParams = null )
    {
        for( $PosY = $this -> Height-1; $PosY >= 0; $PosY -- )
        {
            call_user_func_array( $ACallback, array( $this, $this -> Print( $PosY ), &$AParams ));
        }
        return $this;
    }



    /*
        Setters and getters
    */
    public function SetLength( $AValue )
    {
        $this -> Values = array_pad( $this -> Values, $AValue, 0 );
        if( $this -> Position > count( $this -> Values )) $this -> Position = 0;
        return $this;
    }



    public function GetLength()
    {
        return $this -> Length;
    }



    public function GetMinY()
    {
        return $this -> MinY;
    }



    public function GetMaxY()
    {
        return $this -> MaxY;
    }



    public function SetWidth( $AValue )
    {
        $this -> Width = $AValue;
        return $this;
    }
}
