<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

namespace catlair;

/*
    Author:
        still@itserv.ru
        igorptx@gmail.com
*/

require_once( ROOT . '/core/loader.php' );
require_once( ROOT . '/core/url.php' );



class TPayload extends TResult
{
    private $Parent = null; /* Parent of mutant */



    /*
        Payload constructor
    */
    function __construct
    (
        $AApp   /* Application object */
    )
    {
        $this -> App = $AApp;
    }



    /*
        Create the Payload
        This is a template for extendsible objects and should be overriden
    */
    static public function Create
    (
        $AApp,          /* Application object */
        string $AClass  /* Pusa class for call */
    )
    {
        $Result = new self( $AApp );
        if( empty( $AClass ))
        {
            $Result -> Mutate( $ACalss );
        }
        return $Result;
    }



    /*
        Load payload module
    */
    public function Load
    (
        string $AClass = null /* Pusa class name */
    )
    {

        /* Check classname exists */
        if( !class_exists( __NAMESPACE__ . '\\' . $AClass ))
        {
            Loader::create()
            -> Load( $this -> GetApp() -> GetPayloadFile( $AName ))
            -> ResultTo( $this );
        }
        return $this;
    }



    /*
        Mutate to another payload
    */
    public function Mutate
    (
        string $AClass = null /* Pusa class name */
    )
    {
        /* Setting this Pusa as result */
        $Result = $this;

        /* Redefine class */
        $AClass = empty( $AClass ) ? $this -> Class : $AClass;
        $ClassName = basename( $AClass );
        $Class = 'catlair\\' . $ClassName;

        $this -> Load( $AClass );

        /* Checking the class existing */
        if( $this -> IsOk() )
        {
            if( class_exists( $Class ))
            {
                /* ... and create new typed Pusa */
                $Result = new $Class( $this -> App );
                /* Write parent for mutant */
                $Result -> setParent( $this );
            }
            else
            {
                $this -> SetCode( 'ClassNotFound' );
            }
        }
        else
        {
            $this -> GetLog() -> Error( $this -> GetCode() ) -> Dump( $this -> GetDetailes() );
        }

        /* Cloning */
        if( $Result != $this )
        {
            $Result -> ResultFrom( $this );
        }

        if( !$Result -> IsOK() )
        {
            $this -> SetDetaile( 'Class', $AClass );
        }

        return $Result;
    }



    /*
        Clone the Pusa object
    */
    public function Clone()
    {
        /* Setting this Pusa as result */
        $Class = get_class( $this );
        $Result = new $Class( $this -> App, $Class );
        return $Result;
    }



    /*
        Return true if method exists
    */
    public function IsMethod( $AMethod )
    {
        return method_exists( $this, $AMethod );
    }



    /*
        Call payload method
    */
    public function CallMethod
    (
        $AMethod,       /* Method name */
        $AStrict = true /* Method must exists */
    )
    {
        if( method_exists( $this, $AMethod ))
        {
            $this -> Log
            -> Begin( 'Run' )
            -> Param( 'Class', $this -> Class )
            -> Param( 'Method', $AMethod );

            try
            {
                call_user_func_array
                (
                    [ $this, $AMethod ],
                    $this -> GetSendingParameters( $AMethod, $AParams )
                );
            }
            catch( \Throwable $Error )
            {
                $this -> SetResult
                (
                    'ErrorCallMethod',
                    null,
                    [
                        'Class'     => get_clas( $this ),
                        'Message'   => $Error -> getMessage(),
                        'File'      => $Error -> getFile(),
                        'Line'      => $Error -> getLine()
                    ]
                );
            }

            $this -> Log -> End();
        }
        else
        {
            if( $AStrict )
            {
                $this -> SetResult
                (
                    'MethodNotExists',
                    null,
                    [
                        'Class'     => get_clas( $this ),
                        'Method'    => $AMethod
                    ]
                );
            }
        }
        return $this;
    }



    /*
        Run pusa controller
    */
    public function Run
    (
        string  $AMethod,
        array   $AParams            = [],
        bool    $AMethodIsExist     = true
    )
    {
        if( $this -> IsOk() )
        {
            /* On Before run method */
            $this -> CallMethod( 'OnBeforeRun', false );

            if( $this -> IsOk() )
            {
                /* Execute payload method */
                $this -> CallMethod( $AMethod, true );
                /* Action on after run */
                $this -> CallMethod( 'OnAfterRun', false );
            }
        }
        return $this;
    }



    /*
        Return list of parameters for method
    */
    private function GetSendingParameters
    (
        string $AMethod,        /* Method name */
        array $AParams = []     /* List of incoming parameters */
    )
    {
        /* Prepare parameters */
        $Method = new \ReflectionMethod( $this, $AMethod );
        $WaitingParams = $Method -> getParameters();
        $Result = [];

        /* Translate names to lower case */
        $LowerParams = [];
        foreach( $AParams as $Name => $Value )
        {
            $LowerParams[ clAnyToLower( $Name )] = $Value;
        }

        /* Fill method parameters from $AParams */
        foreach( $WaitingParams as $Param )
        {
            $Name = $Param -> getName();
            switch( $Param -> getType() )
            {
                case 'int':     $Default = 0;       break;
                case 'float':   $Default = 0.0;     break;
                case 'string':  $Default = '';      break;
                case 'bool':    $Default = false;   break;
                case 'array':   $Default = [];      break;
                default:        $Default = null;    break;
            }
            $Default = $Param -> isDefaultValueAvailable() ? $Param -> getDefaultValue() : $Default;
            $Value = clValueFromObject( $LowerParams,  clAnyToLower( $Name ), $Default );
            $Result[ $Name ] = $Value;
        }

        return $Result;
    }



    /*
        Switch to other payload
    */
    public function Switch
    (
        $APayload
    )
    {
        return $APayload;
    }



    public function Return()
    {
        return
        empty( $this -> GetParent() )
        ? $this
        : $this -> Switch( $this -> GetParent() );
    }



    /******************************************************************
        Getters and setters
    */

    public function GetLog()
    {
        return $this -> App -> GetLog();
    }



    /*
        Return parent
    */
    public function GetParent()
    {
        return $this -> Parent;
    }



    /*
        Set parent payload
    */
    private function SetParent( $AValue )
    {
        $this -> Parent = $AValue;
        return $this;
    }

}
