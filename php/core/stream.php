<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.


    still@itserv.ru 16.10.2019
*/

namespace catlair;



include_once 'debug.php';


class TStream extends TResult
{
    /*
        private propertyes
    */
    private $FPosition = 0;     /* Current position in stream */


    /*
        public propertyes
    */
    public $Buffer = '';        /* Memory buffer */
    public $Portion = 655360;    /*Size buffer for read or write operations */


    /*
        Constructor for class
    */
    function __construct()
    {
        /* Set result code */
        $this->Code = rcOk;
    }



    /*
        Reset buffer and position
    */
    public function &Reset()
    {
        $Buffer = '';
        $FPosition = 0;
        return $this;
    }



    /*
        Return current position
    */
    public function GetPosition()
    {
        return $this->FPosition;
    }



    public function SetPosition($APosition)
    {
        $Size = $this->GetSize();
        if ($APosition >= $Size) $this->FPosition = $Size;
        else $this->FPosition = $APosition;
        return $this;
    }



    public function ShiftPosition($ADelta)
    {
        $this->SetPosition($this->FPosition + $ADelta);
        return $this;
    }



    /* Читет сождержимое из буфера */
    public function &ReadFrom(&$AStream)
    {
        $Continue = true;
        while (!$AStream->IsEnd() && $this->IsOk() && $Continue)
        {
            $Buffer = $AStream->ReadBuffer($this->Portion);
            $Continue = strlen($Buffer) > 0;
            $this->WriteBuffer($Buffer);
        }
        return $this;
    }



    /* Пишет содержимое в буфер */
    public function &WriteTo(&$AStream)
    {
        while (!$this->IsEnd() && $this->IsOk())
        {
            $Buffer = $this->ReadBuffer($this->Portion);
            $AStream->WriteBuffer($Buffer);
        }
        return $this;
    }



    /*
        Return size of stream
    */
    public function GetSize()
    {
        return strlen($this->Buffer);
    }



    /*
        Return true if end of stream
    */
    public function IsEnd()
    {
        return !($this->GetPosition() < $this->GetSize());
    }



    /*
        Return buffer form strem and change position in stream
    */
    public function ReadBuffer($ACount)
    {
        $Result = substr($this->Buffer, $this->GetPosition(), $ACount);
        $this->FPosition = $this->GetPosition() + strlen($Result);
        return $Result;
    }



    public function &WriteBuffer($Buffer)
    {
        $this->Buffer .= $Buffer;
        return $this;
    }
}
