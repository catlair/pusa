<?php

/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/


/*
    Main daemon unit.
    Based on systemctl
*/

namespace catlair;

require_once( ROOT . '/core/result.php' );
require_once( ROOT . '/core/debug.php' );
require_once( ROOT . '/core/shell.php' );
require_once( ROOT . '/core/moment.php' );



class TDaemon extends TResult
{
    private $Active             = false;

    public $Log                 = null;
    public $CLIParams           = [];
    public $ServiceCaption      = 'Catlair daemon';
    public $ServiceName         = 'catlair';
    public $StartAfter          = 'syslog.target network.target';
    public $StartUser           = 'root';
    public $PIDFileName         = null;
    public $ExecStart           = '/usr/bin/php';
    public $WorkingDirectory    = '/tmp';
    public $SkipPause           = false;
    public $MainPause           = CL_SECOND;



    function __construct
    (
        $ALog,          /* TLog object */
        $ACLIParams     /* CLI params array */
    )
    {
        $this -> CLIParams = $ACLIParams;
        $this -> Log = $ALog;
    }



    static public function Create
    (
        $ALog,
        $ACLIParams
    )
    {
        return new TDaemon( $ALog, $ACLIParams );
    }



    public function &Init()
    {
        if( method_exists( $this, 'BeforeInit' ))
        {
            $this -> BeforeInit();
        }
        return $this;
    }



    public function &Start()
    {
        if( array_search( '--log', $this -> CLIParams ))
        {
            $this -> Log -> SetDestination( TLog::FILE );
        }
        else
        {
            $this -> Log -> SetDestination( TLog::CONSOLE );
        }

        if( array_search( '--help',      $this -> CLIParams)) $this -> Help();
        if( array_search( '--install',   $this -> CLIParams)) $this -> Install();
        if( array_search( '--uninstall', $this -> CLIParams)) $this -> Uninstall();
        if( array_search( '--execute',   $this -> CLIParams)) $this -> Loop();
        if( array_search( '--daemon',    $this -> CLIParams)) $this -> Daemon();
        if( array_search( '--expel',     $this -> CLIParams)) $this -> Expel();
        return $this;
    }



    private function SignalInt()
    {
        $this->Active = false;
    }



    private function SignalTerm()
    {
        $this -> Active = false;
    }



    public function &Terminate()
    {
        $this -> Active = false;
        return $this;
    }



    /*
        Main loop for daemon
    */
    public function &Loop()
    {
        /* Устанавливаем сигнал SIGTERM */
        pcntl_signal( SIGTERM, [ $this, 'SignalTerm' ]);

        /* Устанавливаем сигнал SIGHUP */
        pcntl_signal( SIGINT, [ $this, 'SignalInt' ]);

        /* Call before action */
        if ( method_exists( $this, 'BeforeLoop' )) $this -> BeforeLoop();

        /* Start main loop */
        $this -> Active = true;
        $this -> Log -> Begin( 'Loop' ) -> LineEnd();
        while( $this -> Active )
        {
            /* Skip pause set as false. Job can be set skip pause in true */
            $this -> SkipPause = false;

            /* Main job */
            if( method_exists( $this, 'Job')) $this -> Job();

            /* Loop pause with signal interrupt */
            $PauseAccum = 0;
            while
            (
                $PauseAccum == 0 ||                     /* первый цикл сработает всегда */
                ! $this -> SkipPause &&                 /* не надо пропускать паузу */
                $PauseAccum < $this -> MainPause &&     /* пауза меньше основной */
                $this -> Active                         /* сервис еще активен */
            )
            {
                pcntl_signal_dispatch();
                $Pause = 10 * TMoment::MILISECOND;
                $PauseAccum += $Pause;
                usleep( $Pause );
            }
        }

        /* Call After action */
        if ( method_exists( $this, 'AfterLoop' )) $this -> AfterLoop();

        $this -> Log -> End();
        return $this;
    }



    /*
        Print help information
    */
    public function &Help()
    {
        $this->Log
        -> Begin( 'Help' )
        -> Trace() -> Param( 'Service name'     , $this -> ServiceName)
        -> Trace() -> Param( 'Caption'          , $this -> ServiceCaption)
        -> Trace() -> Param( 'Log folder'       , $this -> Log -> Path)
        -> Trace() -> Param( 'Service start '   , $this -> GetExecStart())
        -> Trace() -> Text( 'Options')
        -> Trace() -> Key( '--help'             , 'this help' )
        -> Trace() -> Key( '--install'          , 'install systemctl service' )
        -> Trace() -> Key( '--uninstall'        , 'uninstall systemctl service' )
        -> Trace() -> Key( '--execute'          , 'execute application in CLI mode' )
        -> Trace() -> Key( '--daemon'           , 'demonize application' )
        -> Trace() -> Key( '--expel'            , 'expel the demon' )
        -> Trace() -> Key( '--log'              , 'write output in to log file' )
        -> End();
        return $this;
    }



    /**************************************************************************
        Systemctl based functional
    */

    /*
        Installing service with systemctl
    */
    public function Install()
    {
        $SystemdFileName = $this->GetServiceFile();

        $this->Log
        -> Begin( 'Service install' )
        -> Param( 'File', $SystemdFileName );

        $Handle = fopen($SystemdFileName, 'w+');
        if ($Handle === false) $this->Log->Error() -> Param('Error install service', $SystemdFileName);
        else
        {
            fwrite($Handle, '[Unit]'.PHP_EOL);
            fwrite($Handle, 'Description='      . $this->ServiceCaption . PHP_EOL);
            fwrite($Handle, 'Requires='         . PHP_EOL);
            fwrite($Handle, 'After='            . $this->StartAfter . PHP_EOL);

            fwrite($Handle, '[Service]'.PHP_EOL);
            fwrite($Handle, 'User='             . $this->StartUser . PHP_EOL);
            fwrite($Handle, 'Type='             . 'simple'.PHP_EOL);
            fwrite($Handle, 'TimeoutSec='       . '10'.PHP_EOL);
            fwrite($Handle, 'PIDFile='          . $this -> GetPIDFile() . PHP_EOL );
            fwrite($Handle, 'ExecStart='        . $this -> GetExecStart() . PHP_EOL );
            fwrite($Handle, 'WorkingDirectory=' . $this -> GetWorkingDirectory() . PHP_EOL );
            fwrite($Handle, 'KillMode='         . 'process' . PHP_EOL);
            fwrite($Handle, 'Restart='          . 'always' . PHP_EOL);
            fwrite($Handle, 'RestartSec='       . '30s' . PHP_EOL);

            fwrite($Handle, '[Install]'.PHP_EOL);
            fwrite($Handle, 'WantedBy='         . 'default.target'.PHP_EOL);
            fwrite($Handle, 'Alias='            . $this->ServiceName.'.service');
            fclose($Handle);

            $this
            -> Log
            -> Info()
            -> Param('Create service file', $SystemdFileName);

            /* Create new shell */
            $Shell = new TShell( $this -> Log );
            $Shell -> Cmd( 'systemctl daemon-reload' );
            if ( ! $this -> IsInstall()) $Shell -> Cmd( 'systemctl enable ' . $this->ServiceName );
        }
        $this -> Log -> End();

        return $this;
    }



    /*
        Гninstalling service
    */
    public function Uninstall()
    {
        $this -> Log -> Begin();
        if ($this -> IsInstall())
        {
            $SystemdFileName = $this->GetServiceFile();
            $Shell = new TShell( $this -> Log );
            $Shell
            -> Cmd( 'systemctl stop ' . $this->ServiceName)
            -> Cmd( 'systemctl disable ' . $this->ServiceName)
            -> Cmd( 'systemctl daemon-reload' )
            -> Cmd( 'systemctl reset-failed' );

            if ( file_exists( $SystemdFileName )) unlink( $SystemdFileName );
        }
        $this -> Log -> End();
        return $this;
    }



    public function IsInstall()
    {
        $Shell = new TShell( $this -> Log );
        $Shell -> Cmd( 'systemctl is-enabled ' . $this -> ServiceName );
        return count( $Shell -> CommandResultLines ) > 0 && $Shell -> CommandResultLines[0] == 'enabled';
    }



    /**************************************************************************
        Daemon on posx
    */



    /*
        Return true if daemon exists
    */
    public function IsDaemonActive()
    {
        $Result = false;
        $PIDFile = $this -> GetPIDFile();
        if( file_exists( $PIDFile ) && is_file( $PIDFile ))
        {
            $Result = posix_kill
            (
                file_get_contents( $PIDFile ),
                0
            );
        }
        return $Result;
    }



    /*
        Daemonize the process
    */
    private function Daemon()
    {
        if( !$this -> IsDaemonActive())
        {
            $ChildPID = pcntl_fork();
            if( $ChildPID )
            {
                /* Parent pocess */
            }
            else
            {
                /* Chils process */

                /* Write PID */
                $this -> WritePIDFile( getmypid() );

                /* Child process detach from console */
                fclose( STDIN );
                fclose( STDOUT );
                fclose( STDERR );

                /* Child process transforms to main process */
                posix_setsid();

                /* Run main loop */
                $this -> Loop();

                /* Delete PID file after loop */
                $this -> DeletePIDFile();
            }
        }
        return $this;
    }



    /*
        Expel the daemon
    */
    function Expel()
    {
        if( $this -> IsDaemonActive() )
        {
            $PIDFile = $this -> GetPIDFile();
            if( file_exists( $PIDFile ) && is_file( $PIDFile ))
            {
                posix_kill
                (
                    file_get_contents( $PIDFile ),
                    SIGTERM
                );
            }
        }
        return $this;
    }



    public function WritePIDFile( $APID )
    {
        file_put_contents
        (
            $this -> GetPIDFile(),
            $APID
        );
        return $this;
    }



    public function DeletePIDFile()
    {
        unlink( $this -> GetPIDFile() );
        return $this;
    }



    /**************************************************************************
        Setters and getters
    */

    public function GetExecStart()
    {
        return $this -> ExecStart;
    }



    public function SetExecStart( $AValue )
    {
        $this -> ExecStart = $AValue;
        return $this;
    }



    public function GetWorkingDirectory()
    {
        return $this -> WorkingDirectory;
    }



    public function SetWorkingDirectory( $AValue )
    {
        $this -> WorkingDirectory = $AValue;
        return $this;
    }



    public function GetActive()
    {
        $this -> Active;
        return $this;
    }



    private function GetServiceFile()
    {
        return '/etc/systemd/system/' . $this->ServiceName.'.service';
    }



    private function GetPIDFile()
    {
        return
        empty( $this -> PIDFileName )
        ? '/var/run/' . $this -> ServiceName . '.pid'
        : $this -> PIDFileName;
    }
}
