<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/


namespace catlair;

require_once( 'utils.php' );

class TTextExtractor
{
    /*
        Extract list of parametes from text.

        Input array of rules:
        [
            0 =>
            [

                "Key" => "KeyName"
                "From" => "String value or start position in format {Line:Position}"
                "To" => "String value or {length}"
            ],
            ...
        }

        Return array: [ "Target" => "Value", .... ]
    */
    static public function Parse( $AText, $ARules )
    {
        $Result = [];
        $AText = str_replace( '\r', ' ', $AText );
        $Lines = explode( '\n', $AText );

        foreach( $ARules as $Rule )
        {
            /* Getting parameters */
            $From       = clValueFromObject( $Rule, 'From' );
            $To         = clValueFromObject( $Rule, 'To' );
            $Key        = clValueFromObject( $Rule, 'Key' );
            $Default    = clValueFromObject( $Rule, 'Default' );

            /* Set end of line for To */
            if( empty( $To )) $To = PHP_EOL;

            if( !empty( $Key ) && !empty( $From ))
            {
                /* Check From to {line;col} notation */
                if( substr( $From, 0, 1 ) == '{' && substr( $From, -1, 1 ) == '}' && strpos( $From, ';' ) !== false )
                {
                    $Lex        = explode( ';', $From );
                    $Line       = (int) substr( $Lex[0], 1 );
                    $PosFrom    = (int) substr( $Lex[1], 0, -1);
                }
                else
                {
                    $Line       = null;
                    $PosFrom    = strpos( $AText, $From );
                }

                /* Check To {length} notation */
                if( substr( $To, 0, 1 ) == '{' && substr( $To, -1, 1 ) == '}' )
                {
                    /* Read length */
                    $PosTo = $PosFrom + (int) substr( $To, 1, strlen( $To ) - 2 );
                }
                else
                {
                    /* Search marker of end */
                    if( $Line === null)
                    {
                        /* In all the text after PosFrom*/
                        $PosTo = strpos( $AText, $To, $PosFrom );
                    }
                    else
                    {
                        /* In current line after PosFrom */
                        $PosTo = strpos( $Lines[ $Line ], $To, $PosFrom );
                    }
                }


                if( $PosFrom > -1 && $PosFrom < $PosTo )
                {
                    if( $Line === null )
                    {
                        /* From all the text */
                        $ValueFrom = $PosFrom + strlen( $From );
                        $Result[ $Key ] = trim( substr( $AText, $ValueFrom , $PosTo - $ValueFrom ));
                    }
                    else
                    {
                        /* From line only */
                        if( count( $Lines ) > $Line )
                        {
                            $Result[ $Key ] = trim( substr( $Lines[ $Line ], $PosFrom, $PosTo - $PosFrom ));
                        }
                    }
                }

                if( empty( $Result[ $Key ])) $Result[ $Key ] = $Default;
            }
        }
        return $Result;
    }

    /* Test example
        print_r
        (
            TTextExtractor::Parse
            (
                'FirstName:swamp\n' .
                'SecondName:still EndParam\n' .
                'Age:99\n',
                [
                    [ 'Key' => 'FirstParam',    'From' => '{0;0}',          'To' => '{6}',  'Default' => 'Unknown' ],
                    [ 'Key' => 'FirstName',     'From' => 'FirstName:',     'To' => null ],
                    [ 'Key' => 'SecondName',    'From' => 'SecondName:',    'To' => 'EndParam', 'Default' => 'unk' ],
                    [ 'Key' => 'Age',           'From' => 'Age:' ]
                ]
            )
        );
    */
}
