<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

/*
    17.11.2019 - still@itserv.ru
*/

namespace catlair;

/*
    The basic class for any results.
*/


/* Default result code */
define ( 'rcOk', 'Ok' );
define ( 'rcUnknown', 'UnknownError' );



/* Define class */
class TResult
{
    const RC_OK         = rcOk;

    public $Code        = rcOk;
    public $Message     = '';
    public $Detaile     = [];
    public $InfoFlag    = false; /* Result copy as rcOk */
    public $LastOk      = [];



    /*
        return true if result is Ok
    */
    public function IsOk()
    {
        return $this -> Code == rcOk;
    }



    /*
        Method return true for Code = ACode
    */
    public function IsCode( $ACode )
    {
        return $this -> GetCode() == $ACode;
    }



    /*
        set result Ok
        ListCodeCheck - for error from ListCodeCheck the code is reset
    */
    public function &SetOk( $AListCodeCheck = null )
    {
        if( empty( $AListCodeCheck )) $AListCodeCheck = [];
        if( count( $AListCodeCheck ) == 0 || in_array( $this -> Code, $AListCodeCheck ))
        {
            $this -> InfoFlag = false;
            $this -> Code = rcOk;
        }
        $this -> LastOk = $AListCodeCheck;
        return $this;
    }



    /*
       Value set for result code
    */
    public function &SetCode( $AValue, $AInfoFlag = false )
    {
        $this -> InfoFlag = $AInfoFlag;

        if ( ! empty( $this -> Log ) && $AValue != rcOk && !$AInfoFlag)
        {
            $this -> Log
            -> Warning( 'Set code' )
            -> Param( 'Code', $AValue);
        }
        $this -> Code = $AValue;
        return $this;
    }



    public function &SetDetaile( $AName, $AValue )
    {
        $this -> Detaile[ $AName ] = $AValue;
        return $this;
    }



    public function GetDetaile( $AName, $ADefault )
    {
        if ( array_key_exists( $AName, $this -> Detaile ))
        {
            $Result = $this -> Detaile[ $AName ];
        }
        else
        {
            $Result = $ADefault;
        }
        return $Result;
    }



    public function GetDetailes()
    {
        return $Result = $this -> Detaile;
    }



    /*
        Get result code
    */
    public function GetCode()
    {
        return $this -> Code;
    }



    public function BuildMessage( $AReplace = [] )
    {
        $this -> SetMessage( $this -> GetMessage( $AReplace ));
    }



    /*
        Set result message
    */
    public function &SetMessage( $AValue )
    {
        $this -> Message = $AValue;
        return $this;
    }



    /*
       Set result message
    */
    public function GetMessage
    (
        array $AReplace = [], /* Named array for replace */
        string $APrefix = '',
        string $ASuffix = ''
    )
    {
        $Result = $this -> Message;
        if( empty( $Result )) $Message = $this -> Code;

        $Keys = [];
        $Values = [];

        /* Replace for datailes */
        foreach( $this -> Detaile as $Key => $Value )
        {
            array_push( $Keys, '%' . $Key . '%' );
            array_push( $Values, $APrefix . $Value . $ASuffix );
        }

        /* Replace for argument */
        foreach( $AReplace as $Key => $Value )
        {
            array_push( $Keys, $Key );
            array_push( $Values, $Value );
        }

        $Result = str_replace( $Keys, $Values, $Result );

        return $Result;
    }



    public function &SetResult
    (
        $ACode      = rcOk,
        $AMessage   = null,
        $ADetaile    = []
    )
    {
        $this -> SetCode( $ACode );
        $this -> SetMessage( $AMessage );
        $this -> Detaile = array_merge( $this -> Detaile, $ADetaile );
        if ( ! empty( $this -> Log ) && $ACode != rcOk && !empty( $ADetaile ))
        {
            $this -> Log
            -> Trace( 'Set Result' )
            -> Param( 'Code', $ACode)
            -> Dump( $ADetaile );
        }
        return $this;
    }



    public function &ResultFrom( &$AResult )
    {
        if ( $AResult -> InfoFlag )
        {
            $this -> SetOk();
            $this -> Message    = '';
        }
        else
        {
            $this -> Code       = $AResult -> Code;
            $this -> Message    = $AResult -> Message;
            $this -> Detaile    = array_merge( $this -> Detaile, $AResult -> Detaile );
        }
        return $this;
    }



    public function &ResultTo( &$ATarget )
    {
        if( $this -> InfoFlag )
        {
            $ATarget -> SetOk();
            $ATarget -> Message = '';
        }
        else
        {
            $ATarget -> Code    = $this -> Code;
            $ATarget -> Message = $this -> Message;
            $ATarget -> Detaile = array_merge( $ATarget -> Detaile, $this -> Detaile );
        }
        return $this;
    }



    /*
        TODO
        It can be remove from project.
        Use ResultTo, ResultFrom
    */
    public function &CopyResultFrom( &$AResult )
    {
        $this -> ResultFrom( $AResult );
        return $this;
    }



    /*
        return true if interfeca is CLI
    */
    public function IsCLI()
    {
        return php_sapi_name() == 'cli';
    }



    /*
        check interfeca is CLI
    */
    public function &CheckCLI()
    {
        if ( $this -> IsCLI() )
        {
            $this -> SetResult( rcOk, '' );
        }
        else
        {
            $this -> SetResult( 'IsNotCLI', 'This function only for CLI interface' );
        }
        return $this;
    }



    public function IfResult
    (
        bool   $AValue      = false,
        string $ACode       = rcOk,
        string $AMessage    = null,
        array  $ADetaile    = []
    )
    {
        if( $AValue )
        {
            $this -> SetResult( $ACode, $AMessage, $ADetaile );
        }
        return $this;
    }
}
