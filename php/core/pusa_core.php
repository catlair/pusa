<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

namespace catlair;

/*
    Pusa Back Core.
        Contains base structure and commands for Pusa technology.

    Manual:
        https://gitlab.com/catlair/pusa/-/blob/main/site/pusa/src/language_default/man/pusa.md

          ┌──>───── Request( URL,Arguments,GET,POST ) ───>──┐
          │                                                 │
    ╔═════╧═════╗                                     ╔═════╧════╗
    ║ PusaFront ║               (ajax)                ║ PusaBack ║
    ╚═════╤═════╝                                     ╚═════╤════╝
          │                                                 │
          └──<──── Responce( directives and content ) ───<──┘

    Comments:
        1. The php/core/pusa.php contains hilevel methods.

    Author:
        still@itserv.ru
*/

require_once( ROOT . '/core/loader.php' );
require_once( ROOT . '/core/payload.php' );
require_once( ROOT . '/core/url.php' );
require_once( ROOT . '/core/operator.php' );



class TPusaCore extends TPayload
{
    /*
        Commands and arguments of Pusa Core
    */

    /* Browser manipulation */
    const DEBUG                     = 'Debug';          /* Debug level for Pusa-Front */
        const DEBUG_ON              = 'On';             /* Debug on */
        const DEBUG_OFF             = 'Off';            /* Debug off */


    /* Actions with DOM elements */
    /* Set focus at DOM elements in browser ... */
        /* Directions for focusing DOM elements */
        const SELF                  = 'Self';           /* ... to event element (Default focus) */
        const WINDOW                = 'Window';         /* ... to window object (it is not a DOM element) */
        const PROPERTY              = 'Property';       /* ... to property */
        const METHOD                = 'Method';         /* ... to method */
        const BODY                  = 'Body';           /* ... to body */
        const PARENT                = 'Parent';         /* ... to parent for each focus element */
        const PARENT_FIRST          = 'ParentFirst';    /* ... to nearest parent by selector type and value */
        const PARENTS               = 'Parents';        /* ... to all parents elements by selector type and value */
        const CHILDS                = 'Children';       /* ... to all childs elements by selector type and value */
        const CHILDS_OF_THIS        = 'ChildrenOfThis'; /* ... to direct childs elements by selector type and value */
        /* Selectors for focusing DOM elements */
        const CLASS_NAME            = 'Class';          /* Focus elements by className */
        const ID                    = 'ID';             /* Focus elements by Identifier */
        const NAME                  = 'Name';           /* Focus elements by name */
        const TAG                   = 'Tag';            /* Focus elements by tag */

    const ATTR                      = 'Attr';           /* Set attributes for elements */
    const PROP                      = 'Prop';           /* Sett propertyes for elements */
    const REPLACE                   = 'Replace';        /* Replace any content */
    const STYLE                     = 'Style';          /* Set CSS attributes for elements */

    const READ_STATE                = 'ReadState';
    const SET_STATE                 = 'SetState';

    /* Send focus elements content from Pusa-Front to Pusa-Back */
    const SEND_ABORT                = 'abort';
    const SEND_ERROR                = 'error';
    const SEND_LOAD                 = 'load';
    const SEND_LOADEND              = 'loadend';
    const SEND_LOADSTART            = 'loadstart';
    const SEND_PROGRESS             = 'progress';
    const SEND_TIMEOUT              = 'timeout';

    /* Targets */
    const TARGET_DOM                = 'DOM';            /* Target for DOM elements */
    const TARGET_WINDOW             = 'window';         /* Target for window object */

    /* Set event for event DOM elements */
    /*
        These are the main evnts. If you need any events, you can use its names:
        https://developer.mozilla.org/en-US/docs/Web/Events
        LABEL_EVENT_CONSTSNTS
    */
    /* Keyboard */
    const KEY_DOWN                  = 'keydown';
    const KEY_PRESS                 = 'keypress';
    const KEY_UP                    = 'keyup';

    /* Events */
    const AUX_CLICK                 = 'auxclick';
    const CLICK                     = 'click';              /* click event name */
    const CHANGE                    = 'change';             /* click event name */
    const CONTEXT_MENU              = 'contextmenu';
    const BLUR                      = 'blur';

    const DBL_CLICK                 = 'dblclick';
    const MOUSE_DOWN                = 'mousedown';
    const MOUSE_ENTER               = 'mouseenter';
    const MOUSE_LEAVE               = 'mouseleave';
    const MOUSE_MOVE                = 'mousemove';          /* WARNING!!! not recommended for use */
    const MOUSE_OVER                = 'mouseover';
    const MOUSE_OUT                 = 'mouseout';
    const MOUSE_UP                  = 'mouseup';
    const POINTER_LOCK_CHANGE       = 'pointerlockchange';
    const POINTER_LOCK_ERROR        = 'pointerlockerror';
    const SELECT                    = 'select';             /* The constant name is different from value */
    const WHEEL                     = 'wheel';
    const SCROLL                    = 'scroll';

    /* Order items. This elements will be send in request on event */
    const ORDER_WINDOW              = 'window';             /* Window propertyes */
    const ORDER_DOCUMENT            = 'document';           /* Document propertyes */
    const ORDER_BODY                = 'body';               /* Document propertyes */

    /* Crteate new dom elements */
    const CONTAINER_DEFAULT         = 'DIV';                /* Default container type */
    const CONTAINER_FORM            = 'FORM';               /* Form container */

    /* New elements place */
    const FIRST                     = 'First';              /* Element will add firs in focus elements */
    const LAST                      = 'Last';               /* Элемент будет добавлен последним в Target */
    const BEFORE                    = 'Before';             /* Элемент будет добавлен перед Target */
    const AFTER                     = 'After';              /* Элемент будет добавлен после Target */

    /* Pile */
        const SET                   = 'Set';                /* Operator set. Value of pile will be replaced */
        const ADD                   = 'Add';                /* Operator add. Value of pile will be added */
    const PILE_REPLACE              = 'PileReplace';        /* Replace operation in pile */
    const CLIPBOARD                 = 'Clipboard';          /* Move pile in to browser clipboard */

    /* Target window for open */
    const NEW                       = '_blank';             /* ... in blank window */
    const CURRENT                   = '_self';              /* ... in current window */
    const PARENT_FRAME              = '_parent';            /* ... in the parent frame */
    const TOP                       = '_top';               /* */

    protected $URL                  = null;                 /* TURL object with address string from borowser */

    private $Replace                = [];                   /* Named array of replace for each command */
    protected $Commands             = [];                   /* Array accumulator for Pusa commands */
    protected $Class                = null;
    protected $Log                  = null;
    private $ID                     = null;                 /* ID for pust. it will use for State Domain resolving */

    private     $Builder            = null;
    protected   $Library            = null;
    protected   $Content            = null;
    private     $Parent             = null;                 /* Parent of mutant */
    private     $Params             = [];                   /* List of parameters for Pusa */



    function __construct
    (
        $ABuilder,
        $AClass
    )
    {
        $this -> SetOk();
        $this -> Log        = $ABuilder;
        $this -> Builder    = $ABuilder;
        $this -> Class      = $AClass;
    }



    /*
        Pusa constructor
        This is a template for extendsible objects and should be overriden
    */
    static public function Create
    (
        $ABuilder,          /* TBuilder object */
        string $AClass      /* Pusa class for call */
    )
    {
       $Pusa = new TPusaCore( $ABuilder, $AClass );
       return $Pusa -> Prepare() -> Mutate( $AClass );
    }



    /*
        Prepareing the library file name.
        This is a template for extendsible objects and should be overriden
    */
    protected function PrepareLibrary( $AClass)
    {
        $this -> Library = 'pusa/' . $AClass . '.php';
        return $this;
    }



    /*
        Preparing Pusa object after creation
    */
    protected function Prepare()
    {
        $this -> URL = TURL::Create()
        -> Parse
        (
            array_key_exists( 'URL', $_POST )
            /* Return URL form GET. Pusa will send URL for each request */
            ? $_POST[ 'URL' ]
            /* If URL not exists into GET, returns the request URL */
            :
            (
                clValueFromObject( $_SERVER, 'REQUEST_SCHEME', 'https' ) . '://' .
                clValueFromObject( $_SERVER, 'HTTP_HOST', 'localhost' ) .
                clValueFromObject( $_SERVER, 'REQUEST_URI', '' )
            )
        )
        -> NoChanged();

        return $this;
    }



    /*
        Call another pusa controller
    */
    public function Mutate
    (
        string $AClass = null /* Pusa class name */
    )
    {
        /* Setting this Pusa as result */
        $Result = $this;

        /* Redefine class */
        $AClass = empty( $AClass ) ? $this -> Class : $AClass;
        $ClassName = basename( $AClass );
        $Class = 'catlair\\' . $ClassName;

        /* Load library if it not exists */
        if( ! class_exists( $Class ))
        {
            $this -> PrepareLibrary( $AClass );

            if( $this -> IsOk() )
            {
                TLoader::Create() -> Load( $this -> Library ) -> ResultTo( $this );
            }
        }

        /* Checking the class existing */
        if( $this -> IsOk() )
        {
            if( class_exists( $Class ))
            {
                /* ... and create new typed Pusa. Dont touch the new!!! */
                $Result = new $Class( $this -> Builder, $ClassName );

                $Result
                -> SetParams( $this -> Params )     /* Set parameters from parent */
                -> SetID( $this -> GetID() )        /* Set ID for mutant */
                -> Exec( 'OnBeforeRun' )            /* Execute onbefore method */
                -> LoadPublic( $this -> Params );   /* Load public propertyes */

                /* Write parent for mutant */
                $Result -> Parent = $this;
            }
            else
            {
                $this -> SetCode( 'ClassNotFound' );
            }
        }
        else
        {
            $this -> GetLog() -> Error( $this -> GetCode() ) -> Dump( $this -> GetDetailes() );
        }

        /* Cloning */
        if( $Result != $this )
        {
            $Result -> ResultFrom( $this );
            $Result -> URL          = $this -> URL;
        }

        if( !$Result -> IsOK() )
        {
            $this -> SetDetaile( 'Class', $AClass );
        }

        return $Result;
    }



    /*
        Clone the Pusa object
    */
    public function Clone()
    {
        /* Setting this Pusa as result */
        $Class = get_class( $this );
        $Result = new $Class( $this -> Builder, $Class );

        $Result -> SetParams( $this -> Params );     /* Set parameters from parent */

        /* Cloning */
        $Result -> URL          = $this -> URL;

        return $Result;
    }



    /*
        Execute one method
    */
    private function Exec
    (
        string $AMethod,        /* Method */
        bool $AStrict = false   /* Method exists control */
    )
    {
        if( $this -> IsOk() )
        {
            if( method_exists( $this, $AMethod ))
            {
                $SendingParams = $this -> GetSendingParameters( $AMethod, $this -> Params );
                try
                {
                    call_user_func_array([ $this, $AMethod ],  $SendingParams );
                }
                catch( \Throwable $Error )
                {
                    $this -> SetResult
                    (
                        'ErrorCallMethod',
                        null,
                        [
                            'Class'     => $this -> Class,
                            'Message'   => $Error -> getMessage(),
                            'File'      => $Error -> getFile(),
                            'Line'      => $Error -> getLine()
                        ]
                    );
                }
            }
            else
            {
                if( $AStrict )
                {
                    $this -> SetResult
                    (
                        'MethodDoesNotExists',
                        null,
                        [
                            'Class'     => $this -> Class,
                            'Method'    => $AMethod
                        ]
                    );
                }
            }
        }
        return $this;
    }



    /*
        Run pusa controller
    */
    public function Run
    (
        string  $AMethod,
        bool    $AStrict = true    /* Method exists control */
    )
    {
        if( $this -> IsOk() )
        {
            $this -> Log
            -> Begin( 'Run' )
            -> Param( 'Class', $this -> Class )
            -> Param( 'Method', $AMethod );

            /* On Before run method */
            $this
            -> Exec( 'OnBeforeRun' )            /* Execute onbefore method */
            -> LoadPublic( $this -> Params )    /* Load public propertyes */
            -> Exec( $AMethod, $AStrict )       /* Execute pusa method */
            -> Exec( 'OnAfterRun' )             /* Action on after run */
            -> SavePublic()
            ;

            /* URL */
            if( $this -> GetURL() -> IsChanged() )
            {
                $this -> ChangeURL( $this -> GetURL() -> ToString() );
            }

            $this -> Log
            -> End();
        }
        return $this;
    }


    /*
        Return true if method exists
    */
    public function IsMethod( $AMethod )
    {
        return method_exists( $this, $AMethod );
    }




    /*
        Load public propertyes from array of parameters
    */
    protected function LoadPublic
    (
        array $AParams = []
    )
    {
        $Reflect = new \ReflectionClass( $this );
        $ThisClass = $Reflect -> GetName();    /* Get current class */

        foreach( $Reflect -> getProperties() as $Property )
        {
            if
            (
                $Property -> isPublic() &&                                  /* Property is public */
                $ThisClass == $Property -> getDeclaringClass() -> getName() /* Property declared for this class */
            )
            {
                /* Read values */
                $PropertyName = $Property -> getName();
                $this -> $PropertyName = clValueFromObject
                (
                    $AParams,
                    [
                        'Propertyes',
                        'Public',
                        $PropertyName
                    ]
                );
            }
        }
        return $this;
    }



    /*
        Save public propertyes to Pusa State
    */
    protected function SavePublic()
    {
        $ThisClass = get_class( $this );    /* Get current class */
        $Reflect = new \ReflectionClass( $this );
        $Public = [];
        foreach( $Reflect -> getProperties() as $Property )
        {
            $PropertyName = $Property -> getName();
            if
            (
                $Property -> isPublic() &&
                $ThisClass == $Property -> getDeclaringClass() -> getName()
            )
            {
                /* Write values */
                $PropertyName = $Property -> getName();
                $Public[ $PropertyName ] = $this -> $PropertyName;
            }
        }
        $this -> SetState( $Public );
        return $this;
    }



    /*
        Return list of parameters for method
    */
    private function GetSendingParameters
    (
        string $AMethod,        /* Method name */
        array $AParams = []     /* List of incoming parameters */
    )
    {
        /* Prepare parameters */
        $Method = new \ReflectionMethod( $this, $AMethod );
        $WaitingParams = $Method -> getParameters();
        $Result = [];

        /* Translate names to lower case */
        $LowerParams = [];
        foreach( $AParams as $Name => $Value )
        {
            $LowerParams[ clAnyToLower( $Name )] = $Value;
        }

        /* Fill method parameters from $AParams */
        foreach( $WaitingParams as $Param )
        {
            $Name = $Param -> getName();
            switch( $Param -> getType() )
            {
                case 'int':     $Default = 0;       break;
                case 'float':   $Default = 0.0;     break;
                case 'string':  $Default = '';      break;
                case 'bool':    $Default = false;   break;
                case 'array':   $Default = [];      break;
                default:        $Default = null;    break;
            }
            $Default = $Param -> isDefaultValueAvailable() ? $Param -> getDefaultValue() : $Default;
            $Value = clValueFromObject( $LowerParams,  clAnyToLower( $Name ), $Default );
            $Result[ $Name ] = $Value;
        }

        return $Result;
    }



    /*
        Switch to other pusa
        Merge commands and return Pusa from argument
    */
    public function Switch
    (
        $APusa
    )
    {
        $APusa -> Commands = array_merge
        (
            $APusa -> GetCommands(),
            $this -> GetCommands()
        );
        return $APusa;
    }



    public function Return()
    {
        return
        empty( $this -> Parent )
        ? $this
        : $this -> SavePublic() -> Switch( $this -> Parent );
    }



    /*
        Dump commands to log file
    */
    public function DumpCommands()
    {
        if( $this -> Log -> GetEnabled() )
        {
            /* Write title */
            $this -> Log -> SetTable
            ([
                [ 'Color' => TLog::COLOR_LABEL, 'Length' => 4,  'Pad' => ' ', 'Align' => STR_PAD_LEFT ],
                [ 'Color' => TLog::COLOR_VALUE, 'Length' => 15, 'Pad' => ' ', 'Align' => STR_PAD_RIGHT ],
                [ 'Color' => TLog::COLOR_TEXT,  'Length' => 15, 'Pad' => ' ', 'Align' => STR_PAD_RIGHT ]
            ]);

            /* Write records */
            foreach( $this -> Commands as $Index => $Command )
            {
                /* Colored commands */
                switch( $Command[0] )
                {
                    case 'Debug'            : $CommandColor = TLog::COLOR_DEBUG;    break;
                    case 'Focus'            :
                    case 'FocusPop'         :
                    case 'FocusProperty'    :
                    case 'FocusPush'        : $CommandColor = TLog::ESC_INK_GOLD;   break;
                    case 'Create'           : $CommandColor = TLog::ESC_INK_YELLOW; break;
                    case 'Select'           : $CommandColor = TLog::ESC_INK_GREEN;  break;
                    case 'Event'            : $CommandColor = TLog::ESC_INK_BLUE;   break;
                    case 'Call'             : $CommandColor = TLog::ESC_INK_MAGENTA;break;
                    default                 : $CommandColor = TLog::ESC_INK_GREY;   break;
                }

                /* Write commands */
                $this -> Log
                -> Trace()
                -> Cell( $Index )
                -> Cell( $Command[ 0 ], $CommandColor )
                -> Cell( json_encode( $Command[ 1 ] ));
            }
        }
        return $this;
    }



    /*
        Adding the new command for Pusa
        Each command will be send to Pusa Front.
    */
    private function AddCommand
    (
        $ACommand
        /*
            [0] - Command name
            [1] - Named array with parameters
        */
    )
    {
        if( count( $this -> Replace ) > 0 )
        {
            $Cmd = json_encode( $ACommand, true );
            foreach( $this -> Replace as $Key => $Value)
            {
                $Cmd = str_replace( '%'.$Key.'%', $Value,  $Cmd );
            }
            $ACommand = json_decode( $Cmd );
        }
        array_push( $this -> Commands, $ACommand );
        return $this;
    }



    /*
        Return Pusa answer structure in JSON format
    */
    public function GetAnswer
    (
        $AResult = null
    )
    {
        /* Return Pusa result as JSON string */
        return json_encode
        (
            ( object )
            [
                'Header' =>
                [
                    'Code'      => $AResult -> GetCode(),      /* Error code text */
                    'Message'   => $AResult -> GetMessage(),   /* Message text*/
                    'Detaile'   => $AResult -> Detaile         /* Named array with details */
                ],
                'Pusa'          => $this -> GetCommands(),     /* List of Pusa commands for Pusa-front*/
                'Content'       => $this -> Content
            ]
        );
    }



    public function SetContent( $AContent )
    {
        $this -> Content = $AContent;
        return $this;
    }



    public function GetContent()
    {
        return $this -> Content;
    }



    public function IsContent()
    {
        return !empty( $this -> Content );
    }



    /******************************************************************
        Getters and setters
    */

    public function GetLog()
    {
        return $this -> Log;
    }



    public function GetURL()
    {
        return $this -> URL;
    }



    public function GetCommands()
    {
        return $this -> Commands;
    }



    public function SetReplace( $AValue = [] )
    {
        $this -> Replace = $AValue;
        return $this;
    }



    /**************************************************************************
        Browser commands
    */

    /*
        Controlling debug mode on front
    */
    public function Debug
    (
        string  $ALevel = self::DEBUG_ON /* Debug level DEBUG_* */
    )
    {
        return $this
        -> AddCommand
        ([
            self::DEBUG,
            [ 'Level' => $ALevel ]
        ]);
    }


    /*
        Set property for DOM object
    */
    public function SetProperty
    (
        array $AValues = []
    )
    {
        return $this
        -> AddCommand
        ([
            'Set',
            [
                'Values' => $AValues
            ]
        ]);
    }



    /*
        Call JS method by name for DOM elements or other objects with parameters
    */
    public function Call
    (
        string  $AMethod,
        array   $AArguments = []
    )
    {
        return $this
        -> AddCommand
        ([
            'Call',
            [
                'Method' => $AMethod,
                'Arguments' => $AArguments
            ]
        ]);
    }



    /*
        Execute JS code
    */
    public function JS
    (
        $ACode  /* JS source code */
    )
    {
        return $this
        -> AddCommand([ 'JS', $ACode ]);
    }



    /*
        Loop for DOM element
    */
    public function Loop
    (
        $APusa,             /* TPusaCore-based object with commands for execution */
        $ATasks     = []    /* Tasks array for loop. Commands from each task will be execute once.*/
    )
    {
        return $this
        -> AddCommand
        ([
            'Loop',
            [
                'Commands'  => $APusa -> GetCommands(),
                'Tasks'     => ( array )$ATasks
            ]
        ]);
    }



    /*
        Event for Focus elements
        It will call JS addEventListener( $AEvent, ... )
        The backend will be called in the body of the event.
    */
    public function Event
    (
        $AClass,                            /*  1 Pusa class controller for call */
        string      $AMethod    = null,     /*  2 Pusa method in class */
        TPusaCore   $APusa      = null,     /*  3 Pusa Commands*/
        string      $AEvent     = null,     /*  4 Event constant with JS event name */
        int         $ATimeout   = 0,        /*  5 Timeot in milliseconds */
        bool        $ACatch     = false,    /*  6 Catch the even on this object. JS will be call stopPropagation() */
        array       $AFilter    = null      /*  7 Event will be call only for values Key = Value */
    )
    {
        if( gettype( $AClass ) == 'array')
        {
            $Prm = $AClass;
        }
        else
        {
            $Prm = [];
            if( !empty( $AClass ))      $Prm[ 'Class' ]     = $AClass;
            if( !empty( $AMethod ))     $Prm[ 'Method' ]    = $AMethod;
            if( !empty( $APusa ))       $Prm[ 'Commands']   = empty( $APusa ) ? null : $APusa -> GetCommands();
            if( !empty( $AEvent ))      $Prm[ 'Event' ]     = $AEvent;
            if( !empty( $ACatch ))      $Prm[ 'Catch' ]     = $ACatch;
            if( !empty( $ATimeout ))    $Prm[ 'Timeout' ]   = $ATimeout;
            if( !empty( $AFilter ))     $Prm[ 'Filter' ]    = $AFilter;
            if( !empty( $AFilter ))     $Prm[ 'Filter' ]    = $AFilter;
       }
        return $this -> AddCommand([ 'Event', $Prm ]);
    }



    /**************************************************************************
        Arguments
        Request the argument from browser
    */

    public function ReadProperty
    (
        string $AArgumentName,
        string $AProperty
    )
    {
        return
        $this -> AddCommand
        ([
            'Arg',
            [
                'Name'  => $AArgumentName,
                'Property'  => $AProperty
            ]
        ]);
    }



    public function ReadEvent
    (
        string $AArgumentName,
        string $AEventProperty
    )
    {
        return
        $this -> AddCommand
        ([
            'Arg',
            [
                'Name'  => $AArgumentName,
                'Event' => $AEventProperty
            ]
        ]);
    }



    public function ReadMethod
    (
        string  $AArgumentName,
        string  $AMethod,
        array   $AMethodArguments = []
    )
    {
        return
        $this -> AddCommand
        ([
            'Arg',
            [
                'Name'      => $AArgumentName,
                'Method'    => $AMethod,
                'Arguments' => $AMethodArguments
            ]
        ]);
    }



    public function ReadJS
    (
        string $AArgumentName,
        string $AJS
    )
    {
        return
        $this -> AddCommand
        ([
            'Arg',
            [
                'Name'  => $AArgumentName,
                'JS'    => $AJS
            ]
        ]);
    }



    public function ReadStatics
    (
        array $AValues
    )
    {
        return
        $this -> AddCommand
        ([
            'Arg',
            [ 'Statics' => $AValues ]
        ]);
    }



    public function ReadStatic
    (
        string $AArgumentName,
        string $AStaticValue = null
    )
    {
        return $this
        -> ReadStatics
        (
            [ $AArgumentName => $AStaticValue ]
        );
    }



    /*
        Read fields from form
    */
    public function ReadFields
    (
        string $AClass      = null, /* Pusa controller class name for files upload */
        string $AMethod     = null, /* Controllers method for file upload */
        TPusaCore $ACommand = null
    )
    {
        return
        $this -> AddCommand
        ([
            'Arg',
            [
                'Fields'    => true,
                'Class'     => $AClass,
                'Method'    => $AMethod,
                'Command'   => empty( $ACommand ) ? [] : $ACommand -> GetCommands()
            ]
        ]);
    }



    /**************************************************************************
        Focus
    */

    /*
        Focus elements and objects
        Use TOperator filters from [ROOT]./core/operator.php
    */
    public function Focus
    (
        $ATarget    = self::CHILDS, /* self::WINDOW, self::SELF etc */
        $AFilter    = []            /* Filter for object searching [[ TOperator::EQUAL, '@id', 'MyID' ]] */
    )
    {
        return $this
        -> AddCommand
        ([
            'Focus',
            [
                'Target'    => $ATarget,
                'Filter'    => $AFilter
            ]
        ]);
    }



    /*
        Set to focus value of property focus object
    */
    public function FocusProperty
    (
        string $AProperty
    )
    {
        return $this
        -> AddCommand
        ([
            'Focus',
            [
                'Property'  => $AProperty
            ]
        ]);
    }



    /*
        Set focus to result of the method
    */
    public function FocusMethod
    (
        string $AMethod,
        array $AArguments = []
    )
    {
        return $this
        -> AddCommand
        ([
            'Focus',
            [
                'Method'    => $AMethod,
                'Arguments' => $AArguments
            ]
        ]);
    }



    /*
        Put the list the focus objects to stack
    */
    public function FocusPush()
    {
        return $this
        -> AddCommand
        ([
            'FocusPush', []
        ]);
    }



    /*
        Get the list of focus objects from stack
    */
    public function FocusPop
    (
        bool $ASkip = false /* For "true" the stack will be reduced, but focus will not change */
    )
    {
        return $this
        -> AddCommand
        ([
            'FocusPop', [ 'Skip' => $ASkip ]
        ]);
    }



    /*
        Focus is empty condition
    */
    public function IfFocusIsEmpty
    (
        TPusaCore   $ATrue  = null,
        TPusaCore   $AFalse = null
    )
    {
        return $this
        -> AddCommand
        ([
            'If',
            [
                'Condition' => 'FocusIsEmpty',
                'True'  => empty( $ATrue ) ? [] : $ATrue -> GetCommands(),
                'False'  => empty( $AFalse ) ? [] : $AFalse -> GetCommands()
            ]
        ]);
    }



    /**************************************************************************
        DOM manipulations
    */


    /*
       Creating new DOM element
    */
    public function DOMCreate
    (
        string      $ATagName   = 'div',        /* Tagname for new element */
        string      $APosition  = self::LAST    /* Position in parents for new element FIRST LAST BEFORE AFTER */
    )
    {
        return $this
        -> AddCommand
        ([
            'Create',
            [
                'Position'  => empty( $APosition ) ? self::LAST : $APosition,
                'TagName'   => empty( $ATagName ) ? 'div' : $ATagName
            ]
        ]);
    }



    /*
        Timer Event for DOM element
    */
    public function DOMTimer
    (
        string      $AClass,             /* Pusa class controller for call */
        string      $AMethod,            /* Pusa method in class */
        int         $ATimeoutMls = 0,    /* Timeot in milliseconds */
        TPusaCore   $ACommands   = null  /* Order list */
    )
    {
        return $this
        -> AddCommand
        ([
            'Timer',
            [
                'Class'     => $AClass,
                'Method'    => $AMethod,
                'Timeout'   => $ATimeoutMls,
                'Commands'  => empty( $ACommands ) ? [] : $ACommands -> GetCommands()
            ]
        ]);
    }



    /*
        Timer Event for DOM element without backend callback
    */
    public function DOMTimerFront
    (
        $APusa,                                 /* TPusaCore-based object with commands for execution */
        int     $TimeoutMilliseconds    = 0,    /* Timeot in milliseconds */
        string  $AID = null                     /* ID for timer */
    )
    {
        return $this
        -> AddCommand
        ([
            'Timer',
            [
                'ID'        => empty( $AID ) ? md5( json_encode( $APusa -> GetCommands() )) : $AID,
                'Commands'  => $APusa -> GetCommands(),
                'Timeout'   => $TimeoutMilliseconds
            ]
        ]);
    }



    /*
        Timer Event for DOM element
    */
    public function DOMTimerStop
    (
        string $AClass,                 /* Pusa class controller for call */
        string $AMethod                 /* Pusa method in class */
    )
    {
        return $this
        -> AddCommand
        ([
            'Timer',
            [
                'Class'     => $AClass,
                'Method'    => $AMethod,
                'Stop'      => true
            ]
        ]);
    }



    /*
        Replace an attrubutes content for focus DOM elements.
    */
    public function DOMReplace
    (
        $AValues
    )
    {
        return $this
        -> AddCommand
        ([
            self::REPLACE,
            [ 'Values' => $AValues ]
        ]);
    }



    /**************************************************************************
        CSS manipulations
    */

    /*
        Set CSS attributes by name
    */
    public function CSSAttr
    (
        string  $AName,     /* Name of CSS */
        array   $AValues    /* Attributes named array */
    )
    {
        return $this
        -> AddCommand
        ([
            'CSSAttr',
            [
                'Name' => $AName,
                'Values' => $AValues
            ]
        ]);
    }



    /**************************************************************************
        Pile operations
    */

    public function PilePut
    (
        $AValue     = '',           /* Value for pile */
        $AName      = 'default',    /* Key for pile */
        $AOperator  = self::SET     /* Operation TPusaCore::SET, TPusaCore::ADD */
    )
    {
        return $this
        -> AddCommand
        ([
            'PileFrom',
            [
                'Name'      => $AName,
                'Value'     => $AValue,
                'Operator'  => $AOperator
            ]
        ]);
    }



    /*
        Move focus propertyes in to pile
    */
    public function PileFromProperty
    (
        $AProperty = 'value',          /* Name of property o attribute on focus DOM object */
        $APileName = 'default',        /* Key name of Pile */
        $AOperator = self::SET         /* Operator TPusaCore::SET, TPusaCore::ADD */
    )
    {
        return $this
        -> AddCommand
        ([
            'PileFrom',
            [
                'Name'      => $APileName,
                'Property'  => $AProperty,
                'Operator'  => $AOperator
            ]
        ]);
    }




    /*
        Call a method with the Pile like an argument
    */
    public function PileFromMethod
    (
        $AMethod,                   /* Name of method on focus DOM object */
        $AArguments = [],           /* The arguments for method */
        $APileName  = 'default',    /* Key name of Pile */
        $AOperator  = self::SET     /* Operator TPusaCore::SET, TPusaCore::ADD */
    )
    {
        return $this
        -> AddCommand
        ([
            'PileFrom',
            [
                'Name'      => $APileName,
                'Method'    => $AMethod,
                'Arguments' => $AArguments,
                'Operator'  => $AOperator
            ]
        ]);
    }



    /*
        Move pile value to DOM propertyes
    */
    public function PileToProperty
    (
        $AProperty = 'value',      /* Name of property o attribute on focus DOM object */
        $APileName = 'default'     /* Key name of Pile */
    )
    {
        return $this
        -> AddCommand
        ([
            'PileTo',
            [
                'Name'      => $APileName,
                'Property'  => $AProperty
            ]
        ]);
    }



    /*
        Call a method with the Pile like an argument
    */
    public function PileToMethod
    (
        $AMethod,               /* Name of property o attribute on focus DOM object */
        $APileName = 'default'  /* Key name of Pile */
    )
    {
        return $this
        -> AddCommand
        ([
            'PileTo',
            [
                'Name'      => $APileName,
                'Method'    => $AMethod
            ]
        ]);
    }



    public function PileEqual
    (
        $AValue,
        TPusaCore $AEqual = null,
        TPusaCore $ANotEqual = null,
        string $AName  = 'default'
    )
    {
        return $this
        -> AddCommand
        ([
            'PileEqual',
            [
                'Name'      => $AName,
                'Value'     => $AValue,
                'Equal'     => empty( $AEqual ) ? null : $AEqual -> GetCommands(),
                'NotEqual'  => empty( $ANotEqual ) ? null : $ANotEqual -> GetCommands()
            ]
        ]);
    }



    /*
        Pile operation
    */
    public function PileReplace
    (
        $AFrom  = [],
        $ATo    = [],
        $AName  = 'default'
    )
    {
        return $this
        -> AddCommand
        ([
            self::PILE_REPLACE,
            [ 'Name' => $AName, 'From' => $AFrom, 'To' => $ATo ]
        ]);
    }



    /*
        Set state
    */
    public function SetState
    (
        array $AValues
    )
    {
        $Reflect = new \ReflectionClass( $this );
        return $this
        -> AddCommand
        ([
            self::SET_STATE,
            [
                'Class'     => $Reflect -> getShortName(),
                'ID'        => $this -> GetIDOrClass(),
                'Values'    => $AValues
            ]
        ]);
    }



    /*
        Read state from FrontEnd and will retur like parameters
    */
    public function ReadState
    (
        string $AGroup = null
    )
    {
        return $this;
/*
        -> AddCommand
        ([
            self::READ_STATE,
            [ 'Group' => $this -> GetStateGroup( $AGroup )]
        ]);
*/
    }



    /*
        Set ID
    */
    public function SetID
    (
        string $AValue = null
    )
    {
        $this -> ID = $AValue;
        return $this;
    }



    /*
        Get ID or class
    */
    public function GetID()
    {
        return $this -> ID;
    }



    /*
        Get ID or class
    */
    public function GetIDOrClass()
    {
        if( empty( $this -> ID ))
        {
            $Reflect = new \ReflectionClass( $this );
            $Result = $Reflect -> getShortName();
        }
        else
        {
            $Result = $this -> ID;
        }
        return $Result;
    }



    public function SetParams
    (
        array &$AValue
    )
    {
        $this -> Params = &$AValue;
        return $this;
    }
}

