<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

/*
    Debug system
    Catlair PHP

    Create: 2017
    Update:
    05.03.2019 - Add Dump
    23.08.2019 - Add colord

    still@itserv.ru

    Example:
    $Log = TLog::Create()
    -> Start(true)
    -> Begin()
    -> Info()
    -> Text('Hello world')
    -> Param('Param','Value')
    -> End()
    -> Stop();
*/

namespace catlair;

require_once( 'result.php' );
require_once( 'console.php' );

/* Define STDOUT const */
if( !defined( 'STDOUT' )) define( 'STDOUT', fopen( 'php://stdout', 'wb' ));

class TLog extends TResult
{
    const FILE_RIGHT = 0770;

    /* Message type */
    const BEG = '>';    /* Begin job */
    const END = '<';    /* End job */
    const TRS = '~';    /* Information line */
    const INF = 'I';    /* Information line */
    const ERR = 'E';    /* Critical error line */
    const WAR = 'W';    /* Warning line */
    const DEB = '#';    /* Debug line */
    const DLG = '?';    /* Dialog */
    const LIN = '─';    /* Dialog */

    /* Escape inc colors */
    const ESC_INK_DEFAULT       = "\e[39m";
    const ESC_INK_BLACK         = "\e[30m";
    const ESC_INK_BROWN         = "\e[31m";
    const ESC_INK_GREEN         = "\e[32m";
    const ESC_INK_GOLD          = "\e[33m";
    const ESC_INK_BLUE          = "\e[34m";
    const ESC_INK_PURPUR        = "\e[35m";
    const ESC_INK_CYAN          = "\e[36m";
    const ESC_INK_SILVER        = "\e[37m";
    const ESC_INK_GREY          = "\e[90m";
    const ESC_INK_RED           = "\e[91m";
    const ESC_INK_LIME          = "\e[92m";
    const ESC_INK_YELLOW        = "\e[93m";
    const ESC_INK_SKY           = "\e[94m";
    const ESC_INK_MAGENTA       = "\e[95m";
    const ESC_INK_AQUA          = "\e[96m";
    const ESC_INK_WHITE         = "\e[97m";

    const ESC_PAP_DEFAULT       = "\e[49m";
    const ESC_PAP_BLACK         = "\e[40m";
    const ESC_PAP_BROWN         = "\e[41m";
    const ESC_PAP_GREEN         = "\e[42m";
    const ESC_PAP_GOLD          = "\e[43m";
    const ESC_PAP_BLUE          = "\e[44m";
    const ESC_PAP_PURPUR        = "\e[45m";
    const ESC_PAP_CYAN          = "\e[46m";
    const ESC_PAP_SILVER        = "\e[47m";
    const ESC_PAP_GREY          = "\e[90m";
    const ESC_PAP_RED           = "\e[91m";
    const ESC_PAP_LIME          = "\e[92m";
    const ESC_PAP_YELLOW        = "\e[93m";
    const ESC_PAP_SKY           = "\e[94m";
    const ESC_PAP_MAGENTA       = "\e[95m";
    const ESC_PAP_AQUA          = "\e[96m";
    const ESC_PAP_WHITE         = "\e[97m";

    const COLOR_TRACE           = self :: ESC_INK_BLUE;
    const COLOR_INFO            = self :: ESC_INK_CYAN;
    const COLOR_ERROR           = self :: ESC_INK_RED;
    const COLOR_WARNING         = self :: ESC_INK_YELLOW;
    const COLOR_DEBUG           = self :: ESC_INK_AQUA;

    const COLOR_LABEL           = self :: ESC_INK_GREEN;
    const COLOR_TEXT            = self :: ESC_INK_DEFAULT;
    const COLOR_VALUE           = self :: ESC_INK_WHITE;
    const COLOR_TITLE           = self :: ESC_INK_GOLD;
    const COLOR_SYNTAXYS        = self :: ESC_INK_GREY;

    /* Log destination */
    const FILE                  = 'file';
    const CONSOLE               = 'console';

    const CONTINUE              = 'a';
    const CLEAR                 = 'w';

    const LINE                  = '───────────────────────────────────────────────────────────────────────────────';

    /* Private declaration */
    private $MomentLast         = 0;                /* Moment begin for last message */
    private $MomentCurrent      = 0;                /* Moment begin for current message */
    private $InLine             = false;            /* Debugger have begun new line */
    private $CurrentTrace       = null;             /* Current trace information */
    private $CurrentTraceLabel  = '';               /* Trace label for current line */
    private $CurrentString      = '';               /* Full log string message for write to destination with all charaters */
    private $PureString         = '';               /* String message for without escape and control characters */
    private $Stack              = [];               /* Stack for jobs is controlled by Begin and End.*/
    private $Trap               = false;            /* Trap mode */
    private $TrapBuffer         = '';               /* Trap buffer */
    private $TraceResult        = [];
    private $LineType           = '';              /* Type of current line */

    private $LastTrace          = null;
    private $LastDebug          = null;
    private $LastError          = null;
    private $LastWarning        = null;
    private $LastInfo           = null;

    private $OldErrorHandle     = null;         /* Handel for PHP error event */
    private $Handle             = -1;

    /* Public declarations */
    public $Colored             = true;             /* Color out enable or disable */
    public $Destination         = self::CONSOLE;    /* Destination for log TLog::CONSOLE, TLog:FILE  */
    public $OpenType            = self::CLEAR;      /* */
    public $TimeWarning         = 500.0;            /* Line highlight when timeout more value*/
    private $Enabled            = true;             /* Enable or disable log */
    public $Job                 = true;             /* Enable/disbale job line */
    public $Debug               = true;             /* Enable/disable debug messages */
    public $Info                = true;             /* Enable/disable info messeges */
    public $Error               = true;             /* Enable/disable error messages*/
    public $Warning             = true;             /* Enable/disable warning messages */
    public $Path                = '';               /* Path for log file */
    public $File                = '';               /* File name for log file */
    private $CurrentType        = null;
    public $Header              = true;             /* Show header with moment, trace information, type and depth */
    public $CurrentHeader       = true;             /* Current header status */
    public $ShowMoment          = true;             /* Begin line moment out to log */
    public $Trace               = true;             /* */
    public $ShowType            = true;             /* */
    public $ShowDepth           = true;             /* */

    /* Statistic */
    private $TraceCount         = 0;
    private $ErrorCount         = 0;
    private $WarningCount       = 0;
    private $InfoCount          = 0;
    private $DebugCount         = 0;

    private $Table              = [];
    private $Column             = 0;



    function __construct()
    {
        $this -> SetOk();
    }



    static function Create()
    {
        return new TLog();
    }



    public function SetEnabled
    (
        bool $AValue = true
    )
    {
        $this -> Enabled = $AValue;
        return $this;
    }



    public function GetEnabled()
    {
        return  $this -> Enabled;
    }



    public function GetFilePath()
    {
        return $this -> Path . '/' . $this -> File;
    }



    /*
        Return file handle
    */
    private function GetHandle()
    {
        return $this -> Handle;
    }



    /*
        Set hadle for current log
    */
    private function &SetHandle( $AValue )
    {
        $this -> Handle = $AValue;
        return $this;
    }



    /*
        Write current $AString:string to destination
    */
    public function &Write( $AString )
    {
        if( $this->Enabled )
        {
            if ( $this -> GetHandle() == -1 && $this -> Destination == self::FILE )
            {
                $File = $this -> GetFilePath();
                if ( $File != '' && CheckPath( dirname( $File )) )
                {
                    $this -> SetHandle( fopen( $File, $this -> OpenType ));
                }
                else
                {
                    $this -> SetHandle( -1 );
                }
            }

            if ( $this -> GetHandle() == -1 )
            {
                /*Write to console*/
                fwrite( STDOUT, $AString );
            }
            else
            {
                /*Write to file*/
                fwrite( $this -> GetHandle(), $AString );
            }
        }
        return $this;
    }



    /*
        Text $AString:string is outed to current line
    */
    private function &Store( $AString, $APure )
    {
        $this -> CurrentString .= $AString;
        if ($APure) $this -> PureString .= $AString;
        return $this;
    }




    /*
        Out trace information
    */
    public function &TraceTotal()
    {
        if( $this -> Enabled )
        {
            $this
            -> Line( 'Trace total' )
            -> SetTable
            ([
                [ 'Color' => TLog :: COLOR_LABEL, 'Length' => 40, 'Pad' => ' ', 'Align' => STR_PAD_RIGHT ],
                [ 'Color' => TLog :: COLOR_VALUE, 'Length' => 12, 'Pad' => ' ', 'Align' => STR_PAD_LEFT ],
                [ 'Color' => TLog :: COLOR_VALUE, 'Length' => 10, 'Pad' => ' ', 'Align' => STR_PAD_LEFT ]
            ]);
            $this -> Trace()
            -> Cell( 'Call', self :: COLOR_TITLE )
            -> Cell( 'Time ms',  self :: COLOR_TITLE )
            -> Cell( 'Count', self :: COLOR_TITLE );

            /* Trace information sorting */
            uasort
            (
                $this -> TraceResult,
                function($a, $b)
                {
                    if( $a[ 'Delta' ] > $b[ 'Delta' ]) return -1;
                    elseif( $a[ 'Delta' ] < $b[ 'Delta' ]) return 1;
                    else return 0;
                }
            );

            foreach ($this -> TraceResult as $Key => $Value)
            {
                $this
                -> Trace( )
                -> Cell( $Key )
                -> Cell( number_format( $Value['Delta'] * 1000, 2, '.', ' ' ))
                -> Cell( $Value['Count']);
            }
            $this
            -> Line( '' );
        }
        return $this;
    }



    /*
        Begin of new line with type $AType form self::TYPE
    */
    public function &LineBegin( $AType )
    {
        if( $this -> Enabled )
        {
            /* If line is begined then close the line */
            if( $this->InLine ) $this -> LineEnd();

            $this -> Column         = 0;
            $this -> CurrentType    = $AType;
            $this -> CurrentString  = '';
            $this -> PureString     = '';

            /* Get trace information for line */
            $this -> CurrentTrace = debug_backtrace( DEBUG_BACKTRACE_PROVIDE_OBJECT );

            $this -> CurrentTraceLine = count( $this -> CurrentTrace ) > 2
            ? $this -> CurrentTrace[2]
            : $this -> CurrentTrace[1];

            /*Begin new line*/
            $this -> InLine = true;
            $this -> LineType = $AType;
            $this -> MomentCurrent = microtime(true);

            /* Push begin in to stack */
            if ( $AType == self::BEG )
            {
                /* Store trace information */
                if( empty( $this -> CurrentTraceLabel ))
                {
                    $this -> CurrentTraceLabel = $this -> CurrentTraceLine[ 'function' ];
                }

                /* Store trace information */
                array_push
                (
                    $this -> Stack,
                    [
                        'Moment'        => $this -> MomentCurrent,
                        'TraceLabel'    => $this -> CurrentTraceLabel
                    ]
                );
            }

            /* Check log enabled and other settings */
            if
            (
                $AType == self :: LIN && $this -> Info ||
                $AType == self :: ERR && $this -> Error ||
                $AType == self :: DEB && $this -> Debug ||
                $AType == self :: WAR && $this -> Warning ||
                $AType == self :: INF && $this -> Info ||
                $AType == self :: TRS && $this -> Job ||
                $AType == self :: BEG && $this -> Job ||
                $AType == self :: END && $this -> Job
            )
            {
                /* Calculate delta */
                $Delta = $this -> MomentLast == 0 ? 0 : ( $this -> MomentCurrent - $this -> MomentLast ) * 1000;

                if( $this -> CurrentHeader )
                {
                    $Tab = 60;

                    /* Moment line */
                    if( $this -> ShowMoment)
                    {
                        $this -> Color(self::ESC_INK_SILVER) -> Text(date('Y-m-d H:i:s'));
                        $Tab += 20;
                    }

                    /* Trace information */
                    if( $this -> Trace )
                    {
                        /* Interline timeout */
                        $this -> Text
                        (
                            str_pad
                            (
                                (string) ( number_format($Delta, 2, '.', ' ') ),
                                9,
                                ' ',
                                STR_PAD_LEFT
                            ),
                            $Delta > $this -> TimeWarning ? self::ESC_INK_RED : self::ESC_INK_GREY
                        )
                        /* Trace information */
                        -> Color(self::ESC_INK_SILVER)
                        -> Text(str_pad((string) $this -> CurrentTrace[1]['line'], 6, ' ', STR_PAD_LEFT). ' ')
                        -> Color(self::ESC_INK_BLUE)
                        -> Text(basename($this->CurrentTrace[1]['file']).' ')
                        -> Color( self :: COLOR_LABEL )
                        -> Text($this->CurrentTraceLine['function'])
                        /* Tabulate */
                        -> Color(self::COLOR_SYNTAXYS)
                        -> Tab( $Tab )
                        -> Color(self::ESC_INK_DEFAULT);
                    }

                    /* Type information */
                    if( $this -> ShowType )
                    {
                        /* Color */
                        switch ( $this -> LineType )
                        {
                            default:
                            case self::DEB: $Color = self :: COLOR_DEBUG; break;
                            case self::TRS: $Color = self :: COLOR_TRACE; break;
                            case self::INF: $Color = self :: COLOR_INFO; break;
                            case self::WAR: $Color = self :: COLOR_WARNING; break;
                            case self::ERR: $Color = self :: COLOR_ERROR; break;
                            case self::LIN: $Color = self :: ESC_INK_SILVER; break;
                            case self::BEG: $Color = self :: ESC_INK_GOLD; break;
                            case self::END: $Color = self :: ESC_INK_GOLD; break;
                            case self::DLG: $Color = self :: ESC_INK_BLUE; break;
                        }
                        $this -> Text( ' ' . $AType . ' ', $Color );
                    }

                    /* Timing display */
                    if( $this -> ShowDepth )
                    {
                        /* Depth shift */
                        $Depth = count( $this -> Stack );
                        if( $this -> LineType == self :: BEG || $this -> LineType == self :: END ) $Depth -- ;
                        if( $Depth > 0 )
                        {
                            $ps = $this -> LineType == self :: BEG ? '├─' : '│ ';
                            $this -> Text( str_repeat( '│ ', $Depth - 1 ) . $ps, self::COLOR_SYNTAXYS );
                        }
                    }


                    /* Output trace label */
                    if ( $AType == self::BEG )
                    {
                        $this -> Text( $this -> CurrentTraceLabel,  self :: COLOR_TITLE );
                    }

                }
            }
        }

        return $this;
    }



    /*
        Close current line
    */
    public function &LineEnd()
    {
        if( $this->Enabled )
        {
            if ( $this -> InLine )
            {
                $this -> MomentLast = $this -> MomentCurrent;
                /* Write End of line */
                $this -> EOL();
                $this -> InLine = false;
            }

            switch ( $this -> CurrentType )
            {
                case self::DEB: $this -> LastDebug      = $this -> CurrentString; break;
                case self::INF: $this -> LastInfo       = $this -> CurrentString; break;
                case self::WAR: $this -> LastWarning    = $this -> CurrentString; break;
                case self::ERR: $this -> LastError      = $this -> CurrentString; break;
            }

            /* Write to file */
            $this -> Flush();
        }
        return $this;
    }



    public function &Flush()
    {
        if ( $this -> Trap )
        {
            $this -> TrapBuffer .= $this -> CurrentString;
        }
        else
        {
            $this -> Write( $this -> CurrentString );
        }
        $this -> CurrentString = '';
        return $this;
    }



    /*
        Start debug with $AEnabled:boolean
    */
    public function &Start()
    {
        /* reset job stack */
        $this -> Stack          = [];
        /* reset trace array */
        $this -> TraceArray     = [];
        /* reset statistic */
        $this -> ErrorCount     = 0;
        $this -> WarningCount   = 0;
        $this -> InfoCount      = 0;
        $this -> DebugCount     = 0;
        /* write last moment */
        $this -> CurrentLast   = microtime( true );
        return $this;
    }



    /*
        Stop debug
    */
    public function &Stop()
    {
        $this -> LineEnd();

        /* Close all files if it was opened. */
        if ( $this -> Handle != -1) fclose( $this -> Handle );

        return $this;
    }



    public function &GetLastWarning()
    {
        return $this -> LastWarning;
    }



    public function &GetLastError()
    {
        return $this -> LastError;
    }



    /*
        Dump to log the last error and warnig messeges
    */
    public function &LastMessages( $ALog = null )
    {
        if( empty( $ALog )) $ALog = $this;
        if ( $this->Enabled )
        {
            /* Last messages */
            if( $ALog -> LastWarning != null || $ALog -> LastError != null )
            {
                $this
                -> Line( 'Last warning and error' ) -> LineEnd()
                -> Text( $ALog -> LastWarning ) -> LineEnd()
                -> Text( $ALog -> LastError ) -> LineEnd();
            }
        }
        return $this;
    }



    public function GetStatisticString()
    {
        return
        self :: COLOR_ERROR .       $this -> ErrorCount . ' ' .
        self :: COLOR_WARNING .     $this -> WarningCount . ' ' .
        self :: COLOR_DEBUG .       $this -> DebugCount . ' ' .
        self :: COLOR_INFO .        $this -> InfoCount . ' ' .
        self :: COLOR_TRACE .       $this -> TraceCount . ' ' .
        self :: ESC_INK_DEFAULT;
    }



    public function &StatisticOut( $ALog = null )
    {
        if( empty( $ALog )) $ALog = $this;
        if( $this -> Enabled ) $this -> ParamLine( 'Events count', $ALog -> GetStatisticString() );
        return $this;
    }



    public function &Cell( $AString, $AColor = null, $ALength = null, $APad = null, $AAlign = null )
    {
        if( array_key_exists( $this -> Column, $this -> Table ))
        {
            $Column     = $this -> Table[ $this -> Column ];
            $AColor     = !empty( $AColor )     ? $AColor   : ( array_key_exists( 'Color', $Column )    ? $Column[ 'Color' ]    : null );
            $ALength    = !empty( $ALength )    ? $ALength  : ( array_key_exists( 'Length', $Column )   ? $Column[ 'Length' ]   : null );
            $APad       = !empty( $APad )       ? $APad     : ( array_key_exists( 'Pad', $Column )      ? $Column[ 'Pad' ]      : null );
            $AAlign     = !empty( $AAlign )     ? $AAlign   : ( array_key_exists( 'Align', $Column )    ? $Column[ 'Align' ]    : null );
        }

        $this
        -> Text( $this -> Column == 0 ? '' : '│', self::COLOR_SYNTAXYS )
        -> Text( $AString, $AColor, $ALength, $APad, $AAlign );

        $this -> Column ++;
        return $this;
    }



    /*
        Text $AString:string is outed to log
    */
    public function &Text
    (
        $AString,
        $AColor     = null,
        $AMaxLength = null,
        $APad       = ' ',
        $AAlign     = STR_PAD_RIGHT
    )
    {
        if ( $this -> Enabled && ( !empty( $AString ) || !empty( $AMaxLength )) )
        {
            if( $AColor != null ) $this -> Color($AColor);
            if( $AMaxLength != null ) $AString = str_pad( $AString, $AMaxLength, $APad, $AAlign );
            $this -> Store( (string) $AString, true );
            if ( $AColor != null ) $this -> Color(self::ESC_INK_DEFAULT);
        }
        return $this;
    }



    /*
        Color $AColor:self::ESC_* is set for next output
        $AColor - escape sequence from constant self::ESC_*
    */
    public function &Color( $AColor )
    {
        if ( $this -> Enabled && $this -> Colored ) $this -> Store( $AColor, false );
        return $this;
    }



    /*
        Set tabulate to $APosition:integer for current line
    */
    public function &Tab( $APosition )
    {
        if ( $this -> Enabled )
        {
            $l = strlen( $this->PureString );
            if( $l<$APosition ) $this -> Text(str_repeat( ' ', $APosition-$l ));
        }
        return $this;
    }



    /*
        write $AValue to current line
    */
    public function &Value
    (
        $AValue,
        $AColor = self :: COLOR_VALUE,
        $AType = true
    )
    {
        if ( $this -> Enabled )
        {
            $Type = gettype($AValue);
            switch ($Type)
            {
                case 'string':
                    $AValue = preg_replace('/\n/', ' ', $AValue);
                    /* $AValue = preg_replace('/\s\s+/', ' ', $AValue); */
                    $l = strlen( $AValue );
                    $Value =  $l > 1024 ? substr( $AValue, 0, 1024 ) . '...' . $l : $AValue;
                    $Type = 'string';
                break;
                case 'object':
                case 'array':
                    $Value = json_encode( $AValue );
                break;
                case 'boolean':
                    if ( $AValue ) $Value = 'true';
                    else $Value = 'false';
                    $Type = 'boolean';
                break;
                case 'integer':
                    $Value = sprintf( '%0d', $AValue );
                    $Type='integer';
                break;
                case 'double':
                    $Value = sprintf( '%0.f', $AValue );
                    $Type='double';
                break;
                case 'RESOURCE':
                    $Value = NULL;
                break;
                case 'NULL':
                    $Value = NULL;
                break;
                default:
                    $Value = 'UNKNOWN';
                break;
            }
            $this -> Text( $Value, $AColor );
            if( $AType ) $this -> Text( ' ' . $Type, self :: COLOR_SYNTAXYS );
        }
        return $this;
    }



    /*
        Out parameter with $AName:string and $AValue:any
        to current line for result [Name = type:Value]
    */
    public function &Param( $AName, $AValue )
    {
        if ( $this -> Enabled )
        {
            $this
            -> Color( self::COLOR_SYNTAXYS )
            -> Text('[')
            -> Color( self :: COLOR_LABEL )
            -> Text($AName)
            -> Color( self::COLOR_SYNTAXYS )
            -> Text(' = ')
            -> Value($AValue)
            -> Color( self::COLOR_SYNTAXYS )
            -> Text(']')
            -> Color( self::ESC_INK_DEFAULT );
        }
        return $this;
    }



    /*
        Out parameter with $AName:string and $AValue:any
        to current line for result [Name = type:Value]
    */
    public function &Key( $AName, $AValue, $AMaxLength = 30 )
    {
        if( $this -> Enabled )
        {
            $this
            -> Text( $AName, self :: COLOR_LABEL, $AMaxLength, ' ', STR_PAD_RIGHT )
            -> Value( $AValue );
        }
        return $this;
    }



    /*
        Out parameter with $AName:string and $AValue:any
        to current line for result [Name = type:Value]
    */
    public function &ParamLine( $AName, $AValue, $AMaxLength = 30, $AColor = self :: COLOR_VALUE )
    {
        if ( $this -> Enabled )
        {
            $this
            -> Trace( '' )
            -> Text( $AName, self :: COLOR_LABEL, $AMaxLength, ' ', STR_PAD_LEFT )
            -> Text( ': ', self :: COLOR_SYNTAXYS )
            -> Value( $AValue, $AColor, false );
        }
        return $this;
    }



    /*
        Set trace label from $ALabel
    */
    public function &Label($ALabel)
    {
        $this -> CurrentTraceLabel=$ALabel;
        return $this;
    }



    /*
        Job begin for new line
    */
    public function &Begin( $AText = null )
    {
        if( $this -> Enabled )
        {
            $this -> CurrentTraceLabel = $AText;
            $this -> LineBegin( self::BEG );
        }
        return $this;
    }



    /* Job end */
    public function &End( $AText = null )
    {
        if ( $this -> Enabled )
        {
            $this -> LineBegin( self::END ) -> Text( '└─', self::COLOR_SYNTAXYS );

            if ( count( $this -> Stack ) > 0) $StackRecord = array_pop( $this -> Stack );
            else $StackRecord = null;

            if ( $StackRecord != null )
            {
                /* Calculate delta from job begin */
                $TraceDelta = $this -> MomentCurrent - $StackRecord[ 'Moment' ];
                $TraceLabel = $StackRecord[ 'TraceLabel' ];

                $this -> Text
                (
                    (string)(number_format($TraceDelta*1000, 2,'.',' ')).'ms ',
                    self::ESC_INK_GREY
                );

                if ( array_key_exists( $TraceLabel, $this->TraceResult ))
                {
                    $this -> TraceResult[ $TraceLabel ][ 'Delta' ] += $TraceDelta;
                    $this -> TraceResult[ $TraceLabel ][ 'Count' ] ++;
                }
                else
                {
                    $this -> TraceResult[ $TraceLabel ][ 'Delta' ] = $TraceDelta;
                    $this -> TraceResult[ $TraceLabel ][ 'Count' ] = 1;
                }
            }
            else
            {
                $this -> Text(' Tracert heracly error ', self::ESC_INK_RED );
                $TraceDelta = 0;
            }

            if ( $AText != null ) $this -> Text( $AText, self::ESC_INK_SILVER );
        }
        return $this;
    }



    /*
        Wait user input
    */
    public function &Dialog( $ADefault )
    {
        $Input = '';

        if ( $this -> Enabled )
        {
            if ($this->InLine)
            {
                $this->MomentLast = $this->MomentCurrent;
                $this->Text('>');
                $this->InLine = false;
            }
            /* Write to file*/
            $this -> Write( $this -> CurrentString );
            $Input = readline();
        }

        if ( $Input == '' ) $Input=$ADefault;

        return $Input;
    }



    /*
        Waiting input confirmation
    */
    public function &Confirm($AMessage, $AYes)
    {
        if ( $this -> Enabled )
        {
            $this
            -> Color( self::ESC_INK_SKY )
            -> Text($AMessage)
            -> Text(' (')
            -> Color( TLog::ESC_INK_AQUA )
            -> Text( $AYes )
            -> Color(self::ESC_INK_SKY)
            -> text(')')
            -> Color(self::ESC_INK_DEFAULT)
            -> End();
            $Input = readline();
            if ($Input != $AYes) $this->Code = 'RC_NOT_CONFIRM';
        }
        return $this;
    }



    /* New information line */
    public function &Trace( $AText = null )
    {
        if ( $this -> Enabled )
        {
            $this -> LineBegin( self::TRS );
            $this -> TraceCount++;
            if ( $AText !=null ) $this -> Text( $AText, self::COLOR_TEXT );
        }
        return $this;
    }


    /*
        New debug line
    */
    public function Debug( $AText = null )
    {
        if( $this -> Enabled )
        {
            $this -> LineBegin( self::DEB ) -> Text( $AText, self :: COLOR_DEBUG );
            $this -> DebugCount++;
        }
        return $this;
    }



    /* New information line */
    public function &Info( $AText = null )
    {
        if ( $this -> Enabled )
        {
            $this -> LineBegin( self::INF );
            $this -> InfoCount++;
            if ( $AText !=null ) $this -> Text( $AText, self::COLOR_TEXT );
        }
        return $this;
    }



    /* New warning line */
    public function &Warning( $AText = null )
    {
        if ( $this -> Enabled )
        {
            if ( $this -> Trap ) $this -> TrapDump();
            $this -> LineBegin(self::WAR);
            $this -> WarningCount++;
            if ( $AText !=null ) $this -> Text( $AText, self :: COLOR_WARNING );
        }
        return $this;
    }



    /*
        New error line
    */
    public function &Error( $AText = null )
    {
        if ( $this -> Enabled )
        {
            if ( $this -> Trap ) $this -> TrapDump();
            $this -> LineBegin( self::ERR );
            $this -> ErrorCount++;
            if ( $AText != null ) $this -> Text( $AText, self :: COLOR_ERROR );
        }
        return $this;
    }



    /* New dump line */
    public function &Line( $ALabel = '', $AColor = null )
    {
        if ( $this -> Enabled )
        {
            $ALabel = empty( $ALabel ) ? '' : ' ' . $ALabel . ' ';

            $this
            -> LineBegin( self :: LIN )
            -> Text( '──' . $ALabel . str_repeat( '─', 77 - mb_strlen( $ALabel )), $AColor )
            -> LineEnd();
        }
        return $this;
    }



    public function &Separator( $ACaption, $AColor = null )
    {
        return $this
        -> HeaderHide()
        -> Line( $ACaption, $AColor )
        -> HeaderRestore();
    }



    /* New dump line */
    public function &Print( $AValue, $ALabel = '' )
    {
        if ( $this -> Enabled )
        {
            $l = 77 - mb_strlen( $ALabel );
            /* Store Header for resotre in the ond of funciton */
            $this
            -> HeaderHide   ()
            -> Info         ()
            -> Color        ( self::ESC_INK_MAGENTA )
            -> Text         ( '──' . $ALabel . str_repeat( '─', $l ))
            -> Info         ()
            -> Text         ( print_r( $AValue, true ))
            -> EOL          ()
            -> Text         ( self::LINE )
            -> Color        ( self::ESC_INK_DEFAULT )
            -> HeaderRestore();
        }
        return $this;
    }



    /* New dump line */
    public function &Dump($AValue, $AText = 'Dump' )
    {
        if ( $this -> Enabled )
        {
            if ($AText == null) $AText = '';
            $Type = gettype($AValue);
            switch ($Type)
            {
                case 'NULL':
                case 'RESOURCE':
                case 'double':
                case 'integer':
                case 'boolean':
                case 'string': $this -> Trace() -> Param ($AText, $AValue); break;
                case 'array':
                case 'object':
                    $this->Begin( $AText );
                    foreach ($AValue as $Key => $Value)
                    {
                        if ($Value !== $AValue) $this -> Dump( $Value, $Key );
                    }
                    $this->End();
                break;
            }
        }
        return $this;
    }



    /* New error line */
    public function &EOL()
    {
        $this->Text( PHP_EOL );
        return $this;
    }



    public function &SetDestination( $ADestination )
    {
        $this -> Destination = $ADestination;
        return $this;
    }



    public function GetDestination()
    {
        return $this->Destination;
    }



    /*
        Files and pathes
    */

    /* Set log file from $AFile */
    public function &SetLogFile( $AFile )
    {
        if( empty( $AFile ))
        {
            $AFile='index';
        }
        else
        {
            if( strlen( $AFile ) > 250 )
            {
                $AFile = md5( $AFile );
            }
            else
            {
                $AFile = clFileControl( $AFile );
            }
        }
        /* Write propery */
        $this -> File = $AFile . '.log';
        return $this;
    }



    /*
        Get file
    */
    public function GetLogFile()
    {
        return $this->File;
    }



    /*
        Set file
    */
    public function &SetLogPath( $APath )
    {
        $this -> Path = $APath;
        return $this;
    }



    /* Get file */
    public function GetLogPath()
    {
        return $this -> Path;
    }



    /*
        Trap. Log in trap saction not output, but storeging in to trap puffer.
        in case in trap section arise the error or warning, all trap buffer will be dump in log.
    */



    /*
        Begin trap section.
    */
    public function &TrapBegin()
    {
        $this -> LineEnd();
        $this -> Trap = true;
        return $this;
    }



    /*
        Trap dump and end
    */
    public function &TrapDump()
    {
        if ( $this -> Trap )
        {
            $this
            -> Write( $this -> TrapBuffer )
            -> TrapEnd();
        }
        return $this;
    }



    /*
        End of trap section
    */
    public function &TrapEnd()
    {
        $this -> LineEnd();
        $this -> TrapBuffer = '';
        $this -> Trap = false;
        return $this;
    }



    /*
        Get show depth
    */
    public function &HeaderShow()
    {
        $this -> Header = $this -> CurrentHeader;
        $this -> CurrentHeader = true;
        return $this;
    }



    /*
        Set file
    */
    public function &HeaderHide()
    {
        $this -> Header = $this -> CurrentHeader;
        $this -> CurrentHeader = false;
        return $this;
    }



    /*
        Restore header
    */
    public function &HeaderRestore()
    {
        $this -> CurrentHeader = $this -> Header;
        return $this;
    }



    /*
        Setter for the header
    */
    public function &SetHeader( $AValue )
    {
        $this -> Header = $AValue;
        $this -> CurrentHeader = $AValue;
        return $this;
    }



    /*
        Getter for the current header
    */
    public function GetGurrentHeader()
    {
        return $this -> CurrentHeader;
    }



    /*
        Getter for the header
    */
    public function GetHeader()
    {
        return $this -> Header;
    }



    /* Show trace information for each line */
    public function &TraceShow()
    {
        $this -> Trace = true;
        return $this;
    }



    /*
        Hide trace information for each line
    */
    public function &TraceHide()
    {
        $this -> Trace = false;
        return $this;
    }



    /*
        Enable output the log
    */
    public function &Enable()
    {
        $this -> Enabled = true;
        return $this;
    }



    /*
        Disable output the log
    */
    public function &Disable()
    {
        $this -> Disabled = false;
        return $this;
    }



    /*
        Catch all PHP errors and put them in to log
    */
    public function &CatchErrors()
    {
        if ( $this -> OldErrorHandle == null )
        {
            $this -> OldErrorHandle = set_error_handler
            (
                function ( $errno, $errstr, $errfile, $errline )
                {
                    $this
                    -> Error( $errstr )
                    -> TraceDump();
                }
            );
        }

        return $this;
    }



    public function &TraceDump()
    {
        $Debug = debug_backtrace ();
        $this -> Begin();
        foreach( $Debug as $Record )
        {
            $this -> Trace()
            -> Text( $Record[ 'function' ])
            -> Text(' ')
            -> Text( clValueFromObject( $Record, 'line' ), self :: ESC_INK_LIME )
            -> Text(' ')
            -> Text( dirname( clValueFromObject( $Record, 'file' ) ) . '/' )
            -> Text( basename( clValueFromObject( $Record, 'file' ) ), self :: ESC_INK_GREEN );
        }
        $this -> End();
        return $this;
    }



    public function Reset()
    {
        $Handle = $this -> GetHandle();
        if ( $Handle != -1 )
        {
            fclose( $Handle );
            $this -> SetHandle( -1 );
        }
        return $this;
    }



    public function SetTable( $AValue )
    {
        $this -> Table = $AValue;
        return $this;
    }



    public function SetExecute( $AValue )
    {
        $this -> Execute = $AValue;
        return $this;
    }



    public function GetExecute()
    {
        return $this -> Execute;
    }



    public function SetShowMoment( $AValue )
    {
        $this -> ShowMoment = $AValue;
        return $this;
    }



    public function FilePathToStd()
    {
        print_r( 'Log file:' . $this -> GetFilePath() . PHP_EOL );
        return $this;
    }



    /*
        Test the value
    */
    public function Test
    (
        bool    $AValue,
        string  $ACondition = 'Test',
        string  $ATrue  = 'Sucess',
        string  $AFalse = 'Fail'
    )
    {
        if( $AValue )
        {
            $this -> Info( $ACondition ) -> Text( ' ' . $ATrue, TLog::COLOR_INFO );
        }
        else
        {
            $this -> Warning( $ACondition ) -> Text( ' ' . $AFalse, TLog::COLOR_WARNING );
        }
        return $this;
    }
}
