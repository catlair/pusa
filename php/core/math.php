<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

/*
    Math utilities
*/

namespace catlair;

define('EPSILON', 1e-9);



/*
    Interpolate value form $a to $b by time $t from 0 to 1
*/
function ItpLin( $a, $b, $t )
{
    return $a + ( $b - $a ) * $t;
}



function Norm( $a, $b, $v )
{
    return ($v - $a) / ( $b - $a);
}



function MathCompare ($a, $b)
{
    if (abs($a-$b) < EPSILON) return 0;
    if ($a < $b) return 1;
    return 2; /* $a > $b */
}



function MathEqual ($a, $b)
{
    return MathCompare($a, $b) == 0;
}
function MathLess ($a, $b)
{
    return MathCompare($a, $b) == 1;
}
function MathLessEqual ($a, $b)
{
    return MathCompare($a, $b) == 1 || MathCompare($a, $b) == 0;
}



function MathMore ($a, $b)
{
    return MathCompare($a, $b) == 2;
}



function MathMoreEqual ($a, $b)
{
    return MathCompare($a, $b) == 2 || MathCompare($a, $b) == 0;
}
