<?php
/**
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.


    Catlair.
    Default update object. This is parent class for any TUpdate_ID classes
    It class is using by TInstaller from installer.php.

    13.10.2019 still@itserv.ru
*/

namespace catlair;

class TUpdate extends TResult
{
    public $Caption;
    public $Path;
    public $Log;
    public $Builder;
    public $Params = null;



    /*
        $AID - unique identifer on time masq
        $APath - path with content
    */
    function __construct($AID, $APath, $ALog)
    {
        $this->ID       = $AID;
        $this->Path     = $APath;
        $this->Log      = $ALog;
        $this->Builder  = $ALog;
        $this->Code     = rcUnknown;
        $this->Message  = '';
    }



    /**
        Return income parameter $AName:string value or $ADefault:string
        It is coplex function return income params:
        - form get & post for CGI interface
        - command line or console inpur for CLI interface
    */
    public function GetIncome($AName, $ACaption, $ADefault)
    {
        $r = GetIncomeCLI( $AName, $ACaption, $ADefault, true );
        return $r;
    }



    /*
        Load file content from AFileName
    */
    public function ContentLoad($AFileName)
    {
        $Content = false;
        $FileName = $this->Path.'/'.$AFileNane;
        if (!file_exists($FileName)) $this->Code = 'UpdateFileContentNotExists';
        {
            $Content = file_get_contents($FileName);
            if ($Content === false) $this->Code = 'UpdateFileContentNotRead';
        }
        return $Content;
    }



    private function ContentFile($AIdentify)
    {
        $File = $this->Path .'/' . md5($AIdentify) . 'back';
    }



    /**
        Strore file for uninstall
    */
    public function ContentStore($AIdentify, $AContent)
    {
        $File = $this->ContentFile($AIdentify);
        if (file_put_contents($File, $AContent) === false)
        {
            $this->Code='StoreContentWriteError';
            $this->Message=$File;
        }
        return $this;
    }



    /**
        Strore file for uninstall
    */
    public function ContentRestore($AIdentify)
    {
        $Result = false;
        $File = $this->ContentFile($AIdentify);
        if (file_exists($File))
        {
            $Result = file_get_contents($File, $AContent);
            if ($Result === false)
            {
                $this->Code='RestoreContentReadError';
                $this->Message=$File;
            }
        }
        else
        {
            $this->Code='RestoreContentNotFound';
            $this->Message=$File;
        }
        return $Result;
    }



    /**
        Call controller
    */
    public function &CallController($ACall)
    {
        $this->Log->Begin();
        $r=[];
        $this->Builder->LoadController($ACall, $r);
        if ($r['Error']=='')
        {
            $Controller = new $r['Class']($this->Log);
            $this->Log->Begin($ACall);
            call_user_func(array($Controller, $r['Method']));
            $this->Log->End();
            $this->Builder->SetContent($Controller->End());
            $this->Code = $Controller->GetCode();
            unset($Controller);
        }
        else
        {
            $this->Builder->SetContent($this->HTMLError($r['Error'], $r['Message'], $ACall));
            $this->Code = $r['Error'];
        }
        $this->Log->End();
        return $this;
    }
}
