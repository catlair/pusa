<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

/*
    Nibelung library
    2022-10-30 still@itserv.ru chiv@google.com
*/


namespace catlair;

require_once 'url.php';
require_once 'result.php';



class Nibelung extends Result
{
    private $ServiceURL = null; /* Адрес сервиса нибелунг. Передается при создании объекта. */
    private $URL        = null; /* Значение последней ссылк после установки значения методом set */


    /*
        Конструктор
    */
    public function __construct
    (
        string $AServiceURL /* URL сервиса нибелунга */
    )
    {
        $this -> ServiceURL = $AServiceURL;
    }



    /*
        Возвращает новый объект нибелунга
    */
    static public function create
    (
        string $AServiceURL /* URL сервиса нибелунга */
    )
    {
        return new self( $AServiceURL );
    }



    /*
        Cоздание новой записи нибелунга
        Вызывает текущий сервис нибелунга
    */
    public function setParam
    (
        $AValue,             /* Контент для сохранения */
        string $AKey  = null, /* Необязательный клиент Key */
        string $ASecure = null  /* Необязательное имя клиента */
    )
    {
        /* Получение ссылки */
        $AddURL = $this -> getSetLink( $AValue, $AKey, $ASecure );

        /* Направление запроса */
        $json = json_decode( file_get_contents( $AddURL ));

        /* Проверка результата */
        $this -> ifResult( empty( $json ), 'nibelung_error' );

        if( $this -> isOk() )
        {
            $this -> URL = clValueFromObject( $json, [ 'Outcome', 'info_url' ] );
        }

        return $this;
    }



    public function getParam
    (
        string $AKey    = null, /* Необязательный клиент Key */
        string $ASecure = null  /* Необязательное имя клиента */
    )
    {
        /* Получение ссылки */
        $GetURL = getGetLink( $AKey, $ASecure );

        /* Направление запроса по ссылке */
        $Value = file_get_contents( $GetURL );

        /* Получение JSON */
        $Json = json_decode( $Value );

        return empty( $Json ) ? $Value : $Json;
    }



    /*
        Возвращается последняя ссылка на информацию о значении
    */
    public function getURL()
    {
        return $this -> URL;
    }



    /*
        Возвращается URL добавления значения
    */
    public function getSetLink
    (
        $AValue         = null,     /* Сохраняемое значение */
        string $AKey     = null,     /* Необязательный идентификатор значения */
        string $ASecure = null    /* Необязательный секретный пароль клиента */
    )
    {
        return URL::Create()
        -> parse( $this -> ServiceURL )
        -> setPath([ 'main', 'set' ])
        -> clearParams()
        -> setParam( 'value'    , encodeURIComponent( json_encode( $AValue )), true )
        -> setParam( 'key'      , $AKey, true )
        -> setParam( 'secure'   , $ASecure, true )
        -> toString();
    }



    /*
        Возвращает линк для получения информации
    */
    public function getInfoLink
    (
        string $AKey    = null, /* Необязательный идфентификатор значения */
        string $ASecure = null, /* Необязательный идентификатор клиента */
        string $AHash   = null
    )
    {
        return URL::create()
        -> parse( $this -> ServiceURL )
        -> setPath([ 'main', 'info' ])
        -> clearParams()
        -> setParam( 'hash', self::getHash( $AKey, $ASecure, $AHash ), true )
        -> toString();
    }



    /*
        Возвращает линк для получения значения
    */
    public function getGetLink
    (
        string $AKey    = null, /* Необязательный идфентификатор значения */
        string $ASecure = null, /* Необязательный идентификатор клиента */
        string $AHash   = null
    )
    {
        return URL::create()
        -> parse( $this -> ServiceURL )
        -> setPath([ 'main', 'get' ])
        -> clearParams()
        -> setParam( 'hash', self::getHash( $AKey, $ASecure, $AHash ), true )
        -> toString();
    }



    /*
        Возвращает hash
    */
    static public function getHash
    (
        string $AKey    = null, /* Идентификатор */
        string $ASecure = null, /* Необязательный идентификатор клиента */
        string $AHash   = null  /* Необязательный Hash, при наличии возвращается именно он */
    )
    {
        return
        empty( $AHash )
        ? ( md5( empty( $AKey ) ? 'default' : $AKey ) . md5( empty( $ASecure ) ? 'default' : $ASecure ) )
        : $AHash;
    }
}
