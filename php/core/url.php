<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/


namespace catlair;

class TURL
{
    private $Changed     = false;   /* Url change indicator */

    private $Protocol    = null;
    private $Host        = null;
    private $Port        = null;
    private $Hash        = null;
    private $Params      = null;
    private $Path        = null;



    static function Create()
    {
        return new TURL();
    }



    function Parse( $AURL )
    {
        $URL = parse_url( $AURL );

        $this -> Protocol   = array_key_exists( 'scheme',   $URL ) ? $URL[ 'scheme' ]   : 'https';
        $this -> Host       = array_key_exists( 'host',     $URL ) ? $URL[ 'host' ]     : 'localhost';
        $this -> Port       = array_key_exists( 'port',     $URL ) ? $URL[ 'port' ]     : '';
        $this -> User       = array_key_exists( 'user',     $URL ) ? $URL[ 'user' ]     : '';
        $this -> Pass       = array_key_exists( 'pass',     $URL ) ? $URL[ 'pass' ]     : '';
        $this -> Hash       = array_key_exists( 'fragment', $URL ) ? $URL[ 'fragment' ] : '';
        $this -> Query      = array_key_exists( 'query',    $URL ) ? $URL[ 'query' ]    : '';

        /* URL parameters */
        $Params = null;
        parse_str( $this -> Query, $Params );
        $this -> Params = new TParams();
        $this -> Params -> SetParams( $Params );

        /* URL path */
        $this -> Path = explode( '/', $this -> Pass );

        $this -> Changed = true;

        return $this;
    }



    /*
        Return the URL for this PusaController
    */
    public function ToString()
    {
        /* Protocol */
        $Result = $this -> Protocol . '://';

        /* Host */
        $Result .= $this -> Host;

        /* Port */
        if( !empty( $this -> Port )) $Result .= ':' . $this -> Port;

        /* Path */
        if( !empty( $this -> Path ) ) $Result .= '/' . implode( '/', $this -> Path );

        /* Params */
        if( !empty( $this -> Params -> GetParams() )) $Result .= '?' . $this -> Params -> GetParamsAsURL();

        /* Hash */
        if( !empty( $this -> Hash )) $Result .= '#' . $this -> Hash;

        return $Result;
    }



    public function SetPath( $AValue = [] )
    {
        $this -> Path = $AValue;
        $this -> Changed = true;
        return $this;
    }



    public function SetHash( $AValue = null )
    {
        $this -> Hash = $AValue;
        $this -> Changed = true;
        return $this;
    }



    public function ClearParams()
    {
        $this -> Params = new TParams();
        $this -> Changed = true;
        return $this;
    }



    /**************************************************************************
        Getters and setters
    */


    public function SetFloat
    (
        string  $AName,
        float   $AValue = 0.0
    )
    {
        $this -> Params -> SetFloat( $AName, $AValue );
        $this -> Changed = true;
        return $this;
    }



    public function GetFloat
    (
        string  $AName,
        float   $ADefault = 0.0
    )
    {
        return $this -> Params -> GetFloat( $AName, $ADefault );
    }


    public function SetInteger
    (
        string  $AName,
        int     $AValue = 0
    )
    {
        $this -> Params -> SetInteger( $AName, $AValue );
        $this -> Changed = true;
        return $this;
    }



    public function GetInteger
    (
        string  $AName,
        int     $ADefault = 0
    )
    {
        return $this -> Params -> GetInteger( $AName, $ADefault );
    }



    /*
        Set value $AValue for parametrt $AName
    */
    public function SetString
    (
        string  $AName,         /* URL parameter name */
        string  $AValue = ''    /* Value for parameter */
    )
    {
        $this -> Params -> SetString( $AName, $AValue );
        $this -> Changed = true;
        return $this;
    }



    public function GetString
    (
        string  $AName,
        string  $ADefault = ''  /* Default value */
    )
    {
        return $this -> Params -> GetString( $AName, $ADefault );
    }



    public function SetBoolean
    (
        string  $AName,
        bool    $AValue = true
    )
    {
        $this -> Params -> SetBoolean( $AName, $AValue );
        $this -> Changed = true;
        return $this;
    }



    public function GetBoolean
    (
        string  $AName,
        bool    $ADefault = false
    )
    {
        return $this -> Params -> GetBoolean( $AName, $ADefault );
    }



    public function IsChanged()
    {
        return $this -> Changed;
    }



    public function NoChanged()
    {
        $this -> Changed = false;
        return $this;
    }
}
