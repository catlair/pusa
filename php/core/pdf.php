<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/


namespace catlair;

/*
    Require: chrome-browser
    Heracly: TPDF < TShell < TResult
*/

include_once ('result.php');
include_once ('utils.php');
include_once ('shell.php');



class TPDF extends TShell
{
    public $HTMLFile            = 'source.html';
    public $PDFFile             = 'result.pdf';
    public $ProcessedPath       = '/tmp/pdf';
    private $ProcessedFolder    = null;


    /*
        Begin convert procedure
        Create temp directory for all files
    */
    public function Begin()
    {
        /* End previous convertor session */
        if( $this -> ProcessedFolder != null ) $this -> End();
        /* Create new handle for convertor */
        $this -> ProcessedFolder = clGUID();
        return $this;
    }



    /*
        End convert procedure
        Remove temp directory with all files
    */
    public function &End()
    {
        clDeleteFolder( $this -> GetProcessedPath() );
        $this -> ProcessedFolder = null;
        return $this;
    }



    /*
        Return path for processed
    */
    public function GetProcessedPath()
    {
        return $this -> ProcessedPath . '/' . $this -> ProcessedFolder;
    }



    public function GetImageFilePath( $AName )
    {
        return $this->GetProcessedPath() . '/' . $AName;
    }



    /*
        Return PDF path and file
    */
    public function GetPDFFilePath()
    {
        return $this->GetProcessedPath() . '/' . $this -> PDFFile;
    }



    /*
        Return HTML path and file
    */
    public function GetHTMLFilePath()
    {
        return $this->GetProcessedPath() . '/' . $this -> HTMLFile;
    }



    public function IsBegined()
    {
        return $this -> ProcessedFolder != null;
    }



    /*
        Put content in to file
    */
    public function &SetContent( $AContent )
    {
        if( $this->IsOk() )
        {
            if( ! $this -> IsBegined() ) $this -> Begin();

            if( empty( $AContent ))
            {
                $this -> SetCode( 'ContentForPDFIsEmpty' );
            }

            if( $this -> IsOk() )
            {
                $File = $this -> GetHTMLFilePath();
                if( CheckPath( dirname( $File )))
                {
                    if( !file_put_contents( $File, $AContent )) $this -> SetResult( 'ErrorWriteContnet', $File );
                }
                else
                {
                    $this -> SetResult( 'ErrorCreatePathWriteContnet', dirname( $File ));
                }
            }
        }
        return $this;
    }



    public function &AddFileContent( $AName, $AData )
    {
        if( $this -> IsOk() )
        {
            $File = $this -> GetImageFilePath( $AName );
            if( !file_put_contents( $File, $AData )) $this -> SetResult( 'ErrorWriteFile', $File );
        }
        return $this;
    }



    public function &AddFile( $AName, $AFileName )
    {
        if ( $this -> IsOk() )
        {
            $File = $this -> GetImageFilePath( $AName );
            if ( file_exists(  $AFileName ))
            {
                $Content = file_get_contents( $AFileName );
                if ( !file_put_contents( $File, $Content )) $this -> SetResult( 'ErrorWriteFile', $File );
            }
            else
            {
                $this -> SetResult( 'FileNotExists', $AFileName );
            }
        }
        return $this;
    }



    /*
        Build PDF
    */
    public function &BuildPDF()
    {
        if( $this -> IsOk() )
        {
            $HTMLFile = $this -> GetHTMLFilePath();

            if ( file_exists( $HTMLFile ))
            {
                /* Convert source to PDF*/
                $this
                -> CmdBegin()
                -> CmdAdd( 'chromium-browser' )
                -> LongAdd( 'no-sandbox' )
                -> LongAdd( 'headless' )
                -> LongAdd( 'disable-gpu' )
                -> LongAdd( 'print-to-pdf-no-header' )
                -> LongAdd( 'no-margins' )
                -> LongAdd( 'print-to-pdf', $this -> GetPDFFilePath())
                -> FileAdd( $HTMLFile )
                -> CmdEnd();
            }
            else
            {
                $this -> SetResult( 'FileNotFound', $HTMLFile );
            }
        }
        return $this;
    }



    /*
        Export builded PDF file in to $FileName
    */
    public function &ExportPDF( $AFileName )
    {
        if( $this ->IsOk() )
        {
            $PDFFile = $this -> GetPDFFilePath();
            if ( ! copy ( $PDFFile , $AFileName ))
            {
                $this -> SetResult( 'ErrorExportPDFFile', $AFileName );
            }
        }
        return $this;
    }



    /*
        Convert
    */
    public function &ContentToPDF
    (
        $AContent,
        $AFileName
    )
    {
        $this
        -> Begin        ()
        -> SetContent   ( $AContent )
        -> BuildPDF     ()
        -> ExportPDF    ( $AFileName )
        -> End          ();
        return $this;
    }
}
