<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

namespace catlair;



require_once ROOT."/datasource/data.php";
require_once ROOT."/datasource/sql.php";



class TPostgreSQL extends TSQL
{
    const PREP_NAME_QUOTE = '"';
    const VALUE_QUOTE = "'";
    const PREP_NAME_ALWAYS = true;


    /*
        Counstructor for MYSQL
    */
    function __construct( $ALog )
    {
        parent::__construct($ALog);
        $this -> SetType( TData :: MYSQL );
    }



    /*
        Convert native (specific) values in to universal values.
        Last using in other php units.
    */
    public function NativeToUni( $ANative )
    {
        switch ($ANative)
        {
            case 'BOOLEAN'          : $Result = TData::TYPE_BOOLEAN; break;

            case 'SMALLINT'         : $Result = TData::TYPE_INT_8_SIGN; break;
            case 'SMALLINT'         : $Result = TData::TYPE_INT_16_SIGN; break;
            case 'INTEGER'          : $Result = TData::TYPE_INT_24_SIGN; break;
            case 'INTEGER'          : $Result = TData::TYPE_INT_32_SIGN; break;
            case 'BIGINT'           : $Result = TData::TYPE_INT_64_SIGN; break;

            case 'DATE'             : $Result = TData::TYPE_DATE; break;
            case 'TIME'             : $Result = TData::TYPE_TIME; break;
            case 'TIMESTAMP'        : $Result = TData::TYPE_MOMENT; break;

            case 'CHAR'             : $Result = TData::TYPE_STRING; break;
            case 'VARCHAR'          : $Result = TData::TYPE_STRING_UNSIZE; break;

            case 'BYTEA'            : $Result = TData::TYPE_BIN; break; /* ??? may be bit но битовые это строки */
            case 'TEXT'             : $Result = TData::TYPE_TEXT; break;
            case 'BYTEA'            : $Result = TData::TYPE_BIN_UNSIZE; break; /* ??? may be bit */
            case 'BYTEA'            : $Result = TData::TYPE_BLOB; break;
            case 'JSON'             : $Result = TData::TYPE_JSON; break;

            case 'REAL'             : $Result = TData::TYPE_FLOAT; break;
            case 'DOUBLE PRECISION' : $Result = TData::TYPE_DOUBLE; break;

            case 'PRIMARY KEY'      : $Result = TData::KEY_PRIMARY; break;
            case 'UNIQUE'           : $Result = TData::KEY_UNIQUE; break;

            case 'DESC'             : $Result = TData::ZA; break;
            case 'ASC'              : $Result = TData::AZ; break;

            case 'NOT NULL'         : $Result = TData::NOT_EMPTY; break;
            case 'UTF8'             : $Result = TData::CODEPAGE_UTF8; break;
            /*caseunique key 'AUTO_INCREMENT'   : $Result = TData::AUTO_INCREMENT; break;*/

            default:            $Result = TData::UNKNOWN; break;
        }
        return $Result;
    }



    public function UniToNative( $AUni )
    {
        switch( $AUni )
        {
            case TData::TYPE_BOOLEAN        : $Result = 'BOOLEAN'; break;

            case TData::TYPE_INT_8_SIGN     : $Result = 'SMALLINT'; break;
            case TData::TYPE_INT_16_SIGN    : $Result = 'SMALLINT'; break;
            case TData::TYPE_INT_24_SIGN    : $Result = 'INTEGER'; break;
            case TData::TYPE_INT_32_SIGN    : $Result = 'INTEGER'; break;
            case TData::TYPE_INT_64_SIGN    : $Result = 'BIGINT'; break;

            case TData::TYPE_DATE           : $Result = 'DATE'; break;
            case TData::TYPE_TIME           : $Result = 'TIME'; break;
            case TData::TYPE_MOMENT         : $Result = 'TIMESTAMP'; break;

            case TData::TYPE_STRING         : $Result = 'CHAR'; break;
            case TData::TYPE_STRING_UNSIZE  : $Result = 'VARCHAR'; break;

            case TData::TYPE_TEXT           : $Result = 'TEXT'; break;
            case TData::TYPE_JSON           : $Result = 'JSON'; break;

            case TData::TYPE_FLOAT          : $Result = 'REAL'; break;
            case TData::TYPE_DOUBLE         : $Result = 'DOUBLE PRECISION'; break;

            case TData::KEY_PRIMARY         : $Result = 'PRIMARY KEY'; break;
            case TData::KEY_UNIQUE          : $Result = 'UNIQUE'; break;

            case TData::ZA                  : $Result = 'DESC'; break;
            case TData::AZ                  : $Result = 'ASC'; break;

            case TData::NOT_EMPTY           : $Result = 'NOT NULL'; break;
            case TData::CODEPAGE_UTF8       : $Result = 'UTF8'; break;
            /*case TData::AUTO_INCREMENT    : $Result = 'AUTO_INCREMENT'; break;*/

            default:                    $Result = $AUni; break;
        }
        return $Result;
    }



    protected function GetIncrementType( $AType )
    {
        switch( $AType )
        {
            case 'SMALLINT'   : $Result = 'SMALLSERIAL'; break;
            case 'INTEGER'    : $Result = 'SERIAL'; break;
            case 'BIGINT'     : $Result = 'BIGSERIAL'; break;
            default           : $Result = null; break;
        }
        return $Result;
    }



    protected function GetConnectionString
    (
        $ADatabaseOpen
    )
    {
        return
        'host='    . $this -> GetHost() .
        ' port='        . $this -> GetPort( 5432 ) .
        ' dbname='      . ($ADatabaseOpen ? $this -> GetDatabase() : 'postgres') .
        ' user='        . $this -> GetLogin() .
        ' password='    . $this -> GetPassword()
        ;
    }


    public function Opening( $ADatabaseOpen )
    {
        try
        {
            $ConnectionString = $this -> GetConnectionString( $ADatabaseOpen );

            $Handle = pg_connect( $ConnectionString );
            if ( $Handle )
            {
                $this -> SetHandle( $Handle );
                $this -> SetResult( rcOk, '' );
            }
            else
            {
                $this -> SetResult( 'PostgreSQLOpeningError', 'PostgreSQL Opening Error');
            }
        }
        catch( \Exception $Error )
        {
            $this -> SetHandle( false );
            $this -> SetResult( 'PostgreSQLOpeningError', $Error -> getMessage() );
        }

        return !empty( $this -> FHandle );
    }



    /*
        Disconnect from mysql
    */
    public function Closing()
    {
        $this -> Log -> Begin();

        try
        {
            $this -> FHandle = !pg_close($this->FHandle);
        }
        catch( \Exception $Error )
        {
            $Handle = false;
            $this -> SetResult( 'PostgreSQLClosingError', $Error -> getMessage() );
        }

        $this -> Log -> End();

        return $this->FHandle === false;
    }

    public function GetDefaultDatabaseName()
    {
        return 'postgres';
    }

    /*
        Check connection
    */
    public function Pinging()
    {
        $Result = false;
        try
        {
            $Result = pg_ping( $this->FHandle );
        }
        catch( \Exception $Error )
        {
            $this -> SetResult( 'PostgreSQLPingError', $Error -> getMessage() );
        }
        return $Result;
    }



    /**
        Return true if database $ADatabase:string exists
    */
    public function DatabaseExisting( $ADatabase )
    {
        $this -> GetCommand() -> SetContent( "SELECT 1 from pg_database WHERE datname='$ADatabase';" );
        $this -> Calling();

        return
        $this->Result!=null &&
        count( $this->Result ) > 0 &&
        count( $this->Result[0] ) > 0;
    }


    /*
        Database $ADatabase:string create for mysql.
        Calling from TDatasource->DatabaseCreate.
        Return $this.
    */
    public function DatabaseCreating( $ADatabase, $AParams )
    {
        $Cmd = 'create database ' . $this -> PrepName( $ADatabase );
        if ($AParams!==null)
        {
            if (array_key_exists('Codepage', $AParams)) $Cmd .= ' ENCODING '. $this -> UniToNative( $AParams['Codepage'] ) . ' ';

            /*if (array_key_exists('Charset', $AParams)) $Cmd .= ' character set '.$AParams['Charset'] . ' ';*/
            /*if (array_key_exists('Collate', $AParams)) $Cmd .= ' LC_COLLATE '.$AParams['Collate'] . ' LC_CTYPE '.$AParams['Collate'] . ' ';*/
        }
        $this->GetCommand() -> SetContent($Cmd);
        $this->Calling();
        return $this;
    }



    /*
        Database $ADatabase:string deleting for mysql.
        Calling from TDatasource->DatabaseDelete.
        Return $this.
    */
    public function DatabaseDeleting( $ADatabase )
    {
        $this->RevokeConnect($ADatabase);
        $this->GetCommand() -> SetContent('drop database '. $this -> PrepName( $ADatabase ));
        $this->Calling();
        return $this;
    }


    /*
        Database $ADatabase:string deleting for mysql.
        Calling from TDatasource->DatabaseDelete.
        Return $this.
    */
    public function RevokeConnect( $ADatabase )
    {
        $this->GetCommand() -> SetContent('REVOKE CONNECT ON DATABASE ' . $this -> PrepName( $ADatabase ) . ' FROM public');
        $this->Calling();
        return $this;
    }

    /*
        Selet database $ADatabase:string
        $ADatabase - для постгреса не используется. так как выполняется переоткртыие базы из параметра.
    */
    public function DatabaseSelecting( $ADatabase )
    {
        if ($this -> Closing())
            $this -> SetDatabase($ADatabase) -> Opening( true ) ;
        else
            $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    /*
        Lock database
    */
    public function DatabaseLocking()
    {
        $this -> RevokeConnect( $this -> GetDatabase() );
        $this->GetCommand() -> SetContent('ALTER DATABASE ' . $this -> PrepName( $ADatabase ) . ' SET default_transaction_read_only to ON');
        $this->Calling();
        return $this;
    }



    /*
        Unlock database
    */
    public function DatabaseUnlocking()
    {
        $this -> RevokeConnect( $this -> GetDatabase() );
        $this->GetCommand() -> SetContent('ALTER DATABASE ' . $this -> PrepName( $ADatabase ) . ' SET default_transaction_read_only to OFF');
        $this->Calling();
        return $this;
    }



    /*
    Creating database administrator $ADatabase:string
    */
    public function DatabaseAdminCreating( $ALogin, $APassword )
    {
        $this->GetCommand() -> SetContent("create user " . $this -> PrepName( $ALogin ) . " with encrypted password '" . $APassword . "'");
        $this->Calling();
        return $this;
    }



    /**************************************************************************
        Tables
    */


    /*
        Return true if table $AName:string exists
    */
    public function TableExisting
    (
        $AName
    )
    {
        $DBName = $this -> GetDatabase();
        $this -> GetCommand() -> SetContent( "SELECT FROM information_schema.tables WHERE  table_schema='$DBName' AND table_name='$AName'" );
        $this -> Calling();
        return $this->Result!=null && count($this->Result)>0 && count($this -> Result[0])>0;
    }



    /*
        Table creating
        $AFields - array
        $AIndexes - array
    */
    public function TableCreating
    (
        array $AParams  = []
    )
    {
        $Table = clValueFromObject( $AParams, 'Name' );

        if( $Table)
        {
            $FieldsList = [];
            $Fields = clValueFromObject( $AParams, 'Fields', [] );
            foreach( $Fields as $Field )
            {
                array_push( $FieldsList, $this -> CmdFieldCreate( $Field ));
            }

            /* Buil indexes command */
            $IndexList = [];
            $Indexes = clValueFromObject( $AParams, 'Indexes', [] );
            if( $Indexes != null )
            {
                $this -> SetResult( 'IndexesNotSupported', null, [ 'Params' => $AParams ] );
            }
            else
            {
                $this -> GetCommand() -> SetContent
                (
                    'CREATE TABLE ' . $this -> PrepName( $Table )
                    . ' (' . implode( ', ', $FieldsList ) . implode( '', $IndexList ) . ')'
                );
                $this -> Calling();
            }
        }
        else
        {
            $this -> SetResult( 'NoTableName', null, [ 'Params' => $AParams ] );
        }
        return $this;
    }



    /*
        Table creating
    */
    public function &FieldCreating
    (
        $ATable,
        $AField
    )
    {
        $Cmd = 'ALTER TABLE ' . $this -> PrepName( $ATable ) . ' ADD COLUMN ' . $this -> CmdFieldCreate( $AField );
        $this -> GetCommand() -> SetContent( $Cmd );
        $this -> Calling();
        return $this;
    }



    /*
        Table creating
    */
    public function &IndexCreating($ATable, $AType, $AFields, $AName)
    {
        $Fields = [];
        foreach( $AFields as $Field )
        {
            array_push( $Fields, $this -> PrepName( $Field ));
        }
        if (empty($AName))
            $AName=$this -> PrepName( 'fti_' . $ATable . implode('_', $AFields) );

        switch ($AType)
        {
            case TData::INDEX_FULLTEXT:
                $Cmd = 'CREATE INDEX '
                . $AName
                . ' ON ' . $this -> PrepName( $ATable )
                . ' USING GIN ( to_tsvector(\'simple\',' . implode (', ', $Fields) .'))';
            break;
            to_tsvector('english', 'a fat  cat sat on a mat - it ate a fat rats');
            default:
                $Cmd = 'CREATE INDEX '
                . $this -> PrepName( 'fti_' . implode('_', $AFields) )
                . ' ON ' . $this -> PrepName( $ATable )
                . '(' . implode (', ', $Fields) .')';
            break;
        }
        $this -> GetCommand() -> SetContent($Cmd);
        $this -> Calling();
        return $this;
    }



    /*
    */
    public function UserExisting($ALogin, $AHost)
    {
        $this -> GetCommand() -> SetContent( "select usename from pg_user where usename='$ALogin'" );
        $this -> Calling();
        return $this->Result!=null && count($this->Result)>0 && count($this->Result[0])>0;
    }



    /*
    */
    public function &UserDeleting($ALogin, $AHost)
    {
        $this -> GetCommand() -> SetContent( "drop user '$ALogin'" );
        $this -> Calling();
        return $this;
    }


    /*
        Return true if procedure $AName:string exists else return false
    */
    public function ProcedureExisting
    (
        $AName
    )
    {
        $this -> GetCommand() -> SetContent("SELECT proname FROM pg_proc WHERE proname='$AName'");
        $this -> Calling();
        return $this->Result!=null && count($this->Result)>0 && count($this->Result[0])>0;

    }



    /**************************************************************************
        Record CRUD
    */


    /* */
    protected function IDInsertedGetting()
    {
        /* SELECT currval(pg_get_serial_sequence('persons','id')); нужно передовать имя таблицы и поле  */
        $this -> GetCommand() -> SetContent('SELECT lastval() "LastValue"');
        $this -> Calling();
        $Result = null;
        if ($this->Result!=null && count($this->Result)>0 && count($this->Result[0])>0)
            $Result = $this -> Result[0] -> LastValue;
        return $Result;
        
    }


    /**************************************************************************
        Call procedure
    */

    /*
        Call stored procedure $AProcName with input paremeters $AValue:array [ "Name"=>"Value" ]
    */
    public function Procing
    (
        $AProcName,
        $AValues
    )
    {
        $Values = [];

        foreach( $AValues as $Value )
        {
            array_push( $Values,    $this -> ValueToParam( $Value, true ));
        }

        $this -> GetCommand()
        -> SetContent( 'CALL %ProcName%( %Values% )' )
        -> SetParam( 'ProcName', $AProcName )
        -> SetParam( 'Values', implode( ', ', $Values ));

        $this -> Calling();

        return $this;
    }



    /*
        Specific Calling action for this Datasource
    */
    public function &Calling( $ACommand = null ) 
    {
        /* Reset result */
        $this -> Result = null;

        /* Get command */
        $Cmd = empty( $ACommand ) ?  $this -> GetCommand() -> GetContent() : $ACommand -> GetContent();

        $this -> Log
        -> Trace( '' ) -> Param( 'Database', $this->GetDatabase())
        -> Trace() -> Param( 'Command', $Cmd ) -> LineEnd();

        /* Execute query */
        try
        {
            $Result = pg_query( $this->FHandle, $Cmd );

            if ($Result === false)
            {
                $this -> SetResult
                (
                    $this -> GetSpecificResultCode(),
                    pg_last_error( $this -> FHandle )
                );
            }

            if( $Result !==null )
            {
                $this -> Result = [];
                $ds = [];
                while ($Row = pg_fetch_object($Result))
                {
                    array_push( $ds, $Row );
                }
                array_push( $this -> Result, $ds );
            }

        }
        catch( Throwable $Error )
        {
            $this -> SetResult( 'PostgreSQLExchangeError', $Error -> getMessage() );
            $this -> Log -> Error( $Error -> getMessage() );
        }

        return $this;
    }





    /**************************************************************************
        Service
    */


    /*
    */
    protected function GetSpecificResultCode()
    {
        return 'PostgreSQLError';
    }



    /*
        Create Field params from array
    */
    public function CmdFieldCreate( $AField ) 
    {
        $Cmd = '';

        $Name = clValueFromObject( $AField, 'Name' );
        $Type = clValueFromObject( $AField, 'Type' );

        if( ! empty( $Name ) && ! empty( $Type ))
        {
            if( array_key_exists( 'Increment',  $AField ) && $AField[ 'Increment ' ])
            {
                $Type = $this -> GetIncrementType ( $Type );
            }
            if (!empty($Type))
            {
                $Cmd = $this -> PrepName( $Name ) . ' ' . $this -> UniToNative( $Type );
                if( array_key_exists( 'Length',     $AField )) $Cmd .= ' (' . $AField[ 'Length' ] . ')';
                if( array_key_exists( 'NotNull',    $AField ) && $AField[ 'NotNull' ]) $Cmd .= ' ' .  $this -> UniToNative( TData::NOT_EMPTY );
                if( array_key_exists( 'Default',    $AField )) $Cmd .= ' DEFAULT ' . $this -> ValueToParam( $AField[ 'Default' ] );
                if( array_key_exists( 'Key',        $AField )) $Cmd .= ' ' . $this -> UniToNative( $AField[ 'Key' ]);
            }
            else
            {
                $this -> SetResult( 'NoFindIncrementType', null, [ 'Field' => $AField ] );
            }
        }
        else
        {
            $this -> SetResult( 'NoTypeOrField', null, [ 'Field' => $AField ] );
        }
        return $Cmd;
    }



    public function BinaryToString($ABinary)
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    protected function EscapeString( $AValue )
    {
        return pg_escape_string( $this -> FHandle, $AValue );
    }



    protected function BuildLimits
    (
        array $ASelect = [],
        string $APrefix =''
    )
    {
        $Result = '';
        if( $this -> IsOk())
        {
            $Limit = clValueFromObject( $ASelect, 'Limit', null );

            if( $Limit !== null )
            {
                $Skip = clValueFromObject( $ASelect, 'Skip' );

                $Result =  ( empty( $APrefix ) ? '' : ( $APrefix . ' ' ) ) . $Limit . ( empty($Skip) ? '' : ' OFFSET ' . $Skip );

            }
        }
        return $Result;
    }



    /**************************************************************************
        Database import export
    */


    /*
        select databaseobject
    */
    public function &DatabaseObjectTablesSelecting()
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    public function &DatabaseObjectFieldsSelecting( $ATableName )
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    /*
        Execute source
    */
    public function &OnExecuteSource( $ASource )
    {
        $this -> GetCommand()
        -> Clear()
        -> SetContent ($ASource)
        ;
        $this -> Calling();
        return $this;
    }



    /*
    */
    public function &DatabaseObjectFunctionsCodeLoading ( $AFunctionName )
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    /*
        select databaseobject
    */
    public function &DatabaseObjectFunctionsSelecting()
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    /*
        select databaseobject
    */
    public function &DatabaseObjectIndexSelecting()
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    /*
    */
    public function &DatabaseObjectProcedureCodeLoading ( $AProcedureName )
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    /*
        select databaseobject
    */
    public function &DatabaseObjectProceduresSelecting()
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    public function &DatabaseObjectTriggerCodeLoading ( $ATriggerName )
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    /*
        select databaseobject
    */
    public function &DatabaseObjectTriggersSelecting()
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    /*
        START Transaction
    */
    public function &TranStarting()
    {
        $this -> GetCommand() -> SetContent( 'BEGIN' );
        $this -> Calling();
        return $this;
    }



    /*
        Commit Transaction
    */
    public function &TranCommiting()
    {
        $this -> GetCommand() -> SetContent( 'COMMIT' );
        $this -> Calling();
        return $this;
    }



    /*
        Rollback Transaction
    */
    public function &TranRollbacking()
    {
        $this -> GetCommand() -> SetContent( 'ROLLBACK' );
        $this -> Calling();
        return $this;
    }



    /*
        Test Transaction
    */
    public function &TestTransaction()
    {
        $this -> TranStarting();
        $this -> GetCommand() -> SetContent( 'INSERT INTO "Tasks" ("ID", "IDUser", "DTInsert", "Status") VALUES (\'69196613-5d2d-cfc8-9258-c1ba9bd7c2dd\', \'Test\', \'2022-01-08\', \'Ok\')' );
        $this -> Calling();
        /*$this -> TranRollbacking();
        $this -> TranStarting();*/
        $this -> GetCommand() -> SetContent( 'INSERT INTO "Tasks" ("ID", "IDUser", "DTInsert", "Status") VALUES (\'69196613-5d2d-cfc8-9258-c1ba9bd7c2de\', \'Test\', \'2022-01-08\', \'Ok\')' );
        $this -> Calling();
        $this -> TranRollbacking();
        return $this;
    }



    /*
        Build full text conditions from array
    */
    public function BuildFullText
    (
        $AArg1,
        $AArg2
    )
    {
        return "to_tsvector('simple',$AArg1) @@ to_tsquery($AArg2)";
    }

}
