<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

namespace catlair;


require_once ROOT . '/core/result.php';

/*
    Default
*/
class TDataset extends \ArrayObject
{
    private $Index  = -1;
    private $Begin  = true;
    private $End    = true;


    /*
        Create new dataset
    */
    static function Create( $AArray )
    {
        $Result = new TDataset( $AArray );
        $Result -> First();
        return $Result;
    }



    public function First()
    {
        return $this -> SetIndex( 0 );
    }



    public function Last()
    {
       return $this -> SetIndex( count( $this ) - 1 );
    }



    public function Prev( $ACount = 1 )
    {
        return $this -> SetIndex( $this -> Index - $ACount );
    }



    public function Next( $ACount = 1 )
    {
        return $this -> SetIndex( $this -> Index + $ACount );
    }



    /*
        Loop for result dataset
    */
    public function Loop
    (
        $AOnRecord,         /* Callback function for each record */
        $AOnBefore  = null, /* Callback before loop */
        $AOnAfter   = null  /* Callback after loop */
    )
    {
        if( !empty( $AOnRecord ))
        {
            /* Call before action */
            if( $AOnBefore )
            {
                $AOnBefore( $this );
            }

            /* Loop with record */
            foreach( $this as &$Record )
            {
                $AOnRecord( TParams::New( get_object_vars( $Record )));
            }

            /* Call after action */
            if( $AOnAfter )
            {
                $AOnBefore( $this );
            }
        }
        return $this;
    }



    public function IsBegin()
    {
        return $this -> Begin;
    }



    public function IsEnd()
    {
        return $this -> End;
    }



    /*
        Seters and getters
    */
    public function GetCount()
    {
        return count( $this );
    }



    public function GetIndex()
    {
        return $this -> Index;
    }



    /*
        Set index in bounds from 0 to count record - 1
    */
    public function SetIndex( $AIndex )
    {
        $c = count( $this ) - 1;

        if( $c >= 0 )
        {
            $this -> Begin  = $AIndex < $c;
            $this -> End    = $AIndex > $c;

            if( $this -> Begin ) $this -> Index = 0;
            elseif( $this -> End ) $this -> Index = $c;
            else $this -> Index = $AIndex;
        }

        return $this;
    }



    /*
        Return record as array
    */
    public function GetRecordAsArray
    (
        int $AIndex = null
    )
    {
        $Result = [];
        $Index = $AIndex == null ? $this -> Index : $AIndex;
        if( $Index > -1 && $Index < count( $this ) )
        {
            $Result = get_object_vars( $this[ $Index ] );
        }
        return $Result;
    }



    /*
        Return record as array
    */
    public function GetArray
    (
        int $AIndex = null
    )
    {
        return $this -> GetRecordAsArray( $AIndex );
    }



    /*
        Return record as TParams
    */
    public function GetParams
    (
        int $AIndex = null
    )
    {
        return TParams::New() -> SetParams( $this -> GetRecordAsArray( $AIndex ));
    }



    /*
        TODO remove it from dataset to child of TDataset object
    */
    public function GetResult()
    {
        $Result = new TResult();

        if( $this -> GetCount() == 0 )
        {
            $Result -> SetCode( 'DatasetIsEmpty' );
        }
        else
        {
            $Code =  $this -> GetParams() -> GetString( 'Code', '' );
            if( empty( $Code ))
            {
                $Result -> SetCode( 'DatasetCodeIsEmpty' );
            }
            else
            {
                $Result -> SetCode( $Code ) -> SetOk([ 'Ok' ]);
            }
        }
        return $Result;
    }



    public function ResultTo( &$AResult )
    {
        $this -> GetResult() -> ResultTo( $AResult );
        return $this;
    }

}
