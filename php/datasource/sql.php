<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

/*
    2022.01.08
    igorptx@gmail.com
*/

namespace catlair;



require_once ROOT."/datasource/data.php";

class TSQL extends TData
{
    const METHOD_NOT_EMPLEMENTED  = 'MethodNotEmplemented';
    const PREP_NAME_QUOTE = null;
    const VALUE_QUOTE = '';
    const PREP_NAME_ALWAYS = false;

    /*
        Counstructor for MYSQL
    */
    function __construct( $ALog )
    {
        parent::__construct($ALog);
        $this -> SetType( TData :: MYSQL );
    }

    /*
        Convert native (specific) values in to universal values.
        Last using in other php units.
    */
    public function NativeToUni( $ANative )
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return null;
    }



    /*
        Conver universal TData names to native names
    */
    public function UniToNative( $AUni )
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return null;
    }



    public function Opening( $ADatabaseOpen )
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return false;
    }



    /*
        Disconnect from mysql
    */
    public function Closing()
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return true;
    }



    /*
        Check connection
    */
    public function Pinging()
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return false;
    }



    /*
        Return true if database $ADatabase:string exists
    */
    public function DatabaseExisting( $ADatabase )
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return false;
    }




    public function DatabaseCreating( $ADatabase, $AParams )
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    /*
        Database $ADatabase:string deleting for mysql.
        Calling from TDatasource->DatabaseDelete.
        Return $this.
    */
    public function DatabaseDeleting( $ADatabase )
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    /*
        Selet database $ADatabase:string
    */
    public function DatabaseSelecting( $ADatabase )
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    /*
        Lock database
    */
    public function DatabaseLocking()
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    /*
        Unlock database
    */
    public function DatabaseUnlocking()
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    /*
    Creating database administrator $ADatabase:string
    */
    public function DatabaseAdminCreating( $ALogin, $APassword )
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    /**************************************************************************
        Tables
    */


    /*
        Return true if table $AName:string exists
    */
    public function TableExisting( $AName )
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return false;
    }



    /*
        Table creating
        $AFields - array
        $AIndexes - array
    */
    public function TableCreating
    (
        array $AParams  = []
    )
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    /*
        Table delete
        $ATable
    */
    public function TableDeleting
    (
        $ATable
    )
    {
        $this -> GetCommand() -> SetContent( 'DROP TABLE ' . $this -> PrepName( $ATable ));
        $this -> Calling();
        return $this;
    }



    /*
        Table creating
    */
    public function &FieldCreating
    (
        $ATable,
        $AField
    )
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    /*
        Table deleting
    */
    public function &FieldDeleting
    (
        $ATable,
        $AField
    )
    {
        $Cmd = 'ALTER TABLE ' . $this -> PrepName( $ATable ) . ' DROP COLUMN ' . $AField;
        $this -> GetCommand() -> SetContent( $Cmd );
        $this -> Calling();
        return $this;
    }



    /**************************************************************************
        Indexes
    */

    /*
        Table creating
    */
    public function &IndexCreating($ATable, $AType, $AFields, $AName)
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    /*
    */
    public function &IndexDeleting($ATable, $AFields, $AName)
    {
        if (empty($AName))
            $AName=$this -> PrepName( 'fti_' . implode('_', $AFields) );
        $Cmd = 'DROP INDEX ' . $AName . ' ON ' . $this -> PrepName( $ATable );
        $this -> GetCommand() -> SetContent($Cmd);
        $this -> Calling();
        
        return $this;
    }




    /*
    */
    public function UserExisting($ALogin, $AHost)
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return false;
    }



    /*
    */
    public function &UserDeleting($ALogin, $AHost)
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    /*
    */
    public function &FunctionDeleting($AName)
    {
        $this -> GetCommand() -> SetContent( 'DROP function if exists ' . $AName );
        $this -> Calling();
        return $this;
    }



    /*
    */
    public function &ProcedureDeleting($AName)
    {
        $this -> GetCommand() -> SetContent( 'DROP procedure if exists ' . $AName );
        $this -> Calling();
        return $this;
    }


    /*
        Return true if procedure $AName:string exists else return false
    */
    public function ProcedureExisting($AName)
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return false;
    }

    public function FunctionExisting($AName)
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return false;
    }


    public function ProcUploading($AName, $AContent)
    {
        if ($this -> ProcedureExisting($AName)) {
            $this -> ProcedureDeleting($AName);
        }

        $this -> GetCommand() -> SetContent($AContent);
        $this -> Calling();
        return $this;
    }

    public function FuncUploading($AName, $AContent)
    {
        if ($this -> FunctionExisting($AName)) {
            $this -> FunctionDeleting($AName);
        }

        $this -> GetCommand() -> SetContent($AContent);
        $this -> Calling();
        return $this;
    }


    /**************************************************************************
        Record CRUD
    */

    /*
        Specific INSERT action for this Datasource
    */
    public function RecordInserting
    (
        string  $ATable,
        array   $AFields    = [],
        array   $AValues    = []
    )
    {
        $this -> GetCommand()
        -> SetContent( $this -> BuildTables( [ 'Tables' => [ $ATable ]], 'INSERT INTO' ))
        -> Add( $this -> BuildFields( [ 'Fields' => $AFields ], '(', ')' ))
        -> Add( $this -> BuildValues( $AValues, ' VALUES (', ')', true ));

        return $this -> Calling();
    }



    /**/
    protected function IDInsertedGetting()
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return null;
    }



    /*
        Specific UPDATE action for this Datasource
        Updating
        (
            'Table',
            [ 'Filed1' => 'Value1' ],
            TWhere::Create( 'Field2=$Field2 and Field3=$Key3', [ 'Field2' => 10, 'Key3' => 30 ] )
        )
    */
    protected function RecordUpdating
    (
        array   $ATables,
        array   $AValues,
        array   $AConditions    = []
    )
    {
        $this -> GetCommand()
        -> SetContent( $this -> BuildTables( [ 'Tables' => $ATables ], 'UPDATE' ))
        -> Add( $this -> BuildValues( $AValues, ' SET' ))
        -> Add( $this -> BuildWhere( [ 'Conditions' => $AConditions ], ' WHERE' ));
        return $this -> Calling();
    }



    /*
        Prepare and execute Delete query
    */
    protected function RecordDeleting
    (
        array   $ATables,
        array   $AConditions    = [],
        array   $ASort          = []
    )
    {
        $this -> GetCommand()
        -> SetContent( 'DELETE' )
        -> Add( $this -> BuildTables( [ 'Tables' => $ATables ], ' FROM' ))
        -> Add( $this -> BuildWhere( [ 'Conditions' => $AConditions ], ' WHERE' ))
        -> Add( $this -> BuildOrder( $ASort, 'ORDER BY' ));

        return $this -> Calling();
    }


    /*
        Specific SELECT action for this Datasource
    */
    protected function RecordSelecting
    (
        array $ASelect = []
    )
    {
        /* Prepare SELECT query */
        $this
        -> GetCommand()
        -> SetContent( $this -> BuildSelect( $ASelect ) );
        return $this -> Calling();
    }




    /**************************************************************************
        Call procedure
    */

    /*
        Call stored procedure $AProcName with input paremeters $AValue:array [ "Name"=>"Value" ]
    */
    public function Procing
    (
        $AProcName,
        $AValues
    )
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    /*
        Specific Calling action for this Datasource
    */
    public function &Calling( $ACommand = null )
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }





    /**************************************************************************
        Service
    */


    /*
    */
    protected function GetSpecificResultCode()
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return null;
    }



    /*
        Create Field params from array
    */
    public function CmdFieldCreate( $AField )
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return null;
    }

    public function FieldRenaming( $ATableName, $AFieldOldName, $AFieldNewName )
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return null;
    }

    public function BinaryToString($ABinary)
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return null;
    }


    protected function EscapeString( $AValue )
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return null;
    }    

    /*
        Convert value to MySQL parameter
    */
    public function ValueToParam( $AValue, $AFull = true )
    {
        if ( gettype( $AValue ) === 'boolean' && strlen( $AValue ) === 0) {
            $AValue = null;
        }

        switch( gettype( $AValue ))
        {
            case 'boolean':
                if( $AValue ) $Result = 'true';
                else $Result = 'false';
            break;
            case 'object':
            case 'string':
                if( $AFull ) $Result=static::VALUE_QUOTE . $this -> EscapeString( $AValue ) . static::VALUE_QUOTE;
                elseif( strlen( $AValue ) < 255 ) $Result=static::VALUE_QUOTE . '...' . static::VALUE_QUOTE;
                else $Result = static::VALUE_QUOTE . $AValue . static::VALUE_QUOTE;
            break;
            case 'array':
                $Result = implode( ';', $AValue );
            break;
            case 'integer':
            case 'double':
            case 'float':
                $Result = (string) $AValue;
            break;
            case 'NULL':
            default:
                $Result = 'null';
            break;
        }
        return $Result;
    }




    public function ResultLoop( $AOnRecord, $ADatasetIndex = 0 )
    {
        $Element = $this -> Result[ $ADatasetIndex ];
        foreach( $Element as &$Record )
        {
            $AOnRecord( $Record, $this );
        }
        return $this;
    }



    /*
        Build select command
    */
    protected function BuildSelect
    (
        array $ASelect  = []
    )
    {
        $Fields = $this -> BuildFields( $ASelect );
        $Tables = $this -> BuildTables( $ASelect, PHP_EOL . 'FROM' );
        $Where  = $this -> BuildWhere(  $ASelect, PHP_EOL . 'WHERE' );
        $Group  = $this -> BuildGroup(  $ASelect, PHP_EOL . 'GROUP BY' );
        $Sort   = $this -> BuildOrder(  $ASelect, PHP_EOL . 'ORDER BY' );
        $Limits = $this -> BuildLimits( $ASelect, PHP_EOL . 'LIMIT' );

        return TText::Create()
        -> Add( 'SELECT' )
        -> Add( empty( $Fields )    ? '' : ( ' ' . $Fields ))
        -> Add( empty( $Tables )    ? '' : ( ' ' . $Tables ))
        -> Add( empty( $Where )     ? '' : ( ' ' . $Where ))
        -> Add( empty( $Group )     ? '' : ( ' ' . $Group ))
        -> Add( empty( $Sort )      ? '' : ( ' ' . $Sort ))
        -> Add( empty( $Limits )    ? '' : ( ' ' . $Limits ))
        -> GetContent();
    }



        /*
        Build fields

        Income:
            [
                'Fields' =>
                [
                    'Field1',
                    [ 'Operator':self::SUM', 'Table':'Table2', 'Field':'Tabel2', 'Alias':'SumField' ]
                ]
            ]

        Result:
            Field1, SUM( Table2.Field2 ) AS SumField
    */
    protected function BuildFields
    (
        array   $ASelect = [],
        string  $APrefix = null,
        string  $ASuffix = null
    )
    {
        $Result = [];
        if(  $this -> IsOk() && array_key_exists( 'Fields', $ASelect ) )
        {
            $Fields = $ASelect[ 'Fields' ];
            foreach( $Fields as $Field )
            {
                if( !is_array( $Field ))
                {
                    $Field = [ 'Field' => $Field ];
                }

                $Item = '';

                if( array_key_exists( 'Field', $Field ))
                {
                    $Item = static::PrepName( $Field[ 'Field' ] );
                }

                if( !empty( $Item ))
                {
                    if( array_key_exists( 'Table', $Field ))
                    {
                        $Item = static::PrepName( $Field[ 'Table' ] ) . '.' . $Item;
                    }

                    if( array_key_exists( 'Operator', $Field ))
                    {
                        $Item = $Field[ 'Operator' ] . '(' . $Item . ')';
                    }

                    if( array_key_exists( 'Alias', $Field ))
                    {
                        $Item = $Item . ' AS ' . static::PrepName( $Field[ 'Alias' ] );
                    }

                    if( array_key_exists( 'Order', $Field ))
                    {
                        $Item = $Item . ' ' . $this -> UniToNative( $Field[ 'Order' ] );
                    }

                    array_push( $Result, $Item );
                }
            }
        }

        return clWrap( implode( ', ', $Result ), $APrefix, $ASuffix );
    }



    protected function BuildTables
    (
        array $ASelect = [],
        $APrefix = ''
    )
    {
        $Result = [];
        if( $this -> IsOk() && array_key_exists( 'Tables', $ASelect ) )
        {
            $Tables = $ASelect[ 'Tables' ];
            $First = true;
            foreach( $Tables as $Record )
            {
                if( gettype( $Record ) == 'string')
                {
                    array_push
                    (
                        $Result,
                        $this -> PrepName( $Record )
                    );
                }
                else
                {
                    $Table = clValueFromObject( $Record, 'Table', '' );
                    if( !empty( $Table ))
                    {
                        $Join = clValueFromObject( $Record, 'Join', '' );
                        $Conditions = $this -> BuildWhere( $Record, 'ON' );

                        /* Build next table with join */
                        array_push
                        (
                            $Result,
                            ( empty( $Join ) ? ( $First ? '' : ', ' ) : ( ' ' . $Join . ' ' )) .
                            $this -> PrepName( $Table ) .
                            ( empty( $Conditions ) ? '' : ( ' ' . $Conditions ))
                        );
                        $First = false;
                    }
                }
            }
        }

        return ( empty( $APrefix ) ? '' : ( $APrefix . ' ' )) . implode( '', $Result );
    }



    /*
        Build WHERE conditions from array
    */
    protected function BuildWhere
    (
        array   $ASelect    = [],
        string  $APrefix    = ''
    )
    {
        $Result = null;
        if( $this -> IsOk() && array_key_exists( 'Conditions', $ASelect ) )
        {
            $Conditions = $ASelect[ 'Conditions' ];

            /* Conditions is not empty */
            if( !is_array( $Conditions ))
            {
                $Result = $this -> ValueToParam( $Conditions );
            }
            else
            {
                if( array_key_exists( 0, $Conditions ))
                {
                    /* Is operator */
                    $Operator = array_shift( $Conditions );

                    if( is_string( $Operator ))
                    {

                        /* Check operators */
                        switch( $Operator )
                        {
                            case TOperator::NOT:
                                $Result = 'not (' . $this -> BuildWhere([ 'Conditions' => $Conditions ]) . ')';
                            break;
                            case TOperator::OR:
                            case TOperator::AND:
                                $Args = [];
                                foreach( $Conditions as $Argument )
                                {
                                    $Arg = $this -> BuildWhere([ 'Conditions' => $Argument ]);
                                    if( !( $Arg == null || $Arg == [] || $Arg == '' || $Arg == '' ))
                                    {
                                        array_push(  $Args, $Arg );
                                    }
                                }
                                $Result = count( $Args ) == 0 ? '' : '(' . implode( ' ' . $Operator . ' ', $Args ) . ')';
                            break;

                            case TOperator::IN:
                            case TOperator::NOT_IN:
                                if( count( $Conditions ) == 2 )
                                {
                                    $Args = [];

                                    $Arg1 = $this -> BuildWhere([ 'Conditions' => $Argument[ 0 ] ]);
                                    $Arg2 = $Argument[ 1 ];

                                    if( is_array( $Arg2 ))
                                    {
                                        foreach( $Arg2 as $Arg )
                                        {
                                            array_push(  $Args, $this -> BuildWhere([ 'Conditions' => $Arg ]) );
                                        }
                                    }
                                    else
                                    {
                                        array_push(  $Args, $this -> BuildWhere([ 'Conditions' => $Arg2 ]) );
                                    }

                                    $Result = $Arg1 . ' ' . $Operator . ' (' . implode( ', ', $Args ) . ')';
                                }
                                else
                                {
                                    $this -> SetResult( 'CountArgumentMustTwo', null, [ 'Operator' => $Operator ] );
                                }
                            break;

                            case TOperator::FULL_TEXT:
                            case TOperator::LIKE:
                            case TOperator::NOT_LIKE:
                            case TOperator::EQUAL:
                            case TOperator::NOT_EQUAL:
                            case TOperator::MORE:
                            case TOperator::MORE_EQUAL:
                            case TOperator::LESS:
                            case TOperator::LESS_EQUAL:
                                if( count( $Conditions ) == 2 )
                                {
                                    $Arg1 = $this -> BuildWhere([ 'Conditions' => $Conditions[ 0 ] ]);
                                    $Arg2 = $this -> BuildWhere([ 'Conditions' => $Conditions[ 1 ] ]);
                                    switch( $Operator )
                                    {
                                        case TOperator::FULL_TEXT:
                                            $Result = $this -> BuildFullText( $Arg1, $Arg2 );
                                        break;
                                        default:
                                            $Result = $Arg1 . ' ' . $Operator . ' ' . $Arg2;
                                        break;
                                    }
                                }
                                else
                                {
                                    $this -> SetResult( 'NotEnoughArguments', null, [ 'Operator' => $Operator ] );
                                }
                            break;

                            default:
                                $this -> SetResult( 'UnknownOperator', null, [ 'Operator' => $Operator ] );
                            break;
                        }
                    }
                    else
                    {
                        $this -> SetResult( 'UnknownTypeOperator', null, [ 'Operator' => $Operator ] );
                    }
                }
                else
                {
                    /* Is not operator */

                    /* Conditions with tables and fields */
                    if( array_key_exists( 'Field', $Conditions ))
                    {
                        $Result = $this -> BuildFields
                        (
                           [ 'Fields' =>  [ $Conditions ] ]
                        );
                    }

                    /* Subrequest */
                    if( array_key_exists( 'Select', $Conditions ))
                    {
                        $Result = $this -> BuildSelect( $Conditions[ 'Select' ] );
                    }
                }
            }
        }

        return empty( $Result ) ? '' : ( ( empty( $APrefix ) ? '' : ( $APrefix . ' ' )) . $Result );
    }



    /*
        Build source for GROUP BY section
    */
    protected function BuildGroup
    (
        array   $ASelect = [],
        string  $APrefix = ''
    )
    {
        $Result = '';
        if( $this -> IsOk() && array_key_exists( 'Groups', $ASelect ) )
        {
            /* Use build fields for group fields */
            $Result = $this -> BuildFields
            (
                [ 'Fields' => $ASelect[ 'Groups' ]]
            );
        }
        return
        empty( $Result )
        ? ''
        :
        (
            ( empty( $APrefix ) ? '' : ( $APrefix . ' ' ) ) . $Result
        );
    }



    /*
        Build source for GROUP BY section
    */
    protected function BuildOrder
    (
        array   $ASelect = [],
        string  $APrefix = ''
    )
    {
        $Result = '';
        if( $this -> IsOk() && array_key_exists( 'Sort', $ASelect ) )
        {
            /* Use build fields for group fields */
            $Result = $this -> BuildFields
            (
                [ 'Fields' => $ASelect[ 'Sort' ]]
            );
        }
        return
        empty( $Result )
        ? ''
        :
        (
            ( empty( $APrefix ) ? '' : ( $APrefix . ' ' ) ) . $Result
        );
    }



    protected function BuildLimits
    (
        array $ASelect = [],
        string $APrefix =''
    )
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return null;
    }



    /*
        Build values in format "Prefix Key=Value,Key=value,..."
    */
    public function BuildValues
    (
        array   $AValues = [],
        string  $APrefix = '',
        string  $ASuffix = '',
        bool    $ASimple = false
    )
    {
        $Result = [];

        foreach( $AValues as $Name => $Value )
        {
            array_push
            (
                $Result,
                ( $ASimple ? '' : ( $this -> PrepName( $Name ) . '=' )) . $this -> ValueToParam( $Value, true )
            );
        }

        return clWrap( implode( ', ', $Result ), $APrefix, $ASuffix );
    }



    /*
        Quoting sql essences
    */
    static protected function PrepName
    (
        string $AName, /* Name of SQL essence */
        string $AQuote = null
    )
    {
        if ($AQuote == null) $AQuote = static::PREP_NAME_QUOTE;

        $Quote = static::PREP_NAME_ALWAYS ? true : strpos( $AName, ' ' ) === 0;

        /* Or specific words */
        switch( strtolower( $AName) )
        {
            case 'select':
            case 'in':
            case 'group':
            case 'order':
            case 'by':
            case 'limit':
            case 'from':
            case 'where':
            case 'alter':
            case 'index':
            case 'create':
            case 'delete':
            case 'update':
            case 'insert':
            case 'inner':
            case 'join':
            case 'like':
            case 'into':
            case 'values':
            case 'not':
            case 'as':
            case 'sum':
            case 'avg':
            case 'min':
            case 'max':
                $Quote = true;
            break;
        }
        $QuoteChar = $Quote ? $AQuote : '';
        return $QuoteChar . $AName . $QuoteChar;
    }


    /**************************************************************************
        Database import export
    */


    /*
        select databaseobject
    */
    public function &DatabaseObjectTablesSelecting()
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return null;
    }



    public function &DatabaseObjectFieldsSelecting( $ATableName )
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return null;
    }



    /*
        Execute source
    */
    public function &OnExecuteSource( $ASource )
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return null;
    }



    /*
    */
    public function &DatabaseObjectFunctionsCodeLoading ( $AFunctionName )
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    /*
        select databaseobject
    */
    public function &DatabaseObjectIndexSelecting()
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    /*
    */
    public function &DatabaseObjectProcedureCodeLoading ( $AProcedureName )
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    /*
        select databaseobject
    */
    public function &DatabaseObjectProceduresSelecting()
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    public function &DatabaseObjectTriggerCodeLoading ( $ATriggerName )
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    /*
        select databaseobject
    */
    public function &DatabaseObjectTriggersSelecting()
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    /*
        START Transaction
    */
    public function &TranStarting()
    {

        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    /*
        Commit Transaction
    */
    public function &TranCommiting()
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }
    /*
        Rollback Transaction
    */
    public function &TranRollbacking()
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    /*
        Test Transaction
    */
    public function &TestTransaction()
    {
        $this -> NotEmplimented( __FUNCTION__ );
        return $this;
    }



    /*
        Build full text conditions from array
    */
    public function BuildFullText
    (
        $AArg1,
        $AArg2
    )
    {
        return '';
    }
}
