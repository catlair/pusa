<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

/*
    Catlair PHP Datasource module.

    12.09.2019

    still@itserv.ru
    igorptx@gmail.com

    This module contains methods for basic data sources.
    TData does not need to be create directly.
    Factory for:
        class TMySQL            Suported
        class TPostGreeSQL      Suported
        class TFileStorage      Pending implementation
        class TMSSQL extends    Pending implementation
        class TSQLight extends  Pending implementation
        class TRest

    Data operations
                    Create
                    |  Select
                    |  |  Update
                    |  |  |  Delete
                    |  |  |  |  Open
                    |  |  |  |  | cLose
                    |  |  |  |  |  |  Execute
                    |  |  |  |  |  |  | eXists
                    |  |  |  |  |  |  |  |
    -----------------------------------------
    Database        C  S  .  D  О  L  .  .
    Table           C  .  .  D  .  .  .  .
    Field           C  S  U  D  .  .  .  .
    Index           C  S  U  D  .  .  .  .
    Record          C  S  U  D  .  .  .  .
    Transaction     .  .  .  .  O  L  .  .
    StoredProc      C  S- U- D  .  .  E  X
    Function        C  S- U- D  .  .  E  X
    User            C  S  U  D  .  .  .  X
*/

namespace catlair;


/* Include catalir libraryes */
require_once( ROOT . '/core/debug.php' );
require_once( ROOT . '/core/text.php' );
require_once( ROOT . '/core/config.php' );
require_once( ROOT . '/core/operator.php' );
require_once( ROOT . '/datasource/dataset.php' );



class TData extends TResult
{
    /* Type of datacore */
    const DEFAULT               = "default";
    const FILE                  = "file";
    const MYSQL                 = "mysql";
    const SQLITE                = "sqlite";
    const POSTGRESQL            = "postgresql";
    const MSSQL                 = "mssql";
    const REST                  = "rest";

    /* Data Type */
    const UNKNOWN               = 'unknown';
    const TYPE_BOOLEAN          = 'boolean';
    const TYPE_INT_8_SIGN       = 'int_8_sign';
    const TYPE_INT_16           = 'int_16';
    const TYPE_INT_16_SIGN      = 'int_16_sign';
    const TYPE_INT_24           = 'int_24';
    const TYPE_INT_24_SIGN      = 'int_24_sign';
    const TYPE_INT_32           = 'int_32';
    const TYPE_INT_32_SIGN      = 'int_32_sign';
    const TYPE_INT_64           = 'int_64';
    const TYPE_INT_64_SIGN      = 'int_64_sign';
    const TYPE_MOMENT           = 'moment';
    const TYPE_DATE             = 'date';
    const TYPE_TIME             = 'time';
    const TYPE_STRING           = 'string';
    const TYPE_STRING_UNSIZE    = 'string_unsize';
    const TYPE_BIN              = 'binary';
    const TYPE_BIN_UNSIZE       = 'binary_unsize';
    const TYPE_BLOB             = 'blob';
    const TYPE_TEXT             = 'text';
    const TYPE_FLOAT            = 'float';
    const TYPE_DOUBLE           = 'double';
    const TYPE_JSON             = 'json';

    /* Key type*/
    const KEY_UNIQUE            = 'key_unique';
    const KEY_PRIMARY           = 'key_primary';

    /* Key type*/
    const NOT_EMPTY             = 'not_empty';
    const AUTO_INCREMENT        = 'auto_increment';

    const INDEX_SIMPLE          = 'index_simple';
    const INDEX_UNIQUE          = 'index_unique';
    const INDEX_FULLTEXT        = 'index_fulltext';

    /* Codepage */
    const CODEPAGE_UTF8         = 'utf8';


    /*
        Aggregation operators
        Use in GROUP
    */
    const SUM                   = 'SUM';
    const AVG                   = 'AVG';
    const MIN                   = 'MIN';
    const MAX                   = 'MAX';

    const AZ                    = 'AZ';
    const ZA                    = 'ZA';

    /*
        Joins
    */
    const INNER                 = 'INNER JOIN';
    const LEFT                  = 'LEFT JOIN';
    const RIGHT                 = 'RIGHT JOIN';
    const FULL                  = 'FULL JOIN';

    /*
        Error codes
    */
    const NOT_SUPPORTED         = 'NotSuported';

    /*
        Logical operators.
        Use like WHERE parameter

        $Example1 = [ TOperator::IN, @Field, [ $a1, $a2, $a3 ]];

        $Example2 =
        [
            OR,
            [
                AND,
                [ TOperator::EQUAL, '@a', '10' ],
                [ TOperator::EQUAL, '@b', $Valu ],
                $Bool
            ],
            [ EQUAL, true, 16 ]
        ]
    */

    /* Protected declaraions */
    protected $FHandle          = false; /* Handle for any object as File, MySQL, MSSQL etc */

    /* Private declarations */
    private $FLogin             = '';       /* Login */
    private $FPassword          = '';       /* Password */
    private $FHost              = '';       /* Host */
    private $FPort              = 0;        /* Port */
    private $FDatabase          = '';       /* Database */
    private $FOpened            = false;    /* Connected boolan true or false */
    private $FResultID          = false;    /* Last result ID for Insert poeration */
    private $FType              = '';       /* Type datasource. It property set in child specific classes */

    public $Log                 = null;     /* TLog from debugphp */
    public $Result              = null;     /* */
    public $InterceptLog        = null;     /* Intercept log for dataset */

    private $FCommand           = null;     /* Last command text after Prepare* functions */
    private $CountOpenTran      = 0;        /* Count open transaction */



    /*
        Constructor receive $ALog:TLog
    */
    function __construct( $ALog )
    {
        $this -> Log        = $ALog;
        $this -> FType      = self::DEFAULT;
        $this -> FCommand   = TText::Create();
    }



    /*
        Factory.
        Create Datasource $ADataType with log system from $TLog
        Return datasource or null.
    */
    static public function &Create
    (
        $ALog,              /* TLog object */
        $ADataType =null    /* Datatype */
    )
    {
        switch( $ADataType )
        {
            case self::MYSQL:
                require_once 'mysql.php';
                $Result = new TMySQL( $ALog );
            break;

            case self::POSTGRESQL:
                require_once 'postgresql.php';
                $Result = new TPostgreSQL( $ALog );
            break;

            case self::MSSQL:
                require_once 'mssql.php';
                $Result = new TMSSQL( $ALog );
            break;


            default:
                $Result = new TData( $ALog );
            break;
        }
        return $Result;
    }


    final private function NotEmplemented( $AMethodName )
    {
        $this -> SetResult( TSQL::METHOD_NOT_EMPLEMENTED, null, [ 'Method' => $AMethodName ]);
        return $this;
    }

    /*
        Create datacore object from config file
    */
    static public function &CreateFromConfig
    (
        $ALog,
        $AFileName,
        $AActive = null
    )
    {
        $Result = null;

        /* Open new datasource */
        $Config = new TConfig();
        if( !$Config -> SetFile( $AFileName ) -> Read() -> IsOk() )
        {
            $ALog -> Warning( $Config -> Code ) -> Param( 'File', $AFileName );
            $Result = self::Create( $ALog ) -> ResultFrom( $Config );
        }
        else
        {
            $Active = empty( $AActive ) ? $Config -> GetParam( 'Active' ) : $AActive;
            $Section = $Config -> GetParam( $Active );

            if( ! empty( $Section ))
            {
                $Result =
                self::Create( $ALog, clValueFromObject( $Section, 'Type', self::DEFAULT ))
                -> SetDatabase( clValueFromObject( $Section, 'Database' , 'default' ))
                -> SetHost(     clValueFromObject( $Section, 'Host'     , 'localhost' ))
                -> SetLogin(    clValueFromObject( $Section, 'Login'    , 'root' ))
                -> SetPassword( clValueFromObject( $Section, 'Password' , 'root' ))
                -> SetPort(     clValueFromObject( $Section, 'Port'     , 0 ));
            }
         }

         return $Result;
    }



    /*
        Call stored procedure by name $AName:string with $AArguments:$AArgs
        It give posibility call SP as:
            $Datasource = new TDatasource(Log);
            ...
            $Datasource->FunctionName(Param1,Param2,....);
    */
    public function __call( $AName, $AArgs )
    {
        $this -> Proc( $AName, $AArgs );
        return $this -> Result;
    }



    /*
        TODO remove from project (TDescriptStream)
    */
    public function CallJson( $AName, $AArgs )
    {
        return $this
        -> Proc( $AName, [ json_encode( $AArgs )])
        -> Result;
    }



    /**************************************************************************
        Datasource
    */


    /*
        Open datasource
    */
    public function &Open
    (
        bool $ADatabaseOpen = true,
        int $AReadTimeout = 60
    )
    {
        if( !$this -> FOpened && $this -> CheckMethod( 'Opening' ))
        {
            $this -> Log -> Begin();
            $this -> FOpened = $this -> Opening( $ADatabaseOpen, $AReadTimeout );
            if( $this -> FOpened )
            {
                $this -> Log
                -> Trace    ( 'Datasource is opened' )
                -> Param    ( 'Host', $this->FHost )
                -> Param    ( 'Database', $this->FDatabase );
            }
            else
            {
                $this -> Log
                -> Warning  ( 'Datasource is not opened' )
                -> Param    ( 'Host', $this -> FHost )
                -> Param    ( 'Database', $this -> FDatabase );
            }
            $this -> Log -> End();
        }
        return $this;
    }



    /*
        Close datasource
    */
    public function Close()
    {
        if ($this -> CheckOpened() && $this -> CheckMethod('Closing') )
        {
            $this -> Log -> Begin();
            $this -> FOpened = !$this -> Closing();
            $this -> Log -> End();
        }
        return $this;
    }



    /*
        Ping datasource
    */
    public function Ping()
    {
        $Result = false;
        if( $this -> FOpened && $this -> CheckMethod('Pinging'))
        {
            $Result = $this -> Pinging();
        }
        return $Result;
    }



    /*
        Call code with CommandLine
    */
    public function Call()
    {
        if( $this -> CheckOpened() && $this -> CheckMethod( 'Calling' ) )
        {
            $this -> Log -> Begin( 'Call command' );
            $this -> Calling();
            $this -> Log -> End();
        }
        return $this;
    }



    /*
        Call stored procdure with $AProcName:string name
    */
    public function Proc
    (
        string $AName,          /* The stored procedure name */
        array $AValues = []     /* Array of values */
    )
    {
        if( $this -> CheckOpened() && $this -> CheckMethod( 'Procing' ))
        {
            $this -> Log -> Begin( $AName );
            $this -> Procing( $AName, $AValues );
            $this -> Log -> End();
        }
        return $this;
    }



    /*
        Call stored procdure with $AProcName:string name
    */
    public function ProcJSON
    (
        string $AName,          /* The stored procedure name */
        array $AValues = []     /* Array of values key=>value */
    )
    {
        return $this -> Proc( $AName, [json_encode( $AValues )] );
    }


    /**************************************************************************
        Databse
    */


    /*
        Check database $ADatabase:string exists and return true or false
    */
    public function DatabaseExists( $ADatabase )
    {
        $Result = false;
        if( $this -> CheckOpened() && $this->CheckMethod( 'DatabaseExisting' ))
        {
            $this -> Log -> Begin(  );
            $Result = $this->DatabaseExisting( $ADatabase );
            $this -> Log -> End();
        }
        return $Result;
    }



    /*
        Create database $ADatabase:string.
    */
    public function &DatabaseCreate( $ADatabase, $AParams )
    {
        if ($this->CheckOpened() && $this->CheckMethod('DatabaseCreating'))
        {
            $this->Log->Begin();
            $this->DatabaseCreating($ADatabase, $AParams);
            $this->Log->End();
        }
        return $this;
    }



    /*
        Create database $ADatabase:string.
    */
    public function &DatabaseDelete( $ADatabase )
    {
        if ($this -> CheckOpened() && $this->CheckMethod('DatabaseDeleting'))
        {
            $this->Log->Begin();
            $this->DatabaseDeleting( $ADatabase );
            $this->Log->End();
        }
        return $this;
    }



    /*
        Select database.
    */
    public function &DatabaseSelect( $AName = null)
    {
        if ($this->CheckOpened() && $this->CheckMethod( 'DatabaseSelecting' ))
        {
            $this -> Log -> Begin();
            $this -> DatabaseSelecting( empty($AName) ? $this->GetDatabase() : $AName );
            $this -> Log -> End();
        }
        return $this;
    }



    /*
        Create database administrator
    */
    public function &DatabaseAdminCreate($ALogin, $APassword)
    {
        if ($this->CheckOpened() && $this->CheckMethod('DatabaseAdminCreating'))
        {
            $this->Log->Begin();
            $this->DatabaseAdminCreating( $ALogin, $APassword );
            $this->Log->End();
        }
        return $this;
    }



    /*
        Database lock.
    */
    public function &DatabaseLock()
    {
        if( $this -> CheckOpened() && $this->CheckMethod( 'DatabaseLocking' ))
        {
            $this->Log->Begin();
            $this->DatabaseLocking();
            $this->Log->End();
        }
        return $this;
    }



    /*
        Database unlock
    */
    public function &DatabaseUnlock()
    {
        if ($this -> CheckOpened() && $this->CheckMethod( 'DatabaseUnlocking' ))
        {
            $this->Log->Begin();
            $this->DatabaseUnlocking();
            $this->Log->End();
        }
        return $this;
    }



    /**************************************************************************
        Users
    */


    /*
        Check user exists by $ALogin and $Host
    */
    public function UserExists($ALogin, $Host)
    {
        if( $this -> CheckOpened())
        {
            $this->Log->Begin();
            $Result = $this->UserExisting($ALogin, $Host);
            $this->Log->End();
        }
        else
        {
            $Result=false;
        }
        return $Result;
    }



    /*
        User delete
    */
    public function UserDelete($ALogin, $Host)
    {
        if($this->CheckOpened())
        {
            $this->Log->Begin();
            $this->UserDeleting($ALogin, $Host);
            $this->Log->End();
        }
        return $this;
    }




    /*
        Procedure delete
    */
    public function &ProcedureExists( $AName )
    {
        if( $this->CheckOpened() && $this->CheckMethod( 'ProcedureExisting' ))
        {
            $this->Log->Begin();
            $this->ProcedureExisting($AName);
            $this->Log->End();
        }
        return $this;
    }



    /*
        Procedure delete
    */
    public function &ProcedureDelete($AName)
    {
        if ($this->CheckOpened() && $this->CheckMethod('ProcedureDeleting'))
        {
            $this->Log->Begin();
            $this->ProcedureDeleting($AName);
            $this->Log->End();
        }
        return $this;
    }

    /*
        Function exists
    */
    public function &FunctionExists( $AName)
    {
        if( $this->CheckOpened() && $this->CheckMethod( 'FunctionExisting' ))
        {
            $this->Log->Begin();
            $this->FunctionExisting($AName);
            $this->Log->End();
        }
        return $this;
    }

    /*
        Function delete
    */
    public function &FunctionDelete($AName)
    {
        if ($this->CheckOpened() && $this->CheckMethod('FunctionDeleting'))
        {
            $this->Log->Begin();
            $this->FunctionDeleting($AName);
            $this->Log->End();
        }
        return $this;
    }






    /**************************************************************************
        Table
    */


    /*
        Check database $ADatabase:string exists and return true or false
    */
    public function TableExists($ATable)
    {
        $Result = false;
        if( $this -> CheckOpened() && $this -> CheckMethod( 'TableExisting' ))
        {
            $this -> Log -> Begin();
            $Result = $this->TableExisting($ATable);
            $this->Log->End();
        }
        return $Result;
    }



    /*
        Create Table
    */
    public function &TableCreate
    (
        array   $AParams    = []
        /*
            Name,
            Fields  Named array of fields [ 'Name' => 'Field1' ]
            Indexes List of index [ [Field1, Field2.... ], [ ... ], [ ... ] ]
        */
    )
    {
        if
        (
            $this -> CheckOpened() &&
            $this -> CheckMethod( 'TableCreating') &&
            !$this -> TableExists( clValueFromObject( $AParams, 'Name' ))
        )
        {
            $this->Log->Begin();
            $this->TableCreating( $AParams );
            $this->Log->End();
        }
        return $this;
    }



    /*
        Delete Table
    */
    public function &TableDelete($AName)
    {
        if ($this->CheckOpened() && $this->CheckMethod('TableDeleting') && $this->TableExists($AName))
        {
            $this->Log->Begin();
            $this->TableDeleting($AName);
            $this->Log->End();
        }
        return $this;
    }



    /*
        Field create
    */
    public function &FieldCreate($ATableName, $AField)
    {
        if ($this->CheckOpened() && $this->CheckMethod('FieldCreating'))
        {
            $this->Log->Begin();
            $this->FieldCreating($ATableName, $AField);
            $this->Log->End();
        }
        return $this;
    }


    public function &FieldRename($ATableName, $AFieldOldName, $AFieldNewName)
    {
        if ($this->CheckOpened() && $this->CheckMethod('FieldRenaming'))
        {
            $this->Log->Begin();
            $this->FieldRenaming($ATableName, $AFieldOldName, $AFieldNewName);
            $this->Log->End();
        }
        return $this;
    }



    /*
        Field delete
    */
    public function &FieldDelete($ATableName, $AField)
    {
        if ($this->CheckOpened() && $this->CheckMethod('FieldDeleting'))
        {
            $this->Log->Begin();
            $this->FieldDeleting($ATableName, $AField);
            $this->Log->End();
        }
        return $this;
    }



    /*
        Index create
    */
    public function &IndexCreate($ATableName, $AType, $AFields, $AName = '')
    {
        if ($this->CheckOpened() && $this->CheckMethod('IndexCreating'))
        {
            $this->Log->Begin();
            $this->IndexCreating($ATableName, $AType, $AFields, $AName);
            $this->Log->End();
        }
        return $this;
    }



    /*
        Index delete
    */
    public function &IndexDelete($ATableName, $AFields, $AName = '')
    {
        if ($this->CheckOpened() && $this->CheckMethod('IndexDeleting'))
        {
            $this->Log->Begin();
            $this->IndexDeleting($ATableName, $AFields, $AName);
            $this->Log->End();
        }
        return $this;
    }



    /**************************************************************************
        Records CRUD
    */


    /*
        Insert new record in to TableName from $AParams:array['Name'=>'Value'...]
    */
    public function RecordInsert
    (
        array   $AParams    = []
    )
    {
        if( $this -> CheckOpened() )
        {
            $this -> Log -> Begin();

            /* Read parameters */
            $Table  = clValueFromObject( $AParams, 'Table' );
            $Records = clValueFromObject( $AParams , 'Records' );

            /* Check table exists */
            if( empty( $Table ))
            {
                $this -> SetResult( 'UnknownTableName', null, [ 'Params' => $AParams ] );
            }
            else
            {
                foreach( $Records as $Record )
                {
                    $FieldList = [];
                    $ValueList = [];

                    /* Split array on name and value arrays */
                    foreach( $Record as $Name => $Value )
                    {
                        array_push( $FieldList, ( string )$Name );
                        array_push( $ValueList, $Value );
                    }
                    /* Execute inset in data core */
                    $this -> FResultID = $this -> RecordInserting( $Table, $FieldList, $ValueList );
                }
            }
            $this -> Log -> End();
        }

        return $this;
    }



    /*
        Return last ID after insert record
        This method return the value for tables with autoincremental field
    */
    public function IDInsertedGet()
    {
        return IDInsertedGetting();
    }



    /*
        Update record in to $ATableName:string from $AParams:array['Name'=>'Value'...]
    */
    public function RecordUpdate
    (
        array   $AParams    = []
        /*
            Tables      - List of Tables    [ 'Table1', 'Table2' ]
            Values      - Named array with values ['Name'=>'Value'...]
            Conditions  - Array of conditions
        */
    )
    {
        if( $this -> CheckOpened() )
        {
            $this -> Log -> Begin();

            /* Read parameters */
            $Tables  = clValueFromObject( $AParams, 'Tables' );
            $Values = clValueFromObject( $AParams , 'Values' );
            $Conditions = clValueFromObject( $AParams , 'Conditions', [] );

            if( empty( $Tables ) || empty( $Values ))
            {
                $this -> SetResult( 'TablesOrValuesUndefined', null, [ 'Params' => $AParams ] );
            }
            else
            {
                $this -> RecordUpdating( $Tables, $Values, $Conditions );
            }
            $this -> Log -> End();
        }
        return $this;
    }



    /*
        Delete exists cortege in $ATableName:string
    */
    public function RecordDelete
    (
        array   $AParams    = []
        /*
            Tables      - List of Tables    [ 'Table1', 'Table2' ]
            Values      - Named array with values ['Name'=>'Value'...]
            Conditions  - Array of conditions
        */
    )
    {
        if( $this -> CheckOpened() )
        {
            $this -> Log -> Begin();

            /* Read parameters */
            $Tables = clValueFromObject( $AParams, 'Tables' );
            $Conditions = clValueFromObject( $AParams , 'Conditions', [] );
            $Sort = clValueFromObject( $AParams , 'Sort', [] );

            if( empty( $Tables ))
            {
                $this -> SetResult( 'TablesUndefined', null, [ 'Params' => $AParams ] );
            }
            else
            {
                $this -> RecordDeleting( $Tables, $Conditions, $Sort );
            }
            $this -> Log -> End();
        }
        return $this;
    }



    /*
        Select record from tables
        Example:
            (1)
                TSQL
                    SELECT * FROM Table1
                PHP
                    $this -> RecordSelect
                    ([
                        'Tables' => [ 'Table1' ],
                        'Fields' => '*'
                    ]);
            (2)
                TSQL
                    SELECT * FROM Table1 WHERE Table1.Field1 IN ( SELECT .... )
                PHP
                    $this -> RecordSelect
                    ([
                        'Tables' => [ 'Table1' ],
                        'Fields' => '*',
                        'Conditions' => [ 'IN', [ 'Table' => 'Table1', 'Field' => 'Field1' ], [ 'Select' => [ '....' ] ]]
                    ]);
    */
    public function RecordSelect
    (
        array $ASelect = []
        /*
            array   Tables      Array of table name [ 'Table1', [ 'Join':self::LEFT, 'Table':'Table1', 'On':[...] ], ... ]
            array   Fields      Array of field name [ 'Field1', [ 'Operator':self::SUM', 'Table':'Table2', 'Field':'Tabel2', 'Alias':'My' ], ... ]
            array   Conditions  Array of conditions [ TOperator::AND, ... ]
            array   Groups      Array of fields [ 'Field1', 'Field2', ... ]
            int     Limit       Limit records
            int     Skip        Skip records
        */
    )
    {
        if( $this -> CheckOpened() )
        {
            $this -> Log -> Begin();
            $this -> RecordSelecting( $ASelect );
            $this -> Log -> End();
        }

        return $this;
    }



    /**************************************************************************
        Dataset navigator
    */

    /*
        Return dataset
    */
    public function GetDataset( $ADatasetIndex = 0 )
    {
        return TDataset::Create
        (
            clValueFromObject( $this -> Result, $ADatasetIndex, [] )
        );
    }



    /**************************************************************************
        Utils
    */

    public function NativeToUni($AValue)
    {
        return $this->NativeToUni($AValue);
    }



    public function UniToNative($AValue)
    {
        return $this->UniToNative($AValue);
    }



    /*
        Return safe $AValue:string for files.
        This procedure prevents SQL enjections.
        All values for SQL params must be processed by this method.
    */
    public function SafeValue($AValue)
    {
        return str_replace( '"', "'", $AValue );
    }




    /**************************************************************************
        Prepare commands for datasourse By default use DML for MySQL
    */


    /*
        Property getters and setters
    */

    public function &SetOpened($AValue)
    {
        if ($AValue)
        {
            if (!$this->FOpened) $this->Open();
            else $this->Close();
        }
        return $this;
    }



    public function GetResultID()
    {
        return $this->FResultID;
    }



    public function SetResultID($AValue)
    {
        $this->FResultID = $AValue;
        return $this;
    }



    /*
        Return current login
    */
    public function GetOpened()
    {
        return $this -> FOpened;
    }



    /*
        Set handle
    */
    public function &SetHandle($AValue)
    {
        $this->FHandle = $AValue;
        return $this;
    }



    /*
        Return last command after Prepare*
    */
    public function GetCommand()
    {
        return $this -> FCommand;
    }

    public function SetCommand($ACommand)
    {
        $this -> GetCommand() -> SetContent( $ACommand );
        return $this;
    }


    /*
        Set command for Insert Update Select and other...
    */
    public function &LoadCommand($AFileName)
    {
        if( file_exists( $AFileName ))
        {
            $this -> SetCommand( file_get_contents( $AFileName ));
        }
        else
        {
            $this
            -> Log
            -> Warning()
            -> Param( 'File not found', $AFileName );
        }
        return $this;
    }



    /**************************************************************************
        Connection getters & setters
    */

    /*
        Return current login
    */
    public function GetLogin()
    {
        return $this->FLogin;
    }



    /*
        Set login form $AValue:string
    */
    public function SetLogin($AValue)
    {
        $this -> FLogin = $AValue;
        return $this;
    }



    /*
        Return password
    */
    public function GetPassword()
    {
        return $this -> FPassword;
    }



    /*
        Set password from $AValue:string
    */
    public function SetPassword($AValue)
    {
        $this -> FPassword = $AValue;
        return $this;
    }



    public function GetHost()
    {
        return $this -> FHost;
    }



    public function SetHost($AValue)
    {
        $this -> FHost = $AValue;
        return $this;
    }



    public function GetPort( $ADefault )
    {
        return empty( $this -> FPort ) ? $ADefault : $this -> FPort;
    }



    public function SetPort( $AValue )
    {
        $this -> FPort = $AValue;
        return $this;
    }



    public function GetType()
    {
        return $this -> FType;
    }



    public function SetType($AValue)
    {
        $this -> FType = $AValue;
        return $this;
    }



    public function GetDatabase()
    {
        return $this->FDatabase;
    }



    /*
        Set database for current.
        This do not change database.
    */
    public function SetDatabase($AValue)
    {
        $this->FDatabase = $AValue;
        return $this;
    }

    public function GetDefaultDatabaseName()
    {
        return GetDatabase();
    }

    public function GetConnectionInfo()
    {
        return $this -> GetLogin() . '@' . $this -> GetHost() . ':' . $this -> GetDatabase();
    }


    public function &GetLog()
    {
        return $this->Log;
    }



    public function &SetLog($AValue)
    {
        $this->Log = $AValue;
        return $this;
    }



    private function CheckOpened()
    {
        if ( !$this -> FOpened )
        {
            $this -> Log
            -> Info( 'Datasource is close' )
            -> Param( 'Host', $this -> FHost )
            -> Param( 'Database', $this -> FDatabase );
            $this -> FResultID = false;
        }
        return $this -> FOpened;
    }



    private function CheckMethod($AName)
    {
        if (!method_exists($this, $AName))
        {
            $this->Log->Warning()->Param('Method not found', $AName);
            $Result = false;
        }
        else $Result = true;
        return $Result;
    }



    public function &StoreConfig($AFileName)
    {
        $Config = new TConfig();
        $Config->SetFile($AFileName);
        $Config->GetParams()->Type = $this->FType;
        $Config->GetParams()->Login = $this->FLogin;
        $Config->GetParams()->Password = $this->FPassword;
        $Config->GetParams()->Host = $this->FHost;
        $Config->GetParams()->Database = $this->FDatabase;
        $Config->Flush();
        return $this;
    }



    public function &RestoreConfig($AFileName)
    {
        $Config = new TConfig();
        $Params = $Config->SetFile($FileName)->Read()->GetParams();

        $this->FLogin = $Params->Login;
        $this->FPassword  = $Params->Password;
        $this->FHost = $Params->Host;
        $this->FDatabase = $Params->Database;
        /* Write JSON to file */
        if (file_put_contents($AFileName, json_encode($JSON))) $this->SetResult(rcOk,'');
        else
        {
            $this->SetResult( 'ErrorStoreDatabaseConfig', $AFileName );
            $this->Log->Warning()->Text( $this->GetResult() )->Param('File',$AFileName);
        }
        /* Return result */
        return $this;
    }



    public function &DeleteConfig( $AFileName )
    {
        /*Delete config file*/
        if( file_exists( $AFileName )) unlink($AFileName);
        if( !file_exists( $AFileName ))
        {
            $this -> Log -> Debug() -> Text( 'Delete database config' ) -> Param( 'File', $AFileName );
        }
        else
        {
            $this->Log->Warning()->Text( $this -> GetResult() )->Param('File',$AFileName);
            $this->SetResult('ErrorDeleteConfig', $AFileName);
        }
        return $this;
    }



    /*
        Return Binary data as string
        This must override for specific datasources type
    */
    private function BinaryToString( $ABinary )
    {
        return bin2hex( $ABinary );
    }



    /**************************************************************************
        Database import export
    */

    /*
    */
    public function DatabaseObjectTablesSelect()
    {
        if ($this->CheckOpened() && $this -> CheckMethod( 'DatabaseObjectTablesSelecting' ))
        {
            $this->Log->Begin();
            $this->DatabaseObjectTablesSelecting();
            $this->Log->End();
        }
    }


    public function DatabaseObjectFieldsSelect($ATableName)
    {
        if ($this->CheckOpened() && $this -> CheckMethod( 'DatabaseObjectFieldsSelecting' ))
        {
            $this->Log->Begin();
            $this->DatabaseObjectFieldsSelecting($ATableName);
            $this->Log->End();
        }
        return $this;
    }

    public function DatabaseObjectFunctionsSelect()
    {
        if ($this->CheckOpened() && $this -> CheckMethod( 'DatabaseObjectFunctionsSelecting' ))
        {
            $this->Log->Begin();
            $this->DatabaseObjectFunctionsSelecting();
            $this->Log->End();
        }
    }

    public function DatabaseObjectIndexSelect()
    {
        $this -> Log -> Debug( 'DatabaseObjectIndexSelect' );
        if( $this -> CheckOpened() && $this -> CheckMethod( 'DatabaseObjectIndexSelecting' ))
        {
            $this -> Log -> Begin();
            $this -> DatabaseObjectIndexSelecting();
            $this -> Log -> End();
        }
    }



    public function DatabaseObjectFunctionsCodeLoad( $AFunctionName )
    {
        if ($this->CheckOpened() && $this -> CheckMethod( 'DatabaseObjectFunctionsCodeLoading' ))
        {
            $this -> Log -> Begin();
            $this -> DatabaseObjectFunctionsCodeLoading( $AFunctionName );
            $this -> Log -> End();
        }
    }



    public function DatabaseObjectProceduresSelect()
    {
        if ($this->CheckOpened() && $this -> CheckMethod( 'DatabaseObjectProceduresSelecting' ))
        {
            $this->Log->Begin();
            $this->DatabaseObjectProceduresSelecting();
            $this->Log->End();
        }
    }



    public function DatabaseObjectProcedureCodeLoad( $AProcedureName )
    {
        if ($this->CheckOpened() && $this -> CheckMethod( 'DatabaseObjectProcedureCodeLoading' ))
        {
            $this -> Log -> Begin();
            $this -> DatabaseObjectProcedureCodeLoading( $AProcedureName );
            $this -> Log -> End();
        }
    }



    public function DatabaseObjectTriggersSelect()
    {
        if ($this->CheckOpened() && $this -> CheckMethod( 'DatabaseObjectTriggersSelecting' ))
        {
            $this -> Log -> Begin();
            $this -> DatabaseObjectTriggersSelecting();
            $this -> Log -> End();
        }
    }



    public function DatabaseObjectTriggerCodeLoad( $ATriggerName )
    {
        if ($this->CheckOpened() && $this -> CheckMethod( 'DatabaseObjectTriggerCodeLoading' ))
        {
            $this->Log->Begin();
            $this->DatabaseObjectTriggerCodeLoading($ATriggerName);
            $this->Log->End();
        }
    }



    public function ExecuteSource( $ACommand )
    {
        if ($ACommand -> IsOk()  && $this -> CheckMethod( 'OnExecuteSource' ) )
        {
            $this->OnExecuteSource($ACommand -> GetSource() -> GetContent());
        }
        return $this;
    }



    /**************************************************************************
        SQL transactions
    */

    /*
        Start transaction
    */
    public function &TranStart()
    {
        if( $this -> CheckOpened() && $this -> CheckMethod( 'TranStarting' ))
        {
            if( $this -> CountOpenTran == 0 )
            {
                $this -> TranStarting();
            }
            $this -> CountOpenTran++;
        }
        return $this;
    }



    /*
        Commit transaction
    */
    public function &TranCommit()
    {
        if( $this->CheckOpened() && $this -> CheckMethod( 'TranCommiting' ))
        {
            $this -> CountOpenTran--;
            if ($this -> CountOpenTran == 0)
            {
                $this -> TranCommiting();
            }
        }
        return $this;
    }


    /*
        Rollback transaction
    */
    public function &TranRollback()
    {
        if( $this -> CheckOpened() && $this -> CheckMethod( 'TranRollbacking' ))
        {
            $this -> TranRollbacking();
            $this -> CountOpenTran = 0;
        }
        return $this;
    }



    /*
        Rollback transaction
    */
    public function &TestTransact()
    {
        if( $this->CheckOpened() && $this -> CheckMethod( 'TestTransaction' ))
        {
            $this -> TestTransaction();
        }
        return $this;
    }



    /*
        Override TResult.ResultTo to return instead of TData.
    */
    public function &ResultTo( &$ATarget )
    {
        parent::ResultTo( $ATarget );
        return $this;
    }



    public function ProcUpload( $AName, $AFile )
    {
        if( $this -> CheckOpened() && $this -> CheckMethod( 'ProcUploading' ))
        {
            $this -> Log -> Begin();
            if( file_exists( $AFile ))
            {
                $Content = \file_get_contents( $AFile );
                $this -> ProcUploading( $AName, $Content );
            }
            else
            {
                $this -> SetResult( 'FileNotFound', null, [ 'AFile' => $File ]);
            }
            $this -> Log -> End();
        }
        return $this;
    }



    public function ProcUploadByContent( $AName, $AContent )
    {
        if( $this -> CheckOpened() && $this -> CheckMethod( 'ProcUploading' ))
        {
            $this -> Log -> Begin();
            $this -> ProcUploading( $AName, $AContent );
            $this -> Log -> End();
        }
        return $this;
    }



    public function FuncUpload
    (
        $AName, /* Name of SQL function */
        $AFile  /* File with source code */
    )
    {
        if( $this -> CheckOpened() && $this -> CheckMethod( 'FuncUploading' ))
        {
            $this -> Log -> Begin();
            if( file_exists( $AFile ))
            {
                $Content = \file_get_contents( $AFile );
                $this -> FuncUploading( $AName, $Content );
            }
            else
            {
                $this -> SetResult( 'FileNotFound', null, [ 'File' => $AFile ]);
            }
            $this -> Log -> End();
        }
        return $this;
    }

}



