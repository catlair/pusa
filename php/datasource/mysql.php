<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

namespace catlair;



require_once ROOT."/datasource/data.php";
require_once ROOT."/datasource/sql.php";



class TMySQL extends TSQL
{
    const PREP_NAME_QUOTE = '`';
    const VALUE_QUOTE = '"';
    const PREP_NAME_ALWAYS = false;


    /*
        Counstructor for MYSQL
    */
    function __construct( $ALog )
    {
        parent::__construct($ALog);
        $this -> SetType( TData :: MYSQL );
    }



    /*
        Convert native (specific) values in to universal values.
        Last using in other php units.
    */
    public function NativeToUni( $ANative )
    {
        switch ($ANative)
        {
            case 'BOOLEAN'          : $Result = TData::TYPE_BOOLEAN; break;

            case 'TINYINT'          : $Result = TData::TYPE_INT_8_SIGN; break;
            case 'SMALLINT'         : $Result = TData::TYPE_INT_16_SIGN; break;
            case 'MEDIUMINT'        : $Result = TData::TYPE_INT_24_SIGN; break;
            case 'INT'              : $Result = TData::TYPE_INT_32_SIGN; break;
            case 'BIGINT'           : $Result = TData::TYPE_INT_64_SIGN; break;

            case 'DATE'             : $Result = TData::TYPE_DATE; break;
            case 'TIME'             : $Result = TData::TYPE_TIME; break;
            case 'DATETIME'         : $Result = TData::TYPE_MOMENT; break;

            case 'CHAR'             : $Result = TData::TYPE_STRING; break;
            case 'VARCHAR'          : $Result = TData::TYPE_STRING_UNSIZE; break;

            case 'BINARY'           : $Result = TData::TYPE_BIN; break;
            case 'LONGTEXT'         : $Result = TData::TYPE_TEXT; break;
            case 'VARBINARY'        : $Result = TData::TYPE_BIN_UNSIZE; break;
            case 'LONGBLOB'         : $Result = TData::TYPE_BLOB; break;
            case 'JSON'             : $Result = TData::TYPE_JSON; break;

            case 'FLOAT'            : $Result = TData::TYPE_FLOAT; break;
            case 'DOUBLE'           : $Result = TData::TYPE_DOUBLE; break;

            case 'PRIMARY KEY'      : $Result = TData::KEY_PRIMARY; break;
            case 'UNIQUE KEY'       : $Result = TData::KEY_UNIQUE; break;

            case 'DESC'             : $Result = TData::ZA; break;
            case 'ASC'              : $Result = TData::AZ; break;

            case 'NOT NULL'         : $Result = TData::NOT_EMPTY; break;
            case 'AUTO_INCREMENT'   : $Result = TData::AUTO_INCREMENT; break;
            case 'UTF8MB4'          : $Result = TData::CODEPAGE_UTF8; break;

            default:            $Result = TData::UNKNOWN; break;
        }
        return $Result;
    }



    /*
        Conver universal TData names to native names
    */
    public function UniToNative( $AUni )
    {
        switch( $AUni )
        {
            case TData::TYPE_BOOLEAN:       $Result = 'BOOLEAN'; break;

            case TData::TYPE_INT_8_SIGN:    $Result = 'TINYINT'; break;
            case TData::TYPE_INT_16_SIGN:   $Result = 'SMALLINT'; break;
            case TData::TYPE_INT_24_SIGN:   $Result = 'MEDIUMINT'; break;
            case TData::TYPE_INT_32_SIGN:   $Result = 'INT'; break;
            case TData::TYPE_INT_64_SIGN:   $Result = 'BIGINT'; break;

            case TData::TYPE_DATE:          $Result = 'DATE'; break;
            case TData::TYPE_TIME:          $Result = 'TIME'; break;
            case TData::TYPE_MOMENT:        $Result = 'DATETIME'; break;

            case TData::TYPE_STRING:        $Result = 'CHAR'; break;
            case TData::TYPE_STRING_UNSIZE: $Result = 'VARCHAR'; break;

            case TData::TYPE_TEXT           : $Result = 'LONGTEXT'; break;
            case TData::TYPE_BIN            : $Result = 'BINARY'; break;
            case TData::TYPE_BIN_UNSIZE     : $Result = 'VARBINARY'; break;
            case TData::TYPE_BLOB           : $Result = 'LONGBLOB'; break;
            case TData::TYPE_JSON           : $Result = 'JSON'; break;

            case TData::TYPE_FLOAT          : $Result = 'FLOAT'; break;
            case TData::TYPE_DOUBLE         : $Result = 'DOUBLE'; break;

            case TData::KEY_PRIMARY         : $Result = 'PRIMARY KEY'; break;
            case TData::KEY_UNIQUE          : $Result = 'UNIQUE KEY'; break;

            case TData::ZA                  : $Result = 'DESC'; break;
            case TData::AZ                  : $Result = 'ASC'; break;

            case TData::NOT_EMPTY           : $Result = 'NOT NULL'; break;
            case TData::AUTO_INCREMENT      : $Result = 'AUTO_INCREMENT'; break;
            case TData::CODEPAGE_UTF8       : $Result = 'UTF8MB4'; break;


            default:                    $Result = $AUni; break;
        }
        return $Result;
    }



    public function Opening
    (
        $ADatabaseOpen,
        int $AReadTimeout = 60
    )
    {
        try
        {
            /*
                All SQL errors will be create exeptions.
                Its will be catch in try section.
                MySQL create warnings without following command and PHP drop fatal error.
            */
            mysqli_report( MYSQLI_REPORT_STRICT);

            $Handle = mysqli_init();

            /* Connection timeout at seconds */
            mysqli_options( $Handle, MYSQLI_OPT_CONNECT_TIMEOUT, 1 );

            /* The maxumum request execute timeout at seconds */
            mysqli_options( $Handle, MYSQLI_OPT_READ_TIMEOUT, 86400 /* $AReadTimeout */ );

            /* Connect to MySQL */
            if
            (
                mysqli_real_connect
                (
                    $Handle,
                    $this -> GetHost(),
                    $this -> GetLogin(),
                    $this -> GetPassword(),
                    $ADatabaseOpen ? $this -> GetDatabase() : null,
                    $this -> GetPort( 3306 )
                )
            )
            {
                mysqli_set_charset( $Handle, 'utf8mb4' );
                $this -> SetHandle( $Handle );
                $this -> SetResult( rcOk, '' );
            }
            else
            {
                $this -> SetResult( mysqli_connect_errno(), mysqli_connect_error());
            }
        }
        catch( \Exception $Error )
        {
            $this -> SetHandle( false );
            $this -> SetResult( 'MySQLOpeningError', $Error -> getMessage() );
        }

        return !empty( $this -> FHandle );
    }



    /*
        Disconnect from mysql
    */
    public function Closing()
    {
        $this -> Log -> Begin();

        try
        {
            $this -> FHandle = !mysqli_close($this->FHandle);
        }
        catch( \Exception $Error )
        {
            $Handle = false;
            $this -> SetResult( 'MySQLOpeningError', $Error -> getMessage() );
        }

        $this -> Log -> End();

        return $this->FHandle === false;
    }

    public function GetDefaultDatabaseName()
    {
        return 'mysql';
    }


    /*
        Check connection
    */
    public function Pinging()
    {
        $Result = false;
        try
        {
            $Result = mysqli_ping( $this->FHandle );
        }
        catch( \Exception $Error )
        {
            $this -> SetResult( 'MySQLOpeningError', $Error -> getMessage() );
        }
        return $Result;
    }



    /**
        Return true if database $ADatabase:string exists
    */
    public function DatabaseExisting( $ADatabase )
    {
        $this -> GetCommand() -> SetContent( 'show databases like "' . $ADatabase . '"' );
        $this -> Calling();

        return
        $this->Result!=null &&
        count( $this->Result ) > 0 &&
        count( $this->Result[0] ) > 0;
    }




    /*
        Database $ADatabase:string create for mysql.
        Calling from TDatasource->DatabaseCreate.
        Return $this.
    */
    public function DatabaseCreating( $ADatabase, $AParams )
    {
        $Cmd = 'create database ' . $this -> PrepName( $ADatabase );
        if ($AParams!==null)
        {
            if (array_key_exists('Codepage', $AParams)) $Cmd .= ' default charset '. $this -> UniToNative( $AParams['Codepage'] ) . ' ';
            if (array_key_exists('Charset', $AParams)) $Cmd .= ' character set '.$AParams['Charset'] . ' ';
            if (array_key_exists('Collate', $AParams)) $Cmd .= ' collate '.$AParams['Collate'] . ' ';
        }
        $this->GetCommand() -> SetContent($Cmd);
        $this->Calling();
        return $this;
    }



    /*
        Database $ADatabase:string deleting for mysql.
        Calling from TDatasource->DatabaseDelete.
        Return $this.
    */
    public function DatabaseDeleting( $ADatabase )
    {
        $this->GetCommand() -> SetContent('drop database '. $this -> PrepName( $ADatabase ));
        $this->Calling();
        return $this;
    }



    /*
        Selet database $ADatabase:string
    */
    public function DatabaseSelecting( $ADatabase )
    {
        $this -> GetCommand() -> SetContent( 'use ' . $this -> PrepName( $ADatabase ));
        $this -> Calling();
        return $this;
    }



    /*
        Lock database
    */
    public function DatabaseLocking()
    {
        $this -> GetCommand() -> SetContent( 'FLUSH TABLES WITH READ LOCK' );
        $this -> Calling();
        return $this;
    }



    /*
        Unlock database
    */
    public function DatabaseUnlocking()
    {
        $this -> GetCommand() -> SetContent('UNLOCK TABLES');
        $this -> Calling();
        return $this;
    }



    /*
    Creating database administrator $ADatabase:string
    */
    public function DatabaseAdminCreating( $ALogin, $APassword )
    {
        $FullLogin = '"'.$ALogin.'"@"'.$this->GetHost().'"';
        /* Create new user */
        $this->GetCommand() -> SetContent('create user '.$FullLogin.' identified by "'.$APassword.'"');
        $this->Calling();
        /* Gift all priveleges for user */
        $this->GetCommand() -> SetContent('GRANT CREATE,DROP,DELETE,INSERT,SELECT,UPDATE ON '.$this->GetDatabase().'.* TO '.$FullLogin);
        $this->Calling();
        $this->GetCommand() -> SetContent('FLUSH PRIVILEGES');
        $this->Calling();
        return $this;
    }



    /**************************************************************************
        Tables
    */


    /*
    Return true if table $AName:string exists
    */
    public function TableExisting( $AName )
    {
        $this->GetCommand() -> SetContent('show tables like "'.$AName.'"');
        $this->Calling();
        return $this->Result!=null && count($this->Result)>0 && count($this -> Result[0])>0;
    }



    /*
        Table creating
        $AFields - array
        $AIndexes - array
    */
    public function TableCreating
    (
        array $AParams  = []
    )
    {
        $Table = clValueFromObject( $AParams, 'Name' );

        if( $Table)
        {
            $FieldsList = [];
            $Fields = clValueFromObject( $AParams, 'Fields', [] );
            foreach( $Fields as $Field )
            {
                array_push( $FieldsList, $this -> CmdFieldCreate( $Field ));
            }

            /* Buil indexes command */
            $IndexList = [];
            $Indexes = clValueFromObject( $AParams, 'Indexes', [] );
            if( $Indexes != null )
            {
                foreach( $Indexes as $IndexFields )
                {
                    $FieldQuotes = [];
                    foreach( $IndexFields as $Field )
                    {
                        array_push( $FieldQuotes, $this -> PrepName( $Field) );
                    }

                    array_push
                    (
                        $IndexList,
                        ', INDEX ' .
                        $this -> PrepName( 'idx_' . implode( '_', $IndexFields )) .
                        ' (' . implode( ', ',  $FieldQuotes ) . ')'
                    );
                }
            }

            $this -> GetCommand() -> SetContent
            (
                'CREATE TABLE ' . $this -> PrepName( $Table )
                . ' (' . implode( ', ', $FieldsList ) . implode( '', $IndexList ) . ')'
            );
            $this -> Calling();
        }
        else
        {
            $this -> SetResult( 'NoTableName', null, [ 'Params' => $AParams ] );
        }
        return $this;
    }



    /*
        Table creating
    */
    public function &FieldCreating
    (
        $ATable,
        $AField
    )
    {
        $Cmd = 'ALTER TABLE ' . $this -> PrepName( $ATable ) . ' ADD ' . $this -> CmdFieldCreate( $AField );
        $this -> GetCommand() -> SetContent( $Cmd );
        $this -> Calling();
        return $this;
    }



    /*
        Table creating
    */
    public function FieldRenaming
    (
        $ATableName, $AFieldOldName, $AFieldNewName
    )
    {
        $Cmd = 'ALTER TABLE ' . $this -> PrepName( $ATable ) . ' RENAME COLUMN ' .  $this -> PrepName( $AFieldOldName ) . ' TO ' .  $this -> PrepName( $AFieldNewName );
        $this -> GetCommand() -> SetContent( $Cmd );
        $this -> Calling();
        return $this;
    }






    /*
        Table creating
    */
    public function &IndexCreating($ATable, $AType, $AFields, $AName)
    {
        switch ($AType)
        {
            case TData::INDEX_SIMPLE: $AType=''; break;
            case TData::INDEX_FULLTEXT: $AType='FULLTEXT'; break;
        }
        if (empty($AName))
            $AName='fti_' . implode('_', $AFields);
        $Cmd = 'CREATE ' . $AType . ' INDEX ' . $AName . ' ON ' . $ATable  . '(' . implode (', ', $AFields) .')';
        $this -> GetCommand() -> SetContent($Cmd);
        $this -> Calling();
        return $this;
    }


    /*
    */
    public function UserExisting($ALogin, $AHost)
    {
        $this -> GetCommand() -> SetContent( 'select User from mysql.user where User="'.$ALogin.'" and Host="'.$AHost.'"' );
        $this -> Calling();
        return $this->Result!=null && count($this->Result)>0 && count($this->Result[0])>0;
    }



    /*
    */
    public function &UserDeleting($ALogin, $AHost)
    {
        $this -> GetCommand() -> SetContent( 'drop user "' . $ALogin . '"@"'. $AHost .'"' );
        $this -> Calling();
        return $this;
    }



    /*
        Return true if procedure $AName:string exists else return false
    */
    public function ProcedureExisting($AName)
    {
        $DataBase = $this -> GetDatabase();

        $this -> GetCommand() -> SetContent(
            '
            SELECT 
            ROUTINE_NAME 
            FROM INFORMATION_SCHEMA.ROUTINES 
            WHERE ROUTINE_NAME="'.$AName.'" AND ROUTINE_TYPE="PROCEDURE" AND ROUTINE_SCHEMA="'.$DataBase.'"
            '
        );
        $this -> Calling();
        return $this->Result!=null && count($this->Result)>0 && count($this->Result[0])>0;
    }

    /*
    public function &ProcedureDeleting($AName)
    {
        $this -> GetCommand() -> SetContent(
            '
            DROP PROCEDURE IF EXISTS "'.$AName.'"
            '
        );
        $this -> Calling();
        return $this;
    }
    */


    /*
        Return true if function $AName:string exists else return false
    */
    public function FunctionExisting($AName)
    {
        $DataBase = $this -> GetDatabase();

        $this -> GetCommand() -> SetContent(
            '
            SELECT 
            ROUTINE_NAME 
            FROM INFORMATION_SCHEMA.ROUTINES 
            WHERE ROUTINE_NAME="'.$AName.'" AND ROUTINE_TYPE="FUNCTION" AND ROUTINE_SCHEMA="'.$DataBase.'"
            '
        );
        $this -> Calling();
        return $this->Result!=null && count($this->Result)>0 && count($this->Result[0])>0;
    }

    /*
    public function &FunctionDeleting($AName)
    {
        $this -> GetCommand() -> SetContent(
            '
            DROP FUNCTION IF EXISTS "'.$AName.'"
            '
        );
        $this -> Calling();
        return $this;
    }    
    */
    /*
    public function FuncUploading($AName, $AContent)
    {
        if ($this -> FunctionExisting($AName)) {
            $this -> FunctionDeleting($AName);
        }

        $this -> GetCommand() -> SetContent($AContent);
        $this -> Calling();
        return $this;
    }
    */
    /*
    public function ProcUploading($AName, $AContent)
    {
        if ($this -> ProcedureExisting($AName)) {
            $this -> ProcedureDeleting($AName);
        }

        $this -> GetCommand() -> SetContent($AContent);
        $this -> Calling();
        return $this;
    }
    */

    /**************************************************************************
        Record CRUD
    */


    /**/
    protected function IDInsertedGetting()
    {
        return mysqli_insert_id( $this -> FHandle );
    }


    /**************************************************************************
        Call procedure
    */

    /*
        Call stored procedure $AProcName with input paremeters $AValue:array [ "Name"=>"Value" ]
    */
    public function Procing
    (
        $AProcName,
        $AValues
    )
    {
        $Values = [];

        foreach( $AValues as $Value )
        {
            array_push( $Values,    $this -> ValueToParam( $Value, true ));
        }

        $this -> GetCommand()
        -> SetContent( 'CALL %ProcName%( %Values% )' )
        -> SetParam( 'ProcName', $AProcName )
        -> SetParam( 'Values', implode( ', ', $Values ));

        $this -> Calling();
        return $this;
    }



    /*
        Specific Calling action for this Datasource
    */
    public function &Calling( $ACommand = null )
    {
        /* Reset result */
        $this -> Result = null;

        /* Get command */
        $Cmd = empty( $ACommand ) ?  $this -> GetCommand() -> GetContent() : $ACommand -> GetContent();

        $this -> Log
        -> Trace( '' ) -> Param( 'Database', $this->GetDatabase())
        -> Trace() -> Param( 'Command', $Cmd ) -> LineEnd();

        /* Execute query */
        try
        {
            $Result = mysqli_multi_query( $this->FHandle, $Cmd );

            $this -> SetResult
            (
                $this -> GetSpecificResultCode(),
                mysqli_error( $this -> FHandle )
            );

            if( $Result !==null )
            {
                $this -> Result = [];
                do
                {
                    if( $Dataset = mysqli_store_result( $this->FHandle ))
                    {
                        $ds = [];
                        while( $Row = $Dataset -> fetch_object()) array_push( $ds, $Row );
                        array_push( $this -> Result, $ds );
                        $Dataset -> Free();
                    }
                } while( mysqli_more_results( $this -> FHandle ) && mysqli_next_result( $this -> FHandle ));
            }
        }
        catch( Throwable $Error )
        {
            $this -> SetResult( 'MySQLExchangeError', $Error -> getMessage() );
            $this -> Log -> Error( $Error -> getMessage() );
        }

/*
        $this -> Log
        -> Print( $this -> Result, 'Result execute on database' )
        -> LineEnd();
*/
        return $this;
    }





    /**************************************************************************
        Service
    */


    /*
    */
    protected function GetSpecificResultCode()
    {
        $Code = mysqli_errno($this->FHandle);
        if( $Code == '0' ) $Code = rcOk;
        else $Code = 'MySQL-'.$Code;
        return $Code;
    }



    /*
        Create Field params from array
    */
    public function CmdFieldCreate( $AField )
    {
        $Cmd = '';

        $Name = clValueFromObject( $AField, 'Name' );
        $Type = clValueFromObject( $AField, 'Type' );

        if( ! empty( $Name ) && ! empty( $Type ))
        {
            $Cmd = $Name . ' ' . $this -> UniToNative( $Type );
            if( array_key_exists( 'Length',     $AField )) $Cmd .= ' (' . $AField[ 'Length' ] . ')';
            if( array_key_exists( 'NotNull',    $AField ) && $AField[ 'NotNull' ]) $Cmd .= ' ' .  $this -> UniToNative( TData::NOT_EMPTY );
            if( array_key_exists( 'Default',    $AField )) $Cmd .= ' DEFAULT ' . $this -> ValueToParam( $AField[ 'Default' ] );
            if( array_key_exists( 'Increment',  $AField ) && $AField[ 'Increment ' ]) $Cmd .= ' ' . $this -> UniToNative( TData::AUTO_INCREMENT );
            if( array_key_exists( 'Key',        $AField )) $Cmd .= ' ' . $this -> UniToNative( $AField[ 'Key' ]);
        }
        else
        {
            $this -> SetResult( 'NoTypeOrField', null, [ 'Field' => $AField ] );
        }
        return $Cmd;
    }



    public function BinaryToString($ABinary)
    {
        return mysqli_real_escape_string( $this->FHandle, $ABinary );
    }

    protected function EscapeString( $AValue )
    {
        return mysqli_real_escape_string( $this -> FHandle, $AValue );
    }

    
    protected function BuildLimits
    (
        array $ASelect = [],
        string $APrefix =''
    )
    {
        $Result = '';
        if( $this -> IsOk())
        {
            $Limit = clValueFromObject( $ASelect, 'Limit', null );
            if( $Limit !== null )
            {
                $Skip = clValueFromObject( $ASelect, 'Skip' );
                $Skip = empty( $Skip ) ? '' : $Skip . ', ';
                $Result = ( empty( $APrefix ) ? '' : ( $APrefix . ' ' )) . $Skip . $Limit ;
            }
        }
        return $Result;
    }


    /**************************************************************************
        Database import export
    */


    /*
        select databaseobject
    */
    public function &DatabaseObjectTablesSelecting()
    {
        $this
        -> GetCommand()
        -> SetContent
        ('
            SELECT
                TABLE_NAME TableName,
                TABLE_COMMENT TableComment,
                TABLE_COLLATION TableCollation,
                (
                    SELECT CCSA.character_set_name FROM information_schema.`COLLATION_CHARACTER_SET_APPLICABILITY` CCSA
                    WHERE CCSA.collation_name = table_collation
                ) TableCharset
            FROM INFORMATION_SCHEMA.TABLES
            WHERE TABLE_SCHEMA = "%DataBase%"
        ')
        -> SetParam( 'DataBase', $this -> GetDatabase() );

        return $this -> Calling();
    }



    public function &DatabaseObjectFieldsSelecting( $ATableName )
    {
        $this -> GetCommand()
        -> SetContent
        ('
            select
                COLUMN_NAME FieldName,
                ORDINAL_POSITION FieldPosition,
                COLUMN_DEFAULT, IS_NULLABLE,
                DATA_TYPE FieldType,
                CHARACTER_MAXIMUM_LENGTH FieldLength,
                CHARACTER_SET_NAME FieldCharset,
                COLLATION_NAME FieldCollation,
                COLUMN_KEY,
                COLUMN_COMMENT FieldComment
            from
                INFORMATION_SCHEMA.COLUMNS
            where
                TABLE_SCHEMA = "%DataBase%" and TABLE_NAME = "%TableName%"
        ')
        -> SetParam( 'DataBase', $this -> GetDatabase() )
        -> SetParam( 'TableName', $ATableName );

        $this
        -> Calling()
        -> ResultLoop
        (
            function( &$Record )
            {
                $Record -> FieldIsNull      = $Record -> IS_NULLABLE == 'YES' ? true : false;
                $Record -> FieldDefault     = $Record -> COLUMN_DEFAULT == null ? 'NULL' : $Record -> COLUMN_DEFAULT;
                $Record -> FieldKey         = $Record -> COLUMN_KEY == 'PRI' ? 'PrimaryKey' : $Record -> COLUMN_KEY;
            }
        );

        return $this;
    }



    /*
        Execute source
    */
    public function &OnExecuteSource( $ASource )
    {
        $this -> GetCommand()
        -> Clear()
        -> SetContent ($ASource)
        ;
        $this -> Calling();
        return $this;
    }



    /*
    */
    public function &DatabaseObjectFunctionsCodeLoading ( $AFunctionName )
    {
        $this -> GetCommand()
        -> SetContent
        ('
            SHOW CREATE FUNCTION %DataBase%.%FunctionName%
        ')
        -> SetParam( 'DataBase', $this -> GetDatabase() )
        -> SetParam( 'FunctionName', $AFunctionName );

        $this
        -> Calling();

        $this -> ResultLoop
        (
            function( &$Record, $th )
            {
                $Prm = 'Create Function';
                $Record -> SourceCode = $Record -> $Prm;
            }
        );
        return $this;
    }



    /*
        select databaseobject
    */
    public function &DatabaseObjectFunctionsSelecting()
    {
        $this
        -> GetCommand()
        -> SetContent
        ('
            SELECT
                ROUTINE_NAME FunctionName
            FROM INFORMATION_SCHEMA.ROUTINES
            WHERE ROUTINE_SCHEMA = "%DataBase%" and ROUTINE_TYPE="FUNCTION"
        ')
        -> SetParam( 'DataBase', $this -> GetDatabase() );

        $this -> Calling();

        return $this;
    }



    /*
        select databaseobject
    */
    public function &DatabaseObjectIndexSelecting()
    {
        $this -> Log -> Debug( 'DatabaseObjectIndexSelecting' );
        $this -> GetCommand()
        -> SetContent
        ('
            SELECT
                TABLE_NAME IndexTableName,
                NON_UNIQUE IndexUnique,
                INDEX_NAME IndexName,
                SEQ_IN_INDEX IndexRowOrder,
                COLUMN_NAME IndexColumnName,
                COLLATION IndexCollation,
                NULLABLE IndexNullable,
                INDEX_TYPE IndexType,
                IS_VISIBLE IndexVisible,
                COMMENT IndexComment
            FROM INFORMATION_SCHEMA.STATISTICS
            WHERE TABLE_SCHEMA = "%DataBase%"
            ORDER BY TABLE_NAME, INDEX_NAME, SEQ_IN_INDEX
        ')
        -> SetParam( 'DataBase', $this -> GetDatabase() );

        $this -> Calling();

        return $this;
    }



    /*
    */
    public function &DatabaseObjectProcedureCodeLoading( $AProcedureName )
    {
        $this -> GetCommand()
        -> SetContent
        ('
            SHOW CREATE PROCEDURE %DataBase%.%ProcedureName%
        ')
        -> SetParam( 'DataBase', $this -> GetDatabase() )
        -> SetParam( 'ProcedureName', $AProcedureName );

        $this
        -> Calling();

        $this -> ResultLoop
        (
            function( &$Record, $th )
            {
                $Prm = 'Create Procedure';
                $Record -> SourceCode = $Record -> $Prm;
            }
        );
        return $this;
    }



    /*
        select databaseobject
    */
    public function &DatabaseObjectProceduresSelecting()
    {
        $this -> GetCommand()
        -> SetContent
        ('
            SELECT
                ROUTINE_NAME ProcedureName
            FROM INFORMATION_SCHEMA.ROUTINES
            WHERE ROUTINE_SCHEMA = "%DataBase%" and ROUTINE_TYPE="PROCEDURE"
        ')
        -> SetParam( 'DataBase', $this -> GetDatabase() );

        $this -> Calling();

        return $this;
    }



    public function &DatabaseObjectTriggerCodeLoading ( $ATriggerName )
    {
        $this -> GetCommand()
        -> SetContent
        ('
            SHOW CREATE TRIGGER %DataBase%.%TriggerName%
        ')
        -> SetParam( 'DataBase', $this -> GetDatabase() )
        -> SetParam( 'TriggerName', $ATriggerName );

        $this
        -> Calling();

        $this -> ResultLoop
        (
            function( &$Record, $th )
            {
                $Prm = 'SQL Original Statement';
                $Record -> SourceCode = $Record -> $Prm;
            }
        );
        return $this;
    }



    /*
        select databaseobject
    */
    public function &DatabaseObjectTriggersSelecting()
    {
        $this -> GetCommand()
        -> SetContent
        ('
            SELECT
                TRIGGER_NAME TriggerName
            FROM INFORMATION_SCHEMA.TRIGGERS
            WHERE TRIGGER_SCHEMA = "%DataBase%"
        ')
        -> SetParam( 'DataBase', $this -> GetDatabase() );

        $this -> Calling();

        return $this;
    }



    /*
        START Transaction
    */
    public function &TranStarting()
    {

        mysqli_begin_transaction($this->FHandle);
        return $this;
    }



    /*
        Commit Transaction
    */
    public function &TranCommiting()
    {
        mysqli_commit( $this -> FHandle );
        return $this;
    }



    /*
        Rollback Transaction
    */
    public function &TranRollbacking()
    {
        mysqli_rollback($this->FHandle);
        return $this;
    }



    /*
        Test Transaction
    */
    public function &TestTransaction()
    {
        mysqli_begin_transaction( $this->FHandle );
        $Result = mysqli_multi_query( $this->FHandle, 'CALL _TestTran("test php");' );
        mysqli_commit($this->FHandle);
        return $this;
    }



    /*
        Build full text conditions from array
    */
    public function BuildFullText
    (
        $AArg1,
        $AArg2
    )
    {
        return 'MATCH (' . $AArg1 . ' ) AGAINST ( ' . $AArg2 . ' IN BOOLEAN MODE )';
    }

}
