<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

/*
    Catlair.
    The webserver should start this file.
    12.01.2019 still@itserv.ru
*/

namespace catlair;

define ( 'PROJECT_ROOT',    $_SERVER[ 'CATLAIR_PATH' ] );
define ( 'ROOT',            PROJECT_ROOT . '/php' );

include_once( ROOT . '/web/web.php' );

/*
    Start default web engine
*/
TWeb::Create()
-> Start        ()
-> Begin        ( 'Main' )      /* Begin log */
-> Run          ()              /* Run */
-> End          ()              /* End log loop*/
-> TraceTotal   ()              /* Out trace information */
-> StatisticOut ()              /* Out request statistics */
-> LastMessages ()              /* Out last error and warning messages */
-> Stop         ();             /* Stop the logger */
