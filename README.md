# Pusa-PHP Web без JS-Front

[Pusa concept](site/pusa/src/language_en/man/pusa.md) shifts all development to the Backend,
and uses pure REST. No JS development needed. Browser control, DOM, business logic work on the
Back-end without the need to store client state on the server.

## Fast start

You can create a new site on the Pusa engine in the following ways:

1. For acquaintance using [Docker container](https://hub.docker.com/repository/docker/catlairnet/pusa_ubutu_nginx_php_ssh);
2. For real tasks set Linux+Nginx+PHP.


## Step by step

1. [Preparation](#preparation)
0. [Runing a container](#runing a container)
0. [Authorisation](#authorization)
0. [Site folder](#site-folder)
0. [Creating a content](#creating-a-content)
0. [Creating a controller](#creating-a-controller)
0. [Creating a domain](#creating-a-domain)
0. [File permissions](#file-permissions)
0. [Checking results](#checking-results)



### Preparation

We need an OS (Ubuntu, Debian, Windows) and [Docker](http://docker.com)
on localhost to run a new Pusa site. Port 80 should bee free from other applications.
Installation supposes only work in console. For example install docker in ubuntu
with following command:
```
sudo apt install docker docker.io
```



### Runing a container

Upload and run [Pusa container](https://hub.docker.com/r/catlairnet/pusa_ubutu_nginx_php_ssh):
```
sudo docker run -d -p 80:80 -p 22022:22022 catlairnet/pusa_ubutu_nginx_php_ssh:latest
```
**Result**: Once completed, you can open following links in you local browser:
- http://localhost
- http://127.0.0.1

The container is not removed after the stop command **docker stop [ID]**, and can be started again
in its previous state.

### Authorization in container

Connect to container via ssh on port 22022. Follown the ssh recomendations.
```
ssh -p 22022 root@127.0.0.1
```
Password
```
root
```
You need to input the previous password **root** once and the new password twice.

**In th future we will work in the container.**



### Site folder
Pusa is located in **/var/www/html/catlair/site** folder, we call it like **[ROOT]**.

Each a Pusa site has a home directory **[ROOT]/site** for storing files.
Let name of the new site **my_new_site** and it will be hosted in **[ROOT]/site/my_new_site** folder.



### Creating a content

The [Multilanguage text contnet](/site/site_default/src/language_default/man/multylanguage.md)
located in **[ROOT]/site/my_new_site/src** folder.
Pusa looks for content in language folders **language_en**, **language_fr** etc
but if content does not exists, then in the **language_default** folder as default.
Let's create this:
```
mkdir -p /var/www/html/catlair/site/my_new_site/src/language_default
```
and put the new content with nano to:
```
nano /var/www/html/catlair/site/my_new_site/src/language_default/Main.html
```
```
<button type="button" class="Button">
    Click me
</button>
```



### Creating a controller

All PHP controllers for Pusa application are located at **[ROOT]/site/my_new_site/php/pusa**.
Let's create it:
```
mkdir -p /var/www/html/catlair/site/my_new_site/php/pusa
```
Add new controller to **Main.php** file:
```
nano /var/www/html/catlair/site/my_new_site/php/pusa/Main.php
```

```
<?php
namespace catlair;

class Main extends TWebPusa
{
    /* Page init */
    public function Init()
    {
        return $this
        -> FocusBody()
        -> DOMTemplate( 'Main.html')
        -> FocusChildren( 'Button' )
        -> Event( 'Main', 'ButtonClick' );
    }

    /* The button click callback body */
    public function ButtonClick()
    {
        return $this
        -> FocusBody()
        -> DOMContent( 'Hello World' );
    }
}
```
The controller has been created.



### Creating a domain

Pusa [Domain configuration files](site/site_default/src/language_en/man/domain_config.md)
containes in **[ROOT]/domain** folder.
The file name is the same the domain name.
Each file contains the configuration for one domain in json format.
Example: settings for **localhost** domain are contained in the **[ROOT]/domain/localhost** file.

Let's configure **localhost** domain to our new site **my_new_site**.

1. For sites, you need an individual encryption key for user sessions.
It is contained in the **SSLKey**. You can create a new encryption key with the CLI command:
```
php -r 'echo base64_encode(openssl_random_pseudo_bytes( 32 )).PHP_EOL;'
```
The result will be a string, and it must be put into the **SSLKey**:
```
2ByRiqZLal11xBuvTFPHfQcro/g5jwLc1Yjvc0arSmU=
```

2. The **IDSite** key indicates the site being used. The value must be **my_new_site**.

3. Change the file:
```
nano /var/www/html/catlair/domain/localhost
```
```
{
    "IDSite" : "my_new_site",
    [...]
    "SSLKey" : "2ByRiqZLal11xBuvTFPHfQcro/g5jwLc1Yjvc0arSmU=",
    [...]
```


### File permissions

The nginx server runs as **www-data**. It is necessary to give him the opportunity
reading and writing to the **[ROOT]/site/my_new_site** directory. Run the command:
```
chown -R www-data:www-data /var/www/html/catlair/site/my_new_site/
```
This will set the owner and group **www-data** for the folder and its descendants.
You can adjust the rights according to your security requirements later.



### Checking result

The link http://localhost will return a page with one button on the Pusa engine.
Press the button and content will be changed to "Hello. It is works without JS.",
without page reloading.

That's right, you haven't used JS.



## Links

- [Ready dock container](https://hub.docker.com/r/catlairnet/pusa_ubutu_nginx_php_ssh)
- [Definitions](main/site/pusa/src/language_en/man/glossary.md)
- [Pusa technology](site/pusa/src/language_en/man/pusa.md)
- [Pusa protocol](site/pusa/src/language_en/man/pusa_protocol.md)
- [Catlair (web library)](site/site_default/src/language_default/man/catlair.md)
- [Implementation features](site/pusa/src/language_default/man/pusa_php_specifics.md)
- [PHP Pusa methods](site/pusa/src/language_default/man/pusa_php_methods.md)
- [Team and greetings](site/pusa/src/language_default/man/pusa_team.md)
- [Site pusa.dev](https://pusa.dev)
