<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

namespace catlair;

trait TClipboard
{
    public function InitClipboard()
    {
        return $this
        -> FocusBody()
        -> FocusChildren( 'BtnCopyValue' )
        -> EventFront                               /* Event for front. Following commands will work on front only */
        (
            $this -> Clone()                        /* Create new Pusa for commands buffer. */
            -> FocusBody()                            /* Select body object */
            -> FocusChildren( 'SourceTextarea' )        /* Select textarea */
            -> DOMValueToClipboard()                /* Move value to clipboard */
            -> FocusBody()                            /* Select body */
            -> FocusChildren( 'DestTextarea' )          /* Select result example */
            -> DOMShow()                            /* Show result element */
            -> DOMValue()                           /* Clear result element */
        );
    }
}
