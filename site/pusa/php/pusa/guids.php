<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

namespace catlair;


/*
    Using in TPusa or TPusa childrens.
*/
trait TGUIDs
{
    /*
        Initialize GUIDs list
    */
    public function InitGUIDs()
    {
        return $this
        -> FocusBody()
        -> FocusChildren( 'GUIDs' )
        -> FocusChildren( 'List' )          /* Focus on List of guids */
        -> Event                            /* Set event on List */
        (
            'Main',                         /* In Main class controller */
            'LoadGUIDs',                    /* Will be a call the LoadGUIDs method */
            $this -> Clone()                /* Get state of scrolling */
            -> ReadProperty( 'AScrollTop', 'scrollTop' )
            -> ReadProperty( 'AScrollHeight', 'scrollHeight' )
            -> ReadProperty( 'AClientHeight', 'clientHeight' ),
            'scroll',                       /* For event scroll */
            500                             /* With event timeout 200ms before sending*/
        );
    }




    /*
        Load GUIDs endpoint
    */
    public function LoadGUIDs
    (
        int $AScrollTop = 0,
        int $AScrollHeight = 0,
        int $AClientHeight = 0
    )
    {
        if( $AScrollTop + $AClientHeight * 2 > $AScrollHeight )
        {
            $this -> GenerateGUIDs();
        }
        return $this;
    }



    /*
        Generate guids
    */
    public function GenerateGUIDs()
    {
        $GUIDS = [];
        for( $i = 0; $i < 50; $i++ )
        {
            array_push(  $GUIDS, [ 'GUID' => clGUID() ] );
        }

        /* Select the list */
        return $this
        -> FocusBody()
        -> FocusChildren( 'GUIDs' )
        -> FocusChildren( 'List' )
        -> Loop
        (
            $this -> Clone()
            -> DOMCreate()
            -> DOMClass( 'Record' )
            -> DOMContent( '%GUID%' )
            -> FocusParent(),
            $GUIDS
        );

        return $this;
    }
}
