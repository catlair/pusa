<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

namespace catlair;

trait TForm
{
    /*
        Init form demo
    */
    public function FormInit()
    {
        /* Prepare Demo FORM */
        return $this
        -> FocusBody()
        -> FocusChildren( 'FormSend' )
        -> Event
        (
            'Main',
            'FormSend',
            $this -> Clone()
            -> FocusParents( 'DemoForm' )
            -> ReadFields( 'Main', 'FilesUpload' )
        );
    }



    /*
        Scroll up the list of content
    */
    public function FormSend
    (
        string  $IDGroup,

        string  $AString,
        string  $ATextarea,
        string  $ARadio,
        bool    $ACheckbox,
        string  $ASelect,
        string  $AFiles
    )
    {
        /*
            You can read parameters form:
            - argument of method $KeyName
            - $this -> GetValue( 'KeyName' )
            - $_POST[ 'KeyName' ]
        */
        return $this
        -> FormContainer( $IDGroup )        /* Local method */
        -> FocusChildren( 'FormData' )
        -> DOMContent
        (
            'String: '       . $AString . '<br>' .
            'Textarea: '     . $ATextarea . '<br>' .
            'Radio: '        . $ARadio . '<br>' .
            'Checkbox: '     . ( $ACheckbox ? 'true' : 'false' ) . '<br>' .
            'Select: '       . $ASelect . '<br>'
        );
    }



    /*
        Upload files
    */
    public function FilesUpload
    (
        string  $IDGroup    /* Group of file */
    )
    {
        foreach( $_FILES as $File )
        {
            $this
            -> FormContainer( $IDGroup )        /* Local method */
            -> FocusChildren( 'FilesGroup' )
            -> DOMShow()
            -> FocusChildren( 'Files' )
            -> DOMCreate
            (
                TPusaCore::CONTAINER_DEFAULT,
                TPusaCore::FIRST
            )
            -> DOMContent( $File[ 'name' ] );
        }
        return $this;
    }


    private function FormContainer( $AIDGroup )
    {
        return $this
        -> FocusBody()
        -> FocusChildren( 'FormResult' )
        -> FocusChildren( $AIDGroup, TPusaCore::ID )
        -> IfFocusIsEmpty
        (
            $this -> Clone()
            -> FocusBody()
            -> FocusChildren( 'FormResult' )
            -> DOMCreate
            (
                TPusaCore::CONTAINER_DEFAULT,
                TPusaCore::FIRST
            )
            -> DOMID( $AIDGroup )
            -> DOMTemplate( 'ExampleFormItem.html' )
        );
    }
}
