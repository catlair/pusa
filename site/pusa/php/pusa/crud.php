<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

namespace catlair;


trait TCRUD
{
    /*
        Prepare crud FORM
    */
    public function CRUDInit()
    {
        return $this
        /* Add first item */
        -> FocusBody()
        -> FocusChildren( 'CRUDAddFirst' )
        -> Event( 'Main', 'AddFirst' )

        /* Add last item */
        -> FocusBody()
        -> FocusChildren( 'CRUDAddLast' )
        -> Event( 'Main', 'AddLast' )

        /* Scroll on top */
        -> FocusBody()
        -> FocusChildren( 'CRUDScrollTop' )
        -> Event( 'Main', 'ScrollTop' )

        /* Scroll on bottom */
        -> FocusBody()
        -> FocusChildren( 'CRUDScrollBottom' )
        -> Event
        (
            'Main',
            'ScrollBottom',
            $this -> Clone()
            -> FocusBody()
            -> FocusChildren( 'CRUDContainer' )
            -> FocusChildren( 'List' )
            -> ReadProperty( 'AHeight', 'scrollHeight')
        )

        /* Clear action */
        -> FocusBody()
        -> FocusChildren( 'CRUDClear' )
        -> Event( 'Main', 'CRUDClear' )
        ;
    }



    /*
        Clear the CRUD container element
    */
    public function CRUDClear()
    {
        return $this
        -> FocusBody()
        -> FocusChildren( 'CRUDContainer' ) /* Focus on container */
        -> FocusChildren( 'List' )          /* Select the inner List element */
        -> FocusChildren( 'Record' )        /* Select all recods elements */
        -> DOMDelete( );                /* .. and delete them */
    }



    /*
        Add first element
    */
    public function AddFirst()
    {
        return $this

        -> FocusBody()
        -> FocusChildren( 'CRUDContainer' )
        -> FocusChildren( 'List' )
        -> DOMCreate( 'div', TPusaCore::FIRST )
        -> DOMClass( 'Record First' )
        -> RecordContent()
        ;
    }



    /*
        Add last element
    */
    public function AddLast()
    {
        return $this
        -> FocusBody()
        -> FocusChildren( 'CRUDContainer' )
        -> FocusChildren( 'List' )
        -> DOMCreate( 'div', TPusaCore::LAST )
        -> DOMClass( 'Record Last' )
        -> RecordContent()
        ;
    }


    /*
        Add the element before current
    */
    public function AddBefore()
    {
        return $this
        -> FocusParents( 'Record', TPusa::CLASS_NAME )
        -> DOMCreate( 'div', TPusaCore::BEFORE )
        -> DOMClass( 'Record Before' )
        -> RecordContent();
    }



    /*
        Add the element after current
    */
    public function AddAfter()
    {
        return $this
        -> FocusParents( 'Record', TPusa::CLASS_NAME )
        -> DOMCreate( 'div', TPusaCore::AFTER )
        -> DOMClass( 'Record After' )
        -> RecordContent();
    }



    /*
        Delete the current element from list
    */
    public function Delete()
    {
        return $this
        -> FocusParents( 'Record', TPusa::CLASS_NAME )
        -> DOMDelete();
    }



    /*
        Set record content
    */
    private function RecordContent()
    {
        return $this
        -> DOMContent( $this -> Web -> GetTemplate( 'CRUDRecord.html' ) )
        -> FocusChildren( 'Icon' )
        -> DOMAttr([ 'src' => 'https://picsum.photos/100/100?random=' . (string) rand( 1, 10000 ) ])

        -> FocusParent()      /* Focus to new element */
        -> FocusChildren( 'CRUDBefore' )
        -> Event( 'Main', 'AddBefore' )

        -> FocusParent()
        -> FocusChildren( 'CRUDDelete' )
        -> Event( 'Main', 'Delete' )

        -> FocusParent()
        -> FocusChildren( 'CRUDAfter' )
        -> Event( 'Main', 'AddAfter' )

        -> FocusParent()
        -> DOMView()        /* ... and scrall to it */
        ;
    }



    /*
        Scroll up the list of content
    */
    public function ScrollTop()
    {
        return $this
        -> FocusBody()
        -> FocusChildren( 'CRUDContainer' )
        -> FocusChildren( 'List' )
        -> DOMScrollVertical( 0 );
    }



    /*
        Scroll down the list of content
    */
    public function ScrollBottom( int $AHeight )
    {
        return $this
        -> FocusBody()
        -> FocusChildren( 'CRUDContainer' )
        -> FocusChildren( 'List' )
        -> DOMScrollVertical( $AHeight );
    }
}
