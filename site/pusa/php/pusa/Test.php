<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

namespace catlair;


require_once ROOT . '/web/win.php';
require_once 'auth.php';



class Test extends TWebPusa
{
    use TWin;
    use TAuth;



    /*
        Hello world demonstration
    */
    public function HelloWorld( $AX, $AY )
    {
        return $this
        -> WinPopup( $AX, $AY )                 /* Display popup window */
        -> DOMTemplate( 'HelloWorld.html' );    /* Set content for window */
    }



    /*
        Sets new guid for GUID element
    */
    public function GUID()
    {
        return $this
        -> FocusBody()
        -> FocusChildren( 'GUID', self::ID )
        -> DOMValue( clGUID() );
    }



    public function URLParam()
    {
        $URL =  $this -> GetURL();
        return $this -> ChangeURL
        (
            $URL
            -> SetInteger( 'Int',  $URL -> GetInteger( 'Int', 10 ) + 1 )
            -> SetBoolean( 'Bool', ! $URL -> GetBoolean( 'Bool', false ) )
            -> ToString()
        );
    }
}
