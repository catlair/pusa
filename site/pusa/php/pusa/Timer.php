<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

namespace catlair;


class Timer extends TWebPusa
{
    /*
        Timer waiting half five second
    */
    public function HalfSecond()
    {
        return $this
        -> DOMDisable()                         /* Disable current focus object (button) */
        -> DOMTimer( 'Timer', 'Enable', 500 );  /* Set callback for class Timer method Enable */
    }



    /*
        Timer waiting one second
    */
    public function OneSecond()
    {
        return $this
        -> DOMDisable()
        -> DOMTimer( 'Timer', 'Enable', 1000 );
    }



    /*
        Timer waiting five second
    */
    public function FiveSeconds()
    {
        return $this
        -> DOMDisable()
        -> DOMTimer( 'Timer', 'Enable', 5000 );
    }



    /*
        Timer callback enabling the button
    */
    public function Enable()
    {
        return $this
        -> DOMEnable();
    }
}
