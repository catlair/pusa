<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

namespace catlair;

trait TEvents
{
    /*
        Action on button Initialization
    */
    public function InitEvents()
    {
        return $this
        -> FocusBody()
        -> FocusChildren( 'Events' )
        -> FocusParent()

        /* Initialization backend event callback */
        -> FocusChildren( 'BackendEvent' )
        -> Event
        (
            'Main',
            'BackendEvent',
            $this -> Clone() -> ReadPos()
        )
        -> FocusParent()

        /* Initialization frontend event callback */
        -> FocusChildren( 'FrontendEvent' )
        -> EventFront
        (
            $this
            -> Clone()
            -> ReadPos()
            -> WinPopup()
            -> DOMContent( $this -> GetTemplate( 'EventsDemo/EventFront.html' ))
        )
        -> FocusParent()

        /* Initialization JS event callback */
        -> FocusChildren( 'JSEvent' )
        -> EventJS( $this -> GetTemplate( 'EventsDemo/EventJS.js' ))
        ;
    }



    /*
        Demonstrate backend callback
    */
    public function BackendEvent( int $AX = 0, int $AY = 0 )
    {
        return $this
        -> WinPopup( $AX, $AY )
        -> DOMContent( $this -> GetTemplate( 'EventsDemo/EventBack.html' ));
    }
}
