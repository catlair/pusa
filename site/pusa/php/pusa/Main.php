<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

/*
    Main pusa.dev site.
    still@itserv.ru
*/

namespace catlair;


/*
    The main Pusa default controller.
    It can be overriding in specific pathes for each site:
        site/[YOUR_SITE]/php/Main.php
*/

require_once ROOT . '/core/markdown.php';
require_once ROOT . '/web/win.php';
require_once 'auth.php';
require_once 'guids.php';
require_once 'clipboard.php';
require_once 'events.php';
require_once 'form.php';
require_once 'crud.php';



class Main extends TWebPusa
{
    use TWin;
    use TAuth;
    use TGUIDs;
    use TClipboard;
    use TEvents;
    use TForm;
    use TCRUD;

    /*
        Build index page.
        This method will be call from [ROOT]/php/web/web.php
        if file [ROOT]/domains/[YOU_DOMAIN] contins the key "Pusa":true

        The method builds the index page and returns as HTML content.
    */
    public function Init
    (
        string $section = 'Main' /* URL Post parameter section */
    )
    {
        return $this
        /* Set content form [ROOT]/site/site_default/src/[LANG]/index.html */
        -> SetContent( $this -> BuildTemplate( 'index.html' ) )
        -> FocusBody()
        /* Set content from site/[YOUR_SITE]/src/[LANG]/Main.html */
        -> DOMTemplate( 'Main.html')

        /* Activating */

        /* Callback for back button */
        -> FocusWindow()
        /* Set event on URL history changes */
        -> Event( 'Main', 'OpenPage', null, 'popstate' )

        /* Select body object */
        -> FocusBody()
        /* Select menu */
        -> FocusChildren( 'Menu' )
        /* Focus on Actions  */
        -> FocusChildren( 'Action')
        /* Set event for all items in menu */
        -> Event
        (
            'Main',
            'OpenItem',
            $this -> Clone() -> ReadAttribute( 'ASection', 'data-action' )
        )

        /* Set event on languages */
        -> FocusBody()
        -> FocusChildren( 'Menu' )
        -> FocusChildren( 'Lang' )
        -> Event
        (
            'Utils',
            'Lang',
            $this -> Clone() -> ReadAttribute( 'AIDLang', 'data-lang' )
        )

        /* Call main router */
        -> Router( $section )
        ;
    }



    /*
        The main router for Pusa site.
    */
    private function Router
    (
        $ASection       = 'Main',
        $ARefreshURL    = false
    )
    {
        /* Main router */
        switch( $ASection )
        {
            default:
            case 'Main'     : $this -> HelloWorld(); break;
            case 'Concept'  : $this -> Concept(); break;
            case 'Examples' : $this -> Examples(); break;
            case 'Docker'   : $this -> Docker(); break;
            case 'Manual'   : $this -> Man( $this -> GetURL() -> GetString( 'topic' ) ); break;
            case 'Invest'   : $this -> Invest(); break;
            case 'Donate'   : $this -> Donate(); break;
        }

        /* Refresh URL */
        if( $ARefreshURL )
        {
            $this -> ChangeURL
            (
                $this -> GetURL()
                -> ClearParams()
                -> SetString( 'section', $ASection )
                -> ToString()
            );
        }

        /* Hilight main menu */
        $this
        -> FocusBody()
        -> FocusChildren( 'Menu' )
        -> FocusChildren( 'Item' )
        -> DOMClassRemove( 'Active' )

        -> FocusParent()
        -> FocusChildren( $ASection )
        -> DOMClassAdd( 'Active' );

        return $this;
    }



    /*
        User clicks on URL history buttons,
        or opens the page directly.
    */
    public function OpenPage()
    {
        $Section    = $this -> GetURL() -> GetString( 'section', 'Main' );
        return $this
        -> Router( $Section, false );
    }



    /*
        User clicks on page item (menu ets).
    */
    public function OpenItem
    (
        $ASection = 'Main'
    )
    {
        return $this
        -> Router( $ASection, true );
    }




    /**************************************************************************
        Visual actions
    */

    /*
        Build main page
    */
    public function HelloWorld()
    {
        return $this
        -> FocusBody() /* Select body object */
        -> FocusChildren( 'Frame' )
        -> DOMTemplate( 'TitlePage.html' )
        -> FocusChildren( 'HelloWorld' )
        -> Event
        (
            'Test',
            'HelloWorld',
            $this -> Clone() -> ReadPos()
        );
    }



    /*
        Build examples page
    */
    public function Examples()
    {
        return $this
        /* FocusBodymples content */
        -> FocusBody()
        -> FocusChildren( 'Frame' )
        -> DOMTemplate( 'Examples.html' )

        /* Init Set GUID to value example */
        -> FocusChildren( 'SetContentBtn' )
        -> Event( 'Test', 'GUID' )

        /* Init timers */
        -> FocusBody()
        -> FocusChildren( 'HalfSecond' )
        -> Event( 'Timer', 'HalfSecond' )

        -> FocusBody()
        -> FocusChildren( 'OneSecond' )
        -> Event( 'Timer', 'HalfSecond' )

        -> FocusBody()
        -> FocusChildren( 'FiveSeconds' )
        -> Event( 'Timer', 'FiveSeconds' )

        /* Set URL Params */
        -> FocusBody()
        -> FocusChildren( 'ChangeURL' )
        -> Event( 'Test', 'URLParam' )

        /* prepare auth form */
        -> AuthForm()

        /* Prepare GUID scroll demo */
        -> GenerateGUIDs()
        -> InitGUIDs()

        /* Prepare crud FORM */
        -> CRUDInit()

        /* Prepare Demo FORM */
        -> FormInit()

        /* Prepare copy example */
        -> InitClipboard()

        /* Prepere events example*/
        -> InitEvents()
        ;
    }



    /*
        Build docker page
    */
    private function Docker()
    {
        return $this
        /* FocusBodymples content */
        -> FocusBody()
        -> FocusChildren( 'Frame' )
        -> DOMTemplate( 'Docker.html' );
    }



    /*
        Build concept page
    */
    private function Concept()
    {
        return $this
        /* FocusBodymples content */
        -> FocusBody()
        -> FocusChildren( 'Frame' )
        -> DOMTemplate( 'Concept.html' );
    }



    /*
        Build Paticipation page
    */
    private function Donate()
    {
        return $this
        /* FocusBodymples content */
        -> FocusBody()
        -> FocusChildren( 'Frame' )
        -> DOMTemplate( 'donate/Main.html' );
    }



    /*
        Build Paticipation page
    */
    private function Invest()
    {
        return $this
        /* FocusBodymples content */
        -> FocusBody()
        -> FocusChildren( 'Frame' )
        -> DOMTemplate( 'invest/Main.html' );
    }



    public function Man( string $ATopic = '' )
    {
        $ATopic = empty( $ATopic ) ? 'man/head.md' : $ATopic;

        return $this
        /* FocusBodymples content */
        -> FocusBody()
        -> FocusChildren( 'Frame' )
        -> DOMContent
        (
            TMarkdown::Create()
            /* Set markdown calbacks*/
            -> SetOnLink
            (
                function( $AHTML, $ALink, $AContent )
                {
                    if( strpos( $ALink, '#' ) === 0 )
                    {
                        /* Anchor */
                        $AHTML
                        -> TagOpen( 'a' )
                        -> SetAttr( 'href', $ALink )
                        -> AddContent( empty( $AContent ) ? $ALink : $AContent )
                        -> TagClose();
                    }
                    elseif( strpos( $ALink, 'http' ) === 0 )
                    {
                        /* External link */
                        $AHTML
                        -> TagOpen( 'a' )
                        -> SetAttr( 'href', $ALink )
                        -> SetAttr( 'target', '_blank' )
                        -> AddContent( empty( $AContent ) ? $ALink : $AContent )
                        -> TagClose();
                    }
                    else
                    {
                        /* Local link */
                        $AHTML
                        -> TagOpen( 'a' )
                        -> SetAttr( 'class', 'Link' )
                        -> SetAttr( 'data-link', $ALink )
                        -> AddContent( empty( $AContent ) ? $ALink : $AContent )
                        -> TagClose();
                    }
                }
            )
            -> SetOnImage
            (
                function( $AHTML, $ALink, $AContent )
                {
                    $AHTML
                    -> TagOpen( 'img' )
                    -> SetAttr( 'class', 'Image' )
                    -> SetAttr( 'src', $ALink )
                    -> TagClose()
                    -> TagOpen( 'div' )
                    -> AddContent( $AContent )
                    -> TagClose();
                }
            )
            -> ToHTML( $this -> BuildTemplate( $ATopic, '', false ))
            -> GetContent()
        )
        -> FocusChildren( 'Link' )
        -> Event( 'Main', 'Man', $this -> Clone() -> ReadAttribute( 'ATopic', 'data-link' ))
        -> ChangeURL( $this -> GetURL() -> SetString( 'topic', $ATopic ) -> ToString() )
        ;
    }
}
