<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

namespace catlair;


/*
    Using in TPusa or TPusa childrens.
*/
trait TAuth
{
    /*
        Authorization
    */
    public function AuthForm()
    {
        $this
        -> FocusBody()
        -> FocusChildren( 'AuthForm' );

        if( $this -> GetWeb() -> IsGuest())
        {
            /* Login form */
            $this
            -> DOMTemplate( 'ExampleLogin.html' )
            -> FocusChildren( 'Login' )
            -> Event
            (
                'Main',
                'Login',
                $this -> Clone()
                -> FocusParents( 'AuthForm' )
                -> FocusChildren( 'login', TPusaCore::NAME )
                -> ReadValue( 'ALogin' )
                -> FocusParents( 'AuthForm' )
                -> FocusChildren( 'password', TPusaCore::NAME )
                -> ReadValue( 'APassword' )
            );
        }
        else
        {
            /* Logout form */
            $this
            -> DOMTemplate( 'ExampleLogout.html' )
            -> FocusChildren( 'Logout' )
            -> Event( 'Main', 'Logout' )
            -> FocusParents( 'AuthForm' )
            -> FocusChildren( 'LoginText' )
            -> DOMContent( $this -> GetWeb() -> GetLogin() )
            ;
        }

        return $this;
    }



    /*
        Login
    */
    public function Login( $ALogin, $APassword )
    {
        /* Check password for login */
        if
        (
            strlen( $ALogin ) > 4 &&                        /* login must be longe for symbols*/
            $APassword == $ALogin . '*'                     /* password must be login + "*" */
        )
        {
            $this -> GetWeb() -> Login( $ALogin );          /* Authorization is sucessfull */
            $this -> AuthForm();                            /* Build form */
        }
        else
        {
            $this -> SetResult( 'LoginError', $ALogin );    /* Set error */
        }
        return $this;
    }



    /*
        Logout
    */
    public function Logout()
    {
        $this -> GetWeb() -> Logout();
        $this -> AuthForm();
        return $this;
    }
}
