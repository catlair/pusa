# PUSA - web without JS-Front

## Links

- [Pusa manual](site/pusa/src/language_en/man/pusa_manual_head.md)
- [Pusa protocol](site/pusa/src/language_en/man/pusa_protocol.md)
- [PHP implementaition](site/pusa/src/language_default/man/pusa_php_methods.md)
- [Team and greetings](site/pusa/src/language_default/man/pusa_team.md)


---

## Concept

Pusa concept shifts all development to the Backend,
and uses pure REST. No JS development needed. Browser control, DOM, business logic work on the
Back-end without the need to store client state on the server. Pusa can be implemented in any language:
[PHP](https://gitlab.com/catlair/pusa/-/blob/main/site/pusa/src/language_default/man/pusa_php.md), JS, Java, Go, Ruby, etc.

## Positive features

1. Eliminated the need for front-end JS developers to participate.
0. The client application in the browser is compact(6kb)and stable.
0. The code remains on the back and is not available from the front.
0. REST serialization problems are completely solved, no need for tools like gRPC etc.
0. Eliminated back-end and front-end routing negotiation problems.
0. Pure REST is supported, including stateless client state on the server side.

## Technology

Pusa defines a protocol based on minimum
[directives](site/pusa/src/language_default/man/pusa_php_methods.md).

1. First, the brower loads the basic [DOM](site/site_default/src/language_default/index.html)
content and the [core JS Pusa-Front](site/site_default/src/language_default/index.js)
0. Pusa-Front sends browser events(such as click, blur, focus, keypress),
the element that triggered the event, its attributes, the URL of the current page,
as well as GET and POST parameters to the Pusa-Back via an Ajax request.
0. Based on the received data, Pusa-Back determines the controller,
executes the payload and generates a response command set.
0. Upon receiving a request response, Pusa-Front executes commands,
changing the DOM content and the environment of the browser.

![](https://pusa.dev/?image=images/pusa_exchange.png&scalex=1024)

Back sends commands back and has no way of verifying their execution.
The Front state is generated but not controlled by the Back-end.
Development for Pusa is similar to code for a video card or Canvas,
where the result of execution is not controlled by the developer.

## The essence of working with the DOM

Pusa-Front defines the focus list of DOM elements based on Pusa-Back commands.
All DOM actions, including changing content, creating and deleting elements,
change styles for focus elements only. Their list can be changed by the Pusa-back command.
This way, all DOM control is passed to the backend.

![](images/dom_scheme.png&scalex=1024)


### Step by step

1. The focus moves to the body of the document;
0. Child elements with className B are selected;
0. Payload for focus elements is done;
0. The parent of the current focus elements is selected;
0. Focus moves to child C;
0. Payload for element C is done.

## Confines

The following situations will require additional JS scripts on the client:
- Canvas and onmousemove, interactive applications.
- The need to use third-party JS scripts.

There is the ability to download and use additional JS scripts.

Pusa shifts part of the load from the Front-end to the Back-End and increases the
frequency of interaction with the server.

## Resume

Pusa does not focus on any particular implementation, but offers a paradigm shift
in web development. PHP framework, implemented to demonstrate the technology's performance.

Check out [protocol](site/pusa/src/language_en/man/pusa_protocol.md)
and list of [PHP methods](site/pusa/src/language_default/man/pusa_php_methods.md).
