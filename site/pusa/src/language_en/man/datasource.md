# Data sources

The PHP modules for datasources works are located in the **[ROOT]/php/datasource/ ** folder.
They are emplementated the full CRUD interface for records.
Manipulations with databases, tables, fields and indexes implementeded in the same way.
The transaction implementated for Transact SQL data sources.

The MVC modules of Pusa interact with [engine](site/pusa/src/language_ru/man/engines.md) [TWebDB](php/webdb/webdb.php).
The engine provides objects (TMySQL, TPostgreSQL, TMSSQL) for implementating data sources,
which interact with MySQL, PostgresSQL, MSSQL databases respectively.

```mermaid
flowchart LR

Model[Pusa Model]
Engine[Engines]
Implementation[Implementations]
Datasource[(Datasources)]

Engine -.-> Model
Implementation -..-> Engine
Model <==> Implementation
Implementation <===> Datasource
```

There is a detaled scheme interaction between modules and objects with a Web application.

```mermaid
flowchart BT

subgraph "Application"
    Pusa[Pusa Model]
end

subgraph "Engine"
    TWebDB
end

subgraph "Interface"
    TData
    TDataSet
end

subgraph "Implementation"
    TSQL
    TMySQL
    TPostgreeSQL
    TMSSQL
    TREST
    TFile
end

TData .->|Get Datasource| TWebDB
TWebDB .->|Give Datasource| Pusa

TData .-> |Result| TDataSet
TMSSQL --> |extends| TSQL
TPostgreeSQL --> |extends| TSQL
TMySQL --> |extends| TSQL
TSQL --> |extends| TData
TREST --> |extends| TData
TFile --> |extends| TData
Pusa -.-> |Request| TData
TDataSet -.-> |Result| Pusa
```

## Description

- [TWebDB](php/webdb/webdb.php) - Implementation of [engine](site/pusa/src/language_ru/man/engines.md) for working with database.
They have the ability to creation new connections to databases, a connection caching system.
The engine provides the connection through the TData object.
- [TData](php/datasource/data.php) - The factory object provids universal acess to data.
It defines the basic methods of interaction with data sources, contains definitions of data types, but not implementations.
This class is used as factory to create object interaction.
Using the TData object in the user application ensures the transparency of switching between different data sources.
- [TDataset](php/datasource/dataset.php) - An object that returns the result of the meeting via TData.
Contains a list of data sets, with the ability to access the records of each of them.
- [TSQL](php/datasource/sql.php) - A set of common methods for various TransactSQL implementations. Used as a basis for TMySQL, TPostgreSQL, TMSSQL.
- [TMySQL](php/datasource/mysql.php) - Implementation of work with MySQL databases. Contains a set of methods for interacting with MySQL databases.
- [TPostgreeSQL](php/datasource/postgreesql.php) - Реализация работы с базами данных PostgreeSQL.
- [TMSSQL](php/datasource/mssql.php) - Implementation of working with PostgreSQL databases.
- [TREST](php/datasource/rest.php) - Allows you to interact with a remote data source via REST. Awaiting implementation.
- [TFile](php/datasource/rest.php) - Simplified data source on file structure. Is not Transact SQL. Awaiting implementation.

## Configuration

It is possible to use several databases for each site. Configuration file for the site
placed in a file:
```
[ROOT]/site/[ID_SITE]/config/datasource/[DATABASE].json
```
где:
- ID_SITE - site identifier for configuring;
- DATABAE - database name ( default is **default** value ).

The fully qualified database name on the data server is a combination of the site name and the data name configuration.
For a **mysyte** site and a configuration name of **default**, the fully qualified database name will be **mysite_default**.

```mermaid
flowchart TB

subgraph "Databases"
site1_base1[(site1_base1)]
site2_base3[(site1_base3)]
site1_base2[(site1_base2)]
site2_base4[(site1_base4)]
end

subgraph "Names"
base1
base2
base3
base4
end

subgraph "Sites"
 site1
 site2
end


site1 --- base1
site1 --- base2
site2 --- base3
site2 --- base4

base1 --- site1_base1[(site1_base1)]
base2 --- site1_base2[(site1_base2)]
base3 --- site2_base3[(site2_base3)]
base4 --- site2_base4[(site2_base4)]
```


## The configuration file
```
{
    "Active": "Section",                        // Current active configuration
    "Section":
    {
        "Type"              : "mysql",          // Provider database type
        "Login"             : "login",          // Name of user for data server access
        "Password"          : "*****",          // Password for user
        "Host"              : "127.0.0.1",      // IP address or hostname to access the database
        "Port"              : "444",            // port number for server connection
        "Database"          : "mysite_default", // default database name
        "TimeoutPenaltyMls" : 0                 // base health check penalty
    },
    ...
}
```
Each configuration file can include many different data sources for the database.
To change the **Active** parameter, use the corresponding section. So site switch
between different data sources is done by changing the **Section** parameter.

## Accessing a Data Source

A Pusa application can access an arbitrary data source by calling an engine object.
Further, through it, you can directly get the Datasource data source. If the requested source
the data has not been previously called in the session, it will be created.

Inside the model for Pusa, consider the following example of creating a user in a database.

```
private function Registrate
(
    string  $ALogin,
    string  $APassword  /* Password for user */
)
{
    $this
    -> GetWeb()                 /* Return current engine object */
    -> GetDatasource()          /* Return default datasource */
    -> RecordInsert             /* Call insert */
    ([
        'Table' => 'Users',     /* Into Users table*/
        'Records' =>
        [[
            /* Set login field */
            'ID'            => $ALogin,
            /* Set password hash */
            'PasswordHash'  => $this -> GetWeb() -> GetPasswordHash( $ALogin, $APassword )
        ]]
    ])
    -> ResultTo( $this );       /* And return result code to Pusa */
    return $this;
}
```
Fully functional user creation code is in the example [todo](site/todo/php/pusa/model.php).
