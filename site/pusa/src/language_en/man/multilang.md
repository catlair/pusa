# Multilingual support

## Links

- [Catlair](site/site_default/src/language_default/man/catlair.md)
- [Templater](site/site_default/src/language_default/man/templater.md)

## Description

Pusa has built-in multilingual support and content inheritance on Catlair libraries.
Each text template and graphic image for each site can be written in any language.
For a file-based implementation, all templates for each IDSite site are located in the language folder:
```
[ROOT]/site/[IDSite]/src/[IDLanguage]
```
where **IDLanguage** is the language identifier.

For each language, a separate folder is created, from which further and according to the template it searches
identifier.
For example template **example/main.html** for site  **test_site** in language **language_en**.
```
[ROOT]/site/test_site/src/language_en/example/main.com
```

## Template request hierarchy

When building content the templater [](https://gitlab.com/catlair/pusa/-/blob/main/site/site_default/src/language_default/man/templater.md)
does the following:
- tries to request content in the specified language for the current site;
- if not, tries to download content in the default language for the current site;
- if absent, it tries to download content in the specified language for the default site (site_default);
- if absent, it tries to download content in the default language for the default (site_default) site;
- if absent, an error about the absence of content is returned.

THe request scheme **hello.html** in the language **language_en** for site **my_site**.

```mermaid
graph TD
    sl["[ROOT]/site/my_site/src/language_en/hello.html"]
    sld["[ROOT]/site/my_site/src/language_default/hello.html"]
    sdl["[ROOT]/site/site_defult/src/language_en/hello.html"]
    sdld["[ROOT]/site/site_default/src/language_default/hello.html"]

    request-->sl
    sl-->|-|sld
    sl-->|+|result
    sld-->|-|sdl
    sld-->|+|result
    sdl-->|-|sdld
    sdl-->|+|result
    sdld-->|+|result
    sdld-->|-|error
```

Thus, **inheritance** of content for sites and languages is implemented. Developer
may use content from other sites.
For example [[ROOT]/site/site_default/src/language_default/index.html](/site/site_default/src/language_default/index.html)
no need to create for every site. The default will be used.

## Language definitions

With each next request, the language can be obtained from the following sources:
- User session variable;
- Setting for the current domain;
- The default language for the current domain;
- The language_default constant.

The language definition for each user request is done in [[ROOT]/php/web/web.php](/php/web/web.php)

## Language switching

For the current session, language switching is done by calling the Pusa controller:
```
$this                           /* Current controller or Pusa controller*/
-> GetWeb()                     /* Return Web object */
-> SetIDLang( 'language_en' );  /* Set lanaguage */
```
After changing the language, it will be saved in the session variable.
At the next user request, the content will be built in the specified language.
