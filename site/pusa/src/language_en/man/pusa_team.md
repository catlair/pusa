# Pusa team

* still@itserv.ru - idea, PHP and JS реализация;
* igorptx@gmail.com - PHP emplementation;
* petr@gmail.com - devops.

## Greetings
* artem.bashkatoff@yandex.ru, Bashkatov Artem - concept review, discussion of file upload implementation;
* [](https://www.opennet.ru/~fuggy) - licensing guidelines;
* [](https://github.com/booydar) - project deployment testing, comments.

## Links
- [Manual](man/head.md)
- [Pusa methods](man/pusa_php_methods.md)
- [Site pusa.dev](https://pusa.dev)
