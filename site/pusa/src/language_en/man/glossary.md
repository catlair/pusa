# Pusa definitions

## Base

- [Pusa](man/pusa.md) - technology for developing web applications that bring all work to BackEnd without having to work on FrontEnd.
- [Catlair] - implementation (set of libraries) for developing web applications.
- [Pusa protocol](pusa_protocol.md) - the minimum set of exchange directives between Pusa-Front and Pusa-Back.
- [Pusa-Back] - Pusa server side supporting Pusa protocol.
- [Pusa-Front] - Pusa client part on JS with Pusa protocol support.
- [Pusa-PHP](man/pusa_php.md) - first implementation of Pusa-Back in PHP.

## Pusa

- Event Element - The DOM element in the browser for which the event was triggered by the user.
For example, when you click the button button, it will be an event element.
- Focus elements - a list of DOM elements in the browser, for which all subsequent commands will subsequently be executed.
Each time Pusa is called, the list is populated with only the event item.
