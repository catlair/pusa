# Pusa Protocol

## Links

- [Definitions](site/pusa/src/language_en/man/glossary.md);
- [Pusa - WEB without JS](site/pusa/src/language_en/man/pusa.md);
- [PHP emplementaition](site/pusa/src/language_default/man/pusa_php_methods.md).

## Intro

The FrontEnd looks like a black box with finite set of states.
The states persist until the page is reload. Calls are initiated by FrontEnd.
The Backend sends directives as answers on requests. The Frontend executes the directives.

Pusa defines a minimalistic set of directives from which high-level kernel and application methods can be built.
All commands are contained in an array. The first element contains the name of the directive.
Subsequent members of the array are interpreted as arguments at the discretion of FrontEnd.
The result of the directive execution is not sent to the BackEnd.

## Criteria for creating a protocol

1. Atomicity of directives. A directive is included in the protocol if its functionality cannot be implemented by the available means.
2. Readability. The list of commands must be available to humans and therefore must not be binary.
3. Shortness of identifiers. Identifiers and transmitted data should be truncated as long as it does not interfere with point 2.
4. Minimization of branching. The protocol seeks to minimize code branching and conditional execution.

## Focus control

Pusa does not have the ability to work with browser components. Pusa selects DOM elements,
objects, method results, and properties into the list of focus objects and only interacts with them.

- [Focus](#focus) - set focus on DOM elements;
- [FocusPush](#focuspush) - push focus elements in to the stack;
- [FocusPop](#focuspop) - pop the focus elements from the stack;

## Execution control

- [Debug](#debug) - debug mode control for browser;
- [Call](#call) - method call for focus DOM elements or used objects;
- [Set](#set) - property value setting for FrontEnd focus objects;
- [Loop](#loop) - cyclic execution of directives on FrontEnd;
- [If](#if) - conditional execution of directives on FrontEnd;
- [Event](#event) - set event for DOM elements and browser objects;
- [Argument](#argument) - request to return argument values from FrontEnd;
- [Timer](#timer) - start the timer with following event callback or stop the timer;
- [Create](#create) - create new DOM elements (should be removed from the protocol);
- [CSSAttr](#cssattr) - setting style attributes for styles by name (should be removed from the protocol);
- [JS](#js) - run JS code (not recomended).

## Pile

Named array of strings (Pile) for Pusa on the browser. Provides storage and exchange of values
between different elements without calling Back. Retains values throughout life
loaded page. Reloading the page clears Pile.

- [PileFrom](#pilefrom) - placing the values of attributes and properties of focus DOM elements in the Pile.
- [PileTo](#pileto) - filling in property attributes and properties of focus DOM elements from Pile.



## Directives description

### Focus

Sets the browser focus to items that match a filter condition.
The filter uses [search operators](site/pusa/src/language_ru/man/operator.md).
The search for items is conducted in the direction of Target.
The previous list of core items will be replaced with new centerpieces.

#### Arguments

```
[
    'Focus',
    [
        'Target'    : string,   // The selection method for focus elements is determined
                                // by one of the following values
                                // - Body - the body element of document is set as the only focused element;
                                // - Self - the event element is set as the only focused element;
                                // - Children - search for elements from the children of the the currently focused elements;
                                // - ChildrenOfThis - search for elements from the direct children of the the currently focused elements;
                                // - Parent - search for elements from the direct parent of the the currently focused elements;
                                // - Parents - search for elements from the parents of the the currently focused elements.
                                // - ParentFirst - the first parent that matches the filter.
        'Filter'    : array     // Filter for selecting items when Target = Children, ChildrenOfThis, Parent, Parents, ParentFirst
    ]
]
```

#### Example
```
[ 'Focus', [ Target: 'Body' ]]  // set focus on document body;
[ 'Focus', [ Target: 'Children', [ '=', '@className', 'a1' ]]] // set focus on all children elements with className is equal a1;
```

Further, various manipulations can be performed with the focused elements.
---



### FocusPush

Stores the list of focus objects on the stack. The active list does not change.

#### Arguments
```
[
    'FocusPush',
    []
]
```
---



### FocusPop

Restores the list of focus objects from the stack. All focus elements are replaced from the stack.
If there is no stack, a warning is returned. Code execution continues.

#### Arguments
```
[
    'FocusPush',
    []
]
```





### Deubg
Debug mode control for FrontEnd.

#### Arguments
```
[
    'Debug',
    [
        'Level':string  // Debug level
                        // DEBUG_ON - debuging enabled
                        // DEBUG_OFF - debugging disabled
    ]
]
```

---
### Call
Calls a method on objects or on focus DOM elements in the browser.
The method being called is the set parameter Call. The parameter is passed
an array with a sequence of object names, methods, and arguments.

This method cannot be released above TPusa and must be minified in custom code.
All code using direct Call must be wrapped in libraries to provide isolation.
applications from the browser API.

#### Arguments
```
[
    'CALL',
    [
        'Method'    :string,    // The method name
        'Arguments' :array      // Optional array of arguments
    ]
]
```


---
### Loop

Cyclic execution of a list of directives over an array of data.
FrontEnd receives an array of named arrays containing entries in key value format.
For each element, a set of directives is executed.

All values located between the **%** for each command will be replaced before execution
to the corresponding key values from the data array.

For record
```
[ ID:'Alpha']
```
directive
```
[ 'SetProperty',    [[ 'id': '%ID%' ]]
```
will be changed
```
[ 'SetProperty',    [[ 'id': 'Alpha' ]]
```

The method is used to build lists on the FrontEnd.

#### Arguments
```
[
    'Loop',
    [
        'Commands'  : array,    /* Array of Pusa directives */
        'Tasks'     : array     /* Named array */
    ]
]
```

#### Example
```
[
    'Loop',
    [
        'Commands'  :
        [
            [ 'Create',         [ 'Position'    : 'First', 'TagName': 'DIV' ]],
            [ 'SetProperty',    [[ 'id'         : '%ID%' ]]],
            [ 'Focus',          [ 'Target'      : 'Parent' ]]
        ],
        'Tasks'     :
        [
            [ 'ID'  : 'Alpha' ],
            [ 'ID'  : 'Betta' ],
            [ 'ID'  : 'Gamma' ]
        ]
    ]
]
```

The following structure will be created in each focus element:
```
<div id="Alpha">
</div>
<div id="Betta">
</div>
<div id="Gamma">
</div>
```



---
### If

Conditional call directives. The directive receives the condition identifier,
which can be checked against FrontEnd and two sets of directives,
which will be executed depending on the state of the condition.

#### Arguments
```
[
    'If',
    [
        'Condition' : string,   // The condition identifier
                                // - FocusIsEmpty - фокусны список пуст
        'True'      : array,    // Array with Pusa commands for case when the Condition is true
        'False'     : array     // Array with Pusa commands for case when the Condition is false
    ]
]
```
---

### Event

Sets an Event for focus DOM elements and window or document objects depending on the Target parameter.
When an event occurs in the browser with a TimeoutMilliseconds delay,
the back Method in the Class controller will be called.

#### Arguments
```
[
    'Event',
    [
        'Event'     : string,   // Event name like click, blur, keypress, keyup, load etc;
        'Catch'     : bool,     // Enables or disables event propagation in the DOM tree
                                // (using JS call stopPropagation if true).
        'Filter'    : array,    // Filtering conditions for DOM elements if Target is set in the DOM.
                                // Filters use conditional search notation [Operator, Argument 1, Argument 2] *
        // Parameters for raising the event on the BackEnd
        'Class'     : string,   // The name of the Pusa controller for which the method will be called when an event occurs;
        'Method'    : string,   // Method in the Pusa controller class that will be called when an event occurs;
        'Commands'  : array,    // Array of Pusa directives that will be executed when an event occurs without calling BackEnd;
        'Timeout'   : int,      // Timeout in milliseconds defining the delay in sending the event to Back.
                                // All repetitions are not events received within a specified period of time postpone the moment of sending.
                                // Thus, of the many events that followed it interval will be sent only the last one.
                                // If the program value is 0, each event will be dispatched.
                                // This parameter allows you to avoid bombarding the server with events such as mousemove or scroll.
        // Parameters for calling JS code on FrontEnd
        'JS'        : string    // When an event occurs, the passed JS script is executed without calling the Pusa server.
                                // It is recommended to use only when all other possibilities of Pusa have been exhausted.
    ]
]
```

#### HTML notation

Setting Pusa events is possible in HTML for any element via the data-event attribute:

```
<button data-event="click.Class.Method.Timeout">
</button>
```

or

```
<button data-event="mousemove" data-event-class="Class" data-event-method="Method" data-event-timeout="100">
</button>
```

---

### Arg

Request named arguments from the browser. The requested arguments will be collected from various objects
and routed to the BackEnd in the **Arg** POST parameter when the event occurs.

Requesting arguments only makes sense in an event callback via Event.
On direct calls, this directive will be executed, but the arguments themselves will not be sent back.

[
    'Arg',
    [
        'Name'      : string,   // rgument name to redirect to Back
        'Event'     : string,   // An optional value name from the event object from which the argument value will be taken
        'Property'  : string,   // An optional property name of the focus element from which the argument value will be taken
        'Method'    : string,   // An optional method name whose result will be passed as an argument
        'Arguments' : array     // An optional array of arguments for the method
    ]
]




---

### Create

Create a new DOM element for each focus element.
After execution, focus moves to the newly created elements.
Create can create many new elements.
There is one focus for each current element.

#### Arguments
```
[
    'Create',
    [
        'TagName'   : string,   // tagname for new element
        'Position'  : string    // the position for new elements relative to each of the elements in focus
                                // - First - the new element will be placed first in the focused element
                                // - Last - the new element will be placed last in the focused element
                                // - Before - the new element will be placed before of the focused element
                                // - After - the new element will be placed before of the focused element
    ]
]
```

#### Example

Creates a new div in all focused elements.

```
[
    'Create',
    [
        'TagName':'div',
        'Position': 'Last'
    ]
]
```



---

### Timer

The browser sets or stops a timer for focus DOM elements with an interval of Timeout.
When the interval expires, the Backend Method in the controller will be called.

The method can be used to get information about DOM objects.

If Timeout is zero, an immediate call will occur with the transfer of information about the object to Pusa-Back or a call to the list of commands.
This approach is possible but not recommended. Use (Send)[#domsend].

Pusa does not provide for the creation of repeating timers. If you need to work regularly,
the timer can be reset on the next call to Class.Method.

#### Argument
```
[
    'Timer',
    [
        'ID'        : string    // Optional timer identifier by which it can be reset
        'Order'     : array     // An optional list of ordered parameters for sending to BackEnd in the format:
                                // [ 'Object', 'Property', [ 'Method', [ Argument1, ... ]]]
        'Timeout'   : int       // The number of milliseconds before the timer event is called. At 0, an immediate call will occur.
        'Stop'      : bool      // If true, the previously called timer will be stopped for the ID or Class.Method pair.
                                // If no ID or Class.Method is specified, the timer will not be reset.
                                // Fields for calling a timer on the BackEnd
        'Class'     : string,   // The name of the class that will be called on Pusa-Back when the timer event occurs.
        'Method'    : string,   // The name of the method in the class that will be called when the Pusa-Back timer event occurs.
        // Fields for calling the timer on FrontEnd
        'Commands'  : array     // Array of Pusa commands that will be called on FrontEnd when the timer is triggered
    ]
]
```
#### Example
```
[
    'Timer',
    [
        'Class'     : 'Main',
        'Method'    : 'Delete',
        'Timeout'   : 1000
    ]
]
```



---

### Replace

Cyclically replaces all passed patterns with values in attributes of DOM elements.
Each key value will be replaced with the corresponding value.
A rarely used directive.

#### Argument
```
[
    'Replace',                  // The directive identifier
    [
        'Values':               // Array key value to replace
        [
            'Key' : 'Value',    // Key and value record
            ...
        ]
    ]
]
```
#### Example
```
// HTML code
<div color="red">
</div>

// Replacement directive
[
    'Replace'
    [
        'Values':
        [
            'red': 'white'
        ]
    ]
]

// Result HTML code of the element
<div color="white">
</div>

```



---
### CSSAttr

Sets the style attribute values for a style sheet by name. The title includes a complete description of the style.

#### Argument
```
[
    'CSSAttr',                  // The directive identifier
    [
        'Name'  : string,       // The fully qualified name of the style, as described in the style sheet.
        [                       // Named array of style attributes key:value
            'color' : 'red',
            ...
        ]
    ]
]
```
#### Example
```
// CSS style description
body .MyStyle
{
    color: black;
}

// Change the style attribute of a color
[
    'CSSAttr',
    [
        'Name'  : 'body .MyStyle',
        [
            'color' : 'red'
        ]
    ]
]
```

---


### PileFrom

Sets the value for the item in the Pile named Name. Pile is used as a bunch of values,
through which the exchange between DOM elements and infmi components of the browser
is possible without direct involvement of JS.

The value of Pile can be filled from various sources depending on the parameters:
- Direct value
- Property of focus objects
- Result of executing focus object methods

#### Argument
```
[
    'PileFrom',
    [
        'Name'      : string,   // Pile key name for value
        'Operator'  : string,   // method for setting the value. Two options are valid
                                // - Set - the key value in the Pile will be completely overwritten by the Value value
                                // - Add - the key value in Pile will be padded with Value
        'Value'     : string,   // Pile value
        'Property'  : array,    // the name of the focus elements property whose result will be loaded into Pile
        'Method'    : array,    // the name of the focus element method whose result will be loaded into Pile
        'Arguments' : array     // an optional array of arguments for the method

    ]
]
```


---
### DOMToPile
Putting Pile values in different directions:
- Properties of focal objects
- Use Pile values as method argument for focus elements

#### Attribute
```
[
    'DOMToPile',
    [
        'Name'      : string, // name of the key in Pile from which the value will be taken
        'Property'  : array , // the name of the focus elements property that will be set to Pile
        'Method'    : array   // name of focus element method to be called with Pile value argument
    ]
]
```



---

### JS

Direct code execution in the browser. It is recommended to minimize usage.

JS ussage criteria:
1. The required functionality is provided by third-party and can not be implemented othervise.
0. Requires processing of a huge count events in real time, for example
drawin on canvas with mousemove event.
0. Pusa has no functionality for this case. We ask you to inform the developers about such cases.

#### Arguments
```
[
    'JS',
    'JSCode'    // pure JS code without HTML tags **script**.
]
```

