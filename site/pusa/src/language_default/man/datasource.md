# Источники данных

PHP модули для работы c источниками данных размещены в папке **[ROOT]/php/datasource/ **.
С их помощью реализуется полный CRUD интерфейс для записей.
Так же реализованы методы манипуляции с базами данных, таблицами, полями, индексами.
Для источников данных Transact SQL реализованы транзакци.

MVC модули Pusa взаимодейтсвует с [движком](site/pusa/src/language_ru/man/engines.md) [TWebDB](php/webdb/webdb.php).
Движек предоставляет объекты (TMySQL, TPostgreSQL, TMSSQL) реализаций для различных источников данных, которые взаимодейтсвуют
с источниками данных MySQL, PostgresSQL, MSSQL и тд.

```mermaid
flowchart LR

Model[Pusa Model]
Engine[Engines]
Implementation[Implementations]
Datasource[(Datasources)]

Engine -.-> Model
Implementation -..-> Engine
Model <==> Implementation
Implementation <===> Datasource
```

Ниже представлена развернутая схема взаимодействия модулей и объектов c Web приложением.

```mermaid
flowchart BT

subgraph "Application"
    Pusa[Pusa Model]
end

subgraph "Engine"
    TWebDB
end

subgraph "Interface"
    TData
    TDataSet
end

subgraph "Implementation"
    TSQL
    TMySQL
    TPostgreeSQL
    TMSSQL
    TREST
    TFile
end

TData .->|Get Datasource| TWebDB
TWebDB .->|Give Datasource| Pusa

TData .-> |Result| TDataSet
TMSSQL --> |extends| TSQL
TPostgreeSQL --> |extends| TSQL
TMySQL --> |extends| TSQL
TSQL --> |extends| TData
TREST --> |extends| TData
TFile --> |extends| TData
Pusa -.-> |Request| TData
TDataSet -.-> |Result| Pusa
```

## Описание

- [TWebDB](php/webdb/webdb.php) - Реализация [движка](site/pusa/src/language_ru/man/engines.md) для работы с базами данных.
Имеет возможность создания соединений с базами данных, систему кэширования соединений. Движек представляет приложению
соединения с базами данных на основе TData.
- [TData](php/datasource/data.php) - Объект фабрика, предоставляющая возможность унифицированного доступа к данным.
Определяет основные методы взаимодействия с источниками данных, содержит описание типов данных, но не реализацию.
Данный класс используется как фабрика для создания объектов взаимоедйствия с данными.
Если пользовательское приложение работает только с объектом TData, возможно прозрачный переход между различными источниками данных.
- [TDataset](php/datasource/dataset.php) - Объект, возвращающий результат исполнения запросов через TData.
Содержит перечень комлектов данных (Dataset), с возможностью обращаться к записям каждого из них.
- [TSQL](php/datasource/sql.php) - Набор общих методов для различнх TransactSQL реализаций.
Используется как основа для TMySQL, TPostgreeSQL, TMSSQL.
- [TMySQL](php/datasource/mysql.php) - Реализация работы с базами данных MySQL. Содержит набор методов взаимодейтсвия с базами на MySQL.
- [TPostgreeSQL](php/datasource/postgreesql.php) - Реализация работы с базами данных PostgreeSQL.
- [TMSSQL](php/datasource/mssql.php) - Реализация работы с базами данных MSSQL.
- [TREST](php/datasource/rest.php) - Позволяет взаимодйствовать с удаленным источником данных через REST. Ожидает реализации.
- [TFile](php/datasource/rest.php) - Упрощенный источних данных на файловой структуре. Не является Transact SQL. Ожидает реализации.

## Конфигурирование

Для каждого сайта предусмотрена возможность использования множества баз данных. Конфигурационный файл для сайта
размещен в файле:
```
[ROOT]/site/[ID_SITE]/config/datasource/[DATABASE].json
```
где:
- ID_SITE - идентификатор конфигурироемого сайта;
- DATABAE - конфигурационое имя базы данных ( по умлочанию **default** ).

Полное имя базы данных на сервере базы данных складывается из имени сайта и конфигурационного имени базы данных.
Для сайта **mysyte** и конфигурационного имени **default** полное имя базы данных будет **mysite_default**.

```mermaid
flowchart TB

subgraph "Databases"
site1_base1[(site1_base1)]
site2_base3[(site1_base3)]
site1_base2[(site1_base2)]
site2_base4[(site1_base4)]
end

subgraph "Names"
base1
base2
base3
base4
end

subgraph "Sites"
 site1
 site2
end


site1 --- base1
site1 --- base2
site2 --- base3
site2 --- base4

base1 --- site1_base1[(site1_base1)]
base2 --- site1_base2[(site1_base2)]
base3 --- site2_base3[(site2_base3)]
base4 --- site2_base4[(site2_base4)]
```


## Конфигурационный файл
```
{
    "Active": "Section",                        // Текущая активная конфигурация
    "Section":
    {
        "Type"              : "mysql",          // Тип базы данных для раздела
        "Login"             : "login",          // Имя пользователя для доступа к базе
        "Password"          : "*****",          // Пароль для доступа к базе
        "Host"              : "127.0.0.1",      // IP адрес или имя хоста для доступа к базе
        "Port"              : "444",            // номер порта для подключения к серверу
        "Database"          : "mysite_default", // имя умолчальной базы данных
        "TimeoutPenaltyMls" : 0                 // пенальти для проверки работоспособности базы
    },
    ...
}
```

Каждый конфигурационный файл можт включать в себя множество различных источников данных для базы данных.
Изменение параметра **Active** использует соответствующую секцию. Таким образом переключение сайта
между различными источниками данных осуществляется изменением параметра **Section**.


## Обращение к источнику данных

Прикладное приложение Pusa может обратиться к произвольному источнику данны через обращение к объекту движка.
Далее через него может быть получен непосредственно источник данных Datasource. Если запрошенный источинк
данных ранее не вызывался в сессии, он будет создан.

Находясь внутри Модели для Pusа рассмотрим следующий пример создания пользователя в базе данных.

```
private function Registrate
(
    string  $ALogin,
    string  $APassword  /* Password for user */
)
{
    $this
    -> GetWeb()                 /* Return current engine object */
    -> GetDatasource()          /* Return default datasource */
    -> RecordInsert             /* Call insert */
    ([
        'Table' => 'Users',     /* Into Users table*/
        'Records' =>
        [[
            /* Set login field */
            'ID'            => $ALogin,
            /* Set password hash */
            'PasswordHash'  => $this -> GetWeb() -> GetPasswordHash( $ALogin, $APassword )
        ]]
    ])
    -> ResultTo( $this );       /* And return result code to Pusa */
    return $this;
}
```
Полнофункциональный код для создания пользователя размещен в примере [todo](site/todo/php/pusa/model.php).
