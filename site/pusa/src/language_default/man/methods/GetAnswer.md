# GetAnswer

Возвращает код ответа Pusa в виде JSON со следующей структурой:

```
{
    "Header":
    {
        "Code":"ErrorCode",
        "Message":"Error message"
    },
    "Pusa":
    {
        [
            {
                "CommandName",
                [
                    {
                        "Param":"Value",
                        ...
                    }
                ]
            },
            ...
        ]
    }
}
```

Используется для передачи готового ответа на PusaFront.

## Аргументы
- нет

## Результат
- string json

## Ссылки
- [Исходный код](https://gitlab.com/catlair/pusa/-/blob/main/php/core/pusa_core.php)
- [Методы Pusa](man/pusa_php_methods.md)

