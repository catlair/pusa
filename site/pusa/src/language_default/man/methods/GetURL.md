# GetURL

Возвращает объект TURL, заполненный параметрами текущей ссылки. Параметры могут быть получены или установлены методами:
- [GetInteger](GetInteger)
- [SetInteger](SetInteger)
- [GetString](GetString)
- [SetString](SetString)
- [GetBoolean](GetBoolean)
- [SetBolean](SetBoolean)

Для отправки измененного URL на клиента необходимо воспользоваться командой [ChangeURL](ChangeURL).

## Аргументы
- нет

## Результат
- TLog

## Ссылки
- [Исходный код](https://gitlab.com/catlair/pusa/-/blob/main/php/core/pusa_core.php)
- [Методы Pusa](man/pusa_php_methods.md)
