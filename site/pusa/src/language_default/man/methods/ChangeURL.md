# ChangeURL

Изменяет без перезагрузки URL браузера.

## Аргументы
- string $AURL - полный или сокращенный URL.

## Результат
- $this

## Ссылки
- [Исходный код](https://gitlab.com/catlair/pusa/-/blob/main/php/core/pusa_core.php)
- [Методы Pusa](man/pusa_php_methods.md)

