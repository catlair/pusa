# OpenURL

Загружает AURL в окно ATarget браузера.

## Аргументы
- TURL $AURL - объект URL
- string $ATarget - имя целевого окна либо предустановленные TPusaCore::CURRENT, TPusaCore::NEW.

## Результат
- $this

## Ссылки
- [Исходный код](https://gitlab.com/catlair/pusa/-/blob/main/php/core/pusa_core.php)
- [Методы Pusa](man/pusa_php_methods.md)
