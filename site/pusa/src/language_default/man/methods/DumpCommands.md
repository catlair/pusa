# DumpCommands

Выводит в лог перень текущих команд-директив для Pusa-Front c подсветкой синтаксиса.

## Аргументы
- нет

## Результат
- $this

## Ссылки
- [Исходный код](https://gitlab.com/catlair/pusa/-/blob/main/php/core/pusa_core.php)
- [Методы Pusa](man/pusa_php_methods.md)
