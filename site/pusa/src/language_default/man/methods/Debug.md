# Debug

Установка отладочного режима $ALevel для браузера.

## Аргументы
- string $ALevel - уровень вывода:
-- TPusaCore::DEBUG_OFF - сообщения Pusa-Front не будут отображаться в консоли браузера;
-- TPusaCore::DEBUG_ON - сообщения Pusa-Front будут отображаться в консоли браузера.

## Результат
- $this

## Ссылки
- [Исходный код](https://gitlab.com/catlair/pusa/-/blob/main/php/core/pusa_core.php)
- [Методы Pusa](man/pusa_php_methods.md)

