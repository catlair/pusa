# EventJS

Устанавливает событие Event для фокусных элементов DOM и объектов браузера window или document
в зависимости от параметра Target. При наступлении события в браузере, выполняется переданный JS
код. В отличие от метода Event обращения к сереверу Pusa не происходит.

## Аргументы

- string $AJS - непосредственно код JS, который будет выполнен на клиенте при наступлении события;
- string $AEvent - Имя события, такие как click, blur, keypress, keyup, load и прочие;
- string $Target - Имя типа цели для установки события:
    - TPusaCore::TARGET_DOM = 'DOM' - события устанавливается для фокусных DOM элементов (умолчальное значение);
    - TPusaCore::TARGET_WINDOW = 'Win' - событие устанавливается для объекта window;
    - TPusaCore::TARGET_DOCUMENT = 'Doc' - событие устанавливается для объекта document;
- bool $ACatch - Позволяет или запрещает распространение события в дереве DOM (используя JS вызов stopPropagation при значении true).

## Примечание

Использование данного метода рекомендуется только при исчерпании всех иных возможностей Pusa.

## Результат
- $this

## Примеры

```
$this -> EventJS
(
    'alert( "Hello world" );',
    TPusaCore::CLICK    /* Все это сработает на clic */
);
```
## Ссылки
- [Исходный код](https://gitlab.com/catlair/pusa/-/blob/main/php/core/pusa_core.php)
- [Методы Pusa](man/pusa_php_methods.md)
