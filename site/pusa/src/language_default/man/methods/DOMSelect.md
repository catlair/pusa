# DOMSelect

Выбирает на *стороне браузера* DOM элементы в фокус по имени $ASearch для cелектора $ASelector по правилу $ATarget.
Поиск выполняется по дереву DOM. Метод начинает работу с каждым текущим фокусным элементом, производя от него поиск
согласно аргументов и замещает список фокусны элементов вновь выбранными. Дальнейшие манипуляции с DOM элементами так же
производятся только по фокусному списку.

При запуске Front-Pusa устанавливает фокус на событийныый элемент. Таким образом все манипуляции будут начаты относительно него.

### Аргументы
- string $ASearch - строковое выражение для поиска элементов
- string $ASelector = TPusaCore::CLASS_NAME - имя селектора из возможных вариантов:
-- TPusaCore::CLASS_NAME         = 'Class' - выбор по классу DOM элемента *className* (умолчальное);
-- TPusaCore::ID                 = 'ID' - выбор по идентификатору DOM элемента *id*;
-- TPusaCore::NAME               = 'Name' - выбор по имени DOM элемента *name*;
-- TPusaCore::TAG                = 'Tag' - выбор по имени тэга DOM элемента *tagName*;
- string $ATarget = TPusaCore::CHILDS - идентификатор правила поиска для установки фокуса:
-- TPusaCore::SELF               = 'Self' - на событийный элемент.
-- TPusaCore::BODY               = 'Body' - на корневой элемент документа
-- TPusaCore::PARENT             = 'Parent' - на родительский элемент для каждого текущего
-- TPusaCore::PARENT_FIRST       = 'ParentFirst' - поиск первого по иерархии родительского элемента
-- TPusaCore::PARENTS            = 'Parents' - поиск по всем родительским элементам по иерархии
-- TPusaCore::CHILDS             = 'Childs'- поиск по всем дочерним элементам по иерархии (умолчальное)
-- TPusaCore::CHILDS_OF_THIS     = 'ChildsOfThis' - поиск только по непосредственным потомкам

### Результат
- $this

### Пример
```
$this -> FocusChildren( 'MyClass' );
```
```
$this -> DOMSelect( 'MyID', TPusaCore::ID );
```
```
$this -> DOMSelect( 'MyClass', TPusaCore::ID, TPusaCore::Parents );
```

## Ссылки
- [Исходный код](https://gitlab.com/catlair/pusa/-/blob/main/php/core/pusa_core.php)
- [Методы Pusa](man/pusa_php_methods.md)
