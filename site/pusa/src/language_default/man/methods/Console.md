# Console

Направляет сообщение $AMessage в консоль браузера. Сообщение будет выведено вне
зависимости от вызовов (Debug)[#debug].

## Аргументы
- string $AMessage - текстовое сообщение для вывода в консоль.

## Результат
- $this

## Ссылки
- [Исходный код](https://gitlab.com/catlair/pusa/-/blob/main/php/core/pusa_core.php)
- [Методы Pusa](man/pusa_php_methods.md)
