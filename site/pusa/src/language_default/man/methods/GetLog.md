# GetLog

Возвращает текущий объект TLog для логирования.

## Аргументы
- нет

## Результат
- TLog

## Пример
```
$this                                   /* TPusa */
-> GetLog()                             /* Return log */
-> Info( 'Information data' )           /* New information string */
-> Param( 'Integer value', 11 );        /* Print to log parameter Integer value = 10 */
```

## Ссылки

- [Исходный код](https://gitlab.com/catlair/pusa/-/blob/main/php/core/pusa_core.php)
- [Методы Pusa](man/pusa_php_methods.md)


