# Pusa-PHP Web без JS-Front

[Концепция Pusa](site/pusa/src/language_ru/man/pusa.md) переносит всю разработку на Back-end,
используя чистый REST. Разработка JS для Front не требуется. Управление браузером, DOM элементами,
бизнеслогикой выполняются на Back-end без хранения состояния клиента.

## Быстрый страрт

Вы можете создать сайт на движке Pusa одним из двух методов:

- для ознакомления использовать [готовый Docker контейнер](https://hub.docker.com/repository/docker/catlairnet/pusa_ubutu_nginx_php_ssh);
- для реальной работы [развернуть Linux+Nginx+PHP локально](/site/pusa/src/language_default/man/pusa_php.md).

## Используем контейнер по шагам

1. [Подготовка]()
0. [Запуск контейнера]()
0. [Авторизация в контейнере]()
0. [Папка сайта]()
0. [Создание контроллера]()
0. [Создание контента]()
0. [Создание домена]()
0. [Файловые права]()
0. [Проверка результатов]()



### Подготовка

Для запуска новго сайта потребуется установленная ОС ( Ubuntu, Debian, Windows ) на локальном хосте,
а так же [Docker](http://docker.com). Установка не предусматривают никаких команд за исключением прямой работы в консоли.
Например установка docker в Ubuntu выполняется командой:
```
sudo apt install docker docker.io
```

**Внимание:** Локальный порт 80 должен быть свободен от иных приложений.

### Запуск контейнера

Скачиваем и запускаем [контейнер Pusa](https://hub.docker.com/r/catlairnet/pusa_ubutu_nginx_php_ssh) командой:
```
sudo docker run -d -p 80:80 -p 22022:22022 catlairnet/pusa_ubutu_nginx_php_ssh:latest
```
**Результат**: После установки в браузере возможно открыть ваш локальный сайт Pusа:
- http://localhost
- http://127.0.0.1

Контейнер не удаляется после остановки командой **docker stop [ID]** и может быть
запущен вновь в предыдущем состоянии.


### Авторизация в контейнере

Подключитесь к контейнеру через ssh на порт 22022. Следуйте рекомендациям ssh.
```
ssh -p 22022 root@127.0.0.1
```
Пароль
```
root
```
Для смены пароля потребуется указать один раз прежний пароль **root** и два раза новый пароль.

**Далее работаем внутри контейнера.**



### Папка сайта
Pusa размещена в папке **/var/www/html/catlair/site**, далее именуем ее как **[ROOT]**.

Каждый сайт Pusa имеет свою директорию для хранения в общем каталоге сайтов **[ROOT]/site**.
Новый сайт будет называться **my_new_site** и размещаться в папке **[ROOT]/site/my_new_site**.



### Создание контента

[Мультязыковой текстовый контент](/site/pusa/src/language_ru/man/multilang.md)
размещается в папке **[ROOT]/site/my_new_site/src**. Pusa ищет контент в файле соответсвующего языка,
а при отсутствии в папке умолчального языка **language_default**. Создадим ее:
```
mkdir -p /var/www/html/catlair/site/my_new_site/src/language_default
```
Cоздадим новый файл:
```
nano /var/www/html/catlair/site/my_new_site/src/language_default/Main.html
```
```
site<button type="button" class="Button">
    Click me
</button>
```


### Создание контроллера

Все PHP контроллеры Pusa размещаются в папке **[ROOT]/site/my_new_site/php/pusa**. Создадим ее:
```
mkdir -p /var/www/html/catlair/site/my_new_site/php/pusa
```
Добавим новый контроллер в файл:
```
nano /var/www/html/catlair/site/my_new_site/php/pusa/Main.php
```

```
<?php
namespace catlair;

class Main extends TWebPusa
{
    /* Page init */
    public function Init()
    {
        return $this
        -> FocusBody()
        -> DOMTemplate( 'Main.html')
        -> FocusChildren( 'Button' )
        -> Event( 'Main', 'ButtonClick' );
    }

    /* The button click callback body */
    public function ButtonClick()
    {
        return $this
        -> FocusBody()
        -> DOMContent( 'Hello. It is works without JS.' );
    }
}
```
Контроллер создан.



### Создание домена

[Конфигурационные файлы доменов](site/site_default/src/language_ru/man/domain_config.md)
Pusa хрантся в папке **[ROOT]/domain**.
Имя файла совпадает с именем домена. Каждый файл содержит конфигурацию для одного домена в формате json.
Пример: настройки для домена localhost хранятся в файле **[ROOT]/domain/localhost**.

Настроим домен **localhost** на наш новый сайт **my_new_site**.

1. Для сайтов необходм индивидуальный ключ шифрования пользовательских сессий.
Он содержится в ключе **SSLKey**. Вы можете создать новый ключ шифрования cli командой:
```
php -r 'echo base64_encode(openssl_random_pseudo_bytes( 32 )).PHP_EOL;'
```
Результатом будет строка, и ее надо положить в ключ **SSLKey**:
```
2ByRiqZLal11xBuvTFPHfQcro/g5jwLc1Yjvc0arSmU=
```

2. Ключ **IDSite** указывает на используемый сайт. Значение должно быть **my_new_site**.

3. Изменим файл:
```
nano /var/www/html/catlair/domain/localhost
```
```
{
    "IDSite" : "my_new_site",
    [...]
    "SSLKey" : "2ByRiqZLal11xBuvTFPHfQcro/g5jwLc1Yjvc0arSmU=",
    [...]
```

### Файловые права

Сервер nginx работает от имени **www-data**. Необходимо предоставить ему возможность
чтения и записи в директорию [ROOT]/site/**my_new_site**. Выполните команду:
```
chown -R www-data:www-data /var/www/html/catlair/site/my_new_site/
```
Таким образом для папки и ее потомков будут установлены владелец и группа **www-data**.
Позднее вы можете настоить права в соотетствии с требованиями безопасности.


### Проверка результатов

Cсылка http://localhost должна вернуть страницу с кнопкой. Нажатие на кнопку заменит контент
страницы надписью "Hello. It is works without JS." без перезагрузки страницы.

Это правда. Вы не использовали JS для обработки клика.



## Ссылки

- [Готовый docker контейнер](https://hub.docker.com/r/catlairnet/pusa_ubutu_nginx_php_ssh);
- [Определения](main/site/pusa/src/language_ru/man/glossary.md);
- [Технология Pusa](site/pusa/src/language_ru/man/pusa.md);
- [Протокол Pusa](site/pusa/src/language_ru/man/pusa_protocol.md);
- [Catlair (библиотеки WEB разработки для Pusa)](site/site_default/src/language_default/man/catlair.md);
- [Специфика реализации](site/pusa/src/language_default/man/pusa_php_specifics.md);
- [PHP методы Pusa](site/pusa/src/language_default/man/pusa_php_methods.md);
- [Разработчики и участники](site/pusa/src/language_default/man/pusa_team.md);
- [Сайт pusa.dev](https://pusa.dev).
