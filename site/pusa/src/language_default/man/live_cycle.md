# Жизненный цикл запроса

Согласно [концепции Pusa](man/pusa.md), цикл взаимодейтсвия Front-Back начинается с cобытия
браузера. PusaFornt направляет запросы через AJAX на PusaBack. Поступивший запрос содержит указание на контроллер и
метод для обработки вызова. Начиная с момента вызова контроллера работа передатся прикладному приложению. Использование
концепци MVC для прикладного приложения рекомендовано, однако разработчик может организовать внутреннюю архитектуру на
свое усмотрение.

Cntoller содержит публичные методы, которые принимают [параметры запроса](man/request_params.md) и
передают управление Model или View, в зависимости от ситуации. Modle осуществляет обмен с источниками данных и возвращает результат.
Model может вернуть результат в контроллер или передать управление во View.

View на основании полученых аргументов формирует директивы согласно [протокола](man/pusa_protocol.md).
PusaBack направляет директивы в ответе.

PusaFront получив директивы, выполняет их, внося изменения в DOM и производя иные действия в браузере.
Именно на этой стадии выполяется "тяжелая работа" c DOM.

## Общая схема

```mermaid
flowchart TB

subgraph "Back"
    Datasources[(DB)]
    PusaBack
    subgraph MVC
        Model
        View
        Controller
    end
end

subgraph "Front"
    DOM
    PusaFront
end


DOM -->|Event| PusaFront
PusaFront -->|Content| DOM

PusaBack -->|Directives| PusaFront
PusaFront -->|Request| PusaBack
PusaBack -->|Request|Controller
Model <-.->|Data| Datasources
Model --> View
View -->|Directives|PusaBack
Controller -->|Arguments| Model
```

## MVC

Pusa допускает некоторое изменение в концепции MVC так как часть запросов не требут обращения к Model.
Стандартная схема:


```mermaid
flowchart TB

subgraph Back
   view
   model
   controller
end

subgraph front
    user
end

view-->|sees|user
model-->|updates|view
user-->|uses|controller
controller-->|manipulates|model
```
Модифицированная схема содержит возможность вызова из Controller кода View минуя Model.

```mermaid
flowchart TB

subgraph Back
   view
   model
   controller
end

subgraph front
    user
end

view-->|sees|user
model-->|updates|view
user-->|uses|controller
controller-->|manipulates|model & view
```

## Ссылки
- [Подробная схема запрос-ответ](man/php_scheme.md)
- [Концепции Pusa](man/pusa.md)
- [Протокол Pusa](man/pusa_protocol.md)
