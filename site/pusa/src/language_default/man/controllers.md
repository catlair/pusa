# Контроллеры Pusa

Контроллер содержит основной функционал приложения. При работе с Pusa большинство задач решается в контроллерах.
Каждый сайт может иметь собственные контроллеры или использовать общие. Выбор контроллера происходит автоматически
по имени в POST параметре Pusa. При поступлении запроса на PusaBack движек проверяет наличие POST параметра
**Pusa**. Получает из него имя контроллера и пытается загрузить библиотеку.

## Контроллер сайта

Контроллеры сайтов размещены в папке **[ROOT]/site/php/pusa/[CONTROLLER].php** где CONTROLLER - имя контроллера
в CamelCase. В случае если файл не обнаружен, Pusa попытается загрузить общий контроллер.

## Общие контроллеры

Общие контрллеры размещены в папке **[ROOT]/php/pusa/[CONTROLLER].php**.
Если приложению не удается найти общий контроллер то возвращается ошибка.

```mermaid
flowchart TB

Begin((Begin))
Specific["Load from<br>[ROOT]/site/php/pusa/[CONTROLLER].php"]
Share["Load from<br>[ROOT]/php/pusa/[CONTROLLER].php"]
Error
Method{"Does the<br>method<br>exist?"}
Execute["Method<br>execute"]
End((End))

Begin --> Specific
Specific -->|No|Share
Share -->|No|Error
Share -->|Yes|Method
Specific -->|Yes|Method
Error --> End
Method -->|No|Error
Method -->|Exists|Execute
Execute --> End
```

## Структура контроллера

Каждый контроллер наследуется от своего базового класса.
Имя контроллера всегда записывается в CamelCase и полностью соответствует
имени файла контроллера.

Пример минимального кода контроллера **Main** для сайта *my_site* в файле
**[ROOT]/site/my_site/php/pusa/Main.php**:
```
<?php
namespace catlair;

class Main extends TWebPusa
{
}
```

Таким образом клас *Main* имеет следующую цепочку наследования:

**Main**
extends [TWebPusa](php/web/web_pusa.php)
extends [TPusa](php/core/pusa.php)
extends [TPusaCore](php/core/pusa_core.php)
extends [TResult](php/core/result.php).

## Методы

Методы контроллеров описываются как публичные функции класса.
Не обязательно, но желательно каждый метод должен возвращать $this.
Таким образом реализуется возможность последующего цепного вызова
методов контроллера.

### Пример

Описан контроллер **Main** с методом **Payload**.
```
<?php
namespace catlair;

class Main extends TWebPusa
{
    public function Payload()
    {
        /* Функционал контроллера */
        return $this;
    }
}
```


## События

В пользовательском контроллере могут быть переопределены следующие события:
- OnBeforeRun - событие будет вызвано до вызова любого метода.
- OnAfterRun - событие будет вызвано после вызова любого метода.

### Пример
```
<?php
namespace catlair;

class Main extends TWebPusa
{
    public function OnBeforeRun()
    {
        /* Код будет вызван до любого метода */
        return $this;
    }

    public function OnAfterRun()
    {
        /* Код будет вызван после любого метода */
        return $this;
    }

    public function Payload()
    {
        /* Функционал контроллера */
        return $this;
    }
}
```
