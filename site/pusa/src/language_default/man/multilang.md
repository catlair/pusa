# Мультиязыковая поддержка

## Определение

Pusa имеет встроенную мультиязыковую поддержу и наследование контента на библиотеках Catlair.
Каждый текстовый шаблон и графическое изображение для каждого сайта может быть написано на произвольном языке.
Для файловой реализации все шаблоны для каждого сайта IDSite размещены в языковой папке:
```
[ROOT]/site/[IDSite]/src/[IDLanguage]
```
где ITLanguage - идентификтаор языка.

Для каждого языка создается индивидуальная папка от которой далее и осуществляется поиск шаблона по
идентификатору. Пример пути шаблона **example/main.html** для сайта **test_site** на языке **language_en**.
```
[ROOT]/site/test_site/src/language_en/example/main.com
```

## Иерархия запроса шаблонов

При построении контента [шаблонизатор](site/site_default/src/language_default/man/templater.md)
выполняет следующие действия:
- пытается запросить контент на указанном языке для текущего сайта;
- при отсутсвии пытается запросить контент на умолчальном языке (language_default) для текущего сайта;
- при отсутсвии пытается запросить контент на указанном языке для умолчального сайта (site_default);
- при отсутсвии пытается запросить контент на умолчальном языке (language_default) для умолчального (site_default) сайта;
- при отсутсвии возвращается ошибка об отсутсвии контента.

Схема запроса шаблона **hello.html** на языке **language_en** для сайта **my_site**.

```mermaid
graph TD
    sl["[ROOT]/site/my_site/src/language_en/hello.html"]
    sld["[ROOT]/site/my_site/src/language_default/hello.html"]
    sdl["[ROOT]/site/site_defult/src/language_en/hello.html"]
    sdld["[ROOT]/site/site_default/src/language_default/hello.html"]

    request-->sl
    sl-->|-|sld
    sl-->|+|result
    sld-->|-|sdl
    sld-->|+|result
    sdl-->|-|sdld
    sdl-->|+|result
    sdld-->|+|result
    sdld-->|-|error
```

Таким образом релaизуется **наследование** контента для сайтов и языков. Разработчик
может использовать котент из других сайтов. К примеру шаблон
[[ROOT]/site/site_default/src/language_default/index.html](/site/site_default/src/language_default/index.html)
можно не созадавть заново для каждого сйта. Будет использован умолчальный.

## Определенеи языка

При каждом очередном запросе язык может быть получен из следующих источников:
- [Сессионная переменная](site/pusa/src/language_ru/man/session.md) пользователя;
- [Настройка](site/site_default/src/language_ru/man/domain_config.md) для текущего домена;
- Умолчальный язык для текущего домена;
- Константа language_default.

Определение языка при каждом запросе пользователя осуществляется в [ROOT]/php/web/wep.php

## Переключение языка

Для текущей сессии переключение языка осуществляется вызовом для контроллера $this:
```
$this                           /* Current controller or Pusa controller*/
-> GetWeb()                     /* Return Web object */
-> SetIDLang( 'language_en' );  /* Set lanaguage */
```
После изменения языка он будет сохранен в сессионной переменной.
При очередном обращении пользователя, контент будет построен на указанном языке.

## Ссылки

- [Catlair](site/site_default/src/language_default/man/catlair.md)
- [Шаблонизатор](site/site_default/src/language_default/man/templater.md)
- [Реализация шаблонизатора](main/php/web/web.php)

