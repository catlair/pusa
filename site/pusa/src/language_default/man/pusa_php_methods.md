# Методы Pusa PHP

## Ссылки

- [Определения и термины](site/pusa/src/language_ru/man/glossary.md);
- [Специфика реализации](site/pusa/src/language_default/man/pusa_php_specifics.md);
- [Протокол Pusa](site/pusa/src/language_ru/man/pusa_protocol.md).
- [Библиотеки Catlair](site/site_default/src/language_default/man/catlair.md).

## Методы ядра [TPusaCore.php](https://gitlab.com/catlair/pusa/-/blob/main/php/core/pusa_core.php)

Методы сформированы на основе [протокола Pusa](site/pusa/src/language_ru/man/pusa_protocol.md).

### Взаимодействие с Pusa
- [GetAnswer](man/methods/GetAnswer.md) - возвращает код ответа Pusa;
- [GetCommands](man/methods/GetCommands.md) - возвращает список команд для Pusa-Front;
- [DumpCommands](man/methods/DumpCommands.md) - выводит в лог перень текущих команд для Pusa-Front.
- [GetLog](man/methods/GetLog.md) - возвращает объект логирования [TLog](/php/core/debug.php) для ведения логов;
- [GetURL](man/methods/GetURL.md) - возвращает объект TURL для текущего запроса;

### Управление браузером
- [Debug](man/methods/Debug.md) - управление выводом отладочной информации в косоль браузера;
- [Console](man/methods/Console.md) - отправляет сообщение в консоль браузера;
- [ChangeURL](man/methods/ChangeURL.md) - изменяет текущий URL браузера без перезагрузки страницы;
- [OpenURL](man/methods/OpenURL.md) - открывает указанный URL;
- [Loop](man/methods/Loop.md) - циклическое исполнение команд на стороне браузера;
- [JS](man/methods/JS.md) - отправляет код JS в браузер на исполненеи (рекомендуется только для исключительных случаев).

### Управление событиями
- [Event](man/methods/Event.md) - назначение вызова BackEnd для события DOM элемента или объектов браузера;

### Манипуляция DOM
- [DOMFocus](man/methods/DOMFocus.md) - выбирает DOM элементы в фокус для дальнейших манипуляций;
- [DOMAttr](man/methods/DOMAttr.md) - изменяет значение атрибутов фокусных DOM элементов;
- [DOMProp](man/methods/DOMProp.md) - изменение свойств фокусных DOM элементов;
- [DOMMethod](man/methods/DOMMethod.md) - вызов метода для фокусных DOM элементов;
- [DOMStyle](man/methods/DOMStyle.md) - изменение стилевых атрибутов DOM элементов (style);
- [DOMCreate](man/methods/DOMCreate.md) - создание новых DOM элементов;
- [DOMDelete](man/methods/DOMDelete.md) - удаление DOM элементов;
- [DOMTimer](man/methods/DOMTimer.md) - создание таймера с последующим вызовом Pusa-Back для фокусных DOM элементов;
- [DOMTimerStop](man/methods/DOMTimerStop.md) - сброс таймера для фокусных DOM элементов;
- [DOMSend](man/methods/DOMSend.md) - отправка содержимого фокусных DOM элементов на Pusa-Back;
- [DOMClassAdd](man/methods/DOMClassAdd.md) - добавление класса в DOM элементы;
- [DOMClassRemove](man/methods/DOMClassRemove.md) - удаление класса из DOM элементов.

### Манипуляция CSS
- [CSSAttr](man/methods/CSSAttr.md) - установка атрибута для именованого правила CSS.

### Pile [TPusaCore]( https://gitlab.com/catlair/pusa/-/blob/main/php/core/pusa_core.php )

Именованный массив строк (куча) для Pusa на браузере. Обеспечивает хранение и обмен значениями
между различными элементами браузера без обращения к Back. Сохраняет значения на протяжении жизни загруженной страницы.
Перезагрузка страницы очищает Pile.

- [PilelePut](PilePut.md) - установка значения для элемента объекта Pile.
- [PileReplace](PileReplace.md) - замена фрагментов в выражении Pile с именем ключая Name (не реализовано).
- [DOMToPile](DOMToPile.md) - помещение значений атрибутов и свойств фокусных DOM элементов в Pile.
- [DOMFromPile](DOMFromPile.md) - заполнение значений атрибутов и свойств фокусных DOM элементов из Pile.
- [PileToClipboard](PileToClipboard.md) - помещение значения Pile в Clipboard браузера.

## Расширение функционала ядра [TPusa]( https://gitlab.com/catlair/pusa/-/blob/main/php/core/pusa.php )
- [DOMSelect](man/methods/DOMSelect.md) - выбирает DOM элементы на основе предопределенных селекторв в фокус;
- [SetData](man/methods/SetData.md) - устанавливает значение из HTML атрибута data- для событийного DOM элемента;
- [GetData](man/methods/GetData.md) - возвращает нетипизированное значение из HTML атрибута data- для событийного DOM элемента;
- [GetDataString](man/methods/GetDataString.md) - возвращает строковое значение из HTML атрибута data- для событийного DOM элемента;
- [GetDataInteger](man/methods/GetDataInteger.md) - возвращает целочисленное значение из HTML атрибута data- для событийного DOM элемента;
- [GetDataBoolean](man/methods/GetDataBoolean.md) - возвращает логическое значние из HTML атрибута data- для событийного DOM элемента;
- [ReloadPage](man/methods/ReloadPage.md) - перегружает страницу с текущим URL;
- [DOMValue](man/methods/DOMValue.md) - изменение значения value для DOM элементов (value);
- [DOMContent](man/methods/COMContent.md) - изменение контента для DOM элементов (innerHTML);
- [DOMClear](man/methods/DOMClear.md) - очистка контента для фокусных DOM элементов;
- [DOMLink](man/methods/DOMLink.md) - установка гиперссылки для фокусных DOM элементов;
- [DOMEventTrap](man/methods/DOMEventTrap.md) - недопускает всплытие указанного события stopPropagation();
- [EventFront](man/methods/EventFront.md) - назначение локального действия на события DOM элементов или объектов браузера;
- [EventJS](man/methods/EventJS.md) - назначение локального вызова JS на события DOM элементов или объектов браузера;
- [DOMClass](man/methods/DOMClass.md) - изменяет className для фокусных DOM элементов;
- [DOMID](man/methods/DOMID.md) - изменяет id для фокусных DOM элементов;
- [DOMHide](man/methods/DOMHide.md) - скрывает фокусные DOM элементы;
- [DOMShow](man/methods/DOMShow.md) - показывает фокусные DOM элементы;
- [DOMVisible](man/methods/DOMVisible.md) - показывает или скрывает фокусные DOM элементы в зависимости от аргумента;
- [DOMEnable](man/methods/DOMEnable.md) - включает фокусные DOM элементоы;
- [DOMDisable](man/methods/DOMDisable.md) - отключает фокусные DOM элементы;
- [FocusSelf](man/methods/FocusSelf.md) - переносит фокус на текущий событийный элемент;
- [FocusBody](man/methods/FocusBody.md) - переносит фокус на body документа;
- [FocusParent](man/methods/FocusParent.md) - переносит фокус на непосредсвтенных родителей фокусных элементов;
- [FocusChildren](man/methods/FocusChildren.md) - переносит фокус на потомков в фокусных элементов по DOM;
- [FocusParents](man/methods/FocusParents.md) - переносит фокус на искомых родителей фокусны элементов;
- [DOMCSSAdd](man/methods/DOMCSSAdd.md) - добавляет новый стилевой контент из текста.

## Расширение функционала [TWebPusa](https://gitlab.com/catlair/pusa/-/blob/main/php/web/web_pusa.php)

- [GetWeb](man/methods/GetWeb.md) - возвращает текущий объект класса [TWeb](https://gitlab.com/catlair/pusa/-/blob/main/php/web/web.php);
- [GetTemplate](man/methods/GetTempalte.md) - возвращает контент шаблона из по идентификaтору;
- [GetValue](man/methods/GetValue.md) - возвращает нетипизированной значение POST запроса по имени ключа;
- [GetString](man/methods/GetString.md) - возвращает строковое значение POST запроса по имени ключа;
- [GetInteger](man/methods/GetInteger.md) - возвращает целочисленное значение POST запроса по имени ключа;
- [GetFloat](man/methods/GetFloat.md) - возвращает значение c плавающей точкой POST запроса по имени ключа;
- [GetBoolean](man/methods/GetBoolean.md) - возвращает логическое значение POST запроса по имени ключа;
- [DOMTemplate](man/methods/DOMTemplate.md) - устанавливает контент фокусных элементов из шаблона по идентификатору используя шаблонизатор;
- [DOMCSSAddFromTemplate](man/methods/DOMCSSAddFromTemplate.md) - добавляет новый стилевой контент из шаблона по идентификатору используя шаблонизатор;

### Работа с состоянием
Наследуется от [TResult](https://gitlab.com/catlair/pusa/-/blob/main/php/core/result.php)
- IsOk - проверка состояния Pusa;
- SetCode - устанавливает код ошибки;
- SetResult - устанавливает код ошибки и сообщение;
- GetCode - возвращает код ошибки;
- ResultTo - копирует сообщение и код ошибки в указанный объект;
- ResultFrom - копирует сообщение и код ошибки из указанного объекта.
