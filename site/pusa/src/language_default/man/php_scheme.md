# Cхема запрос-ответ для приложения Pusa

## Подготовка

- PHP FPM - стандартный вызов PHP со сторны web сервера. PHP получает $_GET, $_POST, $_SERVER, $COOKIES массивы.
- [index.html](root/index.php) - стартовый файл для начала работы с приложением. Создает объект [TWeb](php/web/web.php), инициализируется система логирования.
- Read Domain Config - читается [файл настроек](site/site_default/src/language_ru/man/domain_config.md) для запрошенного домена.
- Engine defined? - проверка определения [движка](site/pusa/src/language_ru/man/engines.md) в файле настроек домена.
- Load CustomEngine - загружается библиотека указанного движка, далее он клонируется от имеющегося TWeb и приложение работает на клоне.
- Use Web Engine - в случае если движек в конфиге не определен используется уже созданный TWeb.
- Decrypt session - из переменных $_COOKIES читаются и расшифровываются [сессионные состояния](site/pusa/src/language_ru/man/sessions.md) сайта, языка и пользователя.
- User defined? - проверка определен ли пользователь из сесситнных настроек.
- Guest - в случае если пользователь не опеределен используется гость.
- User - при наличии логина в сессионных состояних далее он используется для как авторизованный.
- Language defined? - проверяется текущий язык для сессии. [Подробнее о мультиязыковой поддержке](site/site_default/src/language_default/man/multylanguage.md#определенеи-языка).
- Default Language - при отсутсвии языка устанавливается умолчальный язык из файла конфигурации домена, а при отсутсвии language_default.
- Custom Language - далее используется выбранный из сессионных переменных язык.
- User Log - запускается система логирования для сайта и пользователя. В дальнейшем все логи будут направлены в специфический каталог для пользователя и сайта.

## Построитель

- Pusa requested? - запрошено ли обращение к контреллерам Pusa.
- Use storage? - запрошено ли обращение к хранилищу файлов.
- Return file - возвращается файл. Права не проверяются.
- Image requested? - запрошено ли [графическое изображение](site/pusa/src/language_ru/man/images.md).
- Return Image - возвращается изображение. Права не проверяются.
- Buld content - запускается построитель контента на [шаблонизторе Catlair](site/site_default/src/language_default/man/templater.md).

## Pusa
- Does the controller exists - в случае, если контрлер Pusa запрошен, проверяется его наличие.
- Controller - в парадигме MVC запуск контрллера Pusa при его наличии.
- View - контрлер Pusa может вызвать построитель форм View.
- Model - контрлер Pusa может вызвать модель Model.
- Data source - [источник данных](site/pusa/src/language_ru/man/datasource.md) с которым работает View.
- Pusa Error - при отсутсвии контрллера формируется ошибка Pusa.
- Pusa Answer - формируется ответ Pusa, содержащий набор директив, состояние вызова, сообщение об ошибках.


```mermaid
graph

Begin((Begin))
PHP[PHP FPM]
End((End))


Datasources[(Data<br>Sources)]


subgraph PusaController[Pusa Application]
    Controller
    View
    Model
end

subgraph pusa_core.php
    LoadController{Does<br>the controller<br>exist}
    PusaAnswer[Pusa<br>answer]
    PusaError[Pusa Error]
end

subgraph "web.php (Catlair)"
    Error
    ReadDomain[[Read<br>Domain<br>Config]]
    SelectEngine{Engine<br>defined?}
    SelectSite{Site<br>defined?}
    SelectUser{User<br>defined?}
    UserLog[User<br>log]
    SetGuest[Guest]
    SetUser[User]

    SelectLang{Language<br>defined?}
    SetLangDefault[Default<br>Language]
    SetLangCustom[Custom<br>Language]

    LoadEngine[Load<br>Custom<br>Engine]
    UseWebEngine[Use<br>WEB<br>engine]
    PusaExists{Pusa<br>requested?}
    Storage{Use<br>storage?}
    ReturnFileFromStorage[Return<br>file]

    Image{Image<br>requested?}
    ReturnImage[Return<br>image]

    BuildContent[Build<br>content]
    Session[Decrypt session]
    Answer[Send<br>answer]
end



Begin -->|Request| PHP
PHP --> index.php
index.php --> ReadDomain
ReadDomain -->|No| Error
ReadDomain -->|Yes| SelectEngine
SelectEngine -->|Yes| LoadEngine
SelectEngine -->|No| UseWebEngine
UseWebEngine --> Session
LoadEngine --> Session

Session --> SelectUser
SelectUser -->|No| SetGuest
SelectUser -->|Yes| SetUser
SetGuest --> SelectSite
SetUser --> SelectSite

SelectSite -->|No| Error
SelectSite -->|Yes| SelectLang

SelectLang -->|No| SetLangDefault
SelectLang -->|Yes| SetLangCustom

SetLangDefault --> UserLog
SetLangCustom --> UserLog

UserLog --> PusaExists

PusaExists -->|No| Storage
PusaExists -->|Yes| LoadController

LoadController -->|Yes| Controller
LoadController -->|No| PusaError

Controller --> View
Controller --> Model
View --> PusaAnswer
Model --> View
Model .- Datasources
PusaAnswer -->|Directives| Answer

Storage -->|No| Image
Storage -->|Yes| ReturnFileFromStorage
ReturnFileFromStorage --> |Binary data| Answer

Image -->|No| BuildContent
Image -->|Yes| ReturnImage
ReturnImage -->|Binary data| Answer

Error -->|HTML error| Answer
BuildContent -->|Text content| Answer
PusaError --> PusaAnswer
Answer -->|Answer| End

```
