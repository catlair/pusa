# Разработчики Pusa Web без JS-Front

* still@itserv.ru - идея, PHP, JS реализация;
* igorptx@gmail.com - PHP реализация;
* petr@gmail.com - devops.

# Благодарности
* artem.bashkatoff@yandex.ru, Башкатов Артём - рецензия, обсуждение реализации файловой загрузки;
* https://www.opennet.ru/~fuggy - рекомендации по лицензированию;
* https://github.com/booydar - тестирование разворачивания проекта, замечания.

## Ссылки
- [Оглавление](man/head.md)
- [Методы Pusa](man/pusa_php_methods.md)
- [Сайт pusa.dev](https://pusa.dev)
