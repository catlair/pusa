# Pusa-PHP Web без JS-Front

[Концепция Pusa](man/pusa.md) переносит всю разработку на Back-end, используя чистый REST.
Разработка JS для Front не требуется. Управление браузером, DOM элементами,
бизнеслогикой выполняются на Back-end без хранения состояния клиента.

### Среда

Для ознакомления c Pusa можно воспользоваться
[Готовым docker контейнером](https://hub.docker.com/r/catlairnet/pusa_ubutu_nginx_php_ssh)
или установить из GIT.

Для ручной установки потребуются следующие средства:

- Ubutu 18.04 (Debian)
- Nginx/1.14.0
- PHP7.2

Ручная установка:

```
apt -yq install nginx \
php-fpm \
php-mysql \
php-xml \
php-imap \
php-readline \
php-curl \
php-gd \
php-json \
php-mbstring \
php-opcache \
php-zip \
php-pgsql
```

### GIT

Для стандартного размещения скриптов nginx /var/www/html выполните:

```
cd /var/www/html
git clone https://gitlab.com/catlair/pusa.git
```
Папка /var/www/html/pusa содержит код php проекта и далее именуется как [ROOT].

### Nginx

1. Все запросы Nginx должны быть перенаправлены на файл [ROOT]/root/index.html.
Указанный файл - единственный в директории и изолирован для предотвращаения доступа к коду проекта.
0. Файл конфигурации должен содержать параметр fastcgi_param CATLAIR_PATH '[ROOT]';

### PHP

Служба PHP www-data минимально должна иметь файловый доступ на чтение и запись к папками и файлам от [ROOT]:

```
- [ROOT]/domain     r-- - настройки доменов;
- [ROOT]/php        r-- - исходный код Pusa на Php;
- [ROOT]/root       r-- - корневая папка, содержащая единственный файл index.php;
- [ROOT]/site       r-- - каталог сайтов на текущем сервере;
- [ROOT]/cli        r-x - исполняемый файл для cli интерфейса;
- [ROOT]/log        rw- - отладочные логи Pusa;
- [ROOT]/LICENSE    r-- - лицензия для Pusa.
```

### Домены

Для каждого домена вашего сайта должн быть создан отдельный конфигурационный файл в папке **[ROOT]/domain**.
Для каждого домена должен быть настрен свой специфический SSLKey, используемый для шифрования сессионной и иной информации.

Подробная инструкция размещена [тут](https://gitlab.com/catlair/pusa/-/blob/main/domain/README.md).

### Завершение

После завершения настроек необходимо запустить службы:
```
service nginx start
service php7.2-fpm start
```

### Логирование

Логи размещаются в каталоге **[ROOT]/log.** Каждый сайт создает свой каталог имя которого совпадает с доменым именем
сайта. Таким образом сайт pusa.catlaif.net будет писатьлоги в папку **[ROOT]/log/pusa.dev**.

Включение логов осуществляется в [настройках домена](https://gitlab.com/catlair/pusa/-/blob/main/domain/README.md).

## Ссылки

- [Руководство](man/head.md)
- [Готовый docker контейнер](https://hub.docker.com/r/catlairnet/pusa_ubutu_nginx_php_ssh);
- [Определения](man/glossary.md);
- [Технология Pusa](man/pusa.md);
- [Протокол Pusa](man/pusa_protocol.md);
- [Специфика реализации](man/pusa_php_specifics.md);
- [Методы Pusa](man/pusa_php_methods.md);
- [Разработчики и участники](man/pusa_team.md);
- [Сайт pusa.dev](https://pusa.dev).

