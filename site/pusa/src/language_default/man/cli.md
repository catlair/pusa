# Интерфейс командной строки (cli)

## Ссылки

- [PHP реализация](man/pusa_php_methods.md);

Интерфейс командной строки Pusa предназначен для отладки кода и исполнения сервисных процедур.
Исполняемый файл размещен в корне проекта **[ROOT]/cli**. Запуск выполняется из любой точки файловой системы
с абсолютным путем например:
```
/var/www/html/catlair/cli
```
или из папки проекта **[ROOT]**:
```
./cli
```

Исполняемый файл cli по сути является намордником над основым индексным файлом проекта
[[ROOT]/root/index.php](https://gitlab.com/catlair/pusa/-/blob/main/root/index.php)
и при его запуске выполняются тe же самые действия, что и при запуске движка через вебсервер.

Интерфейс cli принимает входящие параметры в длинной нотации:
```
./cli --ParamName1="Value1" --ParamName2="Value2"
```

Командный интерфейс всегда ожидает на вход как минимум один параметр **--IDDomain**, необходимый для получения
базовой конфигурации окружения. При запуске от вебсервера, параметр читается из ключа $_SERVER[ 'DOMAIN_LOCALHOST' ].

В случае, если обязателный параметр отсутствует, cli будет запрашивать его в интерактивном режиме.
Каждый параметр имеет значение по умолчанию, которое будет использовано при отсутсвии ввода:
```
./cli
Choose the domain for request [IDDomain = localhost]:
```

Указание обязательного параметра избавляет от необходимости его интерактивного ввода.

```
./cli --IDDomain=localhost
```

## Логирование cli

При запуске cli лог всегданаправляется в STDOUT. Логирование включается в параметре **debug** файла настроек домена.

## Pusa cli

Из командного интерфейса возможен запуск контроллеров Pusa. Пример:
```
./cli --IDDomain=localhost --Pusa=Main --Method=Init
```
В примере для сайта домена **localhost** будет вызван контроллер Pusa из класса **Main** c методом **Init**.

## CLI параметры

### [Движек Web](man/engines.md)

- --IDDomain - домен для чтения конфигурации (обязательный) в формате --IDDomain=my.domain.com
- --IDLang - идентификатор языка --IDLang=language_en
- --IDUser - логин пользователя для запроса --IDUser=my_user
- --IDSite - идентификатор сайта для запроса --IDSite=my_site. Таким образом может быть открыт любой сайт для любого домена.
- --URI - строка GET запроса в формате --URI="?&param1=value1&param2=value2"
- --TypeContent - формат ответа HTML(по умолчанию), JSON, XML(не работает) --TypeContent=JSON
- --Pusa - имя класса для вызова контроллера Pusa
- --Method - имя метода для контроллера Pusa. Имеет значение при наличии флага --Pusa.

### [Движек WebDB](man/engines.md)

- --DatasourceActive - устанавливает активную конфигурацию для источника данных. При отсутсвии ключая используется значение из файла конфигурации парамера Active.

Все иные параметры в длинной нотмации будут использованы как POST параметры. Все параметры в короткой нотации будут проигнорированы.

## POST параметры

Все переданные через длинную нотацию cli параметры будут использоваться как POST параметры.
Доступ к ним из контроллеров Pusa единообразен для cli и fpi через методы:
- [GetValue](site/pusa/src/language_default/man/pusa_php_methods.md#getvalue)
- [GetString](site/pusa/src/language_default/man/pusa_php_methods.md#getstring)
- [GetInteger](site/pusa/src/language_default/man/pusa_php_methods.md#getinteger)
- [GetFloat](site/pusa/src/language_default/man/pusa_php_methods.md#getfloat)
- [GetBoolean](site/pusa/src/language_default/man/pusa_php_methods.md#getboolean)

### Пример контроллера
```
class MyPusaController extends TWebPusa
{
    public function MyMethod()
    {
        return $this
        -> GetLog()
        -> Info( $this -> GetString( 'Param1', 'Value1 not exists' ))
        -> Info( $this -> GetString( 'Param2', 'Value2 not exists' ));
    }
}

```
### Пример cli вызова:
```
./cli --IDDomain=localhost --Pusa="MyPusaController" --Method="MyMethod" --Param1="Value exists"
```
