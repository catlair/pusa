# Параметры REST запроса

Pusa резервирует следующие параметры-аргументы для входящих запросов:

## GET параметры
- &template string - возвращается текст шаблона по идентфикатору относительно пути **[ROOT]/site/[SITE]/src**. Пример: https://pusa.dev/?template=js/pusa.js
- &image string - (устарело) возвращается изображение по идентфикатору относительно пути **[ROOT]/site/[SITE]/src**
- &file string - (устарело) возвращается файл по идентификатору относительно пути **[ROOT]/site/[SITE]/src**

## GET или POST параметры
- &Pusa string - имя модуля для запуска контроллера Pusa из каталога **[ROOT]/site/[SITE]/php/pusa**. Пример https://pusa.dev/?Pusa=Main&Method=Refresh
- &Method string - метод вызова в Pusa контроллере. Игнорируется, если отсутсвует параметр &Pusa.
- &call string - устаревший вызов контролера Catlair в формате controller.methd. Не рекомендуется к использованию.
- &lang string - переключение языка для сессии. Принимает произвольное значение идентификатор языка, но рекомендуется
language_default, language_en, language_ru, и тд.

## POST параметры
- IDRequest string - уникальный идентификатор запроса.
- IDGroup string - уникальный идентификатор группы запроса. В одну группу может входить несколько запросов например при отправки нескольких файлов.
- GroupSize int - количество запросов в группе.
- TypeContent string - тип контента, ожидаемого PusaFront на запрос: JSON,HTML,XML,TEXT.
- URL string - полный URL указанный в браузере на момент отправки запроса.

## COOKIES
- session - сессионная переменная, содержащая информацию об авторизованном пользователе, языке и тд.


## Информация

Все иные параметры GET, POST, COOKIES могут быть использованы разработчиками по своему усмотрению.
Pusa не планирует расширять список.

Информация о способах доступа к параметрам описана в разделе [сериализованный
доступ](site/pusa/src/language_ru/man/serialization.md).

