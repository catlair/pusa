<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

namespace catlair;

/*
    Pusa file sharing demo controller.
    All methods contains in this file.
*/



class Main extends TWebPusa
{
    const KEY_GROUP     = 'group';      /* Group key for url */
    const STORAGE       = 'storage';    /* Storage folder for [ROOT]/site/[SITE]/src/[STORAGE] */


    /*
        Initialization endpoint
        This is the main router. In this method you can create any route scheme.
        For more complex cases, it is recommended to create an autorouting scheme.
        Take a look at https://gitlab.com/catlair/pusa/-/blob/main/site/pusa/php/pusa/Main.php
    */
    public function Init()
    {
        /* Define parameters */
        $IDGroup = $this -> GetURL() -> GetString( self::KEY_GROUP );

        /* Router */
        $this -> MainPage( $IDGroup );

        return $this;
    }



    /*
        Show main page for group identifier
    */
    private function MainPage( $AIDGroup )
    {
        $this
        -> FocusBody()
        -> DOMTemplate( 'Upload.html' )
        -> FocusChildren( 'Download' )
        -> DOMHide()

        -> FocusBody()
        -> FocusChildren( 'FilesButton' )
        -> FocusChildren( 'Files' )

        -> EventFront
        (
            $this -> Clone() -> UploadStart(),
            'change'
        );

        if( empty( $AIDGroup ))
        {
            /* Hide download link */
            $this
            -> FocusBody()
            -> FocusChildren( 'Download' )
            -> DOMHide();
        }
        else
        {
            $this -> BuildFileList( $AIDGroup );
        }

        return $this;
    }



    /*
        Build file list by Group identifier
    */
    private function BuildFileList( $AIDGroup )
    {
        $Path = $this -> GetStoragePath( $AIDGroup );

        /* Prepare file list container */
        $this
        -> FocusBody()
        -> FocusChildren( 'FileList' )
        -> DOMClear();

        if( file_exists( $Path ))
        {
            /* Build file list */
            foreach( scandir( $Path ) as $FileName )
            {
                if( $FileName != '.' && $FileName != '..' )
                {
                    $this -> AddFileRecord
                    (
                        $AIDGroup,
                        clGUID(),
                        $FileName,
                        filesize( $Path . '/' . $FileName )
                    );
                }
            }

        }
        else
        {
            /* Files not found */
            $this -> DOMTemplate( 'NoFiles.html' );
        }

        return $this;
    }



    /*
        Start uploading files event
    */
    public function UploadStart()
    {
        return $this
        -> FocusBody()
        -> FocusChildren( TPusaCore::CONTAINER_FORM, TPusaCore::TAG )
        -> ReadFields( 'Main', 'UploadFile' )

        /* Clear file list */
        -> FocusBody()
        -> FocusChildren( 'FileList' )
        -> DOMContent( '' )
        /* Hide download link */
        -> FocusBody()
        -> FocusChildren( 'Download' )
        -> DOMHide()
        ;
    }



    public function UploadFile
    (
        string $IDGroup,
        string $IDRequest
    )
    {
        /* Prepare parameters */
        /* Current file name */
        $FileName       = clFileControl( $_FILES['Blob']['name']);
        /* Tempoeryr file path in local system */
        $SourceFileName = $_FILES['Blob']['tmp_name'];
        /* Received file size */
        $FileSize       = $_FILES['Blob']['size'];
        /* Read size of group ( count of files to upload ) */
        $GroupSize      = $this -> GetInteger( 'GroupSize' );
        /* Change URL with group */
        $DownloadURL    = $this -> GetURL() -> SetString( self::KEY_GROUP, $IDGroup ) -> ToString();
        /* Define the path for file storage*/
        $Path           = $this -> GetStoragePath( $IDGroup );

        /* Check free space on disk more 5mb */
        if( disk_free_space( $this -> GetWeb() -> GetSitePath() ) < 1024 * 1024 * 5 )
        {
            /* No free space */
            $this -> SetCode( 'NoFreeSpace' . $Path );
        }
        else
        {
            /* Move file in to the storage */
            if( CheckPath( $Path ) )
            {
                rename( $SourceFileName, $Path . '/' . $FileName );
            }

            /*
                Check uploading all files. If files in local sistem equal size of group then upload is complete.
                May show upload button again.
            */
            if( $GroupSize == count( scandir( $Path )) - 2 )
            {
                $this
                -> FocusBody()
                -> FocusChildren( 'FilesButton' )
                -> DOMShow();
            }

            $this
            /* Fill download link */
            -> FocusBody()
            -> FocusChildren( 'Download' )
            -> DOMShow()
            -> FocusChildren( 'DownloadLink' )
            -> DOMLink( $DownloadURL )
            -> ChangeURL( $DownloadURL )

            /* Build uplod information for file */
            -> FocusBody()
            -> FocusChildren( 'FileList' )
            -> AddFileRecord( $IDGroup, $IDRequest, $FileName, $FileSize )
            -> DOMView()

            /* Set click on button */
            -> FocusBody()
            -> FocusChildren( 'CopyLink' )
            -> EventFront
            (
                $this -> Clone()
                -> PilePut( $DownloadURL )
                -> PileToClipboard()
            )
            ;
        }

        return $this;
    }



    /*
        Add file record to DOM
    */
    private function AddFileRecord
    (
        $AIDGroup,
        $AIDRequest,
        $AFileName,
        $AFileSize
    )
    {
        return $this
        -> DOMCreate( 'div', TPusaCore::FIRST )
        -> DOMClassAdd( 'Record' )
        -> DOMTemplate( 'UploadRecord.html' )
        -> FocusChildren( 'FileName' )
        -> DOMLink
        (
            $this
            -> GetURL()
            -> SetPath([ self::STORAGE, $AIDGroup, $AFileName ])
            -> ClearParams()
            -> SetHash()
            -> ToString()
        )
        -> DOMContent( $AFileName )
        -> FocusParent()
        -> FocusChildren( 'Size' )
        -> DOMContent( clSizeToStr( $AFileSize ))
        -> FocusParent()
        -> FocusParent();
    }



    /*
        Return storage path
    */
    private function GetStoragePath( $AIDGroup )
    {
        return
        clSourceFile
        (
            self::STORAGE . '/' . $AIDGroup,
            $this -> GetWeb() -> GetIDSite(),
            $this -> GetWeb() -> GetIDLangDefault()
        );
    }
}
