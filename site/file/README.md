> Catlair PHP Copyright (C) 2021 https://itserv.ru
>
> This text is part of free software: you can redistribute
> it and/or modify it under the terms of the GNU Aferro General
> Public License as published by the Free Software Foundation,
> either version 3 of the License, or (at your option) any later version.
> This program (or part of program) is distributed in the hope that
> it will be useful, but WITHOUT ANY WARRANTY; without even the implied
> warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
> See the GNU Aferro General Public License for more details.
> You should have received a copy of the GNU Aferror General Public License
> along with this program. If not, see <https://www.gnu.org/licenses/>


# Pusa file sharing

It is small file exchange powered by the Pusa engine. The source code does not contain JS.
We are recomended use this solution for training purposes only.

## Links
- [File sharing demo](https://file.catlair.net/)
- [Docker - web without JS](https://hub.docker.com/r/catlairnet/pusa_file_sharing)
- [Pusa - web without JS](https://pusa.catlair.net/)

