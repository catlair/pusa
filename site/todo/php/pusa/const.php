<?php

namespace catlair;

class TConst
{
    const SAVE_TIMEOUT_MILLISECONDS = 800;
    const PAGE_COUNT_RECORD         = 50;

    const STATUS_WAIT               = 'Wait';
    const STATUS_DONE               = 'Done';
    const STATUS_NEW                = 'New';
}
