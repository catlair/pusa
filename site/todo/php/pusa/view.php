<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

/*
    Pusa TODO demo
    MVC View

    This module should only consist of private methods if you want to follow the MVC paradigm.
    Pusa method are not used in other Controller or Model modules.
*/

namespace catlair;


require_once 'const.php';


trait View
{
    /*
        Build main page content
    */
    private function MainPage()
    {
        return $this
        -> FocusBody()
        -> DOMTemplate( 'Main.html' )   /* Set main form */

        /* Datasource selector init */
        -> FocusChildren( 'Datasource', TPusaCore::ID )
        -> DOMValue( $this -> GetCookie( 'Datasource', 'mysql' ))
        -> Event
        (
            'Main',
            'DatasourceSelect',
            $this -> Clone() -> ReadValue( 'ADatasource' ),
            TPusaCore::CHANGE
        )

        /* Set refresh action on button */
        -> FocusBody()
        -> FocusChildren( 'Refresh' )

        /* Event on Refresh button */
        -> Event
        (
            'Main',
            'Refresh',
            /* Request the value of Search string */
            $this -> Clone()
            -> FocusParent( 'DIV', TPusaCore::TAG )
            -> FocusChildren( 'Search' )
            -> ReadProperty( 'ASearch',  'value' )
        )

        /* Event for serch input */
        -> FocusBody()
        -> FocusChildren( 'Search' )
        -> EventOnEnter
        (
            'Main',
            'Refresh',
            $this -> Clone()
            -> FocusParent( 'DIV', TPusaCore::TAG )
            -> FocusChildren( 'Search' )
            -> ReadProperty( 'ASearch',  'value' )
        )

        /* Event on window scroll */
        -> FocusWindow()
        -> Event
        (
            'Main',
            'Scroll',
            $this -> Clone()
            -> FocusBody()
            -> FocusChildren( 'Search' )
            -> ReadValue( 'ASearch' )

            -> FocusBody()
            -> FocusChildren( 'Last', TPusaCore::NAME )
            -> ReadValue( 'ALast' )

            -> FocusBody()
            -> ReadProperty( 'AOffset',  'offsetHeight' )

            -> FocusWindow()
            -> ReadProperty( 'AScroll',  'scrollY' )
            -> ReadProperty( 'AHeight',  'innerHeight' )
            ,
            TPusaCore::SCROLL,
            500
        )
        /* Call refresh */
        -> UserForm()                   /* Prepare user form with login and password */
        -> Refresh()                    /* Refresh records list */
        ;
    }



    /*
        Prepare TODO list
    */
    private function PrepareList()
    {
        $this
        /* Clear list */
        -> FocusBody()
        -> FocusChildren( 'TaskList' )
        -> DOMContent();

        if( !$this -> GetWeb() -> IsGuest() )
        {
            $this
            -> DOMTemplate( 'NewTask.html' )    /* Clear list and set default task */
            -> FocusChildren( 'NewBody' )
            -> EventFront( $this -> Clone() -> EditBegin(), TPusa::KEY_PRESS )
            /* Set Add record action */
            -> FocusParents( 'TaskList' )
            -> FocusChildren( 'Add' )
            -> Event /* Set action on button */
            (
                'Main',
                'ClickAdd',
                $this -> Clone()
                -> FocusParents( 'Record' )
                -> ReadFields()
            );
        }

        /* Reset last page */
        $this
        -> FocusBody( 'TaskList' )
        -> FocusChildren( 'Last', TPusaCore::NAME )
        -> DOMValue( 0 );

        return $this;
    }



    /*
        Prepare user form for login or logout
    */
    private function UserForm()
    {
        $this
        -> FocusBody()
        -> FocusChildren( 'UserForm' );

        if( $this -> GetWeb() -> IsGuest() )
        {
            /* Login form */
            $this
            -> DOMTemplate( 'LoginForm.html' )
            /* Set action on Login button */
            -> FocusChildren( 'BtnLogin' )
            -> Event
            (
                'Main',
                'Login',
                $this -> Clone()
                -> FocusParents( 'UserForm' )
                -> ReadFields()
            )
            /* Set action on Enter press in password field */
            -> FocusParents( 'UserForm' )
            -> FocusChildren( 'APassword', TPusaCore::NAME )
            /* Set event */
            -> Event
            (
                'Main',
                'Login',
                $this -> Clone()
                -> FocusParent( 'UserForm' )
                -> ReadFields(),
                TPusaCore::KEY_UP,
                0,
                false,
                [ '=', '@key', 'Enter' ]            /* Filter LABEL_OPERATOR_ENTER */
            );
        }
        else
        {
            /* User form */
            $this
            -> DOMTemplate( 'UserForm.html' )        /* Set login content */
            /* Action on btn logout */
            -> FocusChildren( 'BtnLogout' )
            -> Event( 'Main', 'Logout' )
            -> FocusParents( 'UserForm' )
            -> FocusChildren( 'CaptionUser' )
            -> DOMContent( $this -> GetWeb() -> GetLogin() );
        }

        return $this;
    }



    /*
        build the page with records
    */
    public function BuildPage
    (
        $ADataset,                /* List of records in dataset TData */
        int $ALast  = 0
    )
    {
        return $this
        -> FocusBody()
        -> FocusChildren( 'TaskList' )
        -> BuildRecords( $ADataset )
        -> FocusBody()
        -> FocusChildren( 'Last', TPusaCore::NAME )
        -> DOMValue( $ALast + $ADataset -> GetCount() );
    }



    /*
        Build records to TODO list container
    */
    private function BuildRecords
    (
        $ADataset
    )
    {
        $Template = $this -> GetTemplate( 'Task.html' );
        return $this
        -> Loop
        (
            $this
            -> Clone()
            /* Create record container */
            -> DOMCreate()
            -> DOMClass( 'Record' )
            -> DOMID( 'Record_%ID%' )         /* For rebuild */
            -> BuildRecord( $Template )
            -> FocusParents( 'TaskList' )
            ,
            $ADataset
        );
    }



    /*
        Build one record in Container.
        Conteiner should be focused before calling BuildRecord.
    */
    private function BuildRecord( $ATemplate )
    {
        return $this
        /* Set record content */
        -> DOMContent( $ATemplate )

        -> FocusChildren( 'Status' )
        -> DOMClassAdd( '%Status%' )

        /* Set text */
        -> FocusParents( 'Record' )
        -> FocusChildren( 'Text' )
        -> DOMContent( '%Body%' )
        -> Event
        (
            'Main',
            'Edit',
            $this -> Clone()
            -> ReadAttribute( 'AID', 'data-id-record' )
            -> ReadValue( 'ABody' ),
            TPusaCore::KEY_UP,
            TConst::SAVE_TIMEOUT_MILLISECONDS,
            true
        )
        -> SetData( 'id-record', '%ID%' )     /* For callback data */
        -> DOMID( 'Text_%ID%' )               /* For unique */
        -> EventFront
        (
            $this -> Clone() -> EditBegin(),
            TPusa::KEY_PRESS
        )

        /* Set options action */
        -> FocusParents( 'Record' )
        -> FocusChildren( 'Options' )
        -> Event
        (
            'Main',
            'Options',
            $this -> Clone() -> ReadStatic( 'AID', '%ID%' )
        )
        -> SetData( 'id-record', '%ID%' )

        -> FocusParents( 'Record' )
        -> FocusChildren( 'DTInsert' )
        -> DOMContent( '%DTInsert%' )

        -> FocusParents( 'Record' )
        -> FocusChildren( 'CaptionUser' )
        -> DOMContent( '%CaptionUser%' );
    }



    /*
        Reload one record and refresh its form if exists.
    */
    private function RecordReload( $AID )
    {
        return $this
        -> FocusBody()
        -> FocusChildren( 'Record_' . $AID, TPusaCore::ID )         /* Focus on record conatiner */
        -> SetReplace                                           /* Replace array for code in BuildRecord */
        (
            $this -> RecordByID( $AID ) -> GetRecordAsArray()   /* Select record by ID */
        )
        -> BuildRecord( $this -> GetTemplate( 'Task.html' ))    /* Build the record*/
        -> SetReplace();                                        /* Clear Replace array */
    }



    private function EditBegin()
    {
        return $this
        -> FocusParents( 'Record' )
        -> DOMClassAdd( 'Edit' );
    }



    private function EditEnd()
    {
        $this
        -> FocusParents( 'Record' )
        -> DOMClassRemove( 'Edit' );

        if( !$this -> IsOk() )
        {
            $this -> DOMClassAdd( 'Error' );
        }

        return $this;
    }



    /*
        Options popup window
    */
    private function BuildOptions( string $AID )
    {
        $Record = $this -> RecordByID( $AID ) -> GetRecordAsParams();

        return $this
        -> WinPopup()
        -> WinNorm()
        -> DOMTemplate( 'Popup.html' )

        -> FocusChildren( 'Body' )
        -> DOMContent( $Record -> GetString( 'Body' ) )

        /* Set delete event */
        -> FocusParents( 'Options' )
        -> FocusChildren( 'Delete' )
        -> EventFront
        (
            $this
            -> Clone()
            -> WinConfirm
            ([[
                'Class'     => 'Main',
                'Method'    => 'Delete',
                'Label'     => 'Delete'
            ]])
            -> WinNorm()
        )

        /* Copy text button */
        -> FocusParents( 'Toolbar' )
        -> FocusChildren( 'Copy' )
        -> EventFront
        (
            $this -> Clone()
            -> WinPopupClose()
            -> SetClipboard( $Record -> GetString( 'Body' ))
        )

        /* Set Delete */
        -> FocusParents( 'Toolbar' )
        -> FocusChildren( 'Delete' )
        -> Event( 'Main', 'TaskDelete', $this -> Clone() -> ReadStatic( 'AID', $AID ))
        -> DOMVisible( !$this -> GetWeb() -> IsGuest())

        /* Set Wait */
        -> FocusParents( 'Toolbar' )
        -> FocusChildren( 'Wait' )
        -> Event( 'Main', 'Wait', $this -> Clone() -> ReadStatic( 'AID', $AID ))
        -> DOMVisible( $Record -> GetString( 'Status' ) != TConst::STATUS_WAIT )

        /* Set Done */
        -> FocusParents( 'Toolbar' )
        -> FocusChildren( 'Done' )
        -> Event( 'Main', 'Done', $this -> Clone() -> ReadStatic( 'AID', $AID ))
        -> DOMVisible( $Record -> GetString( 'Status' ) != TConst::STATUS_DONE )

        -> FocusParents( 'Toolbar' )
        -> FocusChildren( 'button', TPusaCore::TAG )
        -> SetData( 'id-record', $AID )

        ;
    }



    private function AfterDelete
    (
        $AID
    )
    {
        return $this
        /* Remove container */
        -> WinPopupClose()
        /* Remove record container */
        -> FocusBody()
        -> FocusChildren( 'Record_' . $AID, TPusaCore::ID )
        -> DOMDelete()
        ;
    }

}
