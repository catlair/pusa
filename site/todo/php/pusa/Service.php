<?php

namespace catlair;

require_once ROOT . '/core/moment.php';
require_once 'const.php';



/*
    Pusa todo list demonstrration.
    It is CLI service controller.
    All method available only from cli.
*/
class Service extends TWebPusa
{
    /*
        Create default database
        CLI call:
            ./cli --IDDomain=dev.todo.catlair.net --Pusa=Service --Method=CreateDatabase
            ./cli --IDDomain=dev.todo.catlair.net --Pusa=Service --Method=CreateDatabase --Datasource=postgres
        Pusa call:
            TWebPusa::Create( $this -> Web, 'Service', 'CreateDatabase' ) -> SetValue( 'Datasource', 'postgres' ) -> Run();
    */
    public function CreateDatabase()
    {
        if( !$this -> GetWeb() -> IsCLI() )
        {
            $this -> SetResult( 'OnlyForCLI' );
        }
        else
        {
            $DatasourceName = GetIncomeCLI( 'Datasource', null, $this -> GetValue( 'Datasource' ));
            $Now = TMoment::Create() -> Now();

            /* Get datasource from Web */
            $Datasource = $this -> GetWeb() -> GetDatasource( $DatasourceName, null, false );

            if
            (
                ! empty( $Datasource ) &&
                ! $Datasource -> DatabaseExists( $Datasource -> GetDatabase() )
            )
            {
                /* Create database */
                $Datasource
                -> DatabaseCreate
                (
                    $Datasource -> GetDatabase(),
                    [
                        'Codepage'  =>  TData::CODEPAGE_UTF8,
                        'Charset'   =>  'utf8mb4',
                        'Collate'   =>  'utf8mb4_unicode_ci'
                    ]
                )

                /* Select database */
                -> DatabaseSelect()

                /*
                    Create Users table
                    CREATE TABLE Users
                    (
                        ID              VARCHAR (32) NOT NULL PRIMARY KEY,
                        PasswordHash    VARCHAR (32)
                    )
                */
                -> TableCreate
                ([
                    'Name'      => 'Users',
                    'Fields'    =>
                    [
                        [
                            'Name'      => 'ID',
                            'Type'      => TData::TYPE_STRING_UNSIZE,
                            'Length'    => 32,
                            'NotNull'   => true,
                            'Key'       => TData::KEY_PRIMARY
                        ],
                        [
                            'Name'      => 'PasswordHash',
                            'Type'      => TData::TYPE_STRING_UNSIZE,
                            'Length'    => 32
                        ],
                        [
                            'Name'      => 'Caption',
                            'Type'      => TData::TYPE_STRING_UNSIZE,
                            'Length'    => 64
                        ]
                    ]
                ])

                /*
                    Create Tasks table
                    CREATE TABLE Tasks
                    (
                        ID          VARCHAR (32) NOT NULL PRIMARY KEY,
                        IDUser      VARCHAR (32) NOT NULL,
                        DTInsert    DATETIME NOT NULL,
                        Body        LONGTEXT,
                        Status      VARCHAR (32) NOT NULL,
                        INDEX       idx_IDUser (IDUser)
                    )
                */
                -> TableCreate
                ([
                    'Name'      => 'Tasks',
                    'Fields'    =>
                    [
                        [
                            'Name'      => 'ID',
                            'Type'      => TData::TYPE_STRING_UNSIZE,
                            'Length'    => 36,
                            'NotNull'   => true,
                            'Key'       => TData::KEY_PRIMARY
                        ],

                        [
                            'Name'      => 'IDUser',
                            'Type'      => TData::TYPE_STRING_UNSIZE,
                            'Length'    => 36,
                            'NotNull'   => true
                        ],

                        [
                            'Name'      => 'DTInsert',
                            'Type'      => TData::TYPE_MOMENT,
                            'NotNull'   => true
                        ],

                        [
                            'Name'      => 'DTUpdate',
                            'Type'      => TData::TYPE_MOMENT
                        ],

                        [
                            'Name'      => 'Body',
                            'Type'      => TData::TYPE_TEXT
                        ],

                        [
                            'Name'      => 'Status',
                            'Type'      => TData::TYPE_STRING_UNSIZE,
                            'Length'    => 36,
                            'NotNull'   => true
                        ]
                    ]/*,

                    'Indexes'           => [[ 'IDUser' ]]*/

                ])

                -> IndexCreate
                (
                    'Tasks',
                    TData::INDEX_SIMPLE,
                    ['IDUser']
                )

                /* Create full text index */
                -> IndexCreate
                (
                    'Tasks',
                    TData::INDEX_FULLTEXT,
                    ['Body']
                )

                -> RecordInsert
                ([
                    'Table' => 'Users',
                    'Records' =>
                    [
                        /* Root account */
                        [
                            'ID'            => 'root',
                            'PasswordHash'  => $this -> GetWeb() -> GetPasswordHash( 'root', 'root' ),
                            'Caption'       => 'Adminstrator'
                        ],
                        /* Guest account */
                        [
                            'ID'            => 'guest',
                            'Caption'       => 'Guest'
                        ]
                    ]
                ])

                -> RecordInsert
                ([
                    'Table' => 'Tasks',
                    'Records' =>
                    [
                        /* Create default task */
                        [
                            'ID'        => clGUID(),
                            'Body'      => 'Hello world.',
                            'IDUser'    => 'guest',
                            'DTInsert'  => $Now -> Add( TMoment::SECOND * 0 ) -> ToStringODBC(),
                            'Status'    => TConst::STATUS_DONE
                        ],
                        /* Create default task */
                        [
                            'ID'        => clGUID(),
                            'Body'      => 'It is pusa todo demo on ' . $DatasourceName . '.',
                            'IDUser'    => 'guest',
                            'DTInsert'  => $Now -> Add( TMoment::SECOND * 1 ) -> ToStringODBC(),
                            'Status'    => TConst::STATUS_WAIT
                        ],
                        /* Create default task */
                        [
                            'ID'        => clGUID(),
                            'Body'      => 'You can create a login. Input the login and you password. The login will be created automaticaly.',
                            'IDUser'    => 'guest',
                            'DTInsert'  => $Now -> Add( TMoment::SECOND * 2 ) -> ToStringODBC(),
                            'Status'    => TConst::STATUS_WAIT
                        ]
                    ]
                ])
                -> ResultTo( $this )
                ;
            }
        }

        return $this;
    }



    /*
        Delete default database
        Call:
            ./cli --IDDomain=dev.todo.catlair.net --Pusa=Service --Method=DeleteDatabase
            ./cli --IDDomain=dev.todo.catlair.net --Pusa=Service --Method=DeleteDatabase --Datasource=postgres
    */
    public function DeleteDatabase()
    {
        if( !$this -> GetWeb() -> IsCLI() )
        {
            $this -> SetResult( 'OnlyForCLI' );
        }
        else
        {
            $DatasourceName = GetIncomeCLI( 'Datasource', $this -> GetValue( 'Datasource' ));
            $Datasource = $this -> GetWeb() -> GetDatasource( $DatasourceName, null, false );
            if
            (
                ! empty( $Datasource ) &&
                $Datasource -> DatabaseExists( $Datasource -> GetDatabase() )
            )
            {
                $Datasource -> DatabaseDelete( $Datasource -> GetDatabase() );
            }
            $Datasource -> ResultTo( $this );
        }

        return $this;
    }



    /*
        Execute job by IDRecord
        ./cli --IDDomain=dev.todo.catlair.net --Pusa=Service --Method=TestTransact
    */
    public function &TestTransact()
    {
        $this -> SetOk();

        $DatasourceName = GetIncomeCLI( 'Datasource', $this -> GetValue( 'Datasource' ));
        $Datasource = $this -> GetWeb() -> GetDatasource( $DatasourceName, null, true );
        if (!empty( $Datasource ))
        {
            $Datasource -> TestTransact();
        }

        $Datasource -> ResultTo( $this );

        return $this;
    }
}
