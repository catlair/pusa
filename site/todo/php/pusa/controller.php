<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

/*
    Pusa TODO demonstration
    MVC Controller module
*/

namespace catlair;



trait Controller
{
    /*
        Refresh event
    */
    public function Refresh( string $ASearch = '' )
    {
        return $this
        -> PrepareList()    /* Prepare list */
        -> BuildPage
        (
            /* Load page */
            $this
            -> Select( 0, TConst::PAGE_COUNT_RECORD, $ASearch )
            -> GetDataset()
        );
    }



    /*
        The scroll event callback is initialized in the MainPage method.
    */
    public function Scroll
    (
        string  $ASearch = '',
        int     $ALast = 0,
        int     $AOffset = 0,
        int     $AScroll = 0,
        int     $AHeight = 0
    )
    {
        if( $AOffset - $AScroll < $AHeight * 1.5 )
        {
            /* Load next page */
            $this -> BuildPage
            (
                /* Load page */
                $this
                -> Select
                (
                    $ALast,
                    TConst::PAGE_COUNT_RECORD,
                    $ASearch
                )
                -> GetDataset(),
                $ALast
            );
        }

        return $this;
    }



    /*
        Login action
    */
    public function Login
    (
        string $ALogin = '',
        string $APassword = ''
    )
    {
        /* Check empty */
        if( empty( $ALogin ) && empty( $APassword ))
        {
            $this -> SetResult( 'LoginOrPasswordIsEmpty' );
        }

        if( $this -> IsOk() )
        {
            if( $this -> UserExists( $ALogin ) )
            {
                if( $this -> CheckPassword( $ALogin, $APassword ))
                {
                    /* Login success */
                    $this -> GetWeb() -> Login( $ALogin );
                    $this -> UserForm() -> Refresh();
                }
                else
                {
                    $this -> SetResult( 'IncorrectPassword' );
                }
            }
            else
            {
                /* Registrate sucess */
                $this -> Registrate( $ALogin, $APassword );
                $this -> GetWeb() -> Login( $ALogin );
                $this -> UserForm() -> Refresh();
            }
        }

        return $this;
    }



    /*
        Logout action
    */
    public function Logout()
    {
        $this -> GetWeb() -> Logout();
        return $this
        -> UserForm()   /* Rebuld user form */
        -> Refresh()    /* Refresh records list */
        ;
    }



    /*
        Add new task
    */
    public function ClickAdd()
    {
        if( $this -> GetWeb() -> IsGuest() )
        {
            $this -> SetCode( 'GuestCanNotAdd' );
        }
        else
        {
            /* Prepare values */
            $Values =
            [
                'ID'        => clGUID(),
                'IDUser'    => $this -> GetWeb() -> GetIDUser(),
                'Body'      => $this -> GetString( 'Body' ),
                'DTInsert'  => TMoment::Create() -> Now() -> ToStringODBC(),
                'DTUpdate'  => TMoment::Create() -> Now() -> ToStringODBC(),
                'Status'    => 'New'
            ];

            /* Insert opertaion in to DB */
            if( $this -> Insert( $Values ) -> IsOk())
            {
                $this -> Refresh();
            }
        }

        return $this;
    }



    /*
        Edit exists tasks
    */
    public function Edit
    (
        string $AID = 'guest',
        string $ABody = ''
    )
    {
        if( $this -> GetWeb() -> IsGuest() )
        {
            $this -> SetCode( 'GuestCanNotEdit' );
        }
        else
        {
            if( !$this -> CheckOwner( $AID ))
            {
                $this -> SetCode( 'IsNotOwner' );
            }
            else
            {
                $this
                -> Update( $AID, [ 'Body' => $ABody ] )
                -> EditEnd();
            }
        }
        return $this;
    }



    /*
        Delete record
    */
    public function TaskDelete( string $AID )
    {
        if( $this -> Delete( $AID ) -> IsOk() )
        {
            $this -> AfterDelete( $AID );
        }
        return $this;
    }



    public function Options( string $AID )
    {
        return
        $this -> BuildOptions( $AID );
    }



    public function Done( string $AID )
    {
        if( !$this -> CheckOwner( $AID ))
        {
            $this -> SetCode( 'AreNotOwner' );
        }
        else
        {
            $this
            -> ChangeStatus( $AID, TConst::STATUS_DONE )
            -> WinPopupClose()
            -> RecordReload( $AID );
        }
        return $this;
    }



    public function Wait( string $AID )
    {
        if( !$this -> CheckOwner( $AID ))
        {
            $this -> SetCode( 'AreNotOwner' );
        }
        else
        {
            $this
            -> ChangeStatus( $AID, TConst::STATUS_WAIT )
            -> WinPopupClose()
            -> RecordReload( $AID );
        }
        return $this;
    }



    public function DatasourceSelect( string $ADatasource = 'mysql' )
    {
        $this -> DatasourceName = $ADatasource;
        setcookie( 'Datasource', $this -> DatasourceName );

        return $this
        -> Logout()
        -> Refresh()
        -> Info
        (
            'You are using the ' . $this -> DatasourceName . ' database now.'
        );
    }
}



