<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

/*
    Pusa TODO demo
    MVC Model

    This module contain work with DOM
*/

namespace catlair;


require_once ROOT . '/core/operator.php';
require_once ROOT . '/core/moment.php';



trait Model
{
    /*
        Return Datasource from cooke "Datasource"
    */
    private function GetDatasource()
    {
        /*  Define the datasource name */
        $this -> DatasourceName
        = empty( $this -> DatasourceName )
        ? $this -> GetCookie( 'Datasource', 'mysql' )   /* DatasourceName can be set from Cookie */
        : $this -> DatasourceName;                      /* DatasourceName can be set on controller.php method DatasourceSelect */

        return $this
        -> GetWeb()
        -> GetDatasource( $this -> DatasourceName );
    }



    /*
        Insert record in to DB
    */
    private function Insert( $AValues )
    {
        return $this
        -> GetDatasource()
        -> RecordInsert
        ([
            'Table'     => 'Tasks',
            'Records'   => [ $AValues ]
        ])
        -> ResultTo( $this );
    }



    /*
        Send request to DB and return Dataset with records
    */
    private function Select
    (
        int     $AFrom      = 0,    /* Start record number */
        int     $ACount     = 0,    /* Count records for return */
        string  $ASearch    = ''
    )
    {
        return $this
        -> GetDatasource()
        -> RecordSelect
        ([

            'Tables' =>
            [
                [ 'Table' => 'Tasks' ],
                [
                    'Table' => 'Users',
                    'Join' => TData::INNER,
                    'Conditions' =>
                    [
                        TOperator::EQUAL,
                        [ 'Table' => 'Tasks', 'Field' => 'IDUser' ],
                        [ 'Table' => 'Users', 'Field' => 'ID' ]
                    ]
                ]
            ],

            'Fields' =>
            [
                [ 'Table' => 'Tasks', 'Field' => 'ID' ],
                [ 'Table' => 'Tasks', 'Field' => 'Status' ],
                [ 'Table' => 'Tasks', 'Field' => 'DTInsert' ],
                [ 'Table' => 'Tasks', 'Field' => 'Body' ],
                [ 'Table' => 'Tasks', 'Field' => 'IDUser' ],
                [ 'Table' => 'Users', 'Field' => 'Caption', 'Alias' => 'CaptionUser' ]
            ],

            'Conditions' =>
            [
                TOperator::AND,
                [
                    TOperator::EQUAL,
                    [ 'Table' => 'Tasks', 'Field' => 'IDUser' ],
                    $this -> GetWeb() -> GetIDUser()
                ],
                empty( $ASearch ) ? [] :
                [
                    TOperator::FULL_TEXT,
                    [ 'Table' => 'Tasks', 'Field' => 'Body' ],
                    $ASearch
                ]
            ],

            'Sort' =>
            [
                [
                    'Table' => 'Tasks',
                    'Field' => 'DTInsert',
                    'Order' => TData::ZA
                ]
            ],

            'Limit' => $ACount,
            'Skip' => $AFrom
        ])
        -> ResultTo( $this );
    }



    /*
        Update record by ID
    */
    public function Update
    (
        string $AID,            /* IDRecord */
        array $AValues = []     /* List of values */
    )
    {
        $Values[ 'DTUpdate' ]  = TMoment::Create() -> Now() -> ToStringODBC();
        /* Update opertaion in to DB */

        $this
        -> GetDatasource()
        -> RecordUpdate
        ([
            'Tables' => [ 'Tasks' ],
            'Values' => $AValues,
            'Conditions' =>
            [
                TOperator::EQUAL,
                [ 'Table' => 'Tasks', 'Field' => 'ID' ],
                $AID
            ]
        ])
        -> ResultTo( $this );

        return $this;
    }




    /*
        Update record by ID
    */
    public function ChangeStatus
    (
        string $AID,    /* IDRecord */
        string $AStatus /* List of values */
    )
    {
        /* Update opertaion in to DB */
        $this
        -> GetDatasource()
        -> RecordUpdate
        ([
            'Tables' => [ 'Tasks' ],
            'Values' => [ 'Status' => $AStatus ],
            'Conditions' =>
            [
                TOperator::EQUAL,
                [ 'Table' => 'Tasks', 'Field' => 'ID' ],
                $AID
            ]
        ])
        -> ResultTo( $this );

        return $this;
    }



    /*
        Delete record by ID
    */
    private function Delete
    (
        string $AID /* IDRecord */
    )
    {
        /* Delete opertaion in to DB */
        $this
        -> GetDatasource()
        -> RecordDelete
        ([
            'Tables' => [ 'Table' => 'Tasks' ],
            'Conditions' =>
            [
                TOperator::EQUAL,
                [ 'Table' => 'Tasks', 'Field' => 'ID' ],
                $AID
            ]
        ])
        -> ResultTo( $this )
        ;
        return $this;
    }



    /*
        Select and return Task by ID and
    */
    private function RecordByID( $AID )
    {
        return
        $this
        -> GetDatasource()
        -> RecordSelect
        ([
            'Tables' =>
            [
                [ 'Table' => 'Tasks' ],
                [
                    'Table' => 'Users',
                    'Join' => TData::INNER,
                    'Conditions' =>
                    [
                        TOperator::EQUAL,
                        [ 'Table' => 'Tasks', 'Field' => 'IDUser' ],
                        [ 'Table' => 'Users', 'Field' => 'ID' ]
                    ]
                ]
            ],

            'Fields' =>
            [
                [ 'Table' => 'Tasks', 'Field' => 'ID' ],
                [ 'Table' => 'Tasks', 'Field' => 'Status' ],
                [ 'Table' => 'Tasks', 'Field' => 'DTInsert' ],
                [ 'Table' => 'Tasks', 'Field' => 'Body' ],
                [ 'Table' => 'Tasks', 'Field' => 'IDUser' ],
                [ 'Table' => 'Users', 'Field' => 'Caption', 'Alias' => 'CaptionUser' ]
            ],

            'Conditions' =>
            [
                TOperator::EQUAL,
                [ 'Table' => 'Tasks', 'Field' => 'ID' ],
                $AID
            ]
        ])
        -> ResultTo( $this )
        -> GetDataset();
    }



    /*
        Return true for user is owner of record
    */
    private function CheckOwner( $AID )
    {
        return $this
        -> RecordByID( $AID )
        -> GetRecordAsParams()
        -> GetString( 'IDUser' ) == $this -> GetWeb() -> GetIDUser();
    }



    /*
        Return true for user exists
    */
    private function UserExists( $ALogin )
    {
        return
        $this
        -> GetDatasource()
        -> RecordSelect
        ([
            'Tables' => [ [ 'Table' => 'Users' ] ],
            'Fields' => [ [ 'Table' => 'Users', 'Field' => 'ID' ] ],
            'Conditions' =>
            [
                TOperator::EQUAL,
                [ 'Table' => 'Users', 'Field' => 'ID' ],
                $ALogin
            ]
        ])
        -> ResultTo( $this )
        -> GetDataset()
        -> GetCount() > 0
        ;
    }



    /*
        Return true for user exists
    */
    private function GetCaptionUser( $ALogin )
    {
        return
        $this
        -> GetDatasource()
        -> RecordSelect
        ([
            'Tables' => [ [ 'Table' => 'Users' ] ],
            'Fields' => [ [ 'Table' => 'Users', 'Field' => 'Caption' ] ],
            'Conditions' =>
            [
                TOperator::EQUAL,
                [ 'Table' => 'Users', 'Field' => 'ID' ],
                $ALogin
            ]
        ])
        -> ResultTo( $this )
        -> GetDataset()
        -> GetRecordAsParams()
        -> GetString( 'Caption', $ALogin )
        ;
    }



    /*
        Insert record in to DB
    */
    private function Registrate
    (
        string  $ALogin,
        string  $APassword  /* Password for user */
    )
    {
        $this
        -> GetDatasource()
        -> RecordInsert
        ([
            'Table' => 'Users',
            'Records' =>
            [[
                'ID'            => $ALogin,
                'PasswordHash'  => $this -> GetWeb() -> GetPasswordHash( $ALogin, $APassword ),
                'Caption'       => $ALogin
            ]]
        ])
        -> ResultTo( $this );
        return $this;
    }



    /*
        Return true for password is correct
    */
    private function CheckPassword
    (
        string  $ALogin,
        string  $APassword
    )
    {
        return
        $this
        -> GetDatasource()
        -> RecordSelect
        ([
            'Tables' => [[ 'Table' => 'Users' ]],
            'Fields' => [[ 'Table' => 'Users', 'Field' => 'PasswordHash' ]],
            'Conditions' =>
            [
                TOperator::EQUAL,
                [ 'Table' => 'Users', 'Field' => 'ID' ],
                $ALogin
            ]
        ])
        -> GetDataset()
        -> GetRecordAsParams()
        -> GetString( 'PasswordHash' ) == $this -> GetWeb() -> GetPasswordHash( $ALogin, $APassword );
    }
}
