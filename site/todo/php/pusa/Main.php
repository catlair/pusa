<?php
/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

/*
    Main module of Pusa TODO demonstration
*/

namespace catlair;

/* Engine modules */
require_once ROOT . '/web/win.php';

/* Local modules */
require_once 'model.php';       /* MVC model */
require_once 'view.php';        /* MVC view */
require_once 'controller.php';  /* MVC controller */



class Main extends TWebPusa
{
    /* Pusa traits */
    use TWin;

    /* Local traits */
    use Model;
    use View;
    use Controller;



    /*
        Initialization method for title page
        This method is called first when the user opens the page of demonstration.
    */
    public function Init()
    {
        return $this
        -> MainPage();                  /* Build main page */
    }



    /*
        Method on web after build event.
        This method you can override for the browser logger.
    */
    /*
        public function OnAfterWebBuild( $AWeb )
        {
            parent::OnAfterWebBuild( $AWeb );
            return $this;
        }
    */
}
