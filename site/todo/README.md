> Catlair PHP Copyright (C) 2021 https://itserv.ru
>
> This text is part of free software: you can redistribute
> it and/or modify it under the terms of the GNU Aferro General
> Public License as published by the Free Software Foundation,
> either version 3 of the License, or (at your option) any later version.
> This program (or part of program) is distributed in the hope that
> it will be useful, but WITHOUT ANY WARRANTY; without even the implied
> warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
> See the GNU Aferro General Public License for more details.
> You should have received a copy of the GNU Aferror General Public License
> along with this program. If not, see <https://www.gnu.org/licenses/>


# Pusa todo demonstration

This "[Hello world](https://todo.catlair.net)" on [Pusa](https://pusa.catlair.net) technology.
The project contains the following features:
- does not use JS application code;
- contains full CRUD for MySQL and PostgreSQL demonstration;
- follows the MVC paradigm;
- demonstrates the user authorization;
- works with interective scrolling list;
- has the dinamyc refresh;
- contains the popup and confirmation interface;
- has the copy to clipboard interface;
- changes status for tasks.

## Links
- [Demonstration docker container](https://hub.docker.com/r/catlairnet/pusa_todo_demo)
- [Pusa sources](https://gitlab.com/catlair/pusa/-/tree/main/)
- [Pusa manual](https://gitlab.com/catlair/pusa/-/blob/main/site/pusa/src/language_en/man/pusa_manual_head.md)
- [Pusa home site](https://pusa.catlair.net)
