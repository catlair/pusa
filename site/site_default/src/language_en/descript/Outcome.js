class TOutcome
{
    /*
        Build list of outcomes for descript
    */
    static Select( AParams )
    {       
        cl.Post
        ({
            Call:        'Outcome.Select',
            Income:      { IDDescript: AParams.IDDescript },
            OnAfterLoad: Result =>
            {
                if ( cl.PostResult( Result ))
                {
                    if ( AParams.OnAfterLoad ) AParams.OnAfterLoad( Result );
                }
                else
                {
                    if ( AParams.OnError ) AParams.OnError( Result );
                }
            }
        });
    }

}









