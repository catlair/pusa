# Catlair Domains configuration files

> :warning: You must generate your SSLKey.

The folder contains domains configurations. Each web request gets a hostname and reads the domain.name.json file.
Example: for the *catlair.net* domain, the file *ROOT/domain/catlair.net.json* will be use.

```
{
    "IDSite"            : "site_default",
    "IDLang"            : "language_ru",
    "IDContentStart"    : "Main.html",
    "Pusa"              : "true",
    "DefaultURL"        : {},
    "Optimize"          : "false",
    "Debug"             : "true",
    "Engine"            : "cms",
    "SSLKey"            : "dD9dPnO+uwuihp*UdaliqhiqdvqwFKTaRS0XA=",
    "SSLMethod"         : "aes-256-cbc",
    "SSLLengthVector"   : "16",
    "SessionExpireMin"  : "60"
}
```

## Main values

### IDSite
Identifier of site.

### IDLang
Language identifer. The key can contain any string values.
language_default, language_ru, language_en and etc...

### IDLangDefault
If system cannot find content on the requested language, it tries to request in the default language.
If content will not found, an error will be returned.

### IDContentStart
Example:    Main.html"

### Pusa
A boolean value (false or true ). If Pusa Method and Class not exists in the URL,
Will be run the **Init** method of **Main** class.

### DefaultURL"
{
    "body"          : "Body.html",
    "page"          : "Page.html"
},

## Log settings

### Optimize
A boolean value (false or true ) to optimize the content. All spaces, returns, comments will be remmoved from the source code for client responses.

### Debug
A boolean value (false or true) for enable logs. All logs will be writen to the ROOT/folder.

### Engine
Name of engine, string value.
- web - simple file engine on a TWeb object (recomended). All content and data store in ROOT/src;
- cms - DB engine Catlair.

### SessionExpireMin
The session expired time in minutes since the last user request.
If the time elapsed since the last request is more than this value, the session will be closed.

## SSL values

### SSLKey

> :warning: You must generate new SSL key.

SSL secret key for encript and decript operations in TWeb. For example an SSLKey used to encrypt and decript cookies with information about a user's session.
SSL_KEY can be created by openssl_random_pseudo_bytes.
Example:
```
php -r 'echo base64_encode(openssl_random_pseudo_bytes( 32 )).PHP_EOL;'
```

### SSLMethod

Method for encript and decrypt operations.
Example: aes-256-cbc.
You can get list of methods from [PHP manual](https://www.php.net/manual/en/function.openssl-get-cipher-methods.php).

### SSLLengthVector     16

Integer length of initial vector 16,32...
