# Настрокий доменов Catlair
[Оглавление](site/pusa/src/language_ru/man/pusa_manual_head.md)

Папка **[ROOT]/domain** содержит перечень конфигурационных файлов для доменов в формате json.
Каждый веб запрос по имени домана определяет свой конфигурационный файл и читает из него параметры.
Пример: для запроса домена **catlair.net**, будет использован файл **[ROOT]/domain/catlair.net**.

```
{
    "Engine"            : "web",
    "IDSite"            : "site_default",
    "IDLang"            : "language_ru",
    "Pusa"              : "true",
    "IDContentStart"    : "index.html",
    "DefaultURL"        : {},
    "Optimize"          : "false",
    "Debug"             : "true",
    "SSLKey"            : "dD9dPnO+uwuihp*UdaliqhiqdvqwFKTaRS0XA=",
    "SSLMethod"         : "aes-256-cbc",
    "SSLLengthVector"   : "16",
    "SessionExpireMin"  : "60"
}
```

## Основные значения

### Engine
Наименование движка для обработки запросов.
- web - простой движек на локальных файлах, использующий класс TWeb (Рекомендуется). Весь конент хранится в папке [SITE]/src;
- webdb - движек, работающий с источниками данных. Использует класс TWebDB.
- cms - движе на основе DB Catlair (не распространяется под GPL3).

### IDSite
Идентификатор сайта. Каждый сайт имеет свой идентификатор. Данное поле указывает,
для какого сайта будет обрабатываться очередной запрос. Соответственно несколько доменов
могут указывать на один идентификатор сайта.

### IDLang
Идентификатор языка. Ключ содержит строковое значение с произвольным именем языка.
Принятое наименование выклядит как language_default, language_en и так далее.
Тем не менее имя языка может быть произвольным.

### IDLangDefault
Идентификатор умолчального языка. В случае, если для запрошенного контента на текущем языке не найден файл,
будет запрошен контент на указанном умолчальном языке. Обычно это language_default. Возможно установить
любой язык по умолчанию. Если контент не будет найден и на умаолчальном языке, будет возвращена ошибка.

### Pusa
Флаг false или true для запуска Pusa контроллера если таковой отсутсвует в GET или POST запросе.
При значении true Pusa проверит указан ли в вызове контроллер и метод для вызова. Если не указаны,
будет вызван метод Init контроллера Main. Этот флаг имеет значения для первоначальных вызовов по прямым
ссылкам, когда Pusa должна предоставить контент посковому серверу.

### IDContentStart
Идентификатор стартового конетна с которого начинается сборка страницы. Чаще всего это index.html.

### DefaultURL
Массив именованных клчей которые будут использованы в качестве GET запроса при его полном отсутствии.
Следующий пример сформирует ссылку ?&body=Body.html&page=Page.html:
```
{
        "body"          : "Body.html",
        "page"          : "Page.html"
}
```
Параметр устарел и не имеет смысла для использования Pusa.

### Optimize
Булево значение (false или true), указывающее на необходимость оптимизации контента при возврате результата.
Все двойные пробелы, переводы стро, коментарии будут удалены из исходного кода перед передачей его клиенту.

### Debug
Булево значние (false or true) для включения логирования.

### SessionExpireMin
Время устаревания сессии в минтуах с момента последнего обрщарения.
Если времени с момента последнего обращения прошло более указанного количества минут сессия будет закрыта.



## Параметры шифрования

### SSLKey

Секретный ключ SSL используется для шифрования и расшифровки сообщений и информации. Для примера авторизация пользователей
основана на шифровании сессионной информации и хранится на стороне пользователя. SSLKey может быть создан при помощи
вызова команды openssl_random_pseudo_bytes.
Пример:
```
php -r 'echo base64_encode(openssl_random_pseudo_bytes( 32 )).PHP_EOL;'
```
> :warning: Вы должны создать персоноальный SSL ключ для ваших серверов.

### SSLMethod

Метод шифрования.
Пример: aes-256-cbc.
Вы можете получить список методов шифрования по ссылке https://www.php.net/manual/en/function.openssl-get-cipher-methods.php.

### SSLLengthVector     16
Длинна вектора инициализации для шифрвоания, может быть 16, 32 и так далее.
