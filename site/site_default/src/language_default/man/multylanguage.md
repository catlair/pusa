> Catlair PHP Copyright (C) 2021 https://itserv.ru
>
> This text is part of free software: you can redistribute
> it and/or modify it under the terms of the GNU Aferro General
> Public License as published by the Free Software Foundation,
> either version 3 of the License, or (at your option) any later version.
> This program (or part of program) is distributed in the hope that
> it will be useful, but WITHOUT ANY WARRANTY; without even the implied
> warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
> See the GNU Aferro General Public License for more details.
> You should have received a copy of the GNU Aferror General Public License
> along with this program. If not, see <https://www.gnu.org/licenses/>


# Мультиязыковая поддержка

## Ссылки

- [Catlair](site/site_default/src/language_default/man/catlair.md)
- [Шаблонизатор](site/site_default/src/language_default/man/templater.md)
- [Реализация шаблонизатора](main/php/web/web.php)

## Определение

Catlair имеет встроенную мультиязыковую поддержу. Каждый шаблон для каждого сайта может быть
написан на произвольном языке. Для файловой реализации Catlair все шаблоны для каждого
сайта IDSite размещены в языковой папке IDLanguage:
```
[ROOT]/site/[IDSite]/src/[IDLanguage]
```
Для каждого языка создается индивидуальная папка от которой далее и осуществляется поиск шаблона по
идентификатору. Пример полного пути для шаблона **example/main.html** для сайта **test_site** на языке **language_en**.
```
[ROOT]/site/test_site/src/language_en/example/main.com
```

## Иерархия запроса шаблонов

При построении контента [шаблонизатор](https://gitlab.com/catlair/pusa/-/blob/main/site/site_default/src/language_default/man/templater.md)
выполняет следующие действия:
- пытается запросить контент на указанном языке;
- при отсутсвии пытается запросить контент на умолчальном языке (language_default) для текущего сайта;
- при отсутсвии пытается запросить контент на указанном языке для умолчального сайта (site_default);
- при отсутсвии пытается запросить контент на умолчальном языке (language_default) для умолчального (site_default) сайта;
- при отсутсвии возвращается ошибка об отсутсвии контента.

Схема запроса шаблона **hello.html** на языке **language_en** для сайта **my_site**.

```mermaid
graph TD
    sl["[ROOT]/site/my_site/src/language_en/hello.html"]
    sld["[ROOT]/site/my_site/src/language_default/hello.html"]
    sdl["[ROOT]/site/site_defult/src/language_en/hello.html"]
    sdld["[ROOT]/site/site_default/src/language_default/hello.html"]

    request-->sl
    sl-->sld
    sl-->result
    sld-->sdl
    sld-->result
    sdl-->sdld
    sdl-->result
    sdld-->result
    sdld-->error
```

## Определенеи языка

При каждом очередном запросе язык может быть получен из следующих источников:
- Сессионная переменная пользователя;
- Настройка для текущего домена;
- Умолчальный язык для текущего домена;
- Константа language_default.

Определение языка при каждом запросе пользователя осуществляется в [ROOT]/php/web/wep.php

## Переключение языка

Для текущей сессии переключение языка осуществляется вызовом для контроллера $this:
```
$this                           /* Current controller or Pusa controller*/
-> GetWeb()                     /* Return Web object */
-> SetIDLang( 'language_en' );  /* Set lanaguage */
```
После изменения языка он будет сохранен в сессионной переменной.
При очередном обращении пользователя, контент будет построен на указанном языке.

