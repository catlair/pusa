> Catlair PHP Copyright (C) 2021 https://itserv.ru
>
> This text is part of free software: you can redistribute
> it and/or modify it under the terms of the GNU Aferro General
> Public License as published by the Free Software Foundation,
> either version 3 of the License, or (at your option) any later version.
> This program (or part of program) is distributed in the hope that
> it will be useful, but WITHOUT ANY WARRANTY; without even the implied
> warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
> See the GNU Aferro General Public License for more details.
> You should have received a copy of the GNU Aferror General Public License
> along with this program. If not, see <https://www.gnu.org/licenses/>


# Catlair

Библиотека для Web разработки.

## Ссылки
- [Шаблонизатор](/site/site_default/src/language_default/man/templater.md)
- [Мультиязыковая поддержка](/site/site_default/src/language_default/man/multylanguage.md)
- [Pusa framework - реализация на PHP](site/pusa/README.md)


# Структура вызовов PHP Catlair

```mermaid
graph TD
subgraph root["root/"]
    index.php
end
subgraph domain["domain/"]
    user_domain["[user.domain.config].json"]
end
subgraph log["log/[IDSite]/"]
    logs
end
subgraph web["php/web/"]
    web.php
    session.php
end
subgraph core["php/core/"]
    debug.php
    builder.php
    controller.php
    pusa.php
end

webserver-->index.php
index.php-->webserver

subgraph web
    web.php---builder.php
    web.php---session.php
    user_domain-->web.php
end
subgraph core
    builder.php---debug.php
    builder.php---controller.php
    builder.php---pusa.php
    debug.php-->logs
end
subgraph root
    index.php---web.php
end
```
