> Catlair PHP Copyright (C) 2021 https://itserv.ru
>
> This text is part of free software: you can redistribute
> it and/or modify it under the terms of the GNU Aferro General
> Public License as published by the Free Software Foundation,
> either version 3 of the License, or (at your option) any later version.
> This program (or part of program) is distributed in the hope that
> it will be useful, but WITHOUT ANY WARRANTY; without even the implied
> warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
> See the GNU Aferro General Public License for more details.
> You should have received a copy of the GNU Aferror General Public License
> along with this program. If not, see <https://www.gnu.org/licenses/>


# Шаблонизатор Catlair

## Ссылки

- [Catlair](site/site_default/src/language_default/man/catlair.md)
- [Реализация](php/core/builder.php)

## Определение

Шаблонизатор - средство обработки текстового контента на стороне сервера перед возвращением результата браузеру.
Шаблонизатор получает на вход текстовый контент, ищет и циклически обрабатывает все запросы на шаблоны.
Запросом на шаблон является тэг cl в двух нотациях:

## Нотация

### Однострочный
```
<cl parameter1="value" parameter2="value" ... />
```

### Многострочный
```
<cl>
    <command1 parameter1="value"/>
    <command2 parameter2="value"/>
    ...
</cl>
```

Закрывающий слэш обязателен.

## Процедура обработки шаблона

При запуске шаблона его контент пуст. Каждый параметр шаблона может интерпритироватся либо
как команда, либо как аргумент, в зависимости от имени. Параметры-команды обрабатываются
слева на право, последовательно изменяя контент шаблона. После завершения обработки каждого
шаблона, он заменяется на результирующий контет.

## Команды

### Основные

- [content](#content) - загрузка контента из файлового или иного шаблона;
- [convert](#convert) - конертация контента в различные форматы и представления на основе параметра;
- [replace](#replace) - подмена подстрок в контенте на переданное значение;
- [call](#call) - вызоы метода PHP.

### Вспомогательные

- [add](#add) - добавляет значение контента;
- [error](#error) - подавление ошибки;
- [image](#image) - возврат изображениея или бинарного файла в виде тектового контента;
- [set](#set) - устанавливает значение контента;
- [masreplace](masreplace) - конвеерная подмена подстрок на переданные значения;
- [redirect](#redirect) - перенаправление браузера на указанный URL;
- [header](#header) - установка HTTP заголовка.

### Устаревшие

Устаревшие команды остались в коде по причине использование в ранних проектах,
но будут удалены в последствии.

- [library](library) - загрузка PHP библиотеки по имени;
- [include](include) - то же самое что library;
- [pars](pars) - шаблон будет рекурсивно обработан после завершения;
- [url](url) - собирает URL из параметров (не реализовано );
- [keys](keys) - изменение входящих параметров на переданные;
- [optimize](optimize) - оптимизация контента;
- [pure](optimize) - то же что optimize.
