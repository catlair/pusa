/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

/*
    Default JS script for Pusa on Catlair.
    https://pusa.dev

    Script must be loaded with "defer" attribute.
    Warning! This file is not recommended to be modified.
*/

<cl content="/js/pusa.js"/>
<cl content="/js/pusa_inline.js"/>

/*
    Check the container PusaDirectives.
    It can container the pusa directives from method Main.Init.
    You can create Main.Init at:
        site/[YOUR_SITE]/php/pusa/Main.php - for your Pusa controller
    otherwise it will be called:
        php/pusa/Main.php - default template for Pusa controller
*/

let StaticContainer = document.getElementById( 'PusaStaticCreated' );
if( StaticContainer )
{
    let StaticList = StaticContainer.innerHTML.split( ' ' );
    StaticContainer.remove();
}

let Container = document.getElementById( 'PusaDirectives' );
if( Container )
{
    /* Extract directives from tag PusaDirectives */
    let Directives = JSON.parse( decodeURIComponent( Container.innerHTML));
    /* Remove container with directives. It is no longer needed. */
    Container.remove();
    /* If the array of directives exists, it is run */
    if( Directives )
    {
        TPusa.Create().Run( Directives );
    }
}
else
{
    /*
        Request init method from Back
        It create new Pusa, and calls Pusa's method Main.Init after that.
    */
    TPusa.Create().Request({ Class: 'Main', Method: 'Init' });
}
