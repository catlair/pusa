/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

/*
    This module contains Pusa methods.
    Все функции являются копиями имеющихся модулей.
    При внесении изменений в любую из частей pusa_inline.js необходимо
    вносить изменения в основную библитеку откуда был заимстован код.

    still@itserv.ru
*/

/*
    Return GUIDform identifier
*/
function clGUID()
{
    return 'xxxxxxxx-xxxx-xxxx-yxxx-xxxxxxxxxxxx'.replace
    (
        /[xy]/g,
        function( c )
        {
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString( 16 );
        }
    );
}



/*
    clString.js
*/


/*
    Перебирает все свойства в массиве и возвращает только значения
    - boolean
    - number
    - string
    - symbol
    Исключаются пустые значения, массивы объекты и прочее.
*/
function GetValuesOnly
(
    AObject,    /* Объект для обхода */
    AInclude,   /* Массив имен включаемых значений. Невходящие будут исключены. */
    AExclude    /* Массив имен исключаемых значений. Входящие будут исключены. */
)
{
    var Result = {};
    if( !AInclude ) AInclude = [];
    if( !AExclude ) AExclude = [];

    for( var Key in AObject )
    {
        if
        (
            Key != 'webkitStorageInfo'     /* Chromium creates the warning message */
        )
        {
            try
            {
                 switch( typeof( AObject[ Key ] ))
                 {
                     case 'boolean':
                     case 'number':
                     case 'string':
                     case 'symbol':
                        if
                        (
                            ( AInclude.length == 0 || AInclude.indexOf( Key ) > -1 ) &&
                            ( AExclude.length == 0 || AExclude.indexOf( Key ) == -1 )
                        )
                        {
                            Result[ Key ] = AObject[ Key ];
                        }
                     break;
                }
            }
            catch( e )
            {
                /* selectionStart attribute rase exception at IOS */
            }
        }
    }

    return Result;
}


/*
    Find parent
*/
Element.prototype.ParentFilter = function
(
    AFilter,    /* Finding value by selector */
    ACallback   /* Cllback for each parent */
)
{
    var Result = null;
    var e = this;

    while( e && e.nodeType === 1 && !Result )
    {
        if( MatchData( AFilter, e ) )
        {
            Result = e;
            if( ACallback ) ACallback( Result );
        }
        e = e.parentNode;
    }

    return Result;
};



/*
    Find parents
*/
Element.prototype.ParentsFilter = function
(
    AFilter,    /* Finding value by selector */
    ACallback   /* Cllback for each parent */
)
{
    var Result = [];
    var e = this;

    while( e && e.nodeType === 1 )
    {
        if( MatchData( AFilter, e ) )
        {
            Result.push( e );
        }
        e = e.parentNode;
    }

    if( ACallback ) Result.forEach( e => ACallback( e ) );

    return Result;
};



/*
    clDOM.js
*/

/*
    DOM recursion for children
*/
Element.prototype.ChildRecursion = function( AParams, AMaxDepth, ADepth )
{
    AMaxDepth = AMaxDepth ? AMaxDepth : 0;
    ADepth = ADepth ? ADepth : 0;

    if ( AParams.OnBefore ) AParams.OnBefore( this, AParams );

    if( AMaxDepth == 0 || AMaxDepth > ADepth )
    {
        for (var i in this.childNodes )
        {
            var iNode=this.childNodes[i];
            if (iNode.nodeType == 1) iNode.ChildRecursion( AParams, AMaxDepth, ADepth+1 );
        }
    }

    if ( AParams.OnAfter ) AParams.OnAfter( this, AParams );

    return this;
};



/*
    Find children
*/
Element.prototype.Children = function
(
    AFilter,    /* Finding value by selector */
    ACallback,   /* Cllback for each parent */
    AMaxDepth
)
{
    var Result = [];
    AMaxDepth = AMaxDepth ? AMaxDepth : 0;

    this.ChildRecursion
    (
        {
            OnBefore: e =>
            {
                if( MatchData( AFilter, e ))
                {
                    Result.push( e );
                }
            }
        },
        AMaxDepth
    );

    if( ACallback ) Result.forEach( e => ACallback( e ) );
    return Result;
};



/*
*/
Element.prototype.ReplaceEventListener = function
(
    AEvent,     /* Event name */
    AFunction   /* Callback function */
)
{
    /* Remove */
    if( this.LastEventListener )
    {
        /* Remove previous listener */
        this.removeEventListener( AEvent, this.LastEventListener );
    }
    /* Store event listener for removing in future */
    this.LastEventListener = AFunction;
    /* Add new listener */
    this.addEventListener( AEvent, AFunction );
    return this;
};



/*
    Call ACallback for CSS rule by selector name
    https://developer.mozilla.org/en-US/docs/Web/API/Document/styleSheets
*/
function clCSSRuleBySelector( ASelector, ACallback )
{
    if( ACallback )
    {
        for( const Sheet of document.styleSheets )
        {
            for( const Rule of Sheet.rules )
            {
                if( Rule.selectorText == ASelector )
                {
                    ACallback( Rule );
                }
            }
        }
    }
}



function MatchData
(
    AFilter,    /* Conditions in format [ 'Operator', Arguments, ... ] */
    AData       /* array of named values */
)
{
    /* Return simple value */
    return Match
    (
        AFilter,
        function( AArg )
        {
            let Result = AArg;
            if( AArg.substr( 0,1 ) == '@' )
            {
                let Key = AArg.slice( 1 );
                Result = AData[ Key ];
            }
            return Result;
        }
    );
}



/*
    Match conditions in data.
    Return true or false if AData are matcing AFilter

    Filter example:
        (0)
            true == true
            [ '=', true, true ]
        (1)
            Data['DataKey'] == 'Value'
            [ '=', '@DataKey', 'Value' ]

        (2)
            Data[ 'Key1' ] == 'Value1' && Data[ 'Key2' ] != 'Value2'
            [ '&&', [ [ '=', '@Key1', 'Value1' ], [ '!=', 'Key2', 'Value2' ] ] ]
*/
function Match
(
    AFilter,    /* Conditions in format [ 'Operator', Arguments, ... ] */
    ACallback   /* callback function function(AArg){return AArg;}*/
)
{
    /* Value check */
    let Val = function( AArg )
    {
        let Result = AArg;
        switch( typeof( AArg ))
        {
            case 'string':
                Result = ACallback ? ACallback( AArg ) : AArg;
            break;
            case 'object':
                Result = Match( AArg, ACallback );
            break;
        }
        return Result;
    };

    let Result = false;
    if( AFilter )
    {
        let Operator = AFilter[ 0 ];

        /* Prepare parameters */
        switch( Operator )
        {
            case '=':
            case '<>':
            case '>':
            case '<':
            case '<=':
            case '>=':
            case 'in':
            case '+':
            case '-':
            case '*':
            case '/':
                var a1 = Val( AFilter[ 1 ] );
                var a2 = Val( AFilter[ 2 ] );
            break;
        }

        /* Do operation with parameters and operator */
        switch( Operator )
        {
            case 'in'   : Result = a2.split( ' ' ).indexOf( a1 ) > -1; break;
            case '='    : Result = a1 == a2; break;
            case '!='   : Result = a1 != a2; break;
            case '>'    : Result = a1 >  a2; break;
            case '<'    : Result = a1 <  a2; break;
            case '<='   : Result = a1 <= a2; break;
            case '>='   : Result = a1 >= a2; break;
            case '+'    : Result = a1 + a2; break;
            case '-'    : Result = a1 - a2; break;
            case '*'    : Result = a1 * a2; break;
            case '/'    : Result = a1 / a2; break;
            case 'or':
                Result = false;
                for( let i = 1; i < AFilter.length; i++ )
                {
                    Result = Result || Val( AFilter[i] );
                }
            break;
            case 'and':
                Result = true;
                for( let i = 1; i < AFilter.length; i++ )
                {
                    Result = Result && Val( AFilter[i] );
                }
            break;
        }
    }
    else
    {
        Result = true;
    }
    return Result;
}



function ReplaceInObject
(
    AObject,
    AValues
)
{
    for( var Prop in AObject )
    {
        var Value = AObject[ Prop ];
        switch( typeof Value )
        {
            case 'object':
                ReplaceInObject( Value, AValues );
            break;
            case 'string':
                if( Value.indexOf( '%' ) >= 0 )
                {
                    let Content = AObject[ Prop ];
                    for( var Key in AValues )
                    {
                        let Value = ( AValues[ Key ] === null ? '' : AValues[ Key ] );
                        Content = Content.replaceAll( '%' + Key + '%', Value );
                    }
                    AObject[ Prop ] = Content;
                }
            break;
        }
    }
}






/*
    Move params from onbect in to DOM
*/
function clValuesFromObject( AObject, AConteiner, ACreate )
{
    for (var Key in AObject)
    {
        var Element = AConteiner.ChildByID( Key );

        if( !Element && ACreate)
        {
            Element = document.createElement( 'input' );
            Element.type = 'hidden';
            Element.name = Key;
            AConteiner.append( Element );
        }

        if (Element != null)
        {
            try {
                var KeyValue = decodeURIComponent( AObject[ Key ]);
            } catch (ex) {
                var KeyValue = null;
            }

            if ( KeyValue != null && KeyValue != 'null' )
            {
                switch( Element.tagName )
                {
                    case 'INPUT':
                       switch( Element.type )
                       {
                           case 'checkbox':
                               Element.checked = AObject[Key] == 'on' || AObject[Key] == 'true' || AObject[Key] == '1';
                           break;
                           case 'text':
                               Element.value = KeyValue;
                           break;
                           case 'hidden':
                               Element.value = KeyValue;
                           break;
                       }
                    break;

                    case 'SELECT':
                       Element.value = KeyValue;
                    break;

                    case 'TEXTAREA':
                       Element.innerHTML = KeyValue;
                    break;

                    default:
                       Element.innerHTML = KeyValue;
                    break;
                }
            }
        }
    }
}
