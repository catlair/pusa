var STROKE_ONLY = 1;
var FILL_ONLY = 2;
var STROKE_FILL = 3;
var FILL_STROKE = 4;

/********************************************************************
    Scene
*/


class TScene
{
    Canvas = null;

    constructor( ACanvas, AEvents )
    {
        this.pStarted = false;                              /* Индикатор того что отрисовка сцены запущена */

        this.Canvas=ACanvas;
        this.Context=ACanvas.getContext('2d');
        this.Canvas.style.display = 'block';

        this.FPS = 0;                                       /* Счетчик FPS */
        this.FPSCount = 0;                                  /* Счетчик текущих кадров */
        this.FPSMoment = 0;                                 /* Момент последнего счетчика */

        this.CurrentCursorPosition = new clVector(0,0,0);   /* Текущая позиция курсора */
        this.LastCursorPosition = new clVector(0,0,0);      /* Позиция курсора с прошлого кадра */

        /*Флаги*/
        this.MouseLeft = false;
        this.MouseRight = false;
        this.MouseDrag = false;

        if ( AEvents )
        {
            this.OnMouseMove        = null;
            this.OnMouseUp          = null;
            this.OnMouseDown        = null;
            this.OnMouseDragBegin   = null;
            this.OnMouseWheel       = null;

            ACanvas.onmousemove     = function(){clSceneMouseMove(event, Result)};
            ACanvas.onmouseup       = function(){clSceneMouseUp(event, Result)};
            ACanvas.onmousedown     = function(){clSceneMouseDown(event, Result)};
            ACanvas.onmousewheel    = function(){clSceneMouseWheel(event, Result)};
            ACanvas.ondblclick      = function(){clSceneMouseDblClick(event, Result)};
        }
    }



    /*
        Запуск обработки сцены.
        Если сцена не запущена, то создается таймер с интервалом AFrameRate который работает до тех пор, пока не вызовут Stop
    */
    Start( AFrameRate, ACallback )
    {
        function Frame(AScene)
        {
            if( AScene.pStarted ) 
            {
                setTimeout(function(){Frame(AScene)}, AFrameRate);
                if (ACallback) ACallback(AScene);
            }
        }
        if( !this.pStarted ) 
        {
            this.pStarted=true;
            Frame(this);
        }
    }



    Stop()
    {
        if (this.pStarted) this.pStarted=false;
    }



    DrawCameraDump( ACamera )
    {
        this.Context.font='12px Monospace';
        this.Context.fillStyle='lime';
        this.Context.strokeStyle='black';
        this.Context.lineWidth=5;
        this.Text(ACamera.Dump(), 14, STROKE_FILL, new clVector(60,10,0), clYVector);
        this.DrawAxis(clVector.Create( 30, 30, 0), ACamera.GetLeft(), ACamera.GetTop(), ACamera.GetFront(), 20);
        return this;
    }



    GetSize()
    {
        return clVector.Create( this.Canvas.width, this.Canvas.height );
    }



    GetRect()
    {
        return {
           x: 0, 
           y: 0,
           w: this.Canvas.width,
           h: this.Canvas.height 
        };
    }


    IsRectVisible( ARect )
    {
        var Result =
        ( ARect.x + ARect.w > 0 ) &&
        ( ARect.x < this.Canvas.width ) &&
        ( ARect.y + ARect.h > 0 ) &&
        ( ARect.y < this.Canvas.height );

        return Result;
    } 



    Maximize()
    {
        this.Canvas.width = this.Canvas.clientWidth - 1;
        this.Canvas.height = this.Canvas.clientHeight - 1;
    }



    DrawAxis( APosition, AX, AY, AZ, ASize )
    {
            this.Context.lineWidth=3;

            this.Context.strokeStyle='green';
            this.Context.beginPath();
            this.Context.moveTo(APosition.x,APosition.y);
            this.Context.lineTo(APosition.x+AX.x*ASize, APosition.y+AX.y*ASize);
            this.Context.stroke();

            this.Context.strokeStyle='red';
            this.Context.beginPath();
            this.Context.moveTo(APosition.x,APosition.y);
            this.Context.lineTo(APosition.x+AY.x*ASize, APosition.y+AY.y*ASize);
            this.Context.stroke();

            this.Context.strokeStyle='blue';
            this.Context.beginPath();
            this.Context.moveTo(APosition.x,APosition.y);
            this.Context.lineTo(APosition.x+AZ.x*ASize, APosition.y+AZ.y*ASize);
            this.Context.stroke();

            this.Context.fillStyle='white';
            this.Context.beginPath();
            this.Context.rect(APosition.x-2, APosition.y-2, 4, 4);
            this.Context.fill();
        return this;
    }



    Text( AText, ASize, AType, APosition, ATop )
    {
        var lLine=AText.split('\n');
        var lShift = new clVector().Load(ATop).Scal(ASize);
        var lCur = new clVector().Load(APosition);

        for (var i=0; i<lLine.length; i++)
        {
            lCur.Add(lShift);
            switch(AType)
            {
                case STROKE_ONLY:
                    this.Context.strokeText(lLine[i], lCur.x, lCur.y);
                    break;

                case FILL_ONLY:
                    this.Context.fillText(lLine[i], lCur.x, lCur.y);
                    break;

                case STROKE_FILL:
                    this.Context.strokeText(lLine[i], lCur.x, lCur.y);
                    this.Context.fillText(lLine[i], lCur.x, lCur.y);
                    break;

                case FILL_STROKE:
                    this.Context.fillText(lLine[i], lCur.x, lCur.y);
                    this.Context.strokeText(lLine[i], lCur.x, lCur.y);
                    break;
            }
        }
    }
}














/*
----------------------------------------------------------------------
Под истребление
----------------------------------------------------------------------
*/


/*Создание объекта Scene альтернатива clScene*/
function clSceneCreate(ACanvas, AEvents)
{
 return new clScene(ACanvas, AEvents);
}




/*Перемещение мышью*/
function clSceneMouseMove(AEvent, AScene)
{
 AScene.CurrentCursorPosition = new clVector().Set(AEvent.layerX, AEvent.layerY, 0);

 if (AScene.MouseLeft)
 {
  if (AScene.MouseDrag) 
  {
   if (AScene.OnMouseDrag) AScene.OnMouseDrag(AEvent, AScene);
  }
  else
  {
   /*Начало драгдропа*/
   AScene.MouseDrag = true;
   if (AScene.OnMouseDragBegin) AScene.OnMouseDragBegin(AEvent, AScene);
  }
 }

 if (AScene.OnMouseMove) AScene.OnMouseMove(AEvent, AScene);
 AScene.LastCursorPosition.Load(AScene.CurrentCursorPosition);
}




function clSceneMouseUp(AEvent, AScene)
{
 if (AScene.OnMouseUp) AScene.OnMouseUp(AEvent, AScene);

 AScene.MouseLeft = false;
 AScene.MouseRight = false;
 AScene.MouseDrag = false;
}



function clSceneMouseDown(AEvent, AScene)
{  
 AScene.MouseLeft=(AEvent.button == 0);
 AScene.MouseRight=(AEvent.button == 2);
 AScene.MouseDrag = false;

 if (AScene.OnMouseDown) AScene.OnMouseDown(AEvent, AScene);
}



function clSceneMouseWheel(AEvent, AScene)
{
 if (AScene.OnMouseWheel) AScene.OnMouseWheel(AEvent, AScene);
}



function clSceneMouseDblClick(AEvent, AScene)
{
 if (AScene.OnMouseDblClick) AScene.OnMouseDblClick(AEvent, AScene);
}



function clSceneDestroy()
{
}







function clSceneFlat(AScene)
{
 AScene.Context.setTransform(1, 0, 0, 1, 0, 0);
}

function clSceneDrawBegin(AScene)
{
/*
    var n = clNow();
    AScene.FPS = clSec / (n - AScene.FPSMoment);
    AScene.FPSMoment = n;
*/

 AScene.FPSCount++;
 var n = clNow();
 if (n - AScene.FPSMoment > clSec)
 {
  AScene.FPS = AScene.FPSCount / (n-AScene.FPSMoment) * clSec;
  AScene.FPSCount = 0;
  AScene.FPSMoment = n;
 }
}



function clSceneDrawEnd(AScene)
{

}




function clSceneGetWidth(AScene)
{
    return AScene.Canvas.width;
}



function clSceneGetHeight(AScene)
{
    return AScene.Canvas.height;
}
