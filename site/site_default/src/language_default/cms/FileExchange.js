const FILE_STATUS_WAIT = 'Wait';
const FILE_STATUS_UPLOADING = 'Uploading';
const FILE_STATUS_OK = 'Ok';
const FILE_STATUS_ERROR = 'Error';



function TFileExchange (ALog)
{
    this.Log = ALog; /* Write Log object */
    /* Visual components */
    this.Files = null; /* Files element */
    this.FileList = null; /* */
    this.DownloadContainer = null; /* */
    this.DownloadLink='';

    this.IDParent = 'FileExchange'; /* Defailt parent */
    this.IDGroup = null; /* Group identify */
    return this;
}


TFileExchange.prototype.SetFiles = function(AFiles)
{
    var that = this;
    this.Files = AFiles;
    this.Files.onchange = function()
    {
        that.BuildListFiles();
        that.Start();
    };
    return this;
};



/*
 * Set download link
 */
TFileExchange.prototype.SetDownloadLink = function(ALink)
{
    this.DownloadLink = ALink;
    return this;
};



/*
 * Set download container from AElement
 */
TFileExchange.prototype.SetDownloadContainer= function(AElement)
{
    this.DownloadContainer = AElement;
    this.DownloadContainer.style.display = 'none';
    return this;
};



TFileExchange.prototype.UpdateFileRecord = function(AFile)
{
    var Percent='';
    if (AFile.Total>0) Percent = Math.trunc(AFile.Loaded / AFile.Total * 100) + '%';
    /* Update propertyes */

    AFile.Record.ChildByID('Status').className='Status Status'+AFile.Status;
    AFile.Record.ChildByID('Name').innerHTML=AFile.name;
    AFile.Record.ChildByID('Size').innerHTML=clSizeToStr(AFile.size, '-');
    AFile.Record.ChildByID('Result').innerHTML=AFile.Result;
    AFile.Record.ChildByID('Loaded').innerHTML=Percent;
    return this;
};




TFileExchange.prototype.BuildListFiles = function()
{
    /* Clear file list */
    this.FileList.innerHTML = '';

    var RecordContent = decodeURIComponent('<cl content="FileExchangeRecord.html" convert="URI"/>');
    var c = this.Files.files.length;
    var i = 0;
    while (i<c)
    {
        var iFile = this.Files.files[i];
        iRecord = document.createElement('div');
        iRecord.className = 'Record';
        iRecord.innerHTML = clContentFromObject({}, RecordContent);
        /* Greeting iRecord with iFile  */
        iRecord.File = iFile;
        iFile.Record = iRecord;
        iFile.Status = FILE_STATUS_WAIT;
        /* Insetr new iRecord  in to List */
        this.FileList.append(iRecord);
        this.UpdateFileRecord(iFile);
        i++;
    }
    return this;
};



TFileExchange.prototype.GetNextFile = function()
{
    var Result=null;
    var c=this.Files.files.length;
    var i=0;
    while (i<c && Result==null)
    {
        var iFile = this.Files.files[i];
        if (iFile.Status == FILE_STATUS_WAIT) Result = iFile;
        i++;
    }
    return Result;
};



/*
 * Start files upload. Grate new Group and return it
 */
TFileExchange.prototype.Start = function()
{
    var that=this;

    this.DownloadContainer.style.display='none';

    /*Подготовка параметров*/
    clPost
    ({
        TypeContent:tcXML,
        Pars:true,
        Call:'FileExchange.GetGroup',
        Income:{'IDParent':'FileExchange'},

        /*Отработка после загрузки*/
        OnAfterLoad: function(p)
        {
            if (clPostResult(p))
            {
                that.IDGroup = p.Result.Outcome.IDGroup;
                that.DownloadContainer.href = clContentFromObject({IDGroup:that.IDGroup}, that.DownloadLink);
                that.DownloadContainer.innerHTML = clContentFromObject({IDGroup:that.IDGroup}, that.DownloadLink);
                that.DownloadContainer.style.display=null;
                var NextFile = that.GetNextFile();
                if (NextFile) that.Upload(NextFile);
            }
        }
    });
    return Result;
};



/*
 * Upload file
 */
TFileExchange.prototype.Upload = function(AFile)
{
    var that=this;
    /* Прописываем что файл стал на загрузку */
    AFile.Status = FILE_STATUS_UPLOADING;
    AFile.Loaded = 0;
    /* Создание новой формы */
    var Fm = new FormData();
    /* Заполненеи параметров */
    Fm.append('File', AFile);
    Fm.append('IDGroup', this.IDGroup);
    Fm.append('RandomName', true);
    Fm.append('CreateGroup', true);

    that.UpdateFileRecord(AFile);
    /*Подготовка параметров*/
    clPost
    ({
        File:AFile,
        FormData:Fm,
        TypeContent:tcXML,
        Pars:true,
        Call:'FileExchange.Upload',

        OnUploadProgress: function(p)
        {
            AFile.Loaded = p.loaded;
            AFile.Total = p.total;
            that.UpdateFileRecord(AFile);
        },

        /*Отработка после загрузки*/
        OnAfterLoad: function(p)
        {
            if (clPostResult(p)) p.File.Status = FILE_STATUS_OK;
            else p.File.Status = FILE_STATUS_ERROR;
            that.UpdateFileRecord(p.File);
            /*Upload next file*/
            var NextFile = that.GetNextFile();
            if (NextFile) that.Upload(NextFile);
        },

        /*Отработка ошибки*/
        OnError:function(p)
        {
            p.File.Status = FILE_STATUS_ERROR;
            p.File.Result = 'UnknownError';
            that.UpdateFileRecord(p.File);

            /*Upload next file*/
            var NextFile = that.GetNextFile();
            if (NextFile) that.Upload(NextFile);
        }
    });
};
