class TDescriptParams
{
    static Do( AApplication, ADescript )
    {
        /* Create form and popup */
        var Popup = AApplication.Popup
        ({
            TypeContent: tcText,
            Content: decodeURIComponent( '<cl content="DescriptParams.html" pars="true" convert="URI"/>' )
        });

        /* Init popup */
        AApplication.SelectActivate( Popup );

        /* Set values for form */
        Popup.ChildByID( 'IDDescript' ).Refresh( ADescript.ID );

        Popup.ChildsByClass( 'BtnSave',   Btn => Btn.onclick = () => Save());
        Popup.ChildsByClass( 'BtnLoad',   Btn => Btn.onclick = () => Load());
        Popup.ChildsByClass( 'BtnCancel', Btn => Btn.onclick = () => Popup.Close());

        function Save()
        {
            TRequest.Create( AApplication )
            .SetParam( 'ID', Popup.ChildByID( 'IDDescript' ).value )
            .SetParam( 'Params', Popup.ChildsByClass( 'Parameters' )[0].value )
            .Execute
            ({
                Name: 'Descript.ParamsSave'
            });
        }

        function Load()
        {
            TRequest.Create( AApplication )
            .SetParam( 'ID', Popup.ChildByID( 'IDDescript' ).value )
            .Execute
            ({
                Name: 'Descript.ParamsLoad',
                OnAfterExecute: Result =>
                {
                   Popup.ChildsByClass( 'Parameters', Area => Area.value = Result.Result.Outcome.Params );
                }
            });
        }

        Load();
    }
}



