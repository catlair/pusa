/*
 * Constructor for Task Application
 */

function TTaskApp()
{
    this.Container = null; /* DOM Container for tasks content*/

    this.IDParentDefault = null; /* Default ID parent for new Task */
    this.Content = decodeURIComponent('<cl content="TaskContent.html" pars="true" convert="URI"/>');
    this.ToolbarContent = decodeURIComponent('<cl content="TaskToolbar.html" pars="true" convert="URI"/>');

    return this;
}



/*
 * New task interface Open
 */

TTaskApp.prototype.Open = function(AContainer)
{
    var that = this;
    if (this.Container == null)
    {
        /* Load task css */
        cl.StyleLoad({ID:"Task.css"});
        cl.StyleLoad({ID:"Message.css"});

        /*  */
        cl.ScriptLoad({ID:'Task.js'});
        cl.ScriptLoad({ID:'Message.js'});

        cl.ScriptWait
        (
            ['Task.js', 'Message.js'],
            function()
            {
                /* Set content */
                that.Container = AContainer;
                that.Container.innerHTML = that.Content;
                that.Toolbar = that.Container.ChildByID('Toolbar');
                that.Records = that.Container.ChildByID('Records');
                that.Toolbar.innerHTML = that.ToolbarContent;

                that.Refresh = function()
                {
                    /*Построение списка*/
                    var Search = that.Toolbar.ChildByID('Search').value;
                    var Tasks = new TDescripts();

                    Tasks.RecordConteiner = that.Records;
                    Tasks.RecordContent = decodeURIComponent('<cl content="TaskRecordSimple.html" pars="true" convert="URI"/>');
                    Tasks.OnAfterLoadRecord = function (AConteiner)
                    {
                        AConteiner.ChildByID('BtnEdit').onclick = function()
                        {
                            var Task = new TTask();
                            Task.ID = AConteiner.Record.ID;
                            Task.EditSpecial();
                        }
                    }

                    Tasks.Load
                    ({
                        Find:Search,
                        IDParent:that.IDParentDefault,
                        Library: 'task_load',
                        ProcName: 'TaskLoad',
                        RecordCount:100,
                        RecordCurrent:0
                    });
                }

                /* Add action for refresh*/
                that.Toolbar.ChildByID('BtnRefresh').onclick = that.Refresh;

                /* Add action for New Task*/
                that.Toolbar.ChildByID('BtnAdd').onclick = function()
                {
                    var Task = new TTask();
                    Task.EditSpecial({IDParent: that.IDParentDefault});
                }

                /* */
                that.Refresh();
            }
        );
    }

    return this;
};



/*
 * Curent task interface close
 */

TTaskApp.prototype.Close = function()
{
    if (this.Container != null)
    {
        this.Container.innerHTML='';
    }
    return this;
};
