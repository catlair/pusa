/*
    Catlair JS Copyright (C) 2019  a@itserv.ru

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.


    Модуль содержит перечень функций для pusa.js
    Все функции являются копиями имеющихся модулей.
    При внесении изменений в любую из частей pusa_inline.js необходимо
    вносить изменения в основную библитеку откуда был заимстован код.

    still@itserv.ru
*/


/*
    Возвращает подобие GUID, путь и плюшевый, но лучше чем ничего
*/
function clGUID()
{
    return 'xxxxxxxx-xxxx-xxxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8); return v.toString(16);});
}



/*
    clString.js
*/


/*
    Перебирает все свойства в массиве и возвращает только значения
    - boolean
    - number
    - string
    - symbol
    Исключаются пустые значения, массивы объекты и прочее.
*/
function GetValuesOnly
(
    AObject,    /* Объект для обхода */
    AInclude,   /* Массив имен включаемых значений. Невходящие будут исключены. */
    AExclude    /* Массив имен исключаемых значений. Входящие будут исключены. */
)
{
    var Result = {};

    if( !AInclude ) AInclude = [];
    if( !AExclude ) AExclude = [];

    for( var Key in AObject )
    {
         switch( typeof( AObject[ Key ] ))
         {
             case 'boolean':
             case 'number':
             case 'string':
             case 'symbol':
                 if( AExclude.length == 0 || AExclude.indexOf( Key ) == -1 )
                 {
                     Result[ Key ] = AObject[ Key ];
                 }
             break;
        }
    }
    return Result;
}




/*
    clDOM.js
*/


/*
    Константы селекторы поиска объектов
*/
var SELECTOR_CLASS    = 'Class';
var SELECTOR_NAME     = 'Name';
var SELECTOR_TAG      = 'Tag';
var SELECTOR_ID       = 'ID';


/*
    Поиск детей по имени и селектору
*/
Element.prototype.ChildsOfThis = function( AValue, ASelector, ACall )
{
    var Result = [];
    if( !ASelector ) ASelector = SELECTOR_CLASS;
    for( var i=0; i<this.childNodes.length; i++ )
    {
        var Node  = this.childNodes[i];
        if
        (
            Node.nodeType == 1 &&
            (
                ASelector == SELECTOR_CLASS && Node.classList.contains( AValue ) ||
                ASelector == SELECTOR_NAME  && Node.name      == AValue ||
                ASelector == SELECTOR_TAG   && Node.tagName   == AValue ||
                ASelector == SELECTOR_ID    && Node.id        == AValue
            )
        )
        {          
            Result.push( Node );
            if( ACall ) ACall( Node );
        }
    }
    return Result;
};




/*
    Рекурсивный обход дерева
*/
Element.prototype.ChildRecursion = function( AParams )
{
    if ( AParams.OnBefore ) AParams.OnBefore( this, AParams );

    for (var i in this.childNodes )
    {
        var iNode=this.childNodes[i];
        if (iNode.nodeType == 1) iNode.ChildRecursion( AParams );
    }

    if ( AParams.OnAfter ) AParams.OnAfter( this, AParams );
    return this;
};




Element.prototype.ChildsByClass = function( AClass, ACallback )
{
    var Result = [];
    this.ChildRecursion
    ({
        OnBefore: e => 
        {
             if( e.classList && e.classList.contains( AClass )) Result.push( e );
        }
    });
    if( ACallback ) Result.forEach( e => ACallback( e ) );
    return Result;
};



/*
*/
Element.prototype.ChildsByID = function( AID, ACallback )
{
    var Result = [];
    this.ChildRecursion
    ({
        OnBefore: e =>
        {
            if( e.id == AID ) Result.push( e );
        }
    });
    if( ACallback ) Result.forEach( e => ACallback( e ) );
    return Result;
};




/*
    Циклический поиск родителя по имени тэга
*/
Element.prototype.ParentByTag = function( ATagName )
{
    var Result = this;
    ATagName = ATagName.toUpperCase();
    while( Result && Result.tagName != ATagName ) Result = Result.parentNode;
    return Result;
};



/*
    Show element
*/
Element.prototype.Show = function()
{
    this.style.display = null;
    return this;
};



/*
    Hide element
*/
Element.prototype.Hide = function()
{
    this.style.display = 'none';
    return this;
};



/*
    Поиск родителя по Class name
*/
Element.prototype.ParentByClass = function( AClassName )
{
    var Current = this;
    var Result = null;
    while( Current && ! Result ) {
       if( Current.classList.contains( AClassName ) ) Result = Current;
       Current = Current.parentNode;
    }
    return Result;
};



/*
    Поиск детей по имени и селектору
*/
Element.prototype.ParentFirst = function( AValue, ASelector, ACall )
{
    var Result = null;
    var Current = this;
    while( Current && !Result )
    {
        if
        (
            ASelector == SELECTOR_CLASS && Current.classList.contains( AValue ) ||
            ASelector == SELECTOR_NAME  && Current.name      == AValue ||
            ASelector == SELECTOR_TAG   && Current.tagName   == AValue ||
            ASelector == SELECTOR_ID    && Current.id        == AValue
        )
        {
            Result = Current;
            if( ACall ) ACall( Result );
        }
        Current = Current.parentNode;
    }
    return Result;
};

