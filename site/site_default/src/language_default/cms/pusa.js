/*
    Catlair PHP Copyright (C) 2021 https://itserv.ru

    This program (or part of program) is free software: you can redistribute
    it and/or modify it under the terms of the GNU Aferro General
    Public License as published by the Free Software Foundation,
    either version 3 of the License, or (at your option) any later version.

    This program (or part of program) is distributed in the hope that
    it will be useful, but WITHOUT ANY WARRANTY; without even the implied
    warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
    See the GNU Aferro General Public License for more details.
    You should have received a copy of the GNU Aferror General Public License
    along with this program. If not, see <https://www.gnu.org/licenses/>.

*/

/*
    The Pusa
    It is the front-back of Pusa core.
    It works with PusaBack over ajax.

          ┌──>── Request( URL,Element,Event,GET,POST ) ──>──┐
          │                                                 │
    ╔═════╧═════╗                                     ╔═════╧════╗
    ║ PusaFront ║               (ajax)                ║ PusaBack ║
    ╚═════╤═════╝                                     ╚═════╤════╝
          │                                                 │
          └──<────── Responce( commands and content ) ───<──┘

*/
var JSON_TYPE   = 'JSON';

/* Debug level */
var DEBUG_OFF   = 'Off';
var DEBUG_ON    = 'On';

class TPusa
{
    constructor( ALog )
    {
        this.Log            = ALog;     /* TLog object */
        this.Events         = [];       /* Event timers accumulator. Contain actual timer for each event. */
        this.Requests       = [];
        this.Groups         = [];       /* List of groups */
        this.Debug          = DEBUG_OFF;
        this.LastRequests   = [];       /* Antispam events history. Each event registrated in this array. */
        this.EventTimeouts  = [];       /* Timeots in milliseconds for event {  mousmove: 1000 }*/
    }



    /*
        Create and return new Pusa
    */
    static Create( ALog )
    {
        return new TPusa( ALog );
    }



    /*
        Set all handlers from element AParent using DOM recursion
        Each element with data-event attribute will have event.Class.Method
    */
    SetHandlers
    (
        AParent /* Start element */
    )
    {
        AParent.ChildRecursion
        ({
            OnBefore: e =>
            {
                if( e.getAttribute( 'data-event' ))
                {
                    var Parts = e.getAttribute( 'data-event' ).split( "." );
                    if( Parts.length = 3 )
                    {
                        e.ReplaceEventListener
                        (
                            Parts[0],
                            Event =>
                            {
                                this.Request
                                ({
                                    Event,
                                    Class   : Parts[1],
                                    Method  : Parts[2],
                                    Timeout : null,
                                    Element : e
                                });
                            }
                        );
                    }
                }
            }
        });
        return this;
    }



    /*
        The Pusa request.
        1. Recive array of parameters.
        2. Create timer for controlling events
        3. Send event to back-end.
    */
    Request
    (
        AParams   /* Named array of arguments. Look at TPusa.Send method */
    )
    {
        if( AParams.Timeout )
        {
            /* The event timer start for sending */
            var Key = AParams.Event + '.' + AParams.Class + '.' + AParams.Method;
            if( this.Events[ Key ] ) clearInterval( this.Events[ Key ]);
            this.Events[ Key ] = setTimeout
            (
                () => this.Send( AParams ),
                AParams.Timeout
            );
        }
        else
        {
            /* Direct send */
            this.Send( AParams );
        }
        return this;
    }



    /*
        Send element fo PusaBack.
        Each alement will be sended with content and specific fields.
    */
    SendElement
    (
        AParams   /* Named array of arguments. Look at TPusa.Send method. */
    )
    {
        if
        (
            AParams.Element &&
            AParams.Events &&
            Array.isArray( AParams.Events )
        )
        {
            /* Files */
            if( AParams.Element.files )
            {
                let c = AParams.Element.files.length;
                let IDGroup = clGUID();
                for( let i = 0; i < c; i++ )
                {
                    let File = AParams.Element.files[ i ];
                    /* Send file */
                    this.Send
                    ({
                        Element     : File,
                        Class       : AParams.Class,
                        Method      : AParams.Method,
                        Events      : AParams.Events,
                        IDGroup     : IDGroup,          /* Identifier for group */
                        GroupSize   : c
                    });
                }
            }

            if( AParams.Element.tagName == 'FORM' )
            {
                this.Send
                ({
                    Element     : AParams.Element,
                    Class       : AParams.Class,
                    Method      : AParams.Method,
                    Events      : AParams.Events,
                    Form        : AParams.Element
                });
            }
        }
        return this;
    }



    /*
        Send Pusa request
    */
    Send
    (
        AParams
        /*
            Array of parametes:
            string      Event   - DOM Event from event element;
            string      Element - DOM element, called the event;
            string      Class   - Pusa class controllername for call;
            string      Method  - Method name in Pusa controller;
            int         Timeout - Request Timeout in milliseconds. 0 - the request will be sent immeidiately;
            string      IDGroup - Identifier for group
            Form        Form    - External form
            array       Events  - Array of rules with name of XMLHttpRequest evends and callback class and method for pusa:
                string  Event   - name of event ( loadend, progress, error etc );
                string  Class   - Pusa class controller name for call back;
                string  Method  - Method name in Pusa controller for call back.
        */
    )
    {
        var EventName           = AParams.Event ? AParams.Event.type : null;
        var IDGroup             = AParams.IDGroup ? AParams.IDGroup : clGUID();
        var GroupSize           = AParams.GroupSize ? AParams.GroupSize : 1;
        var IDRequest           = clGUID();
        var Data                = null; /* Create FormData object */
        var AttributesString    = null; /* Strint element attributes */
        var ElementString       = null; /* String element propertyes */
        var Data                = new FormData( AParams.Form );

        /* Create the Request */
        var Request = this.RequestNew
        ({
            Element     : AParams.Element,
            EventName   : EventName,
            IDGroup     : IDGroup,
            ID          : IDRequest
        });

        if( Request )
        {
            /* If element exists in parameters then collect element attributes for request */
            if( AParams.Element )
            {
                /* Set attributes for element */
                var Attr = AParams.Element.attributes;
                if( Attr )
                {
                    let Attributes = [];
                    for( let i = 0; i < Attr.length; i++ )
                    {
                        Attributes[ Attr[i].name ] = Attr[i].value;
                    }
                    AttributesString = JSON.stringify( GetValuesOnly( Attributes ));
                }

                /* Build string of the elements propertyes */
                ElementString = JSON.stringify
                (
                    GetValuesOnly
                    (
                        AParams.Element, [],
                        [
                            /* Exclude following propertyes */
                            'innerHTML',
                            'outerHTML',
                            'textContent',
                            'innerText',
                            'outerText'
                        ]
                    )
                );

                if( AParams.Element instanceof File )
                {
                    Data.append( 'Blob', AParams.Element );
                }
            }

            /* Build income */
            Data.append( 'Pusa'             , true );
            Data.append( 'IDRequest'        , IDRequest );
            Data.append( 'IDGroup'          , IDGroup );
            Data.append( 'GroupSize'        , GroupSize );
            Data.append( 'TypeContent'      , JSON_TYPE );
            Data.append( 'Class'            , AParams.Class );
            Data.append( 'Method'           , AParams.Method );
            Data.append( 'Event'            , JSON.stringify( GetValuesOnly( AParams.Event ) ));
            Data.append( 'URL'              , window.location.href );
            Data.append( 'Element'          , ElementString );
            Data.append( 'Attributes'       , AttributesString );

            Request.Self = AParams.Element;

            /* Set users callback for Request events */
            if( AParams.Events )
            {
                AParams.Events.forEach
                (
                    Rule =>
                    {
                        let EventName   = Rule[ 'Event' ];
                        let Class       = Rule[ 'Class' ];
                        let Method      = Rule[ 'Method' ];
                        if( EventName && Class && Method )
                        {
                            Request.upload.addEventListener
                            (
                                EventName,
                                e =>
                                {
                                    this.Request
                                    ({
                                        Event           : e,            /* Current event from XMLHttpRequest*/
                                        Class           : Class,        /* Class from current rule */
                                        Method          : Method,       /* Method from current rule */
                                        Element         : AParams.Element,
                                        IDGroupParent   : IDGroup
                                    })
                                }
                            );
                        }
                    }
                )
            }

            /* Set callback for request */
            Request.addEventListener
            (
                'loadend',
                Event =>
                {
                    this.Log.JobEnd();
                    this.RequestRemove( Request );

                    /* Get pure content */
                    var Content = Event.target.response;
                    var Result = null;

                    /* Define type content */
                    try
                    {
                        Result = JSON.parse( Content );
                        Request.Result = Result;
                    }
                    catch( e )
                    {
                        console.warn( 'Error in JSON result', Content );
                    }

                    if( Result && Result.Header )
                    {
                        if( Result.Header.Code == 'Ok' )
                        {
                            this.Answer( Request );
                        }
                        else
                        {
                            this.Log.War( Result.Header.Message, true);
                            console.log( Result.Header );
                        }
                    }
                    else
                    {
                        this.Log.War( 'Unknown answer. Check your connection.', true );
                    }
                }
            );

            /* Set callback for the request error event */
            Request.addEventListener
            (
                'error',
                () =>
                {
                    this.Log.JobEnd().Err( 'Pusa request error', true );
                    this.RequestRemove( Request );
                }
            );

            /* Set callback for the request abort event */
            Request.addEventListener
            (
                'abort',
                () =>
                {
                    this.RequestRemove( Request );
                    console.log('Pusa request abort', AParams );
                }
            );


            if( this.Debug == DEBUG_ON )
            {
                console.info( 'Pusa AJAX call ' + AParams.Class + '.' + AParams.Method );
            }

            this.Log.JobBegin();

            /* Send request with Data form and URL */
            Request.open( 'POST', '?pusa=' + AParams.Class + '.' + AParams.Method );
            Request.send( Data );
        }

        return this;
    };



    /*
        Select elements for focus
    */
    SelectElements
    (
        ASelf,      /* Self element */
        AFrom,      /* Array of DOM elements for start searching */
        AParams     /* Searching argumets */
    )
    {
        var Result = [];

        /*
            The pushing element subfunction in to focus list
            if element is not exists in focus list.
        */
        function Push( AElement )
        {
            if( Result.indexOf( AElement ) == -1 )
            {
                Result.push( AElement );
            }
        }

        /* Заполнение From текущим элементов если массив пуст */
        AFrom = AFrom ? AFrom : [ ASelf ];

        switch( AParams.Target )
        {
            /* Set the self element as focus. It will be the only focus element */
            case 'Self':
                if( ASelf ) Push( ASelf );
            break;

            /* Set document.body as focus. It will be the only focus element */
            case 'Body':
                Push( document.body );
            break;

            /* Each focus element set its parent as focus */
            case 'Parent':
                for( var i in AFrom )
                {
                    Push( AFrom[ i ].parentNode );
                }
            break;

            case 'Parents':
                if( AParams.Selector && AParams.Name )
                {
                    for( var i in AFrom )
                    {
                        AFrom[ i ].Parents
                        (
                            AParams.Name,
                            AParams.Selector,
                            e => Push( e )
                        );
                    }
                }
                else
                {
                    console.warn( 'Pusa Parents: Type or name selector not defined' );
                }
            break;

            case 'ParentFirst':
                for( var i in AFrom )
                {
                    AFrom[ i ].Parent( AParams.Name, AParams.Selector, e => Push( e ) );
                }
            break;

            case 'ChildsOfThis':
                for( var i in AFrom )
                {
                     Result = AFrom[ i ].ChildsOfThis( AParams.Name, AParams.Selector );
                }
            break;

            case 'Childs':
                if( AParams.Selector && AParams.Name )
                {
                    for( var i in AFrom )
                    {
                        AFrom[ i ].Childs
                        (
                            AParams.Name,
                            AParams.Selector,
                            e => Push( e )
                        );
                    }
                }
                else
                {
                    console.warn( 'Pusa Childs: Type or name selector not defined' );
                }
            break;
        }
        return Result;
    }



    /*
        Processing Pusa answer
    */
    Answer( ARequest )
    {
        return this.Commands
        (
            ARequest.Result.Pusa,   /* Send commands */
            ARequest.Self           /* Send self element */
        );
    }



    /*
        Processing the pusa commands
    */
    Commands
    (
        ACommands,  /* Pusa commands list */
        ASelf       /* Self element */
    )
    {
        var Focus = [ ASelf ];
        /* Loop for the list of commands received from Pusa-Back */
        ACommands.forEach
        (
            Line =>
            {
                /* Get new command */
                var Command = Line[ 0 ] ? Line[ 0 ] : '';

                /* Get arguments for command */
                var Prm = Line[ 1 ] ? Line[ 1 ] : {};

                switch( Command )
                {
                    /* Receive and output message to console */
                    case 'Debug':
                        this.Debug = Prm[ 'Level' ];
                    break;

                    /* Receive and output message to console */
                    case 'Console':
                        console.log( Prm.Message );
                    break;

                    /* Select and focus to DOM elements from current focus elements */
                    case 'Select':
                        Focus = this.SelectElements
                        (
                            ASelf,
                            Focus,
                            {
                                Target      : Prm[ 'Target' ],
                                Name        : Prm[ 'Name' ],
                                Selector    : Prm[ 'Selector' ]
                            }
                        );

                        if( this.Debug == DEBUG_ON && Focus.length == 0 )
                        {
                            console.warn
                            (
                                'No elements by' +
                                ' target:' + Prm[ 'Target' ] +
                                ' selector type:' + Prm[ 'Selector' ] +
                                ' selector name:' + Prm[ 'Name' ]
                            );
                        }
                    break;

                    /* Add class to class list */
                    case 'ClassAdd':
                        if( Prm.Class )
                        {
                            Focus.forEach( e => e.classList.add( Prm.Class ));
                        }
                    break;

                    /* Remove class from class list */
                    case 'ClassRemove':
                        if( Prm.Class )
                        {
                            Focus.forEach( e => e.classList.remove( Prm.Class ));
                        }
                    break;

                    /* Set event on DOM element */
                    case 'Event':
                        switch( Prm.Target )
                        {
                            /* Add listeners for focused DOM elements */
                            case 'DOM':
                                Focus.forEach
                                (
                                    e =>
                                    {
                                        e.ReplaceEventListener
                                        (
                                            Prm.Event,
                                            event =>
                                            {
                                                /* Send callback to PusaBack */
                                                if( Prm.Class && Prm.Method && Prm.Event )
                                                {
                                                    this.Request
                                                    ({
                                                        Event   : event,
                                                        Class   : Prm.Class,
                                                        Method  : Prm.Method,
                                                        Timeout : Prm.Timeout,
                                                        Element : e
                                                    });
                                                }
                                                /* Call JS */
                                                if( Prm.JS )
                                                {
                                                    eval( Prm.JS );
                                                }
                                                /* Call commands */
                                                if( Prm.Commands )
                                                {
                                                    TPusa.Create( this.Log ).Commands( Prm.Commands, e );
                                                }
                                                /* Stop floating event in DOM if catch is true */
                                                if( Prm.Catch ) event.stopPropagation();
                                            }
                                        );
                                    }
                                );
                            break;

                            /* Add new Listener for window object */
                            case 'Win':
                                window.addEventListener
                                (
                                    Prm.Event,
                                    event => this.Request
                                    ({
                                        Event   : event,
                                        Class   : Prm.Class,
                                        Method  : Prm.Method,
                                        Timeout : Prm.Timeout,
                                        Element : window
                                    })
                                );
                            break;

                            /* Add new listener for document object */
                            case 'Doc':
                                document.addEventListener
                                (
                                    Prm.Event,
                                    event => this.Request
                                    ({
                                        Event   : event,
                                        Class   : Prm.Class,
                                        Method  : Prm.Method,
                                        Timeout : Prm.Timeout,
                                        Element : document
                                    })
                                );
                            break;
                        }
                    break;

                    /* Replace any text in element attributes or content */
                    case 'Replace':
                        Focus.forEach
                        (
                            e =>
                            {
                                /* Attributes loop */
                                for (var i = 0; i < e.attributes.length; i++ )
                                {
                                    var s = e.attributes[i].value;
                                    /* Replace loop */
                                    for( var Key in Prm.Values )
                                    {
                                        var r = new RegExp( Key );
                                        s = s.replace( r, Prm.Values[ Key ]);
                                    }
                                    e.attributes[i].value = s;

                                    /* event listener replace */
                                    if( e.attributes[i].name=='data-event' ) this.SetHandlers( e );
                                }
                            }
                        );
                    break;

                    /* Set attributes for focused elements */
                    case 'Attr':
                        if( Prm.Values )
                        {
                            Focus.forEach
                            (
                                e =>
                                {
                                    for( var Key in Prm.Values )
                                    {
                                        e.setAttribute( Key, Prm.Values[ Key ]);
                                        /* The element can recevie new Handlers with the change of data-event */
                                        if( Key.toLowerCase().substr(0,4) == 'data-event' ) this.SetHandlers( e );
                                    }
                                }
                            );
                        }
                    break;

                    /* Set properties for element */
                    case 'Prop':
                        if( Prm.Values )
                        {
                            Focus.forEach
                            (
                                e =>
                                {
                                    for( var Key in Prm.Values )
                                    {
                                        e[ Key ] = Prm.Values[ Key ];
                                        /* Set a handlers for innerHTML changed */
                                        if( Key == 'innerHTML' ) this.SetHandlers( e );
                                    }
                                }
                            );
                        }
                    break;

                    /* Установка атрибутов для элементов */
                    case 'Style':
                         if( Prm.Values )
                         {
                             /* Прописываем значения стилей */
                             Focus.forEach
                             (
                                 e => { for( var Key in Prm.Values ) e.style[ Key ] = Prm.Values[ Key ] }
                             );
                         }
                    break;

                    /*
                        Create DOM elements
                        Elements will be added if does not exists with the passed ID
                    */
                    case 'Create':
                         /* New elements array */
                         var NewElements = [];

                         /* Loop for focused elements */
                         Focus.forEach
                         (
                             e =>
                             {
                                 /* Find existing elements for the passed ID */
                                 var Exists = Prm.ID ? e.ChildsOfThis( Prm.ID, SELECTOR_ID ) : [];
                                 if( Exists.length == 0 )
                                 {
                                     /* Create new element. */
                                     var NewElement = document.createElement( Prm.TagName );
                                     NewElements.push( NewElement );
                                     switch( Prm.Position )
                                     {
                                        default:
                                        case 'Last':   e.append( NewElement ); break;
                                        case 'First':  e.prepend( NewElement ); break;
                                        case 'Before': e.before( NewElement ); break;
                                        case 'After':  e.after( NewElement ); break;
                                     }
                                     if( Prm.ID ) NewElement.id = Prm.ID;
                                 }
                                 else
                                 {
                                     /* Find elements for ID and move its to New elements */
                                     Exists.forEach( a => NewElements.push( a ));
                                 }
                             }
                         );

                         /* Set focus to the selected elements */
                         Focus = NewElements;
                    break;

                    /* Delete elements from DOM */
                    case 'Delete':
                        Focus.forEach( e => e.remove() );
                    break;

                    /* Scroll into View element */
                    case 'View':
                         Focus.forEach
                         (
                            e => e.scrollIntoView
                            ({
                                behavior    : 'smooth',
                                block       : 'nearest',
                                inline      : 'start'
                            })
                         );
                    break;

                    /* Scrall into View element */
                    case 'Horizontal':
                        Focus.forEach
                        (
                            e => e.scrollLeft = Prm.Place * e.scrollWidth
                        );
                    break;

                    /* Scrall into View element */
                    case 'Vertical':
                        Focus.forEach
                        (
                            e => e.scrollTop = Prm.Place * e.scrollHeight
                        );
                    break;

                    /* Execute code js */
                    case 'JS':
                        eval( Prm );
                    break;

                    /* Page reload by URL */
                    case 'Open':
                        if( window.location.href == Prm.URL && Prm.Target == '_self' )
                        {
                            /* Firefox does not reopen the link as the current one */
                            window.location.reload();
                        }
                        else
                        {
                            window.open( Prm.URL, Prm.Target );
                        }
                    break;

                    /* URL sets for a page without reloading */
                    case 'URL':
                        window.history.pushState( null, null, Prm.URL );
                    break;

                    /* Timer */
                    case 'Timer':
                        if( Prm.Timeout && Prm.Class && Prm.Method )
                        {
                            Focus.forEach
                            (
                                e =>
                                {
                                    e[ 'Timer.' + Prm.Class + '.' + Prm.Method ] = setTimeout
                                    (
                                        () =>
                                        {
                                            this.Request
                                            ({
                                                Element : e,
                                                Class   : Prm.Class,
                                                Method  : Prm.Method,
                                                Timeout : 0
                                            });
                                        },
                                        Prm.Timeout
                                    )
                                }
                            );
                        }
                    break;

                    /* Send elements to server */
                    case 'Send':
                        Focus.forEach
                        (
                            e =>
                            this.SendElement
                            ({
                                Element : e,
                                Class   : Prm.Class,
                                Method  : Prm.Method,
                                Events  : Prm.Events
                            })
                        );
                    break;


                }
            }
        );
        return this;
    }



    /**************************************************************************
        Requests
    */


    /*
        Create new XMLHTTPRequest and store it in to global array Requests
    */
    RequestNew( AParams )
    {
        var Result = null;

        if( this.RequestCheck( AParams.EventName ))
        {
            /* Close all previous request for current parameter without ID */
            this.RequestsClose
            ({
                EventName   : AParams.EventName,
                Element     : AParams.Element,
                IDGroup     : AParams.IDGroup
            });
            /* Create new request */
            Result = new XMLHttpRequest();
            /* Set parameters for new request */
            Result.Params = AParams;
            /* Push new request in to list */
            this.Requests.push( Result );
        }
        return Result;
    }



    /*
        Antispam check request by event name
    */
    RequestCheck( AEventName )
    {
        var Result = true;
        var Now = Date.now();

        /* Define antispam timeout */
        AEventName = AEventName ? AEventName : 'any';
        var Timeout = this.EventTimeouts[ AEventName ];
        if( !Timeout ) Timeout = 0;

        /* Check antispam timeout */
        var LastRequest = this.LastRequests[ AEventName ];
        if( LastRequest && LastRequest + Timeout > Now )
        {
            Result = false;
        }
        else
        {
            this.LastRequests[ AEventName ] = Now;
        }

        return Result;
    }



    /*
        Close requests by params
    */
    RequestsClose( AParams )
    {
        var Requests = this.RequestsFind( AParams );
        for( Request of Requests ) Request.abort();
        return true;
    }



    /*
        Return list of Requests by conditions
    */
    RequestsFind( AParams )
    {
        var Result = [];
        for( Request of this.Requests )
        {
            if
            (
                (!AParams.Element   || AParams.Element      && ( AParams.Element    == Request.Params.Element )) &&
                (!AParams.EventName || AParams.EventName    && ( AParams.EventName  == Request.Params.EventName )) &&
                (!AParams.IDGroup   || AParams.IDGroup      && ( AParams.IDGroup    == Request.Params.IDGroup )) &&
                (!AParams.ID        || AParams.ID           && ( AParams.ID         == Request.Params.ID ))
            )
            {
                Result.push( Request );
            }
        }
        return Result;
    }



    /*
        Remove XMLHTTPRequest from request list
    */
    RequestRemove( ARequest )
    {
        let Index = this.Requests.indexOf( ARequest );
        this.Requests.splice( Index, 1 );
        return this;
    }
}
