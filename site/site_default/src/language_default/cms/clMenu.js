/*
   Выпадающее меню
   Catlair JS
   Скрипт содержит функции позволяющие управлять выпадающими меню.
   Расширяет возможности класса TCatlair
   Вроде бы скрипт доведён до работоспособного состояния.
*/

/*Перечень символических констант определяющих метод получения информации для выпадающего меню*/
var tcNone        = 0; /*Контент не загружается*/
var tcText        = 1; /*Текстовое содержимое*/
var tcURL         = 2; /*Ссылка на URL которая будет загружена*/
var tcObject      = 3; /*ID объекта из которого будет принят контент*/
var tcObjectURI   = 4; /*ID объекта из которого будет принят контент в URI*/
var tcURLCache    = 5; /*Ссылка на URL которая будет загружена в меню и может быть закеширована*/

var BTN_YES     = 'Yes';
var BTN_NO      = 'No';
var BTN_CANCEL  = 'Cancel';
var BTN_OK      = 'Ok';

var ptDefault     = 1;

var mtInfo = 'info';
var mtWarning = 'warning';
var mtError = 'error';
var mtConfirm = 'confirm';

var mbOk = 'mbOk';
var mbCancel = 'mbCancel';



/*
   Функция открывающая меню
   AParams.ID - id объекта используемого как выпадающее меню. Рекомендуется DIV. В случае если параметр не передан пустым, используется  объект со случайным идентификатором.
   AParams.Object - объект, используемый как выпадающее меню
   AParams.TypeContent - тип контента.
   AParams.Content - значение интерпритируемое в зависимости от типа контента.
   AParams.Pos - позиция на экране. Если не передана, то попап выводится на мышкурсоре.
   AParams.Class - имя класса которое будет примененео к окну если указано
   AParams.FullScreen - признак того что попап открывается на всеь экран
*/

TCatlair.prototype.Popup = function( AParams )
{
    var that = this;

    /*
        Функция обратного вызова для таймера корректировки положения окна на экране.
        Выполняет перемещение меню в рамку экрана
    */
    function WinPos( AObject )
    {
        var DocSize   = new clVector().Load(clGetSizeDocument()); /*размер документа*/
        var ObjPos    = new clVector().Load(clGetObjectPos(AObject)); /*размер объекта*/
        var ObjSize   = new clVector().Load(clGetObjectSize(AObject)); /*размер объекта*/
        var NewObjPos = new clVector().Load(ObjPos); /*новая позиция объекта*/

        if (DocSize.x < ObjPos.x+ObjSize.x-5) NewObjPos.x = DocSize.x - ObjSize.x-5;
        if (DocSize.y < ObjPos.y+ObjSize.y-5) NewObjPos.y = DocSize.y - ObjSize.y-5;
        if (NewObjPos.x < 5) NewObjPos.x = 5;
        if (NewObjPos.y < 5) NewObjPos.y = 5;

        if (!ObjPos.Equal(NewObjPos)) clSetObjectPos(AObject, NewObjPos);
    }



    function Show( AObject )
    {
        if ( AParams.Object )
        {
            AObject.OldParent = AParams.Object.parentNode;
            AParams.Object.remove();
        }

        /* Устанавливаем видимость объект подложки*/
        AObject.Back.style.display= null;

        /* Устанавливаем видимость */
        AObject.Back.append( AObject );
        AObject.style.display = null;

        /* Set floating */
        AObject.onclick = ( event ) => event.stopPropagation();

        /* создаем таймер для позиционирования открытых окон в случае если позиционирование необходимо */
        if (AObject.Timer == null && AParams.Pos != ptDefault && !AParams.FullScreen)
        {
            AObject.Timer=setInterval( () => WinPos( AObject ), 100 );
        }

        if ( AParams.Pos != ptDefault && !AParams.FullScreen) WinPos(AObject);
    }



    /*Проверка параметров*/
    AParams.ID = AParams.ID ? AParams.ID : clGUID();
    AParams.Class = AParams.Class ? AParams.Class : 'MenuPopup';
    if ( ! AParams.Pos ) AParams.Pos = clVector.Create( posX-5, posY-5 );

    /* Определение объекта по Object или по ID */
    if ( AParams.Object ) var obj = AParams.Object;
    else var obj=document.getElementById( AParams.ID );

    /*Создание объекта самого окна если оно не существует*/
    if ( !obj )
    {
        /*Не удалось найти элемент, значит создаем его*/
        obj = document.createElement( 'div' ); /*Создали элемент*/
        obj.style.display='none';
        obj.id=AParams.ID;
        obj.name=AParams.ID;
        obj.className = AParams.Class;
        obj.innerHTML='';
        obj.Delete=true;
        obj.style.position='fixed';
        if( AParams.FullScreen )
        {
            obj.style.top='0';
            obj.style.bottom='0';
            obj.style.left='0';
            obj.style.right='0';
        }
        document.body.append( obj );
    }

    /* Создание объекта подложки покрывающего весь экран */
    obj.Back = document.createElement( 'div' ); /*Создали элемент*/
    obj.Back.style.position = 'fixed';
    obj.Back.style.zIndex=200;
    obj.Back.style.top='0px';
    obj.Back.style.bottom='0px';
    obj.Back.style.left='0px';
    obj.Back.style.right='0px';
    obj.Back.style.display='none';
    obj.Back.className='BackObject';
    document.body.append( obj.Back );
    obj.Back.onclick = () => obj.Close();

    /*Устанавливаем процедуру закрытия окошка*/
    obj.Close = function()
    {
        /* Users onclose */
        if( AParams.OnClose ) AParams.OnClose( AParams );

        /*Останавливаем таймер центрирования окошка*/
        clearInterval( this.Timer );
        this.Timer=null;

        if ( obj.OldParent )
        {
            obj.remove();
            obj.OldParent.append( obj );
            obj.OldParent = null;
            this.style.display = null;
        }
        else
        {
            /*Убираем объект с экрана*/
            this.style.display='none';
            if ( this.Delete )
            {
                this.innerHTML='';
                this.remove();
            }
        }

        /*Удаление фонового объекта*/
        if (this.Back)
        {
            this.Back.style.display='none';
            this.Back.remove();
        }
    };


    /* устанавливаем позицию если передана позиция */
    if (AParams &&AParams.Pos != ptDefault && !AParams.FullScreen) clSetObjectPos(obj, AParams.Pos);

    /*определяем тип контента*/
    switch(AParams.TypeContent)
    {
        default:
        case tcNone:
            Show( obj );
        break;

        /*загружаем контент переданный в параметр*/
        case tcText:
            obj.innerHTML = AParams.Content;
            Show( obj );
        break;

        /*загружаем контент из объекта*/
        case tcObject:
            objContent=document.getElementById( AParams.Content );
            if (objContent.innerHTML!='')
            {
                obj.innerHTML=objContent.innerHTML;
                Show( obj );
            }
        break;

        /*загружаем контент из урла*/
        case tcURL:
            obj.innerHTML='';
            this.Post
            ({
                    Template:    AParams.Content,
                    Conteiner:   obj,
                    OnAfterLoad: Result =>
                    {
                        /* Функция обратного вызова для события завершения загрузки */
                        Result.Conteiner.innerHTML = Result.Content;
                        Show( Result.Conteiner );
                    }
            });
        break;

        /*загружаем контент из объекта*/
        case tcObjectURI:
            objContent=document.getElementById(AContent);
            if (objContent.innerHTML!='')
            {
                obj.innerHTML=decodeURIComponent(objContent.innerHTML);
                if (AParams.Pos!=ptDefault) clSetObjectPos(obj, obj.p);
                Show(obj);
            }
        break;
    }


    AParams.Popup = obj;
    return obj;
};








/*
   вывод диалога подтверждения с одной кнопкой в стиле ДА-УВЕРЕН
   Buttons: [ BTN_YES, BTN_NO, BTN_OK, BTN_CANCEL ]
   Caption: confirmation text
   FullScreen: true or falce
   OnYes: user function
   OnNo: user function
   OnCancel: user function
   OnOk: user function
*/
TCatlair.prototype.Confirm = function( AParams )
{
    var that = this;

    if( AParams.Template )
    {
        clPost({ Template:AParams.Template, OnAfterLoad: Result => Display( Result.Content ) });
    }
    else
    {
        Display( AParams.Caption );
    }

    function Display( AContent )
    {
        /* Дополение параметров */
        if ( !AParams.ImageClass ) AParams.ImageClass = 'ImageOk';
        
        /* Построение контента */
        var Form = decodeURIComponent('<cl content="ConfirmBtn.html" Pars="True" Convert="URI"/>');
        Form = clContentFromObject( AParams, Form );
        Form = clContentFromObject({ Content: AContent ? AContent : '' }, Form );

        /* Вывод окошка с кнопкой */
        var Popup = that.Popup({ TypeContent:  tcText, Content: Form, FullScreen:AParams.FullScreen });

        Popup.ChildsByClass
        (
            'BtnOk',
            Btn => 
            {
                Btn.SetVisible( !AParams.Buttons || AParams.Buttons.includes( BTN_OK ));
                Btn.onclick = () =>
                {
                    if( AParams.OnClick ) AParams.OnClick( AParams );
                    if( AParams.OnOk ) AParams.OnOk( AParams );
                    Popup.Close();
                }
            }
        );

        Popup.ChildsByClass
        (
            'BtnYes',
            Btn => 
            {
                Btn.SetVisible( AParams.Buttons && AParams.Buttons.includes( BTN_YES ));
                Btn.onclick = () =>
                {
                    if( AParams.OnYes ) AParams.OnYes( AParams );
                    Popup.Close();
                }
            }
        );

        Popup.ChildsByClass
        (
            'BtnNo',
            Btn => 
            {
                Btn.SetVisible( AParams.Buttons && AParams.Buttons.includes( BTN_NO ));
                Btn.onclick = () =>
                {
                    if( AParams.OnNo ) AParams.OnNo( AParams );
                    Popup.Close();
                }
            }
        );

        Popup.ChildsByClass
        (
            'BtnCancel',  
            Btn =>
            {
                Btn.SetVisible( AParams.Buttons && AParams.Buttons.includes( BTN_CANCEL ));
                Btn.onclick = () =>
                {
                    if( AParams.OnCancel ) AParams.OnCancel( AParams );
                    Popup.Close();
                }
            }
        );
    }
};
