/**
 Catlair JS Copyright (C) 2019  a@itserv.ru

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

 Vector coordinate work
 http://catlair.net/?template=clVector.js

 still@itserv.ru

 Plans:
 Сделать надо нормальный генератор RND и на него переделать метод RND для получения случайной точки.
*/


var clEpsilon = 1e-8; /* Small value */

var clPI2 = Math.PI*2;
var clPI1 = Math.PI;
var clPI05 = Math.PI*0.5;


class clVector
{
    constructor( AX,AY,AZ )
    {   
        this.x = AX ? AX : 0;
        this.y = AY ? AY : 0;
        this.z = AZ ? AZ : 0;
    }



    static Create( AX,AY,AZ )
    {
        return new clVector( AX,AY,AZ );
    }



    static CreateZero()
    {
        return new clVector();
    }



    static CreateX()
    {
        return new clVector( 1, 0, 0 );
    }



    static CreateY()
    {
        return new clVector( 0, 1, 0 );
    }



    static CreateZ()
    {
        return new clVector( 0, 0, 1 );
    }



    Set( AX, AY, AZ )
    {
        this.x = AX ? AX : 0;
        this.y = AY ? AY : 0;
        this.z = AZ ? AZ : 0;

        return this;
    }


    
    SetGeo( ALat, ALong )
    {
        var ARad = ALong / 360 * clPI1;
        var BRad = ALat / 360 * clPI1;
        this.Set( Math.cos(ARad) * Math.cos(BRad), Math.sin(ARad) * Math.cos(BRad), Math.sin(BRad) );
        return this;  
    }



    Get()
    {
        return clVector.Create( this.x, this.y, this.z );
    }



    SetZero()
    {
        this.x=0;
        this.y=0;
        this.z=0;
        return this;
    }



    Load( AVector )
    {
        this.x = AVector.x;
        this.y = AVector.y;
        this.z = AVector.z;
        this.v = AVector.v;
        return this;
    }



    Mov(AVector)
    {
        AVector.x=this.x;
        AVector.y=this.y;
        AVector.z=this.z;
        return AVector;
    }



    Not()
    {
        this.x = -this.x;
        this.y = -this.y;
        this.z = -this.z;
        return this;
    }



    Equal(AVector)
    {
        return 
        Math.abs( this.x - AVector.x ) < clEpsilon && 
        Math.abs( this.y - AVector.y ) < clEpsilon && 
        Math.abs( this.z - AVector.z ) < clEpsilon;
    }



    Epsilon()
    {
        return (Math.abs(this.x) < clEpsilon && Math.abs(this.y) < clEpsilon && Math.abs(this.z) < clEpsilon);
    }



    Add(AVector)
    {
        this.x=this.x+AVector.x;
        this.y=this.y+AVector.y;
        this.z=this.z+AVector.z;
        return this;
    }



    Sub(AVector)
    {
        this.x=this.x-AVector.x;
        this.y=this.y-AVector.y;
        this.z=this.z-AVector.z;
        return this;
    }



    Scal(AScale)
    {
        this.x=this.x*AScale;
        this.y=this.y*AScale;
        this.z=this.z*AScale;
        return this;
    }




    Mul(AVector)
    {
        this.x=this.x*AVector.x;
        this.y=this.y*AVector.y;
        this.z=this.z*AVector.z;
        return this;
    }



    Magnitude()
    {
       return Math.sqrt( this.x * this.x + this.y * this.y + this.z * this.z );
    }



    /*Расстояние от текущего вектора до AV*/
    Distance( AV )
    {
       var dx = AV.x - this.x;
       var dy = AV.y - this.y;
       var dz = AV.z - this.z;
       return Math.sqrt( dx * dx + dy * dy + dz * dz );
    };





    String()
    {
        return 'x:'+clFloatToStr(this.x)+' y:'+clFloatToStr(this.y)+' z:'+clFloatToStr(this.z);
    }

}