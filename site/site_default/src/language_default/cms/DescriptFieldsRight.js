class TDescriptFieldsRight
{
    static Popup( ADescript, AParams )
    {
        var Application = ADescript.Application;

        /* Create form and popup */
        var Popup = Application.Popup
        ({
            TypeContent: tcText,
            Content: decodeURIComponent( '<cl content="FieldsRightPopup.html" pars="true" convert="URI"/>' )
        });

        /* Init popup */
        Application.SelectActivate( Popup );

        /* Set values for form */
        Popup.ChildByID( 'IDObject' ).Refresh( AParams.IDObject ? AParams.IDObject : null );
        Popup.ChildByID( 'IDDescript' ).Refresh( AParams.IDDescript ? AParams.IDDescript : null );
        Popup.ChildByID( 'Field' ).value = AParams.Field ? AParams.Field : null;


        Popup.Add = Params =>
        {
            Application.Post
            ({
                Call: 'TypeObject.FieldRightAdd',
                Income: Params,
                OnAfterLoad: AResult => Application.PostResult( AResult )
            });
        };


        Popup.Delete = Params =>
        {
            Application.Post
            ({
                Call: 'TypeObject.FieldRightDelete',
                Income: Params,
                OnAfterLoad: AResult => Popup.Refresh()
            });
        };


        Popup.Refresh = () =>
        {
            var List = Popup.ChildByID( 'List' );
            var RecordContent = decodeURIComponent( '<cl content="FieldsRightRecord.html" pars="true" convert="URI"/>' );
            Application.Post
            ({
                Call: 'TypeObject.FieldRightSelect',
                Form: Popup.ChildByID( 'Form' ),
                OnAfterLoad:function( AResult )
                {
                    if( Application.PostResult( AResult ))
                    {
                        List.SetContent( '' );
                        AResult.Result.Data.Records.Dataset.forEach
                        ( 
                            Record => 
                            {
                                var RecordContainer = document.createElement( 'div' );
                                RecordContainer.className = 'Record';
                                RecordContainer.SetContent( clContentFromObject( Record, RecordContent ));
                                List.append( RecordContainer );

                                RecordContainer.ChildByID
                                (
                                    'BtnDel',
                                    BtnDel => BtnDel.onclick = () =>
                                    {
                                        BtnDel.Record = Record;
                                        Application.Confirm
                                        ({
                                            OnClick: function()
                                            {
                                                Popup.Delete
                                                ({
                                                    IDObject: BtnDel.Record.IDObject, 
                                                    Field: BtnDel.Record.Field
                                                });
                                            }
                                        });
                                    }
                                );

                                RecordContainer.ChildByID
                                (
                                    'Right',
                                    Element => 
                                    {
                                        Element.value = Record.Right;
                                        Element.Record = Record;
                                        Element.onchange = () =>
                                        {
                                            Popup.Add
                                            ({
                                                IDObject: Element.Record.IDObject, 
                                                IDDescript: Element.Record.IDDescript, 
                                                Field: Element.Record.Field, 
                                                Right: Element.value
                                            });
                                        }
                                    }
                                );

                            }
                        );
                    }
                }
            });
        };


        /* Action on ok */
        Popup.ChildByID( 'BtnRefresh' ).onclick = function( AEvent )
        {
             Popup.Refresh(); 
        };

        /* Action on ok */
        Popup.ChildByID( 'BtnFieldAdd' ).onclick = function( AEvent )
        {
            Application.Post
            ({
                Call: 'TypeObject.FieldRightAdd',
                Form: this.form,
                OnAfterLoad: AResult =>
                {
                    if( cl.PostResult( AResult )) Popup.Refresh(); 
                }
            });
        };

        /* Action on cancel */
        Popup.ChildByID( 'BtnCancel' ).onclick = function()
        {
            Popup.Close();
        };
    }
}