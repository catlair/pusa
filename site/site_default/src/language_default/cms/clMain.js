/*
     Start point for Monhive application
*/

/* Inline include core module */
<cl content="loader.js"/>

window.onload = function ()
{
    clScriptWait
    (
        [
            'clResult.js',
            'clDebug.js',
            'clFoundation.js',

            'ajax.js',
            'clVector.js',
            'clDOM.js',
            'clAuth.js',
            'clString.js',
            'clMouse.js',
            'clMenu.js',
            'clMoment.js',
            'clConsole.js',
            'clMouse.js',
            'clWindow.js'
        ],

        function()
        {
            cl = new TCatlair();

            /* Load CSS styles */
            cl.StyleLoad({ ID: 'clBase.css' });
            cl.StyleLoad({ ID: 'clWindow.css' });
            cl.StyleLoad({ ID: 'Toolbar.css' });
            cl.StyleLoad({ ID: 'Menu.css' });
        }
    );
}
