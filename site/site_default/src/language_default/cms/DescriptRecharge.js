class TDescriptRecharge
{
    static Popup( ADescript, AParams )
    {
        var Application = ADescript.Application;

        var Content = decodeURIComponent( '<cl content="DescriptRechargeForm.html" pars="true" convert="URI"/>' );
        var Popup = Application.Popup({ TypeContent:tcText, Content:Content });

        /* Popup prepare */
        Application.MomentActivate( Popup );
        Application.SelectActivate( Popup );

        /* Set values for select */
        var Object = Popup.ChildByID( 'IDDescript' );
        Object.Refresh( ADescript.ID );

        var Sale = Popup.ChildByID( 'IDSale' );
        Sale.Refresh( AParams.IDSale );       

        /* Check DT filed */
        var DTBegin = Popup.ChildByID( 'DTBegin' );
        var DTEnd = Popup.ChildByID( 'DTEnd' );

        function Refresh()
        {
            Application.Post
            ({
                Call: 'Charge.PrepareRecharge',
                Income:
                {
                    IDDescript:Object.value,
                    IDSale:Sale.value
                },
                OnAfterLoad: Result =>
                {
                    if( Application.PostResult( Result ))
                    {
                        DTBegin.value = Result.Result.Outcome.DTBegin;
                        DTEnd.value = Result.Result.Outcome.DTEnd;
                        Sale.Refresh( Result.Result.Outcome.IDSale );
                    }
                }                 
            });
        };

        Refresh();

        Popup.ChildByID( 'BtnRechargeRefresh', Btn => Btn.onclick = () => Refresh());
        Popup.ChildByID( 'BtnCancel', Btn => Btn.onclick = () => Popup.Close());
        Popup.ChildByID
        (
            'BtnRechargeOk', Btn =>
            {
                Btn.onclick = () =>
                { 
                    /* Call Recharge */
                    Application.Post
                    ({
                        Call: 'Charge.Recharge',
                        Income:       
                        {
                            IDDescript:Object.value,
                            IDSale:Sale.value,
                            DTBegin: DTBegin.value,
                            DTEnd: DTEnd.value
                        },
                        OnAfterLoad: function( AResult )
                        {
                            if( Application.PostResult( AResult ))
                            {
                                Application.Log.Inf( 'Ok', true );
                                Popup.Close();
                            }
                        }                 
                    });
                }
            }
        );
    }


    /*
        Initialize the form for multiple recgarge procedure
    */
    static InitPopup( ADescript, AParams )
    {
        var Application = ADescript.Application;

        var Content = decodeURIComponent( '<cl content="DescriptRechargeInitForm.html" pars="true" convert="URI"/>' );
        var Popup = Application.Popup({ TypeContent:tcText, Content:Content });

        /* Popup prepare */
        Application.MomentActivate( Popup );
        Application.SelectActivate( Popup );

        /* Set values for select */
        var Object = Popup.ChildByID( 'IDDescript', Element => Element.Refresh( ADescript.ID ) );
        /* Check DT filed */
        var DTBegin = Popup.ChildByID( 'DTBegin' );
        /* Set client */
        var IDClient = Popup.ChildByID( 'IDClient', Element => Element.Refresh( AParams.IDSale ) );

        Popup.ChildsByClass( 'BtnCancel', Btn => Btn.onclick = () => Popup.Close());
        Popup.ChildsByClass
        (
            'BtnOk', Btn =>
            {
                Btn.onclick = () =>
                { 
                    /* Call Recharge */
                    Application.Post
                    ({
                        Call: 'Charge.RechargeInit',
                        Income:       
                        {
                            IDType:Object.value,
                            DTBegin: DTBegin.value,
                            IDClient: IDClient.value 
                        },
                        OnAfterLoad: function( AResult )
                        {
                            if( Application.PostResult( AResult ))
                            {
                                Application.Log.Inf( 'Ok', true );
                                Popup.Close();
                            }
                        }                 
                    });
                }
            }
        );
    }



}