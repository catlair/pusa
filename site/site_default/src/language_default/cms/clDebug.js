/*
    Catlair JS Copyright (C) 2019  a@itserv.ru

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

    Debug system
    http://catlair.net/?template=clDebug.js

    still@itserv.ru

    Used SCC styles:
       .Log for log window;
       .PostIndicator for job indicator with loader or other.
*/

const HIDE_TIMEOUT_SECOND = 4;


class TLog
{
    constructor()
    {
        this.CountJob = 0;
    }


    static Create()
    {
        return new TLog();
    }



    GetMessages()
    {
        var Result = document.getElementById( 'clLogElement' );
        if( !Result )
        {
            /* Create board for log */
            Result                = document.createElement( 'div' );
            Result.id             = 'clLogElement';
            Result.className      = 'MenuPopup Log';
            Result.style.display  = 'none';
            Result.onclick        = () => this.Hide();
            document.body.append( Result );
        }
        return Result;
    }



    GetIndicator()
    {
        var Result = document.getElementById( 'clIndicator' );
        if( !Result )
        {
            /* Create job indicator */
            Result                 = document.createElement( 'div' );
            Result.id              = 'clIndicator';
            Result.className       = 'PostIndicator';
            Result.style.display   = 'none';
            Result.style.opacity   = '0';
            document.body.append( Result );
        }
        return Result;
    }



    /*
        Show log
    */
    Show()
    {
        this.GetMessages().Show();
        return this;
    }



    /*
        Hide log
    */
    Hide()
    {
        this.GetMessages().Hide();
        return this;
    }



    /*
        Begin job is named AMassage:string
    */
    Beg( AMessage, AShow )
    {
        this.Msg( 'Beg', AMessage, AShow );
        return this;
    }



    /*
        End job AMessage:string
    */
    End( AMessage, AShow )
    {
        this.Msg('End', AMessage, AShow);
        return this;
    }



    /*
        Debug message AMessage:string
    */
    Deb( AMessage, AShow )
    {
        this.Msg('Deb', AMessage, AShow);
        return this;
    }



    /*
        Information message AMessage:string
    */
    Inf( AMessage, AShow )
    {
        this.Msg( 'Inf', AMessage, AShow );
        return this;
    }



    /*
        Warning message AMessage:string
    */
    War( AMessage, AShow )
    {
        this.Msg('War', AMessage, AShow);
        console.warn(AMessage);
    }



    /*
        вывод сообщения об ошибке
    */
    Err( AMessage, AShow )
    {
        this.Msg( 'Err', AMessage, AShow );
        console.error( AMessage );
    }



    /*
        Write to log
        AClass: type of message Inf, War, Deb, Err, Beg, End.
        AMessage: string - текст сообщения
        AShow - true показать окно лога, false - не показывать окно лога.
    */
    Msg( AClass, AMessage, AShow )
    {
        if( AMessage )
        {
            var Messages = this.GetMessages();
            var lLine=document.createElement( 'div' );
            lLine.className='LogLine Log'+AClass;
            lLine.innerHTML='<div class="LogMessage">'+AMessage+'</div>'+'<div class="LogTime"></class>';
            Messages.insertBefore( lLine, Messages.firstChild );
            if(AShow) this.Show();

            /* start timer for hiding message */
            lLine.Timer = setTimeout
            (
                () =>
                {
                    lLine.IntervalCount = 0;
                    /*запуск таймера гасящего сообщение*/
                    lLine.Timer = setInterval
                    (
                        () =>
                        {
                            lLine.IntervalCount ++;
                            lLine.style.opacity = 1 - lLine.IntervalCount / 10;
                            if( lLine.IntervalCount == 10 )
                            {
                                clearInterval( lLine.Timer );
                                lLine.Timer = null;
                                lLine.className = '';
                                lLine.parentNode.removeChild(lLine);
                                if ( Messages.childNodes.length == 0 ) this.Hide();
                            }
                        },
                        10
                    )
                },
                1000 * HIDE_TIMEOUT_SECOND
            );
        }
    }



    /*
        Begin job. Start new job and enable indicator.
    */
    JobBegin()
    {
        var Indicator = this.GetIndicator();
        if( document.body )
        {
            this.CountJob++;
            if( this.CountJob == 1 && Indicator.Timer == null )
            {
                Indicator.Timer = setTimeout
                (
                    () => 
                    {
                        Indicator.style.display = null;
                        Indicator.style.opacity = '1';
                        Indicator.Timer = null;
                    },
                    500
                );
            }
            Indicator.innerHTML = this.CountJob;
        }
        return this;
    }



    /*
        Job end. End currentn job and dec indicator.
    */
    JobEnd()
    {
        var Indicator = this.GetIndicator();

        this.CountJob--;
        Indicator.innerHTML = this.CountJob;

        if (this.CountJob < 1)
        {
            /* Hide indicator*/
            Indicator.style.display = 'none';
            Indicator.style.opacity = '0';
            this.CountJob = 0;
            /* Clear timer if it was set */
            if( Indicator.Timer )
            {
                clearTimeout( Indicator.Timer );
                Indicator.Timer = null;
            }
        }
        return this;
    }
}
