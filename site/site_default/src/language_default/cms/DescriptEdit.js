class TDescriptEdit
{
    static Popup( AApplication, ADescript, AParams )
    {
        /* Определяем ID */
        if (AParams && AParams.ID) ADescript.ID=AParams.ID;

        /* Создаем окно */
        if (! AApplication.WinControl) AApplication.WinControl = new clWinControl();
        ADescript.Window = AApplication.WinControl.WindowCreate();

        /* Знакомим окно с дескриптом */
        ADescript.Window.Descript = ADescript;
        ADescript.Window.SetCaption( ADescript.ID );
        ADescript.Window.SetToolbar(decodeURIComponent('<cl content="DescriptEditToolbar.html" pars="true" convert="URI"/>'));
        ADescript.Window.Icon.SetParams({Caption: ADescript.ID, Group:'Form', GUIDImage:''});

        /*
            Описание формы обновления дескрита
            заменяет весь контент на форме
        */
        ADescript.Window.Refresh = ( AEvent ) =>
        {
            /* Спрятали кнопки управления дескриптом */
            ADescript.Window.ChildByID( 'BtnDelete',        Btn => Btn.Hide() );
            ADescript.Window.ChildByID( 'BtnContent',       Btn => Btn.Hide() );
            ADescript.Window.ChildByID( 'BtnRenameID',      Btn => Btn.Hide() );
            ADescript.Window.ChildByID( 'BtnDescriptClose', Btn => Btn.Hide() );
            ADescript.Window.ChildByID( 'BtnDescriptOpen',  Btn => Btn.Hide() );
            ADescript.Window.ChildByID( 'BtnDescriptInfo',  Btn => Btn.Hide() );

            if( ADescript.ID != null )
            {
                /*Пытаемся загрузить запись*/
                ADescript.Load
                ({
                    ID:             ADescript.ID,
                    FormTemplate:   true,
                    OnAfterLoad:    function(p)
                    {
                        if( AApplication.PostResult( p ))
                        {
                            /* Вывод формы */
                            var FormContent = p.Result.Outcome.FormContent;

                            /* вывод контента в форму */
                            if ( ! ADescript.Window.FormSet || ( AEvent && AEvent.shiftKey ) )
                            {
                                FormContent = ADescript.Window.SetContent( FormContent );
                                ADescript.Window.FormSet = true;
                            }

                            /* Buttons show */
                            ADescript.Window.ChildByID( 'BtnDelete',        Btn => Btn.Show() );
                            ADescript.Window.ChildByID( 'BtnContent',       Btn => Btn.Show() );
                            ADescript.Window.ChildByID( 'BtnRenameID',      Btn => Btn.Show() );
                            ADescript.Window.ChildByID( 'BtnDescriptInfo',  Btn => Btn.Show() );
                            ADescript.Window.ChildByID( 'BtnDescriptClose', Btn => Btn.SetVisible( ADescript.GetOpened() ));
                            ADescript.Window.ChildByID( 'BtnDescriptOpen',  Btn => Btn.SetVisible( ! ADescript.GetOpened() ));

                            /* заполняем элементы формы */
                            clValuesFromObject( p.Result.Params, ADescript.Window.Control );
                            clValuesFromObject( p.Result.Outcome, ADescript.Window.Control );

                            /* Показать скрыть системные параметры */
                            var BtnSystem = ADescript.Window.ChildByID( 'BtnSystemParams' );
                            if ( BtnSystem )
                            {
                                BtnSystem.onclick = function()
                                {
                                    var WndSystem = ADescript.Window.ChildByID( 'WndSystemParams' );
                                    if (WndSystem)
                                    {
                                        if ( BtnSystem.classList.contains('ImagePlus') )
                                        {
                                            WndSystem.Show();
                                            BtnSystem.classList.remove('ImagePlus');
                                            BtnSystem.classList.add('ImageMinus');

                                        }
                                        else
                                        {
                                            WndSystem.Hide();
                                            BtnSystem.classList.remove('ImageMinus');
                                            BtnSystem.classList.add('ImagePlus');
                                        }
                                    }
                                }
                            }

                            /* Восстановление параметров окна */
                            ADescript.Window.Icon.SetParams
                            ({
                                Caption: p.Result.Outcome.Caption,
                                Group:'Form',
                                GUIDImage: p.Result.Outcome.IDImage
                            });

                            /* Вывод попапов */
                            AApplication.SelectActivate( ADescript.Window.Control );

                            /* Активация дататайм элементов */
                            AApplication.ScriptWait
                            (
                                ['clMomentElement.js'],
                                () => AApplication.MomentActivate( ADescript.Window.Control )
                            );

                            /* Загрузка инициализационного скрипта при его наличии */
                            if ( ADescript.Data.Outcome.FormScript != '')
                            {
                                clScriptLoad({ ID: ADescript.Data.Outcome.IDType, Code: ADescript.Data.Outcome.FormScript });
                                var FunctionName = 'cl' + ADescript.Data.Outcome.IDType + 'FormInit';
                                if (  window[ FunctionName ] && typeof( window[ FunctionName ] == 'function')) 
                                {
                                    window[ FunctionName ]( ADescript );
                                }
                            }

                        }
                    }
                });
            }
            else
            {
                /* ID для загрузки не указан */
                /* устанавливаем значения параметров для тулбара */
                ADescript.Window.Icon.SetParams
                ({
                    Caption:    'New descript',
                    Group:      'Form',
                    GUIDImage:  ''
                });

                /* Грузим форму добавления дескрипта */
                var FormContent = decodeURIComponent('<cl content="DescriptAddForm.html" pars="true" convert="URI"/>');
                ADescript.Window.SetContent( FormContent );

                /* Подготовка параметров */
                if ( ! AParams.IDType ) AParams.IDType = '';
                if ( ! AParams.IDParent ) AParams.IDParent = 'home';
                if ( ! AParams.Caption ) AParams.Caption = '';

                /* Заполнение полей формы из параметров */
                clValuesFromObject( AParams, ADescript.Window.Control );

                /*Активация попапов*/
                AApplication.SelectActivate( ADescript.Window.Control );

                var Parent = new TDescript();
                Parent.ID = AParams.IDParent;
                Parent.Load
                ({
                    OnAfterLoad:    function( ParentResult )
                    {
                        if ( ParentResult.Result.Outcome.IDTypeChild )
                        {
                            ADescript.Window.ChildByID( 'IDType' ).Refresh( ParentResult.Result.Outcome.IDTypeChild );
                            ADescript.Window.Icon.SetParams
                            ({
                                Caption:    'New descript in ' + ParentResult.Result.Outcome.Caption,
                                Group:      'Form',
                                GUIDImage:  ParentResult.Result.Outcome.IDImagePreview
                            });
                        }

                    }
                });
            }
        };



        /*
          Открыть дескрипт
        */
        ADescript.Window.DescriptOpen = function()
        {
            ADescript.Open({ OnAfterOpen: () => ADescript.Window.Refresh() });
        };



        /*
          Открыть дескрипт
        */
        ADescript.Window.DescriptClose = function() 
        {
            ADescript.Close
            ({
                OnAfterClose:function()
                {
                    ADescript.Window.Refresh();
                }
            });
        };



        /*
          Процедура сохранения формы
        */
        ADescript.Window.Save = function() 
        {
            var Param={};
            if (ADescript.ID!=null) Param.Call = 'Descript.Update'; /*ID существует значит Update*/
            else Param.Call = 'Descript.Create'; /*ID не существует значит Create*/
            Param.Pars = true;
            Param.Form = ADescript.Window.ChildByID('Form');

            /*Процедура после сохранения*/
            Param.OnAfterLoad = function(p)
            {
                if( AApplication.PostResult( p ))
                {
                    ADescript.ID = p.Result.Outcome.ID;
                    ADescript.Window.Refresh();
                }
            };
            AApplication.Post(Param);
        };



        /*
            Функция удаления дескрипта
        */
        ADescript.Window.Delete = function()
        {
            ADescript.Delete
            (
                {
                    OnAfterDelete:function(){ADescript.Window.Close()}
                }
            );
        };



        /* Дейставия на кнопки */
        ADescript.Window.ChildByID('BtnRefresh').onclick = function( AEvent ){ ADescript.Window.Refresh( AEvent ) };
        ADescript.Window.ChildByID('BtnSave').onclick    = ADescript.Window.Save;
        ADescript.Window.ChildByID('BtnDelete').onclick  = ADescript.Window.Delete;
        ADescript.Window.ChildByID('BtnDescriptOpen').onclick    = ADescript.Window.DescriptOpen;
        ADescript.Window.ChildByID('BtnDescriptClose').onclick   = ADescript.Window.DescriptClose;
        ADescript.Window.ChildByID('BtnDescriptInfo').onclick   = function(){ ADescript.HistoryPopup({}) };

        ADescript.Window.ChildByID('BtnContent').onclick = () =>
        {
            ADescript.ContentEdit
            ({
                IDLang: AParams && AParams.IDLang ? AParams.IDLang : null,
                OnAfterSave: () => ADescript.Window.Refresh()
            });
        };

        ADescript.Window.ChildByID('BtnRenameID').onclick = function()
        {
            ADescript.RenameID({OnSuccess:function(){ADescript.Window.Refresh()}});
        };

        /*Обновление дескрипта*/
        ADescript.Window.Refresh();
    }
}