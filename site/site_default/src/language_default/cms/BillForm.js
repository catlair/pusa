function clBillFormInit( ADescript )
{

    /* Amount refresh for this form */
    ADescript.AmountRefresh = function()
    {
        var that = this;
        ADescript.AmountLoad
        ({
            RecordContainer:               that.Window.ChildByID( 'BillAmount' ),
            TotalAmountChargeContainer:    that.Window.ChildByID( 'TotalAmountCharge' ),
            TotalAmountChargeTaxContainer: that.Window.ChildByID( 'TotalAmountChargeTax' ),
            TotalAmountPayContainer:       that.Window.ChildByID( 'TotalAmountPay' )

        });
        return this;
    }




    ADescript.FormPrepare = function()
    {
        var that = this;
        /* Acton on refresh button */
        var BtnAmountRefresh = that.Window.ChildByID('BtnAmountRefresh');
        if (BtnAmountRefresh) BtnAmountRefresh.onclick = function()
        {
            that.AmountRefresh();
        }



        /* Action on pdf */
        var BtnPrint = that.Window.ChildByID( 'BtnPrint' );
        if ( BtnPrint ) BtnPrint.onclick = function()
        {
            ADescript.BillPDF();
        }



        /* Action on Pay */
        var BtnPay = that.Window.ChildByID( 'BtnPay' );
        if ( BtnPay ) BtnPay.onclick = function()
        {
            ADescript.BillPay({});
        }




        /* Action on copy provider params */
        var BtnFillParams = that.Window.ChildByID( 'BtnFillParams' );
        if ( BtnFillParams ) BtnFillParams.onclick = function()
        {
            that.BillFillParams
            ({
                OnAfterCopyRequisites: function()
                {
                    that.Window.Refresh();
                }
            });
        }




        /* Action on recharge */
        var BtnRecharge = that.Window.ChildByID('BtnRecharge');
        if ( BtnRecharge ) BtnRecharge.onclick = function()
        {
           that.BillRecharge
           ({
               OnAfterRecharge: function()
               {
                   that.Window.Refresh();
               }
           });
        }
        return this;
    }



    cl.ScriptWait
    (
        [ 'clBill.js' ], 
        function()
        {
            ADescript.FormPrepare().AmountRefresh();
        }
    );
}




