function clAccountFormInit( ADescript )
{
    cl.ScriptWait
    (
        [ 'clAccount.js' ], () => 
        {
            ADescript.Window.Control.ChildByID( 'ChangePassword', Btn => Btn.onclick = () => ADescript.PasswordAdminPopup() );
        }
    );
}
