const RESULT_OK = 'Ok';
const RESULT_UNKNOWN = 'Unknown';


/*
    
*/
class TResult
{
    constructor()
    {
        this.Code = RESULT_OK;
        this.Message = '';
    }



    /* Check reault */
    IsOk()
    {
        return this.Code == RESULT_OK;
    }



    /* Set OK */
    SetOk()
    {
        this.Code = RESULT_OK;
        return this;
    }



    SetCode( ACode )
    {
        this.Code = ACode;
        return this;
    }


    
    SetResult(ACode, AMessage)
    {
        this.Code = ACode;
        this.Message = AMessage;
        return this;
    } 
}