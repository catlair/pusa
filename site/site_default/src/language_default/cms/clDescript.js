/*
    Catlair JS
    Управленеи объектами
    still@itserv.ru
*/



/*
    Объект дескрипт
    Предоставляет функционал для работы дескриптами
*/
class TDescript extends TResult 
{
    constructor( AApplication )
    {
        super();
        this.Application = AApplication ? AApplication : cl;
        this.ID = null;
    }



    static Create( AApplication )
    {
        return new TDescript( AApplication );
    }



    SetID( AValue )
    {
        this.ID = AValue;
        return this;
    }



    /*
        Получение параметра из данных Post
    */
    GetPost( AParam, ADefault )
    {
        if (this.Data && this.Data.Detale && this.Data.Detale.Post) return this.Data.Detale.Post[AParam];
        else return ADefault;
    }



    /*
        Check the right for descript
    */
    CheckRight( AParams )
    {
        this.Application.Post
        ({
            Call: 'Descript.CheckRight',
            Income: { ID: this.ID, IDRight: AParams.IDRight },
            OnAfterLoad: Result => 
            {
                if( AParams.OnCheckRight ) AParams.OnCheckRight( Result.Result.Header.Code == 'Ok' );
                if( AParams.OnRightExists ) Aparams.OnRightExists();
                if( AParams.OnRightNotExists ) Aparams.OnRightNotExists();
            }
        });
    }

  


    History( AParams )
    {
        if ( !AParams.ID ) AParams.ID = this.ID;
        var that = this;
        cl.Post
        ({
            Call:        'Descript.HistoryRead',
            Income:      AParams,            
            OnAfterLoad: p =>
            {
                if( cl.PostResult( p ) && AParams.OnHistoryLoad )
                {
                   AParams.OnHistoryLoad( p.Result.Outcome );
                }
            }
        });
        return this;
    }



    /*
    */
    HistoryPopup( AParams )
    {
        AParams.OnHistoryLoad = function(p)
        {
            var Content = decodeURIComponent('<cl content="DescriptHistoryForm.html" pars="true" convert="URI"/>');
            Content = clContentFromObject(p, Content);
            var Popup = cl.Popup({ ID:'FormSpreadBind', TypeContent:tcText, Content: Content });
        };

        this.History( AParams );
        return this;
    }



    Pathes( AParams )
    {
        if ( !AParams.ID ) AParams.ID = this.ID;
        var that = this;
        cl.Post
        ({
            Call:        'Descript.GetPathes',
            Income:      AParams,            
            OnAfterLoad: function( p )
            {
                if ( cl.PostResult(p) && AParams.OnPathesLoad )
                {
                   AParams.OnPathesLoad( p.Result.Outcome );
                }
            }
        });
        return this;
    }



    /*
    */
    PathesPopup( AParams )
    {
        AParams.OnPathesLoad = function( p )
        {
            var Content = decodeURIComponent('<cl content="DescriptPathForm.html" pars="true" convert="URI"/>');
            var Popup = cl.Popup({ ID:'FormSpreadBind', TypeContent:tcText, Content: Content });
            p.Pathes.forEach
            (
                function( AElement )
                {
                    var NewLine = document.createElement('li');
                    NewLine.innerHTML = AElement;
                    Popup.ChildByID( 'PathesList' ).append( NewLine );
                }
            ); 
        };

        this.Pathes( AParams );
        return this;
    }




    /*
       Task list for descript
    */
    TaskPopup( AParams )
    {
        var that = this;

        cl.StyleLoad({ ID:'TaskFormPopup.css' });
        var ContentForm     = decodeURIComponent( '<cl content="TaskFormPopup.html" pars="true" convert="URI"/>' );
        var ContentRecord   = decodeURIComponent( '<cl content="TaskRecordPopup.html" pars="true" convert="URI"/>' );
        var Popup           = cl.Popup({ TypeContent:tcText, Content:ContentForm });
        var RecordConteiner = Popup.ChildByID( 'TaskList' );

        Popup.Refresh = function()
        {
            var ID = AParams.ID ? AParams.ID : that.ID;
            cl.Post
            ({
                Call: 'Descript.Tasks',
                Income: { ID: ID },
                OnAfterLoad: Result =>
                {
                    if( cl.PostResult( Result ))
                    {
                        RecordConteiner.SetContent( '' );
                        Result.Result.Data.Records.Dataset.forEach
                        (
                            ARecord =>
                            {
                                var Record       = document.createElement( 'div' );
                                ARecord.DTActualNow = clMomentDeltaToStr( ARecord.DTActualNow );
                                ARecord.DTCompleteLength = clMomentDeltaToStr( ARecord.DTCompleteLength );
                                ARecord.DTBeginNow = clMomentDeltaToStr( ARecord.DTBeginNow );
                                Record.innerHTML = clContentClear( clContentFromObject( ARecord, ContentRecord ));
                                Record.className = 'Record';
                                RecordConteiner.append( Record );

                                Record.ChildsByClass
                                (
                                    'BtnControl',
                                     Btn => Btn.onclick = () => cl.Confirm
                                     ({
                                        Buttons: [ BTN_OK ],
                                        OnOk: () =>
                                        {   
                                            cl.Post
                                            ({
                                                Call: 'Descript.TaskControl',
                                                Income: { IDRecord: ARecord.IDRecord, Operation:Btn.id },
                                                OnAfterLoad: Result =>
                                                {
                                                    if( cl.PostResult( Result )) Popup.Refresh();
                                                }  
                                            });
                                        }
                                    })
                                );                            
                            }

                        );
                    }
                }
            });
        };

        Popup.ChildsByClass( 'BtnRefresh', Element => Element.onclick = () => Popup.Refresh() );

        Popup.Refresh();   


        return this;
    }



    /*
        Форма распространения прав по пути
        AParams.IDBindConductor - идентификатор связи проводника
        AParams.IDBindSpread - идентификатор связи для распространения
    */
    BindSpreadForm( AParams )
    {
        var that = this;

        /* Create form and popup */
        var Content = decodeURIComponent( '<cl content="FormSpreadBind.html" pars="true" convert="URI"/>' );
        var Popup = cl.Popup({ ID:'FormSpreadBind', TypeContent:tcText, Content:Content });

        /* Set default values */
        if ( ! AParams.ID )              AParams.ID              = this.ID;
        if ( ! AParams.IDBindConductor ) AParams.IDBindConductor = 'bind_right';
        if ( ! AParams.IDBindSpread )    AParams.IDBindSpread    = '';
        if ( ! AParams.Operation )       AParams.Operation       = 'Insert';

        /* Set values for form */
        Popup.ChildByID( 'ID' ).value              = AParams.ID;
        Popup.ChildByID( 'IDBindConductor' ).value = AParams.IDBindConductor;
        Popup.ChildByID( 'IDBindSpread' ).value    = AParams.IDBindSpread;
        Popup.ChildByID( 'Operation' ).value       = AParams.Operation;

        /* Init popup */
        cl.SelectActivate( Popup );

        /* Action on ok */
        Popup.ChildByID( 'BtnOk' ).onclick = function( AEvent )
        {
            that.BindSpread
            ({
                Call: 'Descript.BindSpread',
                Form: this.form,
                OnAfterSpread:function(p)
                {
                    if ( AParams && AParam.OnSucess ) AParams.OnSucess( p );
                    Popup.Close();
                }
            });
        };

        /* Action on cancel */
        Popup.ChildByID( 'BtnCancel' ).onclick = function()
        {
            Popup.Close();
        };
    };



    /*
        Распространение или удаление связей по пути
        AParams.Income.ID - идентификатор узла для начала
        AParams.Income.IDBindConductor - идентификатор связи проводника
        AParams.Income.IDBindSpread - идентификатор связи для распространения
        AParams.Income.Operation - тип операции 'Insert' or 'Delete'
    */
    BindSpread( AParams )
    {
        var that = this;
        /*Процедура удаления*/
        cl.Post
        ({
            Pars:        true,
            Call:        'Descript.BindSpread',
            Form:        AParams.Form,
            OnAfterLoad: function(p)
            {
                if (cl.PostResult(p))
                {
                    if (AParams.OnAfterSpread) AParams.OnAfterSpread(AParams);
                    if (that.OnAfterSpread) that.OnAfterSpread(that);
                }
            }
        });
    };



    /*
        Загрузка дескрипта по идентификатору
        AParams.ID - идентификатор дескрипта
        AParams.Form:boolean - признак возвращает ли Load форму для редактирования данного дескрипта
        AParams.Content - размер возвращаемого контента: none-нет; all-весь;
    */
    Load( AParams )
    {
        var that = this;
        if ( AParams.Income ) var Income = AParams.Income; 
        else var Income = {};

        /* Copy params from AParams to Income */
        if (AParams && AParams.ID) Income.ID=AParams.ID;
        else Income.ID = this.ID;

        if ( AParams && AParams.IDSiteDescript )Income.IDSite         = AParams.IDSite;
        if ( AParams && AParams.IDSiteRequest ) Income.IDSiteRequest  = AParams.IDSiteRequest;
        if ( AParams && AParams.FormTemplate )  Income.FormTemplate   = AParams.FormTemplate;
        if ( AParams && AParams.ContentSize )   Income.ContentSize    = AParams.ContentSize;
        if ( AParams && AParams.History )       Income.History        = AParams.History;

        /*Исполняем запрос*/
        cl.Post
        ({
            Income:         Income,
            Call:           'Descript.Load',
            OnAfterLoad:    Result =>
            {
                if( cl.PostResult( Result ))
                {
                    Result.Descript = that;
                    that.ID = Income.ID;
                    that.Data = Result.Result;
                    that.LoadParams = AParams;
                    if( AParams && AParams.OnAfterLoad ) AParams.OnAfterLoad( Result );
                }
            }
        });
    }



    /*
        Вызов формы опций для дескрипта
    */
    Option( AParams )
    {
        this.Application.ScriptWait( [ 'DescriptMenu.js' ], () =>  TDescriptMenu.Popup( this.Application, this, AParams ) );
        return this;
    }



    /*
        Вызов формы редактора дескрипта
        AParam.ID
    */
    Edit( AParams )
    {
        this.Application.ScriptWait( [ 'DescriptEdit.js' ], () =>  TDescriptEdit.Popup( this.Application, this, AParams ) );
        return this;
    }



    /*
        Вызов формы редактора контента дескрипта
    */
    ContentEdit( AParams )
    {
        this.Application.ScriptWait( [ 'DescriptContentEditor.js' ], () =>  TDescriptContentEditor.Open( this, AParams ) );
        return this;
    }



    ParamsToChild( AParams )
    {
        this.Application.ScriptWait( [ 'DescriptParamsToChild.js' ], () =>  TDescriptParamsToChild.Do( this.Application, this, AParams ) );
        return this;
    }



    TemplateFields()
    {
        this.Application.ScriptWait([ 'DescriptTemplateFields.js' ], () => TDescriptTemplateFields.Do( this.Application, this ));
        return this;
    }



    ParamsEditor( AParams )
    {
        this.Application.ScriptWait( [ 'DescriptParams.js' ], () =>  TDescriptParams.Do( this.Application, this ));
        return this;
    }



    EnabledHistory()
    {
        this.Application.ScriptWait( [ 'DescriptEnabledHistory.js' ], () => TEnabledHistory.Popup( this.Application, this ));
        return this;
    }



    /*
        Вызов формы опций для дескрипта
    */
    Recharge( AParams )
    {
        this.Application.ScriptWait( [ 'DescriptRecharge.js' ], () => TDescriptRecharge.Popup( this, AParams ));
        return this;
    }



    /*
        Вызов формы опций для дескрипта
    */
    RechargeInit( AParams )
    {
        this.Application.ScriptWait( [ 'DescriptRecharge.js' ], () => TDescriptRecharge.InitPopup( this, AParams ));
        return this;
    }




    /*
        Форма управления правами полей, элементов, параметров
        AParams.IDObject 
        AParams.IDDescript 
    */
    FieldsRightForm( AParams )
    {
        this.Application.ScriptWait( [ 'DescriptFieldsRight.js' ], () =>  TDescriptFieldsRight.Popup( this, AParams ) );
        return this;
    };



    /*
        Вызов списка детей
        AParam.ID
    */
    ChildPopup( AParams )
    {
        this.Application.ScriptWait( [ 'DescriptChildForm.js' ], () =>  TDescriptChildForm.Popup( this, AParams ) );
        return this;
    }



    /*
        Вызов списка детей
        AParam.ID
    */
    ParentPopup( AParams )
    {
        this.Application.ScriptWait( [ 'DescriptParentForm.js' ], () =>  TDescriptParentForm.Popup( this, AParams ) );
        return this;
    }



    /*
        Extra include timeout in schedule
        AParam.ID
        AParam.Timeout
    */
    ExtraScheduleInclude( AParams )
    {
        this.Application.ScriptWait( [ 'DescriptExtraSchedule.js' ], () =>  TDescriptExtraSchedule.ExtraInclude( this, AParams ) );
        return this;
    }



    /*
        Extra exclude schedule in schedule
        AParam.ID
        AParam.Timeout
    */
    ExtraScheduleExclude( AParams )
    {
        this.Application.ScriptWait( [ 'DescriptExtraSchedule.js' ], () =>  TDescriptExtraSchedule.ExtraExclude( this, AParams ) );
        return this;
    }



    /*
        Create new descript in database
    */
    Add( AParams )
    {
        var that = this;
        cl.Post
        ({
            Call: 'Descript.Create',
            Income:
            {
                IDType: AParams.IDType,
                Caption: AParams.Caption,
            },
            OnAfterLoad: function( AResult )
            {
                if ( cl.PostResult( AResult ) )
                {
                    that.ID = AResult.Result.Outcome.ID;
                    if( AParams.OnSucess ) AParams.OnSucess( AResult );
                }
            }
        });
        return this;
    };



    /*
        Процедура сохранения формы дескрипта
    */
    Save( AParams )
    {
        var that = this;
        var Income = {};

        if ( this.ID != null )
        {
            /* ID существует значит Update */
            Income.ID = this.ID;
            var Call = 'Descript.Update';
        }
        else
        {
            /* ID не существует значит Create */
            Income.IDType = this.IDType ? this.IDType : AParams.IDType;
            var Call = 'Descript.Create';
        }

        cl.Post
        ({
            Income: Income,
            Call: Call,
            Form: AParams.Form,
            OnAfterLoad: function( AResult )
            {
                if ( cl.PostResult( AResult ) )
                {
                    that.ID = AResult.Result.Outcome.ID;
                    if( AParams.OnAfterSave ) AParams.OnAfterSave( AResult );
                }
            }
        });
    };



    /*
        Изменение идентификатора дескрипта
    */
    RenameID(AParams)
    {
        var that = this;
        var Content = decodeURIComponent('<cl content="DescriptRenameIDForm" pars="true" convert="URI"/>');
        var Popup = cl.Popup({TypeContent:tcText, Content:Content});

        Popup.ChildByID('ID').value=this.ID;
        Popup.ChildByID('BtnCancel').onclick=function(){Popup.Close();};
        Popup.ChildByID('BtnOk').onclick=function()
        {
            cl.Confirm
            ({
                OnClick: function()
                {
                    /*Процедура изменения идентификатора*/
                    cl.Post
                    ({
                        Form:           Popup.ChildByID('Form'),
                        TypeContent:    tcJSON,
                        Call:           'Descript.ChangeID',
                        OnAfterLoad:    function(p)
                        {
                            if (cl.PostResult(p))
                            {
                                that.ID=p.Result.Outcome.ID;
                                if (AParams && AParams.OnSuccess) AParams.OnSuccess(AParams);
                                Popup.Close();
                            }
                        }
                    });
                }
            });
        };
    }



    /*
        Попытка удаления дескрипта с подтверждением
    */
    Delete(AParams)
    {
        var that = this;
        cl.Confirm
        ({
            OnClick: function()
            {
                /*Процедура удаления*/
                cl.Post
                ({
                    Pars:true,
                    Call: 'Descript.Delete',
                    Income: {ID:that.ID},
                    OnAfterLoad:function(p)
                    {
                        if (cl.PostResult(p))
                        {
                            if (AParams.OnAfterDelete) AParams.OnAfterDelete(AParams);
                            if (that.OnAfterDelete) that.OnAfterDelete(that);
                        }
                    }
                });
            }
        });
    }



    /*
        Попытка индексации дескрипта
    */
    Index( AParams )
    {
        var that = this;
        /*Процедура удаления*/
        cl.Post
        ({
            Pars: true,
            Call:'Descript.Index',
            Income: { ID:that.ID },
            OnAfterLoad:function(p)
            {
                if (cl.PostResult(p))
                {
                    if (AParams.OnAfterIndex) AParams.OnAfterIndex(AParams);
                    if (that.OnAfterIndex) that.OnAfterIndex(that);
                }
            }
        });
    }



    /*
        Попытка индексации дескрипта
    */
    IndexSingle( AParams )
    {
        var that = this;
        /*Процедура удаления*/
        cl.Post
        ({
            Pars: true,
            Call: 'Descript.IndexSingle',
            Income: { ID:that.ID },
            OnAfterLoad:function(p)
            {
                if (cl.PostResult(p))
                {
                    if (AParams.OnAfterIndex) AParams.OnAfterIndex(AParams);
                    if (that.OnAfterIndex) that.OnAfterIndex(that);
                }
            }
        });
    }



    /*
        Раздача прав вниз
    */
    RightFromParent( AParams )
    {
        cl.Post
        ({
            Call:'Descript.RightFromParent',
            Income: { ID: this.ID },
            OnAfterLoad: ( p ) =>
            {
                if( cl.PostResult(p) )
                {
                    if( AParams.OnRightFromParent ) AParams.OnRightFromParent( AParams );
                }
            }
        });
    }



    /*
    */
    RightNorm( AParams )
    {
        cl.Post
        ({
            Call: 'Descript.RightNorm',
            Income: { ID: this.ID },
            OnAfterLoad: p =>
            {
                if( cl.PostResult( p ))
                {
                    if( AParams.OnRightNorm ) AParams.OnRightNorm( AParams );
                }
            }
        });
    }




    /*
        Раздача прав вниз
    */
    RightDown( AParams )
    {
        var that = this;
        cl.Post
        ({
            Call:'Descript.RightDown',
            Income: {ID:that.ID},
            OnAfterLoad:function(p)
            {
                if (cl.PostResult(p))
                {
                    if (AParams.OnAfterRightDown) AParams.OnAfterRightDown(AParams);
                    if (that.OnAfterRightDown) that.OnAfterRightDown(that);
                }
            }
        });
    }



    /**
        Попытка удаления записи с подтверждением
        AParams.IDFrom - откуда удаляем связь
        AParams.IDBind - тип связи
    */
    Remove(AParams)
    {
        var that = this;
        cl.Post
        ({
            Pars:true,
            Call: 'Descript.Remove',
            Income: {
                ID:that.ID,
                IDFrom: AParams.IDFrom,
                IDBind: AParams.IDBind
            },
            OnAfterLoad:function(p)
            {
                if (cl.PostResult(p))
                {
                    if (AParams.OnAfterRemove) AParams.OnAfterRemove(AParams);
                    if (that.OnAfterRemove) that.OnAfterRemove(that);
                }
            }
        });
    }



    /*
        Попытка удаления записи с подтверждением
        AParams.IDFrom - откуда удаляем связь
        AParams.IDBind - тип связи
    */
    Clear( AParams )
    {
        var that = this;
        cl.Post
        ({
            Pars:true,
            Call: 'Descript.Clear',
            Income: 
            {
                ID:that.ID,
                IDFrom: AParams.IDFrom,
                IDBind: AParams.IDBind
            },
            OnAfterLoad:function(p)
            {
                if (cl.PostResult(p))
                {
                    if ( AParams.OnAfterRemove ) AParams.OnAfterRemove( AParams );
                }
            }
        });
    }




    /*
        Перенос дескрипта из одной папки в другую
        AParams.IDFrom - откуда переносим дескрипт
        AParams.IDTo - куда переносим дескрипт
        AParams.IDBind - тип связи
    */
    Move(AParams)
    {
        var that = this;
        /*Исполняем запрос*/
        cl.Post
        ({

            Pars:true,
            Call: 'Descript.Move',
            Income: {
                ID: that.ID,
                IDFrom: AParams.IDFrom,
                IDTo: AParams.IDTo,
                IDBind: AParams.IDBind
            },
            OnAfterLoad:function(p)
            {
                if (cl.PostResult(p))
                {
                    /*Успешное выполнение*/
                    if (AParams.OnAfterMove) AParams.OnAfterMove(AParams);
                    if (that.OnAfterMove) that.OnAfterMove(that);
                }
            }
        });
    }



    /**
        Создание линка дескрипта в новом месте
        AParams.ID - кого копируем
        AParams.IDTo - куда копируем
        AParams.IDBind - тип связи
    */
    Link(AParams)
    {
        if ( ! AParams.ID ) AParams.ID = this.ID;
        var that = this;
        /*Исполняем запрос*/
        cl.Post
        ({
            Pars:true,
            Call: 'Descript.Link',
            Income: {
                ID:     AParams.ID,
                IDTo:   AParams.IDTo,
                IDBind: AParams.IDBind
            },
            OnAfterLoad:function(p)
            {
                if (cl.PostResult(p))
                {
                    /*Успешное выполнение*/
                    if (AParams.OnAfterLink) AParams.OnAfterLink(AParams);
                    if (that.OnAfterLink) that.OnAfterLink(that);
                }
            }
        });
    }



    /*
        Создание копии дескрипта в новом месте
        AParams.IDTo - куда копируем
        AParams.IDBind - тип связи с которой создается копия
    */
    Copy(AParams)
    {
        var that = this;
        /*Исполняем запрос*/
        cl.Post
        ({
            Pars:true,
            Call: 'Descript.Copy',
            Income: {
                ID: that.ID,
                IDTo: AParams.IDTo,
                IDBind: AParams.IDBind
            },
            OnAfterLoad:function(p)
            {
                if (cl.PostResult(p))
                {
                    /*Успешное выполнение*/
                    if (AParams.OnAfterCopy) AParams.OnAfterCopy(AParams);
                    if (that.OnAfterCopy) that.OnAfterCopy(that);
                }
            }
        });
    }



    /*
        Размешение дескрипта в новом месте
        AParams.ID - кого размещаем копируем
        AParams.IDTo - куда копируем
        AParams.IDBind - тип связи с которой создается копия
    */
    Place(AParams)
    {
        if ( ! AParams.ID ) AParams.ID = this.ID;

        /*Исполняем запрос*/
        cl.Post
        ({
            Pars:true,
            Call: 'Descript.Place',
            Income: 
            {
                ID:     AParams.ID,
                IDTo:   AParams.IDTo,
                IDBind: AParams.IDBind
            },
            OnAfterLoad: p =>
            {
                if( cl.PostResult( p ))
                {
                    if( AParams.OnAfterPlace ) AParams.OnAfterPlace( AParams );
                    if( this.OnAfterPlace ) this.OnAfterPlace( this );
                }
            }
        });
    }



    /*
        Import descripts
    */
    Import( AParams )
    {
        var that = this;
        /* Исполняем запрос */
        cl.Post
        ({
            Call: 'Descript.Import',
            Form: AParams.Form, 
            OnAfterLoad:function(p)
            {
                if (cl.PostResult(p))
                {
                    /*Успешное выполнение*/
                    if ( AParams.OnAfterImport ) AParams.OnAfterImport( AParams );
                }
            }
        });
    }




    /*
    */
    GetOpened()
    {
        return this.Data.Outcome.Opened == 'on';
    }



    /*
       
    */
    Open( AParams )
    {
        var that = this;
        /*Исполняем запрос*/
        cl.Post
        ({
            Call: 'Descript.Open',
            Income: { ID: that.ID },
            OnAfterLoad:function(p)
            {
                if (cl.PostResult(p))
                {
                    /*Успешное выполнение*/
                    if (AParams.OnAfterOpen) AParams.OnAfterOpen( AParams );
                }
            }
        });
    }



    /*
       
    */
    Close( AParams )
    {
        var that = this;
        /*Исполняем запрос*/
        cl.Post
        ({
            Call: 'Descript.Close',
            Income: { ID: that.ID },
            OnAfterLoad:function(p)
            {
                if (cl.PostResult(p))
                {
                    /*Успешное выполнение*/
                    if (AParams.OnAfterClose) AParams.OnAfterClose( AParams );
                }
            }
        });
    }




    /*
        Change param in Descript and Database
        AParams.Name - имя параметра
        AParams.Value - значение параметра
    */
    ChangeParam( AParams )
    {
        var that = this;
        var Income = 
        {
            ID: this.ID,
            Name: AParams.Name,
            Value: AParams.Value
        };

        /*Процедура удаления*/
        cl.Post
        ({
            Pars:        true,
            Call:        'Descript.SetParamSingle',
            Income:      Income,

            OnAfterLoad: function(p)
            {
                if (cl.PostResult(p))
                {
                    if (AParams.OnAfterChangeParam) AParams.OnAfterChangeParam( AParams );
                }
                else
                {
                    if (AParams.OnErrorChangeParam) AParams.OnErrorChangeParam( AParams );
                }
            }
        });
    }




    /*
    */
    PlaceByHolder( AParams )
    {
        var that = this;
        var Income = 
        {
            ID: this.ID
        };

        cl.Post
        ({
            Call:        'Descript.PlaceByHolder',
            Income:      Income,

            OnAfterLoad: function(p)
            {
                if (cl.PostResult( p ))
                {
                    if ( AParams.OnSucess ) AParams.OnSuccess( AParams );
                }
            }
        });
    }



    /*
        Запись контента
    */
    ContentWrite( AParams )
    {
        var that = this;
        var Income = 
        {
            ID:     this.ID,
            Content:  AParams.Content
        };
        if ( AParams.IDLang ) Income.IDLang = AParams.IDLang;

        /* Процедура отправки дескрипта на сервер */
        cl.Post
        ({
            Pars:        true,
            Call:        'Descript.ContentWrite',
            Income:      Income,
            OnAfterLoad: function(p)
            {
                if (cl.PostResult(p))
                {
                    if ( AParams.OnAfterContentWrite ) AParams.OnAfterContentWrite( AParams );
                }
                else
                {
                    if ( AParams.OnErrorContentWrite ) AParams.OnErrorContentWrite( AParams );
                }
            }
        });
    }



    Cast( AParams )
    {
        var that = this;
        this.IDType = AParams.IDType;
        clScriptWait
        (
            [ this.IDType + '.js' ],
            function()
            {
                try
                {
                    that.Class = eval( 'T' + that.IDType );
                } 
                catch( error ) 
                {
                    if( AParams.OnUncast ) AParams.OnUncast( AParams );
                    that.Class = null;
                };

                if ( that.Class != null )
                {
                    if( AParams.OnCast ) AParams.OnCast( AParams );
                }
            }
        );
        return this;
    }
}



/*
    Запрос Caption по ID
*/
TCatlair.prototype.CaptionByID = function(AParams)
{
    var that = this;
    if (AParams.ID && AParams.ID!=null)
    {
        AParams.Income = {ID:AParams.ID};
        AParams.Pars = true;
        AParams.Call = 'Descript.CaptionByID';
        AParams.OnAfterLoad = function( p )
        {
            p.Caption = p.Result.Outcome.Caption;
            if ( p.Element != null )
            {
                p.Element.innerHTML = p.Result.Outcome.Caption;
            }
            if ( p.OnAfterCaption) p.OnAfterCaption( p );
        };
        this.Post(AParams);
    }
};




/*
    Активация списков выбора в контейнере
    Необходимо выполнять после загрузки контента со списками выбора
*/
TCatlair.prototype.SelectActivate = function( AConteiner )
{
    var SelectList = AConteiner.ChildsByClass( 'SelectValue' );
    for (var i=0; i<SelectList.length; i++)
    {
        /* очередной элемент выбираем */
        var Select = SelectList[i];

        /* если элемент не инициализирован то инициализируем его */
        if ( ! Select.Caption )
        {
            Select.IDResetOnChange = true;

            /*Связываем элементы Caption Valueмежду собой*/
            Select.Caption = Select.ParentByClass('CustomInput').ChildByClass('SelectCaption');
            Select.Caption.Value = Select;

            Select.Button = Select.ParentByClass('CustomInput').ChildByClass('SelectButton');
            Select.Button.Select = Select;
            Select.Button.onclick = function()
            {
                this.Select.Popup();
            };



            Select.Set = function( AValue )
            {
                var Difference = this.value != AValue;

                if ( Difference && this.OnBeforeChange ) this.OnBeforeChange();
                this.value = AValue;
                if ( Difference && this.OnAfterChange ) this.OnAfterChange();

                return this; 
            };



            /*
                 Метод Refresh
            */
            Select.Refresh = function( AValue )
            {
                var that = this;
                if (AValue !== undefined) this.Set( AValue );

                if ( this.value === '' )
                {
                    that.Caption.classList.add( 'TextCorrect' );
                    that.Caption.classList.remove( 'TextError' );
                    that.Caption.value = '';
                }
                else
                {
                    /*Выполнение запроса Caption по ID*/
                    cl.CaptionByID
                    ({
                        ID:that.value,
                        OnAfterCaption:function(p)
                        {
                            if (p.Result.Header.Code == 'Ok')
                            {
                                /* Получено верное значение */
                                that.Caption.classList.add('TextCorrect');
                                that.Caption.classList.remove('TextError');
                                that.Caption.value = decodeURIComponent( p.Result.Outcome.Caption );
                                if ( Select.OnChange ) Select.OnChange();
                            }
                            else
                            {
                                /* Получено неверное значение */
                                that.Caption.classList.remove('TextCorrect');
                                that.Caption.classList.add('TextError');
                                try
                                {
                                    that.Caption.value = decodeURIComponent(p.Result.Outcome.ID);
                                }
                                catch
                                {
                                    that.Caption.value = '';
                                }
                            }
                        }
                    });
                }
            };



            /*
                Метод Popup
                Поиск и выбор дескрипта из списка
            */
            Select.Popup = function ()
            {
                var that = this;

                if( ! this.Popuping )
                {
                    this.Popuping = true;

                    cl.StyleLoad({ ID:'DescriptSelectForm.css' });

                    var p=clGetObjectLeftBottom(this.Caption); /*Позиция элемента*/

                    /*Построение URL для запроса поиска записей в справочниках*/
                    var Content = decodeURIComponent('<cl content="DescriptSelectForm.html" convert="uri"/>');
                    var Popup = cl.Popup
                    ({
                        TypeContent:tcText, 
                        Content:Content,  
                        Pos:p,
                        OnClose: () => clRequestsClose( 'RecordPopupSelectRequest' )
                    });

                    /* Приняли строку поиска */
                    Popup.FindString = Popup.ChildByID('String');
                    Popup.FindString.value=this.Caption.value;
                    Popup.FindString.focus();

                /*Поиск на кнопке BtnSearch*/
                Popup.Refresh=function()
                {
                    /* Создание перечня дескриптов */
                    if (!Popup.Descripts)
                    {
                        Popup.Descripts = new TDescripts();
                        Popup.Descripts.RecordContent = decodeURIComponent('<cl content="DescriptRecord.html" pars="true" convert="URI"/>');
                        Popup.Descripts.RecordConteiner = Popup.ChildByID('SearchResult');

                        /*Событие при загрузке записи и развешивание onlcick*/
                        Popup.Descripts.OnAfterLoadRecord = function(AConteiner)
                        {
                            AConteiner.onclick = function()
                            {
                                Popup.Close();
                                that.Set( AConteiner.Record.ID );
                                that.Refresh();
                            }
                       };
                    }

                    /* Chouse site siurce */
                    var SiteRequest = Popup.ChildByID( 'SiteRequest' );
                    var IDSiteRequest = '';
                    if ( SiteRequest && SiteRequest.checked ) IDSiteRequest = 'site_default';

                    SiteRequest.onchange = function()
                    {
                        Popup.Refresh();
                    };

                    /* Загрузка дескриптов */
                    Popup.Descripts.Load
                    ({
                        IDGroup:       'RecordPopupSelectRequest',
                        Find:          Popup.FindString.value,
                        IDType:        that.getAttribute('cltype'),
                        IDSiteRequest: IDSiteRequest,
                        RecordCount:   10,
                        RecordCurrent: 0
                    });
                };

                Popup.Add = function()
                {
                    if( Popup.FindString.value )
                    {
                        cl.Confirm
                        ({
                            ImageClass: 'ImagePlus',
                            OnClick: function()
                            {
                                var NewDescript = new TDescript();
                                NewDescript.Add
                                ({
                                    Caption: Popup.FindString.value,
                                    IDType: that.getAttribute('cltype'),
                                    OnSucess( AResult )
                                    {
                                        Popup.Close();
                                        that.Refresh( NewDescript.ID );
                                    }
                                });                            
                            }
                        });                   
                    }
                };

                    /*Клик на кнопку поиска*/
                    Popup.ChildByID( 'BtnSearch', Btn => Btn.onclick = () =>  Popup.Refresh() );

                    /* Закрытие окна */
                    Popup.ChildByID( 'BtnAdd', Btn => Btn.onclick = () => Popup.Add() );
  
                    /* Закрытие окна */
                    Popup.ChildByID( 'BtnCancel', Btn => Btn.onclick = () => Popup.Close() );

                    /* Выполняем поиск при открытии формы */
                    Popup.Refresh();
                }
                this.Popuping = false;
            };



            Select.Caption.onblur = function(AEvent)
            {
                 if
                 (
                     this.value != null && 
                     this.value.trim() != '' && 
                     ( this.Value.value == null || this.Value.value == '' )
                 )
                 {
                     this.Value.Popup();
                 }
            };



            /* обработчик клавиауры */
            Select.Caption.onkeydown = function(AEvent)
            {
                var kc = AEvent.keyCode || AEvent.which;
                if (kc==40) this.Value.Popup(this);
                if ((kc>=46)|| /*symbols from 0*/
                    (kc==0)|| /*Firefox bug with RU = 0*/
                    (kc==8)|| /*back*/
                    (kc==32)) /*space*/
                {
                    if ( this.Value.IDResetOnChange )
                    {
                        this.Value.Set( '' ); /*Сброс значения UID*/
                        this.classList.add( 'TextError' );
                        this.classList.remove( 'TextCorrect' );
                    }
                }
            };


            /* double click */
            Select.Caption.ondblclick = function( AEvent )
            {
                this.Value.Popup();
            };



            Select.SelectHide = function()
            {
                this.Hide();
                this.parentNode.Hide();
                this.Caption.Hide();
                this.Button.Hide();
            };



            Select.SelectDisable = function()
            {
                this.disabled = 'disabled';
                this.parentNode.classList.add( 'Disabled' );
                this.Caption.disabled = 'disabled';
                this.Button.disabled = 'disabled';
            };
        }

        Select.Refresh();
    }
};


