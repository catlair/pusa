class TDescriptParentForm
{
    static Popup( ADescript, AParams )
    {
        var Application = ADescript.Application;

        /* Create form and popup */
        var Popup = cl.Popup
        ({
            TypeContent: tcText,
            Content: decodeURIComponent( '<cl content="DescriptParentForm.html" pars="true" convert="URI"/>' )
        });

        /* Init popup */
        Application.SelectActivate( Popup );

        /* Set values for form */
        Popup.ChildByID( 'ID', Element => Element.Refresh( ADescript.ID ));
        Popup.ChildByID( 'IDBind', Element => Element.Refresh( clNotEmpty( AParams.IDBind )));



        Popup.Add = () =>
        {
             var Child = new TDescript();
             Child.Place
             ({
                 ID:         Popup.ChildByID( 'ID' ).value,
                 IDBind:     Popup.ChildByID( 'IDBind' ).value,
                 IDTo:       Popup.ChildByID( 'IDParent' ).value,
                 OnAfterPlace: () =>
                 {
                     if ( AParams.OnAfterPlace ) AParams.OnAfterPlace( AParams );
                     Popup.Refresh();
                 }
             });
        };



        Popup.Refresh = () =>
        {
            var List = Popup.ChildByID( 'List' );
            var RecordContent = decodeURIComponent( '<cl content="DescriptBindRecord.html" pars="true" convert="URI"/>' );
            Application.Post
            ({
                Call: 'Descript.ParentSelect',
                Form: Popup.ChildByID( 'Form' ),
                OnAfterLoad:function( AResult )
                {
                    if( Application.PostResult( AResult ))
                    {
                        List.SetContent( '' );
                        
                        /* Records loop */
                        AResult.Result.Data.Records.Dataset.forEach
                        ( 
                            Record => 
                            {
                                var RecordContainer = document.createElement( 'div' );
                                RecordContainer.className = 'Record';
                                RecordContainer.Record = Record;
                                RecordContainer.SetContent( clContentFromObject( Record, RecordContent ));
                                List.append( RecordContainer );

                                RecordContainer.ChildsByClass( 'BtnDel' ).forEach
                                (   
                                    BtnDel => BtnDel.onclick = () =>
                                    {
                                        BtnDel.Record = Record;
                                        Application.Confirm
                                        ({
                                            OnClick: function()
                                            {
                                                Popup.Delete
                                                ({
                                                    IDObject: BtnDel.Record.IDObject, 
                                                    Field: BtnDel.Record.Field
                                                });
                                            }
                                        });
                                    }
                                );

                                RecordContainer.ChildsByClass( 'Parent' ).forEach
                                (
                                    Element => Element.onclick = () =>
                                    {
                                        TDescript.Create().SetID( Element.parentNode.Record.IDParent ).Option
                                        ({
                                            OnAfterRemove: () => Popup.Refresh ()
                                        });
                                    }
                                );

                            }
                        );
                    }
                }
            });
        };



        /* Action on buttons */
        Popup.ChildByID( 'BtnRefresh', Btn => Btn.onclick = () => Popup.Refresh() ); 
        Popup.ChildByID( 'BtnCancel', Btn => Btn.onclick = () => Popup.Close() );
        Popup.ChildByID( 'BtnParentAdd', Btn => Btn.onclick = () => Popup.Add() );

        Popup.Refresh();
    }
}








