class TInterval
{
    constructor( ABegin, AEnd )
    {
        this.Set( ABegin, AEnd ).SetOperator( 1 );
    }



    /*
        Create and return new interval
    */
    static Create( ABegin, AEnd )
    {
        return new TInterval( ABegin, AEnd );
    }



    /*
        Set begin and end for interval
    */
    Set( ABegin, AEnd )
    {
        ABegin = ABegin ? ABegin : -Infinity;
        AEnd = AEnd ? AEnd : Infinity;

        this.Begin = ABegin;
        this.End = AEnd;

        return this;
    }



    /*
        Create and return copy of this interval
    */
    Copy()
    {
        return TInterval.Create
        (
            this.GetBegin(),
            this.GetEnd()
        ).SetOperator( this.GetOperator() );
    }



    /*
        Return the begin of interval
    */
    GetBegin()
    {
        return this.Begin;
    }



    /*
        Return the end of interval
    */
    GetEnd()
    {
        return this.End;
    }



    /*
        Set operator for interval
    */
    SetOperator( AValue )
    {
        this.Operator = AValue;
        return this;
    }



    /*
        Get and return the operator of interval
    */
    GetOperator()
    {
        return this.Operator;
    }



    /*
        Return the string fo interval
    */
    GetAsMomentString()
    {
        return clMomentToStr( this.Begin ) + ' > ' + clMomentToStr( this.End ) + ' [' + this.GetOperator() + ']';
    }



    FromTimeString( ADate, AString )
    {
        var Parts = AString.split( '>', 2 );
        var From = Parts.length > 0 ? Parts[ 0 ] : '';
        var To = Parts.length > 1 ? Parts[ 1 ] : '';
        this.Set( ADate + clStrTimeToMoment( From ), ADate + clStrTimeToMoment( To ));
        return this;
    }



    FromString( AString )
    {
        var Parts = AString.split( '>', 2 );
        var From = Parts.length > 0 ? Parts[ 0 ] : '';
        var To = Parts.length > 1 ? Parts[ 1 ] : '';
        this.Set( clStrToMoment( From ), clStrToMoment( To ));
        return this;
    }
}



class TIntervals extends Array
{
    static Create()
    {
        return new TIntervals();
    }



    Add( AInterval )
    {
        this.push( AInterval );
        return this;
    }



    Clear()
    {
        this.length = 0;
        return this;
    }



    CopyFrom( AIntervals )
    {
        this.Clear();

        var c = AIntervals.length;
        for( var i=0; i<c; i++ )
        {
            this.Add( AIntervals[i].Copy() );
        }
        return this;
    }



    SetOperator( AOperator )
    {
        var c = this.length;
        for( var i=0; i<c; i++ ) this[ i ].SetOperator( AOperator );
        return this;
    }



    /*
        Return Interval with begin or end nearest to AValue from right side

        1.
        -------b+++++e-------b+++*===>e------b+++++e----------> t
                                 ^    |
                            AValue    RESULT

        2.
        -------b+++++e-------b++++++++e--*==>b+++++e----------> t
                                         ^   |
                                    AValue   RESULT
    */
    GetMore( AValue )
    {
        var Result =
        {
            Balans:    0,          /* + Begins; -Ends; 0 */
            Value:     Infinity
        };

        var c = this.length;
        for( var i = 0; i<c; i++ )
        {
             /* Начало интервала */
             if( this[ i ].GetEnd() > AValue && this[ i ].GetEnd() == Result.Value )
             {
                 Result.Balans -= this[ i ].Operator;
             }

             /* Конец интервала */
             if( this[ i ].GetEnd() > AValue && this[ i ].GetBegin() == Result.Value )
             {
                 Result.Balans += this[ i ].Operator;
             }

             /*
                 Found end more left limit and less last more
                 -inf -----*---b============e------------- +inf
                 -inf -----*---b========================== +inf
                 -inf =====*================e------------- +inf
                 -inf =====*============================== +inf
             */
             if( this[ i ].GetEnd() > AValue && this[ i ].GetEnd() < Result.Value )
             {
                 Result.Balans = - this[ i ].Operator;
                 Result.Value  = this[ i ].GetEnd();
             }

             /* Found begin more left limit and less last more */
             if( this[ i ].GetBegin() > AValue && this[ i ].GetBegin() < Result.Value )
             {
                 Result.Balans = + this[ i ].Operator;
                 Result.Value  = this[ i ].GetBegin();
             }
        }

        return Result;
    }



    Operation()
    {
        var Result = TIntervals.Create();
        var Current = - Infinity;
        var Balans = 0;
        var NewBegin = null;
        var NewEnd = null;
        do
        {
            var More = this.GetMore( Current );
            if( More.Value != Infinity )
            {

                /* Начало отрезка переход от -0 к + */
                if( Balans <= 0 && Balans + More.Balans > 0 ) NewBegin = More.Value;

                /* Конец отрезка переход от + к -0 */
                if( Balans > 0 && Balans + More.Balans <= 0 ) NewEnd = More.Value;

                /* Create interval */
                if( NewBegin !== null && NewEnd !== null )
                {
                    Result.Add( TInterval.Create( NewBegin, NewEnd ));
                    NewBegin = null;
                    NewEnd = null;
                }

                Balans = Balans + More.Balans;
                Current = More.Value;
            }
        } while( More.Value != Infinity );

        return Result;
    }



    /*
        Return minus
        this       +=====-        +======-     +===-
                   .     .               .     .   .
                   .   +====-            .     .   .
                   .   . .               .     .   .
        AInterval  . -==============+    .     .   .
                   . . . .          .    .     .   .
        Result     *=* *=*          *====*     *===*
                0  1 0 1 0 -1     0 1    0     1   0
    */
    Exclude( AInterval )
    {
        return TIntervals.Create()
        .CopyFrom( this )
        .SetOperator( +1 )
        .Add( AInterval.SetOperator( -1 ))
        .Operation();
    }



    /*
        Return merge
        this       +=====-        +======-     +===-
                   .   +===-             .     .   .
                   .                     .     .   .
        AInterval  . +==============-    .     .   .
                   .                     .     .   .
        Result     *=====================*     *===*
                0  1 2 3 2 1      2 1    0     1   0  0
    */
    Merge( AInterval )
    {
        return TIntervals.Create()
        .CopyFrom( this )
        .SetOperator( +1 )
        .Add( AInterval.SetOperator( +1 ))
        .Operation();
    }



    /*
        Return difference
        this           -======+      -===+  -=====+   -===+
                       .   -=====+   .   .  .
                       .         .   .   .  .
        AInterval   +==========================-
                    .  .         .   .   .  .
        Result      *==*         *===*   *==*
                0   1  0  -1  0  1   0   1  0  1  0  -1   0  0
    */
    Difference( AInterval )
    {
        return TIntervals.Create()
        .CopyFrom( this )
        .SetOperator( -1 )
        .Add( AInterval.SetOperator( +1 ))
        .Operation();
    }



    LogAsMoment()
    {
        var c = this.length;
        for( var i = 0; i < c; i++ )
        {
             console.log( this[ i ].GetAsMomentString() );
        }
        return this;
    }



    FromMomentString( AString )
    {
        var Parts = AString.split( ';' );
        Parts.forEach( Part => this.Add( TInterval.Create().FromString( Part ) ) );
        return this;
    }
}
