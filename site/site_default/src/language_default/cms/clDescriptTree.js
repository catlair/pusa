/*
    Tree of objects
    Catlair JS
    still@itserv.ru
    15.03.2019
*/

class TDescriptTree
{
    constructor(ACatlair)
    {
        this.OnBeforeSelected = null;
        this.OnAfterSelected  = null;
        this.Tree             = null; /*Tree conteiner*/
        this.RecordCurrent    = 0;
        this.OnRecordClick    = null; /*клик на запись*/
        this.IDSelect         = null; /*Текущая выбранная запись*/
    }



    /*
        Очистка родительского контейнера
    */
    Clear()
    {
        if( this.Tree != null ) this.Tree.innerHTML = '';
        return this;
    }



    LoadRoot()
    {
        if ( this.Tree != null )
        {
            this.Clear();
            this.Load({ Parent:this.Tree });
        }
        return this;
    }



    /*
        Delete the conteiner AConteiner
    */
    ConteinerDelete( AConteiner )
    {
        var Parent = AConteiner.GetParent();
        Parent.Reload();
        return this;
    }



    /*
        выпадающее меню опций для контейнера AConteiner
    */
    ConteinerOptions( AConteiner )
    {
        var that = this;

        var Params =
        {
            IDLang: this.OnGetLang(),                     /* Set IDLang for params */
            IDBind: this.ConteinerGetIDBind(AConteiner),  /* Set current IDBind */
            OnAfterPlace: function( AParams )
            {
                /* Self Reload for replace operation */
                AConteiner.Reload();
            }
        };

        var Parent = AConteiner.GetParent();
        if ( Parent )
        {
            Params.IDFrom = Parent.Descript.ID; /* Set parent Descript */
            Params.IDBindParent = this.ConteinerGetIDBind(Parent); /* Set current IDBind */
            /* Set action on remove element */
            Params.OnAfterRemove = function( AParams )
            {
                /* Parent conteiner reload */
                Parent.Reload();
            }
        }

        AConteiner.Descript.Option( Params );
        return this;
    };



    /*
        Получение типа связи для текущего узла 
    */
    ConteinerGetIDBind( AConteiner )
    {
        var Result = 'bind_default';
        var ChildList = AConteiner.ChildByID('ChildList');
        if (ChildList && ChildList.IDBind)
        {
            Result = ChildList.IDBind;
        }
        return Result;
    }



    /*
        Начало загузки папок
        AParams.Parent.Record
    */
    Load( AParams )
    {
        this.DragDescriptSourceConteiner = null;
        var that = this;

        if ( ! AParams.Parent.Record )
        {
            var ID='root';
            var IDBind = 'bind_default';
            var IDParent = null;
            var IDSiteRequest = '';
        }
        else
        {
            var IDParent = AParams.Parent.Record.ID;
            var IDBind = AParams.Parent.IDBind;
            var IDSiteRequest = AParams.Parent.Record.IDSiteSource;
        }

        /* Create new descripts object */
        var Descripts = new TDescripts();
        this.Descripts = Descripts;
        Descripts.RecordConteiner = AParams.Parent;
        Descripts.RecordContent = this.ConteinerContent;

        /* Write Descripts in to conteiner for stop if conteiner will be collapsed */
        AParams.Parent.Descripts = Descripts;

        /* Присылается вновь созданный AConteiner и в нем есть запись AConteiner.Record */
        Descripts.OnAfterLoadRecord = function( AConteiner )
        {
            var Record = AConteiner.Record;
            var Descript = AConteiner.Descript;
            Descript.Conteiner = AConteiner;

            /* Bind type button */
            var BindType = AConteiner.ChildByID('BindType');

            /* Childs conteiner */
            var Childs = AConteiner.ChildByID('Childs');

            /* ChildList conteiner */
            var ChildList = AConteiner.ChildByID('ChildList');
            ChildList.Record = Record;

            Descript.OnAfterDelete = function(){that.ConteinerDelete(AConteiner)};

            /*Получение родительского контейнера*/
            AConteiner.GetParent = function()
            {
                return this.parentNode.ParentByClass('Record');
            };


            /* Click action on record */
            var Check = AConteiner.ChildsByClass( 'Check' ).forEach
            (
                Element => Element.onclick = function()
                {
                    AConteiner.Descripts.LoadStop();
                    if (that.IDSelect == Record.ID)
                    {
                        Element.checked = false;
                        that.IDSelect = null;
                    }
                    else
                    {
                        that.IDSelect = Record.ID;
                    }
                    if (that.OnRecordClick) that.OnRecordClick(AConteiner);
                }
            );

            /*Обработка select подсветка элементов с иного сайта*/
            AConteiner.ChildsByClass( 'Options' ).forEach
            (
                Element => Element.onclick = () => that.ConteinerOptions( AConteiner )
            );


            /*  */
            var Caption = AConteiner.ChildByID('Caption');
            Caption.ondragstart = function(AEvent)
            {
                /* this GLOBAL variable */
                that.DragDescriptSourceConteiner = AConteiner;
                return true;
            };


            Caption.ondragover = function(AEvent)
            {
                return false;
            };


            Caption.ondrop = function(AEvent)
            {
                var Content = decodeURIComponent('<cl content="DescriptOperation.html" pars="true" convert="URI"/>');
                var Win = cl.Popup({TypeContent:tcText, Content:Content});

                /* Получение контейнеров для операции */
                var ObjDrag         = that.DragDescriptSourceConteiner.ParentByClass('Record');
                var ObjSource       = ObjDrag.GetParent();
                var ObjDest         = AConteiner;

                /* Получение дескриптов для операции */
                var DescriptDrag               = null;
                var DescriptSource             = null;
                var DescriptDest               = null;
                if (ObjDrag)   DescriptDrag    = ObjDrag.Descript;
                if (ObjSource) DescriptSource  = ObjSource.Descript;
                if (ObjDest)   DescriptDest    = ObjDest.Descript;
              
                /* Действие перемещения из папки */
                Win.ChildByID('BtnMove').onclick=function()
                {
                    DescriptDrag.Move
                    ({
                        IDFrom:   DescriptSource.ID,
                        IDTo:     DescriptDest.ID,
                        IDBind:   that.ConteinerGetIDBind(ObjDest),
                        OnAfterMove:function()
                        {
                           Win.Close();
                           ObjSource.Reload();
                           ObjDest.Reload();
                        }
                    });
                };

                /* Действие создания ссылки */
                Win.ChildByID('BtnPlace').onclick=function()
                {
                    DescriptDrag.Place
                    ({
                        IDTo:     DescriptDest.ID,
                        IDBind:   that.ConteinerGetIDBind(ObjDest),
                        OnAfterPlace:function()
                        {
                           Win.Close();
                           ObjDest.Reload();
                        }
                    });
                };


                /* Действие копирования дескрипта */
                Win.ChildByID('BtnCopy').onclick=function()
                {
                    DescriptDrag.Copy
                    ({
                        IDTo:     DescriptDest.ID,
                        IDBind:   that.ConteinerGetIDBind(ObjDest),
                        OnAfterCopy:function()
                        {
                           Win.Close();
                           ObjDest.Reload();
                        }
                    });
                };

                /* Действие создания ссылки */
                Win.ChildByID('BtnLink').onclick=function()
                {
                    DescriptDrag.Link
                    ({
                        IDTo:     DescriptDest.ID,
                        IDBind:   that.ConteinerGetIDBind(ObjDest),
                        OnAfterLink:function()
                        {
                           Win.Close();
                           ObjDest.Reload();
                        }
                    });
                };
            };


            /* Button Bind type*/
            AConteiner.ChildByID('BindType').onclick = () =>
            {
                Descript.ChildPopup
                ({ 
                    OnBindClick: ( IDBind, Popup ) =>
                    {
                        AConteiner.SetIDBind( IDBind );
                        Popup.Close();
                    }
                });
            };



            /* Установка типа связи для текущего узла */
            AConteiner.Reload = function()
            {
                this.SetIDBind( that.ConteinerGetIDBind(this) );
            };



            /* Установка типа связи для текущего узла */
            AConteiner.SetIDBind = function(AIDBind)
            {
                ChildList.IDBind = AIDBind;
                var Bind = new TDescript();
                Bind.Load
                ({
                    ID: AIDBind,
                    IDSite: 'site_default',
                    OnAfterLoad:function(p)
                    {
                        var BindCaption = AConteiner.ChildByID( 'BindCaption' );
                        BindCaption.innerHTML = decodeURIComponent( Bind.Data.Outcome.Caption );
                        var Color=new clRGBA
                        (
                            Bind.Data.Outcome.ColorR,
                            Bind.Data.Outcome.ColorG,
                            Bind.Data.Outcome.ColorB,
                            Bind.Data.Outcome.ColorA
                        );
                        AConteiner.style.backgroundColor = Color.CanvasColor();
                        /* Когда тип связи загружен и усановлен, подгружаем детей в контейнер*/
                        that.Load({ Parent:ChildList });
                    }
                });
            };


            /* Обработка кнопки expend */
            var Expend = AConteiner.ChildByID('Expend');
            Expend.classList.add('Collapsed');
            Childs.Hide();
/*
            if (Record.ChildCount>0)
            {
*/
                /* Клик на кнопку разворачивания папок */
                Expend.onclick = function()
                {
                    that.Descripts.LoadStop();
                    if (this.classList.contains('Collapsed'))
                    {
                        /* Распахиваем */
                        Expend.classList.remove('Collapsed');
                        Expend.classList.add('Expended');
                        Childs.Show();
                        ChildList.SetContent( '' );
                        /* Перегружаем контейнер */
                        AConteiner.Reload();
                    }
                    else
                    {
                        /* Stop requests for folder */
                        clRequestsClose( 'FolderTree_' + Record.ID  );

                        /* схлопываем  */
                        Expend.classList.remove('Expended');
                        Expend.classList.add('Collapsed');
                        Childs.Hide();
                    }
                };
/*
            }
            else
            {              
                Expend.style.opacity=0;
            }
*/
        };


        var Param = {};

        Param.IDParent      = clNotEmpty( IDParent, Param.IDParent );
        Param.ID            = clNotEmpty( ID, Param.ID );
        Param.IDLang        = that.OnGetLang();
        Param.IDBind        = IDBind;
        Param.IDSiteRequest = IDSiteRequest;
        Param.IDGroup       = 'FolderTree_' + Param.IDParent;

        Descripts.Load( Param );
    }
}


