/*Dialog mode*/
const dmMoment   = 'moment';
const dmPeriod   = 'period';

/* Dialog stage */
const dsUnknown  = 'unknown';
const dsBegin    = 'begin';
const dsEnd      = 'end';

const diSimple   = 'simple';
const diFull     = 'full';


/*
    Активация элементов TMoment
*/

Element.prototype.MomentActivate = function( AApplication )
{
    var IDBegin      = this.getAttribute( 'clBegin' );
    var IDEnd        = this.getAttribute( 'clEnd' );

    var ObjBegin     = this.parentNode.ChildByID( IDBegin );
    var ObjEnd       = this.parentNode.ChildByID( IDEnd );

    var ObjButton    = this.ChildsByClass( 'MomentButton' )[0];
    var ObjValue     = this.ChildsByClass( 'MomentValue' )[0];

    ObjValue.Type    = 'Moment';
    ObjValue.Format  = AApplication.SessionParam.MomentFormat;

    /* если элемент не инициализирован то инициализируем его */
    if( ! this.Object )
    {
        this.Object = new TMomentControl( AApplication );               /* Создали новый элемент */
        this.Object.InputBegin = ObjBegin ? ObjBegin : ObjValue;        /* Записали в элемент указатель на DOM элемент */
        this.Object.InputEnd = ObjEnd ? ObjEnd : null;                  /* Записали в элемент указатель на DOM элемент */
        ObjValue.onkeyup = Event => MomentValidate( Event, this.Format );

        if( ObjButton )
        {
             this.Object.Button = ObjButton;           /* Сохранили кнопочку */
             this.Object.Button.Object = this.Object;                 
             this.Object.Button.onclick = () => this.Object.Popup();
        }
    }

    ObjValue.SetMoment = function( AValue )
    {
        this.value = clMomentToStr( AValue, '' , this.Format );
        return this;
    };



    ObjValue.GetMoment = function()
    {
        return clStrToMoment( this.value, '', this.Format );
    };



    ObjValue.MomentHide = function()
    {
/*
        this.Hide();
        this.parentNode.Hide();
        this.Caption.Hide();
        this.Button.Hide();
*/
console.log('Hide');
    };



    ObjValue.MomentDisable = function()
    {
console.log('Disabled');
/*
        this.disabled = 'disabled';
        this.parentNode.classList.add( 'Disabled' );
        this.Caption.disabled = 'disabled';
        this.Button.disabled = 'disabled';
*/
    };


    return this;
};





TCatlair.prototype.TimeIntervalActivate = function( AConteiner )
{
    for( let Element of AConteiner.ChildsByClass( 'TimeInterval' ))
    {
        Element.onkeyup = TimeIntervalValidate;
    }
    return this;
};



TCatlair.prototype.MomentActivate = function( AConteiner )
{
    var SelectList = AConteiner.ChildsByClass( 'Moment' );
    for ( var i=0; i < SelectList.length; i++ )
    {
        /* очередной элемент выбираем */
        SelectList[ i ].MomentActivate( this );
    }
    return this;
};



class TMomentControl
{

    constructor ( AApplication )
    {
       this.Application  = AApplication;

       this.InputBegin   = null;
       this.InputEnd     = null;

       this.Base         = 0; /* Point of view for output */ 
       this.Current      = 0; /* Current moment for single input*/
       this.CurrentBegin = 0; /* Current moment for begin of period */
       this.CurrentEnd   = 0; /* Current moment for end of period */

       this.Mode         = dmMoment;
       this.Stage        = dsUnknown;

    }



    Popup()
    {
        var that = this;

        /* Установка типа элемента */
        this.Mode = this.InputEnd ? dmPeriod : dmMoment;

        this.Stage = dsUnknown;
        this.Interface = this.InputBegin.getAttribute( 'interface' );

        if (this.Mode == dmMoment)
        {
            this.Current = clStrToMoment
            (
                this.InputBegin.value, 
                clBegDate(clNow()), 
                this.Application.SessionParam.MomentFormat
            );
        }
        else
        {
            this.CurrentBegin = clStrToMoment
            (
                this.InputBegin.value, 
                clBegDate(clNow()), 
                this.Application.SessionParam.MomentFormat 
            );

            this.CurrentEnd = clStrToMoment
            (
                this.InputEnd.value, 
                clEndDate(clNow()), 
                this.Application.SessionParam.MomentFormat 
            );
            this.Current = this.CurrentBegin;
        }
        this.Base = this.Current;

        var PopupContent = decodeURIComponent( '<cl content="MomentPopup.html" convert="URI"/>' );
        this.PopupWin = this.Application.Popup
        ({
            TypeContent:tcText, 
            Content:PopupContent, 
            Class:'MenuPopup DateTimePopup' 
        });

        /* Fill hour */                   
        this.PopupWin.ChildByID
        (
            'Hour',
             Select => 
             {
                 for( var i = 0; i < 24; i++ )
                 {
                     var Option = document.createElement( 'option' );
                     Option.text = i;
                     Option.value = i;
                     Select.add( Option );
                 }
                 Select.onchange = function() { that.SetHour( Select.value ) }; 
             }
        );

        /* Fill Minutes */                   
        this.PopupWin.ChildByID
        (
            'Minute',
             Select => 
             {
                 for( var i = 0; i < 60; i++ )
                 {
                     var Option = document.createElement( 'option' );
                     Option.text = i;
                     Option.value = i;
                     Select.add( Option );
                 }
                 Select.onchange = function() { that.SetMinute( Select.value ) }; 
             } 
        );

        /* Fill Minutes */                   
        this.PopupWin.ChildByID
        (
            'Second',
             Select => 
             {
                 for( var i = 0; i < 60; i++ )
                 {
                     var Option = document.createElement( 'option' );
                     Option.text = i;
                     Option.value = i;
                     Select.add( Option );
                 }
                 Select.onchange = function() { that.SetSecond( Select.value ) };
             }
        );

        this.Fill();

        return this; 
    }



    Fill()
    {        
        var that = this; 

        function Holyday(AValue)
        {
            var d = clGetDay(AValue);
            return (d==5 || d==6);
        }

        function FillMonth( ATable, ABegMonth )
        {
            /*Первый день в календаре*/
            var dn = ABegMonth - clDate * ( clGetDay( ABegMonth ));

            for ( var y=1; y<7; y++ )
            {
                for ( var x=1; x<8; x++ )
                {
                    var Cell = ATable.ChildByID( 'c' + y + x );

                    if ( dn >= ABegMonth && dn < clEndMonth( ABegMonth ))
                    {
                        Cell.innerHTML = clGetDate( dn );
                        Cell.Value = dn;
                        Cell.className='';

                        if 
                        (
                            that.Mode == dmPeriod &&
                            dn >= clGetDayBegin( that.CurrentBegin ) && 
                            dn < clGetDayBegin( that.CurrentEnd )
                        )
                        {
                            Cell.classList.add( 'Marked' );
                        }
                        else
                        {
                            Cell.classList.add( 'Unselected' );
                        }

                        /* Set class for current cell */
                        if ( clGetDayBegin( that.Current ) == dn ) Cell.classList.add( 'Selected' );

                        /* Marked class for holidays */
                        if ( Holyday( dn )) Cell.classList.add( 'Holyday' );

                        Cell.onclick = function()
                        {
                            that.Click( this.Value );
                            if (that.Mode == dmMoment) that.Ok();
                        };

                        Cell.ondblclick = function() 
                        {
                            that.Click( this.Value ); 
                            that.Ok();
                        };
                    }
                    else 
                    {
                        Cell.className  = '';
                        Cell.innerHTML  = '';
                        Cell.onclick    = null;
                        Cell.ondblclick = null;
                    }
                    dn=dn + clDate;
                }
            }
        }

        var Left   = this.PopupWin.ChildByID( 'CaptionLeft' );
        var Center = this.PopupWin.ChildByID( 'CaptionCenter' );
        var Right  = this.PopupWin.ChildByID( 'CaptionRight' );

        Left.innerHTML = clGetMonthName( clBegMonth(this.Base) - clDate)+' '+clGetYear(clBegMonth(this.Base) - clDate);
        Center.innerHTML = clGetMonthName(this.Base)+' '+clGetYear(this.Base);
        Right.innerHTML = clGetMonthName(clEndMonth(this.Base) + clDate)+' '+clGetYear(clEndMonth(this.Base) + clDate);

        FillMonth( this.PopupWin.ChildByID( 'Left' ), clBegMonth( clBegMonth(this.Base)-clDate));
        FillMonth( this.PopupWin.ChildByID( 'Center'), clBegMonth( this.Base));
        FillMonth( this.PopupWin.ChildByID( 'Right'), clBegMonth( clEndMonth(this.Base)+clDate));

        /* Время */
        var Hour = this.PopupWin.ChildByID( 'Hour' );
        var Minute = this.PopupWin.ChildByID( 'Minute' );
        var Second = this.PopupWin.ChildByID( 'Second' );

        Hour.value = clGetHour( this.CurrentBegin );
        Minute.value = clGetMin( this.CurrentBegin );
        Second.value = clGetSec( this.CurrentBegin );

        var PeriodNext = this.PopupWin.ChildByID( 'PeriodNext' );
        if ( PeriodNext )
        {
            PeriodNext.onclick = function(){ that.PeriodNext() };
            PeriodNext.SetVisible( this.Mode == dmPeriod );
        }
 
        var PeriodPrev = this.PopupWin.ChildByID( 'PeriodPrev' );
        if( PeriodPrev )
        {
            PeriodPrev.onclick = function(){ that.PeriodPrev() };    
            PeriodPrev.SetVisible( this.Mode == dmPeriod );
        }

        var MonthCurrent = this.PopupWin.ChildByID( 'MonthCurrent' );
        if( MonthCurrent )
        {
            MonthCurrent.onclick = function(){ that.MonthCurrent() };
            MonthCurrent.SetVisible( this.Mode == dmPeriod );
        }

        var WeekCurrent = this.PopupWin.ChildByID( 'WeekCurrent' );
        if( WeekCurrent )
        {
            WeekCurrent.onclick = function(){ that.WeekCurrent() };
            WeekCurrent.SetVisible( this.Mode == dmPeriod );
        }

        this.PopupWin.ChildByID( 'Today' ).onclick     = () => this.Today();
        this.PopupWin.ChildByID( 'Now' ).onclick       = () => that.Now();
        this.PopupWin.ChildByID( 'Noon' ).onclick      = () => that.Noon();
        this.PopupWin.ChildByID( 'Midnight' ).onclick  = () => this.Midnight();

        this.PopupWin.ChildByID( 'YearNext' ).onclick  = () => that.YearNext();
        this.PopupWin.ChildByID( 'YearPrev' ).onclick  = () => that.YearPrev();
        this.PopupWin.ChildByID( 'MonthNext' ).onclick = () => that.MonthNext();
        this.PopupWin.ChildByID( 'MonthPrev' ).onclick = () => this.MonthPrev();

        this.PopupWin.ChildByID( 'Ok' ).onclick        = () => this.Ok();
        this.PopupWin.ChildByID( 'Reset' ).onclick     = () => this.Reset();
        this.PopupWin.ChildByID( 'BtnCancel' ).onclick = () => this.Close();

        return this;
    }



    Click( AValue )
    {
       if ( this.Mode == dmPeriod )
       {
           switch ( this.Stage )
           {
               case dsUnknown:
               case dsEnd:
                   this.Stage = dsBegin;
                   this.CurrentBegin = clGetDayBegin( AValue ) + clGetTime( this.CurrentBegin );
                   this.CurrentEnd = this.CurrentBegin;
               break;
               case dsBegin:
                   this.Stage = dsEnd;
               break;
          }
       }
       else
       {
           this.CurrentBegin = clGetDayBegin( AValue ) + clGetTime( this.CurrentBegin );
       }

       /* Set current moment */
       this.Current = clGetDayBegin( AValue ) + clGetTime( this.Current );

       this.SetDay( AValue );
       return this;
    };



    SetDay( AValue ) 
    {     
        if (this.Mode == dmPeriod)
        {
            if ( this.Stage == dsBegin ) this.CurrentBegin = clGetDayBegin( AValue ) + clGetTime( this.CurrentBegin );
            if ( this.Stage == dsEnd ) this.CurrentEnd = clGetDayBegin( AValue ) + clGetTime( this.CurrentEnd );
            if ( this.CurrentBegin > this.CurrentEnd )
            {
                var Accum = this.CurrentBegin;
                this.CurrentBegin = this.CurrentEnd;
                this.CurrentEnd = Accum;
            }
        }
        this.Fill();
        return this;
    }



    Ok() 
    {
        if (this.Mode == dmPeriod)
        {

            this.Return
            (
                clMomentToStr( this.CurrentBegin, '', this.Application.SessionParam.MomentFormat ),
                clMomentToStr( this.CurrentEnd, '', this.Application.SessionParam.MomentFormat )
            );
        }
        else
        {
            this.Return
            (
               clMomentToStr( this.CurrentBegin, '', this.Application.SessionParam.MomentFormat ),
               null
            );
        }
        this.Close();
        return this;
    }



    Close()
    {
        this.PopupWin.Close();
        return this;
    }



    Reset()
    {
        this.Return('','');
        this.Close();
        return this;
    }



    YearPrev() 
    {
        this.Shift( - clYear );
        return this;
    }



    YearNext() 
    {
        this.Shift( + clYear );
        return this;
    }




    MonthPrev() 
    {
        this.Base = clSetMonth( this.Base, clGetMonth(this.Base) - 1 );
        this.Fill();
        return this;
    }



    MonthNext() 
    {
        this.Base = clSetMonth( this.Base, clGetMonth( this.Base ) + 1 );
        this.Fill();
        return this;
    }



    Shift( AValue ) 
    {
        this.Base =  this.Base + AValue;
        this.Fill();
        return this;
    }



    Return(AValueBegin, AValueEnd)
    {
        this.InputBegin.value = AValueBegin;
        if (this.Mode==dmPeriod) this.InputEnd.value = AValueEnd;
        return this;
    }



    Today() 
    {
        if ( this.Mode == dmPeriod )
        {
            this.CurrentBegin = clGetDayBegin( clNow() ) + clGetTime( this.CurrentBegin );
            this.CurrentEnd = clGetDayBegin( clNow() + clDate ) + clGetTime( this.CurrentEnd );
            this.Current = this.CurrentBegin;
        }
        else
        {
            this.Current = clGetDayBegin( clNow() ) + clGetTime( this.Current );
        }

        this.Base = this.Current;

        this.Fill();
        return this;
    }



    Noon() 
    {
        this.CurrentBegin = clBegDate( this.CurrentBegin ) + clDate * 0.5;
        this.CurrentEnd = clBegDate( this.CurrentEnd ) + clDate * 0.5;
        this.Fill();
        return this;
    }



    Midnight() 
    {
        this.CurrentBegin = clBegDate( this.CurrentBegin );
        this.CurrentEnd = clBegDate( this.CurrentEnd );
        this.Fill();
    }



    Now() 
    {
        this.CurrentBegin =clNow();
        this.CurrentEnd = clNow() + clDate;
        this.Base = this.CurrentBegin;
        this.Fill();
    }





    SetHour( AValue ) 
    {
        var h = parseInt( AValue );
        if ( h >= 0 && h < 24)
        {
            this.CurrentBegin = clSetHour( this.CurrentBegin, h );
            this.CurrentEnd = clSetHour( this.CurrentEnd, h );
        }
        return this;
    }



    SetMinute( AValue ) 
    {
        var m = parseInt( AValue );
        if ( m >= 0 && m < 60 )
        {
            this.CurrentBegin = clSetMin( this.CurrentBegin, m );
            this.CurrentEnd = clSetMin( this.CurrentEnd, m );
        }
        return this;
    }



    SetSecond( AValue ) 
    {
        var s = parseInt( AValue );
        if ( s >= 0 && s < 60 ) 
        {
            this.CurrentBegin = clSetSec( this.CurrentBegin, s );
            this.CurrentEnd = clSetSec( this.CurrentEnd, s );
        }
        return this;
    }



    PeriodPrev() 
    {
        var Delta = this.CurrentEnd - this.CurrentBegin;
        this.CurrentBegin = this.CurrentBegin - Delta;
        this.CurrentEnd = this.CurrentEnd - Delta;
        this.Base = ( this.CurrentEnd+this.CurrentBegin ) * 0.5;
        this.Fill();
    }



    MonthCurrent() 
    {
        this.CurrentBegin = clBegMonth( this.Current );
        this.CurrentEnd = clEndMonth( this.Current );
        this.Fill();
    }



    WeekCurrent() 
    {
        this.CurrentBegin = clBegWeek( this.Current );
        this.CurrentEnd = clEndWeek( this.Current );
        this.Fill();
    }


    PeriodNext() 
    {
        var Delta = this.CurrentEnd - this.CurrentBegin;
        this.CurrentBegin = this.CurrentBegin + Delta;
        this.CurrentEnd = this.CurrentEnd + Delta;
        this.Base = (this.CurrentEnd + this.CurrentBegin) * 0.5;
        this.Fill();
    }
}


