/*************************************************************
    Camera
*/



var CAMERA_BASE_POSITION = 1;
var CAMERA_BASE_TARGET = 2;



function clCameraCreate()
{
 var Result = new clVector();
 Result.Scale=new clVector(0.3,0.3);
 Result.Mouse=null;
 return Result;
}



function clCameraAply(AScene, ACamera)
{
 AScene.Context.setTransform(ACamera.Scale.x, 0, 0, ACamera.Scale.y, -ACamera.x+AScene.Canvas.width*0.5, -ACamera.y+AScene.Canvas.height*0.5);
}



function clCameraReset(AScene)
{
 AScene.Context.setTransform(1, 0, 0, 1, 0, 0);
}



function clCameraSetPosition(ACamera, APosition)
{
 ACamera.Load(APosition);
}



function clCameraShift(ACamera, ADelta)
{
 ACamera.Add(ADelta);
}



function clCameraScale(ACamera, AScale)
{
/* ACamera.Scale=clVec(ACamera.Scale.x*AScale, ACamera.Scale.y*AScale);*/
}



function clCameraMouseMove(ACamera, AEvent)
{
 if (ACamera.Mouse)
 {
  if (ACamera.LeftButton)
  {
   ACamera.Drag=true;
   var DeltaPos=clVecSub(clVec(AEvent.layerX, AEvent.layerY), ACamera.Mouse);
   clCameraShift(ACamera, DeltaPos);
  }
 }
 ACamera.Mouse=clVec(AEvent.layerX, AEvent.layerY);
}



function clCameraMouseDown(ACamera, AEvent)
{
 if (AEvent.which==1)
 {
  ACamera.LeftButton=true;
  ACamera.Drag=false;
 }
}



function clCameraMouseUp(ACamera, AEvent)
{
 if (AEvent.which==1)
 {
  ACamera.LeftButton=false;
  ACamera.Drag=false;
 }
}



function clCameraMouseOut(ACamera)
{ 
 ACamera.Mouse=null;
}



function clCameraMouseClick(ACamera)
{ 
 ACamera.Mouse=null;
}



function clWorldToScreen(AScene, ACamera, AWorld)
{
 return AWorld.GetMul(ACamera.Scale).Sub(ACamera).Add( AScene.GetSize().Scal(0.5) );
}


function clScreenToWorld(AScene, ACamera, AScreen)
{
    return new clVector
    (
       (ACamera.x + AScreen.x-AScene.GetSize().x*0.5) / ACamera.Scale.x,
       (ACamera.y + AScreen.y-AScene.GetSize().y*0.5) / ACamera.Scale.y
    );
}



function clScreenTest(AScene, ACamera, AWorld, ARadius)
{
 var s=clWorldToScreen(AScene, ACamera, AWorld);
 return (s.x>-ARadius && s.y>-ARadius && s.x<AScene.Canvas.width+ARadius && s.y<AScene.Canvas.height+ARadius);
}


function clCamera()
{
 this.Position = new clVector(0,0,-1);
 this.Top = new clVector(0,1,0);
 this.Target = new clVector(0,0,0);
 this.Type = CAMERA_BASE_TARGET;
 return this;
}

clCamera.prototype.Dump = function()
{
 var s='Catlair camera dump\n';
 s=s+'----------------------------------------------------------------\n';
 s=s+'Pos: '+this.GetPosition().String()+'\n';
 s=s+'Tgt: '+this.GetTarget().String()+'\n';
 s=s+'----------------------------------------------------------------\n';
 s=s+'Lft: '+this.GetLeft().String()+'\n';
 s=s+'Top: '+this.GetTop().String()+'\n';
 s=s+'Frt: '+this.GetFront().String()+'\n';
 s=s+'----------------------------------------------------------------\n';
 s=s+'DgX: '+clFloatToStr(Math.acos(this.GetLeft().Dot(clXVector)) / clPI2*360) +'   SXY: '+clFloatToStr(Math.acos(this.GetTop().Dot(this.GetLeft())) / clPI2*360) + '°\n';
 s=s+'DgY: '+clFloatToStr(Math.acos(this.GetTop().Dot(clYVector)) / clPI2*360) +'   SXZ: '+clFloatToStr(Math.acos(this.GetFront().Dot(this.GetLeft())) / clPI2*360) + '°\n';
 s=s+'DgZ: '+clFloatToStr(Math.acos(this.GetFront().Dot(clZVector)) / clPI2*360) +'   SYZ: '+clFloatToStr(Math.acos(this.GetFront().Dot(this.GetTop())) / clPI2*360) + '°\n';
 s=s+'----------------------------------------------------------------\n';
 s=s+'Dst: '+clFloatToStr(this.GetTarget().Dist(this.GetPosition()))+'\n';
 return s;
};



clCamera.prototype.GetPosition = function()
{
 return new clVector().Load(this.Position);
};

clCamera.prototype.GetTarget = function()
{
 return new clVector().Load(this.Target);
};

clCamera.prototype.GetLeft = function()
{
 return this.GetTop().GetCross(this.GetFront()).Norm();
};

clCamera.prototype.GetTop = function()
{
 return new clVector().Load(this.Top);
};

clCamera.prototype.GetFront = function()
{
 return this.Target.GetSub(this.Position).Norm();
};


clCamera.prototype.Rotate = function(ADelta, ABase)
{
 if (this.Type == CAMERA_BASE_TARGET)
 {
  this.Position = this.GetPosition().Sub(this.GetTarget()).Rotate({Base:ABase, Delta:ADelta}).Add(this.GetTarget());
 } 
 else
 {
  this.Target=this.GetTarget().Sub(this.GetPosition()).Rotate({Base:ABase, Delta:ADelta}).Add(this.GetPosition());
 }
 this.Top.Rotate({Base:ABase, Delta:ADelta});
};



