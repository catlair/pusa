class TPay extends TParams
{
    constructor ( AApplication )
    {
        super();

        this.Application = AApplication;
        this.OnGetCharge = null;
        this.OnGetPay = null;

        this.BtnCharge = null;
        this.BtnPay = null;
        this.BtnIncome = null;
        this.BtnChange = null;

        this.BtnPayCash = null;
        this.BtnPayCard = null;
        this.BtnPayCashless = null;
        this.BtnPayPaypal = null;          

        this.Charge = 0.0;
        this.Income = 0.0;

        this.Point = 0;
    }


    /*
        !!!!
    */
    static Create( AApplication, AIDPaymentMethod, AOnInit )
    {
        var Result = null;
        if( AIDPaymentMethod )
        {
            /* Result */
            TRequest.Create( AApplication )
            .SetParam( 'IDPaymentMethod', AIDPaymentMethod )
            .Execute
            ({
                Name: 'Billing.PaymentMethodConfig',
                OnAfterExecute: Request =>
                {           
                    var PaymentMethodName = Request.Result.Outcome.ExtLibrary;
                    AApplication.ScriptWait
                    (
                        [ 'Payment' + PaymentMethodName + '.js' ], () =>
                        {
                            var ClassName = 'TPayment' + PaymentMethodName;
                            var Pay = null;
                            try
                            {
                               var Pay = eval( ClassName );
                            } catch ( error ) {}
                            if( Pay )
                            {
                                Pay.Create( AApplication )
                                .SetParams( Request.Result.Outcome )
                                .SetParam( 'IDPaymentMethod', AIDPaymentMethod )
                                .Init( AOnInit );
                            }
                        }
                    );
                }
            });
        }
        else
        {
            Result = new TPay( AApplication );
        }
        return Result;
    }



    Popup( AParams )
    {
        var that = this;

        /* Content popup */
        var PopupContent = decodeURIComponent( '<cl content="PayForm.html" pars="true" convert="URI"/>' );
        this.PopupWin = this.Application.Popup
        ({
            TypeContent:  tcText, 
            Content:      PopupContent,
            FullScreen:   true
        });
        this.PopupWin.classList.add( 'PayForm' );

        /* Set action on close */
        this.PopupWin.ChildByID( 'BtnClose' ).onclick = () =>
        {
            that.PopupWin.Close();
        };


        /* Set action on close */
        this.BtnCharge = this.PopupWin.ChildByID( 'BtnCharge' );
        if ( this.BtnCharge ) this.BtnCharge.onclick = function()
        {
            that.CalcCharge();
        };


        /* Set action on close */
        this.BtnPay = this.PopupWin.ChildByID( 'BtnPay' );
        this.BtnChange = this.PopupWin.ChildByID( 'BtnChange' );

        /* Set caption */
        this.Caption = this.PopupWin.ChildByID( 'Caption' );
        if ( this.Caption ) this.Caption.innerHTML = AParams.Caption;


        /* Set action on close */
        this.BtnIncome = this.PopupWin.ChildByID( 'BtnIncome' );
        if ( this.BtnIncome ) this.BtnIncome.onclick = function()
        {
            that.AutoIncome();
        };

        this.PopupWin.ChildsByClass( 'Button' ).SetAction
        (
            'click',
            function()
            {
                that.Button(this);
            }
        );



        /* 
            Actions on pay buttons 
        */

        /* Set action on close */
        this.PopupWin.ChildByID( 'BtnPayCash', Btn => Btn.onclick = () => { if( that.OnPay ) that.OnPay( 'OperaionPayCash' )} );

        /* Set action on close */
        this.BtnPayCard = this.PopupWin.ChildByID( 'BtnPayCard' );
        if ( this.BtnPayCard ) this.BtnPayCard.onclick = function()
        {
            if ( that.OnPay ) that.OnPay( 'OperationPayPlastic' );
        };

        /* Set action on close */
        this.BtnPayCashless = this.PopupWin.ChildByID( 'BtnPayCashless' );
        if ( this.BtnPayCashless ) this.BtnPayCashless.onclick = function()
        {
            if ( that.OnPay ) that.OnPay( 'OperationPayCashless' );
        };

        /* Set action on close */
        this.BtnPayPaypal = this.PopupWin.ChildByID( 'BtnPayPaypal' );
        if ( this.BtnPayPaypal ) this.BtnPayPaypal.onclick = function()
        {
            if ( that.OnPay ) that.OnPay( 'OperationPayPaypal' );
        };


        this.CalcCharge();
    }


    /*
        Close dialog 
    */
    Close()
    {
        this.PopupWin.Close();
    }



    Button( AButton )
    {
        var Char = AButton.innerHTML.trim();
        var ValueStr = this.Income.toString();
        var Float = ValueStr . split( '.' );
        if (Float.length < 2 ) Float[1] = '';

        switch ( Char )
        {
            default:
               Float[ this.Point ] += Char;
            break;

            case '.':
               this.Point = this.Point == 0 ? 1 : 0;
            break;

            case 'C':
               Float[0] = 0;
               Float[1] = 0;
               this.Point = 0;
            break;
        }

        this.SetIncome( parseFloat( Float[0] + '.' + Float[1] ) );
        return this;  
    }



    CalcCharge()
    {     
        var that = this;
        if ( this.OnGetCharge )
        {
            this.OnGetCharge
            ({
                OnGetChargeSucess: function( p )
                {
                    that.SetChargeAndPay
                    (
                        parseFloat( p.ChargeAmount ), 
                        parseFloat( p.PayAmount )
                    );
                }   
            });
        }
        else 
        {
            this.SetChargeAndPay( 0, 0 );
        }
        return this;
    }



    AutoIncome()
    {
        this.SetIncome( this.Charge - this.Pay );
        return this;
    }



    SetChargeAndPay( ACharge, APay )
    { 
       this.Charge = ACharge; 
       this.Pay = APay;
       this.Refresh();
       return this;
    }



    SetIncome( AValue )
    { 
       this.Income = AValue; 
       this.Refresh();
       return this;
    }



    Refresh()
    {
       if ( this.BtnCharge ) this.BtnCharge.innerHTML = this.Charge.toFixed( 2 );
       if ( this.BtnPay ) this.BtnPay.innerHTML = this.Pay.toFixed( 2 );
       if ( this.BtnIncome ) this.BtnIncome.innerHTML = this.Income.toFixed( 2 );
       if ( this.BtnChange ) this.BtnChange.innerHTML = (this.Income - this.Charge + this.Pay ).toFixed( 2 );
    }



    /*
         Popup payments method list
    */
    PopupPaymentMethod( AParams )
    {
        /* Content popup */
        var PopupContent = decodeURIComponent( '<cl content="PayMethodForm.html" pars="true" convert="URI"/>' );

        var Popup = this.Application.Popup
        ({
            TypeContent:  tcText, 
            Content:      PopupContent,
            FullScreen:   true
        });

        Popup.classList.add( 'PayMethod' );

        var Container = Popup.ChildsByClass( 'List' )[0];

        /* List of payment methods */
        TDOMDataset.Create( this.Application )
        .SetParam( 'IDType', 'PaymentMethod' )
        .Load
        ({
            Name: 'DescriptContent.Unilist',
            OnGetRecordContent: () => { return decodeURIComponent( '<cl content="PayMethodRecord.html" pars="true" convert="URI"/>' ) },
            OnGetParentContainer: () => { return Container },
            OnAfterRecord: ( AContainer, ARecord ) =>
            {
                 AContainer.onclick = () =>
                 {
                     Popup.Close();
                     TPay.Create
                     (
                         this.Application,
                         ARecord.ID,
                         NewPay => 
                         {
                             if( NewPay.PayBegin ) NewPay.PayBegin( AParams );
                         }
                     );
                 }
            }
        });


        /* Set action on close */
        Popup.ChildByID( 'BtnCancel', Btn => Btn.onclick = () =>  Popup.Close() );
        return this;
    }



    /*
        Popup payment form with content
        This method must be alled form specific library
    */
    PopupPayForm( AParams, AContent )
    {
        AParams.Popup = this.Application.Popup
        ({
            TypeContent:  tcText, 
            Content:      clContentFromObject( AParams, AContent ),
            FullScreen:   true
        });

        AParams.Popup.classList.add( 'PaymentForm' );


        /* Set action on close */
        AParams.Popup.ChildByID
        (
            'Amount', Element => 
            {
                Element.onkeyup = () => this.CalcCommission( Element.value, AParams.Popup );
                this.CalcCommission( Element.value, AParams.Popup );
            }
        );


        /* Set action on close */
        AParams.Popup.ChildByID
        (
            'BtnCancel', Btn => Btn.onclick = () => AParams.Popup.Close() 
        );

        /* Set action on pay */
        AParams.Popup.ChildByID
        (
            'BtnPay', Btn => Btn.onclick = () => 
            {
                TRequest.Create( this.Application )
                .SetParam( 'ConfirmPaymentAndRefund', AParams.Popup.ChildByID( 'ConfirmPaymentAndRefund' ).checked )
                .Execute
                ({
                    Name: 'Billing.ConfirmPaymentAndRefund',
                    OnAfterExecute: Request =>
                    {
                        /* Build forms parameters */
                        AParams.Form = clValuesToArray( AParams.Popup );
                        /* Add payment method */
                        AParams.Form[ 'PaymentMethod' ] = this.GetParam( 'ID' );
                        this.Pay( AParams );
                    }
                });
            }
        );
    
        return this;
    }



    /*
        Send payment form to server
    */
    SendPay( AParams )
    {
        this.Application.Confirm
        ({
            OnClick: () => this.Application.Post
            ({
                Call:'Billing.Pay',
                Income: AParams.Form,
                OnAfterLoad: Result =>
                {
                    if( this.Application.PostResult( Result ) && this.PayEnd )
                    {
                        this.PayEnd( AParams );
                    }
                }
            })
        });
        return this;
    }



    CalcCommission( AAmount, APopup )
    {
            TRequest.Create( this.Application )
            .SetParam( 'Amount', AAmount )
            .SetParam( 'IDPaymentMethod',  this.GetParam( 'ID' ))
            .Execute
            ({
                Name:'Billing.CalcAmoutForPayment',
                OnAfterExecute: Request =>
                {
                    APopup.ChildByID( 'CommissionPercent', Element => Element.SetContent( Request.Result.Outcome.CommissionPercent ));
                    APopup.ChildByID( 'CommissionAmount',  Element => Element.SetContent( Request.Result.Outcome.CommissionAmount ));
                    APopup.ChildByID( 'AmountForPaymentLabel',  Element => Element.SetContent( Request.Result.Outcome.AmountForPayment ));
                    APopup.ChildByID( 'AmountForPayment',  Element => Element.value = Request.Result.Outcome.AmountForPayment );
                }
            });
    }
}



