/*
    Popup dialog for change password
*/
TDescript.prototype.PasswordAdminPopup = function()
{
    var that = this;
    var Content = decodeURIComponent( '<cl content="AccountPasswordAdminForm.html" pars="true" convert="URI"/>' );
    Content = Content.replace( '%ID%', this.ID );
    var Popup = cl.Popup({ Content:Content, TypeContent:tcText });

    Popup.ChildByID('BtnCancel').onclick = function()
    {
        Popup.Close();
    };

    /* ОК buttons action */
    Popup.ChildByID('BtnOk').onclick = function()
    {
        that.PasswordAdminChange
        ({
            Form: Popup.ChildByID('Form'),
            OnAfterChange: function(p)
            {
                 Popup.Close();
            }
        });
    };
};



TDescript.prototype.PasswordAdminChange = function( AParams )
{
    cl.Post
    ({
        Call: 'Account.SetPassword',
        Form: AParams.Form,
        OnAfterLoad: function(p)
        {
            if (cl.PostResult(p))
            {
                if ( AParams.OnAfterChange ) AParams.OnAfterChange(p);
            }
        }
    });
};
