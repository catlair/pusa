class TDescriptMenu
{
    static Popup( AApplication, ADescript, AParams )
    {
        AParams.OnAfterLoad = p =>
        {
            var Content = decodeURIComponent( '<cl content="DescriptMenu.html" pars="true" convert="URI"/>' );

            /* Set ID if caption is not exists */
            var Caption = ! ADescript.Record || ! ADescript.Record.Caption ? ADescript.ID : ADescript.Record.Caption;

            Content = clContentFromObject( { ID: ADescript.ID, Caption:Caption },  Content );
            Content = clContentFromObject( p.Result.Outcome, Content );

            var Win = cl.Popup({ TypeContent:tcText, Content:Content });

            var BtnRemove  = Win.ChildByID('BtnRemove');

            var BtnInsert  = Win.ChildByID('BtnInsert');
            if (BtnInsert) BtnInsert.onclick=function()
            {
                var NewDescript = new TDescript();
                NewDescript.Edit({ IDParent: ADescript.ID });
                Win.Close();
            };




            Win.ChildByID
            (
                'BtnImport',
                Btn => BtnImport.onclick = () =>
                {
                    /* Get form content and pop it in form */
                    var Content = decodeURIComponent( '<cl content="DescriptsImport.html" pars="true" convert="URI"/>' );
                    Content = clContentFromObject( p.Result.Outcome, Content );

                    var PopupForm = AApplication.Popup({ ID:'DescriptsImport', TypeContent:tcText, Content:Content });

                    /* Get elements */
                    var BtnOk        = PopupForm.ChildByID( 'BtnOk' );
                    var BtnCancel    = PopupForm.ChildByID( 'BtnCancel' );

                    var ImportForm  = PopupForm.ChildByID( 'Form' );

                    var InputParent  = PopupForm.ChildByID( 'IDParent' );
                    /* Set value for popups */
                    InputParent.value  = ADescript.ID;

                    /*Активация всех элементов выбора окна*/
                    AApplication.SelectActivate( PopupForm );

                    /* Set event ok */
                    BtnOk.onclick = function()
                    {
                        var Child = new TDescript();
                        Child.Import
                        ({
                            Form: ImportForm,
                            OnAfterImport: () =>
                            {
                                PopupForm.Close();
                                Win.Close();
                            }
                        });
                    };

                    /* Set event Cancel */
                    BtnCancel.onclick = () => PopupForm.Close();
                }
            );






            Win.ChildByID
            (
                'BtnContent', Btn => Btn.onclick = () =>
                {
                    ADescript.ContentEdit({ IDLang: AParams.IDLang });
                    Win.Close(); 
                }
            );



            Win.ChildByID
            (
                'BtnEdit',
                Btn => Btn.onclick = () => 
                {
                    ADescript.Edit( { IDLang: AParams.IDLang } ); 
                    Win.Close(); 
                }
            );




            /* Действие перемещения из папки */
            /* Hide button for trash only*/
            BtnRemove.onclick=function()
            {
                AApplication.Confirm
                ({
                    OnClick: function()
                    {
                        ADescript.Remove
                        ({
                            ID: ADescript.ID,
                            IDFrom: AParams.IDFrom,
                            IDBind: AParams.IDBindParent,
                            OnAfterRemove:function()
                            {
                                Win.Close();
                                if (AParams.OnAfterRemove) AParams.OnAfterRemove(AParams);
                            }
                        });
                    }
                });
            };


            var BtnClear = Win.ChildByID( 'BtnClear' );
            if ( BtnClear ) BtnClear.onclick=function()
            {
                AApplication.Confirm
                ({
                    OnClick: function()
                    {
                        ADescript.Clear
                        ({
                            ID: ADescript.ID,
                            IDFrom: AParams.IDFrom,
                            IDBind: AParams.IDBindParent,
                            OnAfterClear:function()
                            {
                                Win.Close();
                            }
                        });
                    }
                });
            };




            /* Принудительная индексация */
            var BtnIndex = Win.ChildByID('BtnIndex');
            BtnIndex.onclick=function()
            {
                AApplication.Confirm
                ({
                    OnClick: function()
                    {
                        ADescript.Index
                        ({
                            ID: ADescript.ID,
                            OnAfterIndex:function()
                            {
                                Win.Close();
                            }
                        });
                    }
                });
            };



            /* Принудительная индексация */
            var BtnIndexSingle = Win.ChildByID('BtnIndexSingle');
            if ( BtnIndexSingle ) BtnIndexSingle.onclick = function()
            {
                 ADescript.IndexSingle
                 ({
                      ID: ADescript.ID,
                      OnAfterIndex:function()
                      {
                          Win.Close();
                      }
                 });
            };




            /* Получение прав от родителя */
            Win.ChildByID
            ( 
                'BtnRightFromParent', 
                function ( ABtn )
                {
                    ABtn.onclick=function()
                    {
                        AApplication.Confirm
                        ({
                            OnClick: function()
                            {
                                ADescript.RightFromParent
                                ({
                                    ID: ADescript.ID,
                                    OnRightFromParent:function(){ Win.Close(); }
                                });
                            }
                        });
                    };
                }
            );




            /* Normalize rights */
            Win.ChildByID
            ( 
                'BtnRightNorm', 
                function ( ABtn )
                {
                    ABtn.onclick=function()
                    {
                        AApplication.Confirm
                        ({
                            OnClick: function()
                            {
                                ADescript.RightNorm
                                ({
                                    ID: ADescript.ID,
                                    OnRightNorm:function(){ Win.Close(); }
                                });
                            }
                        });
                    };
                }
            );




            /* Раздача прав вниз */
            Win.ChildByID( 'BtnRightDown' ).onclick = () =>
            {
                AApplication.Confirm
                ({
                    OnClick: () =>
                    {
                        ADescript.RightDown
                        ({
                            ID: ADescript.ID,
                            OnAfterRightDown: () => Win.Close()
                        });
                    }
                });
            };

            Win.ChildByID( 'BtnBindSpreadDown', Btn => Btn.onclick = () => ADescript.BindSpreadForm ({}) );

            /* Add new bind event */
            Win.ChildByID( 'BtnChilds', Btn => Btn.onclick = () => ADescript.ChildPopup( AParams ));
            Win.ChildByID( 'BtnParents', Btn => Btn.onclick = () => ADescript.ParentPopup( AParams ));

            Win.ChildByID( 'BtnFieldsRight', Btn => Btn.onclick = () => ADescript.FieldsRightForm({ IDDescript: ADescript.ID, IDObject: null } ));
            Win.ChildByID( 'BtnTemplateFields', Btn => Btn.onclick = () => ADescript.TemplateFields() );
            Win.ChildByID( 'BtnParamsToChild', Btn => Btn.onclick = () => ADescript.ParamsToChild({ IDParent: ADescript.ID, IDBind: AParams.IDBind } ));
            Win.ChildByID( 'BtnParams', Btn => Btn.onclick = () => ADescript.ParamsEditor());
            Win.ChildByID( 'BtnInfo', Btn => Btn.onclick = () => ADescript.HistoryPopup({ ID:ADescript.ID } ));
            Win.ChildByID( 'BtnPath', Btn => Btn.onclick = () => ADescript.PathesPopup({ ID:ADescript.ID } ));
            Win.ChildByID( 'BtnEnabledHistory', Btn => Btn.onclick = () => ADescript.EnabledHistory());
            Win.ChildByID( 'BtnTask', Btn => Btn.onclick = () => ADescript.TaskPopup({ ID: ADescript.ID } ));
        };

        ADescript.Load(AParams);
    }
}


