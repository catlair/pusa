/*
 Catlair JS Copyright (C) 2019  a@itserv.ru

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.

 DOM tools

 still@itserv.ru

 Расиширение функционала Element
 ChildByTag
 ParentByTag
 ChildByID
 ParentByID
 ChildByClass
 ParentByClass
*/




/*
    Установка действия на объекты по классу
*/
class TElements extends Array
{
    SetAction( AActionName, AAction )
    {
        for (let Element of this)
        {
            Element.addEventListener(AActionName, AAction);
        }
        return this;
    }
}


/*
    Константы селекторы поиска объектов
*/
var SELECTOR_CLASS    = 'Class';
var SELECTOR_NAME     = 'Name';
var SELECTOR_TAG      = 'Tag';
var SELECTOR_ID       = 'ID';


/*
    Поиск детей по имени и селектору
*/
Element.prototype.ChildsOfThis = function( AValue, ASelector, ACall )
{
    var Result = [];
    if( !ASelector ) ASelector = SELECTOR_CLASS;
    for( var i=0; i<this.childNodes.length; i++ )
    {
        var Node  = this.childNodes[i];
        if
        (
            Node.nodeType == 1 &&
            (
                ASelector == SELECTOR_CLASS && Node.classList.contains( AValue ) ||
                ASelector == SELECTOR_NAME  && Node.name      == AValue ||
                ASelector == SELECTOR_TAG   && Node.tagName   == AValue ||
                ASelector == SELECTOR_ID    && Node.id        == AValue
            )
        )
        {          
            Result.push( Node );
            if( ACall ) ACall( Node );
        }
    }
    return Result;
};



/*
    DOM recursion for children
*/
Element.prototype.ChildRecursion = function( AParams, AMaxDepth, ADepth )
{
    AMaxDepth = AMaxDepth ? AMaxDepth : 0;
    ADepth = ADepth ? ADepth : 0;

    if ( AParams.OnBefore ) AParams.OnBefore( this, AParams );

    if( AMaxDepth == 0 || AMaxDepth > ADepth )
    {
        for (var i in this.childNodes )
        {
            var iNode=this.childNodes[i];
            if (iNode.nodeType == 1) iNode.ChildRecursion( AParams, AMaxDepth, ADepth+1 );
        }
    }

    if ( AParams.OnAfter ) AParams.OnAfter( this, AParams );

    return this;
};




/*
    Рекурсивный поиск первого ребенка по имени тэга
*/
Element.prototype.ChildByTag = function( ATagName )
{
    var Result = null;
    if (this.tagName != ATagName)
    {
        for (var i=0; i<this.childNodes.length && Result == null; i++)
        {
            var iNode=this.childNodes[i];
            if (iNode.nodeType == 1) Result = iNode.ChildByTag(ATagName);
        }
    }
    else Result = this;
    return Result;
};



/*
    Циклический поиск родителя по имени тэга
*/
Element.prototype.ParentByTag = function( ATagName )
{
    var Result = this;
    ATagName = ATagName.toUpperCase();
    while( Result && Result.tagName != ATagName ) Result = Result.parentNode;
    return Result;
};



/*
    Рекурсивный поиск ребенка по ID
*/
Element.prototype.ChildByID = function( AID, ACallback )
{
    var Result = [];
    this.ChildRecursion
    ({
        OnBefore: e => 
        {
             if( e.id == AID ) Result.push( e );
        }
    });
    if( ACallback ) Result.forEach( e => ACallback( e ) );
    return Result.length > 0 ? Result[0] : null;
};




/*
    Циклический поиск родителя по идентификатору
*/
Element.prototype.ParentByID = function(AID)
{
    return clGetParentByID(this, AID);
};



/*
    Поиск родителя по Class name
*/
Element.prototype.ParentByClass = function( AClassName )
{
    var Current = this;
    var Result = null;
    while( Current && ! Result ) {
       if( Current.classList && Current.classList.contains( AClassName ) ) Result = Current;
       Current = Current.parentNode;
    }
    return Result;
};



/*
    Рекурсивный поиск ребенка по Class
*/
Element.prototype.ChildByClass = function( AClass )
{
    var Result = null;
    if (this.classList && this.classList.contains( AClass ))
    {
        Result = this;
    }
    else
    {
        for (var i=0; i < this.childNodes.length && Result == null; i++)
        {
            var iNode=this.childNodes[i];
            if (iNode.nodeType == 1) Result = iNode.ChildByClass(AClass);
        }
    }
    return Result;
};




Element.prototype.ChildsByClass = function( AClass, ACallback )
{
    var Result = [];
    this.ChildRecursion
    ({
        OnBefore: e => 
        {
             if( e.classList && e.classList.contains( AClass )) Result.push( e );
        }
    });
    if( ACallback ) Result.forEach( e => ACallback( e ) );
    return Result;
};




Element.prototype.GetChildValueByID = function( AID, ADefault )
{
    var Result = ADefault ? ADefault : null;
    var Child = this.ChildByID( AID );
    if ( Child && Child.value != null ) Result = Child.value;
    return Result;
};



Element.prototype.SetChildValueByID = function( AID, AValue )
{
    this.ChildByID
    (
        AID, function( AElement )
        {
            AElement.value = AValue;
        }
    );
    return this;
};



/*
    Copy property by name AProperty:string from list of objects ASources:array to ATarget:object if property exists.
*/
function CopyProperty(ASources, ATarget, AProperty)
{
    var c=ASources.length;
    var i=0;
    var Continue = true;
    while (i<c && Continue)
    {
        var Source = ASources[i];
        if (Source.hasOwnProperty(AProperty) && Source[AProperty]!=null)
        {
            ATarget[AProperty] = Source[AProperty];
            Continue = false;
        }
        i++;
    }
    return !Continue;
}




/*
    Set content for element
*/
Element.prototype.IsEmpty = function()
{
    return ( ! this.innerHTML || this.innerHTML.trim() == '' );
};





/*
    Set content for element
*/
Element.prototype.SetContent = function( AValue )
{
    this.innerHTML = AValue ;
    return this;
};



/*
    Get content for element
*/
Element.prototype.getContent = function()
{
    return this.innerHTML;
};




/*
    Hide element
*/
Element.prototype.Hide = function()
{
    this.style.display='none';
    return this;
};



/*
    Fade off
*/
Element.prototype.Fade = function()
{

    this.style.transition = "opacity 0.25s ease-in-out";
    this.style.opacity = 0;

    return this;
};



Element.prototype.Unfade = function()
{
    this.style.opacity = 1; 
    return this;
};




/*
    Disable DOM element
*/
Element.prototype.Disable = function()
{
    this.disabled = 'disabled';
    return this;
};



/*
    Enable DOM element
*/
Element.prototype.Enable = function()
{
    this.disabled = null;
    return this;
};



/*
    Enable DOM element
*/
Element.prototype.SetEnable = function( AValue )
{
    if( AValue ) this.Enable();
    else this.Disable();
    return this;
};





/*
    Show element
*/
Element.prototype.Show = function()
{
    this.style.display=null;
    return this;
};



/*
    Set visible
*/
Element.prototype.SetVisible = function( AValue )
{
    AValue ? this.Show() : this.Hide();
    return this;
};



function clValuesToArray( AContainer )
{
   var Result = [];
   AContainer.ChildRecursion
   ({
       OnAfter: Element =>
       {
           if( Element.value && Element.id )
           {
               Result[ Element.id ] = Element.value;
           }
       }
   });
   return Result;
}





/*
    Move params from onbect in to DOM
*/
function clValuesFromObject( AObject, AConteiner, ACreate )
{
    for (var Key in AObject)
    {
        var Element = AConteiner.ChildByID( Key );

        if( !Element && ACreate)
        {
            Element = document.createElement( 'input' );
            Element.type = 'hidden';
            Element.name = Key;
            AConteiner.append( Element );
        }

        if (Element != null)
        {
            try {
                var KeyValue = decodeURIComponent( AObject[ Key ]);
            } catch (ex) {
                var KeyValue = null;
            }

            if ( KeyValue != null && KeyValue != 'null' ) 
            {
                switch( Element.tagName )
                {
                    case 'INPUT':
                       switch( Element.type )
                       {
                           case 'checkbox':
                               Element.checked = AObject[Key] == 'on' || AObject[Key] == 'true' || AObject[Key] == '1';
                           break;
                           case 'text':
                               Element.value = KeyValue;
                           break;
                           case 'hidden':
                               Element.value = KeyValue;
                           break;
                       }
                    break;

                    case 'SELECT':
                       Element.value = KeyValue;
                    break;

                    case 'TEXTAREA':
                       Element.innerHTML = KeyValue;
                    break;

                    default:
                       Element.innerHTML = KeyValue;
                    break;
                }
            }
        }
    }
}




/*
    Вспомогательный фукнционал
*/


/*
    Element by id
    просто укороченная запись
*/
function EBI(AID)
{
    return document.getElementById(AID);
}



/*
    Инвертировать видимость объекта по идентифиатору
*/
function clShowHideByID(AIDObject)
{
    var obj=document.getElementById(AIDObject);
    if (obj)
    {
        if (obj.style.display=='none')
        {
            obj.style.display = null;
        }
        else
        {
            obj.style.display = 'none';
        }
    }
}



/*
    Преобразование контента DOM в объект
    рекурсивно объходит переданную ноду формирует объект с своствами и методами
*/
function clDOMToObject(AStartNode, AStartObject)
{
    function Recurs(ANode, AObject)
    {
        if (ANode.attributes)
        {
            var l=ANode.childNodes.length;
            var i=0;
            while (i<l)
            {
                var iNode = ANode.childNodes[i];
                if (iNode.nodeType == 1)
                {
                    if (iNode.childElementCount==0 && iNode.attributes.length==0 || iNode.firstChild && iNode.firstChild.nodeType==3)
                    {
                        if (Array.isArray(AObject)) AObject.push(iNode.textContent);
                        else AObject[iNode.tagName] = iNode.textContent;
                    }
                    else
                    {
                        /*Создание массива или объекта для новой ноды*/
                        if (iNode.getAttribute('Type')=="Array") var iObject = new Array();
                        else var iObject = new Object();
                        /*Запись элемента как элемент массива или объект*/
                        if (Array.isArray(AObject)) AObject.push(iObject);
                        else AObject[iNode.tagName] = iObject;
                        /*Взов рекурсии*/
                        Recurs(iNode,iObject);
                    }
                }
                i++;
            }
            /*Сохранение атрибутов в текущий объект*/
            var l=ANode.attributes.length;
            var i=0;
            while (i<l)
            {
                var iAttribute = ANode.attributes[i];
                /*если это не ключ массива, тогда записываем атрибут*/
                if (iAttribute.name!='Type' && iAttribute.value!='Array') AObject[iAttribute.name] = decodeURIComponent(iAttribute.value);
                i++;
            }
        }
    }

    if (!AStartObject) AStartObject = new Object();
    Recurs(AStartNode, AStartObject);
    return AStartObject;
}










Element.prototype.CopyToClipboard = () =>
{
    if( document.selection )
    { 
        var Range = document.body.createTextRange();
        Range.moveToElementText( this );
        Range.select().createTextRange();
        document.execCommand( 'copy' ); 
    } 
    else 
    {
        if( window.getSelection )
        {
            var Range = document.createRange();
            Range.selectNode( this );
            window.getSelection().addRange( Range );
        }
        document.execCommand( 'copy' );
    }
    return this;
};









/**
 * Hell and terror from this and down
 */


/*
Определяем в где же хранится загруженный документ
*/
function clGetFrameDocument(AFrame)
{
var Result=null;
Result=AFrame.contentWindow.document;
return Result;
}



/*
Получить контент из фрэйма.
*/
function clGetFrameContent(AFrame)
{
var Result='';
var lDocument=clGetFrameDocument(AFrame);
if (lDocument && lDocument.body) Result=lDocument.body.innerHTML;
return Result;
}



/*
Получить контент из фрэйма по идентификатору контейнера.
*/
function clContentByID(AFrame, AObjectID, ADefault)
{
r=ADefault;
if (AFrame)
{
var e=clGetFrameDocument(AFrame).getElementById(AObjectID);
if (e) r=e.innerHTML;
}
return r;
}





/*
 * Возвращает размер документа
 */

function clGetSizeDocument()
{
    var r= new clVector();
    if (document.documentElement && document.documentElement.clientWidth && document.documentElement.clientHeight)
    {
        r.Set(document.documentElement.clientWidth, document.documentElement.clientHeight);
    }
    else
    {
        if (document.body) r.Set(document.body.clientWidth, document.body.clientHeight);
    }
    return r;
}



/*
 * Возвращает абсолютную позицию переданного объекта на экране с учетом скролеров
 */
function clGetObjectPos(AObject)
{
    var l = AObject.offsetLeft;
    var t = AObject.offsetTop;
    var p = AObject.offsetParent;

    while(p && p.tagName != "BODY")
    {
        l=l+p.offsetLeft - p.scrollLeft;
        t=t+p.offsetTop - p.scrollTop;
        p=p.offsetParent;
    }

    return new clVector().Set(l,t);
}



function clSetObjectPos(AObject, APos)
{
    AObject.style.left=APos.x+'px';
    AObject.style.top=APos.y+'px';
}



function clSetObjectTop(AObject, AValue)
{
    AObject.style.top=AValue+'px';
}



function clGetObjectTop(AObject)
{
    return parseInt(AObject.style.top);
}



function clGetObjectBottom(AObject)
{
 return parseInt(AObject.style.bottom);
}



function clSetObjectBottom(AObject, AValue)
{
 AObject.style.bottom=AValue+'px';
}



function clSetObjectWidth(AObject, AValue)
{
 AObject.style.width=AValue+'px';
}



function clGetObjectWidth(AObject)
{
 return AObject.offsetWidth;
}



function clGetObjectHeight(AObject)
{
 return AObject.offsetHeight;
}



function clGetObjectSize(AObject)
{
 return new clVector().Set(clGetObjectWidth(AObject), clGetObjectHeight(AObject));
}



function clSetObjectHeight(AObject, AValue)
{
 AObject.style.height=AValue+'px';
}



function clGetObjectLeftBottom(AObject)
{
 var p=clGetObjectPos(AObject);
 var h=clGetObjectHeight(AObject);
 return {"x":p.x, "y":p.y+h};
}



function clGetObjectRightTop(AObject)
{
 var p=clGetObjectPos(AObject);
 var l=clGetObjectWidth(AObject);
 return {"x":p.x+l, "y":p.y};
}






function clSetContentByID(AIDObject, AContent)
{
 var obj=document.getElementById(AIDObject);
 if (obj) obj.innerHTML = AContent;
}



function clSetValueByID(AIDObject, AContent)
{
 var obj=EBI(AIDObject);
 if (obj) obj.value = AContent;
}



function clScrollDown(AObj)
{
 if (AObj) AObj.scrollTop = AObj.scrollTop + AObj.clientHeight*0.7;
}



function clScrollUp(AObj)
{
 if (AObj) AObj.scrollTop = AObj.scrollTop - AObj.clientHeight*0.7;
}



function clScrollTop(AObj)
{
    if (AObj) AObj.scrollTop=0;
}



function clScrollBottom(AObj)
{
    if (AObj) AObj.scrollTop=AObj.scrollHeight;
}





function clCSSRuleByName(ASheet, AName, AResult)
{
 var i=0;
 var iRule;
 try
 {
        /*Это в FF вызывает исключение Unsecure потому пришлось завернуть в try*/
        var cl=ASheet.cssRules;
 } catch (e) {};
 if (cl)
 {
  var l=cl.length;
  while (i<l)
  {
   iRule=cl[i];
   if (iRule.selectorText==AName) AResult.push(iRule);
   i=i+1;
  }
 }

}



function clStyleByName( AName )
{
 var Sheets=document.styleSheets;
 var i=0;
 var iSheet;
 var r=new Array();
 while (i<Sheets.length)
 {
  iSheet=Sheets[i];
  clCSSRuleByName(iSheet, AName, r);
  i=i+1;
 }
 return r;
}



/*Проверяет нажате кнопки и возвращает true если кнопка нажата*/
function KeyControl(AEvent, AKeyCode)
{
 var KeyCode;
 if (AEvent.which) {KeyCode=AEvent.which} else {KeyCode=AEvent.keyCode};
 if (KeyCode==AKeyCode) AEvent.preventDefault();
 return (KeyCode==AKeyCode);
}



/*Получение значения радиокнопки по имени*/
function clGetRadioValueByName(AName)
{
 var r=null;
 var objList=document.getElementsByName(AName);
 for(var i = 0; i<objList.length; i++)
 {
  if(objList[i].checked) r=objList[i].value;
 }
 return r;
}


/*
 * Получение значения радиокнопки по имени
 */
function clSetRadioValueByName(AName, AValue)
{
 var r=null;
 var objList=document.getElementsByName(AName);
 for(var i = 0; i<objList.length; i++)
 {
  iRadio = objList[i];
  if (iRadio.id==AValue) iRadio.checked = true;
 }
 return r;
}






function clGetChildValues(AParent)
{

 function Recurs(RParent)
 {
  for (var i=0; i<RParent.childNodes.length; i++)
  {
   var iChild = RParent.childNodes[i];
   if (typeof iChild.value != "undefined") Result=Result+iChild.value+';';
   else Recurs(iChild);
  }
 }

 var Result = '';
 Recurs(AParent);
 return Result;
}





/*
 */


function clGetChildByClass(AParent, AClassName)
{
 var r=null;
 if (AParent.className!=AClassName)
 {
  for (var i=0; i<AParent.childNodes.length && r==null; i++)
  {
   r=clGetChildByClass(AParent.childNodes[i], AClassName);
  }
 }
 else r=AParent;
 return r;
}



function clGetChildByParent(AParent, ATagName)
{
 var r=null;
 if (AParent && AParent.tagName!=ATagName)
 {
  for (var i=0; i<AParent.childNodes.length && r==null; i++) r=clGetChildByParent(AParent.childNodes[i], ATagName);
 }
 else r=AParent;
 return r;
}



/*Принимает любой элемент AChild и возвращает элемент ATagName в которой он находится.*/
function clGetParentByChild(AChild, ATagName)
{
 while (AChild && AChild.tagName!=ATagName) AChild=AChild.parentNode;
 return AChild;
}



/*Принимает элемент AChild и возвращает элемент c классом AClassName в которой он находится.*/
function clGetParentByClass(AChild, AClassName)
{
    while (AChild && ! AChild.classList.contains( AClassName )) AChild=AChild.parentNode;
    return AChild;
}



/*
    Принимает элемент AChild и возвращает элемент c классом AClassName в которой он находится.
*/
function clGetParentByID(AChild, AID)
{
    while (AChild && AChild.id!=AID) AChild=AChild.parentNode;
    return AChild;
}





/*
    Move params from onbect in to DOM
*/
function clObjectToStrings( AObject, AArray )
{ 
    function Recurs( AObject, AArray, ADeep )
    {
        for( var Name in AObject )
        {
            switch( typeof( AObject[ Name ] ))
            {
                case 'object': Recurs( AObject[ Name ], AArray, ADeep + 1 ); break;
                case 'string': AArray.push( String.fromCharCode(9).repeat( ADeep ) + Name + ': ' + AObject[ Name ]); break;
            }
        }
    }

    Recurs(  AObject, AArray, 0 );
    return true;
}



/*
    Find childs by AValue and ASelector
*/
Element.prototype.ReplaceEventListener = function
(
    AEvent,     /* Event name */
    AFunction   /* Callback function */
)
{
    /* Remove */
    if( this.LastEventListener )
    {
        /* Remove previous listener */
        this.removeEventListener( AEvent, this.LastEventListener );
    }
    /* Store event listener for removing in future */
    this.LastEventListener = AFunction;
    /* Add new listener */
    this.addEventListener( AEvent, AFunction );
    return this;
};



/*
    Find parent
*/
Element.prototype.Parent = function
(
    AValue,     /* Finding value by selector */
    ASelector,  /* Selector name from const SELECTOR_* */
    ACall
)
{
    var Result = null;
    var c = this;
    if( ASelector == SELECTOR_TAG ) AValue = AValue.toUpperCase();
    while( c && !Result )
    {
        if
        (
            ASelector == SELECTOR_CLASS && c.classList.contains( AValue ) ||
            ASelector == SELECTOR_NAME  && c.name      == AValue ||
            ASelector == SELECTOR_TAG   && c.tagName   == AValue ||
            ASelector == SELECTOR_ID    && c.id        == AValue
        )
        {
            Result = c;
            if( ACall ) ACall( Result );
        }
        c = c.parentNode;
    }
    return Result;
};




/*
    Find parents
*/
Element.prototype.Parents = function
(
    AValue,     /* Finding value by selector */
    ASelector,  /* Selector name from const SELECTOR_* */
    ACallback   /* Cllback for each parent */
)
{
    var Result = [];
    var c = this;
    if( ASelector == SELECTOR_TAG ) AValue = AValue.toUpperCase();
    while( c )
    {
        if
        (
            ASelector == SELECTOR_CLASS && c.className && c.classList.contains( AValue ) ||
            ASelector == SELECTOR_NAME  && c.name      == AValue ||
            ASelector == SELECTOR_TAG   && c.tagName   == AValue ||
            ASelector == SELECTOR_ID    && c.id        == AValue
        )
        {
            Result.push( c );
        }
        c = c.parentNode;
    }
    if( ACallback ) Result.forEach( e => ACallback( e ) );
    return Result;
};



/*
    Find childs
*/
Element.prototype.Childs = function
(
    AValue,     /* Finding value by selector */
    ASelector,  /* Selector name from const SELECTOR_* */
    ACallback   /* Cllback for each parent */
)
{
    var Result = [];
    if( ASelector == SELECTOR_TAG ) AValue = AValue.toUpperCase();

    this.ChildRecursion
    ({
        OnBefore: e =>
        {
            if
            (
                ASelector == SELECTOR_CLASS && e.className && e.classList.contains( AValue ) ||
                ASelector == SELECTOR_NAME  && e.name      == AValue ||
                ASelector == SELECTOR_TAG   && e.tagName   == AValue ||
                ASelector == SELECTOR_ID    && e.id        == AValue
            )
            {
                Result.push( e );
            }
        }
    });

    if( ACallback ) Result.forEach( e => ACallback( e ) );
    return Result;
};

