class TScheduleFormFields
{  
    static INTERVAL_MORNING = '00:00 > 08:00';
    static INTERVAL_EVENING = '18:00 > 24:00';
    static INTERVAL_ALWAYS = '00:00 > 24:00';
    static INTERVAL_CLEAR = '';


    static Test( AApplication, AButton )
    {
        var Group = AButton.ParentByID( 'ScheduleGroup' );       
        AApplication.Post
        ({
            Call: 'Schedule.TestParams',
            Income: clValuesToArray( Group ),
            OnAfterLoad: Result => AApplication.Log.Inf(  Result.Result.Outcome.TestMoment + ' ' + Result.Result.Outcome.Confirm, true )
        });
    }


    static Fill( AApplication, AButton, AValues )
    {
        var Group = AButton.ParentByID( 'ScheduleGroup' );

        switch( AValues )
        {
            case 'Always':
            Group.ChildByID( 'MonInterval1', Element => Element.value = TScheduleFormFields.INTERVAL_ALWAYS );
            Group.ChildByID( 'MonInterval2', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'MonInterval3', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'MonInterval4', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'TueInterval1', Element => Element.value = TScheduleFormFields.INTERVAL_ALWAYS );
            Group.ChildByID( 'TueInterval2', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'TueInterval3', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'TueInterval4', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'WedInterval1', Element => Element.value = TScheduleFormFields.INTERVAL_ALWAYS );
            Group.ChildByID( 'WedInterval2', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'WedInterval3', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'WedInterval4', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'ThuInterval1', Element => Element.value = TScheduleFormFields.INTERVAL_ALWAYS );
            Group.ChildByID( 'ThuInterval2', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'ThuInterval3', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'ThuInterval4', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'FriInterval1', Element => Element.value = TScheduleFormFields.INTERVAL_ALWAYS );
            Group.ChildByID( 'FriInterval2', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'FriInterval3', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'FriInterval4', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'SatInterval1', Element => Element.value = TScheduleFormFields.INTERVAL_ALWAYS );
            Group.ChildByID( 'SatInterval2', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'SatInterval3', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'SatInterval4', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'SunInterval1', Element => Element.value = TScheduleFormFields.INTERVAL_ALWAYS );
            Group.ChildByID( 'SunInterval2', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'SunInterval3', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'SunInterval4', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            break;

            case 'Never':
            Group.ChildByID( 'MonInterval1', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'MonInterval2', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'MonInterval3', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'MonInterval4', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'TueInterval1', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'TueInterval2', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'TueInterval3', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'TueInterval4', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'WedInterval1', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'WedInterval2', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'WedInterval3', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'WedInterval4', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'ThuInterval1', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'ThuInterval2', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'ThuInterval3', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'ThuInterval4', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'FriInterval1', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'FriInterval2', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'FriInterval3', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'FriInterval4', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'SatInterval1', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'SatInterval2', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'SatInterval3', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'SatInterval4', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'SunInterval1', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'SunInterval2', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'SunInterval3', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'SunInterval4', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            break;

            case 'Night':
            Group.ChildByID( 'MonInterval1', Element => Element.value = TScheduleFormFields.INTERVAL_MORNING );
            Group.ChildByID( 'MonInterval2', Element => Element.value = TScheduleFormFields.INTERVAL_EVENING );
            Group.ChildByID( 'MonInterval3', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'MonInterval4', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'TueInterval1', Element => Element.value = TScheduleFormFields.INTERVAL_MORNING );
            Group.ChildByID( 'TueInterval2', Element => Element.value = TScheduleFormFields.INTERVAL_EVENING );
            Group.ChildByID( 'TueInterval3', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'TueInterval4', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'WedInterval1', Element => Element.value = TScheduleFormFields.INTERVAL_MORNING );
            Group.ChildByID( 'WedInterval2', Element => Element.value = TScheduleFormFields.INTERVAL_EVENING );
            Group.ChildByID( 'WedInterval3', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'WedInterval4', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'ThuInterval1', Element => Element.value = TScheduleFormFields.INTERVAL_MORNING );
            Group.ChildByID( 'ThuInterval2', Element => Element.value = TScheduleFormFields.INTERVAL_EVENING );
            Group.ChildByID( 'ThuInterval3', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'ThuInterval4', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'FriInterval1', Element => Element.value = TScheduleFormFields.INTERVAL_MORNING );
            Group.ChildByID( 'FriInterval2', Element => Element.value = TScheduleFormFields.INTERVAL_EVENING );
            Group.ChildByID( 'FriInterval3', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'FriInterval4', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'SatInterval1', Element => Element.value = TScheduleFormFields.INTERVAL_MORNING );
            Group.ChildByID( 'SatInterval2', Element => Element.value = TScheduleFormFields.INTERVAL_EVENING );
            Group.ChildByID( 'SatInterval3', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'SatInterval4', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'SunInterval1', Element => Element.value = TScheduleFormFields.INTERVAL_MORNING );
            Group.ChildByID( 'SunInterval2', Element => Element.value = TScheduleFormFields.INTERVAL_EVENING );
            Group.ChildByID( 'SunInterval3', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            Group.ChildByID( 'SunInterval4', Element => Element.value = TScheduleFormFields.INTERVAL_CLEAR );
            break;
        }
    }
}