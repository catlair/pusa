/*
TODO Remove this code !!!!!
*/


TDescript.prototype.AmountLoad = function( AParams )
{
    var that = this;

    if ( ! AParams ) AParams = {};
    if ( ! AParams.ID ) AParams.ID = this.ID;

    if ( AParams.RecordContainer )
    {
        cl.Post
        ({
            Call: 'Bill.SaleSelect',
            Income: AParams,
            OnAfterLoad: function( p )
            {
                if ( cl.PostResult( p ))
                {
                    /* Total amount */
                    if ( AParams.TotalAmountChargeContainer )
                    {
                         AParams.TotalAmountChargeContainer.innerHTML = p.Result.Outcome.TotalAmountChargeClear;
                    }

                    if ( AParams.TotalAmountChargeTaxContainer )
                    {
                         AParams.TotalAmountChargeTaxContainer.innerHTML = p.Result.Outcome.TotalAmountCharge;
                    }

                    if ( AParams.TotalAmountPayContainer )
                    {
                         AParams.TotalAmountPayContainer.innerHTML = p.Result.Outcome.TotalAmountPay;
                    }


                    /* Clear records */
                    AParams.RecordContainer.innerHTML = '';

                    /* Read content for record */
                    var RecordContent = decodeURIComponent( '<cl content="BillAmountRecord.html" pars="true" convert="URI"/>' );

                    /* Loop */
                    p.Result.Data.Records.Dataset.forEach
                    (
                        ARecord =>
                        {
                            var NewRecord = document.createElement( 'tr' );
                            NewRecord.innerHTML = clContentFromObject( ARecord, RecordContent );
                            AParams.RecordContainer.append( NewRecord );
                            AParams.ChildsByClass( 'BtnDelete', Btn => Btn.onclick = () => that.AmountDelete( ARecord.ID ) );
                        }
                    );
                }
            }
       });
    }
};



/*
    Export PDF
*/
TDescript.prototype.BillPDF = function()
{
    cl.PDF
    ({
        Income: { ID:this.ID },
        Call: 'Bill.PDF' 
    });
    return this;
};



/* 
    Recharge bill 
*/
TDescript.prototype.BillRecharge = function( AParams )
{
   if ( ! AParams ) AParams = {};
   if ( ! AParams.ID ) AParams.ID = this.ID;

   cl.Post
   ({
       Call: 'Bill.Recharge',
       Income: AParams,
       OnAfterLoad: function( p )
       {
           if ( cl.PostResult( p ))
           {
               if ( p.OnAfterRecharge )  p.OnAfterRecharge( p );
           }
       }
   });
};



TDescript.prototype.BillFillParams = function( AParams )
{
    if ( ! AParams ) AParams = {};
    if ( ! AParams.Income ) AParams.Income = {};
    if ( ! AParams.Income.ID ) AParams.Income.ID = this.ID;
   
    AParams.Call = 'Bill.CopyRequisites';
    AParams.OnAfterLoad = function()
    {
         if ( cl.PostResult( AParams ))
         {
             if ( AParams.OnAfterCopyRequisites ) AParams.OnAfterCopyRequisites( AParams );
         }
    };

    cl.Post( AParams );
    return this;
};



TDescript.prototype.CalcInfo = function( AParams )
{
   if ( ! AParams ) AParams = {};
   if ( ! AParams.ID ) AParams.ID = this.ID;

   cl.Post
   ({
       Call: 'Bill.CalcInfo',
       Income: AParams,
       OnAfterLoad: function( p )
       {
           if ( cl.PostResult( p ))
           {
               if ( AParams.OnAfterCalcInfo )  AParams.OnAfterCalcInfo( p );
           }
       }
   });
};



/*
    Pay for bill
*/
TDescript.prototype.BillPay = function( AParams )
{
    var that = this;
    cl.ScriptWait
    (
        ['clPay.js'],
        function()
        {
            var Pay = new TPay( cl ); 

            /* 
                Request on charged summ 
            */
            Pay.OnGetCharge = function( p )
            {
                that.CalcInfo
                ({
                    /* Событие после информации */
                    OnAfterCalcInfo: function( ACalcInfo )
                    {
                        /* Проверяем событие успешного получения сумм оплаты и начисления */
                        if ( p.OnGetChargeSucess ) 
                        {
                            /* Информируем форму платежей о полученных суммах */
                            p.OnGetChargeSucess
                            ({
                                ChargeAmount: ACalcInfo.Result.Outcome.TotalChargeWithTax,
                                PayAmount: ACalcInfo.Result.Outcome.TotalPay
                            });
                        }
                    }
                });
            };

            /* Action on pay */
            Pay.OnPay = function( AIDOperation )
            {
                that.BillPayOperation
                ({                   
                    IDOperation: AIDOperation,
                    Amount: Pay.Income,
                    OnAfterPayOperation: function( p )
                    {
                        /* Закрытие диалога оплаты после успешной оплаты */
                        Pay.Close();
                    }
                });
            };

            var Window = Pay.Popup
            ({              
                Caption: that.ID,
                ID: that.ID
            });
        }
    );

    return this;
};



/*
    Фактическое выполнение операции оплаты на сервере
*/
TDescript.prototype.BillPayOperation = function( AParams )
{
   var Params = 
   {
       ID: AParams.ID ? AParams.ID : this.ID,
       IDOperation: AParams.IDOperation,
       Amount: AParams.Amount,
   };

   cl.Post
   ({
       Call: 'Bill.Pay',
       Income: Params,
       OnAfterLoad: function( p )
       {
           if ( cl.PostResult( p ))
           {
               if ( AParams.OnAfterPayOperation ) AParams.OnAfterPayOperation( p );
           }
       }
   });
};


