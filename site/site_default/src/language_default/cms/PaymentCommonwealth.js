/*
  Catlair PHP Copyright (C) 2019  a@itserv.ru
 
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 
 
  TPay interface.
  Support provider: commonwealth
*/



class TPaymentCommonwealth extends TPay
{
    static Create( AApplication )
    {
        var Result = new TPaymentCommonwealth( AApplication );
        return Result;
    }



    /*
        This method can call ScriptLoad for assinchrone calls
    */
    Init( AOnInit )
    {
        if( AOnInit ) AOnInit( this );
        return this;
    }



    /*
    */
    PayBegin( AParams )
    {
        var Content = decodeURIComponent( '<cl content="PaymentCommonwealthPayFrom.html" pars="true" convert="URI"/>' );
        this.PopupPayForm( AParams, clContentFromObject( this.GetParams(), Content ));
        return this;
    }



    Pay( AParams )
    {
        this.SendPay( AParams );
        return this;
    }


    PayEnd( AParams )
    {
        /* Close popup dialig */
        if( AParams.Popup ) AParams.Popup.Close();
        if( AParams.OnAfterPay ) AParams.OnAfterPay( AParams );
        return this;
    }

}