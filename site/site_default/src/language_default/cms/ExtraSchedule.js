class TExtraSchedule
{
    constructor( AApplication )
    {
        this.Application = AApplication;
    }



    static Create( AApplication )
    {
        return new TExtraSchedule( AApplication );
    }



    Popup( AParams )
    {
        var Content = decodeURIComponent( '<cl content="ExtraSchedulePopup.html" pars="true" convert="URI"/>' );
        var Popup = this.Application.Popup({ TypeContent:tcText, Content:Content, FullScreen:true });

        Popup.ChildsByClass( 'BtnCancel', Btn => Btn.onclick = () => Popup.Close() );


        Popup.ChildsByClass
        (
           'Item',
           Btn => Btn.onclick = () => this.Confirm
           (
               Btn.getAttribute( 'time' ), 
               AParams,
               () => Popup.Close() 
           )
        );

        return this;
    }



    SendRequest( AParams, AOnAfterRequest )
    { 
        TRequest
        .Create( this.Application )
        .SetParams( AParams )
        .Execute
        ({
            Name: 'Descripts.ExtraSchedule',
            OnAfterExecute: Request =>
            {
                if( AOnAfterRequest ) AOnAfterRequest();
            }
        });

        return this;
    }
         


    Confirm( AValue, AParams, AOnConfirm )
    {
        var Popup = this.Application.Popup
        ({
            TypeContent: tcText,
            Content: decodeURIComponent( '<cl content="ExtraScheduleConfirm.html" pars="true" convert="URI"/>' ),
        });

        Popup.ChildsByClass( 'BtnCancel', Btn => Btn.onclick = () => Popup.Close() );

        Popup.ChildsByClass
        (
            'BtnArm',
            Btn => Btn.onclick = () => this.SendRequest
            (
                {
                    Operation:'Include',
                    Timeout: AValue,
                    IDType: AParams.IDType
                }, 
                () => 
                {
                    Popup.Close();
                    if( AOnConfirm ) AOnConfirm();
                }
            )
        );

        Popup.ChildsByClass
        (
            'BtnDisarm',
            Btn => Btn.onclick = () => this.SendRequest
            (
                {
                     Operation:'Exclude',
                     Timeout: AValue,
                     IDType: AParams.IDType
                }, 
                () => 
                {
                    Popup.Close();
                    if( AOnConfirm ) AOnConfirm();
                }
            )
        );
        return this;
    }

}