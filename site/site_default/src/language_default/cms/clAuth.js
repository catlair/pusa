/*
    Catlair JS
    Набор функций расширений для работы с интерфейсами авторизации Catlair
    still@itserv.ru
*/



/*
    функциия вывода диалога о правах пользователя
*/
TCatlair.prototype.SessionPopup = function()
{
    var that = this;
    var lContent = decodeURIComponent('<cl content="SessionInfoForm.html" convert="URI"/>');
    this.SessionInfo
    (
        {
            OnAfterInfo: function(AParam)
            {
                lContent = clContentFromObject(AParam.Result.Outcome, lContent);
                that.Popup({TypeContent:tcText, Content:lContent});
            }
        }
    );
    return this;
};



/*
    функциия вывода диалога о правах пользователя
*/
TCatlair.prototype.UserPopup = function()
{
    var that = this;
    var lContent = decodeURIComponent('<cl content="SessionInfoUserForm.html" convert="URI"/>');
    this.SessionInfo
    (
        {
            OnAfterInfo: function(AParam)
            {
                /* Построение контента */
                lContent = clContentFromObject(AParam.Result.Outcome, lContent);
                var Popup = that.Popup({TypeContent:tcText, Content:lContent});
                /*Создание действия кнопки ОК*/
                var BtnLogout = Popup.ChildByID('BtnLogout');
                BtnLogout.onclick = function()
                {
                    that.Logout({});
                    Popup.Close();
                };

                /*Создание действия кнопки Cancel*/
                var BtnCancel = Popup.ChildByID('BtnCancel');
                BtnCancel.onclick = function()
                {
                    Popup.Close();
                };
            }
        }
    );
    return this;
};



/*
    функция проверки сесионных данных
    AParams.OnAfterLoad - on after load
*/
TCatlair.prototype.SessionInfo = function( AParams )
{
    var that = this;
    if( ! AParams ) AParams = {};
    AParams.Call = 'Session.Info';
    AParams.OnAfterLoad = function( p )
    { 
        if ( that.PostResult( p ) )
        {
            that.SessionParam = p.Result.Outcome;
            if ( AParams.OnAfterInfo ) AParams.OnAfterInfo( p );
        }
    };
    this.Post( AParams );
    return this;
};



/*
    функция проверки является ли пользователь авторизованным
*/
TCatlair.prototype.AuthorizeStatus = function( AParams )
{
    var that = this;
    this.SessionInfo
    ({
        OnAfterInfo: function( p )
        {
            that.MomentFormat = p.Result.Outcome.MomentFormat;
            if ( p.Result.Outcome.Auth == 'true')
            {
                /* Пользователь является авторизованным */
                if ( AParams.OnUser ) AParams.OnUser( AParams );
            }
            else
            {
                /* Пользователь не авторизован */
                if ( AParams.OnGuest ) AParams.OnGuest( AParams );
            }
        }
    });
    return this;
};



/*
*/
TCatlair.prototype.TransparentAuthorization = function( AParams )
{
    /* Вызов на случай если пользовать уже авторизован */
    AParams.OnUser = () =>
    {
        if (AParams.OnAuth) AParams.OnAuth( AParams );
    };

    AParams.OnGuest = () =>
    {
        /* Вызов на случай если прользователя необходимо авторизовать */
        this.Authorization
        ({
            FullScreen: true,
            OnAfterLogin: () =>
            {
                if( AParams.OnAuth ) AParams.OnAuth( AParams );
            }
        });
    };

    this.AuthorizeStatus( AParams );
};



/*
    Вызов формы авторизации
    AParams.OnAfterLogin - функция обратного вызова при успешной авторизации
*/
TCatlair.prototype.Authorization = function(AParams)
{
    /*контроль парметров*/
    if (!AParams.Content) AParams.Content = decodeURIComponent('<cl content="AuthForm.html" convert="URI"/>');


    if (!AParams.TypeContent) AParams.TypeContent = tcText;

    var Popup = this.Popup(AParams); /*Создание формы Popup*/

    var LoginParams = {};
    LoginParams.Form = Popup.ChildByTag('FORM');

    /*Обработка успешной авторизации*/
    LoginParams.OnAfterLogin =function()
    {
        Popup.Close();
        if (AParams.OnAfterLogin) AParams.OnAfterLogin(AParams);
    };

    /*Создание действия кнопки ОК*/
    Popup.ChildByID( 'BtnOk', Btn => Btn.onclick = () => this.Login( LoginParams ));

    /*Создание действия кнопки Cancel*/
    Popup.ChildByID( 'BtnCancel', Btn => Btn.onclick = () => Popup.Close() );

    /*Создание действия кнопки Cancel*/
    Popup.ChildByID( 'BtnResetPassword', Btn => Btn.onclick = () => this.ResetPassword( LoginParams ));

    /* Check ENTER button on password */
    Popup.ChildByID
    (
        'Pass', Element => Element.onkeyup = AEvent =>
        {   
            if( AEvent.keyCode == 13 ) this.Login( LoginParams );
        }
    );


    return this;
};



/*
    Log in user by password
*/
TCatlair.prototype.Login = function(AParams)
{
    var Params =
    {
        Pars:        true,
        Form:        AParams.Form,
        Call:        'Session.Login',
        OnAfterLoad: Result =>
        {
            if( this.PostResult( Result ) && AParams.OnAfterLogin ) AParams.OnAfterLogin( AParams );
        }
    };
    this.Post( Params );
    return this;
};



/*
    Log in user by password
*/
TCatlair.prototype.ResetPassword = function( AParams )
{
    var Params =
    {
        Pars:        true,
        Form:        AParams.Form,
        Call:        'Guest.ResetPasswordRequest',
        OnAfterLoad: Result =>
        {
            if( this.PostResult( Result ) && AParams.OnAfterLogin )
            {
                 this.Confirm({ Buttons:[ BTN_OK ], FullScreen:true, Template:'ResetPasswordNotify.html' });
            }
        }
    };
    this.Post( Params );
    return this;
};




/*
    Log out user. Close current session.
*/
TCatlair.prototype.Logout = function(AParams)
{
    var that = this;
    AParams.Pars = true;
    AParams.Call = 'Session.Logout';
    AParams.OnAfterLoad = function()
    {
        if (that.PostResult(AParams) && AParams.OnAfterLogout) AParams.OnAfterLogout(AParams);
    };
    this.Post(AParams);
    return this;
};



/**
 * Процедура переключения языка
 */
TCatlair.prototype.Lang = function(AParams)
{
    var that = this;
    Params =
    {
        Pars: true,
        Call: 'Session.Lang',
        Income: 
        {
            IDLanguage: AParams.IDLang,
            TypeContent: tcJSON
        },
        OnAfterLoad: function()
        {
            if (AParams.OnAfterLogout) AParams.OnAfterLogout(AParams);
            else location.reload();
        }
    };
    this.Post(Params);
    return this;
};



/*
    Процедура смены пароля
*/
TCatlair.prototype.ChangePassword = function( AParams )
{
    var that = this;

    AParams.Call = 'Session.ChangePassword';

    AParams.Income = 
    {
        CurrentPassword: AParams.Form.CurrentPassword.value,
        NewPassword: AParams.Form.NewPassword.value,
        CheckPassword: AParams.Form.CheckPassword.value
    };

    AParams.OnAfterLoad = function( p )
    {
console.log( p );
        if ( that.PostResult( p ))
        {
            if (AParams.OnSuccess) AParams.OnSuccess(AParams);
        }
    };

    this.Post(AParams);
    return this;
};




/*
    Send Event or visit to server
    ID
*/
TCatlair.prototype.Visit = function( AParams )
{
    var that = this;
    this.Post
    ({  
        Call : 'Visit.Create',
        Income : AParams,
        OnAfterLoad : function( p )
        {
            if (that.PostResult( p ))
            {
                if ( AParams.OnSuccess ) AParams.OnUser( OnSuccess );
            }
        }
    });
    return this;
};



/*
    Процедура смены пароля
*/
TCatlair.prototype.ChangePasswordPopup = function(AParams)
{
    var that = this;

    /*контроль парметров*/
    if (!AParams.Content) AParams.Content = decodeURIComponent('<cl content="PasswordChangeForm.html" convert="URI"/>');
    if (!AParams.TypeContent) AParams.TypeContent = tcText;

    var Popup = this.Popup(AParams); /*Создание формы Popup*/

    var AParams = 
    {
        Form: Popup.ChildByID('Form'),
        OnSuccess: function()
        {
           Popup.Close();
        }
    };

    /*Создание действия кнопки Cancel*/
    Popup.ChildByID('BtnCancel').onclick = function()
    {
        Popup.Close();
    };

    /*Создание действия кнопки ОК*/
    Popup.ChildByID('BtnOk').onclick = function()
    { 
        that.ChangePassword( AParams );
    };


    /* Check ENTER button on password */
    Popup.ChildByID('NewPassword').onkeyup = function( AEvent )
    {
        if (AEvent.keyCode==13) that.ChangePassword(Params);
    };

    Popup.ChildByID('CheckPassword').onkeyup = function(AEvent)
    {
        if (AEvent.keyCode==13) that.ChangePassword(Params);
    };

    return this;
};
