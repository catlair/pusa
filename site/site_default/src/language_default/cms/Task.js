/*
 * Task object
 */
function TTask()
{
    this.ID = null;
    this.IDType = 'Task';
    return this;
}

/* Inherites from TDescript */
Object.setPrototypeOf(TTask.prototype, TDescript.prototype);




TTask.prototype.EditSpecial = function(AParams)
{
    var that = this;
    /* Создаем окно */
    if (!cl.WinControl) cl.WinControl = new clWinControl();
    this.Window = cl.WinControl.WindowCreate();

    /* Знакомим окно с задачей */
    this.Window.Task = this;
    this.Window.SetCaption(this.ID);
    this.Window.SetToolbar(decodeURIComponent('<cl content="TaskEditToolbar.html" pars="true" convert="URI"/>'));
    this.Window.Icon.SetParams({Caption:that.ID, Group:'Task', GUIDImage:''});

    /* вывод контента в форму */
    this.Window.SetContent(decodeURIComponent('<cl content="TaskEditForm.html" pars="true" convert="URI"/>'));

    /*
     * Описание формы обновления дескрита. Заменяет контент на форме
     */
    this.Window.ObjectToForm = function()
    {
        if (!that.Data)
        {
            that.Data =
            {
                Detale:
                {
                    Post:
                    {
                        /*Identify values*/
                        ID:that.ID,
                        IDType: that.IDType,
                        IDParent: AParams.IDParent,
                        /*Main values*/
                        Caption:'',
                    }
                },
                Params:
                {
                    /*Specific values*/
                    Tag:'',
                    IDPriority:'PriorityNormal',
                    IDStatus: 'StatusNew',
                }
            }
            var CaptionIcon = TaskLangPack.CaptionNewTask;
        }
        else var CaptionIcon = that.Data.Outcome.Caption;

        that.Window.Icon.SetParams
        ({
            Caption:CaptionIcon,
            Group:TaskLangPack.CaptionGroupWindow,
            GUIDImage:TaskLangPack.IDImageWindow
        });

        /* Put values to from */
        clValuesFromObject(that.Data.Detale.Post, that.Window.Control);
        clValuesFromObject(that.Data.Outcome, that.Window.Control);
        clValuesFromObject({IDLabel:that.ID}, that.Window.Control);
        /* Activate popup controls*/
        cl.SelectActivate(that.Window.Control);
    }



    this.Window.MessageRefresh = function(AParams)
    {
        if (that.ID == null)
        {
        }
        else
        {
            /*Построение списка сообщений*/
            var Messages = new TDescripts();
            Messages.RecordConteiner = that.Window.ChildByID('MessageList');
            Messages.RecordContent = decodeURIComponent('<cl content="MessageRecordSimple.html" pars="true" convert="URI"/>');
            Messages.OnAfterLoadRecord = function (AConteiner)
            {

            }

            Messages.Load
            ({
                IDParent:that.ID,
                Library: 'message_load',
                ProcName: 'MessageLoad',
                RecordCount:10,
                RecordCurrent:0
            });
        }
    }



    this.Window.Refresh = function(AParams)
    {
        if (that.ID == null)
        {
            /* Спрятали кнопки управления дескриптом */
            that.Window.ChildByID('BtnRefresh').Show();
            that.Window.ChildByID('BtnSave').Show();
            that.Window.ChildByID('BtnDelete').Hide();

            /* Закидываем параметры в форму */
            that.Window.ObjectToForm();
        }
        else
        {
            /* Пытаемся загрузить запись*/
            that.Load
            ({
                ID: that.ID,
                OnAfterLoad: function(p)
                {
                    if (cl.PostResult(p))
                    {
                        that.Window.ChildByID('BtnRefresh').Show();
                        that.Window.ChildByID('BtnSave').Show();
                        that.Window.ChildByID('BtnDelete').Show();
                    }
                    else
                    {
                        that.Window.ChildByID('BtnSave').Hide();
                        that.Window.ChildByID('BtnRefresh').Show();
                        that.Window.ChildByID('BtnDelete').Hide();
                    }
                    that.Window.ObjectToForm();
                    that.Window.MessageRefresh();
                }
            });
        }
    }



    /**
     * Процедура сохранения формы
     */
    this.Window.Save = function()
    {
        var Param=
        {
            Pars:true,
            Form: that.Window.ChildByID('Form'),
            OnAfterLoad: function(p)
            {
                if (cl.PostResult(p))
                {
                    that.ID = p.Result.Outcome.ID;
                    that.Window.Refresh();
                    that.Window.SendMessage();
                }
            }
        };


        if (that.ID!=null)
        {
            /*ID существует значит Update*/
            Param.Library = 'descript_update';
            Param.ProcName = 'DescriptUpdate';
        }
        else
        {
            /*ID не существует значит Create*/
            Param.Library = 'descript_create';
            Param.ProcName = 'DescriptCreate';
        }

        /*Процедура после сохранения*/
        cl.Post(Param);
    }



    /*
     * Функция удаления дескрипта
     */
    this.Window.Delete = function()
    {
        that.Delete
        (
            {
                OnAfterDelete:function(){that.Window.Close()}
            }
        );
    }



    /**
     * Процедура сохранения сообщения
     */
    this.Window.SendMessage = function()
    {
        var FormMessage = that.Window.ChildByID('FormMessage');
        if (that.ID!=null && FormMessage.Message.value!='')
        {
            var MaxLen= 25;
            FormMessage.Caption.value = FormMessage.Message.value.substr(0,MaxLen);
            if (FormMessage.Message.value.length>MaxLen) FormMessage.Caption.value += '...';
            /*Процедура после сохранения*/
            cl.Post
            ({
                Pars:true,
                Library:'descript_create',
                ProcName:'DescriptCreate',
                URL:'&IDParent='+that.ID+'&IDType=Message',
                Form: FormMessage,
                OnAfterLoad: function(p)
                {
                    if (cl.PostResult(p))
                    {
                        FormMessage.Message.value='';
                    }
                }
            });
        }
    }


    /* Дейставия на кнопки */
    this.Window.ChildByID('BtnRefresh').onclick = this.Window.Refresh;
    this.Window.ChildByID('BtnSave').onclick = this.Window.Save;
    this.Window.ChildByID('BtnDelete').onclick = this.Window.Delete;

    /*Обновление дескрипта*/
    this.Window.Refresh();

    return this;
}


var TaskLangPack = <cl content="TaskLanguagePack.js" pars="true"/>;
