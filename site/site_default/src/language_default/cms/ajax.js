/*
    AJAX library
*/


/* Умолчальный путь для запроса шаблонов cl*/
var Requests = [];

/* Type content */
var tcXML   = 'XML';
var tcHTML  = 'HTML';
var tcJSON  = 'JSON';
var tcText  = 'Text';


var Cache = [];

/*
    Send Post
*/
function clPost( APrm )
{
    /* Создание формы если отсутствует */
    if( !APrm.FormData ) APrm.FormData = APrm.Form ? new FormData( APrm.Form ) : new FormData();

    /* Создание URL */
    var URL = null;
    if( APrm.URL && APrm.URL.indexOf( 'http' ) === 0 )
    {
        /* External link */
        URL = APrm.URL;
    }
    else
    {
        /* Check URLParams existing */
        var URLParams = APrm.URLParams ? APrm.URLParams : [];

        if( APrm.URL ) 
        {
            /* Remove first ? */
            var URL = ( APrm.URL.indexOf( '?' ) === 0 ) ? APrm.URL.substring( 1 ) : APrm.URL;
            var Params = URL.split( '&' );
            Params.forEach
            ( 
                Value => 
                {
                    var Param = Value.split( '=', 2 );
                    URLParams[ Param[ 0 ]] = decodeURIComponent( Param[ 1 ] );
                }
            );
        }


        if( APrm.Call )
        {
            /* Call controller method */
            URLParams[ 'call' ] = APrm.Call;

            APrm.Income = APrm.Income ? APrm.Income : {};          
            if( !APrm.Income.TypeContent ) APrm.Income.TypeContent = APrm.TypeContent;
            if( !APrm.Income.TypeContent ) APrm.Income.TypeContent = tcJSON;
            for( var Key in APrm.Income ) APrm.FormData.append( Key, APrm.Income[ Key ]);
        }
        else
        {
            if( APrm.Template ) URLParams[ 'template' ] = APrm.Template;
        }


        if( URLParams[ 'pusa' ] )
        {
            APrm.Income.TypeContent = tcJSON;
            for( var Key in APrm.Income ) APrm.FormData.append( Key, APrm.Income[ Key ]);
        }


        var Params = [];
        for( var Key in URLParams )
        {
            Params.push( Key + '=' + encodeURIComponent( URLParams[ Key ] ));
        }

        URL = Params.length > 0 ? '?' + Params.join( '&' ) : null ;
    }

    var IDGroup = APrm.IDGroup ? APrm.IDGroup : clGUID();
    var IDGroupFull = IDGroup + clGUID();

    /* Close connections by ID Group */
    clRequestsClose( IDGroup ); 

    /* Create the Request */
    var Request = clRequestNew( IDGroupFull );

    /* устанавливаем процедуры обратного вызова */
    Request.onloadend = AResult =>
    {
        if( APrm.IDCache && APrm.IDCache ) Cache[ APrm.IDCache ] = AResult;
        
        if( APrm.Log ) APrm.Log.JobEnd();

        if( !APrm.Stoped )
        {
            /* Get pure content */
            APrm.Content=AResult.target.response;

            /* Define type content */
            var TypeContent = APrm.Income && APrm.Income.TypeContent ? APrm.Income.TypeContent : tcHTML;

            switch ( TypeContent )
            {
                case tcJSON:
                    try
                    {
                        APrm.Result = JSON.parse( APrm.Content );
                    }
                    catch (e)
                    {
                        var Error ='Error in JSON result for URL [' + URL + ']';
                        console.warn( Error, APrm.Content );
                    }
                break;

                case tcXML:
                    if ( AResult.target && AResult.target.responseXML && AResult.target.responseXML.documentElement )
                    {
                        try
                        {
                            APrm.Result = clDOMToObject(AResult.target.responseXML.documentElement)
                        }
                        catch (e)
                        {
                        }
                    }
                    if (!APrm.Result)
                    {
                        var Error ='Error in XML result for URL [' + URL + ']';
                        if (APrm.Log) APrm.Log.Err(Error, true);
                        console.warn(Error);
                        console.warn(APrm.Content);
                    }
                break;
            }

            /* Автоматическая проверка успешного вызова при наличии Header */
            if ( APrm.OnSucessLoad && APrm.Result && APrm.Result.Header && clPostResult( APrm ))
            {
                 APrm.OnSucessLoad( APrm );
            }

            /*обработка пользовательской функции после загрузки при наличии*/
            if( APrm.OnAfterLoad ) APrm.OnAfterLoad( APrm );
       }
       clRequestRemove( this );
    };



    Request.upload.onprogress = AEvent =>
    {
        if( APrm.OnUploadProgress ) APrm.OnUploadProgress( AEvent );
    };



    /*
        обработчик ошибки вызова
    */
    Request.upload.onerror = AResult =>
    {
        if( APrm.Log )
        {
            APrm.Log.JobEnd();
            APrm.Log.Err('Post error', true);
        }
        clRequestRemove( this );
    };



    /*обработчик отмены вызова*/
    Request.onabort = () =>
    {
        APrm.Aborted = true;
        APrm.Stoped = true;

        this.Aborted = true;

        if( APrm.OnAbort ) APrm.OnAbort( APrm );

        clRequestRemove( this );
    };



    /* Выполнение события до загрузки если оно назначено */
    if (APrm.OnBeforeLoad) APrm.OnBeforeLoad( APrm );

    console.info( 'AJAX:' + URL, 'IDGroup:' + IDGroupFull );

    APrm.Stoped = false;

    /* Create cache ID */
    if( APrm.Cache ) APrm.IDCache = APrm.Cache + URL;
    if( APrm.IDCache && Cache[ APrm.IDCache ] )
    {
        Request.onloadend( Cache[ APrm.IDCache ] );
    }
    else
    {
        if ( APrm.Log ) APrm.Log.JobBegin();
        Request.open( 'POST', URL );
        Request.send( APrm.FormData );
    }

    return Request;
};



function clPostStop( ARequest )
{
    ARequest.abort();
    return this;
}



/*
    Create new XMLHTTPRequest and store it in to global array Requests
*/
function clRequestNew( AIDGroup )
{
    var Result = new XMLHttpRequest();
    Result.IDGroup = AIDGroup;
    Requests.push( Result );
    return Result;
}



/*
    Create new XMLHTTPRequest and store it in to global array Requests
*/
function clRequestRemove( ARequest )
{
    var RequestIndex = Requests.indexOf( ARequest );
    Requests.splice( RequestIndex, 1 );
}



/*
    Return list of Requests
*/
function clRequestsByIDGroup( AIDGroup )
{
    var Result = [];

    for( Request of Requests )
    {
        if( Request.IDGroup.match( AIDGroup ) != null ) Result.push( Request );
    }

    return Result;
}



/*
    Close requests by IDGroup
*/
function clRequestsClose( AIDGroup )
{
    var Requests = clRequestsByIDGroup( AIDGroup );
    for( Request of Requests ) Request.abort();
    return true;
}



/*
    Check Post Resilt
*/
function clPostResult( APrm, AHide )
{
    var Result = false;
    switch( APrm.Income.TypeContent )
    {
        case tcJSON:
        case tcXML:
            if( APrm.Result && APrm.Result.Header )
            {
                if ( APrm.Result.Header.Code == 'Ok' ) Result = true;
                else if (APrm.Log) APrm.Log.War( decodeURIComponent( APrm.Result.Header.Message ), true);
            }
            else
            {
                if(APrm.Log && ! APrm.Aborted && AHide !== false ) APrm.Log.War( 'Unknown answer. Check your connection.', true );
            }
        break;

        default:
            /* Source content */
            Result = true;
        break;
    }
    return Result;
}




function clNotEmpty()
{ 
    var Result = null;
    var c = arguments.length;
    for( var i = 0; i < c && Result == null; i++ )
    {
        if( arguments[ i ] ) Result = arguments[ i ];
    }
    return Result;
}
