class TDescriptExtraSchedule
{
    static ExtraInclude( ADescript, AParams )
    {
        var Application = ADescript.Application;

        /* Create form and popup */
        var Popup = cl.Popup
        ({
            TypeContent: tcText,
            Content: decodeURIComponent( '<cl content="DescriptExtraIncludeForm.html" pars="true" convert="URI"/>' )
        });


        Popup.ChildsByClass
        ( 
            'BtnCancel', Btn => Btn.onclick = () => Popup.Close()
        );

        Popup.ChildsByClass
        ( 
            'BtnOk', Btn => Btn.onclick = () =>
            {
                TRequest.Create( Application )
                .SetParams
                ({
                    ID: ADescript.ID,
                    Timeout: Popup.ChildByID( 'Timeout' ).value
                })
                .Execute
                ({
                    Name:'Descript.ExtraScheduleInclude',
                    OnAfterExecute: () =>
                    {
                        if( AParams.OnExtraScheduleInclude ) AParams.OnExtraScheduleInclude( AParams );
                        Popup.Close();
                    }                
                })
            }
        );
    }



    static ExtraExclude( ADescript, AParams )
    {
        var Application = ADescript.Application;

        /* Create form and popup */
        var Popup = cl.Popup
        ({
            TypeContent: tcText,
            Content: decodeURIComponent( '<cl content="DescriptExtraExcludeForm.html" pars="true" convert="URI"/>' )
        });

        Popup.ChildsByClass
        ( 
            'BtnCancel', Btn => Btn.onclick = () => Popup.Close()
        );

        Popup.ChildsByClass
        ( 
            'BtnOk', Btn => Btn.onclick = () =>
            {
                TRequest.Create( Application )
                .SetParams
                ({
                    ID: ADescript.ID,
                    Timeout: Popup.ChildByID( 'Timeout' ).value
                })
                .Execute
                ({
                    Name:'Descript.ExtraScheduleExclude',
                    OnAfterExecute: () =>
                    {
                        if( AParams.OnExtraScheduleExclude ) AParams.OnExtraScheduleExclude( AParams );
                        Popup.Close();
                    }                
                })
            }
        );
    }

}






