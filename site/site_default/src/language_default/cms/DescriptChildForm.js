class TDescriptChildForm
{
    static Popup( ADescript, AParams )
    {
        var Application = ADescript.Application;

        /* Get form content and pop it in form */
        var Content = decodeURIComponent( '<cl content="FormDescriptBind.html" pars="true" convert="URI"/>' );
        Content = clContentFromObject( ADescript.Record, Content );
        var Popup = Application.Popup({ ID:'BindAddForm', TypeContent:tcText, Content:Content });

        /*Активация всех элементов выбора окна*/
        Application.SelectActivate( BindAddForm );

        var InputParent  = BindAddForm.ChildByID( 'IDParent' );
        var InputBind    = BindAddForm.ChildByID( 'IDBind' );
        var InputChild   = BindAddForm.ChildByID( 'IDChild' );

        /* Set value for popups */
        InputParent.Refresh( ADescript.ID );
        InputBind.Refresh( AParams.IDBind );
        InputChild.Refresh( '' );

        /* Get elements */
        BindAddForm.ChildByID( 'BtnDirect',  Btn => Btn.onclick = () => Popup.Direct() );
        BindAddForm.ChildByID( 'BtnCancel',  Btn => Btn.onclick = () => Popup.Close() );
        BindAddForm.ChildByID( 'BtnAdd',     Btn => Btn.onclick = () => Popup.Add() );
        BindAddForm.ChildByID( 'BtnRefresh', Btn => Btn.onclick = () => Popup.Refresh() );



        Popup.Refresh = () =>
        {
            TDOMDataset.Create( Application )
            .SetParam( 'IDParent', InputParent.value )
            .SetParam( 'IDBind', InputBind.value )
            .Load
            ({
                Name:             'Descript.ChildBinds',
                ParentContainer:  Popup.ChildsByClass( 'List' )[ 0 ],
                RecordContent:    decodeURIComponent( '<cl content="DescriptChildFormRecord.html" pars="true" convert="URI"/>' ),
                OnAfterRecord:    ( Container, Record ) => 
                {
                     if( AParams.OnBindClick )
                     {
                          Container.ChildsByClass
                          (
                              'CaptionBind', Element => 
                              {
                                  Element.classList.add( 'Unselected' );
                                  Element.onclick = () => AParams.OnBindClick( Record.IDBind, Popup );
                              }
                          );
                     }
                }
            });
        };



        Popup.Direct = () =>
        {
             var p = InputParent.value;
             InputParent.Refresh( InputChild.value );
             InputChild.Refresh( p );
        };



        Popup.Add = () =>
        {
             var Child = new TDescript();
             Child.Place
             ({
                 IDTo:         InputParent.value,
                 IDBind:       InputBind.value,
                 ID:           InputChild.value,
                 OnAfterPlace: () =>
                 {
                     Popup.Refresh();
                     if ( AParams.OnAfterPlace ) AParams.OnAfterPlace( AParams );
                 }
             });
        };

        Popup.Refresh();
    }
}