/******************************************************************************
* Ubload files
* Catlair JS
*/

const FILE_STATUS_WAIT = 'Wait';
const FILE_STATUS_UPLOADING = 'Uploading';
const FILE_STATUS_OK = 'Ok';
const FILE_STATUS_ERROR = 'Error';



class TFileUpload
{
    /*
    Constructor
    */
    constructor(ALog)
    {
        /* установка базовых параметров */
        this.FLog = null;
        this.FParams = null;
        this.FLog = ALog;
        this.FFiles = null;
    }



    /*
        Upload files from object AFiles
    */
    Start(AParams)
    {
        this.FParams = AParams;
        var NextFile = this.GetNextFile();
        if (NextFile != null )
        { 
            this.Upload (NextFile);
        }
        return this;
    }



    /*
    Get next file or return null
    */
    GetNextFile()
    {
        var Result=null;
        if (this.FParams.Files !== null)
        {
            var c=this.FParams.Files.files.length;
            var i=0;
            while (i<c && Result==null)
            {
                var iFile = this.FParams.Files.files[i];
                if (!iFile.Status) Result = iFile;
                i++;
            }
        }
        /* Clear element from files */
        if (Result == null) this.FParams.Files.value = null;
        return Result;
    };



    /*
    Заливка переденнагого AFile
     */
    Upload(AFile)
    {
        var that=this;

        /*Прописываем что файл стал на загрузку*/
        AFile.Status = 'Processing';
        this.FParams.File = AFile;

        /*Подготовка параметров*/
        clPost
        ({
            TypeContent:tcXML,
            Pars:true,
            Call:'File.Upload',
            Log:this.FLog,
            Income:this.FParams,

            OnUploadProgress: function(p)
            {
                AFile.Loaded = p.loaded;
                AFile.Total = p.total;
            },


            /*Отработка ошибки*/
            OnError:function(p)
            {
                p.File.Status = FILE_STATUS_ERROR;
                /*Upload next file*/
                var NextFile = that.GetNextFile();
                if (NextFile)
                {
                    that.Upload(NextFile)
                }
                else
                {
                    if ( that.FParams.OnFinishUpload()) that.FParams.OnFinishUpload( that.FParams );
                }
            },


            /*Отработка после загрузки*/
            OnAfterLoad:function(p)
            {
                if (clPostResult(p))
                {
                    p.Income.File.Status = FILE_STATUS_OK;
                    if (that.FLog) that.FLog.Inf('Upload file '+p.Income.File.name, true);
                    var NextFile = that.GetNextFile();
                    if (NextFile)
                    {
                        that.Upload(NextFile)
                    }
                    else
                    {
                        if ( that.FParams.OnFinishUpload()) that.FParams.OnFinishUpload( that.FParams );
                    }
                }
                else p.Income.File.Status = FILE_STATUS_ERROR;
            },


            /*Отработка ошибкиo*/
            OnError:function(p)
            {
                p.Income.File.Status = FILE_STATUS_ERROR;
            }
        });
    }
}
