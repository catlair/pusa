/*
    Catlair JS Copyright (C) 2019  a@itserv.ru

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

    Main library
    - динамическая работа со скриптом
    - ожидание загрузки скриптов
    - динамическая работа со стилями
    - управленеи индиактором загрузки

    Библиотеки расширения загружаемые отдельно разработчиком
    - ajax.js
    - loader.js
    - clAuth.js
    - clPopup.js
*/



class TCatlair
{
    constructor( ADatabase, ALog )
    {
        this.Log = ALog ? ALog : new TLog();
        this.MomentFormat = 'd.m.Y H:i:s';
    }



    /*
        Ожидание завершения загрузки скриптов
    */
    ScriptWait( AList, ACall )
    {
        clScriptWait( AList, ACall, this.Log );
        return this;
    }



    /*
        Динамическая загрузка стиля
        ID - идентификатор скрипта, по нему определяется надо ли загружать скрипт заново
        Type - источник, ссылка или сам контнет
        OnAfterLoad - вызов функции в которую передаются PAarams
    */
    StyleLoad(AParams)
    {
        var that = this;
        /*Заполнение параметров умолчальными значениями*/
        if (!AParams.URL) AParams.URL = '?template=' + AParams.ID;
        /*Отметка о начале работы*/
        /*Поиск идентификатора который возможно ранее был загружен*/
        var Style = document.getElementById(AParams.ID);
        if (!Style)
        {
            var Head  = document.getElementsByTagName('head')[0];
            var Link  = document.createElement('link');
            /*Добавление раздела Style*/
            Link.id=AParams.ID;
            Link.type='text/css';
            Link.rel='stylesheet';
            Link.href=AParams.URL;
            Head.appendChild(Link);
            console.log('Begin to load dynamic style ' + Link.href);
        }
        else
        {
            /*Выполняем пользовательскую функцию, если скрипт уже загружен*/
            AParams.Style = Style;
        }

        return this;
    }


    StyleUnload(AID)
    {
        var obj=document.getElementById(AID);
        if (obj) obj.parentNode.removeChild(obj);
        return this;
    }



    /*
        AJAX
    */



    /*
       Call AJAX procedure
       For params look at clPost
    */
    Post(APrm)
    {
        APrm.Log = this.Log;
        clPost(APrm);
        return this;
    }



    /*
        Обработка результатов запроса. 
        Если запрос вернул положительные результат то функцяи возвращает true.
        В противном случае возвращается ошибка выводится в лог
     */
    PostResult(APrm, AHide)
    {
        APrm.Log = this.Log;
        return clPostResult(APrm, AHide);
    }



    /*
        AParams.Files
        AParams.IDParent
        AParams.ID
        AParams.OnFinishUpload
    */
    FilesUpload(AParams)
    {
        /*проверка и загрузка библиотеки при необходмости*/
        this.ScriptWait
        (
            [ 'clFile.js' ],
            () =>
            {
                File = new TFileUpload( this.Log );
                File.Start( AParams );
            }
        );
    }



    /*
       Bild PDF
    */
    PDF( AParams )
    {
        cl.Post
        ({
            Income:      AParams.Income,
            Call:        AParams.Call,
            OnAfterLoad: function(p)
            {
                if (cl.PostResult(p))
                {
                   window.open( '?file=' + p.Result.Outcome.IDFile, '_blank' );
                }
            }
        });    
        return this;
    }
}



function clFindPrepare( AString )
{
    return AString.replace(/[^a-zA-Zа-яА-Я0-9_]/g, '');
}












class TParams
{
    Params = [];

    SetParams( AParams )
    {
        this.Params = AParams;
        return this;
    }



    GetParams( AParams )
    {
        return this.Params;
    }



    GetParam( AName, ADefault )
    {
        return this.Params[ AName ] ? this.Params[ AName ] : ADefault ? ADefault : null;
    }



    GetBoolean( AName, ADefault )
    {
        var Value = this.GetParam( AName, ADefault );
        return Value === true || Value === 'true' || Value === 1 || Value === 'on';
    }




    GetString( AName, ADefault, AReplace )
    {
        var Result = this.GetParam( AName, ADefault );
        if( AReplace )
        {
            for (const Key in AReplace) 
            {
                Result = Result.replace( '%' + Key + '%', AReplace[ Key ] );
            }
        }
        return Result;
    }



    SetParam( AName, AValue )
    {
        this.Params[ AName ] = AValue;
        return this;
    }
}



/*
    REST request
*/
class TRequest extends TParams
{

    constructor( AApplication )
    {
        super();
        this.Application = AApplication;
        this.URL = new TParams();
        this.Handle = null;

        /* Define */
        if( ! this.Application.Requests ) this.Application.Requests = [];
    }



    static Create( AApplication )
    {
        return new TRequest( AApplication );
    }



    SetURL( AParams )
    {
        for( var Key in AParams )
        {
            this.URL.SetParam( Key, AParams[ Key ] );
        }
        return this;
    }



    Execute( AParams )
    {   
        if( this.Handle ) this.Stop();

        this.Handle = clPost
        ({
            Call:              AParams.Name,
            TypeContent:       AParams.TypeContent,
            URLParams:         this.URL.GetParams(),
            Income:            this.GetParams(),
            Log:               this.Application.Log,

            OnAbort: Params =>
            {
                if( AParams.OnAbort ) AParams.OnAbort( Params );
            },

            OnAfterLoad: Result =>
            {
                if( ! Result.Aborted )
                {
                    this.Result = Result.Result;
                    this.Content = Result.Content;
                    if( this.Application.PostResult( Result )) 
                    {
                        if( AParams.OnAfterExecute ) AParams.OnAfterExecute( this, AParams )
                    }
                    else
                    {
                        if( AParams.OnErrorExecute ) AParams.OnErrorExecute( this, AParams )
                    }
                }
            }
        });
        return this;
    }



    Stop()
    {
        if( this.Handle ) clPostStop( this.Handle );
        this.Handle = null;
        return this;
    }
}






const LOAD_MODE_AUTO      = 'auto';
const LOAD_MODE_MANUAL    = 'manual';
const LOAD_MODE_SCROLL    = 'scroll';
const LOAD_MODE_CALLBACK  = 'callback';

class TDataset extends TRequest
{
    /* 
        Constants multiload records 
    */
    constructor( AApplication )
    {
        super( AApplication );
        this.LoadMode = LOAD_MODE_AUTO;
    }



    /*
        Create function 
    */
    static Create( AApplication )
    {
        return new TDataset( AApplication );
    }



    /*
        OnAfterLoad      ( Request, Params )
        OnBeginOfRecords ( Request, Params )
        OnEndOfRecords   ( Request, Params )
        OnBeforeRecords
        OnAfterRecords
        OnNextRecord     ( Record )
    */
    Load( AParams )
    {
        this.RecordsLoaded = 0;
        this.SetParam( 'RecordCurrent', this.RecordsLoaded );

        /* Call begin of records */
        if( AParams.OnBeginOfRecords ) AParams.OnBeginOfRecords( Request, AParams );

        AParams.OnAfterExecute = ( Request, Params ) =>
        {
            /* Get records count and total */
            this.RecordsLoaded += GetObjectProperty( Request, [ 'Result', 'Data', 'Records', 'Dataset', 'length' ], 0 );
            this.RecordsTotal = GetObjectProperty( Request, [ 'Result', 'Data', 'Records', 'RecordTotal' ], 0 );

            /* Call on after load */
            if( AParams.OnAfterLoad ) AParams.OnAfterLoad( Request, Params );                              

            if( this.RecordsTotal == 0 )
            {
                /* Call no records */
                if( AParams.OnNoRecords ) AParams.OnNoRecords( Request, Params );
            }
            else
            {
                /* Call OnBeforeRecords */
                if( AParams.OnBeforeRecords ) AParams.OnBeforeRecords( Request, Params );

                /* Records loop */
                this.Result.Data.Records.Dataset.forEach
                (
                    Record =>
                    {
                        if( AParams.OnNextRecord ) AParams.OnNextRecord( Record );                   
                    }
                );

                /* Call OnAfterRecords */
                if( AParams.OnAfterRecords ) AParams.OnAfterRecords( Request, Params );

                /* Upload next records */
                if( this.RecordsTotal > this.RecordsLoaded )
                {
                    this.SetParam( 'RecordCurrent', this.RecordsLoaded );

                    switch( this.LoadMode )
                    {
                        /* Automatic load */
                        case LOAD_MODE_AUTO :
                            this.Execute( AParams );            
                        break;                     
                    }
                }
                else
                {
                    /* Call end of records event if exists */
                    if( AParams.OnEndOfRecords ) AParams.OnEndOfRecords( Request, Params );

                }
            }
        };

        this.Execute( AParams );

        return this;
    }



    GetEmptyContent()
    {
        return decodeURIComponent( '<cl content="RecordEmpty.html" pars="true" convert="URI"/>' );
    }



    SetScroller( AElement )
    {
        AElement
    }
}




class TDOMDataset extends TDataset
{
    constructor( AApplication )
    {
        super( AApplication );
    }



    /* Create function */
    static Create( AApplication )
    {
        return new TDOMDataset( AApplication );
    }



    /*
        Parent - default parent container

        + OnGetParentContainer
        + OnBeforeRecord
        + OnAfterRecord
    */
    Load( AParams )
    {   
        /* OnBeginOfRecords callback */
        if( ! AParams.OnBeginOfRecords )
        {
            AParams.OnBeginOfRecords = ( Request, AParams ) =>
            {
               /* If parent does not define call GetParent */
               var ParentContainer = AParams.OnGetParentContainer ? AParams.OnGetParentContainer( null ) : AParams.ParentContainer;
               if( ParentContainer ) ParentContainer.SetContent( '' );
            };
        }

        /* No records */
        if( ! AParams.OnNoRecords )
        {
            AParams.OnNoRecords = ( Request, Params ) =>
            {
                var ParentContainer = AParams.OnGetParentContainer ? AParams.OnGetParentContainer( null ) : AParams.ParentContainer;
                if( ParentContainer )
                {
                    var EmptyContent = AParams.GetOnGetEmptyContent ? AParams.GetOnGetEmptyContent() : ( AParams.EmptyContent ? AParams.EmptyContent : this.GetEmptyContent() );
                    ParentContainer.SetContent( EmptyContent );
                }
            };
        }


        if( ! AParams.OnNextRecord )
        {
            AParams.OnNextRecord = ( Record ) =>
            {
                var ParentContainer = AParams.OnGetParentContainer ? AParams.OnGetParentContainer( Record ) : AParams.ParentContainer;
                if( ParentContainer ) 
                {
                    /* Call OnBeforeRecord */
                    if( AParams.OnBeforeRecord ) AParams.OnBeforeRecord( Record );                   

                    var RecordContainer = document.createElement( AParams.TagName ? AParams.TagName : 'div' );
                    RecordContainer.className = AParams.ClassName ? AParams.ClassName : 'Record';

                    /* Define content */
                    if( AParams.OnGetRecordContent ) var Content = AParams.OnGetRecordContent();
                    else var Content = AParams.RecordContent ? AParams.RecordContent : '%ID%;';

                    /* Prepare content */
                    if( Record.Params ) Content = clContentFromObject( Record.Params, Content );
                    Content = clContentFromObject( Record, Content );

                    RecordContainer.SetContent( Content );
                    ParentContainer.append( RecordContainer );

                    if( AParams.OnAfterRecord ) AParams.OnAfterRecord( RecordContainer, Record );
                }
            };
        }

        super.Load( AParams );
        return this;
    }
}






/*
    List of requests
*/
class TRequestList extends Array
{
    /*
        Constructor
    */
    constructor( AApplication )
    {
        super();
        this.Multirequest = true;
        this.Application = Application;
    }



    static Create( AApplication )
    {
        return new TRequestList();
    }

    
    /*
        Add new request
    */
    Add( ARequest )
    {
        if( ! this.Multirequest ) this.StopAll();
        this.push( ARequest );
        return this;
    } 



    /*
        Remove request from list
    */
    Remove( ARequest )
    {
        var Index = this.indexOf( ARequest );
        if( Index >= 0) this.slice( Index );
        return this;
    }



    /*
        Stop all requests
    */
    StopAll()
    {
        this.forEach( Request => Request.Stop() );
        return this;         
    }



    /*
        Stop all requests
    */
    Clear()
    {
        this.StopAll();
        this.length = 0;
        return this;         
    }
}
