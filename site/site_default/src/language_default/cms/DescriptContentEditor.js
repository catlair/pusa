class TDescriptContentEditor
{
    static Open( ADescript, AParams )
    {
        var Application = ADescript.Application;
        if( ! Application.WinControl ) Application.WinControl = new clWinControl();
        var Win = Application.WinControl.WindowCreate();

        Win.SetCaption( ADescript.ID );
        Win.SetToolbar(decodeURIComponent( '<cl content="DescriptContentToolbar.html" pars="true" convert="URI"/>' ));
        Win.SetContent(decodeURIComponent( '<cl content="DescriptContentEditor.html" pars="true" convert="URI"/>' ));
        Win.Icon.SetParams({Caption: ADescript.ID, Group:'Content editor', GUIDImage:''});
        Win.DescriptID=ADescript.ID;

        /* Set language */
        var IDLang = Win.ChildByID( 'IDLang' );
        if( AParams.IDLang )
        {
            IDLang.value = AParams.IDLang;
            IDLang.OnAfterChange = () =>
            {
                if ( IDLang.value != '' && IDLang.value != null )
                {
                    Win.Refresh();
                }
            };
        }

        var IDDescriptSite = Win.ChildByID( 'IDDescriptSite' );
        if( AParams.IDDescriptSite )
        {
            IDDescriptSite.value = AParams.IDDescriptSite;
            IDDescriptSite.OnAfterChange = () =>
            {
                if ( IDDescriptSite.value != '' && IDDescriptSite.value != null )
                {
                    Win.Refresh();
                }
            };
        }

        /* Activate decript selects */
        Application.SelectActivate( Win.Control );


        /*
            Описание формы обновления дескрита
        */
        Win.Refresh = function()
        {
            if( Win.DescriptID )
            {
                /*Пытаемся загрузить запись*/
                var Prm=
                {
                    Call:'Descript.ContentRead',
                    Income:
                    {
                        ID: Win.DescriptID,
                        IDLang: IDLang.value,
                        IDDescriptSite: IDDescriptSite.value
                    },
                    OnAfterLoad:function(p)
                    {
                        if( Application.PostResult( p ))
                        {
                            Win.ChildByID( 'ID' ).innerHTML = Win.DescriptID;
                            Win.ChildByID( 'DescriptCaption' ).value =  
                            ( p.Result && p.Result.Outcome && p.Result.Outcome.Caption ) 
                            ? p.Result.Outcome.Caption
                            : '';

                            Win.ChildByID( 'DescriptContent' ).value =  
                            ( p.Result && p.Result.Outcome && p.Result.Outcome.Content ) 
                            ? decodeURIComponent( p.Result.Outcome.Content )
                            : '';
                          
                            Win.ChildByID( 'Indexate' ).checked = 
                            p.Result && p.Result.Outcome && p.Result.Outcome.Indexate;
                        }
                    }
                };
                Application.Post(Prm);
            }
        };



        /*
            Процедура сохранения формы
        */
        Win.Save = function()
        {
            if( Win.DescriptID )
            {
                Application.Post
                ({
                    Income: 
                    {
                        ID:             Win.DescriptID,
                        IDLang:         Win.ChildByID( 'IDLang' ).value,
                        IDDescriptSite: Win.ChildByID( 'IDDescriptSite' ).value
                    },
                    Call:        'Descript.ContentUpdate',
                    Pars:        true,
                    Form:        Win.ChildByID('Form'),
                    OnAfterLoad: ( p ) => Application.PostResult( p, true )
                });
            }
        };

        Win.GetControl().ChildByID( 'BtnRefresh', Btn => Btn.onclick = () => Win.Refresh() );
        Win.GetControl().ChildByID( 'BtnSave',    Btn => Btn.onclick = () => Win.Save() );

        Win.Refresh();
    }
}


