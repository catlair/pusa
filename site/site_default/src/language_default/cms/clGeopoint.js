class TGeopoint
{
    constructor ()
    {
        this.Element = null;
    }


    static Create()
    {
        return new TGeopoint();
    }


    /*
        Set geopoint values
    */
    Set ( ALatitude, ALongitude, AAccuracy )
    {
        if (ALatitude) this.Latitude = ALatitude;
        else this.Latitude = 0;

        if (ALongitude) this.Longitude = ALongitude;
        else this.Longitude = 0;

        if (AAccuracy) this.Accuracy = AAccuracy;
        else this.Accuracy = 0;
        
        this.VisualRefresh();

        return this;
    }


    
    /*
        Set geopoint from other geopoint
    */
    SetFrom ( AGeopoint )
    {
        if ( AGeopoint != null )
        {
            this.Set
            (
                AGeopoint.Latitude,
                AGeopoint.Longitude,
                AGeopoint.Accuracy
            );
        }
        else
        {
            this.Set( 0, 0, 40000 );
        }
        return this;
    }



    /* 
        Refresh visual component
    */
    VisualRefresh()
    {
        if ( this.Element != null )
        {
           var Content = "";
           if ( this.Accuracy < 100000 )
           {
               Content = decodeURIComponent( '<cl content="GeopointElement.html" convert="URI"/>' );
               Content = clContentFromObject( this, Content );
           }
           else
           {
               Content = decodeURIComponent( '<cl content="GeopointElementError.html" convert="URI"/>' );
           }
           this.Element.innerHTML = Content;
        }
        return this;
    }



    ToVector()
    {
        var Result = new clVector();
        Result.SetGeo( this.Latitude, this.Longitude );
        return Result;
    }



    SetElement( AElement )
    {
        /* Clear geopoint for old element */
        if ( this.Element != null ) this.Element.Geopoint = null;

        /* Set geopoint for new element */
        if ( AElement != null ) AElement.Geopoint = null;

        /*Set element for this geopoint */
        this.Element = AElement;
        AElement.Geopoint = this;

        return this;
    }


    /*
         
    */
    GetElement()
    {
        return this.Element;
    }



    /*
    */
    ToString()
    {
        return this.Latitude + ', ' + this.Longitude + ", " + this.Accuracy;
    }



    /*
        Return GOOGLE link with AZoom from 1 to 21
    */
    GetURL()
    {
        return 'https://www.google.com/maps/search/?api=1&query=' 
        + this.Latitude + ',' 
        + this.Longitude /* + ',' 
        + this.Accuracy + 'm' 
        + '/data=!3m2!1e3!4b1!4m5!3m4!1s0x0:0x0!8m2!3d' 
        + this.Latitude +'!4d'
        + this.Longitude*/ ;
    }


    Map()
    {
        window.open( this.GetURL(), '_blank');
    }    
}



