class TConversation
{  
    constructor( AApplication, AContainer )
    {
        this.Container = AContainer;
        Pusa.Request
        ({
            Class : "Conversation",
            Method: "Form",
            Element: this.Container
        });
    }


    static Create( AApplication,  AContainer, AIDParent )
    {
        return new TConversation( AApplication, AContainer );
    }



    Refresh( AParams )
    {
        Pusa.Request
        ({
            Class : "Conversation",
            Method: "Refresh",
            Element: this.Container
        });
        return this;     
    }
}