/*
  Catlair PHP Copyright (C) 2019  a@itserv.ru
 
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 
 
  TPay interface. It works with Catlair pay.php.
  Support provider:
  - PayPal
  - PayPal Sandbox
 
*/



class TPaymentPayPal extends TPay
{
    static Create( AApplication )
    {
        var Result = new TPaymentPayPal( AApplication );
        return Result;
    }



    /*
        This method can call ScriptLoad for assinchrone calls
    */
    Init( AOnInit )
    {
        if( AOnInit ) AOnInit( this );
        return this;
    }



    PayBegin( AParams )
    {
        var Content = decodeURIComponent( '<cl content="PaymentPayPalPayForm.html" pars="true" convert="URI"/>' );
        this.PopupPayForm( AParams, clContentFromObject( this.GetParams(), Content ));
        return this;
    }



    Pay( AParams )
    {

        var Income = 
        {
           /* Parameters from form */
           item_name:      AParams.Form.Description,
           amount:         AParams.Form.AmountForPayment,
           currency_code:  AParams.Form.IDCurrency,

           /* Parameters from payment method config */
           cmd:            this.GetParam( 'cmd', '_xclick' ),
           business:       this.GetParam( 'business' ),
           charset:        this.GetParam( 'charset', 'utf-8' ),

           custom:         this.GetString( 'custom' ),
           notify_url:     this.GetString( 'notify_url', null, { Origin: window.location.origin, ID:this.GetString( 'IDPaymentMethod' ) }),
           return:         this.GetString( 'return', null, { Origin: window.location.origin }),
           cancel_return:  this.GetString( 'cancel_return', null, { Origin: window.location.origin }),
           no_shipping:    this.GetString( 'no_shipping', null)
        };

        var Form = document.createElement( 'form' );       
        Form.method='post';
        Form.target='_blank';
        Form.action = 'https://www' + ( this.GetBoolean( 'Sandbox', true ) ? '.sandbox' : '' ) + '.paypal.com/cgi-bin/webscr';
        clValuesFromObject( Income, Form, true );
        AParams.Popup.append( Form );
        Form.submit();

        this.PayEnd( AParams );

        return this;
    }



    PayEnd( AParams )
    {
        /* Close popup dialig */
        if( AParams.Popup ) AParams.Popup.Close();
        if( AParams.OnAfterPay ) AParams.OnAfterPay( AParams );
        return this;
    }

}
