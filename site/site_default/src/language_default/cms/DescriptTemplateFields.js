class TDescriptTemplateFields
{
    static Do( AApplication, ADescript, AParams )
    {
        /* Create form and popup */
        var Popup = AApplication.Popup
        ({
            TypeContent: tcText,
            Content: decodeURIComponent( '<cl content="DescriptTemplateFieldsFrom.html" pars="true" convert="URI"/>' )
        });

        /* Init popup */
        AApplication.SelectActivate( Popup );
        /* Set values for form */
        Popup.ChildByID( 'IDDescript', Element => Element.Refresh( ADescript.ID ) );

        /* Action on ok */
        Popup.ChildByID( 'BtnRefresh',  Btn => Btn.onclick = () => Popup.Refresh() );


        /* Action on ok */
        Popup.ChildByID
        (
            'BtnOk',
            Btn => Btn.onclick = () =>
            {
                TRequest.Create( AApplication )
                .SetParam( 'ID', Popup.ChildByID( 'IDParent' ).value )
                .SetParam( 'IDBind', Popup.ChildByID( 'IDBind' ).value )
                .Execute
                ({
                    Name: 'Descript.CopyParamsToChild',
                    OnAfterExecute: Result => Popup.Close()
                });
            }
        );

        /* Action on cancel */
        Popup.ChildByID( 'BtnCancel', Btn => Btn.onclick = () => Popup.Close());


        Popup.Refresh = () =>
        {
            TDOMDataset.Create( AApplication )
            .SetParam( 'ID', ADescript.ID )
            .Load
            ({
                Name:             'Descript.FieldsList',
                ParentContainer:  Popup.ChildsByClass( 'List' )[ 0 ],
                RecordContent:    decodeURIComponent( '<cl content="DescriptTemplateFieldsFromRecord.html" pars="true" convert="URI"/>' ),
                OnAfterRecord:    ( AContainer, ARecord ) =>
                {
                    AContainer.ChildsByClass
                    (
                        'Checkbox', 
                        Element =>  
                        {
                            Element.checked = ARecord.Export;
                            Element.onchange = () =>
                            {
                                TRequest.Create( AApplication )
                                .SetParam( 'ID', ADescript.ID )
                                .SetParam( 'IDField', ARecord.IDField )
                                .SetParam( 'Attach', Element.checked )
                                .Execute({ Name: 'Descript.FieldsAttach' });
                            }
                        }
                    )
                }
            });
        };

        Popup.Refresh();

    }
}



