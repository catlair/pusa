class TMath
{
    static EPSILON = 1e-8;

    /* Линейная интерполяция */
    static ItpLin( AMin, AMax, ATime )
    {
       return AMin + ( AMax - AMin ) * ATime;
    }



    /*
        Линейная интерполяция 
    */
    static ItpCos( AMin, AMax, ATime )
    {
        var k = -0.5 * Math.cos( ATime*Math.PI ) + 0.5;
        return AMin + (AMax-AMin) * k;
    }



    /* 
        Приведение значения APos между заданными AMin и AMax к интервалу (0;1) 
    */
    static Norm( AMin, AMax, APos )
    {
        var d = AMax - AMin;
        return( -EPSILON < d || d > EPSILON ) ? ( APos - AMin ) / d : 0;
    }
}




