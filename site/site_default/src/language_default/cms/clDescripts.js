/**
    Catlair JS
    Descripts list
    still@itserv.ru
*/

const DESCRIPTS_BUILD_BACK    = 'back';
const DESCRIPTS_BUILD_FRONT   = 'front';

const DESCRIPTS_STATUS_STOP   = 'stop';
const DESCRIPTS_STATUS_WAIT   = 'wait';
const DESCRIPTS_STATUS_WORK   = 'work';
const DESCRIPTS_STATUS_ERROR  = 'error';


/**
    Список дескриптов
*/
class TDescripts
{
    constructor (ACatlair)
    {
        this.Status                 = DESCRIPTS_STATUS_WAIT;
        this.Selected               = null;
        this.OnBeforeSelected       = null;
        this.OnAfterSelected        = null;

        this.Tree                   = null; /*Tree conteiner*/
        this.Record                 = null; /*Record conteiner*/

        this.RecordCount            = 10;
        this.RecordCurrent          = 0;
        this.RecordTotal            = null;
        this.RecordConteiner        = null;

        this.RecordContent          = null;
        this.RecordContentDefault   = null;

        this.Library                = 'descript_load';
        this.ProcName               = 'DescriptLoad';

        this.Find                   = null;
        this.IDParent               = null;
        this.ID                     = null;
        this.IDLang                 = null;
        this.IDSite                 = null;
        this.IDType                 = null;
        this.Continue               = 'auto'; /* Method of load */

        this.NoRecordContent        = decodeURIComponent( '<cl content="NoRecords.html" pars="true" convert="URI"/>' );
    }



    /**
        Check end of record
    */
    EndOfRecord()
    {
        return this.RecordCurrent >= this.RecordTotal;
    }



    /**
        Поиск дескриптов по строке поиска
        AParams.IDFolder - папка с дескриптом
        AParams.Find - строка поиска
        AParams.IDLanguage - идентификатор языка
        AParams.IDSite - идентификатор сайта
        AParams.RecordCount
        AParams.PageCurrent
    */

    Load( AParams )
    {
        var that = this;

        if ( AParams && !AParams.Direction)      AParams.Direction       = DESCRIPTS_BUILD_BACK;
        if ( AParams && !AParams.Continue)       AParams.Continue        = 'auto';
        if ( AParams && !AParams.RecordCurrent)  AParams.RecordCurrent   = 0;
        if ( AParams && !AParams.RecordCount)    AParams.RecordCount     = 10;
        if ( AParams && !AParams.ContentLength)  AParams.ContentLength   = 0;

        /* исполнение */
        this.Status = DESCRIPTS_STATUS_WORK;

        cl.Post
        ({
            IDGroup: clNotEmpty( AParams.IDGroup ),
            IDGroupPrefix: clNotEmpty( AParams.IDGroupPrefix ),
            Call:    clNotEmpty( AParams.Call, 'DescriptContent.List' ),
            Income:  AParams,
            Form:    clNotEmpty( AParams.Form ),

            OnAfterLoad: function( p )
            {
                if( cl.PostResult( p ))
                {
                    if( that.Status == DESCRIPTS_STATUS_WORK )
                    {  
                        /* Очистка списка записей если вернулись записи с начала */
                        if( AParams.RecordCurrent == 0 && ! AParams.KeepContent )
                        {
                            that.Clear( AParams.OnClear );
                        }

                        /* Загрузука записей */
                        if ( p.Result.Data.Records )
                        {
                            that.RecordTotal = clNotEmpty( parseInt( p.Result.Data.Records.RecordTotal ), 0 );

                            /* Empty record set */
                            if( AParams.RecordCurrent == 0 && that.RecordTotal == 0 )
                            {

                                that.RecordConteiner.SetContent( that.NoRecordContent );
                            }

                            /* Вызов до загрузки */
                            if( AParams.OnRecordsBefore ) AParams.OnRecordsBefore( p.Result );

                            var c = p.Result.Data.Records.Dataset.length;
                            for( var i=0; i<c; i++ )
                            {
                               var Record = p.Result.Data.Records.Dataset[i];
                               
                               if( AParams.OnNewRecord ) AParams.OnNewRecord( Record );

                               /* Define container element */
                               var RecordContainer
                               = AParams.OnSelectRecordContainer
                               ? AParams.OnSelectRecordContainer( Record )
                               : that.RecordConteiner;

                               /* Add record to container */
                               if ( RecordContainer ) that.RecordAdd( Record, p, AParams, RecordContainer );

                               /* Increese records count */
                               that.RecordCurrent++;
                            }                               

                            /* Расчет загруженных записей */
                            AParams.RecordCurrent = that.RecordCurrent;

                            if ( AParams.OnRecordsAfter ) AParams.OnRecordsAfter( p.Result );

                            if ( AParams.Continue == 'auto' && !that.EndOfRecord() ) that.Load( AParams );
                            else that.Status = DESCRIPTS_STATUS_WAIT;
                        }
                    }
                }
                else
                {
                   that.Status = DESCRIPTS_STATUS_ERROR;
                }
            }
        });


        /* Если начало запроса и был таймер отслеживания скролирования то его сбрасываем */
        if (AParams.RecordCurrent == 0 && that.LoaderTimer != null)
        {
            clearInterval(that.LoaderTimer);
            that.LoaderTimer = null;
        }


        /* создание отслеживания скролинга */
        if ( that.RecordConteiner && AParams.Continue == 'scroll' && this.LoaderTimer == null)
        {
            /* Запуск таймера отслеживания скролирования*/
            var that=this;
            this.LoaderTimer = setInterval
            (
                function ()
                {
                    /* получаем координаты расположения контейнера на экране*/
                    var box = that.RecordConteiner.getBoundingClientRect();

                    if ( box.x==0 && box.y==0 && box.height==0 && box.width==0)
                    {
                            clearInterval(that.LoaderTimer);
                            that.LoaderTimer = null;
                    }
                    else
                    {
                        if 
                        (
                           that.Status == DESCRIPTS_STATUS_WAIT 
                           && box.bottom < 2*document.documentElement.clientHeight
                        )
                        {
                            if (that.EndOfRecord())
                            {
                                clearInterval( that.LoaderTimer );
                                that.LoaderTimer = null;
                            }
                            else that.Load( AParams );
                        }
                    }
                },
                200
            );
        }
    }



    LoadStop()
    {
        this.Status = DESCRIPTS_STATUS_STOP;
        return this;
    }



    /**
    */
    Clear( AClear )
    {
        this.RecordCurrent = 0;
        this.RecordTotal = null;

        /* Очистка контейнера */
        if( this.OnClear )
        {
            this.OnClear();
        }
        else
        {
            if ( this.RecordConteiner != null ) this.RecordConteiner.innerHTML = '';
        }

        if( AClear ) AClear();

        return this;
    }



    /*
        Добавляет запись в контейнер записей
    */
    RecordAdd 
    ( 
        ARecord,            /* Объект - запись */      
        AResult,
        AParams,
        ARecordContainer    /* Target container */
    )
    {
        /* Set ID as default if Caption is empty */
        if ( ARecord.ID && ( ARecord.Caption == '' || ARecord.Caption == null )) 
        {
            ARecord.Caption = ARecord.ID + '*';
        }

        /* Создаем элемент новый */
        var Container                 = document.createElement('div');
        Container.className           = 'Record';
        Container.Record              = ARecord;
        Container.Record.Conteiner    = Container;
        Container.Record.Container    = Container;
        Container.Descripts           = this;

        /* Create and set descript */
        Container.Descript            = new TDescript();
        Container.Descript.Record     = ARecord; 
        Container.Descript.ID         = ARecord.ID;

        if ( AParams.OnRecordAdd ) 
        {
            AParams.OnRecordAdd( Container.Descript, AParams );
        }
        else
        {
            var Template = this.RecordContent;
            if
            (
                Template == null &&
                AResult.Result.Outcome && 
                AResult.Result.Outcome.RecordTemplate 
            )
            {
                Template = decodeURIComponent( AResult.Result.Outcome.RecordTemplate );
            }
            if ( Template == null ) Template = this.RecordContentDefault;
            if ( Template == null ) Template = '';

            var Content = clContentFromObject( ARecord, Template );
            Content = clContentFromObject( ARecord.Params, Content );
            Content = clContentClear( Content );

            Container.innerHTML = Content;
        }

        /* Добавляем в контейнер */
        if ( AResult.Income.Direction == DESCRIPTS_BUILD_BACK )
           ARecordContainer.append( Container );
        else
           ARecordContainer.prepend( Container );

        /* Вызов события загрузки записи */
        if ( this.OnAfterLoadRecord ) this.OnAfterLoadRecord( Container, AResult );
        if ( AParams.OnAfterLoadRecord ) AParams.OnAfterLoadRecord( Container, AResult.Result );

        return Container;
    }
}


