/*
    Описание константных значений
    Количество миллисекундах в единицах времени
*/

var clMsec   = 1;
var clSec    = 1000;
var clMin    = 60000;
var clHour   = 3600000;
var clDate   = 86400000;
var clMonth  = 2635200000;
var clYear   = 31536000000;



function clDateToMoment( ADate )
{
    return ADate.getTime();
}



function clMomentToDate( AMoment )
{
    return new Date( AMoment );
}



function clNow()
{
    var d = new Date();
    return clDateToMoment( d ) - d.getTimezoneOffset() * clMin;
}


function clGetTime( AMoment )
{
    return parseInt((( AMoment / clDate ) - parseInt( AMoment / clDate )) * clDate );
}



function clGetDayBegin( AMoment )
{
    return parseInt( AMoment / clDate ) * clDate;
}



/*
    Возвращает true если время ATime лежит в пределах между моментами ABegin и AEnd
*/
function clTimeInInterval( ATime, ABegin, AEnd )
{
    var r = null;
    var b = ABegin - clGetDayBegin(ABegin);
    var l = AEnd - ABegin;
    if ((ATime>=b && ATime<b+l) || (ATime+clDate>=b && ATime+clDate<b+l)) r = clGetDayBegin(ABegin) + ATime;
    return r;
}



function clSetDay(AMoment,ADay)
{
    return clMomentToDate(AMoment).setUTCDay(ADay);
}



function clSetYear(AMoment,AYear)
{
    return clMomentToDate(AMoment).setUTCFullYear(AYear);
}



function clSetMonth(AMoment,AMonth)
{
    return clMomentToDate(AMoment).setUTCMonth(AMonth);
}



function clSetDate(AMoment,ADay)
{
    return clMomentToDate(AMoment).setUTCDate(ADay);
}



function clSetHour(AMoment,AHour)
{
    return clMomentToDate(AMoment).setUTCHours(AHour);
}



function clSetMin(AMoment,AMin)
{
    return clMomentToDate(AMoment).setUTCMinutes(AMin);
}



function clSetSec(AMoment,ASec)
{
    return clMomentToDate( AMoment ).setUTCSeconds(ASec);
}



function clSetMilli( AMoment, AMilli )
{
    return clMomentToDate( AMoment ).setUTCMilliseconds(AMilli);
}



function clGetDay( AMoment )
{
    return clMomentToDate(AMoment).getUTCDay();
}



function clGetYear( AMoment )
{
    return clMomentToDate(AMoment).getUTCFullYear();
}



function clGetMonth( AMoment )
{
    return clMomentToDate(AMoment).getUTCMonth();
}



function clGetDate( AMoment )
{
    return clMomentToDate( AMoment ).getUTCDate();
}



function clGetHour( AMoment )
{
    return clMomentToDate( AMoment ).getUTCHours();
}



function clGetMin( AMoment )
{
    return clMomentToDate( AMoment ).getUTCMinutes();
}



function clGetSec( AMoment )
{
    return clMomentToDate( AMoment ).getUTCSeconds();
}



function clGetMilli( AMoment )
{
    return clMomentToDate( AMoment ).getUTCMilliseconds();
}



function clGetMonthName( AMoment )
{
    var n=new Array( 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec' );
    return n[ clGetMonth( AMoment )];
}



function clGetDayName( AMoment )
{
    var n = [ 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat' ];
    return n[ clGetDay( AMoment )];
}



function clBegYear( AMoment )
{
    var d=clMomentToDate(AMoment);
    return Date.UTC(d.getUTCFullYear(), 0, 1, 0, 0, 0, 0);
}



function clEndYear( AMoment )
{
    var d = clMomentToDate(AMoment);
    return Date.UTC(d.getUTCFullYear()+1, 0, 1, 0, 0, 0, 0);
}



function clBegMonth( AMoment )
{
    var d = clMomentToDate( AMoment );
    return Date.UTC( d.getUTCFullYear(), d.getUTCMonth(), 1, 0, 0, 0, 0 );
}



function clEndMonth(AMoment)
{
    var d = clMomentToDate( AMoment );
    return Date.UTC( d.getUTCFullYear(), d.getUTCMonth()+1, 1, 0, 0, 0, 0 );
}



function clBegWeek( AMoment )
{
    return parseInt( AMoment / clDate ) * clDate - clGetDay( AMoment )  * clDate;
}



function clEndWeek( AMoment )
{
    return parseInt( AMoment / clDate ) * clDate - clGetDay( AMoment ) * clDate +  7 * clDate;
}



function clBegDate( AMoment )
{
    return parseInt( AMoment / clDate ) * clDate;
}



function clEndDate( AMoment )
{
    return parseInt( AMoment / clDate ) * clDate + clDate;
}



function clBegHour(AMoment)
{
    return parseInt(AMoment/clHour)*clHour;
}



function clEndHour(AMoment)
{
    return parseInt(AMoment/clHour) * clHour + clHour;
}



function clBegMin( AMoment )
{
    return parseInt(AMoment/clMin)*clMin;
}



function clEndMin( AMoment )
{
    return parseInt(AMoment/clMin)*clMin+clMin;
}



function clBegSec( AMoment )
{
    return parseInt(AMoment/clSec)*clSec;
}



function clEndSec( AMoment )
{
    return parseInt(AMoment/clSec)*clSec+clSec;
}



/*
    Convert string to moment
*/
function clStrToMoment
(
    AString,  /* String for convert */
    ADefault, /* Default  */
    AFormat
)
{
   /* Parse value */
   if ( ! AString ) AString = '';
   var ValueString  = AString.replace( /[-:_\.\///]/gi, ' ' );
   var ValueParts   = ValueString.trim().split( ' ' );

   /* Parse format */
   if ( ! AFormat )
   {
       AFormat = 'Y-m-d H:i:s';
       if( ValueParts[ 0 ] && ValueParts[ 0 ].length == 4 ) AFormat = 'Y-m-d H:i:s';
       if( ValueParts[ 2 ] && ValueParts[ 2 ].length == 4 ) AFormat = 'd-m-Y H:i:s';
       if( ValueParts[ 3 ] && ValueParts[ 3 ].length == 4 ) AFormat = 'H:i:s Y-m-d';
       if( ValueParts[ 5 ] && ValueParts[ 5 ].length == 4 ) AFormat = 'H:i:s d-m-Y';
   }
   var FormatString = AFormat.replace( /[-:_\.\///]/gi, ' ' );
   var FormatParts  = FormatString.split( ' ' );

   var y = ValueParts[ FormatParts.indexOf( 'Y' ) ];
   var m = ValueParts[ FormatParts.indexOf( 'm' ) ];
   var d = ValueParts[ FormatParts.indexOf( 'd' ) ];
   var h = ValueParts[ FormatParts.indexOf( 'H' ) ];
   var n = ValueParts[ FormatParts.indexOf( 'i' ) ];
   var s = ValueParts[ FormatParts.indexOf( 's' ) ];

   if ( isNaN( y ) || isNaN( m ) || isNaN( d ))
   {
       Result = ADefault;
   }
   else
   {
       if ( isNaN( h )) h = 0;
       if ( isNaN( n )) n = 0;
       if ( isNaN( s )) s = 0;
       Result = Date.UTC(y, m - 1, d, h, n, s, 0);
   }

   return Result;
}



function clStrTimeToMoment( AString, ADefault )
{
    AString = AString.trim();

    if (AString)
    {
        AString=clStrRight( AString, 8, '0' );
        var h = parseInt( AString.substring( 0, 2 ));
        var n = parseInt( AString.substring( 3, 5 ));
        var s = parseInt( AString.substring( 6, 8 ));
    }

    return h * clHour + n * clMin + s * clSec;
}



function clMomentToStr( AMoment, ADefault, AFormat )
{
    if( AFormat == null ) AFormat = 'Y-m-d H:i:s';
    if( AMoment )
    {
        var d = clMomentToDate( parseInt( AMoment ));

        r = AFormat;
        r = r.replace( 'Y', d.getUTCFullYear().zeroFix( 4 ));
        r = r.replace( 'y', d.getUTCFullYear().zeroFix( 2 ));
        r = r.replace( 'm', (d.getUTCMonth()+1).zeroFix( 2 ));
        r = r.replace( 'd', d.getUTCDate().zeroFix( 2 ));
        r = r.replace( 'H', d.getUTCHours().zeroFix( 2 ));
        r = r.replace( 'i', d.getUTCMinutes().zeroFix( 2 ));
        r = r.replace( 's', d.getUTCSeconds().zeroFix( 2 ));
    }
    else
    {
        r = ( ADefault ) ? ADefault : '';
    }
    return r;
}



Number.prototype.zeroFix = function(l)
{
    return '000000000000'.concat(this).slice(-l||l);
};



/*

*/
function clMomentDeltaToStr(ADelta, AValue)
{
    if ( ! AValue ) AValue = 
    {
        Year:  'Y',
        Month: 'M',
        Day:   'd',
        Hour:  'h',
        Min:   'm',
        Sec:   's',
        MSec:  'ms',
        Zero:  '-'
    };

    if (ADelta >= clYear) return (ADelta/clYear).toFixed(0) + ' ' + AValue.Year;
    else if (ADelta >= clMonth) return (ADelta/clMonth).toFixed(0) + ' ' + AValue.Month;
    else if (ADelta >= clDate) return (ADelta/clDate).toFixed(0) + ' ' + AValue.Day;
    else if (ADelta >= clHour) return (ADelta/clHour).toFixed(0) + ' ' + AValue.Hour;
    else if (ADelta >= clMin) return (ADelta/clMin).toFixed(0) + ' ' + AValue.Min;
    else if (ADelta >= clSec) return (ADelta/clSec).toFixed(0) + ' ' + AValue.Sec;
    else if (ADelta >= 1 && AZero!=undefined) return ADelta.toFixed(0) + ' ' + AValue.MSec;
    else return AValue.Zero;
}



