/*******************************************************************************
    Timeline
*/

class TTimeline
{

    DTBegin =  clNow() - clDate; /* Time interval from */
    DTEnd   =  clNow() + clDate * 0.5; /* Time interval to */

    Scene = null;                /* Used TScene object */

    OnChageAges = null;

    constructor( ACanvas, ALinePars )
    {
        this.Now        = clNow();

        /* Массив создаем линий */
        this.Lines      = TLines.Create( this );
        this.LinePars   = ALinePars;

        this.ActiveBar     = null;
        this.ActiveBars    = null;

        this.ActiveMarker  = null;
        this.DragMarker    = null;
        this.SelectMarker  = null;

        this.MouseBarDTBeg=null;
        this.MouseBarDTEnd=null;

        this.MouseBarLine=null;
        this.MouseBarEndSelect=null;

        this.WheelAccuracy=1.3;
        this.XMouseAccuracy=3;

        /*Определение графического интерфейса Canvas*/
        this.Scene = new TScene( ACanvas, null );
        this.Camera = clCameraCreate();

        this.LastDraw = 0;

        this.Top=0;
        this.LineHeight        = 20;
        this.TimeLineHeight    = 80;
        this.TimeLineMinWidth  = 5;
        this.TimeLineFullWidth = 50;
        this.AlignAcuracy      = 10; /*количество точек прилипания баров к граням*/
        this.LegendWidth       = 200; /*ширина легенды*/
        this.MarkerSize        = 18;

        this.DragBeginAcuracy  = 10; /*Дистанция в точках, со скольки начианется драгдроп*/

        this.FadeDelay         = 0.2;
        this.FadeMoment        = 0.0;

        this.BarCornerRadius   = this.LineHeight*0.5;
        this.BarFont='normal normal 14px Arial';

        this.GradientGlass=this.Scene.Context.createLinearGradient(0,0,0,this.LineHeight);
        this.GradientGlass.addColorStop(0.0, 'rgba(255,255,255, 0.5)');
        this.GradientGlass.addColorStop(0.2, 'rgba(255,255,255, 0.2)');
        this.GradientGlass.addColorStop(0.3, 'rgba(255,255,255, 0.0)');

        /* Past and future gradients  */
        this.GradientPast      = '#373e48ff';
        this.GradientFuture    = '#535d6cff';

        /*Градиент фона таймлайна*/
        this.GradientTimeLine=this.Scene.Context.createLinearGradient( 0, 0, 0, this.TimeLineHeight );
        this.GradientTimeLine.addColorStop( 0.00, '#6f7c91ff' );
        this.GradientTimeLine.addColorStop( 1.00, '#6f7c91AA' );

        /* Настройка градиента легенды */
        this.GradientLegendBack       = '#6f7c91AA';
        this.GradientLegendBackActive = '#b7bec8ff';

        this.HolydayText = '#FFFFFFFF';
        this.WorkdayText = '#FFFFFF7C'; 

        /* Настройка градиента легенды */
        this.GradientLegendText=this.Scene.Context.createLinearGradient( 0, 0, this.LegendWidth, 0 );
        this.GradientLegendText.addColorStop(0.7, '#FFFFFFFF');
        this.GradientLegendText.addColorStop(0.9, '#FFFFFF00');

        /* Настройка градиента легенды */
        this.GradientLegendTextActive=this.Scene.Context.createLinearGradient( 0,0, this.LegendWidth, 0 );
        this.GradientLegendTextActive.addColorStop( 0.8, '#000000BB' );
        this.GradientLegendTextActive.addColorStop( 0.9, '#00000000' );

        /* The context menu disabe */
        ACanvas.oncontextmenu = () => false;

        ACanvas.onmousedown = AEvent =>
        {
            this.Mouse          = clVector.Create( AEvent.offsetX, AEvent.offsetY );
            this.Drag           = false;
            this.LeftButton     = AEvent.which == 1;
            this.SelectMarker   = this.Lines.MarkerByPos( this.Mouse );
            this.SelectBar      = this.SelectMarker ? null : this.ActiveBar;
        };


        ACanvas.onmouseup = AEvent =>
        {
            this.Mouse = clVector.Create( AEvent.offsetX, AEvent.offsetY );
            this.LeftButton=false;
            this.SelectMarker=null;
            this.DragMarker=null;

            if( this.DragBar )
            {
                if( this.DragBar.onDragEnd ) this.DragBar.onDragEnd( this.DragBar );
                this.DragBar=null;
            }

            if( this.MouseBarLine != null )
            {
                if( MouseBarEndSelect )
                {
                    MouseBarEndSelect
                    (
                        Math.min( this.MouseBarDTBeg, this.MouseBarDTEnd),
                        Math.max( this.MouseBarDTBeg, this.MouseBarDTEnd),
                        this.MouseBarLine.ID
                    );
                } 
                this.MouseBarLine=null;
            }

            if( this.Drag )
            {
                this.Drag = false;
                this.Draw( AEvent );
            }

            if( this.OnMouseUp ) this.OnMouseUp( AEvent );
        };

        ACanvas.onmousemove = AEvent =>
        {
            this.ActiveBar = null;
            this.Now = clNow();

            /*определение смещения мыши*/
            var DeltaPos = this.Mouse
            ? clVector.Create( AEvent.movementX, AEvent.movementY )
            : clVector.CreateZero();

            var MousePosition = clVector.Create( AEvent.offsetX, AEvent.offsetY );


            if( this.LeftButton )
            {
                if( !this.Drag )
                {
                    /*Начало драгдропа*/
                    this.DragStartPos = MousePosition.Get();
                    this.Drag=true;
                }

                var DragDistance = this.DragStartPos.Distance( MousePosition );
                if (!this.DragBar && this.SelectMarker && this.SelectMarker.Mobile ) 
                {
                    /*Начало перетаскивания маркера*/
                    this.DragMarker=this.SelectMarker;
                }

                if (!this.DragMarker && this.SelectBar && !this.DragBar && DragDistance > this.DragBeginAcuracy) 
                {
                    var d = false;
                    if (this.SelectBar.onDragBegin) d=this.SelectBar.onDragBegin(this.SelectBar);

                    /*Начало перетаскивания бара*/
                    if (d) 
                    { 
                        this.DragBar=this.SelectBar;
                        this.DragBar.DTBegBeforeDrag=this.DragBar.DTBegin;
                        this.DragStart=this.PosToMoment( AEvent.offsetX, false);

                        if (!this.DragBar.Modify)
                        {
                            this.DragBar.Modify=true;
                            this.DragBar.ModifyDTBeg=this.DragBar.DTBegin;
                            this.DragBar.ModifyDTEnd=this.DragBar.DTEnd;
                            this.DragBar.ModifyIDLine=this.DragBar.IDLine;
                            this.DragBar.ModifyLine=this.DragBar.Line;
                        }
                    }
                }

                if( this.DragMarker ) this.DragMarker.Moment = this.PosToMoment( AEvent.offsetX, true );

                if( this.DragBar )
                {
                    /*Перетаскивание бара между ресурсами*/
                    var iLine = this.Lines.ByPosY( AEvent.offsetY );
                    if (!iLine) iLine=this.DragBar.Line;
                    this.DragBar.Line=iLine;
                    this.DragBar.IDLine=iLine.UID;


                    /*Перетаскивание бара по времени*/
                    var s=this.PosToMoment( AEvent.offsetX, false);
                    var l=this.DragBar.DTEnd-this.DragBar.DTBegin;
                    var n=this.DragBar.DTBegBeforeDrag + (s - this.DragStart);
                    n=clResourceMomentGreed(this, n);

                    var sd=clResourceAlignBar(this, iLine, n, this.DragBar);
                    var se=clResourceAlignBar(this, iLine, n+l, this.DragBar);

                    if( sd ) n=sd;
                    if( se ) n=se-l;

                    this.DragBar.DTBegin = n;
                    this.DragBar.DTEnd = n + l;
                    this.ActiveBar=this.DragBar; 
                }

                if (!this.DragMarker && !this.DragBar && DragDistance>this.DragBeginAcuracy)
                {
                    if (AEvent.shiftKey==false)
                    {
                        /*Смещение по времени*/
                        this.TimeSet
                        (
                           this.DTBegin - this.TimeBySize( DeltaPos.x ), 
                           this.DTEnd - this.TimeBySize( DeltaPos.x ), 
                           false
                        );
                    }
                    else
                    {
                        /*Обработка действий с MouseBar*/
                        var ct=this.PosToMoment( AEvent.offsetX, true);
                        var SelectLine = this.Lines.ByPosY( AEvent.offsetY );
                        if (SelectLine && this.MouseBarLine==null) clResourceMouseBarShow(this, ct, ct, SelectLine);
                        if (SelectLine && this.MouseBarLine) 
                        {
                            this.MouseBarDTEnd=ct;
                            this.StatusString=clMomentDeltaToStr
                            (
                                 Math.max( this.MouseBarDTEnd, this.MouseBarDTBeg ) 
                                 - Math.min( this.MouseBarDTEnd, this.MouseBarDTBeg )
                            );
                        }
                    }

                    if (AEvent.ctrlKey==false)
                    {
                        /*Скроллинг вертикальный*/   
                        this.Top = this.Top+DeltaPos.y;
                    } 
                    else
                    {
                        /*Скалирование*/
                        var Dir;
                        Dir=DeltaPos.y;
                        var s = 1+Dir*0.02; /*Скаляр времени*/
                        this.Scale( s, AEvent.offsetX, false);
                    }
                } /*Действия с сеткой*/
            }

            if ( ! this.LeftButton )
            {
                this.ActiveMarker = this.Lines.MarkerByPos( clVector.Create( AEvent.offsetX, AEvent.offsetY ));

                if (this.ActiveMarker)
                {
                    this.StatusString=this.ActiveMarker.Caption+' '+clMomentToStr(this.ActiveMarker.DTMoment);
                }
                else
                {
                    if (AEvent.offsetX > this.LegendWidth)
                    {
                       /* Calculate active bars */
                       this.ActiveBars = this.CalcBarsByPos( clVector.Create( AEvent.offsetX, AEvent.offsetY ));
                    }
                    else
                    {
                       this.ActiveBars = [];
                    }
                    /* Calculate active bar */
                    this.ActiveBar = this.ActiveBars.length > 0 ? this.ActiveBars[ this.ActiveBars.length - 1 ] : null;
                }
            }

            if( this.OnMouseMove ) this.OnMouseMove( AEvent );

            this.Mouse = clVector.Create( AEvent.offsetX, AEvent.offsetY );
            this.Draw( AEvent );
        };


        ACanvas.onmouseout = AEvent =>
        { 
            this.Mouse = clVector.Create( AEvent.offsetX, AEvent.offsetY );
            this.Drag=false;
            this.LeftButton=false;
            if ( this.MouseBarLine != null) this.MouseBarLine = null;
        };


        /* Mouse click */
        ACanvas.onclick = AEvent =>
        {
            var Mouse = clVector.Create( AEvent.offsetX, AEvent.offsetY );
            if( ! this.Drag )
            {
                /* Markers control */
                var Marker = this.Lines.MarkerByPos( Mouse );
                if( Marker && this.OnMarkerClick ) this.OnMarkerClick( Marker, AEvent );

                /*
                if( this.ActiveBar )
                {
                    if( this.ActiveBar.onMouseClick ) {this.ActiveBar.onMouseClick( this, AEvent, this.ActiveBar)}
                }
                else
                {
                    iLine = this.Lines.ByPosY( AEvent.offsetY );
                    if( iLine && iLine.onMouseClick ) iLine.onMouseClick( this, AEvent, iLine );
                }
                */
            }
            this.Drag=false;

            if( this.OnMouseClick ) this.OnMouseClick( AEvent );
        };



        /*
            Mouse wheel scaling
        */
        ACanvas.addEventListener
        (
            'onwheel' in document ? 'wheel' : 'onmousewheel' in document ? 'mousewheel' : 'MozMousePixelScroll',
            AEvent =>
            {
                var Standart = true;
                if( this.OnMouseWheel ) Standart = this.OnMouseWheel( AEvent );
                if( Standart )
                {
                    var delta = AEvent.deltaY || AEvent.wheelDelta || AEvent.detail;
                    var s; /* Скаляр времени */
                    if (delta < 0) s = 1 / this.WheelAccuracy;
                    if (delta>0) s = 1* this.WheelAccuracy;
                    this.Scale( s, AEvent.offsetX, AEvent, false);
                    this.Draw( AEvent );
                }
            }
        );
    }



    static Create( ACanvas, ALinePars )
    {
        return new TTimeline( ACanvas, ALinePars );
    }



    /*
        Remove all elements
    */
    Clear()
    {
        this.Lines.Clear();
        return this;
    }



    GetBegin()
    {
        return this.DTBegin;
    }



    GetEnd()
    {
        return this.DTEnd;
    }



    /*
        Main drav method for timeline
    */
    Draw( AEvent )
    {
        this.Now = clNow();
        this.Scene.Maximize();

        /* Пересчет текущего временного интервала вывода в зависиости от Fade */
        if (this.Now < this.FadeMoment + clSec*this.FadeDelay)
        {
            var k = ( this.Now-this.FadeMoment ) / ( clSec * this.FadeDelay );
            this.DTBegin = TMath.ItpCos( this.DTBeginFadeStart, this.DTBeginFadeEnd, k );
            this.DTEnd = TMath.ItpCos( this.DTEndFadeStart, this.DTEndFadeEnd, k );
        }
        else 
        {
            if( this.DTBeginFadeEnd )
            {
                /* Обработка последнего кадра, когда время уже прошло а кадра еще небыло */
                this.DTBegin         = this.DTBeginFadeEnd;
                this.DTEnd           = this.DTEndFadeEnd;
                this.DTBeginFadeEnd  = null;
                this.DTEndFadeEnd    = null;
            }
        }

        /* Сборос положения топ, если записей не видно на экране */
        if( this.Lines.length > 0 )
        {
            var BLim = 
            - this.LineHeight * this.Lines.length 
            + ( this.Scene.Canvas.height - this.TimeLineHeight );

            if( this.Top < BLim ) this.Top = BLim;
            if( this.Top > 0 ) this.Top = 0;
        }

        var YearPx  = this.SizeByTime( clYear );
        var MonthPx = this.SizeByTime( clMonth );
        var DayPx   = this.SizeByTime( clDate );
        var HourPx  = this.SizeByTime( clHour );
        var MinPx   = this.SizeByTime( clMin );
        var SecPx   = this.SizeByTime( clSec );

        var CountLine =
        ( YearPx  > this.TimeLineMinWidth ? 1 : 0 ) +
        ( MonthPx > this.TimeLineMinWidth ? 1 : 0 ) +
        ( DayPx   > this.TimeLineMinWidth ? 1 : 0 ) +
        ( HourPx  > this.TimeLineMinWidth ? 1 : 0 ) +
        ( MinPx   > this.TimeLineMinWidth ? 1 : 0 ) + 
        ( SecPx   > this.TimeLineMinWidth ? 1 : 0 );

        var h = this.TimeLineHeight / CountLine;
        var NowMoment = clNow();
        var NowPos = this.MomentToPos( NowMoment );

        /* Начало отрисовки */
        clSceneDrawBegin( this.Scene );
        clSceneFlat( this.Scene );

        var ctx = this.Scene.Context;

        if( this.OnDrawBefore ) this.OnDrawBefore( ctx, this.Scene.GetRect());

        ctx.font = 'normal normal 12px Tahoma';
        ctx.lineWidth = 0.5;

        /*Отрисовка фон прошлого*/
        if( NowPos > 0)
        {
            var pe = ctx.canvas.width ? ctx.canvas.width : NowPos;
            ctx.fillStyle = this.GradientPast;
            ctx.fillRect( 0, 0, pe, ctx.canvas.height );
        }

        /*Отрисовка фон будущего*/
        if( NowPos < this.Scene.Context.canvas.width )
        {
            var fb = Math.max( NowPos, 0 );
            ctx.fillStyle = this.GradientFuture;
            ctx.fillRect( fb, 0, ctx.canvas.width - fb, ctx.canvas.height );
        }

        if( this.OnDrawWallpaper ) this.OnDrawWallpaper( ctx, this.Scene.GetRect());



        /* Bars draw */
        this.Lines.BarsDraw();

        /* Подготовка маркеров */
        /* this.MarkersCalc(); */

        /* Отирсовка маркеров */
        this.Lines.MarkersDraw();

        /* Отрисовка сетки горизонтальной */
        /* this.Lines.DrawBack( AEvent ); */

        /* Отрисовка легенды */
        if ( this.Lines.length > 0 ) this.Lines.DrawLegend( AEvent );
        else
        {
            ctx.font            = 'normal normal 32px Tahoma';
            ctx.textAlign       = 'center';
            ctx.textBaseline    = 'middle';
            ctx.fillStyle       = '#FFFFFF7C';
            ctx.fillText( 'No data', ctx.canvas.width*0.5, ctx.canvas.height*0.5);
        }

        /*Отрисовка фона таймлайна */
        ctx.fillStyle = this.GradientTimeLine;
        ctx.fillRect(0, 0, ctx.canvas.width, this.TimeLineHeight );

        /*Отрисовка сетки таймлайна*/
        ctx.lineWidth = 0.3;
        ctx.beginPath();

        if( YearPx  > this.TimeLineMinWidth ) this.DrawTimeLineDiv( 1, h );
        if( MonthPx > this.TimeLineMinWidth ) this.DrawTimeLineDiv( 2, h );
        if( DayPx   > this.TimeLineMinWidth ) this.DrawTimeLineDiv( 3, h );
        if( HourPx  > this.TimeLineMinWidth ) this.DrawTimeLineDiv( 4, h );
        if( MinPx   > this.TimeLineMinWidth ) this.DrawTimeLineDiv( 5, h );
        if( SecPx   > this.TimeLineMinWidth ) this.DrawTimeLineDiv( 6, h );

        ctx.strokeStyle = '#0000007C';
        ctx.stroke();

        /*Настройка текста таймлайна*/
        ctx.font            = 'normal normal 14px Tahoma';
        ctx.textAlign       = 'center';
        ctx.textBaseline    = 'middle';
        ctx.fillStyle       = '#FFFFFFDD';
  

        ctx.beginPath();

        if( YearPx  > this.TimeLineMinWidth )
        {
            clResourceDrawTimeLine(this, h, h*0, clYear);
            this.TimeGrid = clMonth;
            this.TimeSnap = clYear;
        };

        if( MonthPx > this.TimeLineMinWidth ){ clResourceDrawTimeLine(this, h, h*1, clMonth); this.TimeGrid=clDate; this.TimeSnap=clMonth;};
        if( DayPx   > this.TimeLineMinWidth ){ clResourceDrawTimeLine(this, h, h*2, clDate); this.TimeGrid=clHour; this.TimeSnap=clDate};
        if( HourPx  > this.TimeLineMinWidth ){ clResourceDrawTimeLine(this, h, h*3, clHour); this.TimeGrid=clMin; this.TimeSnap=clHour;};
        if( MinPx   > this.TimeLineMinWidth ){ clResourceDrawTimeLine(this, h, h*4, clMin); this.TimeGrid=clSec; this.TimeSnap=clMin;};
        if( SecPx   > this.TimeLineMinWidth ){ clResourceDrawTimeLine(this, h, h*5, clSec); this.TimeGrid=clMsec; this.TimeSnap=clSec;};
        ctx.strokeStyle = '#0000007C';
        ctx.stroke();



        if( this.Mouse )
        {
            ctx.fillStyle       = '#FFFFFFDD';
            ctx.textAlign       = 'left';
            ctx.textBaseline    = 'top';
            ctx.font            = 'normal normal 14px Tahoma';

            var DTCur = this.PosToMoment( this.Mouse.x, true );
            ctx.fillText
            (
                ( DTCur > clNow() ? 'Light future' : 'Glorious past' ) + ' ' + clMomentToStr( DTCur, ''), 
                5, 5
            );
        } 


        if( this.OnDrawAfter )
        {
            this.OnDrawAfter( ctx, this.Scene.GetRect(), AEvent );
        }

        if( this.Now < this.FadeMoment + clSec * this.FadeDelay ) 
        {
            setTimeout( () => this.Draw( null ), 20 );
        }

        return this;
    }



    DrawTimeLineDiv( ANum, AHeigth )
    {
        var Ctx = this.Scene.Context;
        Ctx.moveTo( 0, ANum * AHeigth );
        Ctx.lineTo( Ctx.canvas.width, ANum * AHeigth );
        return this;
    }



    /*
        Set the time line begin and end ages.
    */
    TimeSet( ABegin, AEnd, AFade )
    {
        if( AFade )
        {
            if
            (
                this.DTBeginFadeEnd != ABegin &&
                this.DTEndFadeEnd != AEnd &&
                ( this.OnChageAges == null || this.OnChageAges( this, ABegin, AEnd ))
            )
            {
                this.DTBeginFadeStart = this.DTBegin;
                this.DTEndFadeStart   = this.DTEnd;
                this.DTBeginFadeEnd   = ABegin;
                this.DTEndFadeEnd     = AEnd;
                this.FadeMoment       = clNow();
                this.Draw( null );
            }
        }
        else
        {
            if
            (
                this.DTBegin != ABegin && 
                this.DTEnd != AEnd &&
                ( this.OnChageAges == null || this.OnChageAges( this, ABegin, AEnd ))
            ) 
            {
                this.DTBegin = ABegin;
                this.DTEnd   = AEnd;
                this.Draw( null );
            }
        }
         
        return this;
    }



    TimeShift( APart, AFade )
    {
        var Delta = parseInt(( this.DTEnd - this.DTBegin ) * APart );
        this.TimeSet
        (
            this.DTBegin + Delta,
            this.DTEnd + Delta,
            AFade
        );
        return this;
    }




    TimeBySize( ASize )
    {
        var Result =  
        this.Scene.Canvas.width > 0 
        ? ( this.DTEnd - this.DTBegin ) / this.Scene.Canvas.width * ASize
        : 0;
        return Result;
    }




    /******************************************************************
        Markers
    */

    MarkersCalc()
    {
        var p;
        var iRec;

        for( var i=0; i < this.Lines.length; i++)
        {
            iRec = this.Lines[i];

            /* Сброс маркерных масивов */
            iRec.MarkersLeft.length = 0;
            iRec.MarkersRight.length = 0;
 
            /* Занесение маркеров в стеки слева и справа */
            for( var j=0; j < iRec.Markers.length; j++ ) 
            {
                var jMarker = iRec.Markers[j];
                p = jMarker.GetPosReal();
                if( p )
                {
                    if ( p.x < AResource.LegendWidth ) iRec.MarkersLeft.push( jMarker );
                    if ( p.x > AResource.Scene.Context.canvas.width ) iRec.MarkersRight.push( jMarker );
                }
            }

            /*Сортировка маркерных масивов*/
            iRec.MarkersLeft.sort( function( a, b ){ return( a.Moment - b.Moment )} );
            iRec.MarkersRight.sort( function( a, b ){ return( b.Moment - a.Moment )} );
        }
        return this;
    }



    /*
        NewLine
    */
    NewLine( AIDLine )
    {
        /* Serach marker by id */
        var Result = this.Lines.ByID( AIDLine );
        if( Result == null ) Result = this.Lines.New( AIDLine );
        return Result;
    }



    /*
        NewMarker
    */
    NewMarker
    (
        AIDLine,
        AIDMarker
    )
    {
        /* Serach marker by id */
        var Result = this.Lines.MarkerByID( AIDMarker );
        if ( Result == null )
        {           
            /* Search or create new line */
            var Line = this.Lines.ByID( AIDLine );
            if( Line == null ) Line = this.Lines.New( AIDLine );

            /* Create new marker for line */
            Result = Line.Markers.New( AIDMarker );
        }
        return Result;
    }





    /*
        New bar
    */
    NewBar
    (
        AIDLine,
        AIDBar
    )
    {
        /* Serach marker by id */
        var Result = this.Lines.BarByID( AIDBar );
        if ( Result == null )
        {           
            /* Search or create new line */
            var Line = this.Lines.ByID( AIDLine );
            if( Line == null ) Line = this.Lines.New( AIDLine ).SetCaption( AIDLine );

            /* Create new marker for line */
            Result = Line.Bars.New( AIDBar );
        }
        return Result;
    }



    CalcBarsByPos( APos )
    {
        var Result = [];
        var iLine = this.Lines.ByPosY( APos.y );
        if( iLine )
        {
            var c = iLine.Bars.length;
            for( var i = 0; i<c; i++ ) 
            {
                 var Rect = iLine.Bars[ i ].GetRect();
                 if
                 (
                     ( APos.x >= Rect.x - this.XMouseAccuracy ) && 
                     ( APos.x <= Rect.x + Rect.w + this.XMouseAccuracy )
                 )
                 Result.push( iLine.Bars[ i ] );
             }
        }
        return Result;
    }




    /******************************************************************
        Горизонтальные линии
    */

    /*
        Поиск линии по идентификатору
    */
    LineByID
    (
        AResource, /* Ресурс*/
        AID        /* Идентификатор линии*/
    )
    {
        var i=0;
        var r=null;
        while ( i < this.Lines.length)
        {
            if ( this.Line[i].ID==AID)
            {
                r = this.Line[i];
                i = this.Lines.length;
            }
            i++;
        }
        return r;
    }





    /**********************************************************************
        Moment and position converstions
    */
    MomentToPos( AMoment )
    {
        var d = this.DTEnd - this.DTBegin;
        var k = d < clEpsilon ? k=0 : ( AMoment - this.DTBegin ) / d;
        return parseInt( this.Scene.Canvas.width * k );
    } 



    PosToMoment( APos, ATimeGrid )
    {
        var k = APos / this.Scene.Canvas.width;
        var r = this.DTBegin + ( this.DTEnd - this.DTBegin ) * k;

        if( ATimeGrid ) r = parseInt( r / this.TimeGrid ) * this.TimeGrid;
        return r;
    }



    SizeByTime( ATime )
    {
        var d = this.DTEnd - this.DTBegin;
        return d < clEpsilon ?  0 : this.Scene.Canvas.width / d * ATime;
    }




    LineIndexToPos( ALineIndex )
    {
        return parseInt( this.TimeLineHeight + ALineIndex * this.LineHeight + this.Top );
    }



    Scale
    (
        AScale, ADTPoint, AEvent, AFade
    )
    {
        var d = this.DTEnd - this.DTBegin; /*Дельта по времени*/
        var Cur = this.PosToMoment( ADTPoint ); /*Время под курсором*/

        this.TimeSet( Cur-(Cur-this.DTBegin)*AScale, Cur+(this.DTEnd-Cur)*AScale, AFade );
 
        /* Ограничение минимального скалирования */ 
        if (this.DTEnd - this.DTBegin > clYear * 20) this.TimeSet( Cur-clYear * 20 * 0.5, Cur+clYear * 20 * 0.5, AFade);

        /*Ограничение максимального скалирования*/ 
        if (this.DTEnd - this.DTBegin < clSec*10) this.TimeSet( Cur - clSec * 10*0.5, Cur+clSec * 10 * 0.5, AFade);
    }
}









/*******************************************************************************
    Bars
*/

function clResourceAlignBar( AResource, ALine, ATime, AExcludeBar )
{
    var t = null;
    var r = AResource.AlignAcuracy;
    var p = AResource.MomentToPos( ATime);
    var l = AResource.length;
    for( var i=0; i<l; i++ ) 
    {
        iBar=AResource[i];
        if (iBar.Line==ALine && AExcludeBar!=iBar) 
        {
            var b=AResource.MomentToPos( iBar.DTBeg);
            var e=AResource.MomentToPos( iBar.DTEnd);
            if (Math.abs(p-b)<r) {r=Math.abs(p-b); t=iBar.DTBeg;}
            if (Math.abs(p-e)<r) {r=Math.abs(p-e); t=iBar.DTEnd;}
        }
    }
    return t
}




function clResourceBarByTime(AResource, ALine, ATime, AExcludeBar)
{
    var r=null;
    for (var i=AResource.length-1; (i>=0 && r==null); i--) 
    {
        iBar=AResource[i];
        if ( iBar.Line == ALine && AExcludeBar != iBar && iBar.DTBeg >= ATime && iBar.DTEnd <= ATime) r = iBar;
    }
    return r;
}




function clResourceBarModifyReset(AResource)
{
    var l = AResource.length;
    for( var i=0; i<l; i++ ) 
    {
        iBar = AResource[i];
        if( iBar.Modify )
        {
            iBar.DTBeg   = iBar.ModifyDTBeg;
            iBar.DTEnd   = iBar.ModifyDTEnd;
            iBar.IDLine  = iBar.ModifyIDLine;
            iBar.Line    = iBar.ModifyLine;
            iBar.Modify  = false;
        }
    }
}




function clResourceBarGetCaption(AResource, ARec)
{
    var r;
    if (ARec.Caption) 
    {
        r=ARec.Caption;
    } 
    else 
    {
        if (ARec.onGetCaption) r=ARec.onGetCaption(AResource, ARec);
        else r='';
    };
    return r;
}











/*
    Функция построения временного интервала
*/
function clResourceDrawTimeLine( AResource, AHgt, ATop, AInterval)
{
    var cb;
    var ce;
    var w;

    var i=AResource.DTBegin;
    do
    {
        var cb;
        var ce;

        switch (AInterval)
        { 
            case clSec:     cb=clBegSec(i); ce=clEndSec(i); break;
            case clMin:     cb=clBegMin(i); ce=clEndMin(i); break;
            case clHour:    cb=clBegHour(i); ce=clEndHour(i); break;
            case clDate:    cb=clBegDate(i); ce=clEndDate(i); break;
            case clMonth:   cb=clBegMonth(i); ce=clEndMonth(i); break;
            case clYear:    cb=clBegYear(i); ce=clEndYear(i); break;
        }

        /*Проверка минимальной границы до текущего интервала*/
        var lb = ( cb<AResource.DTBegin ) ? AResource.DTBegin : lb=cb;

        /*Проверка максимальной границы до текущего интервала*/
        var le = ( ce>AResource.DTEnd ) ? AResource.DTEnd : le=ce;

        var pb = AResource.MomentToPos( lb );
        var pe = AResource.MomentToPos( le );
        var pw = pe - pb;


        var Style = '#FFFFFFDD';
        var s;
        switch (AInterval)
        { 
            case clSec:     
                s = ( pw < 200 ? '' : 'Sec ' ) + clGetSec(i); 
            break;
            case clMin:     
                s = ( pw < 200 ? '' : 'Min ' ) + clGetMin(i); 
            break;
            case clHour:
                s = ( pw < 200 ? '' : 'Hour ' ) + clGetHour(i); 
                Style = s>=6 && s < 18 ? Style : Style = '#00FFFFDD'; 
            break;
            case clDate:
                var DayNumber = clGetDay( i );
                s = pw < 50 ? clGetDate(i) : clGetDate(i) + ' ' + clGetDayName(i)+' '; 
                Style = DayNumber == 0 || DayNumber == 6 ? Style = AResource.HolydayText : AResource.WorkdayText; 
            break;
            case clMonth:   s = pw < 60 ? clGetMonth(i)+1 : s=clGetMonthName(i); break;
            case clYear:    s = clGetYear(i); break;
        }

        clResourceDrawTimeLineBar(AResource, s, pb, ATop, pw, AHgt, Style );
        i=ce;
    }
    while( i < AResource.DTEnd && Math.abs(ce-cb)>clEpsilon );
}



function clResourceDrawTimeLineBar(AResource, ACaption, AX, AY, AW, AH, AStyle )
{ 
    var ctx = AResource.Scene.Context;
    ctx.moveTo(AX, AY);
    ctx.fillStyle = AStyle;
    ctx.lineTo(AX, AResource.Scene.Canvas.height);
    if( AW>20 ) { ctx.fillText( ACaption, AX+AW*0.5, AY+AH*0.5 ) };
}










function clResourceMomentGreed(AResource,AMoment)
{
    return parseInt(AMoment / AResource.TimeGrid) * AResource.TimeGrid;
}






function clMomenCheck( AMoment, ADefault )
{
    return AMoment ? AMoment : ADefault;
}



CanvasRenderingContext2D.prototype.TextOut = function( APosition, ASize, AHeight, AText )
{
    var SizeX = 0;
    var PosX = 0;
    var PosY = 0;

    var c = AText.length;
    for( var i = 0; i < c; i++ )
    {
        var ChrWidth = this.measureText( AText[ i ] ).width;

        switch( AText.charCodeAt( i ) )
        {
            case 10:
                PosX = 0;
                PosY += AHeight;
            break;

            case 9:
                PosX += AHeight * 4;
            break;

            default:
                if( PosX + ChrWidth > ASize.x )
                {
                    PosX = 0;
                    PosY += AHeight;
                }
                if( APosition ) this.fillText( AText[ i ], APosition.x + PosX, APosition.y + PosY );
                PosX += ChrWidth;
            break;
        }

        if( SizeX < PosX ) SizeX = PosX;
    }

    return clVector.Create( SizeX, PosY + AHeight );
};




CanvasRenderingContext2D.prototype.Hint = function( APosition, ASize, AText )
{
    var HintFont = 14;
    this.font = 'normal normal '+HintFont+'px Tahoma';

    var TextSize = ASize.Get().Sub( clVector.Create( HintFont*2, HintFont*2 ));
    TextSize = this.TextOut( null, TextSize, HintFont, AText );

    ASize = TextSize.Get().Add( clVector.Create( HintFont*2, HintFont*2 ));

    /* Check screen frame*/
    if ( APosition.x + ASize.x > this.canvas.width ) APosition.x = this.canvas.width - ASize.x;
    if ( APosition.y + ASize.y > this.canvas.height ) APosition.y = this.canvas.height - ASize.y;
    if ( APosition.x < 0 ) APosition.x = 0;
    if ( APosition.y < 0 ) APosition.y = 0;

    var TextPos = APosition.Get().Add( clVector.Create( HintFont, HintFont ));

    this.beginPath();
    this.fillStyle = '#FFFFAAFF';
    this.textBaseline = 'top';
    this.buildRoundedRect( APosition.x, APosition.y, ASize.x, ASize.y, 5, 5, 5, 5 );
    this.fill();

    this.fillStyle = '#000000FF';
    this.TextOut( TextPos, TextSize, HintFont, AText );
};







CanvasRenderingContext2D.prototype.buildRoundedRect = function( x, y, w, h, AR1, AR2, AR3, AR4 )
{
    var m=Math.min(w,h)*0.5;
    var r1=Math.min(AR1,m);
    var r2=Math.min(AR2,m);
    var r3=Math.min(AR3,m);
    var r4=Math.min(AR4,m);

    with(this) 
    {
        moveTo(x+r1, y);
        lineTo(x+w-r2, y);
        quadraticCurveTo(x+w, y, x+w, y+r2);
        lineTo(x+w, y+h-r3);
        quadraticCurveTo(x+w, y+h, x+w-r3, y+h);
        lineTo(x+r4, y+h);
        quadraticCurveTo(x, y+h, x, y+h-r4);
        lineTo(x, y+r1);
        quadraticCurveTo(x, y, x+r1, y);
    }
};
















/*******************************************************************************
    CatLair Resource Map

    Structure:

    TTimeline
    {
        TLines
        [
            TLine
            {
                TMarkers[ TMarker ],
                TBars[ TBar ]
            }
        ]
     }                
*/



var clCenter = 0;
var clRightTop = 1;
var clRightBottom = 2;
var clLeftTop = 3;
var clLeftBottom = 4;



class TTimelineElement extends TParams 
{
    Parent  = null;
    ID      = null;
    Index   = null;



    constructor( AParent, AID )
    {
        super();

        this.Parent = AParent;
        this.ID = AID;
    }



    /*
        Return index of element
    */
    GetIndex()
    {
        return this.Index;
    }
}



/****************************************************************
    Line
*/
class TLine extends TTimelineElement
{
    Caption      = '';  
    MarkersLeft  = null;
    Markers      = null;
    MarkersRight = null;
    Bars         = null;

    constructor( AParent, AID )
    {
        super( AParent, AID );

        this.MarkersLeft   = [];
        this.Markers       = TMarkers.Create( this );
        this.MarkersRight  = [];
        this.Bars          = TBars.Create( this );
    }



    SetCaption( AValue )   
    {
        this.Caption = AValue;
        return this;
    }



    GetCaption()
    {
        return this.Caption;
    }



    Clear()
    {
        this.Markers.Clear();
        this.Bars.Clear();
    }


    
    AddEvent
 
}



/****************************************************************
    Bar
*/
class TBar extends TTimelineElement
{
    DTBegin       = null;
    DTEnd         = null;
    BackFillStyle = 'green';

    SetBegin( AValue )
    {
        this.DTBegin = AValue;
        return this;
    }



    SetEnd( AValue )
    {
        this.DTEnd = AValue;
        return this;
    }

    

    SetBackFillStyle( AValue )
    {
        this.BackFillStyle = AValue;
        return this;
    }



    GetRect( )
    {
        /*
            pw - длинна
            pb - начало
            pe - конец
            pt - верх
        */

        var Timeline = this.Parent.Line.Parent.Timeline;

        /*
             Сравнение ABar с границами экрана. Если он выходит за границы экрана, то используются они
             ------|++++         |
                   |     ++++    |
                   |          +++|------
             ------|+++++++++++++|------
             ----  |             |
                   |             |  ----
        */
        var lb = Math.max( clMomenCheck( this.DTBegin, Timeline.DTBegin), Timeline.DTBegin );
        var le = Math.min( clMomenCheck( this.DTEnd,   Timeline.DTEnd),   Timeline.DTEnd );

        /*Переводим границы дат в пикселы*/
        var pb = Timeline.MomentToPos( lb );
        var pe = Timeline.MomentToPos( le );
        var pw = pe - pb;

        /* Вертикальная координата в пикселах */

        return {
           x: pb, 
           y: Timeline.LineIndexToPos( this.Parent.Line.Index ),
           w: pw,
           h: Timeline.LineHeight
        };
    }





    Draw()
    {
        var Rect = this.GetRect();

        var Timeline = this.Parent.Line.Parent.Timeline;
        var Context = Timeline.Scene.Context;

        if ( Timeline.Scene.IsRectVisible( Rect ) )
        {
            if( Timeline.OnBarDraw ) Timeline.OnBarDraw( this, Context, Rect );
            else
            {
                Context.translate( Rect.x, Rect.y );

                Context.fillStyle = this.BackFillStyle;

                if( Rect.w > 1 )
                {
                    Context.beginPath();
                    Context.rect( 0, 0, Rect.w, Rect.h, 3, 3, 3, 3 );
                    Context.fill();

                    /* Вывод раскраса бара под мышкой */
                    if( this == Timeline.ActiveBar )
                    {
                        /*Отрисовка рамки подсветки на активном баре*/
                        Context.rect( 0, 0, Rect.w, Rect.h, 3, 3, 3, 3 );

                        Context.strokeStyle = 'white';
                        Context.lineWidth = 1;
                        Context.stroke();
                    }
                }
                else 
                {
                    Context.fillRect(-1, 0, 2, Rect.h);
                }
    
                Context.translate( -Rect.x, -Rect.y );
            }

 /*

                        Context.fillStyle=Timeline.GradientGlass;
                        Context.rect(1, 1, Rect.w-2, Rect.h-2, 3, 3, 3, 3 );
                        Context.fill();

                   else
                    {
                        if (ARec.Modify) {globalAlpha=0.5} else {globalAlpha=1};
                        translate(c.pb,c.pt);

                     Отрисовка фона
                        fillStyle=ARec.BackFillStyle;
                        beginPath();
                        buildRoundedRect(0, 0, c.pw, c.ph, c.r, c.r, c.r, c.r);
                        fill();

                     Отрисовка блика на барах
                        fillStyle=AResource.GradientGlass;
                        beginPath();
                        buildRoundedRect(1, 1, c.pw-2, c.ph-2, c.r,c.r,c.r,c.r);
                        fill();

                        translate(-c.pb, -c.pt);

                     Компенсация начала вывода на легенду слева
                        var cb = Math.max(AResource.LegendWidth, c.pb);
                        var cw = c.pe-cb-5;

                        if (cw>c.ph)
                        {

                            if (ARec.Icon) 
                            {
                                var TextShiftByIcon = AResource.LineHeight;
                                fillStyle='white';
                                beginPath();
                                buildRoundedRect(c.pb+3, c.pt+3, AResource.LineHeight-6, AResource.LineHeight-6, c.r,c.r,c.r,c.r);
                                fill();
                                drawImage(ARec.Icon, c.pb+4, c.pt+4, AResource.LineHeight-8, AResource.LineHeight-8);
                            } 
                            else var TextShiftByIcon = 0;

                            fillStyle=ARec.TextFillStyle;
                            save();
                            beginPath();
                            rect(cb, c.pt, cw-c.ph*0.25, c.ph);
                            clip();
                            fillText(clResourceBarGetCaption(AResource,ARec), cb+c.ph*0.25 + TextShiftByIcon, c.pt+c.ph*0.5);
                            restore(); 
                        }
                        globalAlpha=1;
                    }

                }

*/
        }

        return this;
    }

}



/****************************************************************
    TMarker
*/
class TMarker extends TTimelineElement
{
    Moment = null;


    /*
        Set the moment
    */
    SetMoment( AMoment )
    {
        this.Moment = AMoment;
        return this;
    }



    /*
        Set the moment
    */
    GetMoment()
    {
        return this.Moment;
    }



    /*
       Return position on timeline
    */
    GetPosReal()
    {
        var r = null;
        if ( this.Moment )
        {           
            var Timeline = this.Parent.Line.Parent.Timeline;
            var IndexLine = this.Parent.Line.GetIndex();

            r = clVector.Create
            (
                Timeline.MomentToPos( this.Moment ),
                Timeline.LineIndexToPos( IndexLine ) + Timeline.LineHeight * 0.5
            );
        }
       return r;
    }



    /*
       Return position on screen with stacks correction
    */
    GetPos()
    {
        var p = this.GetPosReal();
        if(p) 
        {
            /*Поправка координат слева*/
            /*
            var il = this.Parent.Line.MarkerStackLeft.indexOf( AMarker );
            if (il>=0) p.x = AResource.LegendWidth + il * (AResource.MarkerSize+3) + AResource.MarkerSize * 0.5;
            var ir=AMarker.Line.MarkerStackRight.indexOf(AMarker);
            if (ir>=0) p.x = AResource.Scene.Context.canvas.width - ir * (AResource.MarkerSize+3) - AResource.MarkerSize * 0.5;
            */
        }
        return p;
    }



    GetRect( AActive )
    {
        var Timeline = this.Parent.Line.Parent.Timeline;

        var Shift    = Timeline.MarkerSize * ( AActive ? 1 : 0.5 );
        var Size     = Timeline.MarkerSize * ( AActive ? 2 : 1.0 );
        var Pos      = this.GetPos();

        return {
           x: Pos.x - Shift, 
           y: Pos.y - Shift,
           w: Size,
           h: Size 
        };
    }



    Draw( AActive )
    {     
        var Timeline = this.Parent.Line.Parent.Timeline;
        var Rect = this.GetRect( AActive );
        if ( Timeline.OnMarkerDraw && Timeline.Scene.IsRectVisible( Rect ) )
        {
            Timeline.OnMarkerDraw( this, Timeline.Scene.Context, Rect );
        }
        return this;
    }
}



/*********************************************************************
    TTimelineElements
*/
class TTimelineElements extends Array
{
    Timeline = null;
    


    Add( AElement )
    {
        if( this.ByID( AElement.ID ) == null )
        {
            this.push( AElement );
            AElement.Index = this.IndexOf( AElement );
            return this;
        }
        else
        {
            return null;
        }
    }



    Loop( AUserCallback  )
    {
        var i = 0;
        var c = this.length;
        var r = null;

        while( i < c && r == null )
        {
            r = AUserCallback( this[i] );
            i++;
        };

        return r;
    }



    ByID( AID )
    {
        var i = 0;
        var c = this.length;
        var r = null;

        while( i < c && r == null )
        {
            if( this[ i ].ID == AID ) r = this[i];
            i++;
        };

        return r;
    }



    CalcIndexs() 
    {
        var c = this.length;
        for( var i = 0; i < c; i++ ) this[ i ].Index = i;
        return this;
    }




    IndexOf( AElement )
    {
        return this.indexOf( AElement );
    }



    Remove( AElement)
    {
        var i = this.IndexOf( AElement );
        if (i >=0 )
        {
            delete this[i];
            this.splice(i,1);
        }
        return this;
    }



    Clear()
    {
        this.length = 0;
        return this;
    }



    Sort( AProperty )
    {
        this.sort( ( a, b ) => ( a[ AProperty ] > b[ AProperty ] ? 1 : ( a[ AProperty ] < b[ AProperty ] ? -1 : 0 ) ));
        this.CalcIndexs();
        return this; 
    }
}



/***************************************************************************
    Horizontal lines list for timeline
*/
class TLines extends TTimelineElements
{
    static Create( ATimeline )
    {
        var Lines = new TLines();
        Lines.Timeline = ATimeline;
        return Lines;
    }


     
    New( AID )
    {
        var Line = this.ByID( AID );
        if( ! Line ) 
        { 
            Line = new TLine( this, AID );
            this.Add( Line );
        }
        return Line;
    }



    /*
        Clear arrays Bars and markers in all lines
    */
    Clear()
    {
        var i = this.length;
        var c = 0;
        while( i < c )
        {
            this[i].Clear();
            i++;
        };
        super.Clear();         
        return this;
    }



    /*
        Line by positon
    */
    ByPosY( APosY )
    {
        var LineIndex = parseInt(( APosY - ( this.Timeline.TimeLineHeight + 2 + this.Timeline.Top )) / this.Timeline.LineHeight );
        return ( LineIndex >= 0 ) && ( LineIndex < this.Timeline.Lines.length ) ? this.Timeline.Lines[ LineIndex ] : null;
    } 



    /*
        Получение маркера по позиции
    */
    MarkerByPos( APos )
    {
        var Result = null;
        var c = this.length;      
        for( var i = 0; i < c && Result == null; i++ ) 
        {
            Result = this[i].Markers.ByPos( APos );
        }
        return Result;
    }



    /*
        Получение маркеров по позиции
    */
    MarkersByPos( APos )
    {
        var Result = [];
        var c = this.length;      
        for( var i = 0; i < c; i++ ) 
        {
            this[i].Markers.MarkersByPos( APos, Result );
        }
        return Result;
    }



    /*
        Serch the marker by ID
    */
    MarkerByID( AID )
    {
        var Result = null;
        var c = this.length;      
        for( var i = 0; i < c && Result == null; i++ ) 
        {
            Result = this[i].Markers.ByID( AID );
        }
        return Result;
    }



    /*
        Serch the marker by ID
    */
    MarkersLoop( AUserCallback )
    {
        var Result = null;
        var c = this.length;      
        for( var i = 0; i < c && Result == null; i++ ) 
        {
            Result = this[i].Markers.Loop( AUserCallback );
        }
        return Result;
    }



    MarkersDraw()
    {
        var Context  = this.Timeline.Scene.Context;
        var c = this.length;

        /* Carkers draw before call */
        if( this.Timeline.OnMarkersDrawBefore ) this.Timeline.OnMarkersDrawBefore( Context );

        /* Loop for each line */
        for( var i=0; i < c; i++) this[ i ].Markers.Draw();

        /* Carkers draw after call */
        if( this.Timeline.OnMarkersDrawAfter ) this.Timeline.OnMarkersDrawAfter( Context );

        return this;
    }



    BarsDraw()
    {
        var Context  = this.Timeline.Scene.Context;
        var c = this.length;

        /* Carkers draw before call */
        if( this.Timeline.OnBarsDrawBefore ) this.Timeline.OnBarsDrawBefore( Context );

        /* Loop for each line */
        for( var i=0; i < c; i++) this[ i ].Bars.Draw();

        /* Carkers draw after call */
        if( this.Timeline.OnBarsDrawAfter ) this.Timeline.OnBarsDrawAfter( Context );

        return this;
    }



    /*
        Serch the marker by ID
    */
    BarByID( AID )
    {
        var Result = null;
        var c = this.length;      
        for( var i = 0; i < c && Result == null; i++ ) 
        {
            Result = this[i].Bars.ByID( AID );
        }
        return Result;
    }




    DrawBack( AEvent )
    {
        var Context  = this.Timeline.Scene.Context;

        Context.lineWidth    = 0.5;
        Context.strokeStyle  = '#0000007C';

        var i=0;
        var l = this.length;

        Context.beginPath();
        for( var Index = 0; Index < l; Index++ )
        {
            var pl=0;
            var pt = this.Timeline.LineIndexToPos( Index ) + this.Timeline.LineHeight;
            if( pt >= 0 && pt <= Context.canvas.height )
            {
                Context.moveTo(pl, pt);
                Context.lineTo( Context.canvas.width, pt);
            }
        }
        Context.stroke();
    }



    DrawLegend( AEvent )
    {
        var Context  = this.Timeline.Scene.Context;
        var ActiveLine = AEvent ? this.ByPosY( AEvent.offsetY ) : null;

        for( var Index = 0; Index < this.length; Index++ )
        {
            var pl = 0;
            var pt = this.Timeline.LineIndexToPos( Index );
            var ph = this.Timeline.LineHeight;

            if( pt >= 0 && pt <= Context.canvas.height )
            {
                Context.fillStyle 
                = this[ Index ] == ActiveLine
                ? this.Timeline.GradientLegendBackActive
                : this.Timeline.GradientLegendBack;

                Context.translate( pl, pt );
                Context.fillRect(0, 0, this.Timeline.LegendWidth, this.Timeline.LineHeight );
                Context.translate(-pl, -pt);

                Context.save();
                Context.rect( pl + 6 + this.Timeline.LineHeight, pt, this.Timeline.LegendWidth, ph );
                Context.clip();

                Context.fillStyle 
                = this[ Index ] == ActiveLine
                ? this.Timeline.GradientLegendTextActive
                : this.Timeline.GradientLegendText;

                Context.fillText( this[ Index ].Caption, pl + 6 + this.Timeline.LineHeight, pt + this.Timeline.LineHeight * 0.5 );
                Context.restore();

                if( this.Image ) Context.drawImage( this[ Index ].Image, pl+2, pt, this.Timeline.LineHeight - 2, this.Timeline.LineHeight - 2 );
            }
        }

        return this;
    }
}






/*******************************************************************************
    Bars list for timeline
*/
class TBars extends TTimelineElements
{
     Line = null;



     static Create( ALine )
     {
         var Bars = new TBars();
         Bars.Line = ALine;
         return Bars;
     }



     /*
         Create new bar with ID
     */
     New( AID )
     {
         var Bar = new TBar( this, AID );
         this.Add( Bar );
         return Bar;
     }



     /* 
         Отирсовка баров 
     */
     Draw()
     {
         var Context  = this.Line.Parent.Timeline.Scene.Context;

         Context.font            = this.BarFont;
         Context.textAlign       = 'left';
         Context.textBaseline    = 'middle';
         Context.lineWidth       = 0.5;

         var c = this.length;
         for( var i = 0; i < c; i++ ) this[i].Draw();

         return this;
     }

}



/*******************************************************************************
    Markers list for timeline
*/
class TMarkers extends TTimelineElements
{
     Line = null;



     static Create( ALine )
     {
         var Markers = new TMarkers();
         Markers.Line = ALine;
         return Markers;
     }



     New( AID )
     {
         var Marker = new TMarker( this, AID );
         this.Add( Marker );
         return Marker;
     }



     /*
         Получение маркера по позиции
     */
     ByPos( APos )
     {
        var Result = null;
        var iPos;
        var c = this.length;      
        var i = 0;

        var hm = this.Line.Parent.Timeline.MarkerSize * 0.5;

        while( i < c && Result == null ) 
        {
            var iPos = this[i].GetPos();
            if 
            (
                ( iPos != null ) &&
                ( APos.x > iPos.x - hm ) &&
                ( APos.x < iPos.x + hm ) &&
                ( APos.y > iPos.y - hm ) &&
                ( APos.y < iPos.y + hm )
            )
            {
                Result = this[i];
            }
            i++;
        }

        return Result;
    }



     /*
         Получение списка маркеров по позиции
     */
     MarkersByPos( APos, AResult )
     {
        if( !AResult ) AResult = [];
        var iPos;
        var c = this.length;
        var i = 0;

        var hm = this.Line.Parent.Timeline.MarkerSize * 0.5;

        while( i < c ) 
        {
            var iPos = this[i].GetPos();
            if 
            (
                ( iPos != null ) &&
                ( APos.x > iPos.x - hm ) &&
                ( APos.x < iPos.x + hm ) &&
                ( APos.y > iPos.y - hm ) &&
                ( APos.y < iPos.y + hm )
            )
            {
                AResult.push( this[i] );
            }
            i++;
        }

        return AResult;
    }



    Draw()
    {
        var c = this.length;
        for( var i=0; i<c; i++ ) this[i].Draw();
        return this;
    }
}
