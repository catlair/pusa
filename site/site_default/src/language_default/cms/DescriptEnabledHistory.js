class TEnabledHistory
{
    static Popup( AApplication, ADescript )
    {
        AApplication.ScriptWait
        (
            [ 'clMomentElement.js' ],
            () =>
            {
               var Content = decodeURIComponent( '<cl content="EnabledHistoryForm.html" pars="true" convert="URI"/>' );
               var Popup = AApplication.Popup({ TypeContent:tcText, Content:Content });

               /* Popup prepare */
               AApplication.MomentActivate( Popup );
               AApplication.SelectActivate( Popup );

               /* Check DT filed */
               var Descript = Popup.ChildByID( 'IDDescript' );
               var DTBegin = Popup.ChildByID( 'DTBegin' );
               var DTEnd = Popup.ChildByID( 'DTEnd' );
               var Container = Popup.ChildsByClass( 'List' )[0];

               Descript.Refresh( ADescript.ID );
    
               Popup.Refresh = () =>
               {
                   TDOMDataset.Create( AApplication )
                   .SetParam( 'IDDescript', IDDescript.value )
                   .SetParam( 'DTBegin', DTBegin.value )
                   .SetParam( 'DTEnd', DTEnd.value )
                   .Load
                   ({
                       Name:            'Descript.EnabledHistory',
                       ParentContainer: Container,
                       RecordContent:   decodeURIComponent( '<cl content="DescriptEnabledHistoryRecord.html" pars="true" convert="URI"/>' ),
                       OnAfterRecord:   ( Content, Record ) => { console.log( Record, Content ) }
                   });
               }

               Popup.ChildsByClass( 'BtnRefresh', Btn => Btn.onclick = () => Popup.Refresh() );
               Popup.Refresh();
           }
       );
    }
}





