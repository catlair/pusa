/*
    Catlair JS

    Консоль управления объектами
    Расширение TCatlair

    still@itserv.ru
*/


/*
    Console
    this.WinControl - контроллер оконного ифейса
*/
TCatlair.prototype.Console = function(APrm)
{
    var that = this;

    /* проверка на необходимость авторизации при вызове консоли */
    if (APrm.Authorize)
    {
        /* проверка текущего состояния авторизации */
        that.AuthorizeStatus
        (
            {
                OnUser:Start, /* вызов основной функции */
                OnGuest:function()
                {
                    /* вызов формы авторизации */
                    that.Authorization({OnAfterLogin:Start});
                }
            }
        );
    }
    else
    {
        /* или запуск консоли заразу без авторизации */
        Start();
    }



    /*
         основное тело функции
     */
    function Start()
    {
        if (that.ConsoleWin && that.ConsoleWin!=null)
        {
            /* Вывод окна в том случае если уже создано */
            that.ConsoleWin.Maximize();
            that.ConsoleWin.Active();
        }
        else
        {
            /* Загрузка стилей */
            that.StyleLoad({ID:'clWindow.css'});
            that.StyleLoad({ID:'clConsole.css'});

            /* Ожидание загрузки скриптов */
            that.ScriptWait
            (
                [
                    'clWindow.js',
                    'RGBA.js',
                    'clDescript.js',
                    'clDescripts.js',
                    'clDescriptTree.js'
                ],
                function()
                {
                    if (!that.WinControl) that.WinControl = new clWinControl();

                    /* Создание окна */
                    var Console = that.WinControl.WindowCreate('Console');
                    Console.SetCaption('Console');
                    Console.SetToolbar(decodeURIComponent( '<cl content="ConsoleToolbar.html" pars="true" convert="URI"/>' ));
                    Console.SetContent(decodeURIComponent( '<cl content="ConsoleForm.html" convert="URI"/>' ));
                    Console.Icon.SetParams({Caption:'Console', Group:'Form', GUIDImage:'ooo'});

                    /* Создание перечня дескриптов */
                    Console.Descripts = new TDescripts();
                    Console.Descripts.RecordConteiner = Console.ChildByID('RecordList');
                    Console.Descripts.RecordContentDefault = decodeURIComponent('<cl content="DescriptRecord.html" pars="true" convert="URI"/>');
                    Console.Descripts.OnAfterLoadRecord = function(AConteiner)
                    {
                        AConteiner.onclick = function()
                        {
                            var Descript = new TDescript();
                            Descript.Option({ ID:AConteiner.Record.ID });
                        };

                        AConteiner.draggable=true;
                        AConteiner.ondragstart = function(AEvent)
                        {
                           /* this GLOBAL variable */
                           DragDescriptSourceConteiner = AConteiner;
                           return true;
                        };
                    };

                    /* Creation of the tree */
                    Console.DescriptTree = new TDescriptTree();
                    Console.DescriptTree.Tree = Console.ChildByID('TreeConteiner');
                    Console.DescriptTree.ConteinerContent = decodeURIComponent('<cl content="DescriptConteinerTree.html" pars="true" optimize="true" convert="URI"/>');

                    /* Функция опрпеделения текущей раскладки */
                    Console.DescriptTree.OnGetLang = function()
                    {
                        return Console.ChildByID('IDLang').value;
                    };

                    /* Обработка клика на записи */
                    Console.DescriptTree.OnRecordClick = function(AConteiner)
                    {
                        Console.Refresh();
                    };

                    /*Поиск и обновление списка записей*/
                    Console.Refresh = function()
                    {
                        var Params = {};
                        if (Search) Params.Find = Search.value;
                        if (Console.DescriptTree.IDSelect) Params.IDParent = Console.DescriptTree.IDSelect;
                        Params.IDLang           = Console.ChildByID('IDLang').value;
                        Params.RecordCurrent    = 0;
                        Params.Continue         = 'scroll';
                        Params.Params           = 'true';
                        Params.RGBAHex          = 'true';
                        this.Descripts.Load (Params);
                    };



                    /*Действие на загрузку файла*/
                    Console.ChildByID('BtnUpload').onchange = function()
                    {
                        that.FilesUpload
                        ({
                            Files:this.ChildByID('File'),
                            IDParent:Console.DescriptTree.IDSelect,
                            ID:null
                        });
                    };

                    /*Действия ввод символов в консоль*/
                    Console.ChildByID('Search').onkeyup = function(AEvent)
                    {
                        if ( AEvent.keyCode == 13 ) Console.Refresh();
                    };

                    /*Создание дескрипта*/
                    Console.ChildByID('BtnAdd').onclick=function()
                    {
                        var IDParent = Console.DescriptTree.IDSelect;
                        var Descript = new TDescript();
                        Descript.Edit({IDParent:IDParent});
                    };

                    /* обновление дескрипта */
                    Console.ChildByID('BtnFind').onclick=function()
                    {
                        Console.Refresh();
                    };

                    /*Активация всех элементов выбора окна*/
                    cl.SelectActivate(Console.Control);

                    /*Активация всех элементов выбора окна*/
                    Console.DescriptTree.LoadRoot();
                }
            );
        }
    }
};
