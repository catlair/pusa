/*
*/

class TGeolocation extends TResult
{    
    constructor()
    {
        super();
        this.ScanerHandle = null;
        this.Timer = null;
    }


    /*
        Initialize geolocation object
    */
    Init()
    {
        if ('geolocation' in navigator) 
        {
            this.SetOk();
        } 
        else
        {
            this.Code = 'NotAviable';
        }
        return this;
    }



    /*
       Сканирование геопозиции
    */
    ScanStart( AParams )
    {

        var that = this;
        if ( this.ScanerHandle == null && this.Timer == null )
        {
            if ( !AParams.HighAccuracy ) AParams.HighAccuracy = false;
            if ( !AParams.RequestTimeout ) AParams.RequestTimeout = 5000;
            if ( !AParams.AnswerTimeout ) AParams.AnswerTimeout = 0;

  /*          
        navigator.geolocation.watchPosition
        (
           function(APosition)
           {
               if ( AParams.OnSuccess )
               {
                   AParams.Position = APosition;
                   AParams.Geopoint = new TGeopoint();
                   AParams.Geopoint.Set( APosition.coords.latitude, APosition.coords.longitude, APosition.coords.accuracy );
                   AParams.OnSuccess( AParams );
               }
           },
           function(AError)
           {

               if ( AParams.OnError )
               {
                   AParams.Error = AError;
                   AParams.OnError( AParams );
               }
           },

           {
               enableHighAccuracy: AParams.HighAccuracy,
               timeout:            AParams.RequestTimeout,
               maximumAge:         AParams.AnswerTimeout
           }
       );
*/

            that.GetPosition( AParams );
            this.Timer = setInterval
            (
                 function()
                 {
                     that.GetPosition( AParams );
                 },
                 AParams.RequestTimeout * 1.1
            );

        }
        return this;
    }



    /*
        Остановка сканирования
    */
    ScanStop()
    {
        if ( this.Timer != null )
        {
             this.Timer = clearInterval( this.Timer );
             this.Timer = null;
        }
        return this;
    }



    /*
        Получение геопозиции
    */
    GetPosition( AParams )
    {
        if ( AParams.OnBeforeGetPosition ) AParams.OnBeforeGetPosition( AParams );

        var that = this;
        navigator.geolocation.getCurrentPosition
        (
           APosition =>
           {
               /* Success position */ 
               if ( AParams.OnSuccess )
               {
                   AParams.Position = APosition;
                   AParams.Geopoint = new TGeopoint();
                   AParams.Geopoint.Set( APosition.coords.latitude, APosition.coords.longitude, APosition.coords.accuracy );
                   AParams.OnSuccess( AParams );
               }
           },
           AError => 
           {
               /* Error get position */ 
               if ( AParams.OnError )
               {
                   AParams.Error = AError;
                   AParams.OnError( AParams );
               }
           },

           {
               enableHighAccuracy: AParams.HighAccuracy,
               timeout:            AParams.RequestTimeout,
               maximumAge:         AParams.AnswerTimeout
           }
       );
    }


}
