/*
    Catlair JS Copyright (C) 2019  a@itserv.ru
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

    CatlairPHP
    12.04.2016
    still@itserv.ru
    Loader library
*/

/*
     AParams.Code - при наличии данного параметра скрипт будет загружен на прямую из него
     AParams.OnAfterLoad:function(AParams) - вызов функции в которую передаюется PAarams
*/

function clScriptLoad( AParams )
{
    var that = this;

    /* Отметка о начале работы */
    if ( AParams.Log ) AParams.Log.JobBegin();

    /* Поиск идентификатора который возможно ранее был загружен */
    var Script = document.scripts[ AParams.URL ];
    if( !Script )
    {
        /* Create new container and put it in to params */
        Script = AddConteiner ( AParams.URL );
        AParams.Script = Script;

        if ( AParams.Code )
        {
            /* Выполняем пользовательскую функцию, если скрипт уже загружен */
            Script.innerHTML = AParams.Code;
            OnLoad(AParams);
        }
        else
        {
            /* Загрузка скрипта через URL */
            var URL = ( AParams.URL && AParams.URL.indexOf( 'http' ) === 0 ? '' : '?template=' ) + AParams.URL;

            /* Добавление раздела Script */
            Script.onload = function(){ OnLoad(AParams) };
            Script.src = URL;
            /* console.log( 'Begin to load dynamic script ' + Script.src ); */
        }
    }
    else
    {
        /* Выполняем пользовательскую функцию, если скрипт уже загружен */
        AParams.Script = Script;
        OnLoad(AParams);
    }

    /* Add script conteiner */
    function AddConteiner( AID )
    {
        var Script = document.createElement( 'script' );
        var Head = document.getElementsByTagName('head')[0];
        Head.append( Script );
        Script.id = AID;
        Script.type = 'text/javascript';
        return Script;
    }

    /* On after load action */
    function OnLoad(p)
    {
        console.log('Loaded script ' + p.Script.src);
        if ( p.Log ) p.Log.JobEnd();
        p.Script.Loaded=true;
        if (p.OnAfterLoad) p.OnAfterLoad( p );
    }

    return this;
}


/*
    Ожидание завершения загрузки скриптов
    AList:array - aray of strings with scriptname
    ACall:function() - for user function
*/
function clScriptWait ( AList, ACall, ALog )
{
    var c = 0;
    var l = AList.length;

    for ( var i=0; i < l; i++ )
    {
        var iScript = document.scripts[ AList[ i ] ];
        if ( !iScript ) clScriptLoad({ URL: AList[ i ], Log:ALog });
        if ( iScript && iScript.Loaded ) c++;
    }

    if ( c==i )
    {
        ACall();
    }
    else
    {
        setTimeout( () => clScriptWait( AList, ACall ), 100 );
    }
    return this;
};
