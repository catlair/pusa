/*
 * Task object
 */
function TMessage()
{
    this.ID = null;
    this.IDType = 'Message';
    return this;
}

/* Inherites from TDescript */
Object.setPrototypeOf(TMessage.prototype, TDescript.prototype);
