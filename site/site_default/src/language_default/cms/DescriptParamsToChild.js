class TDescriptParamsToChild
{
    static Do( AApplication, ADescript, AParams )
    {
        /* Create form and popup */
        var Popup = AApplication.Popup
        ({
            TypeContent: tcText,
            Content: decodeURIComponent( '<cl content="DescriptParamsToChildFrom.html" pars="true" convert="URI"/>' )
        });

        /* Init popup */
        AApplication.SelectActivate( Popup );

        /* Set values for form */
        Popup.ChildByID( 'IDParent' ).Refresh( AParams.IDParent ? AParams.IDParent : null );
        Popup.ChildByID( 'IDBind' ).Refresh( AParams.IDBind ? AParams.IDBind : null );

        /* Action on ok */
        Popup.ChildByID
        (
            'BtnOk',
            Btn => Btn.onclick = () =>
            {
                TRequest.Create( AApplication )
                .SetParam( 'ID', Popup.ChildByID( 'IDParent' ).value )
                .SetParam( 'IDBind', Popup.ChildByID( 'IDBind' ).value )
                .Execute
                ({
                    Name: 'Descript.CopyParamsToChild',
                    OnAfterExecute: Result => Popup.Close()
                });
            }
        );

        /* Action on cancel */
        Popup.ChildByID( 'BtnCancel', Btn => Btn.onclick = () => Popup.Close());
    }
}



