/*
 Catlair JS Copyright (C) 2019  a@itserv.ru

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.


 still@itserv.ru
*/



/*
    Phone validate
*/
function PhoneValidate(AEvent)
{
    var St = AEvent.target.value.replace(/\D/g, '');
    St = St.replace
    (
        /^(\d{0,1})(\d{0,3})(\d{0,3})(\d{0,2})(\d{0,2})\d*?$/,
        ( match, p1, p2, p3, p4, p5 ) =>
        {
            var r = '';
            if( p1.length > 0 ) r = p1;
            if( p2.length > 0 ) r += '-' + p2;
            if( p3.length > 0 ) r += '-' + p3;
            if( p4.length > 0 ) r += '-' + p4;
            if( p5.length > 0 ) r += '-' + p5;
            return r;
        }
    );
    AEvent.target.value = St;
}



/*
    Moment validate
*/
function MomentValidate( AEvent, AFormat )
{

    /* Remove all characters except digits */
    var St = AEvent.target.value.replace( /\D/g, '' );

    /* Calculate length of parts and separators */
    var Sep = [];
    var Len = [];
    for( var i = 0; i < AFormat.length; i++ )
    {
        switch( AFormat[i] )
        {
            case 'Y': Len.push( 4 ); break;
            case 'm':
            case 'd':
            case 'H':
            case 'i':
            case 's': Len.push( 2 ); break;
            default: Sep.push( AFormat[ i ] ); break;
        }
    }

    /* Don't touch it. It is small, but it will have eat you. */
    var Reg = new RegExp( '^(\\d{0,' + Len.join( '})(\\d{0,' ) + '})\\d*?$' );

    if( Reg )
    {
        AEvent.target.value = St.replace
        (
            Reg,
            ( match, p0, p1, p2, p3, p4, p5 ) =>
            {
                var r = '';
                if( p0.length > 0 ) r = p0;
                if( p1.length > 0 ) r += Sep[ 0 ] + p1;
                if( p2.length > 0 ) r += Sep[ 1 ] + p2;
                if( p3.length > 0 ) r += Sep[ 2 ] + p3;
                if( p4.length > 0 ) r += Sep[ 3 ] + p4;
                if( p5.length > 0 ) r += Sep[ 4 ] + p5;
                return r;
            }
        );
    }
}



/*
    Phone validate
*/
function CardValidate( AEvent )
{
    var St = AEvent.target.value.replace(/\D/g, '');
    St = St.replace
    (
        /^(\d{0,4})(\d{0,4})(\d{0,4})(\d{0,4})\d*?$/,
        function(match, p1, p2, p3, p4)
        {
            if( p1.length > 0 ) r = p1;
            if( p2.length > 0 ) r += ' ' + p2;
            if( p3.length > 0 ) r += ' ' + p3;
            if( p4.length > 0 ) r += ' ' + p4;
            return r;
        }
    );
    AEvent.target.value = St;
}




/*
    Time interval validate
*/
function TimeIntervalValidate( AEvent )
{
    function ParsHour( AValue )
    {
        AValue = AValue.substr( 0, 2 );
        var Hour = parseInt( AValue );
        Hour = Hour < 0 ? 0 : Hour > 24 ? 24 : Hour;
        return Hour.toString().padStart( 2, "0");
    };

    function ParsMin( AValue )
    {
        AValue = AValue.substr( 0, 2 );
        var Hour = parseInt( AValue );
        Hour = Hour < 0 ? 0 : Hour > 60 ? 60 : Hour;
        return Hour.toString().padStart( 2, "0");
    };

    var St = AEvent.target.value.replace(/\D/g, '');
    AEvent.target.value = St.replace
    (
        /^(\d{0,2})(\d{0,2})(\d{0,2})(\d{0,2})\d*?$/,
        function(match,p1,p2,p3,p4,p5)
        {
            var r="";
            if( p1.length == 1 ) r = r + p1 ;
            if( p1.length > 1 ) r = r + ParsHour( p1 );

            if( p2.length == 1 ) r = r + ":" + p2;
            if( p2.length > 1 ) r = r + ":" + ParsMin( p2 );

            if( p3.length == 1 ) r = r + " > " + p3;
            if( p3.length > 1 ) r = r + " > " + ParsHour( p3 );

            if( p4.length == 1 ) r = r + ":" + p4;
            if( p4.length > 1 ) r = r + ":" + ParsMin( p4 );

            return r;
        }
    );
}



