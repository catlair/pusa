# Sites

Pusa on Catlair supported many sites on a single host. Each site is identified by a unique identifier and contains many files and subfolders.
The site files are located in the folder **[ROOT]/site/[ID_SITE]** where, ID_SITE is an identifer.

# Site creation

To create a site, you need to create its folder and configure [domains](site/site_default/src/language_ru/man/domain_config.md) for it.
The site name can consist of English letters, numbers and '_' symbol.
It is possible to use any other name, however Pusa has not been tested for such modes.

# File rights

The web server service must have access to the site folder.
For a standard configuration with an nginx server, you need to give *rx* access to the **www-data** user to the site folder.
The site folder can have a different structure. The webserver must have the following minimum permissions on subfolders:

| Folder    | Rights | Autocreate | Description |
| --- | :--- | :--- | ---
| bash      | r     | - | bash scripts
| log       | rw    | + | log files
| src       | r     | - | craphics and text content
| php       | r     | - | PHP source code
| cache     | rw    | + | data and files cache
| config    | r     | - | configuration files
| tmp       | rw    | + | temporary files
| storage   | rw    | - | files storage

# Default site

Pusa expects a default site with id **site_default** in the **[ROOT]/site/site_default** folder.
If the required content is missing from the application's site folder, Pusa will try to find it in **site_default**.
